%RSTRTRIM Remove non-standard characters from a string.
%    RSTRTRIM(X) returns a string equivalent to X after removal of
%    the non-letter and non-numeric characters (excluding '.').
%
% Rodrigo Fernandez-Gonzalez
% fernanr1@mskcc.org
% 09/18/2007


function str = rstrtrim(in)
str = in;
str = str(find(str ~= ' '));
str = str(find(str ~= ''''));
str = str(find(str ~= ','));
str = str(find(str ~= '*'));
str = str(find(str ~= '?'));
str = str(find(str ~= '!'));
str = str(find(str ~= '@'));
str = str(find(str ~= '#'));
str = str(find(str ~= '$'));
str = str(find(str ~= '%'));
str = str(find(str ~= '^'));
str = str(find(str ~= '&'));
str = str(find(str ~= '('));
str = str(find(str ~= ')'));
str = str(find(str ~= '|'));
str = str(find(str ~= '/'));
str = str(find(str ~= '\'));
str = str(find(str ~= '='));
str = str(find(str ~= '~'));
str = str(find(str ~= '`'));






