% cb_setpixvalue(fig, val, brush)
function cb_setpixvalue(fig, val, brush)
ax_children = get(get(fig, 'CurrentAxes'), 'Children');
img = ax_children(end);
udata = get(fig, 'UserData');
udata.ax = get(img,'Parent');
udata.oldAxesUnits = get(udata.ax,'Units');
udata.oldNumberTitle = udata.curslice;
set(udata.ax,'Units','pixels');
%      if strcmp(get(fig,'SelectionType'),'alt')
%         % Right mouse button
%         %udata.coords = dipfig_getcurpos(udata.ax); % Always over image!
%         udata.lineh = line([udata.coords(1),udata.coords(1)],...
%             [udata.coords(2),udata.coords(2)],'EraseMode','xor','Color',[0,0,0.8]);
%         set(fig,'WindowButtonMotionFcn','diptest(''motionR'')',...
%                 'WindowButtonUpFcn','diptest(''up'')',...
%                 'NumberTitle','off','UserData',[]);   % Solve MATLAB bug!
%         set(fig,'UserData',udata);
%         updateDisplayR(fig,udata.ax,udata);
%      else
% Left mouse button
pos = round(get(udata.ax, 'CurrentPoint'));

if ~isempty(pos)
    pos = pos(1, 1:2);
    pos(3) = udata.curslice;
    
    half_brush = floor((brush - 1) / 2);
    udata.slices(max(0, pos(1)-half_brush):min(size(udata.slices, 1) - 1, pos(1)+half_brush), max(0, pos(2)-half_brush):min(size(udata.slices, 2) - 1, pos(2)+half_brush), pos(3)) = val;
    
    if ndims(udata.imagedata) == 3
        udata.imagedata = squeeze(udata.imagedata);
    end
    udata.imagedata(max(0, pos(1)-half_brush):min(size(udata.slices, 1) - 1, pos(1)+half_brush), max(0, pos(2)-half_brush):min(size(udata.slices, 2) - 1, pos(2)+half_brush)) = val;
    
    display_data(fig, img, udata);
end
end