% [Aw, angle, crappy_pixels, nang, phi] = orientation2(I, bin_sz, measurement_type, sigma, s, background_fraction, s_deviation, deviation_from_neighbors, threshold_param, an_roi)
%
% Calculates the predominant orientations in image I based on gradients. See [Chaudhuri, Kundu and Sarkar, 1993] and [Karlon, Hsu, Li et al., 1999] for more details.
%
% I is an input dip_image.
% bin_sz is the width in degrees of the bins used to combine pixels with different orientations.
% measurement_type is 'gradient' or 'brightness', depending on what information you want to use to calculate the score of each orientation.
% sigma controls the range of action of the gradient kernel (how far the pixel values are significantly large). Defaults to 3.0.
% s is the size of the kernel (in reality, kernel size is (2s+1)x(2s+1)). A good value is 7. For s = 7, 8, 9, ... the kernel values remain fairly similar and are very close to zero on the edges of the kernel. For s >= 12 the values added are zero, so there is no change in the results.
% background_fraction is used only if the user wants to remove dim pixels from the analysis (otherwise, just do not use). Pixel orientation will be calculated based on the image WITH NO BACKGROUND PIXELS (~ crappy_pixels). If so, the image histogram is calculated. This is typically formed by a peak with a tail. The threshold for dim pixels is set at the value to the right of the histogram peak where the full width at background_fraction maximum is computed. For movie images, 0.5 works fairly well. For high resolution images, the image background is closer to the peak of the image histogram (can use 1.0). If the value of this parameter is 'isodata', then an isodata threshold is applied to I, and the pixels below the threshold are considered as the background. If the value is 'adaptive', an adaptive threshold with window size 2*s+1 is used. When a specific number is provided (>=1) the pixels below that number are the background. Alternatively, this can be a binary image that will be used directly to mask the pixels BUT NOT TO CALCULATE GRADIENT ORIENTATION. Or a greyscale image that will be thresholded (using the isodata algorithm, see help threshold for more information) to generate a mask that will be uses to determine the foreground pixels. Also, when providing a greyscale image, it will be used to calculate the pixel orientations (this assumes that the greyscale image provided nicely outlines the objects of interest, e.g. stretch(~ original_image < background threshold)). background_fraction can be a negative number between 0 and 1. In that case, the pixels where the gradient magnitude is smaller than abs(background_fraction)*max(Gabs) are not used.
% s_deviation is the size of the kernel used to calculate if the orientation assigned to a pixel differs too much from its neighbors. This value defaults to round(s/2). THIS VALUE IS CALLED sk IN THE CODE.
% deviation_from_neighbors is the angle in radians that a pixel is allowed to differ from the mean orientation of the pixels around it. Defaults to pi/18 (10 degrees).
% threshold_param: if using 'adaptive' background fraction, this value is the size of the smallest continuous object to be considered signal. 5 is a good value for 40X images of cell outlines. 100-500 is better for fixed images with higher magnification.
% an_roi: binary dip_image that limits the region of I where the analysis of pixel orientation will be conducted. This is only implemented when background_fraction is 'adaptive'.
%
% Aw is a matrix that contains, for every possible orientation (second dimension, [0,90) degrees), the contribution of the corresponding pixels. Thus, Aw(:, 1) contains the contribution of all the pixels with [0,1)-degree orientation (assuming bin_sz == 1).
% angle returns the highest scored orientation found in the image from 0 to 90 (folding the histogram around 90).
% crappy_pixels is a binary dipimage in which pixels are set to 1 if they were ignored for the analysis.
% nang returns the number of pixels analyzed for each of the angular bins.
% phi is a dip_image in which the pixel value is the orientation assigned to the corresponding pixel (in radians).
%
% Rodrigo Fernandez-Gonzalez
% 20110906
% 20120225 -> added option to use a different image to assign pixel orientations and mask background pixels.
% 20120305 -> return number of pixels analyzed.
% 20120320 -> compute orientation on thresholded image IN ALL CASES in which background_fraction is specified.
% 20120521 -> added sk as a parameter.

% Test image.
%  a = newim(640, 640);
%  %a(0:2, 0:639) = 255;
%  a(128:130, 0:639) = 255;
%  a(256:258, 0:639) = 255; 
%  a(384:386, 0:639) = 255;
%  a(512:514, 0:639) = 255;
%  a(0:128, 200:202) = 255;                            
%  a(128:256, 350:352) = 255; 
%  a(256:384, 150:152) = 255;  
%  a(384:512, 512:514) = 255;
%  a(512:639, 350:352) = 255;
%  I = clip(noise(smooth(a, 15)));
%
% Test image.
%  a = newim(640, 640);
%  a(200:400, 200:202) = 255;
%  a(200:400, 398:400) = 255; 
%  a(200:202, 200:400) = 255;
%  a(398:400, 200:400) = 255;
%  I = clip(noise(a));

function [Aw, angle, crappy_pixels, nang, phi] = orientation2(I, bin_sz, measurement_type, sigma, s, background_fraction, sk, deviation_from_neighbors, threshold_param, an_roi)
if nargin < 10 || isempty(an_roi) 
    an_roi = dip_image(ones(size(double(I))));
end

if nargin < 9 || isempty(threshold_param)
	threshold_param = 15; % 5 is a good value for 40X images of cell outlines. 100-500 is better for fixed images with higher magnification.
end
	
if nargin < 8
	deviation_from_neighbors = pi/18;
end	

if nargin < 7
    	sk = round(s/2);
end

if nargin < 6
        background_fraction = 0; % No thresholding.
end

if nargin < 4
        sigma = 3.0; % From Karlon, Hsu, Li, Chien, McCulloch and Omens.
end

if nargin < 5
        s = round(sigma * sqrt(-2*log10(sigma)-log10(0.005))); % Bergholms solution so that the values in the periphery of the gradient kernels are >= 0.1. "radius" of the window where the gradient is computed. 3 or 4 is a good value for this parameter.
end

        
if nargin < 3
        measurement_type = 'gradient'; % 'intensity' or 'gradient' will be used to sum the contribution of each pixel.
end        

if nargin < 2
        bin_sz = 1;
end                
        
if nargin < 1
        error('ORIENTATION2 error: no input image provided.');
end

% Initialize output values.
Aw = [];
angle = [];
crappy_pixels = [];
nang = [];
phi = [];
                
% Do nothing on empty images.                
if isempty(I) | nnz(double(I)) == 0
	return;
end	
                
I = squeeze(I); % Remove extra dimensions incompatible with the function convolve.
ws = size(I); % Window size covers the entire image. We are not really using windows here (these remains from orientation.m), so this parameter is gone from the argument list.
                
crappy_pixels = []; % Will contain the background image ... if background calculation was selected.
I_original = []; % This will contain the original image in case a second one is provided to calculate pixel orientation and background.

% Try to remove cytoplasmic pixels, where the intensity should be low. This will not affect signals with strong medial components.
I2 = stretch(I); % Stretch the image (to stretch the histogram, I2 will not be used outside this if block).

if isstr(background_fraction) && strcmp(background_fraction, 'isodata') % Isodata threshold. This used to be background_fraction == inf.
	[out, iright] = threshold(I2, 'isodata'); % This is a dip_image function, does not need to subract 1 from the threshold function.
	crappy_pixels = I2 < iright; % Select pixels where the intensity is below the threshold .
	I_original = I;
	I = stretch(~ crappy_pixels); % Use the non-background pixels to calculate orientations.
elseif isstr(background_fraction) && strcmp(background_fraction, 'adaptive') % Adaptive threshold.
	good_pixels = dip_image(radaptivethreshold(double(I2), s*2+1, 0)); % THE SIZE OF THE KERNEL USED FOR THE ADAPTIVE THRESHOLD NEEDS TO CHANGE WITH CELL SIZE IN THE IMAGE.
	good_pixels_labeled = label(good_pixels, 2, threshold_param);
	crappy_pixels = (good_pixels_labeled < 1) | (~ an_roi);
	I_original = I;
	I = stretch(~ crappy_pixels);
elseif isstr(background_fraction) && strcmp(background_fraction, 'themode') % Threshold at the mode level.
       	crappy_pixels = newim(size(I))<-1;
        
	I = I.*(I>=rmodenz(I));
        I2 = stretch(I);

elseif isnumeric(background_fraction) && background_fraction > 0.0 && background_fraction <= 1.0
	[fwhm, imax, ileft, iright] = rfwhm(diphist(I2, [0 255]), background_fraction); % Calculate the value of the peak (iright).
	iright = iright - 1; %(-1 because rfwhm works with Matlab indexing -starting from 1- not with pixel values -starting at zero-)
	crappy_pixels = I2 < iright; % Select pixels where the intensity is below the threshold .

	%crappy_pixels = fillholes(crappy_pixels);

	I_original = I;
	I = stretch(~ crappy_pixels);
elseif isnumeric(background_fraction) && background_fraction > 1
	crappy_pixels = I < background_fraction; % I, not I2, because if the user provides a number, it is probably based on visual inspection of I.
	I_original = I;
	I = stretch(~ crappy_pixels);
elseif isnumeric(background_fraction) && background_fraction < -1 % Use the entire image after applying the threshold abs(background_fraction).
	crappy_pixels = newim(size(I))<-1;
	I = I.*(I>=abs(background_fraction));
        I2 = stretch(I);
elseif isa(background_fraction, 'dip_image') && max(background_fraction) == 1% A binary mask.
	crappy_pixels = background_fraction | (~ an_roi);
	I_original = I;
	I = stretch(~ crappy_pixels);
elseif isa(background_fraction, 'dip_image') && max(background_fraction) == 0% A black mask (all pixels are non crappy).
	crappy_pixels = background_fraction  | (~ an_roi);
elseif isa(background_fraction, 'dip_image') % Another image. This will be used to calculate orientation and pixels to exclude.
	I_original = I;
	I = squeeze(background_fraction);
	I2 = stretch(I);
	[out, iright] = threshold(I2, 'isodata'); % This is a dip_image function, does not need to subtract 1 from the threshold function.
	crappy_pixels = (I2 < iright)  | (~ an_roi); % Select pixels where the intensity is below the threshold or outside the roi.

	I = stretch(~ crappy_pixels);

else % background fraction == 0
	crappy_pixels = newim(size(I))<-1; % <-1 makes crappy_pixels a binary image with all pixels set to zero (i.e. all pixels are valid).
end

%dipshow(crappy_pixels);
                
sigmasq = power(sigma, 2);

% Kernels to calculate gradient images (in the end this is just a differential scheme to estimate the image gradient) ...
[x y] = meshgrid(-s:s, -s:s);
hx = 2 .* x .* exp(- (x.*x + y.*y) ./ sigmasq) ./ sigmasq;
hy = 2 .* y .* exp(- (x.*x + y.*y) ./ sigmasq) ./ sigmasq;

% ... similar to gradientvector in dipimage, only here I have control over the kernel size.
Gx = -convolve(I, hx); % The minus sign is necessary: convolve seems to return -1 .* result (?).
Gy = -convolve(I, hy);
Gabs = sqrt(Gx.*Gx + Gy.*Gy);

% WARNING: work always with radians, all Matlab functions (sin, cos, atan, ...) assume that you provide radian units.
% Important to convert to double here. With the dip_image data type, Inf/0 = 0, and 0/0 = 0. With doubles, inf/0 = inf -> atan(inf) = pi/2.
phi = atan(double(Gy)./double(Gx)); %The direction of the gradient is towards the greatest intensity change, perpendicular to the edge in question. Thus, we will need to find the orthogonal vectors a bit later on.
ind_undeterminedphi = find(isnan(phi)); % These are the 0/0 cases, for which the tangent is undefined. The orientation in these pixels will be interpolated later from the values of the surrounding pixels.
phi = dip_image(phi); % Convert into an image again.

% Convert all angles to the range between 0 and pi.
ind = find((Gx < 0) & (Gy > 0));
phi(ind) = pi + phi(ind);
ind = find((Gx > 0) & (Gy < 0));
phi(ind) = pi + phi(ind);

% $$$%Plot gradient over image.
% $$$           dipshow(I); hold on;
% $$$           rx = 10; ry = 10; scale = 0.035; % x and y resolutions (in pixels) for the points to choose for arrow plotting.
% $$$   	   X = []; Y = []; U = []; V = [];
% $$$   	   
% $$$   	   for ii = 1:rx:size(I, 1)
% $$$   		for jj = 1:ry:size(I, 2)
% $$$   			if  ~ double(crappy_pixels(ii-1, jj-1))
% $$$   				X(end+1) = ii-1;
% $$$   				Y(end+1) = jj-1;
% $$$   				U(end+1) = scale.*floor(double(Gx(ii-1, jj-1)));
% $$$   				V(end+1) = scale.*floor(double(Gy(ii-1, jj-1)));
% $$$   			end
% $$$   		end
% $$$   	   end
% $$$ 
% $$$    	   h = quiver(X, Y, U, V, 0);
% $$$  	   set(h, 'Color', 'r', 'LineWidth', 2);
% $$$ %  %          for ii = 1:rx:size(I, 1)
% $$$ %  %                  for jj = 1:ry:size(I, 2)
% $$$ %  %                  if ~ double(crappy_pixels(ii-1, jj-1))
% $$$ %  %                          h = line([ii-1 ii-1+scale.*floor(double(Gx(ii-1, jj-1)))], [jj-1 jj-1+scale.*floor(double(Gy(ii-1, jj-1)))], 'color', 'r', 'LineWidth', 2);
% $$$ %  %                          h = line([ii-1 ii-1], [jj-1 jj-1], 'color', 'r', 'Marker', 'o', 'LineWidth', 2);
% $$$ %  %                          drawnow;
% $$$ %  %                  end
% $$$ %  %                  end
% $$$ %  %          end


% Find orthogonal vectors (everything is rotated 90 degrees counter-clockwise) and keep everything between 0 and pi.
phi = mod((phi+pi/2), pi);
% Find pixels with a certain orientation and plot their position on the angle and magnitude images.
%  ang = 30;
%  double_phi = double(phi).*180./pi;
%  [i j] = find(double_phi >= ang & double_phi < (ang + bin_sz));
%  dipshow(phi); hold on; plot(j-1, i-1, 'ro', 'MarkerFaceColor', 'r', 'MarkerSize', 4);
%  dipshow(Gabs); hold on; plot(j-1, i-1, 'ro', 'MarkerFaceColor', 'r', 'MarkerSize', 4);


% Now, all angles should be between zero and 90 degreees (fold histogram symmetrically).
ind  = find(phi > (pi/2));
phi(ind) = pi - phi(ind);

% ... except in the case of undetermined pixels. which we make very small so that they are very different from their neighbors and are processed in the next block of code.
phi = double(phi); % Convert to an array first, as the indeces were found working on an array, not a dip_image.
phi(ind_undeterminedphi) = -10.0;
phi = dip_image(phi);

% background_fraction can be a negative number between 0 and 1. In that case, the pixels where the gradient magnitude is smaller than abs(background_fraction)*max(Gabs) are not used.
if background_fraction < 0.0 & background_fraction >= -1.0
	ind_lowgradmag = find(Gabs < (abs(background_fraction)*max(Gabs)));
	phi(ind_lowgradmag) = -1.0; % This also takes care of the pixelss in ind_undeterminedphi (the magnitude of the gradient is zero in those pixels).
else
	% One row of pixels at the center of cell interfaces, where intensity is maximum, will have a gradient that points either towards this same point, or along the interface, rather than perpendicular to it, which is the orientation in the pixels on the periphery of the edge. This is not a problem when cell interfaces are thick, but when they are thin, as in movies, you will be assigning intensities to the wrong bins.
	%  out = threshold(stretch(Gabs), 'isodata');
	%  ind_mag = find(~ out);
	kernel = ones(2*sk+1, 2*sk+1);
	kernel(sk+1, sk+1) = 0; % This is a kernel where all pixels are set to one except for the central pixel, set to zero.

	% For every good pixel calculate the average orientation of the pixels around.
	% Find number of no background neighbors for each pixel to compute the mean angle of the neighbors.
	ngoodneighbors = convolve(~crappy_pixels, kernel);

	% To calculate the mean angle around, we set to zero the angle in invalid pixels.
	nonegphi = phi; nonegphi(crappy_pixels) = 0;
	mean_angle_around = convolve(nonegphi, kernel)./ngoodneighbors;

	% We finally set the angle around background pixels to be the same angle that we had computed earlier so that these pixels are not processed any further.
	mean_angle_around(crappy_pixels) = phi(crappy_pixels);


	%ind = find(abs(mean_angle_around-phi) >= deviation_from_neighbors); % Find pixels whose orientation is very different from that of the pixels around.
        ind_shouldbebright = find(mean_angle_around-phi >= deviation_from_neighbors); % Find pixels whose orientation is significantly lower than that of the pixels around.
        ind_shouldbedim = find(phi-mean_angle_around >= deviation_from_neighbors); % Find pixels whose orientation is significantly greater than that of the pixels around.
        

	%ind = intersect(ind_mag, ind_ang);
	% Display the position of pixels with wrong orientations.
	%  [i j] = find(double(abs(mean_angle_around-phi)) >= deviation_from_neighbors);
	%  dipshow(phi); hold on; plot(j-1, i-1, 'ro', 'MarkerFaceColor', 'r', 'MarkerSize', 4);
	%  dipshow(Gabs); hold on; plot(j-1, i-1, 'ro', 'MarkerFaceColor', 'r', 'MarkerSize', 4);
	%  dipshow(I); hold on; plot(j-1, i-1, 'ro', 'MarkerFaceColor', 'r', 'MarkerSize', 4);
	%phi(ind) = mean_angle_around(ind); % For those pixels, substitute their angle for the average angle around them. A more elegant way to do this would be to interpolate the gradient value based on the gradients around, but this is much slower:
	%  phi_median = phi;
	%  for ii = 1:numel(ind)
	%          thex = floor(ind(ii) ./ size(phi, 2));
	%          they = mod(ind(ii), size(phi, 2));
	%          minx = max(0, thex-sk);
	%          maxx = min(size(phi, 1)-1, thex+sk);
	%          miny = max(0, they-sk);
	%          maxy = min(size(phi, 2)-1, they+sk);
	%          %[x y] = meshgrid(minx:maxx, miny:maxy);
	%          %phi(ind(ii)) = interp2(x, y, double(phi(minx:maxx, miny:maxy)), thex, they); % Interpolate.
	%          phi_median(ind(ii)) = median(reshape(double(phi(minx:maxx, miny:maxy)), [1 numel(double(phi(minx:maxx, miny:maxy)))])); % Alternatively, assign a percentile of the neighbors (the median is best).
	%  end
	%  phi = phi_median;

	% Median filter of angles around each pixel.
	%phi_median = medif(phi, sk, 'rectangular');
        phi_max = percf(phi, 100, sk, 'rectangular');
        phi_min = percf(phi, 0, sk, 'rectangular');

	% For pixels with an odd orientation, take the median around them. WHY NOT THE MAX????
	%phi(ind) = phi_median(ind);
	% For pixels with an odd orientation, take the mean around them.
	%phi(ind) = mean_angle_around(ind);
        % For pixels that should be brighter, assign the max around.
        phi(ind_shouldbebright) = phi_max(ind_shouldbebright);
        % For pixels that should be dimmer, assign the min around.
        phi(ind_shouldbedim) = phi_min(ind_shouldbedim);

	% Should I also substitute their gradient value? NO!! Then you are selectively changing the value that you use to quantify some pixels.
	%  grad_around = convolve(Gabs, kernel)./((2*sk+1) * (2*sk+1)); % For every pixel calculate the average gradient of the pixels around.
	%  Gabs(ind) = grad_around(ind);

	% Try to remove cytoplasmic pixels, where the intensity should be low. This will not affect signals with strong medial components.
	ind = find(crappy_pixels);
	phi(ind) = -1.0; % Make the angle at these pixels negative so that they are ignored (the loop below will not consider the contribution of pixels with negative orientations).
end


% Restore the original I image if a different one was used to calculate pixel orientation and the position of cytoplasmic pixels.
if ~ isempty(I_original)
	I = I_original;
	Gx = -convolve(I, hx); % The minus sign is necessary: convolve seems to return -1 .* result (?).
	Gy = -convolve(I, hy);
	Gabs = sqrt(Gx.*Gx + Gy.*Gy);
end

AwRodrigo = nan.*zeros(1, ceil(90/bin_sz));% For every possible orientation , get the contribution of those pixels.
double_Gabs = double(Gabs);
double_phi = double(phi) .* 180 ./ pi; % Get image containing angles and convert to degrees from radians.

% For every possible angle (SLOW!!)
double_I = double(I);
nang = [];
%dipshow(phi);
for kk = 0:bin_sz:90
%          if bin_sz >= 1 & kk == 90
%                  continue;
%          end
        
        y = find(double_phi >= kk & double_phi < (kk + bin_sz)); % Find pixels with a certain orientation.
        %x = x-1; % phi is indexed from zero, while double_phi is indexed from 1.
        %y = y-1;
                  
        % Check in case there are any pixels with orientation == 90 degrees.
        if (kk+bin_sz) == 90
                y90 = find(double_phi >= 90.);
                y = cat(1, y, y90);
        end
        
        % Plot pixels with a certain orientation.
        %          anIm = newim(size(I));
        %  double_anIm = double(anIm);
        %  if ~isempty(y) double_anIm(y) = 255; dipshow(double_anIm>0); end
        
        if ~isempty(y)
                numG = sum(double_Gabs(y)); % In the numerator, the summed absolute gradient of the pixels with orientation kk. double_phi(y, x) returns a matrix with the pixel value of all possible combinations of y and x. The diagonals of the matrix contain the values at [y(1) x(1), y(2) x(2), ...].
                numI = sum(double_I(y)); % Or the summed brightness.
                den = numel(y); % Number of pixels found with orientation kk in the denominator.
                
                switch lower(measurement_type)
                        case {'intensity', 'brightness'}                        
                                AwRodrigo(kk/bin_sz+1) = numI./den;
                        case 'gradient'
                                AwRodrigo(kk/bin_sz+1) = numG./den;
                        otherwise
                                error('Wrong measurement type.');
                end
        end

	%fprintf('[%d-%d) -> %d, min: %.2f, max %.2f\n', kk, kk+bin_sz, numel(y), min(double_phi(y)), max(double_phi(y)));

        nang(end+1) = numel(y);

        if (kk+bin_sz >= 90)
        	break;
	end
end

% Plot orientation histogram.
edges = [0:bin_sz:90-bin_sz];
figure; bar(edges, nang./sum(nang), 'histc');
set(gca, 'FontSize', 32, 'FontWeight', 'bold', 'LineWidth', 2, 'XTick', (0:30:90));
xlabel('orientation (degrees)', 'FontSize', 32, 'FontWeight', 'bold');
ylabel('fraction of pixels', 'FontSize', 32, 'FontWeight', 'bold');
xlim([-5 95]); ylim([0 .5]);

Aw = AwRodrigo;
[tmp angle] = max(Aw);

% Plot histogram
%  figure; bar(Aw./min(Aw)); set(gca, 'LineWidth', 2);
%  yl = ylim;
%  set(gca, 'YLim', [0 yl(2)], 'Xlim', [0 ceil(90/bin_sz)+1], 'FontSize', 32, 'FontWeight', 'bold', 'XTick', [.5:1:6.5], 'XTickLabel', {'0'; '15'; '30'; '45'; '60'; '75'; '90'}, 'YTick', [0:1000]);
%  xlabel('orientation (degrees)', 'FontSize', 32, 'FontWeight', 'bold');
%  ylabel('relative brightness', 'FontSize', 32, 'FontWeight', 'bold')
