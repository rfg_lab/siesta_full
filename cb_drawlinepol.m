% cb_drawlinepol(fig, coords_origin, coords_end, brush_sz, color)
function cb_drawlinepol(fig, coords_origin, coords_end, brush_sz, color)
udata = get(fig, 'UserData');
hold on;

udata.lineh = plot([coords_origin(1),coords_end(1)],...
    [coords_origin(2),coords_end(2)],'Color',color, 'LineWidth', brush_sz);

set(udata.lineh, 'XData', [coords_origin(1),coords_end(1)], 'YData', [coords_origin(2),coords_end(2)]);
set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
set(fig, 'UserData', udata);
    
%disp('Inside')
%udata.lineh
%coords_origin
%coords_end
%display_data(fig, img, udata); % Sets udata to be the user data in fig.
%elseif strcmp(get(img,'Type'),'line')
%	dad = get(img, 'Parent');
%	grandpa = get(dad, 'Parent');
%	udata = get(grandpa, 'UserData');
%        udata.lineh = line([coords_origin(1),coords_end(1)],...
%	       [coords_origin(2),coords_end(2)],'EraseMode','normal','Color',[0,0,0.8], 'LineWidth', brush_sz);
%		
%	set(udata.lineh, 'XData', [coords_origin(1),coords_end(1)], 'YData', [coords_origin(2),coords_end(2)]);
%	display_data(fig, img, udata); % Sets udata to be the user data in fig
end