% cb_drawcircle(fig, coords_origin, coords_end, brush_sz, color)
function cb_drawcircle(fig, coords_origin, coords_end, brush_sz, color)
udata = get(fig, 'UserData');

hold on;

[coords_x coords_y udata.lineh] = circle(coords_origin(1), coords_origin(2), norm(coords_end - coords_origin, 2), 256, 1);
set(udata.lineh, 'Color',color, 'LineWidth', brush_sz);

% Print circle radius.
fprintf('\nRadius = %.2f', norm(coords_end - coords_origin, 2));

%set(udata.lineh, 'XData', [coords_origin(1),coords_end(1)], 'YData', [coords_origin(2),coords_end(2)]);
set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
set(fig, 'UserData', udata);
end