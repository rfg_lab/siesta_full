function rv = Ranks(x);
% rv = Ranks(x)
% computes the vector rv of ranks of x with the usual 
% convention of averaging ranks in case of ties.
% 
% Lutz Duembgen, 23.02.1999

n = length(x);
rv = zeros(size(x));

[hv,ar] = sort(x);

a = 1;
for b=2:n
   if hv(b) > hv(a)
      hv(a:b-1) = (a+b-1)/2;
      a = b;
   end;
end;
hv(a:n) = (a+n)/2;
rv(ar) = hv;
