% [pix_vals_r pix_vals_g pix_vals_b gaussparams d] = cb_quantifytrajectoryprofile_constantdist(fig, channel, brush_sz, tp_list)
% gaussparams [mu, sigma, scale]
% d is the distance in pixels between subsequent measurements at each profile. It will vary from profile to profile, as every profile has a different length, yet this function uses a constant number of points. Thus, it is important for profiles to maintain a relatively constant length.
% The distance between the points where edge intensity is measured is APPROXIMATELY 0.25pixels. In cb_quantifytrajectoryprofile a certain number of points is used to measure the intensity of the edge, and therefore, for edges with very different lengths, the resolution of the measurements will be different. However, we still have a problem when a certain edge changes length dramatically over time (the number of measurement points is determined based on the initial length and then maintained over time).
function [pix_vals_r pix_vals_g pix_vals_b gaussparams d] = cb_quantifytrajectoryprofile_constantdist(fig, channel, brush_sz, tp_list, smooth_factor)
udata = get(fig, 'UserData');

MAX_PNTS = 1000; % Max number of measurements.

if ndims(udata.rntrajectories) == 3 % Fix trajectiories if messed up.
    udata.rntrajectories = udata.rntrajectories(:, :, 1);
end

pix_vals_r = nan .* zeros(max(tp_list), MAX_PNTS, max(udata.rntrajectories)); % One row per time point, one column per point measured along the trajectory, one slice per trajectory.
pix_vals_g = nan .* zeros(max(tp_list), MAX_PNTS, max(udata.rntrajectories)); % One row per time point, one column per point measured along the trajectory, one slice per trajectory.
pix_vals_b = nan .* zeros(max(tp_list), MAX_PNTS, max(udata.rntrajectories)); % One row per time point, one column per point measured along the trajectory, one slice per trajectory.

d = nan .* zeros(max(udata.rntrajectories), max(tp_list));

% Find shortest trajectory.
%     nl = inf;
%     for ii = 1:numel(tp_list)
%          thetp = tp_list(ii);
%          trajectories = udata.rtrajectories(:, :, thetp);
%
%          for jj = 1:udata.rntrajectories(thetp)
%                  tr = trajectories{jj};
%
%                  [ang l] = cb_multicolormeasuretrajectory(fig, tr, brush_sz, thetp - 1);
%
%                  if l < nl
%                          nl = l;
%                  end
%          end
%     end

if nargin < 5
    smooth_factor = 10;
end

measure_res = 0.25; % Distance in pixels in between measurements along the trajectory.
min_dist_between_fit_and_data_maxima = 20; % How far can the peak of the data and the Gaussian fit be so that we still consider this an appropriate fit? The units for this magnitude are min_dist_between_fit_and_data_maxima .* measure_res pixels.

% For every time point.
for ii = 1:numel(tp_list)
    thetp = tp_list(ii);
    if (~ isfield(udata, 'colordata')) || isempty(udata.colordata)
        im = squeeze(udata.slices(:, :, thetp-1)); % squeeze is necessary, as get_subpixel will be given 2D pixel coordinates.
    else
        im = squeeze(joinchannels('rgb', udata.slices{1}(:, :, thetp-1), udata.slices{2}(:, :, thetp-1), udata.slices{3}(:, :, thetp-1)));
    end
    
    trajectories = udata.rtrajectories(:, :, thetp);
    
    % For every trajectory in this time point.
    for jj = 1:udata.rntrajectories(thetp)
        tr = trajectories{jj};
        %                  tr = cb_changetrajectorylength(tr, nl);
        tr = tr(1:2, 1:2); % Do not need the Z coordinate. And we will only deal with the first segment in the trajectory.
        
        if ~ isempty(tr)
            points = tr;
            N = size(points,1);
            if N<2
                error('You need to select at least two points.');
            end
            coords = [];
            for kk=1:(N-1)
                len = norm(points(kk+1,:)-points(kk,:)); % Length of the iith segment.
                if ii == 1 % Compute the number of points in the first time point, and then assume the same number for all other time points. This assumes that the length of the drawn trajectory remains constant over time.
                    npnts = ceil(len/measure_res) + 1;
                end
                k = len;
                inc = k / (npnts-1); % inc will be very close to 0.25.
                d(jj, thetp) = inc;
                factors = (0:inc:k);
                factors = factors(1:npnts);
                tmp = repmat(points(kk,:),[npnts,1])+factors'*((points(kk+1,:)-points(kk,:))/len);
                %tmp = repmat(points(kk,:),[k,1])+(0:k-1)'*((points(kk+1,:)-points(kk,:))/len); %  ((points(ii+1,:)-points(ii,:))/len) is a unit vector pointing from the current point to the next one in the trajectory.
                % (0:k-1)'*((points(ii+1,:)-points(ii,:))/len) are vectors from point ii to point ii+1 with lengths from 0 to k-1. By adding each one of these vectors to the coordinates of point ii we get a series of equidistant points along the segment that connects points ii and ii+1. The equidistant points are 1 pixel apart.
                coords = [coords;tmp];
            end
            
            if (~ isfield(udata, 'colordata')) || isempty(udata.colordata)
                pix_vals_r(thetp, 1:npnts, jj) = get_subpixel(im,coords,'cubic');
                pix_vals_g(thetp, :, jj) = pix_vals_r(thetp, :, jj);
                pix_vals_b(thetp, :, jj) = pix_vals_r(thetp, :, jj);
                % output = dip_image(get_subpixel(im,coords,'cubic'));
            else
                pix_vals_r(thetp, 1:npnts, jj) = get_subpixel(im{1},coords,'cubic');
                pix_vals_g(thetp, 1:npnts, jj) = get_subpixel(im{2},coords,'cubic');
                pix_vals_b(thetp, 1:npnts, jj) = get_subpixel(im{3},coords,'cubic');
                
            end
            
        end
    end
end

pix_vals_r = pix_vals_r(:, 1:npnts);

ref = mean(pix_vals_r(1:2, :)); % Compute the average intensity at every subpixel coordinate in the first two time points.
normpix = pix_vals_r ./ repmat(ref, [size(pix_vals_r, 1) 1]); % And normalize all other intensities to those averages.

for ii = 1:size(normpix, 1)
    thepix = normpix(ii, find(~isnan(normpix(ii, :))));
    if isempty(thepix) || nnz(thepix) == 0
        gaussparams(ii, :) = [nan nan nan];
        continue;
    end
    
    yvals = 1. - double(smooth(dip_image(thepix), smooth_factor)); % mountain-like gaussian.
    
    if ii <= 3 % Find estimates for parameters from intensity values in the time point immediately after bleaching.
        [scaleestim muestim] = max(yvals);
    else % For all other time points, use the estimated values for the postbleach time point.
        muestim = gaussparams(3, 1);
        scaleestim = gaussparams(3, 3);
    end
    
    [beta] = nlinfit((1:numel(yvals)), yvals, @gaussianfn2, [muestim 1 scaleestim]);
    
    % For bleached regions (i>2), make sure that the value found for mu (beta(1)) is not terribily off from that initially estimated (muestim); and that the intensity curve has a clear minimum between two higher "plateaus".
    if ii > 2 && (abs(beta(1)-muestim) < min_dist_between_fit_and_data_maxima && abs(abs(mean(yvals(1:5)) - mean(yvals((end-4):end))) - max(abs(beta(3)-mean(yvals(1:5))), abs(beta(3)-mean(yvals((end-4):end))))) > 0.05)
        gaussparams(ii, :) = beta;
    elseif ii > 2 && (muestim-5) > 0 && (muestim+5) < numel(yvals)% if the previous fit is not good, take a line (perhaps we should try to fit a polynomial or a gaussian sum here, taking only the gaussian of closest mean to muestim).
        %gaussparams(ii, :) = nan .* beta;
        beta(1) = muestim; % mu
        beta(3) = mean(yvals((round(muestim)-5):(round(muestim)+5)));%mean(yvals); % scale.
        beta(2) = inf; %sigma.
        gaussparams(ii, :) = beta;
    else
        gaussparams(ii, :) = nan .* beta;
    end
end
end