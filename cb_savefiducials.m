% *********************************************************************************
function cb_savefiducials(fidu, nfidu, extensions)

[filename, pathname, ext] = uiputfile(extensions, 'Save fiducial markers');
if (~ isequal(filename, 0))
	[tmp1 tmp2 theext] = fileparts(filename);
	theext = cat(2, '*', theext);
	
	% If the extension is written, use that mode.
	if (~ isempty(theext))
		for ie = 1:numel(extensions)
			if strcmp(extensions{ie}, theext)
				ext = ie;
				break;
			end
		end
	end
	

	if (strcmp(extensions{ext}, '*.csv'))
		% nfidu is not saved so that fidu can be
		% easily read with dlmread.
		dlmwrite(cat(2, pathname, filename), fidu);
	else
		save(cat(2, pathname, filename), 'fidu', 'nfidu');
	end
end
end