% *********************************************************************************
function udata = cb_loadmetamorphunique(fig)

udata = get(fig, 'UserData');
curr_dir = pwd;
cd(udata.rcurrdir);

[filename, pathname, ext] = uigetfile('*.rgn', 'Load Metamorph region as unique marker');
if (~ isequal(filename, 0))
    try
        f = fopen(cat(2, pathname, filename));
        rgn = fgetl(f);
        fclose(f);
    catch
        fprintf('\n%s%s is not a Metamorph region file or the was an error handling the file.', pathname, filename);
        cd(curr_dir);
        return;
    end
    
    % The format for line regions under Metamorph is
    % ..., 6 2 x1 y1 x2 y2, ...
    % Six denotes the sixth field in the rgn file. 2 is the number of points after.
    % We are searching for ', 6'.
    ind = strfind(rgn, ', 6');
    if ~ isempty(ind)
        rgn = rgn(ind(1)+4:end);
        ind = strfind(rgn, ' ');
        
        npoints = str2num(rgn(1:ind(1)));
        
        x = [];
        y = [];
        
        for ii = 1:4:npoints
            x(end+1) = str2num(rgn(ind(ii): ...
                ind(ii+1)));
            x(end+1) = str2num(rgn(ind(ii+2): ...
                ind(ii+3)));
            y(end+1) = str2num(rgn(ind(ii+1): ...
                ind(ii+2)));
            y(end+1) = str2num(rgn(ind(ii+3): ...
                ind(ii+4)));
            
        end
        
        unique_x = uint16(round(mean(x)));
        unique_y = uint16(round(mean(y)));
        
        disp([unique_x unique_y]);
        
        udata.runiquecoords(udata.curslice+1, ...
            :) = [unique_x, unique_y];
        
        
    else
        fprintf('\n%s%s is not a Metamorph region file.', pathname, filename);
        cd(curr_dir);
        return;
    end
end

cd(curr_dir);
end