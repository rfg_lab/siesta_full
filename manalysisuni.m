% [mvalues, u, l, dt, populations_sims, mvalues_sims, mipr, mipr_sims] = manalysisuni(positions, populations, p, r, neigh_graph, weighted, alpha, ns, vis, good_positions, shells)
% [mvalues, u, l, dt, populations_sims, mvalues_sims, mipr, mipr_sims] = manalysisuni(positions, populations, p, r, graph_type, weighted, alpha, ns, vis, good_positions, shells)
% [mvalues, u, l, dt, populations_sims, mvalues_sims, mipr, mipr_sims] = manalysisuni(positions, populations, p, r, dt, alpha, ns, vis, good_positions, shells)
%
% Univariate M function analysis of the distribution of population P.
% Based on Fernandez-Gonzalez et al., "A tool for the quantitative 
% spatial analysis of complex cellular systems", IEEE Transactions in 
% Image Processing 14(9):1300-1313, 2005.
% 
% POSITIONS is a matrix where each row represents the coordinates of a point.
% POPULATIONS is a vector where index i indicates the population of position i.
% P is the population of interest.
% R is the maximum distance/s to consider two positions as neighbors.
% NEIGH_GRAPH (optional) is a matrix where (i, j) = 1 indicates that positions 
% i and j are immediate neighbors.
% GRAPH_TYPE (optional) is a string that determines the type of neighborhood 
% graph to be built if neigh_graph is not given: 'euclidean', 'delaunay', 
% 'rng', 'refrng', 'knn'.
% WEIGHTED (optional) determines whether to use number of edges (0) or edge 
% lengths to measure distances.
% DT is the table of minimum distances among any two positions.
% ALPHA is the desired confidence level for the measurements
% VIS (optional) is 1 to display the visualization of the results, 0 not to
% do it.
% GOOD_POSITIONS is a vector with ones for POSITIONS that can be 
% assigned to P in the simulations, and zeros for those where this is not possible.
% SHELLS is zero for 'within' (<=r) analysis and non-zero for 'at'
% (==r) analysis.
%
% MVALUES is a list of M values that show the distribution of positions 
% belonging to P within the range of distances R.
% U is a list of the minimum M values for significant clustering at a
% certain distance R and with significance level ALPHA.
% L is a list of the maximum M values for significant regularity at a
% certain distance R and with significance level ALPHA.
% DT is a distance table where dt(i, j) indicates the distance between
% positions i and j.
% POPULATIONS_SIMS is a matrix where index i in row j indicates the population 
% of position i in simulation j.
% MVALUES_SIMS is a matrix where index i in row j indicates the M value at
% distance i for simulation j.
% MIPR is a list of M values per position belonging to P.
% MIPR_SIMS is a matrix of M values per position, so that (i, j, k) contains the M
% value for the ith position, at the jth distance in the kth simulation.
%
% Rodrigo Fernandez-Gonzalez
% fernanr1@mskcc.org
% 12/18/2006

function varargout = manalysisuni(varargin)

% Default values for some parameters. This will be modified in a second
% if provided.
r = (0:5);
graph_type = 'euclidean';
weighted = 0;
alpha = 0.05;
ns = 200;
vis = 1;
shells = 0;

small = 2e-9;

% Check parameters and generate neighborhood graph (if not provided)
% and table of distances between positions.
switch (nargin)
	case 10
		positions = varargin{1, 1};
		if (~ (isnumeric(positions) && prod(size(positions)) > 1 && size(positions, 3) == 1))
			error('??? First argument should be a 2D-matrix.');
		end

		populations = varargin{1, 2};
		if (~ ((isnumeric(populations) || islogical(populations)) && prod(size(populations)) > 1 && size(populations, 3) == 1))
			error('??? Second argument should be a 2D-matrix.');
		end
		
		p = varargin{1, 3};
		if (~ (isnumeric(p) && prod(size(p)) == 1))
			error('??? Third argument should be a number.');
		end
		
		r = varargin{1, 4};
		if (~ isnumeric(r))
			error('??? Third argument should be numeric.');
		end

		% Reorder input parameters.
		dt = varargin{1, 5};
			
		alpha = varargin{1, 6};
		if (~ (isnumeric(alpha) && prod(size(alpha)) == 1))
			error('??? Seventh argument should be a number.');
		end

		ns = varargin{1, 7};
		if (~ (isnumeric(ns) && prod(size(ns)) == 1))
			error('??? Eighth argument should be a number.');
		end

		vis = varargin{1, 8};			
		if (~ (isnumeric(vis) && prod(size(vis)) == 1))
			error('??? Nineth argument should be a number.');
		end
			
		good_positions = varargin{1, 9};
		if (~ ((isnumeric(good_positions) || islogical(good_positions))&& prod(size(good_positions)) > 1 && size(good_positions, 3) == 1))
			error('??? Nineth argument should be a 2D-matrix.');
		end
		

		shells = varargin{1, 10};
		if (~ (isnumeric(shells) && prod(size(shells)) == 1))
			error('??? Tenth argument should be a number.');
		end
			
	case 11
		positions = varargin{1, 1};
		if (~ (isnumeric(positions) && prod(size(positions)) > 1 && size(positions, 3) == 1))
			error('??? First argument should be a 2D-matrix.');
		end

		populations = varargin{1, 2};
		if (~ ((isnumeric(populations) || islogical(populations)) && prod(size(populations)) > 1 && size(populations, 3) == 1))
			error('??? Second argument should be a 2D-matrix.');
		end
		
		p = varargin{1, 3};
		if (~ (isnumeric(p) && prod(size(p)) == 1))
			error('??? Third argument should be a number.');
		end

		r = varargin{1, 4};
		if (~ isnumeric(r))
			error('??? Third argument should be numeric.');
		end
		
		weighted = varargin{1, 6};
		if (~ (isnumeric(weighted) && prod(size(weighted)) == 1))
			error('??? Sixth argument should be a number.');
		end

		if (isstr(varargin{1, 5}))
			graph_type = varargin{1, 5};
			[dt, neigh_graph] = mdistancetable(positions, graph_type, weighted);
		elseif (isnumeric(varargin{1, 5}) && prod(size(varargin{1, 5})) > 1)
			neigh_graph = varargin{1, 5};
			dt = mdistancetable(positions, neigh_graph, weighted);
		else
			error('??? Fifth argument should be a matrix or a string.');
		end

		alpha = varargin{1, 7};
		if (~ (isnumeric(alpha) && prod(size(alpha)) == 1))
			error('??? Seventh argument should be a number.');
		end

		ns = varargin{1, 8};
		if (~ (isnumeric(ns) && prod(size(ns)) == 1))
			error('??? Eighth argument should be a number.');
		end

		vis = varargin{1, 9};
		if (~ (isnumeric(vis) && prod(size(vis)) == 1))
			error('??? Nineth argument should be a number.');
		end

		good_positions = varargin{1, 10};
		if (~ ((isnumeric(good_positions) || islogical(good_positions)) && prod(size(good_positions)) > 1 && size(good_positions, 3) == 1))
			error('??? Tenth argument should be a 2D-matrix.');
		end
		
		shells = varargin{1, 11};
		if (~ (isnumeric(shells) && prod(size(shells)) == 1))
			error('??? Eleventh argument should be a number.');
		end



	otherwise
		error('??? Wrong number of input arguments.');			
end

% If there are no positions belonging to p.
if (sum(populations == p) == 0)
	varargout{1, 1} = zeros(1, numel(r));
	varargout{1, 2} = zeros(1, numel(r));
	varargout{1, 3} = zeros(1, numel(r));
	varargout{1, 4} = dt;
	varargout{1, 5} = zeros(ns, size(positions, 1));
	varargout{1, 6} = zeros(ns, numel(r));
	varargout{1, 7} = [];
	varargout{1, 8} = [];
	
	return;

end


% Compute M function for data.
[varargout{1, 1}, varargout{1, 7}] = muniv(positions, populations, p, r, dt, shells);


if (ns > 0)
	% Run simulations.
	[mvalues_sims, populations_sims, mipr_sims] = msims(ns, positions, p, sum(populations == p), r, dt, good_positions, shells);

	if isempty(mvalues_sims)
		varargout{1, 2} = [];
		varargout{1, 3} = [];

		varargout{1, 4} = dt;
		varargout{1, 5} = [];
		varargout{1, 6} = [];
		varargout{1, 8} = [];
		
		return;
	end
	
	% Number of simulation outliers (since alpha = (1 + outliers) / (1 + ns)
	outliers = floor(alpha * (1 + ns) - 1);

	% Order simulation values and discard outliers in order to obtain the maximum
	% and minimum M values randomly attainable at each distance.
	mvalues_ordered = sort(mvalues_sims);
	varargout{1, 2} = mvalues_ordered(ns - outliers, :);
	varargout{1, 3} = mvalues_ordered(outliers + 1, :);

	varargout{1, 4} = dt;
	varargout{1, 5} = populations_sims;
	varargout{1, 6} = mvalues_sims;
	varargout{1, 8} = mipr_sims;
else
	varargout{1, 2} = [];
	varargout{1, 3} = [];

	varargout{1, 4} = dt;
	varargout{1, 5} = [];
	varargout{1, 6} = [];
	varargout{1, 8} = [];

end


return;
