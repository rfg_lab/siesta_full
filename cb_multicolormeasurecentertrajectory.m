% [ang lngth int npix] = cb_multicolormeasurecentertrajectory(fig, tr, brush_sz, slice_index, nl, intensity_fn)
% Like multicolormeasuretrajectory but using a trajectory that spans from -nl/2 to +nl/2 from the center of
% tr. If nl is larger than the trajectory's length, the entire trajectory is measured.

function [ang lngth int npix] = cb_multicolormeasurecentertrajectory(fig, tr, brush_sz, slice_index, nl, intensity_fn)

if nargin < 6
    intensity_fn = @mean; %Other potential values are @sum, @max, @median, ...
end
[ang lngth int npix] = cb_multicolormeasuretrajectory(fig, tr, brush_sz, slice_index, intensity_fn);
end