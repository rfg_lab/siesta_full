function varargout = dialog_snakes(varargin)
%DIALOG_SNAKES M-file for dialog_snakes.fig
%      dialog_snakes([first_avail_slice last_avail_slice current_slice_index], snakes_kappax, snakes_kappay, snakes_beta, snakes_win_sz, sigma_gradient, snakes_areaTol)

% Edit the above text to modify the response to help dialog_snakes

% Last Modified by GUIDE v2.5 02-Apr-2014 11:52:40

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @dialog_snakes_OpeningFcn, ...
                   'gui_OutputFcn',  @dialog_snakes_OutputFcn, ...
                   'gui_LayoutFcn',  [], ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
   gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before dialog_snakes is made visible.
function dialog_snakes_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)

% Choose default command line output for dialog_snakes
handles.output = hObject;
% Parameter values for components. TERESA EDITED HERE.
% THIS HAS TO BE BEFORE guidata(hObject, handles)!!!!
set(handles.figure1, 'name', 'MEDUSA');
warning('off', 'all'); 
timepoints = sort(varargin{1}(1:2));
tps = timepoints(1):timepoints(2);
handles.tps = tps;
handles.closemethod = -1;
str = [];

for i = 1:numel(tps)
	str = cat(2, str, cat(2, num2str(tps(i)), '|'));
end

str = str(1:(end-1));
set(handles.popupmenu1, 'String', str, 'Value', varargin{1}(3));
set(handles.popupmenu2, 'String', str, 'Value', varargin{1}(3));

set(handles.edit1, 'String', sprintf('%.2f', varargin{2}));
set(handles.edit2, 'String', sprintf('%.2f', varargin{3}));
set(handles.edit3, 'String', sprintf('%.2f', varargin{4}));
set(handles.edit4, 'String', sprintf('%d', varargin{5}));
set(handles.edit5, 'String', sprintf('%.2f', varargin{6}));
set(handles.edit6, 'String', sprintf('%.2f', varargin{7}));
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes dialog_snakes wait for user response (see UIRESUME)
 uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = dialog_snakes_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
d = guidata(handles.figure1);

if d.closemethod == 1
	tp(1) = handles.tps(get(handles.popupmenu1, 'Value'));
	tp(2) = handles.tps(get(handles.popupmenu2, 'Value'));
	%tp = sort(tp);
    kappax = (str2double(get(handles.edit1, 'String')));
    kappay= (str2double(get(handles.edit2, 'String')));
    beta= (str2double(get(handles.edit3, 'String')));
    win_size= round(str2double(get(handles.edit4, 'String')));
    sigma_grad= (str2double(get(handles.edit5, 'String')));
    Area_tol= (str2double(get(handles.edit6, 'String')));
else
    tp(1) = -1;
	tp(2) = -1;
    kappax = -1;
    kappay= -1;
    beta= -1;
    win_size= -1;
    sigma_grad =-1;
    Area_tol= -1;
	
end

close(handles.figure1);
	
varargout{1} = tp(1);
varargout{2} = tp(2);
varargout{3} = kappax;
varargout{4} = kappay;
varargout{5} = beta;
varargout{6} = win_size;
varargout{7} = sigma_grad;
varargout{8} = Area_tol;

% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
selected = get(handles.popupmenu1,'Value');
prev_str = get(handles.popupmenu1, 'String');
set(handles.popupmenu1, 'String', prev_str, 'Value', selected);
% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2
selected = get(handles.popupmenu2,'Value');
prev_str = get(handles.popupmenu2, 'String');
set(handles.popupmenu2, 'String', prev_str, 'Value', selected);

% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.closemethod = 1;
guidata(handles.figure1, handles);
uiresume(handles.figure1);

% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.closemethod = 0;
guidata(handles.figure1, handles);
uiresume(handles.figure1);


function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double
if isempty(eventdata) % return pressed or lost focus
    if isnan(str2double(get(hObject,'String'))) % str2double returns nan if there is a problem with the conversion.
        set(hObject, 'String', num2str(0.4));
    end
end

% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double
if isempty(eventdata) % return pressed or lost focus
    if isnan(str2double(get(hObject,'String'))) % str2double returns nan if there is a problem with the conversion.
        set(hObject, 'String', num2str(0.4));
    end
end

% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double
if isempty(eventdata) % return pressed or lost focus
    if isnan(str2double(get(hObject,'String'))) % str2double returns nan if there is a problem with the conversion.
        set(hObject, 'String', num2str(5));
    end
end

% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double
if isempty(eventdata) % return pressed or lost focus
    if isnan(str2double(get(hObject,'String'))) % str2double returns nan if there is a problem with the conversion.
        set(hObject, 'String', num2str(100));
    end
end

% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double
if isempty(eventdata) % return pressed or lost focus
    if isnan(str2double(get(hObject,'String'))) % str2double returns nan if there is a problem with the conversion.
        set(hObject, 'String', num2str(20));
    end
end

% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double
if isempty(eventdata) % return pressed or lost focus
    if isnan(str2double(get(hObject,'String'))) % str2double returns nan if there is a problem with the conversion.
        set(hObject, 'String', num2str(25));
    end
end

% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
