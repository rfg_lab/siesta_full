% *********************************************************************************
% theshift is a three-element vector containing the net shift for each coordinate (generally [shx shy 0]).
function [fidu2 nfidu2] = cb_shiftannotationspolyline(fig, fidu, nfidu, inpoly, thepoly, theshift)

ud = get(fig, 'UserData');

polyx = thepoly(:, 1);
polyy = thepoly(:, 2);
curslice = ud.curslice + 1;
%ind = intersect(find(fidu(:, 1, curslice) == get(obj, 'XData')), ...
%               find(fidu(:, 2, curslice) == get(obj, 'YData')));

fidu(:, :, curslice) = cb_consolidatefiducials(fidu(:, :, curslice));

X = fidu(:, 1, curslice);
Y = fidu(:, 2, curslice);

[in on] = inpolygon(Y, X, polyy, polyx);

% If the point is inside (or on) the polygon and you have to delete points inside
% or if the point is outside and those are the points you are supposed to delete.
if inpoly
        ind = find(((in+on) >= 1) & (fidu(:, 1) > -1) & (fidu(:, 2) > -1));
else
        ind = find((in+on) == 0 & fidu(:, 1) ~= -1 & fidu(:, 2) > -1);
end                

fidu(ind, :, curslice) = fidu(ind, :, curslice) + repmat(theshift, [numel(ind) 1]);

fidu2 = fidu;
nfidu2 = nfidu;
end

% *********************************************************************************
