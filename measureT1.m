% m = measureT1(geometry, <roi>, <min_l>)
%
% alpha is the left angle, beta the right one, gamma the top angle and 
% delta the bottom one. X and Y are the coordinates of the central node.
% These are all fields in m.
function m = measureT1(geometry, roi, min_l)

if nargin < 3
	min_l = 3;
end

if nargin < 2
	roi = [];
end

min_ang = 20;

[t1_celllist g t1_node_indeces] = findrosettes2D(geometry, 1, min_l, roi, 4, 4);

n = g.nodes;
e = g.edges;

if isempty(roi)
	roi = [min(n(:, 2)) min(n(:, 1)); max(n(:, 2)), max(n(:, 1))]; 
end

ncm = g.nodecellmap;
ids = unique(ncm(:, 1));

% We cannot set a size for the measurements, as we do not know yet
% how many cells are inside the roi.
t1_ids1 = [];
t1_ids2 = [];
alpha = [];
beta = [];
gamma = [];
delta = [];

% For every T1 ...
for i = 1:numel(t1_node_indeces)
	node = t1_node_indeces(i);
	node_coords = n(node, :);
	
	% Find the four connected nodes.
	thenodes = cat(1, e(find(e(:, 1) == node), 2), e(find(e(:, 2) == node), 1));
	
	if (numel(thenodes) ~= 4)
		disp('T1 process with more that four nodes attached!!!');
		continue;
	end
	
	coords = n(thenodes, :);
	
	[tmp listx] = sort(coords(:, 2));
	[tmp listy] = sort(coords(:, 1));
	
	if listy(listx(1)) < listy(listx(2))
		ul = listx(1);
		ll = listx(2);
	else
		ul = listx(2);
		ll = listx(1);
	end
	
	if listy(listx(3)) < listy(listx(4))
		ur = listx(3);
		lr = listx(4);
	else
		ur = listx(4);
		lr = listx(3);
	end
	
	a = ang(coords(ul, :), n(node, :), coords(ll, :)); 
	b = ang(coords(ur, :), n(node, :), coords(lr, :));
	g = ang(coords(ul, :), n(node, :), coords(ur, :));
	d = ang(coords(ll, :), n(node, :), coords(lr, :));
	
	% If one of the interfaces is quite vertical or horizontal ignore the vertex.
	%THIS BIT NOT NECESSARY, THE NEXT CONDITION TAKES CARE OF THIS ONE AS WELL
	%if mod(ang(coords(ul, :), n(node, :)), 90) < min_ang | mod(ang(coords(ur, :), n(node, :)), 90) < min_ang | mod(ang(coords(ll, :), n(node, :)), 90) < min_ang | mod(ang(coords(lr, :), n(node, :)), 90) < min_ang
	%	fprintf('\nThe 4-way vertex at (%.2d, %.2d) looks like an aligned structure (2).\n', node_coords(2), node_coords(1));
	%	continue;
	%end
	
	% No configurations with a vertical interface.
	if abs(90-ang(coords(ul, :), n(node, :))) < min_ang | abs(90-ang(coords(ur, :), n(node, :))) < min_ang | abs(90-ang(coords(ll, :), n(node, :))) < min_ang | abs(90-ang(coords(lr, :), n(node, :))) < min_ang
		fprintf('\nThe 4-way vertex at (%.2d, %.2d) looks like an aligned structure (3).\n', node_coords(2), node_coords(1));
		continue;
	end
	
	% No configurations with a horizontal interface.
	if mod(ang(coords(ul, :), n(node, :)), 180) < min_ang | mod(ang(coords(ur, :), n(node, :)), 180) < min_ang | mod(ang(coords(ll, :), n(node, :)), 180) < min_ang | mod(ang(coords(lr, :), n(node, :)), 180) < min_ang
		fprintf('\nThe 4-way vertex at (%.2d, %.2d) looks like an aligned structure (4).\n', node_coords(2), node_coords(1));
		%[ang(coords(ul, :), n(node, :)) ang(coords(ur, :), n(node, :)) ang(coords(ll, :), n(node, :)) ang(coords(lr, :), n(node, :))]
		continue;
	end
	
	if abs(180-ang(coords(ul, :), n(node, :))) < min_ang | abs(180-ang(coords(ur, :), n(node, :))) < min_ang | abs(180-ang(coords(ll, :), n(node, :))) < min_ang | abs(180-ang(coords(lr, :), n(node, :))) < min_ang
		fprintf('\nThe 4-way vertex at (%.2d, %.2d) looks like an aligned structure (5).\n', node_coords(2), node_coords(1));
		continue;
	end
	
	t1_ids1(end+1) = n(node, 2);
	t1_ids2(end+1) = n(node, 1);
	alpha(end+1) = a;
	beta(end+1) = b;
	gamma(end+1) = g;
	delta(end+1) = d;
end
 
m = dip_measurement(1:numel(t1_ids1), 'X', t1_ids1, 'Y', t1_ids2, 'Alpha', alpha, 'Beta', beta, 'Gamma', gamma, 'Delta', delta); 

% Computes the angle formed by three points, where p2 is the point at the vertex of the angle. Coordinate for each point are given as [row column] pairs (y, x).
function angle_degrees = ang(p1, p2, p3)

% If only two arguments are given, we compute the angle with respect to the horizontal direction.
if nargin < 3
	p3 = [p2(1) (p2(2)+10)];
end

v1 = p1 - p2;
v2 = p3 - p2;

angle_radians = acos(dot(v1, v2) / (norm(v1, 2) * norm(v2, 2)));
angle_degrees = angle_radians * 180 / pi;
