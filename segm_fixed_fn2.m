function [segm seed_fidu] = segm_fixed_fn2(a, roi, slice_list, channel, res, tiny_cells, min_dist, dil_thresh, C, subs_factor_xy, p2a_th, min_p2a, max_p2a, min_seed_bit_sz, gauss_factor, clean_flag, clean_seeds, weighted, write_flag, time_str, winsize, debug, only_seeds, closing_param, mgradient) %michael

% segm_fixed_fn2(a, roi, slice_list, channel, res, tiny_cells, min_dist, dil_thresh, C, subs_factor_xy, p2a_th, min_p2a, max_p2a, min_seed_bit_sz, gauss_factor, clean_flag, clean_seeds, weighted, write_flag, time_str, winsize, debug, only_seeds, closing_param)
%
%	Segment an image.
%
%	Input parameters (and some values to start messing around):
%
% a - A dip_image object containg the image to segment.
% roi - A binary dip_image set to one in the pixels that should be segmented. Leave empty ([]) to use the entire image.
% slice_list - list of slices from a to be used for the segmentation.
% channel - channel to use for the segmentation. Use 1 for grayscale images.
% res - a three-element vector with the X, Y, Z resolution of the image ([x y z]).
% tiny_cells - set to a number different from zero that indicates the smallest size (in voxels) for very tiny cells in your image, ONLY if there are very tiny cells in your image. Typically, 0.
% min_dist - threshold above which pixels in the distance transform are kept (1.2).
% dil_thresh - how much to dilate the binarized image after adaptive threshold to close breaks (2).
% C - set to zero.
% subs_factor_xy - how much to subsample the image to segment it. Use 2 to take every other pixel, ... Speeds up the algorithm and can improve the results in noisy images. Normally set to 1.
% p2a_th - threshold in the perimeter to area ratio of the identified objects. Objects of normal size but above this threshold are marked for further processing (1.4).
% min_p2a, max_p2a - minimum and maximum perimeter to area ratios for "odd objects". Objects in this range and too large are marked for further processing (1.6 and 2.15, respectively).
% min_seed_bit_sz - minimum size of a seed in a given plane (4).
% gauss_factor - how much to smooth the image before expanding the seeds (0.65).
% clean_flag - set to 1 to remove image background.
% weighted - set to 1 to grow seeds using dip_growregionsweighted, 0 to use rwaterseed (0).
% write_flag - set to 1 to save a dip_image containing the segmentation results.
% time_str - set to ''.
% winsize - window size to calculate the adaptive threshold (as the mean value of the pixels in a window of that size around any given pixel) (30).
% debug - set to 1 to save the values of some internal variables as the algorithm proceeds.
% only_seeds - set to 1 if only the seed positions are needed (but no seed expansion is required). YOu can also provide a three-column matrix where each row indicates the X, Y, Z coordinates of a seed, and those seeds will be expanded directly.
% closing_param - set to 1.
%
%	Output parameters:
% segm - a dip_image containing the segmentation results.
% seed_fidu - a three-column matrix where each row indicates the X, Y, Z coordinates of a seed.

% Some magic numbers still remain ... They have been indicated. Modify at your own leisure ...
debug_mode = 0;
WIN_SZ = 30;
% WIN_SZ = 50;

if nargin >= 21
	WIN_SZ = winsize;
end

if nargin >= 22 && isstr(debug)
	disp('DEBUG mode.');
	debug_mode = 1;
end

if nargin < 23
	only_seeds = 0;
end

if nargin < 24
    closing_param = 1;
end

%michael
if nargin <25
    mgradient = 0;
end
%michael

tic

% This bit just makes sure that every slice is taken if the image is 
% smaller than selected in slices.
if (slice_list(numel(slice_list)) > (size(a, 3) - 1))
	slice_list = (slice_list(1) : (size(a, 3) - 1));
end	

% Parameters should change so that they are magnification/resolution independent.
slices_or = slice_list;
min_seed_size = floor(90 * numel(slices_or) / 14); % MAGIC NUMBER: This threshold (90) was empirically set for an image with 14 slices. Smaller/larger images require a different threshold. Same thing applies below.
if (tiny_cells)
	%min_seed_size = ceil(min_seed_size / 2);
	min_seed_size = tiny_cells; %45;
end
				   
min_elong_seed_size = floor(125 * numel(slices_or) / 14); % MAGIC NUMBER.

ar = res(1) / res(3); % Aspect ratio: microns per pixel in X (or Y) / microns per pixel in Z.

% These four lines are size limits and shape threshold for seeds to be broken down.
sz_th = floor([2750 50000] .* numel(slices_or) / 14); % MAGIC NUMBERS 

min_border_cell_size = floor(2000 * numel(slices_or) / 14); % MAGIC NUMBER. A threshold is established for border cells to remove messed up ones. This same threshold, when applied to non-border cells, removes some valid cells!!

% Load original image.
if iscell(a)
	a = a{channel};
end

slices = slice_list;
original_a = a;


if numel(slices) >= 3
	top = 0;
	bottom = numel(slices) - 1; % -1 is lost in slice index (starts from zero).
				       
elseif (numel(slices) == 2)
	top = 0;
	bottom = 1;
else
	top = 0;
	bottom = 0;
end


middle = floor((top+bottom)/2);


% Resampling along XY.
a = a(0:subs_factor_xy:end, 0:subs_factor_xy:end, slices);
ar2 = ar * subs_factor_xy;

if clean_flag
	fprintf('Removing background ... ');
	a = clean_image2(a);
	fprintf('done.\n');
end

a2 = a;

if numel(only_seeds) == 1
        b = dip_image(a2, 'float');
        b2 = stretch(b);
        
        % Thresholding. 
        fprintf('Adaptive thresholding ... ');
        b2b = newim(size(b2));
        if ndims(b2) == 3 && size(b2, 3) == 1
	        b2b(:, :, 0) = dip_image(radaptivethreshold2(double(b2), WIN_SZ, C));
%             b2b(:,:,0) = dip_image(threshold(double(b2),'isodata')); %michael
        else
	        b2b = dip_image(radaptivethreshold2(double(b2), WIN_SZ, C));	
%             b2b = dip_image(threshold(double(b2),'isodata')); %michael
        end
        fprintf('done.\n');

        fprintf('Detecting and correcting seeds for watershed ... ');
        b3 = dilation(b2b, dil_thresh, 'elliptic');

        % Clear spurious pixels that end up breaking single seeds into two: the distance
        % transform value becomes small at the center of the seed, and after thresholding
        % the seed is splitted into two.
        if (clean_flag & size(b3, 3) >= 4)
	        tmp = clean_image2(b3);

	        b3 = tmp>0;
	        clear tmp;
        else
	        b3 = b3;
        end

        if tiny_cells && exist('bwmorph.m', 'file')
               sk = dip_image(zeros(size(b3, 2), size(b3, 1), size(b3, 3)), 'bin');;
               for i = 0:(size(b3, 3)-1)
	              sk(:, :, i) = dip_image(bwmorph(double(b3(:, :, i)),'skel',inf));
               end
               lab_sk = label(sk);
               m_sk = measure(lab_sk);
               ind = find(m_sk.size > 10000);
               
               msk = dip_image(zeros(size(sk, 2), size(sk, 1), size(sk, 3)));
               for ii = 1 : numel(ind)
	               msk = msk | (lab_sk == ind(ii));
               end
               sk = sk.* msk;
               b3 = dilation(sk, 5, 'elliptic');
               
	        c = ~b3;

	        % Distance transform to obtain seeds.
	        d = dt(c); 
	        % Extract local maxima.
	        %e = dip_image(radaptivethreshold(double(d), 30, C));
               e = d > min_dist;
        else
	        c = ~b3;

	        % Distance transform to obtain seeds.
	        d = dt(c);
	        % Extract local maxima.
	        e = d > min_dist;
        end
%  dipfig e;
%  dipshow(e);
%  dipfig d;
%  dipshow(d);
        % Label seeds.
        f = label(e, 1, min_seed_size, 2e9);

        % Check for seeds that correspond to two cells. This normally
        % have large sizes and concavities (corresponding to large
        % m.size and m.p2a). 
        m = measure(f, [], {'size'; 'p2a'}, [], 1);
        sz = m.size;
        p2a = m.p2a;
        
        % This bit here tries to take care of very elongated cells that might be
        % segmented as two different objects. This happened after I reduced the
        % minimum_seed_size from 125 to 90 to take care of some cells that were
        % segmented as a single one due to elimination of one of the corresponding
        % seeds. The height constraint alone also
        % works, but makes the algorithm a lot less general, since it does not
        % allow for multiple cell layers on top of each other.
        %I = find((sz < min_elong_seed_size & p2a > p2a_th & height < size(f, 3)));
        % Height constraint removed here, since the embryo is curved and
        % cells present different heights.
        I = find((sz < min_elong_seed_size & p2a > p2a_th));
        if (~ isempty(I))
	        rem_seeds = m(I);
	        for ilbl = 1:size(rem_seeds, 1)
		        msk = f == rem_seeds.id(ilbl);
		        f(msk) = 0;
	        end
        end
end
% The condition after the or was added later on, and it considerably slows down
% everything. But the results, specially at the
% very end of GBE, improve a bit more. It basically decides that "deformed" seeds
% need to be checked independently of their size (as long as they are not too small
% to correspond to more than one cell), but establishes a maximum
% deformation. Seeds with p2a>max_p2a  correspond probably to very elongated, two
% sided cells (and they are likely also smaller than the size threshold). The last
% size threshold cannot be reduced from (mean(m.size)/3 + .1*mean(m.size)/3).
if numel(only_seeds) == 1
        if clean_seeds

        I = find((sz > sz_th(1) & sz < sz_th(2) & p2a > p2a_th) | (p2a > min_p2a & p2a < max_p2a & sz > (mean(m.size)/3 + .1*mean(m.size)/3)));

        if (~ isempty(I))
	        m = m(I);
        else
	        m = [];
        end

        % For each one of those "weird" seeds, try and split it the best you can.
        % Simple 3D erosion does not do a good job, so ...
        for ilbl = 1:size(m, 1)
	        msk = f == m.id(ilbl);
	
	        % Erosion.
	        msk2 = erosion(msk, 2);
	
	        % Now check the number of components of the seed in the top, middle and bottom
	        % slices.
	        m1 = measure(msk2(:, :, top), [], {'size'}, [], 1); % Use 1 as top instead of zero, the segmentation is worst on the first slice. 
	        if (~isempty(m1)) 
		        m1 = m1.size;
		        % Seed bits with a single pixel are discarded.
		        n1 = numel(find(m1>min_seed_bit_sz));
	        else
		        n1 = 0;
	        end
	
	        m2 = measure(msk2(:, :, bottom), [], {'size'}, [], 1); % Use end-1 as bottom instead of end, the segmentation is worse on the last slice. 
	        if (~isempty(m2)) 
		        m2 = m2.size;
		        % Seed bits with a single pixel are discarded.
		        n2 = numel(find(m2>min_seed_bit_sz));
	        else
		        n2 = 0;
	        end
	
	        m3 = measure(msk2(:, :, middle), [], {'size'}, [], 1); % Use end-1 as bottom instead of end, the segmentation is worse on the last slice. 
	        if (~isempty(m3)) 
		        m3 = m3.size;
		        % Seed bits with a single pixel are discarded.
		        n3 = numel(find(m3>min_seed_bit_sz));
	        else
		        n3 = 0;
	        end
	
	
	        % Split ONLY if the top and bottom layers don't both have a single component.
	        % If one has a single component and the other zero, do nothing. If both have
	        % one or zero, do nothing.
	        if (n1 > 1 || n2 > 1)
		        clustersize = 0;
		        tmp = dip_image(zeros(size(f, 2), size(f, 1)), 'uint16');
	
		        f(msk) = 0;		

		        % If the seeds are joined in the bottom slice, use the top one.
		        if n2 == 1
			        clustersize = max(n1, n3);
			        max_f = max(f);
			
			        if (clustersize == n1)
				        depth = top;
			        else
				        depth = middle;
			        end
			
			        tmp = squeeze(label(msk2(:, :, depth), 2, min_seed_bit_sz+1) + max_f);
			        tmp(tmp == max_f) = 0;
						
			        im = squeeze(f(:, :, depth));
			        im = im + tmp;
			        f(:, :, depth) = im;
		
		        % Viceversa.
		        elseif n1 == 1
			        clustersize = max(n2, n3);
			        max_f = max(f);
			
			        if (clustersize == n2)
				        depth = bottom;
			        else
				        depth = middle;
			        end

			        tmp = squeeze(label(msk2(:, :, depth), 2, min_seed_bit_sz+1) + max_f);
			        tmp(tmp == max_f) = 0;
			
			        im = squeeze(f(:, :, depth));
			        im = im + tmp;
			        f(:, :, depth) = im;
		
		        % If the seeds are joined at the center, still use the top one
		        % (might as well have used the bottom one).
		        elseif (n1 == n2 && n2 == n3)
			        clustersize = n3;
			        max_f = max(f);
			        tmp = squeeze(label(msk2(:, :, middle), 2, min_seed_bit_sz+1) + max_f);
			        tmp(tmp == max_f) = 0;
		
			        im = squeeze(f(:, :, middle));
			        im = im + tmp;
			        f(:, :, middle) = im;
		
		        % What do you do when there are two components at the top slice and
		        % three at the bottom one (for example)? Take the maximum.
		        else
		
			        if (max([n1, n2, n3]) == n1)
				        clustersize = n1;
				        max_f = max(f);
				        tmp = squeeze(label(msk2(:, :, top), 2, min_seed_bit_sz+1) + max_f);
				        tmp(tmp == max_f) = 0;
		
				        im = squeeze(f(:, :, top));
				        im = im + tmp;
				        f(:, :, top) = im;
				
			        elseif (max([n1, n2, n3]) == n2)
				        clustersize = n2;
				        max_f = max(f);
				        tmp = squeeze(label(msk2(:, :, bottom), 2, min_seed_bit_sz+1) + max_f);
				        tmp(tmp == max_f) = 0;
		
				        im = squeeze(f(:, :, bottom));
				        im = im + tmp;
				        f(:, :, bottom) = im;				
				
			        elseif (max([n1, n2, n3]) == n3)
				        clustersize = n3;
				        max_f = max(f);
				        tmp = squeeze(label(msk2(:, :, middle), 2, min_seed_bit_sz+1) + max_f);
				        tmp(tmp == max_f) = 0;
		
				        im = squeeze(f(:, :, middle));
				        im = im + tmp;
				        f(:, :, middle) = im;				
			        end

		        end			
	        end
        end
        end
        fprintf('done.\n');
end

% Expand seeds on the full-resolution image.	
if (subs_factor_xy >= 1)	
	
	a = a2;		
	slices = (0:(size(a, 3) - 1));
	
        % We want to find the seeds. Thus, we calculate their center. 
	if numel(only_seeds) == 1
	        m = measure(f, [], 'Center');
	        centers = floor(m.center)';
	        centers(:, 1:2) = centers(:, 1:2) ;%.* subs_factor_xy_grow;
			
	        centers(:, 3) = centers(:, 3) + 1;
	        ind_exc = find(centers(:, 3) >= numel(slices));
	        if (~isempty(ind_exc))
		        centers(ind_exc, 3) = numel(slices) - 1;
	        end
	        %disp('1');
              	ids = m.id;
        % We have the seeds.	
        else
            only_seeds = round(only_seeds ./ subs_factor_xy);
	        centers(:, 1:2) = only_seeds(:, 1:2);
	        centers(:, 3) = 1;
	        ids = 1:size(only_seeds, 1);
            
            if ~isequal(size(centers,1),size(only_seeds,1))
                            %Michael check that only_seeds only contains different seed
            %locations
            pixelDistMatrix = mfloyd(only_seeds(:, 1:2), ones(size(only_seeds, 1)), 1); % find locations where there are repeated seeds
            %find indices of zero distances between pixels, ignoring pixels with the same ID
            [rows,columns]=find(pixelDistMatrix<1); 
            sameSeedInd=find(abs(columns-rows));
            
            if ~isempty(sameSeedInd)
                
                h=msgbox({['Error, there is at least 1 instance where there are more than 1 seed at a pixel location of the following seeds, ',num2str(sameSeedInd')] 'Please reseed.'},'Error','error' );
                segm = [];
                seed_fidu = [];
                return;
            end
            
            
            
            
            %Michael
            end
	end
	
	% If seeds are to be returned, we stop here.
	if only_seeds == 1
		seed_fidu = centers(:, [2 1 3]);
		segm = [];
		if debug_mode
			save debug_data a a2 b b2 b2b b3 c d e f centers roi slices 
		end
		
		return;
        % Else commented out: rather than returning the center of the seeds, we return the center of the segmented objects (see below).	
        %else
	%	seed_fidu = round(centers(:, [2 1 3]) .* subs_factor_xy);				
	end

	fprintf('Expanding seeds using watershed ... ');
	f_large = dip_image(zeros(size(a, 2), size(a, 1), numel(slices)), 'uint16');
        
        for ii = 1:numel(ids)
	        % When seeds are given from fiducials, you may get occasional [-1 -1 1], indicating a fiducial that was deleted and was not replaced by another one. Ignore these cases.
	        if centers(ii, 1) < 0
	                continue;
	        end
                		
                f_large(uint16(centers(ii, 1)), uint16(centers(ii, 2)), uint16(centers(ii, 3))) = ids(ii);
	end

	%disp('1');
	
	% CORRECTING SEEDS
	%overlay(a, f_large);
	%pnts = uint16(ginput);
	%f_large = dip_image(zeros(size(a, 2), size(a, 1), numel(slices)), 'uint16');
	%for ii = 1:numel(ids)
	%	f_large(centers(ii, 1), centers(ii, 2), centers(ii, 3)) = ids(ii);
	%end
	%for ip = 1:size(pnts, 1)
	%	f_large(pnts(ip, 1), pnts(ip, 2), centers(1, 3)) = max(ids)+ip;
	%end
	% END CORRECTING SEEDS.
	
	f_large = dilation(f_large, 3, 'elliptic');

	% REMOVE CELLS OUTSIDE THE GERMBAND BY USING AN ROI.
	if isempty(roi)
		roi = dip_image(ones(size(f_large, 2), size(f_large, 1), size(f_large, 3)), 'bin8');
	end
	
	%disp('1');
	
	if size(roi, 3) < size(f_large, 3)
		roi = repmat(roi(:, :, 0), [1 1 size(f_large, 3)]);
	elseif size(roi, 3) > size(f_large, 3)
		roi = roi(:, :, 0:(size(f_large, 3) - 1));
	end
	%disp('1');

	if (size(roi, 1) ~= size(f_large, 1))
		roi = dip_image(roi(0:subs_factor_xy:end, 0:subs_factor_xy:end, :), 'bin');
	end
	
	%disp('1');
        sm = closing(gaussf(a, gauss_factor.*[1 1 ar]), closing_param);
%    dipfig sm;
%    dipshow(sm);	

    if weighted
        g_large = dip_growregionsweighted(f_large, sm, roi, res, 5, []);
    else

        %michael
        if mgradient==0
            %sm=gradmag(sm,1);
            %michael
            g_large = rwaterseed(f_large, sm, 1, roi); % This is an older version of waterseed that returns labeled objects rather than the watershed lines (it uses dip_growregions rather than dip_seededwatershed).
        else
            sm=gradmag(sm,1);
            g_large = rwaterseed(f_large, sm, 1, roi); % This is an older version of waterseed that returns labeled objects rather than the watershed lines (it uses dip_growregions rather than dip_seededwatershed).
            
        end
    end
    
	%g_large2 = waterseed(f_large, a, 1); % Now connectivity 1, 2 also works fine ... but there are more irregularities in the cells. 
	
	segm = g_large;
	fprintf('done.\n');
	
        fprintf('Collecting seeds ... ');
        ms = measure(segm, [], 'Center');
        centers = floor(ms.center)';

        centers(:, 3) = centers(:, 3) + 1;
        ind_exc = find(centers(:, 3) >= numel(slices));
        if (~isempty(ind_exc))
                centers(ind_exc, 3) = numel(slices) - 1;
        end
        
        seed_fidu = centers(:, [2 1 3]);
        

        fprintf('done.\n');
	
	fprintf('Cleaning up and writing results ... ');
	% Delete objects in direct contact with the border of the roi (they
	% might be broken). Alternatively you could force their inclusion
	% in the ROI.
	% Just in case there are cells immediately next to the image border
	% set a zero-valued frame all around the image.
	borderlab = max(segm) + 1;
	borderim_inside = segm;
	borderim_inside(find(borderim_inside == 0)) = borderlab;

	borderim = dip_image(borderlab .* ones(size(segm, 2)+2, size(segm, 1)+2, size(segm, 3)));
	borderim(1:end-1, 1:end-1, :) = borderim_inside;
	
	border_list = rneighborsq(borderlab, borderim, 0);
	mask = dip_image(zeros(size(segm, 2), size(segm, 1), size(segm, 3)), 'bin'); 
	for io = 1 : numel(border_list)
		mask = mask | (segm == border_list(io));
	end

	mask = ~mask;

	segm = segm .* mask;
	segm = dip_image(segm, 'uint16');


	if write_flag
		writeim(segm, time_str, 'ICSv2', 0);
		writeim(a2, cat(2, 'im_', time_str), 'ICSv2', 1);
		%dipshow(a2);
	end
	
	fprintf('done.\n');

end

if debug_mode
	save debug_data a a2 b b2 b2b b3 c d e f centers f_large roi g_large slices
end

toc
