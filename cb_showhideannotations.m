% ********************************************************************
function cb_showhideannotations(fig)

flag = 'on';

ch = get(get(fig, 'CurrentAxes'), 'Children');
for jj = 1:numel(ch)
        if strcmp(get(ch(jj), 'Tag'), 'Fiducial') || strcmp(get(ch(jj), 'Tag'), 'Delimiter') || strcmp(get(ch(jj), 'Tag'), 'Unique') || strcmp(get(ch(jj), 'Tag'), 'Polyline')
                if strcmp(get(ch(jj), 'Visible'), 'on')
                        flag = 'off';
                        break;
                else 
                        flag = 'on';
                        break;
                end
        end
end

for jj = jj:numel(ch)
        if strcmp(get(ch(jj), 'Tag'), 'Fiducial') || strcmp(get(ch(jj), 'Tag'), 'Delimiter') || strcmp(get(ch(jj), 'Tag'), 'Unique') || strcmp(get(ch(jj), 'Tag'), 'Polyline')
                set(ch(jj), 'Visible', flag);
        end
end
end