% mtrx = cb_multicolorquantifypoly(fig, channels, thepolys)
function mtrx = cb_multicolorquantifypoly(fig, channels, thepolys)

mtrx = zeros(numel(thepolys), 1+numel(channels));
for il = 1:numel(thepolys)
    if ~ isempty(thepolys{il})
        [tmp sz] = cb_multicolormeasurepoly(fig, thepolys{il}, channels);
        for ic = 1:numel(tmp)
            mtrx(il, ic) = tmp(1, ic);
        end
        
        mtrx(il, ic + 1) = sz;
    end
    
    clear tmp;
end
end