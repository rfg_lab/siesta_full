function [intensity_values intensity_values_after_bleaching_corr  rawData md_bg me_bleaching]=IntensityProfilePolygons_tzc(ud,tps,intialPoint,finalPoint)

%%%
% This function normalizes all the values of the boundary to the mean value of the first timepoint 
%%%
% 
%     ud = get(gcf, 'UserData'); %Input Argument 1 
%     tps = [1  13  ]; % Time points where trajectories were defined %Input argument 2
%     intialPoint=[422 289;406 268]; %Input arrgument 3
%     finalPoint=[423 326;405 314];  %Input arrgument 4

    first = 1;

    last = size(ud.rpolygons, 3);


    for jj = 1:size(ud.rpolygons, 3)
            if isempty(ud.rpolygons{1, 1, jj})
                    first = first + 1;
            else
                    break;
            end
    end

    for jj = size(ud.rpolygons, 3):-1:1
            if isempty(ud.rpolygons{1, 1, jj})
                    last = last - 1;
            else
                    break;
            end
    end

    set(gcf, 'UserData', ud);

    szIm=ud.imsize;
    szIm=szIm(1,3);
    
    
   cm = jet(numel(tps));
    fig=gcf;
    for kk=1:size(tps,2)

       [pix_vals_r(kk,:) rawData(kk,:) md_bg(kk,:) me_bleaching(kk,:) pix_vals_g pix_vals_b coords] = quantifypolygonprofile_tzc(fig, ud.rCHANNEL, ud.rBRUSH_SZ, tps(kk),intialPoint(kk,:), finalPoint(kk,:));
        %Normalize with the mean of the first timepoint
         %pix_vals_r(kk,:)=pix_vals_r(kk,:) ./pix_vals_r(kk,1) ;
         
        intensity_values_after_bleaching_corr(kk,:)=pix_vals_r(kk,:);
        if kk==1
            md=mean(pix_vals_r(1,:));
        end
         intensity_values(kk,:)= pix_vals_r(kk,:)/md;
        size(coords);
    kk;
        set(gcf, 'UserData', ud);


        figure (12)
        % Plot data and fits on a pixel scale.
        yvals = smooth_curve2(intensity_values(kk,:),2);
        plot(((0:(numel(yvals)-1))./(numel(yvals)-1)), yvals,'Color',cm(kk,:),'LineWidth', 4)
        hold on
        set(gca,'XTick',[0:0.2:numel(yvals)-1/(numel(yvals)-1)])
        xlabel('edge position', 'FontSize', 32, 'FontWeight', 'bold');
        ylabel('fluorescence (a.u)', 'FontSize', 32, 'FontWeight', 'bold');
        set(gca, 'FontSize', 32, 'FontWeight', 'bold', 'LineWidth', 3)
        xlim([0 1]);
        ylim([0 4]);
        points=((0:numel(yvals)-1)./(numel(yvals)-1));

       

    end
end