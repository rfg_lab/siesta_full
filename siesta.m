%INSTRUCTIONS:
%
% KYMOGRAPHS
% - First annotate the images with fiducials to register them (using 'R').
% - Draw a rectangle ('r' option) in one of the time points to determine the ROI.
% - Select 'K' and click on the contour of the ROI.
% - Choose a file name and format. The kymograph is displayed in a new window.
%
% FRAP analysis
% -Load time series.
% -Draw fiducials around the bleached area in each one of the time points to analyze.
% -Add a unique marker at the center of the ROI.
% -Register all tps to tp before bleaching.
% -Crop the image around the unique marker.
% -Export image as sequence (to save 16b info). Use ics format.
% -Draw ROI in all time points.
% -Draw trajectory along the edge span in the two time points preceding bleaching.
% -Save annotations.
% -Measure signal over time.
%
% CLUSTERING ANALYSIS
% - Project the image (from the color channel) using the 'J' option (channels 1-4).
% - Change to the channel that you will use to segment the image.
% - Save the projected image from the channel you want to quantify ('Y').
% - Segment the image.
% - Save original geometry.
% - Correct geometry.
% - Define ROI (350x350) and save it.
% - Measure edges by selecting positive interfaces ('p', min_l = 0, ang = 0).
% - Save the corrected and consolidated geometry (this will also
% save the edge measurements).
%
% KYMOGRAPH
% If there are fiducials in some time points (e.g. used to register
% the movie) the kymograph will only include the time points with
% fiducials. You can use this to only include some time points in
% the kymograph.
%
% Rodrigo Fernandez-Gonzalez
% rodrigo.fernandez.gonzalez@utoronto.ca
% 2007-2017
% 
%
% Preparing the source for distribution: 
% 1. Comment in siesta.m and mycallbacks2.m the options that should
% not be deployed. Save the new siesta.m as siestaXX.m, where XX is
% the version number.
%
% 2. Run these commands:
% [fList,pList] = matlab.codetools.requiredFilesAndProducts('siestaXX.m');
% [fList2,pList2] = matlab.codetools.requiredFilesAndProducts('mycallbacks2.m');
%
% for ii = 1:numel(fList); fprintf('\n%d %s', ii, fList{ii}); end
% for ii = 1:numel(fList2); fprintf('\n%d %s', ii, fList2{ii}); end
%
% 3. Check in the lists the indices that are not dipimage files and copy them into a new folder. For instance:
% for ii = 307:310; copyfile(fList{ii}, '../siesta4_src/'); end 
% for ii = 380:577; copyfile(fList2{ii}, '../siesta4_src/'); end  
%
% 4. Also copy the siesta_dat.mat file, show_coord_pix.m.
%
% 5. Edit the help of siestaXX.m. For instance, for siesta4:
% SIESTA4
%
% Code provided AS-IS.
% We are not responsible for bugs, errors, etc.
% However, we welcome comments on bugs, errors, etc.
% USE AT YOUR OWN RISK ;)
%
% Execute by typing siesta4 at the Matlab prompt.
%
% SIESTA4 has been developed and (somewhat) tested on this platform:
%
% ----------------------------------------------------------------------------------------------------
% MATLAB Version: 8.4.0.150421 (R2014b)
% MATLAB License Number: 676468
% Operating System: Mac OS X  Version: 10.12.3 Build: 16D32 
% Java Version: Java 1.7.0_55-b13 with Oracle Corporation Java HotSpot(TM) 64-Bit Server VM mixed mode
% ----------------------------------------------------------------------------------------------------
% MATLAB                                                Version 8.4        (R2014b)
% DIPimage, a scientific image analysis toolbox         Version 2.8                
% DIPlib, a scientific image analysis library           Version 2.8                
% Image Processing Toolbox                              Version 9.1        (R2014b)
%
% Which means you need DIPImage (http://www.diplib.org/dipimage) and the Image Processing Toolbox (Mathworks)
% to run all the functionality. This also means that if you are trying to run SIESTA4 on a more recent version
% of Matlab (e.g. 2016b), you may run into trouble, because Mathworks introduced a number of changes that
% affect back-compatibility. We are working on these, but it may take some time before we have a version of
% SIESTA that works on all Matlab versions.
%
% Or we may just rewrite the whole damned thing in Python ;)
%
% Code by:
%
% Teresa Zulueta-Coarasa
%
% Michael Wang
%
% Colin Tong
%
% Angeline Yasodhara
%
% and
%
% Rodrigo Fernandez-Gonzalez
%
% rodrigo.fernandez.gonzalez@utoronto.ca
% 2007-2017
% 

% -----------------------------------------------------------------------------------------
% COMPILING THE SOURCE
% Before you compile: Matlab has decided to deprecate DEPFUN in
% favour of matlab.codetools.requiredFilesAndProducts when looking
% for dependencies in the code before compilation. However, the new
% function does not seem to work with dipimage. Thus, it is
% necessary to force mcc to use depfun. To this end, use this (or
% include in your startup.m):
% setenv('MCC_USE_DEPFUN','1')
%
% Compile (from MATLAB, not from the command line!!!!, and making
% sure the current working directory is /Users/rodrigo/src/CellSegmentation) with
% mcc -m siesta.m -I /Users/rodrigo/dip/common/dipimage -I /Users/rodrigo/dip/common/mlv7_9/dipimage_mex -I /Users/rodrigo/dip/common/mlv7_9/diplib -a . -a /usr/local/lib/libdip.dylib  -a /usr/local/lib/libdipio.dylib -a /usr/local/lib/libdml_mlv7_9.dylib -C
%
% The following error message:
%Depfun error: 'Unable to locate .#siesta.m as a function on the MATLAB path'
%Error using mcc
%Error executing mcc, return status = 1 (0x1).
%
% can be solved by deleting the .#* files in
% /Users/rodrigo/CellSegmentation
%
% Then move the siesta.app folder into ~/siesta/CURRENT. You may want to back up this folder first. Keep the readme file and the shell script to run siesta. Just substitute the .app file.
%
% Run executable with:
% ./run_siesta
% /Applications/MATLAB/MATLAB_Compiler_Runtime/v84/. The parameter is not necessary unless you have multiple MCRs. If you do, you
% will need to use the parameter, pointing towards the MCR version corresponding to the Matlab version that you are using.
%
% To zip the contents, CD into the folder containing readme_siesta.txt, run_siesta3.sh and siesta3.app/ and type
% zip -r siesta .
% If you run the executable before zipping, a bunch of files will
% be extracted (the executable is actually compressed), and the
% final zip files will be much larger.

function fig = siesta(varargin)

if isdeployed
    %cd('/Users/rodrigo/src/CellSegmentation');
    dip_initialise_libs silent;
end


if nargin == 0
        if exist('siesta_data.mat', 'file')	
                load(which('siesta_data.mat'));       
        else
                fprintf('\n\nYour installation is messed up!!!\nsiesta_data.mat is missing!!!\n\n');
                error('');
        end                
        
        % Color logo.
%          r = cat(3, r, r);  
%          g = cat(3, g, g);
%          b = cat(3, b, b);
%          siestaim = joinchannels('rgb', r, g, b);

        % Greyscale logo.
        %siestaim = dip_image(r);
        %siestaim = cat(3, siestaim, siestaim);
end

if nargin >= 1
	siestaim = varargin{1};
end

if nargin >= 2
	param = varargin{2};
else
	param = '';
end



if isa(siestaim, 'dip_image')
        % Do nothing, siestaim contains the variable.
elseif isnumeric(siestaim)
        siestaim = dip_image(siestaim);
elseif(isstr(siestaim))
        file = siestaim;
        if isstr(param) && (~ strcmp(param, 'time'))
                try       
	                siestaim = readim(siestaim);
                catch
                        siestaim = readtimeseries(siestaim, '', [0 0], 'no', 'no');
                end
        elseif isstr(param) && (strcmp(param, 'time'))
                try 
                        siestaim = readtimeseries(siestaim, '', [0 0], 'no', 'no');
                catch
                        siestaim = tiffread(siestaim);
                end                        
        end                
else
	file = [];
end

if (isstr(param) & strcmp(param, 'proj')) 
	siestaim{1} = max(siestaim{1}, [], 3);
	siestaim{2} = max(siestaim{2}, [], 3);
	siestaim{3} = max(siestaim{3}, [], 3);
	file2 = [];

% Make greyscale images into "color" ones
elseif (isstr(param) & strcmp(param, 'color')) || (size(siestaim{1}, 3) == 1 & (~ isa(siestaim,'dip_image_array')))
	if ndims(siestaim) == 2
		color_sz = size(siestaim) == [3 1];
	elseif ndims(siestaim) == 3
		color_sz = size(siestaim) == [3 1 1];
	end
	
	if ~ (sum(color_sz) == ndims(siestaim))
		siestaim = joinchannels('rgb', siestaim, siestaim, siestaim);
	end

% Overlay two images.
elseif isstr(param) & (~ strcmp(param, '')) & (~ strcmp(param, 'time'))
	file2 = param;
	param = readcolorim(param);
	siestaim = overlay_transp(siestaim, param);
elseif ((~isstr(param)) && (~ isempty(param)))
	siestaim = overlay_transp(siestaim, param);
	file2 = [];
else
	file2 = [];
end

% Make non-3D images "pseudo-3D".
if (size(siestaim{1}, 3) == 1)
	siestaim = joinchannels('rgb', cat(3, siestaim{1}, siestaim{1}), cat(3, siestaim{2}, siestaim{2}), cat(3, siestaim{3}, siestaim{3}));
	g = siestaim{2};
	b = siestaim{3};
	
	
end

% Display image.
fig = dipshow(siestaim);

ud = get(fig, 'UserData');
ud = init_userdata(ud);
set(fig, 'UserData', ud);

% The use of currentkey if currentcharacter is empty allows you to use function keys and other random keys on the keyboard (arrows, home, insert, ...).
set(fig, 'KeyPressFcn', 'if (~ isempty(get(gcbf, ''CurrentCharacter''))) mycallbacks2(gcbf, cat(2, ''k_'', get(gcbf, ''CurrentCharacter''))); else mycallbacks2(gcbf, cat(2, ''k_'', get(gcbf, ''CurrentKey'')));end');


% Additional menus.

% IO MENU -----------------------------------------------------------------------
hio = uimenu(fig,'Label','IO','Tag','io');
uimenu(hio,'Label','Load grayscale image ...','Tag','load_ts','Accelerator', 't', 'Callback',...
       'mycallbacks2(gcbf, ''load_ts'');'); 
uimenu(hio,'Label','Load color image ...','Tag','load_stk','Accelerator', 'I', 'Callback',...
       'mycallbacks2(gcbf, ''load_stk'');');        
%uimenu(hio,'Label','Save image ...','Tag','save_im', 'Separator', 'on', 'Callback',...
%       'mycallbacks2(gcbf, ''save_im'');');
uimenu(hio,'Label','Save image ...','Tag','save_ts','Accelerator', 'Y', 'Callback',...
       'mycallbacks2(gcbf, ''save_ts'');'); 
uimenu(hio,'Label','Export as file sequence ...','Tag','save_seq','Accelerator', 'x','Callback',...
       'mycallbacks2(gcbf, ''save_seq'');');
uimenu(hio,'Label','Export as movie ...','Tag','save_avi','Callback',...
       'mycallbacks2(gcbf, ''save_avi'');');
%uimenu(hio,'Label','Next image','Tag','nxt_im','Accelerator', '.', ...
%'Separator', 'on', 'Callback',...
%       'mycallbacks2(gcbf, ''nxt_im'');'); 
%uimenu(hio,'Label','Previous image','Tag','prv_im','Accelerator', ',', 'Callback',...
%       'mycallbacks2(gcbf, ''prv_im'');'); 
%uimenu(hio,'Label','Load fiducial markers ...','Tag','load_fiducials','Callback',...
%       'mycallbacks2(gcbf, ''load_fiducials'');');
%uimenu(hio,'Label','Save fiducial markers ...','Tag','save_fiducials','Callback',...
%       'mycallbacks2(gcbf, ''save_fiducials'');');
uimenu(hio,'Label','Load annotations ...','Tag','load_ann','Separator', 'on', 'Accelerator', 'L', 'Callback',...
       'mycallbacks2(gcbf, ''load_ann'');');
uimenu(hio,'Label','Add annotations (fiducials and polygons) ...','Tag','add_ann', 'Callback',...
       'mycallbacks2(gcbf, ''add_ann'');');
uimenu(hio,'Label','Load fiducials only ...','Tag','load_fidu_only', 'Callback',...
       'mycallbacks2(gcbf, ''load_fidu_only'');');
uimenu(hio,'Label','Load fiducials as delimiters ...','Tag','load_fidu_del', 'Accelerator', 'D', 'Callback',...
       'mycallbacks2(gcbf, ''load_fidu_del'');');
uimenu(hio,'Label','Import Metamorph region as unique marker ...','Tag','load_metamorph', 'Accelerator', 'U', 'Callback',...
       'mycallbacks2(gcbf, ''load_metamorph'');');
uimenu(hio,'Label','Import labeled image ...','Tag','import_labeled', 'Callback',...
       'mycallbacks2(gcbf, ''import_labeled'');'); %Loads segmentation results from a labeled image, 2D or 3D, as polylines to allow correction and interaction with the data. You should implement an option to "export as labeled image".
uimenu(hio,'Label','Save annotations ...','Tag','save_ann','Accelerator', 'a', 'Callback',...
       'mycallbacks2(gcbf, ''save_ann'');');
uimenu(hio,'Label','Export polyline annotations (no dividing cells) ...','Tag','export_poly', 'Callback',...
       'mycallbacks2(gcbf, ''export_poly'');');
uimenu(hio,'Label','Export polyline annotations including dividing cells ...','Tag','export_poly_div', 'Callback',...
       'mycallbacks2(gcbf, ''export_poly_div'');');
uimenu(hio,'Label','Export all polyline annotations','Tag','export_all_poly', 'Callback',...
       'mycallbacks2(gcbf, ''export_all_poly'');');
uimenu(hio,'Label','Export polylines as roi ...','Accelerator', 'O', 'Tag','export_poly_roi', 'Callback',...
       'mycallbacks2(gcbf, ''export_poly_roi'');');

% OPTIONS MENU --------------------------------------------------------------
hcorrections = uimenu(fig,'Label','Options','Tag','options');
uimenu(hcorrections,'Label','Pick pixel value','Tag','pick_val','Callback',...
       'mycallbacks2(gcbf, ''pick_val'');');
uimenu(hcorrections,'Label','Set brush diameter','Accelerator', 'B', 'Tag','set_brush','Callback',...
       'mycallbacks2(gcbf, ''set_brush'');');
uimenu(hcorrections,'Label','Set measurement channel','Tag','set_channel','Callback',...
       'mycallbacks2(gcbf, ''set_channel'');');
switch ud.rLOAD_ANNTIME
	case 0
		status = 'off';
	otherwise
		status = 'on';
end
uimenu(hcorrections,'Label','Load annotations with time series','Tag','load_anntime','Callback',...
       'mycallbacks2(gcbf, ''load_anntime'');', 'Checked', status);
uimenu(hcorrections,'Label','Save geometries after segmentation','Tag','save_geom','Callback',...
       'mycallbacks2(gcbf, ''save_geom'');');
switch ud.rNODESANDEDGES
        case 0
                status = 'off';
        otherwise
                status = 'on';
end
uimenu(hcorrections,'Label','Display geometry in new window after segmentation','Tag','nodes_edges','Callback',...
       'mycallbacks2(gcbf, ''nodes_edges'');', 'Checked', status);
uimenu(hcorrections,'Label','Draw in all frames','Tag','draw_all','Callback',...
       'mycallbacks2(gcbf, ''draw_all'');');
uimenu(hcorrections,'Label','A*','Accelerator', 'A','Tag','astar_config','Callback',...
       'mycallbacks2(gcbf, ''astar_config'');');
       
       
% IMAGE MENU -----------------------------------------------------------------------
himage = uimenu(fig,'Label','Image','Tag','image');
%uimenu(hio,'Label','Decompose in colors','Tag','decompose', 'Accelerator', 'd', %'Callback',...
%       'mycallbacks2(gcbf, ''decompose'');'); 
uimenu(himage,'Label','Go to ...','Tag','go_to','Accelerator', 'g', 'Callback',...
       'mycallbacks2(gcbf, ''go_to'');');        
uimenu(himage,'Label','Switch channels','Tag','switch_ch', 'Accelerator', 's', 'Callback',...
       'mycallbacks2(gcbf, ''switch_ch'');'); 
uimenu(himage,'Label','Crop image','Tag','crop_im', 'Accelerator', 'P', 'Separator', 'on', 'Callback',...
       'mycallbacks2(gcbf, ''crop_im'');'); 
uimenu(himage,'Label','Project image ...','Tag','proj_im', 'Accelerator', 'J', 'Separator', 'on', 'Callback',...
       'mycallbacks2(gcbf, ''proj_im'');'); 
uimenu(himage,'Label','Sum channels','Tag','sum_ch', 'Accelerator', '+', 'Separator', 'on', 'Callback',...
       'mycallbacks2(gcbf, ''sum_ch'');'); 
       
uimenu(himage,'Label','Register all to current frame','Tag','reg_fr', 'Accelerator', 'R', 'Separator', 'on', 'Callback',...
       'mycallbacks2(gcbf, ''reg_fr'');'); 
uimenu(himage,'Label','Find seeds ...','Tag','find_seeds', 'Separator', 'on', 'Accelerator', 'S', 'Callback',...
       'mycallbacks2(gcbf, ''find_seeds'');');        
uimenu(himage,'Label','Propagate these seeds ...','Tag','propagate_seeds', 'Accelerator', 'E', 'Callback',...
       'mycallbacks2(gcbf, ''propagate_seeds'');');        
uimenu(himage,'Label','Expand from these seeds ...','Tag','expand_seeds', 'Accelerator', 'X', 'Callback',...
       'mycallbacks2(gcbf, ''expand_seeds'');');        
uimenu(himage,'Label','Expand and Propagate these seeds ...','Tag','expandnpropagate_seeds', 'Accelerator', 'Z', 'Callback',...
       'mycallbacks2(gcbf, ''expandnpropagate_seeds'');');        
uimenu(himage,'Label','Segment tricellular vertices ...','Tag','segment', 'Separator', 'on', 'Callback',...
       'mycallbacks2(gcbf, ''segment'');'); 
uimenu(himage,'Label','Kymograph','Tag','kymograph', 'Accelerator', 'K', 'Separator', 'on', 'Callback',...
       'mycallbacks2(gcbf, ''kymograph'');'); 


% ANNOTATION MENU ---------------------------------------------------------------
hannotation = uimenu(fig,'Label','Annotation','Tag','annotation');
uimenu(hannotation,'Label','Paint pixels','Tag','paint_pix','Callback',...
       'mycallbacks2(gcbf, ''paint_pix'');');
uimenu(hannotation,'Label','Select individual cells','Tag','select_acell','Separator', 'on', 'Callback',...
       'mycallbacks2(gcbf, ''select_mode'');');    
uimenu(hannotation,'Label','Select groups of cells','Tag','select_mode','Callback',...
       'mycallbacks2(gcbf, ''spoly_line'');');    
uimenu(hannotation,'Label','Delete cell','Tag','delete_mode','Callback',...
       'mycallbacks2(gcbf, ''delete_mode'');');    
%uimenu(hannotation,'Label','Draw line mask','Accelerator', 'M','Tag','line_mask','Separator', 'on', 'Callback',...
%       'mycallbacks2(gcbf, ''pline_mask'');');
%uimenu(hannotation,'Label','Clear line masks','Accelerator', 'C','Tag','clear_lines','Callback',...
%       'mycallbacks2(gcbf, ''clear_lines'');');     
uimenu(hannotation,'Label','Clear dots','Tag','clear_dots','Callback',...
       'mycallbacks2(gcbf, ''clear_dots'');');     
uimenu(hannotation,'Label','Toggle ids','Accelerator', 'i','Tag','toggle_fiduid','Separator', 'on', 'Callback',...
       'mycallbacks2(gcbf, ''toggle_fiduid'');');
uimenu(hannotation,'Label','Toggle annotations','Accelerator', 'h','Tag','toggle_ann', 'Callback',...
       'mycallbacks2(gcbf, ''toggle_ann'');');
uimenu(hannotation,'Label','Set fiducial markers','Accelerator', 'f','Tag','fiducial_mark','Separator', 'on', 'Callback',...
       'mycallbacks2(gcbf, ''fiducial_mark'');');
uimenu(hannotation,'Label','Delete fiducial markers outside polyline','Accelerator', 'e','Tag','del_fidu_poly', 'Callback',...
       'mycallbacks2(gcbf, ''del_fidu_poly'');');
uimenu(hannotation,'Label','Track fiducial markers ...','Accelerator', 'T','Tag','track_fidu', 'Callback',...
       'mycallbacks2(gcbf, ''track_fidu'');');
uimenu(hannotation,'Label','Copy fiducial markers','Accelerator', 'C','Tag','copy_fidu', 'Callback',...
       'mycallbacks2(gcbf, ''copy_fidu'');');
uimenu(hannotation,'Label','Paste fiducial markers','Accelerator', 'V','Tag','paste_fidu', 'Callback',...
       'mycallbacks2(gcbf, ''paste_fidu'');');
uimenu(hannotation,'Label','Move fiducials in polyline','Tag','move_poly','Callback',...
       'mycallbacks2(gcbf, ''move_ann'');');
       
uimenu(hannotation,'Label','Set delimiter','Accelerator', 'd','Tag','delim_mark','Callback',...
       'mycallbacks2(gcbf, ''delim_mark'');');
uimenu(hannotation,'Label','Set unique marker','Accelerator', 'u','Tag','unique_mark','Callback',...
       'mycallbacks2(gcbf, ''unique_mark'');');
uimenu(hannotation,'Label','Draw angle','Tag','draw_ang','Callback',...
       'mycallbacks2(gcbf, ''draw_ang'');');
uimenu(hannotation,'Label','Draw trajectory','Accelerator', 'j', 'Tag','opoly_line','Callback',...
       'mycallbacks2(gcbf, ''opoly_line'');');
uimenu(hannotation,'Label','Equalize trajectories','Accelerator', '=', 'Tag','eq_tr','Callback',...
       'mycallbacks2(gcbf, ''eq_tr'');');       
uimenu(hannotation,'Label','Clear dots','Tag','clear_dots','Callback',...
       'mycallbacks2(gcbf, ''clear_dots'');');     
uimenu(hannotation,'Label','Draw polyline','Accelerator', 'y', 'Tag','ppoly_line','Callback',...
       'mycallbacks2(gcbf, ''ppoly_line'');');
uimenu(hannotation,'Label','LiveWire','Accelerator', 'W', 'Tag','live_wire','Callback',...
       'mycallbacks2(gcbf, ''live_wire'');');
uimenu(hannotation,'Label','LiveA*','Tag','live_astar','Callback',...
       'mycallbacks2(gcbf, ''live_astar'');');
uimenu(hannotation,'Label','MEDUSA','Accelerator', 'M', 'Tag','snakes','Callback',...
       'mycallbacks2(gcbf, ''snakes'');');
uimenu(hannotation,'Label','Circle','Accelerator', 'l', 'Tag','circle','Callback',...
       'mycallbacks2(gcbf, ''circle'');');
uimenu(hannotation,'Label','Rectangle','Accelerator', 'r', 'Tag','rect','Callback',...
       'mycallbacks2(gcbf, ''rect'');');
uimenu(hannotation,'Label','Track polylines (using fiducials) ...', 'Tag','track_poly', 'Callback',...
       'mycallbacks2(gcbf, ''track_poly'');');       
uimenu(hannotation,'Label','Track polylines (using centroids) ...', 'Accelerator', '%', 'Tag','track_poly_area', 'Callback',...
       'mycallbacks2(gcbf, ''track_poly_area'');');       
uimenu(hannotation,'Label','Track polylines (divisions) ...', 'Accelerator', '$', 'Tag','track_poly_div', 'Callback',...
       'mycallbacks2(gcbf, ''track_poly_div'');');       
uimenu(hannotation,'Label','Copy polyline','Accelerator', 'c', 'Tag','copy_poly','Callback',...
       'mycallbacks2(gcbf, ''copy_poly'');');
uimenu(hannotation,'Label','Paste polyline','Accelerator', 'v', 'Tag','paste_poly','Callback',...
       'mycallbacks2(gcbf, ''paste_poly'');');
uimenu(hannotation,'Label','Move polyline','Accelerator', 'm', 'Tag','move_poly','Callback',...
       'mycallbacks2(gcbf, ''move_poly'');');
uimenu(hannotation,'Label','Delete tracked polylines', 'Tag','deletetracked_poly_line','Callback',...
       'mycallbacks2(gcbf, ''deletetracked_poly_line'');');
uimenu(hannotation,'Label','Delete all markings in this image','Accelerator', '\', 'Tag','del_all_this','Callback',...
       'mycallbacks2(gcbf, ''del_all_this'');');
uimenu(hannotation,'Label','Delete all markings in this sequence','Accelerator', '-', 'Tag','del_all_all','Callback',...
       'mycallbacks2(gcbf, ''del_all_all'');');
       
uimenu(hannotation,'Label','Input ROI (FRAP) ...','Tag','input_roi','Separator', 'on', 'Callback',...
       'mycallbacks2(gcbf, ''input_roi'');');
       



% MEASUREMENT MENU --------------------------------------------------------------
hquantif = uimenu(fig,'Label','Measurement','Tag','Quantification');
uimenu(hquantif,'Label','Draw and Measure trajectory','Accelerator', 'q','Tag','qline_mask','Callback',...
       'mycallbacks2(gcbf, ''qpoly_line'');');
%  uimenu(hquantif,'Label','Quantify signal ...','Tag','signal','Callback',...
%           'mycallbacks2(gcbf, ''signal'');');    
uimenu(hquantif,'Label','Quantify signal polarity based on trajectories ...','Accelerator', 'Q','Tag','signal_polarity','Callback',...
       'mycallbacks2(gcbf, ''signal_polarity'');');    
uimenu(hquantif,'Label','Quantify signal polarity based on gradients ...','Accelerator', 'G','Tag','signal_polarity_gradient','Callback',...
       'mycallbacks2(gcbf, ''signal_polarity_gradient'');');    
uimenu(hquantif,'Label','Quantify brightness in regions ...', 'Tag','signal_level','Callback',...
       'mycallbacks2(gcbf, ''signal_level'');');    
%  uimenu(hquantif,'Label','Measure signal over time ...','Accelerator', 'F','Tag','signal_t','Callback',...
%         'mycallbacks2(gcbf, ''signal_t'');');    
uimenu(hquantif,'Label','Measure trajectories over time ...','Tag','tr_t','Callback',...
       'mycallbacks2(gcbf, ''tr_t'');');    
uimenu(hquantif,'Label','Measure trajectory profile ...','Tag','tr_profile','Callback',...
       'mycallbacks2(gcbf, ''tr_profile'');');    
uimenu(hquantif,'Label','Measure poligon intensity profile between fiducials','Tag','vertex_intensity','Callback',...
       'mycallbacks2(gcbf, ''vertex_intensity'');');    
       
uimenu(hquantif,'Label','Measure brightness in roi (only first one drawn) over time ...','Tag','roi_t','Callback',...
       'mycallbacks2(gcbf, ''roi_t'');');           
uimenu(hquantif,'Label','Measure angles ...','Accelerator', 'H','Tag','measure_ang','Callback',...
       'mycallbacks2(gcbf, ''measure_ang'');');    
uimenu(hquantif,'Label','Measure polylines ...','Tag','measure_poly','Separator', 'on', 'Callback',...
       'mycallbacks2(gcbf, ''measure_poly'');');
uimenu(hquantif,'Label','Plot polylines over time ...','Tag','plot_poly', 'Callback',...
       'mycallbacks2(gcbf, ''plot_poly'');');
uimenu(hquantif,'Label','Measure angle of cell division', 'Tag','startDivAnalyze','Separator', 'off', 'Callback',...
       'mycallbacks2(gcbf, ''startDivAnalyze'');');
uimenu(hquantif,'Label','(3D) Print selected cell IDs','Tag','line_mask','Separator', 'on', 'Callback',...
       'mycallbacks2(gcbf, ''sel_cells'');');
uimenu(hquantif,'Label','(3D) Measure cells (dip_image) ...','Tag','msr_dip','Callback',...
       'mycallbacks2(gcbf, ''msr_dip'');');
uimenu(hquantif,'Label','(3D) Measure cells (tri) ...','Tag','msr_tri','Callback',...
       'mycallbacks2(gcbf, ''msr_tri'');');
uimenu(hquantif,'Label','Select two fiducial markers','Accelerator', 'w', 'Tag','sel_two','Separator', 'on', 'Callback',...
       'mycallbacks2(gcbf, ''sel_two'');');
uimenu(hquantif,'Label','Pair distance ...','Tag','pair_dist', 'Separator', 'off', 'Callback',...
       'mycallbacks2(gcbf, ''pair_dist'');');
uimenu(hquantif,'Label','Local cut analysis ...','Tag','local_cut','Separator', 'off', 'Callback',...
       'mycallbacks2(gcbf, ''local_cut'');');
uimenu(hquantif,'Label','Vector field analysis ...', 'Tag','vector_field','Separator', 'off', 'Callback',...
       'mycallbacks2(gcbf, ''vector_field'');');
uimenu(hquantif,'Label','Vector field time analysis ...','Accelerator', 'V', 'Tag','vf_time','Separator', 'off', 'Callback',...
       'mycallbacks2(gcbf, ''vf_time'');');
       
       
hhelp = uimenu(fig,'Label','Help','Tag','Help');
uimenu(hhelp,'Label','Quick guide','Tag','quick_guide','Callback',...
       'mycallbacks2(gcbf, ''quick_guide'');');
uimenu(hhelp,'Label','Help','Tag','help','Callback',...
       'mycallbacks2(gcbf, ''help'');');
uimenu(hhelp,'Label','About','Tag','About', 'Callback',...
       'mycallbacks2(gcbf, ''about'');');
       
       
% FINALLY, SOME BUTTONS!!
%  htb = uitoolbar;
%  
%  imloadann = rand(16, 16, 3); % An icon 16x16 pixels, three channels (color).
%  uipushtool(htb, 'CData', imloadann, 'TooltipString', 'Load annotations ...', 'ClickedCallback', 'mycallbacks2(gcbf, ''load_ann'');');
%  
%  imsaveann = rand(16, 16, 3); % An icon 16x16 pixels, three channels (color).
%  uipushtool(htb, 'CData', imsaveann, 'TooltipString', 'Save annotations ...', 'ClickedCallback', 'mycallbacks2(gcbf, ''save_ann'');');
%  
%  imshowfiduid = rand(16, 16, 3); % An icon 16x16 pixels, three channels (color).
%  uitoggletool(htb, 'CData', imshowfiduid, 'Separator', 'on', 'TooltipString', 'Display annotation ids', 'OnCallback', 'mycallbacks2(gcbf, ''show_fiduid'');', 'OffCallback', 'mycallbacks2(gcbf, ''hide_fiduid'');');
%  
%  imsegment = rand(16, 16, 3); % An icon 16x16 pixels, three channels (color).
%  uipushtool(htb, 'CData', imsegment, 'Separator', 'on', 'TooltipString', 'Segment ...', 'ClickedCallback', 'mycallbacks2(gcbf, ''segment'');');
%  

