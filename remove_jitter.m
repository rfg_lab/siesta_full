% [out d] = remove_jitter(in, fiducials, nfiducials)
%
% Removes background movement from a time sequence (3D image) based on the position
% of fiducial markers. 

% In the case of germband extension movements, this accounts for the global tissue
% movement, but not for the morphological changes that the cells suffer due to this
% movement.
% 
% IN is the image to be corrected.
% FIDUCIALS is MxNxZ matrix, where M is the number of fiducial points, N is the
% dimensionality of the points (normally 3D) and Z is the number of time points
% in the sequence. The successive time points will be shifted the average movement
% of these fiducial points with respect to their position on the first frame.
% NFIDUCIALS is a vector with Z elements, where Z is the number of time points in the sequence,
% containing the number of fiducial points available for each time point.
%   
% OUT is the corrected image.
% D is the correction factor used in each one of the dimensions and time points
%
% Rodrigo Fernandez-Gonzalez
% fernanr1@mskcc.org
% 8/02/2007


function [out d] = remove_jitter(in, fiducials, nfiducials, cut_t)

d = zeros(size(fiducials, 1), 3);
out = dip_image(zeros(size(in, 2), size(in, 1), size(in, 3)));

% Compute XY translation necessary to remove jitter using fiducial markers.
reference = fiducials(1:nfiducials(cut_t), 1:2, cut_t);
%out(:, :, 0) = in(:, :, 0);

h = waitbar(0, cat(2, 'Registering image 1/', cat(2, num2str(size(fiducials, 3)), ' ...')), 'Color', 'w');

for z = 1:size(fiducials, 3)
	target = fiducials(1:nfiducials(z), 1:2, z);
	
	if nfiducials(z) > 0
		d(z, 1:2) = round(mean(reference - target, 1));
		% Apply translation.
		out(:, :, z-1) = shiftim(in(:, :, z-1), d(z, :));
	else
		out(:, :, z-1) = in(:, :, z-1);
	end	
        
        if z < size(fiducials, 3)
                waitbar((z)/size(fiducials, 3), h, cat(2, 'Registering image ', cat(2, cat(2, num2str(z+1), '/'), cat(2, num2str(size(fiducials, 3)), ' ...')))); 
        else
                waitbar(1, h, 'Done!'); 
        end
end

close(h);