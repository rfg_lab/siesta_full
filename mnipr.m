% [nir, nipr] = mnipr(i, p, r, dt, populations, shells)
%
% Computes the total number of neighbors of position i within (<=) or
% at (==) distance r (nir) and those belonging to population p (nipr).
% 
% I is the position/s of interest.
% P is the population of interest.
% R is the maximum distance/s to consider two positions as neighbors.
% DT is a distance table where (i, j) contains the distance between positions
% i and j.
% POPULATIONS is a vector where index i indicates the population of position i.
% SHELLS (optional) is zero for 'within' (<=r) analysis and non-zero for 'at'
% (==r) analysis.
%
%
% NIR is the total number of neighbors.
% NIPR is the number of neighbors belonging to P.
%
% Rodrigo Fernandez-Gonzalez
% fernanr1@mskcc.org
% 11/30/2006

function varargout = mnipr(varargin)

% Default value.
shells = 0;

% Check input parameters.
if (nargin < 5 || nargin > 6)
	error('??? Wrong number of input arguments.');	
end

i = varargin{1, 1};
if (~ isnumeric(i))
	error('??? First argument should be numeric.');
end

p = varargin{1, 2};
if (~ (isnumeric(p) && prod(size(p)) == 1))
	error('??? Second argument should be a number.');
end

r = varargin{1, 3};
if (~ isnumeric(r))
	error('??? Third argument should be numeric.');
end

dt = varargin{1, 4};
if (~ (isnumeric(dt) && prod(size(dt)) > 1 && size(dt, 3) == 1))
	error('??? Fourth argument should be a 2D-matrix.');
end

populations = varargin{1, 5};
if (~ ((isnumeric(populations) || islogical(populations)) && prod(size(populations)) > 1 && size(populations, 3) == 1))
	error('??? Fifth argument should be a 2D-matrix.');
end

if (nargin == 6)
	shells = varargin{1, 6};
	if (~ (isnumeric(shells) && numel(shells) == 1))
		error('??? Sixth argument should be a number.');
	end
end

% END check.

% Number of positions to analyze.
ni = prod(size(i));
i = reshape(i, [1 ni 1]);

% Prepare dt, r and populations for comparison.
[row col] = size(dt);
nr = prod(size(r));
dt = repmat(dt, [1, 1, nr]);
r = reshape(r, [1 1 nr]);
r = repmat(r, [ni col 1]);

% Remove position i from the list of positions closer than r to i.
% nir.
if (~ shells)
	varargout{1, 1} = reshape(sum(dt(i, :, :) <= r, 2) - 1, [ni nr 1]);
else
	varargout{1, 1} = reshape(sum(dt(i, :, :) == r, 2) , [ni nr 1]); % Does not need the -1, since there is no need to remove position i itself.
end

orig_pops = populations;
populations = reshape(populations, [1, col, 1]);
populations = repmat(populations, [ni, 1, nr]) ;
populations = double(populations == p);
% Obtain a matrix where all the positions in the wrong population are
% infinitely far from the one under analysis.
populations(find(populations == 0)) = Inf;

% Take care of the positions in question: they should not be counted as neighbors!
correction = repmat(double((orig_pops(i) == p)'), [1, 1, nr]);

% nipr
if (~ shells)
	varargout{1, 2} = reshape(sum(dt(i, :, :) .* populations <= r, 2) - correction, [ni nr 1]);
else
	varargout{1, 2} = reshape(sum(dt(i, :, :) .* populations == r, 2), [ni nr 1]); % Does not need the - correction, since there is no need to remove position i itself.
end

return;
