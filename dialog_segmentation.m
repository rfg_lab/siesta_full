function varargout = dialog_segmentation(varargin)
% DIALOG_SEGMENTATION M-file for dialog_segmentation.fig
%      H = DIALOG_SEGMENTATION([min max]) returns the handle to a new DIALOG_SEGMENTATION
%	   containing the parameters for the segmentation function.




%      DIALOG_SEGMENTATION, by itself, creates a new DIALOG_SEGMENTATION or raises the existing
%      singleton*.
%
%      H = DIALOG_SEGMENTATION returns the handle to a new DIALOG_SEGMENTATION or the handle to
%      the existing singleton*.
%
%      DIALOG_SEGMENTATION('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DIALOG_SEGMENTATION.M with the given input arguments.
%
%      DIALOG_SEGMENTATION('Property','Value',...) creates a new DIALOG_SEGMENTATION or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before dialog_segmentation_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to dialog_segmentation_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help dialog_segmentation

% Last Modified by GUIDE v2.5 06-Aug-2015 14:09:18

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @dialog_segmentation_OpeningFcn, ...
                   'gui_OutputFcn',  @dialog_segmentation_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before dialog_segmentation is made visible.
function dialog_segmentation_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to dialog_segmentation (see VARARGIN)

% Choose default command line output for dialog_segmentation
handles.output = hObject;


% Parameter values for components. RODRI EDITED HERE.
% THIS HAS TO BE BEFORE guidata(hObject, handles)!!!!
set(handles.figure1, 'name', 'Segment');
warning('off', 'all'); % This is necessary to remove a warning
					   % caused by a Matlab bug.
ud = varargin{1};
only_seeds = varargin{2};
timepoints = [0 (ud.imsize(3) - 1)]; % This variable can represent time points, z slices, ...
tps = timepoints(1):timepoints(2);
handles.tps = tps;
handles.angles = (-180:180);
handles.closingsizes = [1 2 4 8 16 32 64 128 256 512 1024]; % This variable represents a window size that contains enough information for PIV, adaptive threshold, etc.
handles.closemethod = -1;

str = [];
for i = 1:numel(tps)
	str = cat(2, str, cat(2, num2str(tps(i)), '|'));
end
str = str(1:(end-1));

str_angles = [];
for i = 1:numel(handles.angles)
	str_angles = cat(2, str_angles, cat(2, num2str(handles.angles(i)), '|'));
end
str_angles = str_angles(1:(end-1));


str_closings = [];
for i = 1:numel(handles.closingsizes)
	str_closings = cat(2, str_closings, cat(2, num2str(handles.closingsizes(i)), '|'));
end
str_closings = str_closings(1:(end-1));

set(handles.popupmenu1, 'String', str, 'Value', ud.curslice+1);
set(handles.popupmenu2, 'String', str, 'Value', ud.curslice+1);
set(handles.popupmenu3, 'String', str_angles, 'Value', ceil(numel(handles.angles)/2));
set(handles.popupmenu4, 'String', str_closings, 'Value', find(handles.closingsizes==ud.paramclosing));
set(handles.edit1, 'String', sprintf('%.4f', ud.rres(1)));
set(handles.edit2, 'String', sprintf('%.4f', ud.rres(2)));
set(handles.edit3, 'String', sprintf('%.4f', ud.rres(3)));
set(handles.edit4, 'String', sprintf('%.4f', ud.parammindist));
set(handles.edit5, 'String', sprintf('%.4f', ud.paramgaussfactor));

radioButtons = get(handles.uipanel1, 'children');
twoDButton = findobj(radioButtons, 'String', '2D');
threeDButton = findobj(radioButtons, 'String', '3D');

set(threeDButton, 'Enable', 'off', 'Value', 0);
set(twoDButton, 'Enable', 'on', 'Value', 1);

% The button to track divisions is only enabled when segmenting
% cells, not when looking for seeds only.
if only_seeds == 1
    set(handles.segDivision, 'Visible', 'off', 'Enable', 'off', 'Value', 0)
end
% Update handles structure.
guidata(hObject, handles);


% UIWAIT makes dialog_segmentation wait for user response (see UIRESUME)
uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = dialog_segmentation_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
%varargout{1} = handles.output;
%get(handles.figure1)
%uiwait(handles.figure1);

try
    d = guidata(handles.figure1);
    if d.closemethod == 1
        tp(1) = handles.tps(get(handles.popupmenu1, 'Value'));
        tp(2) = handles.tps(get(handles.popupmenu2, 'Value'));
        tp = sort(tp);
        xres = eval(get(handles.edit1, 'String'));
        yres = eval(get(handles.edit2, 'String'));
        zres = eval(get(handles.edit3, 'String'));
        ang = handles.angles(get(handles.popupmenu3, 'Value'));
        closingsize = handles.closingsizes(get(handles.popupmenu4, 'Value'));
        
        radioButtons = get(handles.uipanel1, 'children');
        twoDButton = findobj(radioButtons, 'String', '2D');
        
        twoD = get(twoDButton, 'Value');
        min_dist = eval(get(handles.edit4, 'String'));
        gauss_factor = eval(get(handles.edit5, 'String'));
        %michael
        areaRatioNum=str2double(get(handles.areaRatio,'String'));
        circleMinNum=str2double(get(handles.circleMin,'String'));
        circleMaxNum=str2double(get(handles.circleMax,'String'));
        numTimePoints=str2double(get(handles.numTime,'String'));
        seedDistNum=str2double(get(handles.seedDist,'String'));
        dumbbellCondNum=str2double(get(handles.dumbbellCond,'String'));
        minArea=str2double(get(handles.minArea,'String'));
        mgradient=get(handles.gradSeg,'Value');
        segDiv=get(handles.segDivision,'Value');
        divSettings=[areaRatioNum,circleMinNum,circleMaxNum,numTimePoints,seedDistNum,dumbbellCondNum,mgradient,minArea,segDiv];
        %michael
    else
        tp = [-1 -1];
        xres = -1;
        yres = -1;
        zres = -1;
        ang = 0;
        closingsize = 1;
        twoD = -1;
        min_dist = -1;
        gauss_factor = -1;
        divSettings = -1;
    end
    close(handles.figure1);
catch
    tp = [-1 -1];
    xres = -1;
    yres = -1;
    zres = -1;
    ang = 0;
    closingsize = 1;
    twoD = -1;
    min_dist = -1;
    gauss_factor = -1;
    divSettings = -1;
end
	
varargout{1} = (tp(1):tp(2));
varargout{2} = [xres yres zres];
varargout{3} = ang;
varargout{4} = twoD;
varargout{5} = min_dist;
varargout{6} = gauss_factor;
varargout{7} = closingsize;
%michael
varargout{8} = divSettings;
%michael


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1

selected = get(handles.popupmenu1,'Value');
prev_str = get(handles.popupmenu1, 'String');
set(handles.popupmenu1, 'String', prev_str, 'Value', selected);
radioButtons = get(handles.uipanel1, 'children');
twoDButton = findobj(radioButtons, 'String', '2D');
threeDButton = findobj(radioButtons, 'String', '3D');

% If a single slide is selected, enable only 2D segmentation.
if selected == get(handles.popupmenu2, 'Value')
	set(threeDButton, 'Enable', 'off', 'Value', 0);
	set(twoDButton, 'Enable', 'on', 'Value', 1);
else
	set(threeDButton, 'Enable', 'on');
	set(twoDButton, 'Enable', 'on');	
end

% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2
selected = get(handles.popupmenu2,'Value');
prev_str = get(handles.popupmenu2, 'String');
set(handles.popupmenu2, 'String', prev_str, 'Value', selected);

radioButtons = get(handles.uipanel1, 'children');
twoDButton = findobj(radioButtons, 'String', '2D');
threeDButton = findobj(radioButtons, 'String', '3D');

% If a single slide is selected, enable only 2D segmentation.
if selected == get(handles.popupmenu1, 'Value')
	set(threeDButton, 'Enable', 'off', 'Value', 0);
	set(twoDButton, 'Enable', 'on', 'Value', 1);
else
	set(threeDButton, 'Enable', 'on');
	set(twoDButton, 'Enable', 'on');	
end


% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%set(handles.figure1, 'Visible', 'off');
handles.closemethod = 1;
guidata(handles.figure1, handles);
uiresume(handles.figure1);


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.closemethod = 0;
guidata(handles.figure1, handles);
uiresume(handles.figure1);


function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu3.
function popupmenu3_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu3


% --- Executes during object creation, after setting all properties.
function popupmenu3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)





function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end





function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




% --- Executes on selection change in popupmenu4.
function popupmenu4_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu4 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu4

% --- Executes during object creation, after setting all properties.
function popupmenu4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




% --- Executes on button press in trackDivButton.
function trackDivButton_Callback(hObject, eventdata, handles)
% hObject    handle to trackDivButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%Michael
[a,b,c,d,e,f]=dialog_trackDiv;
divSettings=[a,b,c,d,e,f];
%set(handles.popupmenu1, 'String', str, 'Value', ud.curslice+1);
%guidata(hObject,divSettings);

%Michael



function areaRatio_Callback(hObject, eventdata, handles)
% hObject    handle to areaRatio (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of areaRatio as text
%        str2double(get(hObject,'String')) returns contents of areaRatio as a double


% --- Executes during object creation, after setting all properties.
function areaRatio_CreateFcn(hObject, eventdata, handles)
% hObject    handle to areaRatio (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in areaRatioButton.
function areaRatioButton_Callback(hObject, eventdata, handles)
% hObject    handle to areaRatioButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
message=('The ratio between the current timepoint cell area  versus the minimum cell area. This value would be the area increase threshold value.');
msgbox(message,'Help Menu');



function circleMin_Callback(hObject, eventdata, handles)
% hObject    handle to circleMin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of circleMin as text
%        str2double(get(hObject,'String')) returns contents of circleMin as a double


% --- Executes during object creation, after setting all properties.
function circleMin_CreateFcn(hObject, eventdata, handles)
% hObject    handle to circleMin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function circleMax_Callback(hObject, eventdata, handles)
% hObject    handle to circleMax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of circleMax as text
%        str2double(get(hObject,'String')) returns contents of circleMax as a double


% --- Executes during object creation, after setting all properties.
function circleMax_CreateFcn(hObject, eventdata, handles)
% hObject    handle to circleMax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in circleButton.
function circleButton_Callback(hObject, eventdata, handles)
% hObject    handle to circleButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
message2=('Set the minimum and maximum circularity thresholds.');
msgbox(message2,'Help Menu');


% --- Executes on button press in pushbutton7.
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
message3=('Set the minimum number of time points to analyze before determining when a cell divides.');
msgbox(message3,'Help Menu');



function numTime_Callback(hObject, eventdata, handles)
% hObject    handle to numTime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of numTime as text
%        str2double(get(hObject,'String')) returns contents of numTime as a double


% --- Executes during object creation, after setting all properties.
function numTime_CreateFcn(hObject, eventdata, handles)
% hObject    handle to numTime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton8.
function pushbutton8_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
message4=('Set the distance along the longest axis of the parent cell for which the daughter seeds will be split. For example, a value of 1 makes the daugther the entire axis away from the parent centroid.');
msgbox(message4,'Help Menu');



function seedDist_Callback(hObject, eventdata, handles)
% hObject    handle to seedDist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of seedDist as text
%        str2double(get(hObject,'String')) returns contents of seedDist as a double


% --- Executes during object creation, after setting all properties.
function seedDist_CreateFcn(hObject, eventdata, handles)
% hObject    handle to seedDist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton9.
function pushbutton9_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
message5=('Set the ratio between the minimum distance measure compared to the maximum distance measure to determine the dumbbell shape of the dividing cell.');
msgbox(message5,'Help Menu');



function dumbbellCond_Callback(hObject, eventdata, handles)
% hObject    handle to dumbbellCond (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of dumbbellCond as text
%        str2double(get(hObject,'String')) returns contents of dumbbellCond as a double


% --- Executes during object creation, after setting all properties.
function dumbbellCond_CreateFcn(hObject, eventdata, handles)
% hObject    handle to dumbbellCond (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in gradSeg.
function gradSeg_Callback(hObject, eventdata, handles)
% hObject    handle to gradSeg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of gradSeg


% --- Executes on button press in pushbutton10.
function pushbutton10_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
message5=('Dividing cell should be x times greater than the average of cell areas. Default is x=1.0 which means that dividing cell must be greater than 1.0 times the average area of all cells.');
msgbox(message5,'Help Menu');


function minArea_Callback(hObject, eventdata, handles)
% hObject    handle to minArea (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of minArea as text
%        str2double(get(hObject,'String')) returns contents of minArea as a double


% --- Executes during object creation, after setting all properties.
function minArea_CreateFcn(hObject, eventdata, handles)
% hObject    handle to minArea (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in segDivision.
function segDivision_Callback(hObject, eventdata, handles)
% hObject    handle to segDivision (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of segDivision
set(handles.figure1, 'Units', 'pixels');
pos = get(handles.figure1, 'Position');
if get(handles.segDivision, 'Value') 
    set(handles.figure1, 'Position', [pos(1), pos(2), 868, pos(4)]);
else
    set(handles.figure1, 'Position', [pos(1), pos(2), 450, pos(4)]);
end
    
 
