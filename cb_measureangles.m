% result = cb_measureangles(fig)
function result = cb_measureangles(fig)

ud = get(fig, 'UserData');

extensions = {'*.csv'; '*.mat'; '*.*'};
[filename, pathname, ext] = uiputfile(extensions, 'Save angle measurements');

angles = ud.rangles;

result = [];

% For every Z (or T) plane.
for ii = 1:size(angles, 3)
    % Go through the drawn angles.
    for jj = 1:numel(angles(:, :, ii))
        theta = angles(jj, 1, ii);
        theta = theta{1};
        
        if isempty(theta)
            continue;
        end
        
        % Measure the angle.
        theta = measure_angle(theta(1, :), theta(2, :), theta(3, :));
        
        result(end+1, 1:2) = [(ii-1) theta];
    end
end

if (~ isequal(filename, 0))
    [tmp1 tmp2 theext] = fileparts(filename);
    theext = cat(2, '*', theext);
    
    % If the extension is written, use that mode.
    if (~ isempty(theext))
        for ie = 1:numel(extensions)
            if strcmp(extensions{ie}, theext)
                ext = ie;
                break;
            end
        end
    end
    
    
    if (strcmp(extensions{ext}, '*.csv'))
        %                  dlmwrite(cat(2, pathname, filename), ['I', 'A']);
        %                  dlmwrite(cat(2, pathname, filename), result, '-append', 'roffset', 0, 'coffset', 0);
        savecsv(cat(2, pathname, filename), 'Time point', result(:, 1)', 'Angle', result(:, 2)');
    else
        save(cat(2, pathname, filename), 'result');
    end
end
end
