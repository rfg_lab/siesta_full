% Called by vector_field_analysis.

function vector_field_plots(dr_um, dth_um, cut_p1_um, cut_p1_rot_angle_deg, title_label, save_flag, theshift, the_time_lag)
% theshift is the angle of the cut interface with respect to the AP axis: 90 for AP interfaces, 0 for DV interfaces.
if nargin < 7
	theshift = 0.0;
end

if nargin < 8
        the_time_lag = 1;
end        

bgcolor = [0.8 0.8 0.8];

% dr_um and dth_um can now be subgrouped on the basis of the
% distance from the vertex to the cut given by cut_p1_um
% (0-4um, 4-8, ..., 16-20) or their angle with respect to the cut 
% given by cut_p1_rot_angle (0-30deg, ..., 330-360).
d04 = find(cut_p1_um >= 0 & cut_p1_um < 4);
d48 = find(cut_p1_um >= 4 & cut_p1_um < 8);
d812 = find(cut_p1_um >= 8 & cut_p1_um < 12);
d1216 = find(cut_p1_um >= 12 & cut_p1_um < 16);
d1620 = find(cut_p1_um >= 16 & cut_p1_um < 20);
cm = [1 0 0; 1 0.5 0; 0 1 0; 0 1 1; 0 0 1]; % Color map for plots.

a030 = find(cut_p1_rot_angle_deg >= 0 & cut_p1_rot_angle_deg < 30);
a3060 = find(cut_p1_rot_angle_deg >= 30 & cut_p1_rot_angle_deg < 60);
a6090 = find(cut_p1_rot_angle_deg >= 60 & cut_p1_rot_angle_deg < 90);
a90120 = find(cut_p1_rot_angle_deg >= 90 & cut_p1_rot_angle_deg < 120);
a120150 = find(cut_p1_rot_angle_deg >= 120 & cut_p1_rot_angle_deg < 150);
a150180 = find(cut_p1_rot_angle_deg >= 150 & cut_p1_rot_angle_deg < 180);
a180210 = find(cut_p1_rot_angle_deg >= 180 & cut_p1_rot_angle_deg < 210);
a210240 = find(cut_p1_rot_angle_deg >= 210 & cut_p1_rot_angle_deg < 240);
a240270 = find(cut_p1_rot_angle_deg >= 240 & cut_p1_rot_angle_deg < 270);
a270300 = find(cut_p1_rot_angle_deg >= 270 & cut_p1_rot_angle_deg < 300);
a300330 = find(cut_p1_rot_angle_deg >= 300 & cut_p1_rot_angle_deg < 330);
a330360 = find(cut_p1_rot_angle_deg >= 330 & cut_p1_rot_angle_deg < 360);


% PLOTS
% dr vs distance -----------------------------------
figure;
h = plot(cut_p1_um, dr_um);
set(h, 'Color', cm(5, :), 'Marker', 'o', 'LineStyle', 'none', 'MarkerFaceColor', cm(5, :));
hold on;

sigmas = nlinfit([cut_p1_um cut_p1_rot_angle_deg*pi/180], dr_um, @myholedrillingfunction1, [1 1])
sigma_xy = nlinfit(cut_p1_um, dr_um, @myholedrillingfunction2, [1])

x = xlim;
x = (x(1)+2):.1:(x(2)-2);
[u epsilon_0] = myholedrillingfunction2(sigma_xy, x);
epsilon_0
h = plot(x, u);
set(h, 'Color', [237 34 36]./255, 'LineWidth', 2.5, 'LineStyle', '-');
%set(h, 'Color', cm(1, :), 'Marker', 'o', 'LineStyle', 'none', 'MarkerFaceColor', cm(1, :));

%xlim([0 360]);
ylimits = [-2 5]; %[-5 10];
ylim(ylimits);
set(gca, 'YTick', (ylimits(1):ylimits(2)), 'XGrid', 'off', 'YGrid', 'off', 'FontSize', 16, 'LineWidth', 2);
xlabel('distance from ablation site (\mum)', 'FontSize', 16);
ylabel('radial displacement (\mum)', 'FontSize', 16);
title(title_label);

if (save_flag)
        savefig(cat(2, 'dr_dist_dots', rstrtrim(title_label)), gcf, 'jpeg');
end
%figure; theangles = (0:(1*pi/180):(2*pi)); mmpolar(theangles, myholedrillingfunction1(sigmas, [ones(numel(theangles), 1) theangles']));



% radial velocity vs distance -----------------------------------
figure;
h = plot(cut_p1_um, dr_um./the_time_lag);
set(h, 'Color', cm(5, :), 'Marker', 'o', 'LineStyle', 'none', 'MarkerFaceColor', cm(5, :));
hold on;

%xlim([0 360]);
ylimits = [-0.3 2]; %[-5 10];
ylim(ylimits);
set(gca, 'YTick', (ylimits(1):0.2:ylimits(2)), 'XGrid', 'off', 'YGrid', 'off', 'FontSize', 16, 'LineWidth', 2);
xlabel('distance from ablation site (\mum)', 'FontSize', 16);
ylabel('radial velocity (\mum/s)', 'FontSize', 16);
title(title_label);

if (save_flag)
        savefig(cat(2, 'vr_dist_dots', rstrtrim(title_label)), gcf, 'jpeg');
end






% dr vs angle (coded by distance) wrt AP -----------------------------------
figure;
cut_p1_rot_angle_deg = mod(cut_p1_rot_angle_deg+theshift, 360);
h = zeros(1, 5);
if ~isempty(d1620)
        h(5) = plot(cut_p1_rot_angle_deg(d1620), dr_um(d1620));
        set(h(5), 'Color', cm(5, :), 'Marker', 'o', 'LineStyle', 'none', 'MarkerFaceColor', cm(5, :));
        set(gca, 'Color', bgcolor);
end        
hold on;
if ~isempty(d1216)
        h(4) = plot(cut_p1_rot_angle_deg(d1216), dr_um(d1216));
        set(h(4), 'Color', cm(4, :), 'Marker', 'o', 'LineStyle', 'none', 'MarkerFaceColor', cm(4, :));
end        
if ~isempty(d812)
        h(3) = plot(cut_p1_rot_angle_deg(d812), dr_um(d812));
        set(h(3), 'Color', cm(3, :), 'Marker', 'o', 'LineStyle', 'none', 'MarkerFaceColor', cm(3, :));
end
if ~isempty(d48)
        h(2) = plot(cut_p1_rot_angle_deg(d48), dr_um(d48));
        set(h(2), 'Color', cm(2, :), 'Marker', 'o', 'LineStyle', 'none', 'MarkerFaceColor', cm(2, :));
end     
if ~isempty(d04)
        h(1) = plot(cut_p1_rot_angle_deg(d04), dr_um(d04));
        set(h(1), 'Color', cm(1, :), 'Marker', 'o', 'LineStyle', 'none', 'MarkerFaceColor', cm(1, :));
end

%h = legend(h, {'[0-4) \mum'; '[4-8) \mum'; '[8-12) \mum'; '[12-16) \mum'; '[16-20) \mum';}, 'Location', 'ne');
%set(h, 'Color', bgcolor);
xlim([0 360]);
ylimits = [-3 5]; %[-5 10];
ylim(ylimits);
set(gca, 'XTick', [0:30:330], 'YTick', (ylimits(1):ylimits(2)), 'XGrid', 'off', 'YGrid', 'off', 'FontSize', 16, 'LineWidth', 2, 'XTickLabel', {'0', '', '', '90', '', '', '180', '', '', '270', '', ''});
xlabel('Angle with respect to AP axis (degrees)', 'FontSize', 16);
ylabel('{\itd_r} (\mum)', 'FontSize', 16);
title(title_label);

if (save_flag)
	savefig(cat(2, 'dr_angle_dots_wrtAP', rstrtrim(title_label)), gcf, 'jpeg');
end

% dth vs angle (coded by distance) wrt AP -----------------------------------
figure;
cut_p1_rot_angle_deg = mod(cut_p1_rot_angle_deg+theshift, 360);
h = zeros(1, 5);
if ~isempty(d1620)
        h(5) = plot(cut_p1_rot_angle_deg(d1620), dth_um(d1620));
        set(h(5), 'Color', cm(5, :), 'Marker', 'o', 'LineStyle', 'none', 'MarkerFaceColor', cm(5, :));
        set(gca, 'Color', bgcolor);
end        
hold on;
if ~isempty(d1216)
        h(4) = plot(cut_p1_rot_angle_deg(d1216), dth_um(d1216));
        set(h(4), 'Color', cm(4, :), 'Marker', 'o', 'LineStyle', 'none', 'MarkerFaceColor', cm(4, :));
end        
if ~isempty(d812)
        h(3) = plot(cut_p1_rot_angle_deg(d812), dth_um(d812));
        set(h(3), 'Color', cm(3, :), 'Marker', 'o', 'LineStyle', 'none', 'MarkerFaceColor', cm(3, :));
end
if ~isempty(d48)
        h(2) = plot(cut_p1_rot_angle_deg(d48), dth_um(d48));
        set(h(2), 'Color', cm(2, :), 'Marker', 'o', 'LineStyle', 'none', 'MarkerFaceColor', cm(2, :));
end     
if ~isempty(d04)
        h(1) = plot(cut_p1_rot_angle_deg(d04), dth_um(d04));
        set(h(1), 'Color', cm(1, :), 'Marker', 'o', 'LineStyle', 'none', 'MarkerFaceColor', cm(1, :));
end

%h = legend(h, {'[0-4) \mum'; '[4-8) \mum'; '[8-12) \mum'; '[12-16) \mum'; '[16-20) \mum';}, 'Location', 'ne');
%set(h, 'Color', bgcolor);
xlim([0 360]);
ylimits = [-3 5]; %[-5 10];
ylim(ylimits);
set(gca, 'XTick', [0:30:330], 'YTick', (ylimits(1):ylimits(2)), 'XGrid', 'off', 'YGrid', 'off', 'FontSize', 16, 'LineWidth', 2, 'XTickLabel', {'0', '', '', '90', '', '', '180', '', '', '270', '', ''});
xlabel('Angle with respect to AP axis (degrees)', 'FontSize', 16);
ylabel('{\itd_\theta} (\mum)', 'FontSize', 16);
h = legend(h, {'[0-4) \mum'; '[4-8) \mum'; '[8-12) \mum'; '[12-16) \mum'; '[16-20) \mum';}, 'Location', 'ne');
title(title_label);

if (save_flag)
        savefig(cat(2, 'dth_angle_dots_wrtAP', rstrtrim(title_label)), gcf, 'jpeg');
end



% dr vs angle (coded by distance) -------------------------------------
figure;
cut_p1_rot_angle_deg = mod(cut_p1_rot_angle_deg-theshift, 360);
h = zeros(1, 5);
h(5) = plot(cut_p1_rot_angle_deg(d1620), dr_um(d1620));
set(h(5), 'Color', cm(5, :), 'Marker', 'o', 'LineStyle', 'none', 'MarkerFaceColor', cm(5, :));
set(gca, 'Color', bgcolor);
hold on;
h(4) = plot(cut_p1_rot_angle_deg(d1216), dr_um(d1216));
set(h(4), 'Color', cm(4, :), 'Marker', 'o', 'LineStyle', 'none', 'MarkerFaceColor', cm(4, :));
h(3) = plot(cut_p1_rot_angle_deg(d812), dr_um(d812));
set(h(3), 'Color', cm(3, :), 'Marker', 'o', 'LineStyle', 'none', 'MarkerFaceColor', cm(3, :));
h(2) = plot(cut_p1_rot_angle_deg(d48), dr_um(d48));
set(h(2), 'Color', cm(2, :), 'Marker', 'o', 'LineStyle', 'none', 'MarkerFaceColor', cm(2, :));
h(1) = plot(cut_p1_rot_angle_deg(d04), dr_um(d04));
set(h(1), 'Color', cm(1, :), 'Marker', 'o', 'LineStyle', 'none', 'MarkerFaceColor', cm(1, :));

h = legend(h, {'[0-4) \mum'; '[4-8) \mum'; '[8-12) \mum'; '[12-16) \mum'; '[16-20) \mum';}, 'Location', 'ne');
set(h, 'Color', bgcolor);
xlim([0 360]);
ylimits = [-3 5]; %[-5 10];
ylim(ylimits);
set(gca, 'XTick', [0:30:360], 'YTick', (ylimits(1):ylimits(2)), 'XGrid', 'off', 'YGrid', 'off', 'FontSize', 12, 'LineWidth', 2);
xlabel('Angle with respect to severed interface (degrees)', 'FontSize', 16);
ylabel('{\itd_r} (\mum)', 'FontSize', 16);
title(title_label);

if (save_flag)
	savefig(cat(2, 'dr_angle_dots', rstrtrim(title_label)), gcf, 'jpeg');
end


% dtheta vs angle (coded by distance) ---------------------------------
figure;
h = zeros(1, 5);
h(5) = plot(cut_p1_rot_angle_deg(d1620), dth_um(d1620));
set(h(5), 'Color', cm(5, :), 'Marker', 'o', 'LineStyle', 'none', 'MarkerFaceColor', cm(5, :));
set(gca, 'Color', bgcolor);
hold on;
h(4) = plot(cut_p1_rot_angle_deg(d1216), dth_um(d1216));
set(h(4), 'Color', cm(4, :), 'Marker', 'o', 'LineStyle', 'none', 'MarkerFaceColor', cm(4, :));
h(3) = plot(cut_p1_rot_angle_deg(d812), dth_um(d812));
set(h(3), 'Color', cm(3, :), 'Marker', 'o', 'LineStyle', 'none', 'MarkerFaceColor', cm(3, :));
h(2) = plot(cut_p1_rot_angle_deg(d48), dth_um(d48));
set(h(2), 'Color', cm(2, :), 'Marker', 'o', 'LineStyle', 'none', 'MarkerFaceColor', cm(2, :));
h(1) = plot(cut_p1_rot_angle_deg(d04), dth_um(d04));
set(h(1), 'Color', cm(1, :), 'Marker', 'o', 'LineStyle', 'none', 'MarkerFaceColor', cm(1, :));

h = legend(h, {'[0-4) \mum'; '[4-8) \mum'; '[8-12) \mum'; '[12-16) \mum'; '[16-20) \mum';}, 'Location', 'Best');
set(h, 'Color', bgcolor);
xlim([0 360]);
ylim([-3 5]); %ylim([-5 5]);
set(gca, 'XTick', [0:30:360], 'YTick', (ylimits(1):ylimits(2)), 'XGrid', 'off', 'YGrid', 'off', 'FontSize', 12, 'LineWidth', 2);
xlabel('Angle with respect to severed interface (degrees)', 'FontSize', 16);
ylabel('{\itd_{\theta}} (\mum)', 'FontSize', 16);
title(title_label);

if (save_flag)
	savefig(cat(2, 'dth_angle_dots', rstrtrim(title_label)), gcf, 'jpeg');
end

% Mean dr over distance from cut -------------------------------------
figure;
m = [mean(dr_um(d04)); mean(dr_um(d48)); mean(dr_um(d812)); mean(dr_um(d1216)); mean(dr_um(d1620))];
s = [std(dr_um(d04)); std(dr_um(d48)); std(dr_um(d812)); std(dr_um(d1216)); std(dr_um(d1620))];
%e = s./ sqrt(numel(dr_um));
e = s;

h = errorbar((1:5), m, e);
set(h, 'Color', 'b', 'Marker', 's', 'LineStyle', '-', 'LineWidth', 2);
hold on;

%h = legend(gca, {'[0-4) \mum'; '[4-8) \mum'; '[8-12) \mum'; '[12-16) \mum'; '[16-20) \mum';}, 'Location', 'Best');
%set(h, 'Color', bgcolor);
%xlim([0 360]);
ylimits = [-2 3]; %[-2 10];
ylim(ylimits);
set(gca, 'YTick', (ylimits(1):ylimits(2)), 'XGrid', 'off', 'YGrid', 'off', 'FontSize', 12);
set(gca, 'XTick', (0.5:1:5.5), 'XTickLabel', {'0'; '4'; '8'; '12'; '16'; '20'}, 'LineWidth', 2);
xlabel('Distance from cut (\mum)', 'FontSize', 16);
ylabel('Mean {\itd_r} (\mum)', 'FontSize', 16);
title(title_label);

if (save_flag)
	savefig(cat(2, 'dr_distance_mean', rstrtrim(title_label)), gcf, 'jpeg');
end


% Mean dr over angle with respect to cut -------------------------------------
figure;
m = [mean(dr_um(a030)); mean(dr_um(a3060)); mean(dr_um(a6090)); mean(dr_um(a90120)); mean(dr_um(a120150)); mean(dr_um(a150180)); mean(dr_um(a180210)); mean(dr_um(a210240)); mean(dr_um(a240270)); mean(dr_um(a270300)); mean(dr_um(a300330)); mean(dr_um(a330360))];
s = [std(dr_um(a030)); std(dr_um(a3060)); std(dr_um(a6090)); std(dr_um(a90120)); std(dr_um(a120150)); std(dr_um(a150180)); std(dr_um(a180210)); std(dr_um(a210240)); std(dr_um(a240270)); std(dr_um(a270300)); std(dr_um(a300330)); std(dr_um(a330360))];
%e = s./ sqrt(numel(dr_um));
e = s;

h = errorbar((1:12), m, e);
set(h, 'Color', 'b', 'Marker', 's', 'LineStyle', '-', 'LineWidth', 2);
hold on;

%h = legend(gca, {'[0-4) \mum'; '[4-8) \mum'; '[8-12) \mum'; '[12-16) \mum'; '[16-20) \mum';}, 'Location', 'Best');
%set(h, 'Color', bgcolor);
%xlim([0 360]);
ylimits = [-2 3]; %[-2 10];
ylim(ylimits);
xlim([0 13]);
set(gca, 'XTick', (0.5:1:12.5), 'YTick', (ylimits(1):ylimits(2)), 'XGrid', 'off', 'YGrid', 'off', 'FontSize', 12);
set(gca, 'XTickLabel', {'0'; '30'; '60'; '90'; '120'; '150'; '180'; '210'; '240'; '270'; '300'; '330'; '360'}, 'LineWidth', 2);
xlabel('Angle with respect to severed interface (degrees)', 'FontSize', 16);
ylabel('Mean {\itd_r} (\mum)', 'FontSize', 16);
title(title_label);

if (save_flag)
	savefig(cat(2, 'dr_angle_mean', rstrtrim(title_label)), gcf, 'jpeg');
end


% Mean dth over distance from cut -------------------------------------
figure;
m = [mean(dth_um(d04)); mean(dth_um(d48)); mean(dth_um(d812)); mean(dth_um(d1216)); mean(dth_um(d1620))];
s = [std(dth_um(d04)); std(dth_um(d48)); std(dth_um(d812)); std(dth_um(d1216)); std(dth_um(d1620))];
%e = s./ sqrt(numel(dr_um));
e = s;

h = errorbar((1:5), m, e);
set(h, 'Color', 'b', 'Marker', 's', 'LineStyle', '-', 'LineWidth', 2);
hold on;

%h = legend(gca, {'[0-4) \mum'; '[4-8) \mum'; '[8-12) \mum'; '[12-16) \mum'; '[16-20) \mum';}, 'Location', 'Best');
%set(h, 'Color', bgcolor);
%xlim([0 360]);
ylimits = [-2 3]; %[-3 3];
ylim(ylimits);
set(gca, 'YTick', (ylimits(1):ylimits(2)), 'XGrid', 'off', 'YGrid', 'off', 'FontSize', 12);
set(gca, 'XTick', (0.5:1:5.5), 'XTickLabel', {'0'; '4'; '8'; '12'; '16'; '20'}, 'LineWidth', 2);
xlabel('Distance from cut (\mum)', 'FontSize', 16);
ylabel('Mean {\itd_{\theta}} (\mum)', 'FontSize', 16);
title(title_label);

if (save_flag)
	savefig(cat(2, 'dth_distance_mean', rstrtrim(title_label)), gcf, 'jpeg');
end


% Mean dth over angle with respect to cut -------------------------------------
figure;
m = [mean(dth_um(a030)); mean(dth_um(a3060)); mean(dth_um(a6090)); mean(dth_um(a90120)); mean(dth_um(a120150)); mean(dth_um(a150180)); mean(dth_um(a180210)); mean(dth_um(a210240)); mean(dth_um(a240270)); mean(dth_um(a270300)); mean(dth_um(a300330)); mean(dth_um(a330360))];
s = [std(dth_um(a030)); std(dth_um(a3060)); std(dth_um(a6090)); std(dth_um(a90120)); std(dth_um(a120150)); std(dth_um(a150180)); std(dth_um(a180210)); std(dth_um(a210240)); std(dth_um(a240270)); std(dth_um(a270300)); std(dth_um(a300330)); std(dth_um(a330360))];
%e = s./ sqrt(numel(dr_um));
e = s;

%bar((1:12), m);hold on;
h = errorbar((1:12), m, e);
set(h, 'Color', 'b', 'Marker', 's', 'LineStyle', '-', 'LineWidth', 2);
hold on;

%h = legend(gca, {'[0-4) \mum'; '[4-8) \mum'; '[8-12) \mum'; '[12-16) \mum'; '[16-20) \mum';}, 'Location', 'Best');
%set(h, 'Color', bgcolor);
%xlim([0 360]);
ylimits = [-2 3]; %[-3 3];
ylim(ylimits);
xlim([0 13]);
set(gca, 'XTick', (0.5:1:12.5), 'YTick', (ylimits(1):ylimits(2)), 'XGrid', 'off', 'YGrid', 'off', 'FontSize', 12);
set(gca, 'XTickLabel', {'0'; '30'; '60'; '90'; '120'; '150'; '180'; '210'; '240'; '270'; '300'; '330'; '360'}, 'LineWidth', 2);
xlabel('Angle with respect to severed interface (degrees)', 'FontSize', 16);
ylabel('Mean {\itd_{\theta}} (\mum)', 'FontSize', 16);
title(title_label);

if (save_flag)
	savefig(cat(2, 'dth_angle_mean', rstrtrim(title_label)), gcf, 'jpeg');
end


