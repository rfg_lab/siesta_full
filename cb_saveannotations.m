% *********************************************************************************
function ud2 = cb_saveannotations(ud)

ud2 = ud;
curr_dir = pwd;
cd(ud.rcurrdir);

[filename, pathname, ext] = uiputfile('*.mat', 'Save annotations');

cd(curr_dir);
if (~ isequal(filename, 0))
    % Store lines.
    thelinemasks = {};
    if isfield(ud, 'rlns')
        for ii = 1:numel(ud.rlns)
            thelinemasks{ii} = get(ud.rlns(ii));
        end
    end
    
    % Anti-Angeline block of code to prevent the annotation file
    % from storing the entire figure.
    if isfield(ud, 'zoom')
        ud.zoom = [];
    end
    
    if isfield(ud, 'pan')
        ud.pan = [];
    end
    % End Anti-Angeline block of code.
   
    
    if (~ isequal(filename, 0))
        % Do not store the images in ud.
        sl = ud.slices;
        ud.slices = []; 
        if isfield(ud, 'imagedata')
            ud.imagedata = [];
        else
            ud.colordata = [];
        end
        
        % Fix ud.rntrajectories:
        ud.rntrajectories = ud.rntrajectories(:, :, 1);
        
        save(cat(2, pathname, filename), 'ud', 'thelinemasks');
        
        % No need to restore everything modified in ud, as we are
        % returning ud2=ud.
    end
end
end
