% *********************************************************************************

function cb_drawfiduciallabel(fig, coords, thelabel, d, color, thetag)

ax = get(fig, 'CurrentAxes');
hold(ax, 'on');
t = text(coords(1), coords(2), num2str(thelabel)); 
set(t, 'Color', color(1), 'FontSize', d, 'Tag', thetag);
end