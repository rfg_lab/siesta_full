% cancel = cb_exportpolygonsroi(fig)
% cancel is 1 if the operation was canceled, 0 otherwise.
function cancel = cb_exportpolygonsroi(fig)

roi_extensions = {'*.tif'; '*.ics'; '*.mat'};

ud = get(fig, 'UserData');

curr_dir = pwd;
cd(ud.rcurrdir);
curr_tp = ud.curslice;

[filename, pathname, ext] = uiputfile(roi_extensions, 'Export polygons as roi ...');
cd(curr_dir);
if (~ isequal(filename, 0))
	theroi = newim(ud.imagedata, 'bin'); % Create ROI image.

	for ii = 1:numel(ud.rpolygons(:, :, ud.curslice+1))
		thepoly = ud.rpolygons(ii, :, ud.curslice+1);

		if ~ isempty(thepoly{1})
			theroi = theroi | trajectories2mask(thepoly, size(theroi), 1, 1);
		end
	end

     switch ext
		case 1
			format = 'tif';
                        % 8bit.
                        % Since we are going to write in "append" mode, delete the file if it already exists.
                        if exist(cat(2, pathname, filename), 'file')
                                delete(cat(2, pathname, filename)); 
                        end
                           
                        thematrix = double(theroi);
                        if max(thematrix(:)) < 256
                            cast_fn = @uint16; % This prevents writing a 64bit tiff.
                        else
                            cast_fn = @uint8;
                        end% thematrix(:) arranges all pixels along a single dimension.

                        if ndims(theroi) < 3
                            theroi = reshape(theroi, [size(theroi, 1), size(theroi, 2), 1]);
                        end
                        
                        for ii = 1:size(theroi, 3)
                                imwrite(cast_fn(squeeze(theroi(:,:,ii-1))), cat(2, pathname, filename), format, 'Compression', 'none', 'WriteMode', 'append');
                        end
        	case 2
                        format = 'ICSv2';
                        writeim(theroi, cat(2, pathname, filename), format, 'no');                     
            case 3
                        save(cat(2, pathname, filename), 'theroi');

       end
	
        cancel = 0;      
else
        cancel = 1;
end
end