%      [tp0 tpend thebinsz themethod thesmoothing thekernelsz filename pathname ext bg_pixorientation] = DIALOG_GRADIENTPOLARITY([min_range_tp max_range_tp], [min_default_tp max_default_tp], bin_sz, method, smoothing, kernel_sz, bg_pixorientation_method) returns the handle to a new DIALOG_GRADIENTPOLARITY
%	   [min_range_tp max_range_tp] minimum and maximum possible values for the time points. Smallest value is zero.
%	   [min_default_tp max_default_tp] minimum and maximum selected time point when the dialog is created. Start at zero.
%      bin_sz is the width of the orientation bins in the quantification of polarity (i.e. what angles are pooled together).
%      method is the magnitude use to score each pixel: gradient (1) or brightness (2).
%      smoothing is how much to smooth. 3 by default.
%      kernel_sz is the size of the kernel used to calculate the gradient. 3 (7x7) by default.
%      bg_pixorientation_method is how to identify background pixels and calculate pixel orientation: 1 no background, pixel orientation calculated for each channel independently; 2 background and pixel orientation calculated for each channel independently; 3 background and pixel orientation calculated using red channel; 4 background and pixel orientation calculated using green channel; 5 background and pixel orientation calculated using blue channel.
%
%      Returns:
%        tp0 and tpend are the initial and final time points to be analyzed (their smallest possible value is zero).
%        thebinsz is the selected bin size.
%        themethod is one of 'gradient' or 'brightness'.
%        thesmoothing and thekernelsz contain the values sected for this parameter.
%        filename is the name of the file to save the analysis or []. Includes file extension but NO path.
%        pathname is the path to the file in which the analysis will be saved. File name is not included.
%        ext is 1 for csv files, 2 for mat files, 3 for others.
%        bg_pixorientation is how to identify background pixels and calculate pixel orientation: 1 no background, pixel orientation calculated for each channel independently; 2 background and pixel orientation calculated for each channel independently; 3 background and pixel orientation calculated using red channel; 4 background and pixel orientation calculated using green channel; 5 background and pixel orientation calculated using blue channel.


function varargout = dialog_gradientpolarity(varargin)
% DIALOG_GRADIENTPOLARITY M-file for dialog_gradientpolarity.fig



%      DIALOG_GRADIENTPOLARITY, by itself, creates a new DIALOG_GRADIENTPOLARITY or raises the existing
%      singleton*.
%
%      H = DIALOG_GRADIENTPOLARITY returns the handle to a new DIALOG_GRADIENTPOLARITY or the handle to
%      the existing singleton*.
%
%      DIALOG_GRADIENTPOLARITY('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DIALOG_GRADIENTPOLARITY.M with the given input arguments.
%
%      DIALOG_GRADIENTPOLARITY('Property','Value',...) creates a new DIALOG_GRADIENTPOLARITY or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before dialog_timepoints_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to dialog_gradientpolarity_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help dialog_gradientpolarity

% Last Modified by GUIDE v2.5 25-Feb-2012 15:12:05

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @dialog_gradientpolarity_OpeningFcn, ...
                   'gui_OutputFcn',  @dialog_gradientpolarity_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before dialog_gradientpolarity is made visible.
function dialog_gradientpolarity_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to dialog_gradientpolarity (see VARARGIN)

% Choose default command line output for dialog_gradientpolarity
handles.output = hObject;

% Parameter values for components. RODRI EDITED HERE.
% THIS HAS TO BE BEFORE guidata(hObject, handles)!!!!
set(handles.figure1, 'name', 'Gradient-based polarity measurement');
warning('off', 'all'); % This is necessary to remove a warning
					   % caused by a Matlab bug.

nvargin = numel(varargin);                       
                       
if nvargin < 1 || numel(varargin{1}) < 2
    error('The first argument must be an array with at least 2 numeric elements.');
end

bin_sizes = 1:45;
if nvargin < 3 || varargin{3} > bin_sizes(end)
    bin_sz = 15;
else
    bin_sz = varargin{3};
end

if nvargin < 4
    method = 1; % gradient.
else
    method = varargin{4};
    switch method
        case 'gradient'
                method = 1;
        case {'brightness'; 'intensity'}
                method = 2;                
        otherwise
                method = 1;
    end
end

if nvargin < 5
    smoothing = 3;
else
    smoothing = varargin{5};
end

if nvargin < 6
    kernel_sz = 9;
else
    kernel_sz = varargin{6};
end

if nvargin < 7
    bg_pixorientation_method = 1;
else
    bg_pixorientation_method = varargin{7};
end
                       
timepoints = sort(varargin{1});
tps = timepoints(1):timepoints(end);

if nvargin < 2 | min(varargin{2}) < 0 | max(varargin{2}) >= numel(tps)
    first = 1;
    last = numel(tps);
else
    first = find(tps == varargin{2}(1));
    last = find(tps == varargin{2}(end));
end

handles.tps = tps;
handles.closemethod = -1;
str = [];

for i = 1:numel(tps)
	str = cat(2, str, cat(2, num2str(tps(i)), '|'));
end

str = str(1:(end-1));
set(handles.popupmenu1, 'String', str, 'Value', first);
set(handles.popupmenu2, 'String', str, 'Value', last);
set(handles.popupmenu3, 'String', 'gradient|brightness', 'Value', method);
set(handles.popupmenu4, 'String', 'no background, pixel orientation calculated for each channel independently|background and pixel orientation calculated for each channel independently|background and pixel orientation calculated using red channel|background and pixel orientation calculated using green channel|background and pixel orientation calculated using blue channel', 'Value', bg_pixorientation_method);

step_size = [1/(numel(bin_sizes)-1) 10/(numel(bin_sizes)-1)];
value = (bin_sz-1)/(numel(bin_sizes)-1);
set(handles.slider4, 'Min', 0., 'Max', 1., 'SliderStep', step_size, 'Value', value);
set(handles.text6, 'String', num2str(round(value/step_size(1))+1));
set(handles.edit2, 'String', num2str(round(smoothing)));
set(handles.edit3, 'String', num2str(round(kernel_sz)));

handles.filename = [];
handles.pathname = [];
handles.ext = -1;

% Update handles structure.
guidata(hObject, handles);


% UIWAIT makes dialog_gradientpolarity wait for user response (see UIRESUME)
uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = dialog_gradientpolarity_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
%varargout{1} = handles.output;
%get(handles.figure1)
%uiwait(handles.figure1);

d = guidata(handles.figure1);

if d.closemethod == 1
	tp(1) = handles.tps(get(handles.popupmenu1, 'Value'));
	tp(2) = handles.tps(get(handles.popupmenu2, 'Value'));
	tp = sort(tp);
    
    value = get(handles.slider4, 'Value');
    step_size = get(handles.slider4, 'SliderStep');
    thebinsz = round(value/step_size(1))+1;
    
    switch get(handles.popupmenu3, 'Value')
        case 1
            themethod = 'gradient';
        case 2
            themethod = 'brightness';
    end
    
    bg_pixorientation = get(handles.popupmenu4, 'Value');
    
    thesmoothing = str2num(get(handles.edit2, 'String'));
    thekernelsz = str2num(get(handles.edit3, 'String'));
    filename = d.filename;
    pathname = d.pathname;
    ext = d.ext;
else
    tp = [-1 -1];
    thebinsz = -1;
    themethod = [];
    bg_pixorientation = -1;
    thesmoothing = -1;
    thekernelsz = -1;
    filename = [];
    pathname = [];
    ext = -1;
end

close(handles.figure1);

varargout{1} = tp(1);
varargout{2} = tp(2);
varargout{3} = thebinsz;
varargout{4} = themethod;
varargout{5} = thesmoothing;
varargout{6} = thekernelsz;
varargout{7} = filename;
varargout{8} = pathname;
varargout{9} = ext;
varargout{10} = bg_pixorientation;

% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1

selected = get(handles.popupmenu1,'Value');
prev_str = get(handles.popupmenu1, 'String');
set(handles.popupmenu1, 'String', prev_str, 'Value', selected);

% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2
selected = get(handles.popupmenu2,'Value');
prev_str = get(handles.popupmenu2, 'String');
set(handles.popupmenu2, 'String', prev_str, 'Value', selected);


% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%set(handles.figure1, 'Visible', 'off');
handles.closemethod = 1;
guidata(handles.figure1, handles);
uiresume(handles.figure1);



% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.closemethod = 0;
guidata(handles.figure1, handles);
uiresume(handles.figure1);





function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double

% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double
if isempty(eventdata) % return pressed or lost focus
    if isnan(str2double(get(hObject,'String'))) % str2double returns nan if there is a problem with the conversion.
        set(hObject, 'String', '');
    end
end

% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');    
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double
if isempty(eventdata) % return pressed or lost focus
    if isnan(str2double(get(hObject,'String'))) % str2double returns nan if there is a problem with the conversion.
        set(hObject, 'String', '');
    end
end


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
extensions = {'*.csv'; '*.mat'; '*.*'};
[handles.filename, handles.pathname, handles.ext] = uiputfile(extensions, 'Save quantification data');
guidata(handles.figure1, handles); % This line updates the data stored in handles so that it can be retrieved later.

set(handles.edit1, 'String', cat(2, handles.pathname, handles.filename));

% --- Executes on slider movement.
function slider4_Callback(hObject, eventdata, handles)
% hObject    handle to slider4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
value = get(hObject, 'Value');
step_size = get(hObject, 'SliderStep');
set(handles.text6, 'String', num2str(round(value/step_size(1))+1));

% --- Executes during object creation, after setting all properties.
function slider4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on selection change in popupmenu3.
function popupmenu3_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu3


% --- Executes during object creation, after setting all properties.
function popupmenu3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu4.
function popupmenu4_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu4 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu4


% --- Executes during object creation, after setting all properties.
function popupmenu4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
