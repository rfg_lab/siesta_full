% *********************************************************************************
% Check/Uncheck (status 'on'/'off' respectively) the menu option at menu1->menu2.
function cb_setmenustatus(fig, menu1, menu2, status)
ch = get(fig, 'Children');
for ii = 1:numel(ch)
	if isfield(get(ch(ii)), 'Label') && strcmp(get(ch(ii), 'Label'), menu1)
		gs = get(ch(ii), 'Children');
		for jj = 1:numel(gs)
			if isfield(get(gs(jj)), 'Label') && strcmp(get(gs(jj), 'Label'), menu2)
				set(gs(jj), 'Checked', status);
				break;
			end
		end
		break;
	end
end
