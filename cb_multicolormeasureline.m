% [ang l int] = cb_multicolormeasureline(fig, lineh, channels, brush_sz)
function [ang l int] = cb_multicolormeasureline(fig, lineh, channels, brush_sz)

ang = 0;
l = 0;
int = 0;


udata = get(fig, 'UserData');
x = get(lineh, 'XData');
y = get(lineh, 'YData');

% Angle.
if (x(1) < x(2))
    p1 = [x(1) y(1)];
    p2 = [x(2) y(2)];
else
    p2 = [x(1) y(1)];
    p1 = [x(2) y(2)];
end

% Compute vector.
v = p2 - p1;
l = norm(v, 2);

% If the second point is under the first one (largest y)
% then the angle is larger than pi/2.
if (l ~= 0 && p2(2) > p1(2))
    ang = pi - asin(abs(v(2))/l);
    % Else the angle is smaller than pi/2.
elseif (l ~= 0)
    ang = asin(abs(v(2)) / l);
    % Else where are trying to measure the angle formed by a dot!!
else
    ang = 0.0;
end
% END ANGLE

% Intensity.
mask = dip_image(zeros(udata.imsize(2), udata.imsize(1), 1), 'bin8');
mask = dip_image(drawline2(mask, [x(1) y(1)], ang, 1, brush_sz/4));
mask2 = dip_image(zeros(udata.imsize(2), udata.imsize(1), 1), 'bin8');

[xc, yc] = meshgrid((min(x(1),x(2))-floor(brush_sz/2)):(max(x(1),x(2))+floor(brush_sz/2)), (min(y(1),y(2))-floor(brush_sz/2)):(max(y(1),y(2))+floor(brush_sz/2)));

mask2(xc, yc) = 1;

mask = (mask.*mask2>0);
%Make function cb_showmask:
%overlay(udata.colordata{1}, mask);

if ~ isempty(udata.colordata)
    int = zeros(1, numel(channels));
    for ic = 1:numel(channels)
        channel = channels(ic);
        if (~ isempty(udata.colordata{channel}(mask)))
            int(ic) = mean(udata.colordata{channel}(mask));
            % If the image where intensities should be measured is empty, we are probably
            % measuring the intensity of a point.
        else
            tmp = cb_getpixvalue(fig);
            int(ic) = tmp(channel);
        end
    end
else
    int = zeros(1, 3);
    if (~ isempty(udata.imagedata(mask)))
        if isempty(udata.rcurrchannel) | (udata.rcurrchannel == 0)
            udata.rcurrchannel = (1:3);
        end
        int(udata.rcurrchannel) = mean(udata.imagedata(mask));
        % If the image where intensities should be measured is empty, we are probably
        % measuring the intensity of a point.
    else
        int(udata.rcurrchannel) = cb_getpixvalue(fig);
    end
end
% END INTENSITY
end
