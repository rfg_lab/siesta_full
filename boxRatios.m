function[ratio,length]=boxRatios(polygon)

% find convex hull
ind=convhull(polygon(:,1),polygon(:,2));
convexHull=polygon(ind,:);

% find minimum bounding box
[a,b,~,~]=minboundrect(polygon(:,1),polygon(:,2));
minBox=[a,b];

%plot the min bound box and convex hall
% figure;plot(polygon(:,1),polygon(:,2));hold on;
% plot(convexHull(:,1),convexHull(:,2),'r');
% plot(minBox(:,1),minBox(:,2),'g');

% ratio(1) = min box area/polygon area 
ratio(1)=polyarea(minBox(:,1),minBox(:,2))/polyarea(polygon(:,1),polygon(:,2));
% ratio(2) = convex hull/polygon area 
ratio(2)=polyarea(convexHull(:,1),convexHull(:,2))/polyarea(polygon(:,1),polygon(:,2));

% temp contains length and width of min bound box
temp(1)=sqrt((minBox(1,1)-minBox(2,1))^2+(minBox(1,2)-minBox(2,2))^2);
temp(2)=sqrt((minBox(2,1)-minBox(3,1))^2+(minBox(2,2)-minBox(3,2))^2);

%return length as ratio between length and width of min bound box
length=max(temp)/min(temp);

end