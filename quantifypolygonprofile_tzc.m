% *********************************************************************************
% function [pix_vals_r pix_vals_g pix_vals_b gaussparams d] = cb_quantifytrajectoryprofile_constantdist(fig, channel, brush_sz, tp_list)
% gaussparams [mu, sigma, scale]
% d is the distance in pixels between subsequent measurements at each profile. It will vary from profile to profile, as every profile has a different length, yet this function uses a constant number of points. Thus, it is important for profiles to maintain a relatively constant length.
% The distance between the points where edge intensity is measured is APPROXIMATELY 0.25pixels. In cb_quantifytrajectoryprofile a certain number of points is used to measure the intensity of the edge, and therefore, for edges with very different lengths, the resolution of the measurements will be different. However, we still have a problem when a certain edge changes length dramatically over time (the number of measurement points is determined based on the initial length and then maintained over time).
function [pix_vals_r rawData md_bg me_bleaching pix_vals_g pix_vals_b coords] = quantifypolygonprofile_tzc(fig, channel, brush_sz, tp_list,intialPoint, finalPoint, smooth_factor);


udata = get(fig, 'UserData');
MAX_PNTS = 4000; % Max number of mea

     MAX_PNTS = 4000; % Max number of measurements.
     
%      if ndims(udata.rpolygons) == 3 % Fix trajectiories if messed up.
%         udata.rpolygons = udata.rpolygons(:, :, 1);
%      end        
%      
     % teresa edit: for the moment only 1 trajectory one tp
     numpol=1;
     
     pix_vals_r = nan .* zeros(max(tp_list), MAX_PNTS, max(numpol)); % One row per time point, one column per point measured along the trajectory, one slice per trajectory.
     pix_vals_g = nan .* zeros(max(tp_list), MAX_PNTS, max(numpol)); % One row per time point, one column per point measured along the trajectory, one slice per trajectory.
     pix_vals_b = nan .* zeros(max(tp_list), MAX_PNTS, max(numpol)); % One row per time point, one column per point measured along the trajectory, one slice per trajectory.
     
     d = nan .* zeros(max(numpol), max(tp_list));
     
   % Find shortest trajectory.
%     nl = inf;
%     for ii = 1:numel(tp_list)
%          thetp = tp_list(ii);
%          trajectories = udata.rtrajectories(:, :, thetp);
%          
%          for jj = 1:udata.rntrajectories(thetp)
%                  tr = trajectories{jj};
%                  
%                  [ang l] = cb_multicolormeasuretrajectory(fig, tr, brush_sz, thetp - 1);
%                  
%                  if l < nl
%                          nl = l;
%                  end
%          end
%     end
   
   if nargin < 5
        smooth_factor = 10;
   end     
   
   measure_res = 0.25; % Distance in pixels in between measurements along the trajectory.
   min_dist_between_fit_and_data_maxima = 20; % How far can the peak of the data and the Gaussian fit be so that we still consider this an appropriate fit? The units for this magnitude are min_dist_between_fit_and_data_maxima .* measure_res pixels.
   
   % For every time point.
   for ii = 1:numel(tp_list)
        thetp = tp_list(ii);
        if (~ isfield(udata, 'colordata')) || isempty(udata.colordata)
                im = squeeze(udata.slices(:, :, thetp-1)); % squeeze is necessary, as get_subpixel will be given 2D pixel coordinates.
        else
                im = squeeze(joinchannels('rgb', udata.slices{1}(:, :, thetp-1), udata.slices{2}(:, :, thetp-1), udata.slices{3}(:, :, thetp-1)));
        end        
        
        trajectories = udata.rpolygons{:, :, tp_list};
        
   end
       
        
        
        % For every trajectory in this time point.
        %%TERESA EDIT: FOR THE MOMENTS ONLY ONE POLYGON FOR TP
        for jj = 1:1
                tr = trajectories;
%                  tr = cb_changetrajectorylength(tr, nl);
                tr = tr(:, 1:2); % Do not need the Z coordinate. And we will only deal with the first segment in the trajectory.
                
                if ~ isempty(tr)
                        points = tr;
                        
                        xx=points(:,1);
                        yy=points(:,2);
                        % Teresa Edit: Delete consecutive points
                        index=[];
                        for tt=2:length(xx)
                           
                            if xx(tt)==xx(tt-1) && yy(tt)==yy(tt-1)
                                index=[index;tt];

                            end

                        end
                        points(index,:)=[];
                        N = size(points,1);
                        if N<2
                                error('You need to select at least two points.');
                        end
                        coords = [];
                        co=[];
                        for kk=1:(N-1)
                                len = norm(points(kk+1,:)-points(kk,:)); % Length of the iith segment.
                                if ii == 1 % Compute the number of points in the first time point, and then assume the same number for all other time points. This assumes that the length of the drawn trajectory remains constant over time.
                                        npnts = ceil(len/measure_res) + 1;
                                end  
                               % TeresaEdit
%                                npnts=7;
                                k = len;
                                inc = k / (npnts-1); % inc will be very close to 0.25.
                                d(jj, thetp) = inc;
                                factors = (0:inc:k);
                                factors = factors(1:npnts);
                                tmp = repmat(points(kk,:),[npnts,1])+factors'*((points(kk+1,:)-points(kk,:))/len);
                                %tmp = repmat(points(kk,:),[k,1])+(0:k-1)'*((points(kk+1,:)-points(kk,:))/len); %  ((points(ii+1,:)-points(ii,:))/len) is a unit vector pointing from the current point to the next one in the trajectory.
                                % (0:k-1)'*((points(ii+1,:)-points(ii,:))/len) are vectors from point ii to point ii+1 with lengths from 0 to k-1. By adding each one of these vectors to the coordinates of point ii we get a series of equidistant points along the segment that connects points ii and ii+1. The equidistant points are 1 pixel apart.
                                co = [co ;tmp];
                        end
                        i0=[];
                        fi=[];
                        %FIND THE FIDUCIAL POINTS BETWEEN WICH PERFORM THE
                        %ANALYSIS
                        for tt=1:size(co,1)
                            if co(tt,1)==intialPoint(:,1) && co(tt,2)==intialPoint(:,2)
                                i0=tt;
                          
                            end
                            if co(tt,1)==finalPoint(:,1) && co(tt,2)==finalPoint(:,2)
                                fi=tt;
                            end
            
                        end
                        if isempty(i0)
                            for uu=1:size(co,1)
                                dist(uu)=sqrt((co(uu,1)-intialPoint(:,1)).^2+(co(uu,2)-intialPoint(:,2)).^2);
                            end
                            [xmind ixmind]=min(dist,[],2);
                      
                            i0=ixmind;
                        end
                         if isempty(fi)
                            for uu=1:size(co,1)
                                dist2(uu)=sqrt((co(uu,1)-finalPoint(:,1)).^2+(co(uu,2)-finalPoint(:,2)).^2);
                            end
                            [xmind ixmind2]=min(dist2,[],2);
                      
                            fi=ixmind2;
                        end
                        if i0>fi
                           
                            temp=i0;
                            i0=fi;
                            fi=temp;
                            coo=co(i0:fi,:);
                            coo=coo(end:-1:1,:);
                            if size(coo,1) > size(co,1)/2
                                cur1=co(i0:-1:1,:);
                                cur2=co(end:-1:fi,:);
                                coo=[cur1;cur2];
                            end
                            co=coo;   
                        else
                        
                        coo=co(i0:fi,:);
                        if size(coo,1) > size(co,1)/2
                                cur1=co(i0:-1:1,:);
                                cur2=co(end:-1:fi,:);
                                coo=[cur1;cur2];
                            end
                            co=coo;   
                        end
                        
                        xx=co(:,1);
                        yy=co(:,2);
                        % Teresa Edit: Delete consecutive points
                        index=[];
                        for tt=2:length(xx)
                           
                            if xx(tt)==xx(tt-1) && yy(tt)==yy(tt-1)
                                index=[index;tt];

                            end

                        end
                        co(index,:)=[];
                       
%                       Interpolate points so we have the same in all the
%                       tp:
                        x0=co(:,1);
                        y0=co(:,2);
                        
                        % First compute the number of points that we need:
                        PtsCo=size(co,1);
                        PtsTot=1000;
                        %PtsNeeded=PtsTot-PtsCo;
                        if PtsCo<PtsTot
                            PtsNeeded=1000;
                            Segments=PtsCo-1;
                            PSeg=PtsNeeded/Segments;
                            PSeg=floor(PtsNeeded/Segments);
                            NP=PSeg*Segments;
                            D=PtsNeeded-NP;
                            PtsSeg=PSeg*ones(1,PtsCo-1);
                            for mm=1:D
                                PtsSeg(mm)=PtsSeg(mm)+1;
                            end
                            x=[];
                            y=[];
                            for nn=1:PtsCo-1
                                        if x0(nn)==x0(nn+1)
                                            DistSeg=(y0(nn+1)-y0(nn))/(PtsSeg(nn));
    %                                          if y0(nn)<y0(nn+1)
    %                                             DistSeg=-DistSeg;
    %                                         end
                                            yi=y0(nn):DistSeg:y0(nn+1);
                                            xi = interp1(y0(nn:(nn+1)),x0(nn:(nn+1)),yi);

                                        else
                                           DistSeg=(x0(nn+1)-x0(nn))/(PtsSeg(nn));
    %                                         if x0(nn)<x0(nn+1)
    %                                             DistSeg=-DistSeg;
    %                                         end
                                            xi=x0(nn):DistSeg:x0(nn+1);
                                            yi = interp1(x0(nn:(nn+1)),y0(nn:(nn+1)),xi);
                                        end

                                        x=[x;xi(1:end-1)'];
                                        y=[y;yi(1:end-1)'];

                            end
                                coords(:,1)=x;
                                coords(:,2)=y;
                        else
                            PtsNeeded=10000;
                            Segments=PtsCo-1;
                            PSeg=PtsNeeded/Segments;
                            PSeg=floor(PtsNeeded/Segments);
                            NP=PSeg*Segments;
                            D=PtsNeeded-NP;
                            PtsSeg=PSeg*ones(1,PtsCo-1);
                            for mm=1:D
                                PtsSeg(mm)=PtsSeg(mm)+1;
                            end
                            x=[];
                            y=[];
                            for nn=1:PtsCo-1
                                        if x0(nn)==x0(nn+1)
                                            DistSeg=(y0(nn+1)-y0(nn))/(PtsSeg(nn));
    %                                          if y0(nn)<y0(nn+1)
    %                                             DistSeg=-DistSeg;
    %                                         end
                                            yi=y0(nn):DistSeg:y0(nn+1);
                                            xi = interp1(y0(nn:(nn+1)),x0(nn:(nn+1)),yi);

                                        else
                                           DistSeg=(x0(nn+1)-x0(nn))/(PtsSeg(nn));
    %                                         if x0(nn)<x0(nn+1)
    %                                             DistSeg=-DistSeg;
    %                                         end
                                            xi=x0(nn):DistSeg:x0(nn+1);
                                            yi = interp1(x0(nn:(nn+1)),y0(nn:(nn+1)),xi);
                                        end

                                        x=[x;xi(1:end-1)'];
                                        y=[y;yi(1:end-1)'];

                            end
                            step=10;
                            x=x(1:step:end);
                            y=y(1:step:end);
                                
                            
                            
                                coords(:,1)=x;
                                coords(:,2)=y;
                            
                            
                            
                        end
                        
                       
                       
                            

                        if (~ isfield(udata, 'colordata')) || isempty(udata.colordata)        
                                pix_vals_r(1, 1:size(coords,1), jj) = get_subpixel(im,coords,'cubic');
%                                 pix_vals_g(1, :, jj) = pix_vals_r(thetp, :, jj);
%                                 pix_vals_b(1, :, jj) = pix_vals_r(thetp, :, jj); 
                                % output = dip_image(get_subpixel(im,coords,'cubic'));
                        else
                                pix_vals_r(thetp, 1:npnts, jj) = get_subpixel(im{1},coords,'cubic');
                                pix_vals_g(thetp, 1:npnts, jj) = get_subpixel(im{2},coords,'cubic');
                                pix_vals_b(thetp, 1:npnts, jj) = get_subpixel(im{3},coords,'cubic');
   
                        end
                        
                end        
        end                     
%  %Teresa Edit visualize interp points
% figure()
% im
% hold on
% scatter(x,y,4,[1 0 0], 'filled')
                        
pix_vals_r = pix_vals_r(1, 1:size(coords,1));
rawData=pix_vals_r;
%%Background substraction
%  md=mean(im);
image_d=double(im);
me_bleaching=mean(image_d(image_d~=0));
md_bg=mode(image_d(image_d~=0));
%md_bg=211.3500;% abl movies masako 
%md_bg=187.02; %green channel 20140501
%md_bg=187.9539; %red channel 20140501
%md_bg=192.0924; %red channel 20140502

% This division by im_mean does not do anything, as in the next line you will divide by average value of this same vector, effectively multiplying again by im_mean.
pix_vals_r= (pix_vals_r-md_bg)./me_bleaching;

%  
%% Teresa edit: This is for FRAP
    % Compute the average intensity at every subpixel coordinate in the first two time points.
%    normpix = pix_vals_r ; % And normalize all other intensities to those averages.
%    
%    for ii = 1:size(normpix, 1)
%         thepix = normpix(ii, find(~isnan(normpix(ii, :))));
%         if isempty(thepix) || nnz(thepix) == 0
%                 gaussparams(ii, :) = [nan nan nan];
%                 continue;
%         end
%         
%         yvals = 1. - double(smooth(dip_image(thepix), smooth_factor)); % mountain-like gaussian.
% 
%         if ii <= 3 % Find estimates for parameters from intensity values in the time point immediately after bleaching.
%                 [scaleestim muestim] = max(yvals);
%         else % For all other time points, use the estimated values for the postbleach time point.          
%                 muestim = gaussparams(3, 1);        
%                 scaleestim = gaussparams(3, 3);        
%         end
%         
%         [beta] = nlinfit((1:numel(yvals)), yvals, @gaussianfn2, [muestim 1 scaleestim]);
%         
%         % For bleached regions (i>2), make sure that the value found for mu (beta(1)) is not terribily off from that initially estimated (muestim); and that the intensity curve has a clear minimum between two higher "plateaus".
%         if ii > 2 && (abs(beta(1)-muestim) < min_dist_between_fit_and_data_maxima && abs(abs(mean(yvals(1:5)) - mean(yvals((end-4):end))) - max(abs(beta(3)-mean(yvals(1:5))), abs(beta(3)-mean(yvals((end-4):end))))) > 0.05)
%                 gaussparams(ii, :) = beta;
%         elseif ii > 2 && (muestim-5) > 0 && (muestim+5) < numel(yvals)% if the previous fit is not good, take a line (perhaps we should try to fit a polynomial or a gaussian sum here, taking only the gaussian of closest mean to muestim).
%                 %gaussparams(ii, :) = nan .* beta;
%                 beta(1) = muestim; % mu
%                 beta(3) = mean(yvals((round(muestim)-5):(round(muestim)+5)));%mean(yvals); % scale.
%                 beta(2) = inf; %sigma.
%                 gaussparams(ii, :) = beta;
%         else
%                 gaussparams(ii, :) = nan .* beta;                                
%         end        
%    end
end
