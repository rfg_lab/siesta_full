%sc = smooth_curve2(curve, n)
%
%  Smoothens the outline of a curve using a gaussian kernel of width n.
%  
%  CURVE contains the Y value for each one of the curve points.
%  N is the sigma of the gaussian function.
%
%  SC is the smooth curve.
%  
%  Rodrigo Fernandez-Gonzalez
%  fernanr1@mskcc.org
%  20090725


function sc = smooth_curve2(curve, n)

l = numel(curve);
curve = reshape(curve, [1, l]);

% Work with this version of the curve. Add a nan in front and another one at the end.
sc = [nan curve nan];

% Find nans in the curve.
indnans = find(isnan(sc));

% For every block of numbers in between nans ...
for ii = 1:(numel(indnans)-1)
        % ... and as long as the block has at least to numbers (smoothing of a single number returns an error).
        if indnans(ii+1)-indnans(ii) > 2
                % Smoothen the numbers.
                sc(indnans(ii)+1:indnans(ii+1)-1) = double(smooth(dip_image(sc(indnans(ii)+1:indnans(ii+1)-1)), n));
        end
end

% Remove the front and end nans.
sc = sc(2:(end-1));