%RMEDIANNAN median, std and sem ignoring NaN elements.
%    [med mad] = RMEDIANNAN(X,DIM) returns the median and the absolute
%    deviation of the median (MAD) for matrix X computed along
%    dimension DIM (see median for more info) ignoring elements of
%    the matrix set to the NaN value.  
%
%    The MAD can be used to estimate the scale parameter of
%    distributions for which the variance and standard deviation do not
%    exist, such as the Cauchy distribution. Even when working with
%    distributions for which the variance exists, the MAD has advantages
%    over the standard deviation. For instance, the MAD is more
%    resilient to outliers in a data set. In the standard deviation, the
%    distances from the mean are squared, so in the average, large
%    deviations are weighted more heavily. In the MAD, the magnitude of
%    the distances of a small number of outliers is irrelevant.
%
% Rodrigo Fernandez-Gonzalez
% fernanr1@mskcc.org
% 2008/06/04

function varargout = rmediannan(vector, dim, output)

if nargin < 3
	output = 1;
end

if nargin < 2
	dim = 1;
end

if ~ isempty(vector)
	switch dim
		case 1
			m = zeros(1, size(vector, 2)); % median
			s = zeros(1, size(vector, 2)); % mad
			
			for i = 1:size(vector, 2)
				ind = find(~ isnan(vector(:, i)));
				if ~ isempty(ind)
					m(i) = median(vector(ind, i), dim);
					s(i) = median(abs(vector(ind, i) - m(i)), dim);
				else
					m(i) = nan;
					s(i) = nan;
				end
			end
		
		case 2
			m = zeros(size(vector, 1), 1);
			s = zeros(size(vector, 1), 1);
	
			for i = 1:size(vector, 1)
				ind = find(~ isnan(vector(i, :)));
				
				if ~ isempty(ind)
					m(i) = median(vector(i, ind), dim);
					s(i) = median(abs(vector(i, ind) - m(i)), dim);
				else
					m(i) = nan;
					s(i) = nan;
				end
			end
	
		case 3
			m = zeros(size(vector, 1), size(vector, 2));
			s = zeros(size(vector, 1), size(vector, 2));
			e = zeros(size(vector, 1), size(vector, 2));
			
			for i = 1:size(vector, 1)
				for j = 1:size(vector, 2)
					ind = find(~ isnan(vector(i, j, :)));
					
					if ~ isempty(ind)
						m(i, j) = median(vector(i, j, ind), dim);
						s(i, j) = median(abs(vector(i, j, ind) - m(i)), dim);
					else
						m(i, j) = nan;
						s(i, j) = nan;
					end
				end
			end
	end
else
	m = nan;
	s = nan;
end
		

switch output
	case 1
		varargout{1} = m;
		varargout{2} = s;
	case 2
		varargout{2} = m;
		varargout{1} = s;
end
