% Deletes markings in current frame.

% Michael add a flag to delete trackedPolygon
function udata = cb_deleteall(fig, mtrackPFlag)

% Michael
if nargin<2
    mtrackPFlag=0;
end
%Michael

udata = get(fig, 'UserData');
ind = udata.curslice + 1;

udata.rdelimiters(:, :, ind) = -1 .* ones(size(udata.rdelimiters, 1), size(udata.rdelimiters, 2));
udata.rndelimiters(ind) = 0;

udata.rfiducials(:, :, ind) = -1 .* ones(size(udata.rfiducials, 1), size(udata.rfiducials, 2));
udata.rnfiducials(ind) = 0;

udata.runiquecoords(ind, :) = [-1 -1];

udata.rpolygons(:, :, ind) = {[]};
%michael
if mtrackPFlag==1
    if ~isempty(udata.trackedPolygon)
        udata.trackedPolygon(:,:,ind:end,:)=[];
        if ~isempty(udata.trackDiv)
            deleteTime=find(udata.trackDiv(1,:,1)>(ind-1));
            if deleteTime
                udata.trackDiv(:,deleteTime,:)=[];
            end
        end
    end
end
%michael
udata.rtrajectories(:, :, ind) = {[]};
udata.rntrajectories(:, :, ind) = 0;
udata.rangles(:, :, ind) = {[]};

ax = get(fig, 'CurrentAxes');
ch = get(ax, 'Children');

for ii = 1:numel(ch)
    type = get(ch(ii), 'Tag');
    if strcmp(type, 'Fiducial') | strcmp(type, 'Delimiter') | strcmp(type, 'Unique') | strcmp(type, 'Polyline')
        delete(ch(ii));
    end
end


end
