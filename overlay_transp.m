%out = overlay_transp(grey, labeled)
%
%  Overlays a labeled image on a grey-scale one using some degree of
%  transparency so that the grey image can be seen under the labeled
%  one.
%  
%  GREY is a grayscale image.
%  LABELED is a labeled image with the same dimensions as GREY.
%
%  OUT is the output image.
%  
%  Rodrigo Fernandez-Gonzalez ("inspired" by Cris Luengo)
%  fernanr1@mskcc.org
%  06/15/2007


function out = overlay_transp(grey, lab)

MAX_LAB = 255;

% Decide the color map to use depending on the number of cells.
n = numel(unique(double(lab)));
color_map = jet(n);

% If there are many cells, permutate the colors from their sequential order.
if (n > MAX_LAB)
	myperm = randperm(n);
	color_map = color_map(myperm, :) .* MAX_LAB;
% Otherwise just use them sequentially.
else
	color_map = color_map .* MAX_LAB;
end

color_map(1, :) = [0 0 0]; % Background is always black.


out = overlay(grey, lab, color_map); % Overlay both images.
out(~lab) = 255; % Everything outside the labeled objects preserves its color.
out = out .* grey ./ 256; % Color * grey = transparent.

