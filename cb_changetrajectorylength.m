% **************************************************************************
% Returns a new trajectory that spans from -nl/2 to +nl/2 from the center of
% tr. If nl is larger than the trajectory's length, the entire trajectory is measured.
function atr = cb_changetrajectorylength(tr, nl)

      slice_index = tr(1, 3);
      
      % Compute length.
      l = 0;
      segment_l = zeros(1, size(tr, 1)-1);
      for ii = 1:(size(tr, 1)-1)
        segment_l(ii) = norm([tr(ii+1,1)-tr(ii,1) tr(ii+1,2)-tr(ii,2)], 2);
      end
      l = sum(segment_l);
           
      % If the trajectory is longer than the new length:
      if l > nl
        for ii = 1:2 % Repeat twice, once for the left side of the trajectory and once for the right side (from the center).
                                                                                
                % Find the center of the trajectory.
                ind = find(cumsum(segment_l) < (l/2.));
                
                if isempty(ind)
                        ind = 1;
                else
                        ind = ind(end) + 1; % This is the index of the segment containing the center. ind and ind+1 point at the rows of tr that are to the sides of this segment.
                end                        
                % How far to go in the negative direction.
                segml_before = l/2 - sum(segment_l(1:(ind-1))); % Distance to the "left" of the center and in the same segment.
                allbefore = cumsum([segml_before segment_l((ind-1):-1:1)]); % Cumulative distances from center to the left end of the trajectory.
                allbefore = allbefore(end:-1:1); % Cumulative distances from center to the left end of the trajectory ordered with the leftmost edge first.
                indbefore = find(allbefore < nl/2.);
                
                
                if ~isempty(indbefore)
                        indbefore = indbefore(1) - 1; %Index of the segment where the "leftmost" point is.
                        dbefore = allbefore(indbefore) - nl/2; % Distance from the leftmost point of the selected segment to the point where the trajectory should end.
                        dafter = segment_l(indbefore) - dbefore;
                        
                        % y = mx + t
                        x1 = tr(indbefore, 1);
                        y1 = tr(indbefore, 2);
                        x2 = tr(indbefore+1, 1);
                        y2 = tr(indbefore+1, 2);
                        
                        % If the x coordinate is not constant ...
                        if abs(x2 - x1) > 0.001
                                m = (y2 - y1) / (x2 - x1);
                                t = y1 - m * x1;
                                                
                                a = 1 + m * m;
                                b = - 2 * (x1 - m * (t - y1));
                                c = x1 * x1 + (t - y1) ^ 2 - dbefore ^ 2;
                                
                                % Solve for x3 and y3.
                                x3 = (- b + sqrt(b * b - 4 * a * c)) / (2 * a);
                                y3 = m * x3 + t;
                                
                                % But there are two solutions for x3. If this is the good one, the point we found
                                % should be dafter away from the rightmost point of the segment.
                                if abs(norm([x3 y3] - [x2 y2], 2) - dafter) > 0.001 % Tried to use eps here, but it does not work, it is too small a number.
                                        x3 = (- b - sqrt(b * b - 4 * a * c)) / (2 * a);
                                        y3 = m * x3 + t;
                                end
                        % If x is constant just find y.        
                        else
                                x3 = x1;
                                y3 = y1 + dbefore;
                        end
                else
                        indbefore = ind;
                        dbefore = allbefore(indbefore) - nl/2; % Distance from the leftmost point of the selected segment to the point where the trajectory should end.
                        dafter = segment_l(indbefore) - dbefore;
                        
                        % y = mx + t
                        x1 = tr(indbefore, 1);
                        y1 = tr(indbefore, 2);
                        x2 = tr(indbefore+1, 1);
                        y2 = tr(indbefore+1, 2);
                        
                        if abs(x2 - x1) > 0.001
                                m = (y2 - y1) / (x2 - x1);
                                t = y1 - m * x1;
                                                
                                a = 1 + m * m;
                                b = - 2 * (x1 - m * (t - y1));
                                c = x1 * x1 + (t - y1) ^ 2 - dbefore ^ 2;
                                
                                % Solve for x3 and y3.
                                x3 = (- b + sqrt(b * b - 4 * a * c)) / (2 * a);
                                y3 = m * x3 + t;
                                
                                % But there are two solutions for x3. If this is the good one, the point we found
                                % should be dafter away from the center of the trajectory.
                                if abs(norm([x3 y3] - [x2 y2], 2) - dafter) > 0.001 % Tried to use eps here, but it does not work, it is too small a number.
                                        x3 = (- b - sqrt(b * b - 4 * a * c)) / (2 * a);
                                        y3 = m * x3 + t;
                                end
                        % If x is constant just find y.
                        else
                                x3 = x1;
                                y3 = y1 + dbefore;
                        end                                
                end        
                
                % Invert the trajectory.
                switch ii
                        case 1
                                xleft = x3;
                                yleft = y3;
                                indleft = indbefore + 1;
                                tr = tr(end:-1:1, :);
                                segment_l = segment_l(end:-1:1);
                        case 2
                                xright = x3;
                                yright = y3;
                                indright = size(tr, 1) - indbefore;
                                tr = tr(end:-1:1, :);
                                segment_l = segment_l(end:-1:1);
                end
                
                
        end % End for
        
        atr = round([xleft yleft slice_index; tr(indleft:indright, :); xright yright slice_index]); % Pixel coordinates are integers, so we need to round up the values. THIS WILL ALSO RESULT IN A LENGTH VALUE THAT IS SLIGHTLY DIFFERENT FROM nl.
      else
        atr = tr;
      end
end        
