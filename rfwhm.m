% [fwhm, imax, ileft, iright] = rfwhm(data, fraction)
%
% VERY SIMPLE function that APPROXIMATES the full width at half maximum of a data set. 
%
% DATA is a vector containing the curve.
% FRACTION is the fraction of the maximum where the width is calculated. Defaults to 0.5.
%
% FWHM is the width between the data points to the left and right of the maximum with a value that is closer to half of the maximum value.
% IMAX is the index in DATA of the maximum.
% ILEFT is the index smaller than IMAX where the data is closest to half the maximum value.
% IRIGHT is the index greater than IMAX where the data is closest to half the maximum value.

function [fwhm, imax, ileft, iright] = rfwhm(data, fraction)

if nargin < 2
        fraction = 0.5;
end

[maxval imax] = max(data);

halfval = maxval * fraction;

datadiff = abs(halfval - data);

[tmp ind] = sort(datadiff);

smaller_points = ind(find(ind <= imax));
greater_points = ind(find(ind >= imax));

ileft = smaller_points(1);
iright = greater_points(1);

fwhm = iright-ileft;

