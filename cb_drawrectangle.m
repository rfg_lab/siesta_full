% cb_drawrectangle(fig, coords_ul, coords_lr, brush_sz, color)
function cb_drawrectangle(fig, coords_ul, coords_lr, brush_sz, color)
udata = get(fig, 'UserData');

hold on;

w = coords_lr(1) - coords_ul(1);
h = coords_lr(2) - coords_ul(2);

if w == 0
    w = 1;
end

if h == 0
    h = 1;
end

% The coordinates to rectangle are always the upper left corner, width and height. Provide those values depending on the square drawn by the user.
if w > 0 & h > 0
    udata.lineh = rectangle('Position', [coords_ul(1), coords_ul(2), w, h]);
elseif h > 0
    udata.lineh = rectangle('Position', [coords_lr(1), coords_ul(2), abs(w), h]);
elseif w > 0
    udata.lineh = rectangle('Position', [coords_ul(1), coords_lr(2), w, abs(h)]);
else
    udata.lineh = rectangle('Position', [coords_lr(1), coords_lr(2), abs(w), abs(h)]);
end

set(udata.lineh, 'EdgeColor',color, 'LineWidth', brush_sz);

% Print rectangle dimensions.
fprintf('width = %.2f, height = %.2f\n', abs(w), abs(h));

%set(udata.lineh, 'XData', [coords_origin(1),coords_end(1)], 'YData', [coords_origin(2),coords_end(2)]);
set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
set(fig, 'UserData', udata);
% end
end