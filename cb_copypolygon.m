% *********************************************************************************
function ud = cb_copypolygon(fig)
ud = get(fig, 'UserData');
obj = get(fig, 'CurrentObject');

if strcmp(get(obj, 'Tag'), 'Polyline')
	x = get(obj, 'XData');
	y = get(obj, 'YData');
	z = ud.curslice .* ones(1, numel(x));
	ud.rpoly = [x' y' z'];
end

end