% THEMODE = RMODENZ(IM)
%      Returns the non-zero mode of dipimage IM. If the mode is zero or the maximum pixel value, RMODENZ will calculate the
%      next most frequent pixel value.
%
% IM is a dip_image. Currently, only greyscale, 2D images are supported.
%
% THEMODE is the most frequent pixel value.
%
% Rodrigo Fernandez-Gonzalez
% rodrigo.fernandez.gonzalez@utoronto.ca
% 20150427

function [themode] = rmodenz(im)

themode = mode(reshape(double(im), [1 numel(double(im))]));
theim = reshape(double(im), [1 numel(double(im))]);

if themode == max(theim)
   h = histc(theim, 0:max(theim));
   [tmp themode] = max(h(1:(end-1)));
   themode = themode-1;
elseif themode == 0
   h = histc(theim, 0:max(theim));
   [tmp themode] = max(h(2:end)); % No subtraction necessary here as we are ignoring the first element in the array.
end

