%[lh lv] = polylength(x, y, thefn, resx, resy)
%  Calculates the horizontal and vertical lengths of a polygon by superimposing a grid on top of it and computing
%  the average (or maximum, or median, ...) distances from side to side of the polygon.
%
%  x is columns, y is rows, which is the format used by SIESTA when storing points
%  thefn is a pointer to a function (e.g. @mean, @max, @median, ...) (default = @mean).
%  resx and resy contain the spacing of the "grid" overimposed on the polygon to calculate the different lengths.
%  They represent measurements along the x and the y axis (not rows and columns!). It is possible to obtain 
%  subpixel resolution by using values smaller than 1 (default = 1 for both).
function [lh lv] = polylength(x, y, thefn, resx, resy)

% Handling parameter errors and setting defaults.
if nargin < 2
        eval('help polylength');
        error('At least two parameters necessary.');
end
        
if nargin < 3
        thefn = @mean;
end
        
if nargin < 4
        resx = 1;
        resy = 1;
end
       
if nargin < 5
        resy = 1;
end

% Determine range of coordinates to scan.
minx = min(x);
miny = min(y);
maxx = max(x);
maxy = max(y);

%figure;plot(y, x, 'Color', [0 0.6 0], 'LineWidth', 3); xlim([minx-5 maxx+5]); ylim([miny-5 maxy+5]); axis ij; axis equal;

% Now intersect with horizontal lines.
yvector = (miny:resy:maxy);
dh = [];
for ii = 1:numel(yvector)
        xl = [minx maxx]; % The line goes across the cell.
        yl = [yvector(ii) yvector(ii)]; % At a certain height.
        [intx inty] = intersections(x, y, xl, yl); % Calculate the intersection.
        dh(ii) = intx(end)-intx(1); % And find the distance between the two most extreme points.
        %hold on;
        %plot(xl, yl, 'b'); % Paint grid lines.
        %plot(inty([1 end]), intx([1 end]), 'bo', 'MarkerSize', 5, 'MarkerFaceColor', 'b'); % Paint intersection points.
        %plot(inty([1 end]), intx([1 end]), 'b'); % Paint intersection points connected by a segment.
end
% Calculate horizontal length as the mean of all horizontal lengths. You can also use the maximum, minimum, median, ...
lh = feval(thefn, dh);

xvector = (minx:resx:maxx);
dv = [];
for ii = 1:numel(xvector)
        yl = [miny maxy]; % The line goes across the cell.
        xl = [xvector(ii) xvector(ii)]; % At a certain x coordinate.
        [intx inty] = intersections(x, y, xl, yl); % Calculate the intersection.
        dv(ii) = inty(end)-inty(1); % And find the distance between the two most extreme points.
        %hold on;
        %plot(xl, yl, 'Color', 'r');
        %plot(inty([1 end]), intx([1 end]), 'ro', 'MarkerSize', 5, 'MarkerFaceColor', 'r');
        %plot(inty([1 end]), intx([1 end]), 'r');
end
% Calculate vertical length as the mean of all vertical lengths. You can also use the maximum, minimum, median, ...
lv = feval(thefn, dv);
