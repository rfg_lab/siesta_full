% cancel = cb_saveavi(fig)
% cancel is 1 if the operation was canceled, 0 otherwise.
function cancel = cb_saveavi(fig)

default_fps = 7;

extensions_movies = {'*.avi'; '*.*'};

ud = get(fig, 'UserData');

curr_dir = pwd;
cd(ud.rcurrdir);
curr_tp = ud.curslice;

[filename, pathname, ext] = uiputfile(extensions_movies, 'Export movie ...');

cd(curr_dir);
if (~ isequal(filename, 0))
        answer = inputdlg('Frames per second ', 'Export movie ...', 1, {num2str(default_fps)});
        if isempty(answer)
                cancel = 1;
                cd(curr_dir);
                return;
        else
                fps = str2num(answer{1});
        end                
        
        fprintf('Processing ... ');
        clear M;
        for ii = 0:(ud.imsize(3)-1);
                mycallbacks2(fig, 'go_to', ii);
                M(ii+1) = getframe;
        end
        
        movie2avi(M, fullfile(pathname, filename), 'fps', fps);  
        
        mycallbacks2(fig, 'go_to', curr_tp); % Go back to the time point where everything started.
        
        fprintf('done!\n');
        
        cancel = 0;      
else
        cancel = 1;
end
end