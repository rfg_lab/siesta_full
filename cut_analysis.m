function [l a p ang_values lh lv lellipseh lellipsev center_x center_y] = cut_analysis(p1, p2, polygons, angles, cut_t, t_res, vis, save_flag, timeseries)

% Compute l: distance between two vertices.
% Compute a: area of the two cells connected by the ablated interface;
% and p: perimeter of the ensemble.
% and sf: shape of the wound.
l = zeros(1, size(p1, 1));

empty_polygons = 0; % Check if the polygons are empty
for ii = 1:numel(l)
        if isempty(polygons) || isempty(polygons{1, 1, ii})
                empty_polygons = 1;
                break;
        end        
end                

if ~ empty_polygons
	a = zeros(1, size(polygons, 3));
	p = zeros(1, size(polygons, 3));
	
	lh = zeros(1, size(polygons, 3));
	lv = zeros(1, size(polygons, 3));
        lellipseh = zeros(1, size(polygons, 3));
        lellipsev = zeros(1, size(polygons, 3));
        center_x = zeros(1, size(polygons, 3));
        center_y = zeros(1, size(polygons, 3));
	%sf = zeros(1, size(polygons, 3));
else
	a = [];
	p = [];
	lh = [];
	lv = [];
        lellipseh = [];
        lellipsev = [];
        center_x = [];
        center_y = [];
	%sf = [];
end

empty_fiducials = 0; % Check if fiducials are empty.
if isempty(p1) || isempty(p2)
        empty_fiducials = 1;
end                
if empty_fiducials
        l = zeros(1, size(polygons, 3));
end

ang_values = nan.*ones(size(p1, 1), size(angles, 1));

for t = 1:numel(l)
        if ~ empty_fiducials	
                l(t) = norm(p1(t, :) - p2(t, :), 2);	
        end                
	
	if ~ empty_polygons && nargin > 8 % An image needs to be defined.
                % The user can pass a image stack with one time point per row of p1.
                if ndims(timeseries) == 3 && size(timeseries, 3) > 1
                        theimage = timeseries(:, :, t-1);       
                % Or a single time point in a 3D image.
                elseif ndims(timeseries) == 3
                        theimage = timeseries(:, :, 0);
                % Or a single time point as a 2D image.
                else
                        theimage = timeseries;
                end                        
		m = measurepoly(cell(polygons(1, 1, t)), {'area'; 'perimeter'; 'center'; 'length'; 'length_ellipse'}, theimage);
		a(t) = m(1).Area;
		p(t) = m(1).Perimeter;
		%sf(t) = 4 * pi * a(t) / (p(t) * p(t));
		lh(t) = m(1).LengthH;
		lv(t) = m(1).LengthV;
                lellipseh(t) = m(1).LengthEllipseH;
                lellipsev(t) = m(1).LengthEllipseV;   
                center_x(t) = m(1).Center(1);
                center_y(t) = m(1).Center(2);
        elseif ~ empty_polygons
                m = measurepoly(cell(polygons(1, 1, t)), {'area'; 'perimeter'; 'center'; 'length'; 'length_ellipse'});
                a(t) = m(1).Area;
                p(t) = m(1).Perimeter; 
		lh(t) = m(1).LengthH;
		lv(t) = m(1).LengthV;  
                lellipseh(t) = m(1).LengthEllipseH;
                lellipsev(t) = m(1).LengthEllipseV;  
                center_x(t) = m(1).Center(1);
                center_y(t) = m(1).Center(2);
	end
	
	if ~ isempty(angles)
		for ii = 1:size(angles, 1)
			thepnts = angles{ii, :, t};
			
			if ~ isempty(thepnts)
				ang_values(t, ii) = measure_angle(thepnts(1, :), thepnts(2, :), thepnts(3, :));
			else
				ang_values(t, ii) = nan;
			end
		end
	end
end


% PLOTS
if (vis)
	cut_plots(l, a, p, cut_t, t_res, '', save_flag);
end
