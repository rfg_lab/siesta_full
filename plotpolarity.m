% binranks = plotpolarity(angles, intensities, bin_limits, <plot_flag>)
%
% Finds relative values for a group of intensity measurements classified by
% their angle, i.e., determines whether an angle presents higher intensity values when compared
% to another one.
% 
% ANGLES contains the angle of each one of the measurements.
% INTENSITITES contains the intensity of each one of those measurements.
% BIN_LIMITS is an array of values limiting each one of the angular bins. For example, if
% we want to measure angles in increments of 15 degrees, use (0:15:180). For more info
% see the help for histc.
% PLOT_FLAG (optional) determines whether plots are shown (1, default value) or not (0).
%   
% BINRANKS contains a relative intensity value for each one of the angular increments
% determined in bin_limits. In fact, this value is the average percentile for the
% intensity values contained in this bin.
% NBINRANKS contains a relative intensity value for each one of the angular increments
% determined in bin_limits.This value is the same percentile contained in BINRANKS but
% in the range [-0.5 0.5] rather than [0 100].
%
% Rodrigo Fernandez-Gonzalez
% fernanr1@mskcc.org
% 5/25/2007

function [binranks nbinranks binabs nbinabs] = plotpolarity(angles, intensities, bin_limits, plot_flag, norm_abs)

if nargin < 4
        plot_flag = 1;
end        

if nargin < 5
        norm_abs = 0.;
end        

% Rank all measurements based on intensity.
[tmp, I] = sort(intensities);
% Computes ranks averaging the rank in case of ties.
ranks = Ranks(intensities);


% Normalize ranks so that they expand from 0 to 1 (i.e. they indicate a percentile).
if (max(ranks) - min(ranks)) ~= 0
	ranks = (((ranks - min(ranks))*(1 - 0))/(max(ranks) - min(ranks)))+ 0;
else
	ranks = zeros(1, numel(intensities));
end

% Alternatively, normalize ranks so that maximum is 1 (minimum will not be zero!!). The final
% results should not change.
%ranks = ranks ./ numel(intensities);

%  HISTC(X,EDGES)  will count the value X(i) if EDGES(k) <= X(i) < EDGES(k+1).  The last bin will count any values of X that match EDGES(end). Thus, in case there is any value in X identical to the last one in EDGES, we increase this last one.
bin_limits(end) = bin_limits(end)+1;
% Bin each one of the angular measurements.
[n, bins] = histc(angles, bin_limits);
% And now put it back.
bin_limits(end) = bin_limits(end)-1;

% Find the average rank/absolute intensity per bin.
binranks = nan .* zeros(1, numel(bin_limits)-1);
binabs = nan .* zeros(1, numel(bin_limits)-1);
for ibin = 1:numel(n)
	if (n(ibin) ~= 0)
		binranks(ibin) = mean(ranks(find(bins == ibin)));	
                binabs(ibin) = mean(intensities(find(bins == ibin)));              
	end
end

% Change of coordinates so that 100th percentile corresponds to .5 
% and the 0th corresponds to -.5 rather than to 1 and 0 respectively.
nbinranks = (binranks - 0.5);
nbinabs = binabs - norm_abs;
%nbinabs(find(nbinabs < 0)) = 0.; % Clip zero values. Commenting this out allows intensities smaller than zero (e.g. if the edge intensity is lower than the cytoplasmic one).

% Plot result.
if plot_flag
        figure;
        bar(bin_limits(1:(end-1)) + (bin_limits(2) - bin_limits(1))/2, nbinranks);
        xlim([0 180]);
        xlabel('Angle (degrees)');
        set(gca, 'XTick', bin_limits);
        ylim([-.5 .5]);
        ylabel('Intensity (a.u.)');
        
        figure;
        bar(bin_limits(1:(end-1)) + (bin_limits(2) - bin_limits(1))/2, nbinabs);
        xlim([0 180]);
        xlabel('Angle (degrees)');
        set(gca, 'XTick', bin_limits);
        %ylim([-.5 .5]);
        ylabel('Intensity (abs, bg substracted)');
end