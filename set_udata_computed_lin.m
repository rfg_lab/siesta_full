% *********************************************************************************


%
% Fill-in udata.computed elements, if they don't exist yet.
%
function udata = set_udata_computed_lin(udata,cdata)
if ~isfield(udata,'computed') | ~isfield(udata.computed,'lin')
   if length(udata.imsize)==3 & udata.globalstretch
      if ~isempty(udata.colspace) & ~strcmp(udata.colspace,'RGB')
         warning('Cannot perform global stretching on non-RGB color images.')
         % Don't worry about this. It cannot happen.
      else
         cdata = udata.slices;
         if ~isempty(udata.colspace)
            lin = [inf,-inf];
            for ii=1:udata.channels
               tmp = mapcomplexdata(cdata{ii},udata.complexmapping);
               lin = [min(min(tmp),lin(1)),max(max(tmp),lin(2))];
            end
            return
         else
            cdata = mapcomplexdata(cdata,udata.complexmapping);
         end
      end
   end
   udata.computed.lin = [min(cdata),max(cdata)];
end
udata.computed.lin = [min(cdata),max(cdata)];
end


