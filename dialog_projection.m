function varargout = dialog_projection(varargin)
% DIALOG_PROJECTION M-file for dialog_projection.fig
%      H = DIALOG_PROJECTION([min max]) returns the handle to a new DIALOG_PROJECTION
%	   containing the parameters for the segmentation function.




%      DIALOG_PROJECTION, by itself, creates a new DIALOG_PROJECTION or raises the existing
%      singleton*.
%
%      H = DIALOG_PROJECTION returns the handle to a new DIALOG_PROJECTION or the handle to
%      the existing singleton*.
%
%      DIALOG_PROJECTION('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DIALOG_PROJECTION.M with the given input arguments.
%
%      DIALOG_PROJECTION('Property','Value',...) creates a new DIALOG_PROJECTION or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before dialog_projection_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to dialog_projection_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help dialog_projection

% Last Modified by GUIDE v2.5 18-Jan-2008 17:44:39

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @dialog_projection_OpeningFcn, ...
                   'gui_OutputFcn',  @dialog_projection_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before dialog_projection is made visible.
function dialog_projection_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to dialog_projection (see VARARGIN)

% Choose default command line output for dialog_projection
handles.output = hObject;

% Parameter values for components. RODRI EDITED HERE.
% THIS HAS TO BE BEFORE guidata(hObject, handles)!!!!
set(handles.figure1, 'name', 'Project');
warning('off', 'all'); % This is necessary to remove a warning
					   % caused by a Matlab bug.
ud = varargin{1};
timepoints = [0 (ud.imsize(3) - 1)]; % This variable can represent time points, z slices, ...
tps = timepoints(1):timepoints(2);
handles.tps = tps;
ptypes = varargin{2};
handles.ptypes = ptypes;
handles.closemethod = -1;
str = [];

for i = 1:numel(tps)
	str = cat(2, str, cat(2, num2str(tps(i)), '|'));
end

str = str(1:(end-1));

str_ptype = [];

for i = 1:numel(ptypes)
	str_ptype = cat(2, str_ptype, cat(2, ptypes{i}, '|'));
end

str_ptype = str_ptype(1:(end-1));


set(handles.popupmenu1, 'String', str, 'Value', ud.curslice+1);
set(handles.popupmenu2, 'String', str, 'Value', ud.curslice+1);
set(handles.popupmenu3, 'String', str_ptype, 'Value', 1);

% Update handles structure.
guidata(hObject, handles);


% UIWAIT makes dialog_projection wait for user response (see UIRESUME)
uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = dialog_projection_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
%varargout{1} = handles.output;
%get(handles.figure1)
%uiwait(handles.figure1);

d = guidata(handles.figure1);

if d.closemethod == 1
    tp(1) = handles.tps(get(handles.popupmenu1, 'Value'));
    tp(2) = handles.tps(get(handles.popupmenu2, 'Value'));
    tp = sort(tp);
    ptype = handles.ptypes{get(handles.popupmenu3, 'Value')};
    
else
    tp = [-1 -1];
    ptype = 0;
end

close(handles.figure1);
	
varargout{1} = (tp(1):tp(2));
varargout{2} = ptype;


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1

selected = get(handles.popupmenu1,'Value');
prev_str = get(handles.popupmenu1, 'String');
set(handles.popupmenu1, 'String', prev_str, 'Value', selected);


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2
selected = get(handles.popupmenu2,'Value');
prev_str = get(handles.popupmenu2, 'String');
set(handles.popupmenu2, 'String', prev_str, 'Value', selected);



% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%set(handles.figure1, 'Visible', 'off');
handles.closemethod = 1;
guidata(handles.figure1, handles);
uiresume(handles.figure1);



% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.closemethod = 0;
guidata(handles.figure1, handles);
uiresume(handles.figure1);





% --- Executes on selection change in popupmenu3.
function popupmenu3_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu3


% --- Executes during object creation, after setting all properties.
function popupmenu3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


