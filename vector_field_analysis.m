function [dr_um dth_um cut_p1_um cut_p1_rot_angle_deg x_um y_um] = vector_field_analysis(p1, p2, vertex1_before, vertex2_before, cut, vis, save_flag)

% p1 is the list of vertex positions before cutting.
% p2 is the list of vertex positions 60s after cut.

% IS THIS USED ANYWHERE?
%frame_before = 77;
%frame_after = 137;

% The vertices that were connected by the ablated interface.
%vertex1_before = [63 70];
%vertex2_before = [81 73];

% This is the position of the cut. 
cut_x = cut(1);
cut_y = cut(2);

um_per_pix = 1/4.9;

% Distance that each vertex moved after the cut.
p1_p2_vectors = p2 - p1;
p1_p2_vectors_um = p1_p2_vectors .* um_per_pix;
p1_p2 = (sum(p1_p2_vectors .^ 2, 2)) .^ 0.5; % Uncorrected.

% Vectors from each one of the vertex positions before the cut
% and the cut itself.
cut_p1_vectors = p1 - (repmat([cut_x cut_y], [size(p1, 1) 1]));

% Vector lengths in pixels and microns.
cut_p1 = (sum(cut_p1_vectors .^ 2, 2)) .^ 0.5;
cut_p1_um = cut_p1 .* um_per_pix;

% Vectors along the sectioned interface.
cut_vector = vertex2_before - vertex1_before;

% Sine and cosine of the severed angle.
cut_vector_sin = cut_vector(2) / norm(cut_vector, 2);
cut_vector_cos = cut_vector(1) / norm(cut_vector, 2);

% Vectors from cut point to vertices are rotated so that the
% cut vector is oriented at zero degrees.
cut_p1_vectors_rot = zeros(size(cut_p1_vectors));
cut_p1_vectors_rot(:, 1) = cut_vector_cos .* cut_p1_vectors(:, 1) + cut_vector_sin .* cut_p1_vectors(:, 2);
cut_p1_vectors_rot(:, 2) = - cut_vector_sin .* cut_p1_vectors(:, 1) + cut_vector_cos .* cut_p1_vectors(:, 2);

% Cut angles after rotation. asin only returns values between 
% pi/2 and -pi/2. Therefore, it is necessary to convert to values 
% between 0 and 2pi. 
cut_p1_rot_sin = cut_p1_vectors_rot(:, 2) ./ cut_p1;
cut_p1_rot_cos = cut_p1_vectors_rot(:, 1) ./ cut_p1;
cut_p1_rot_angle = atan(cut_p1_rot_sin./cut_p1_rot_cos);

first_quad = find(cut_p1_rot_sin > 0 & cut_p1_rot_cos > 0);

second_quad = find(cut_p1_rot_sin > 0 & cut_p1_rot_cos < 0);
cut_p1_rot_angle(second_quad) = pi + cut_p1_rot_angle(second_quad);

third_quad = find(cut_p1_rot_sin < 0 & cut_p1_rot_cos < 0);
cut_p1_rot_angle(third_quad) = pi + cut_p1_rot_angle(third_quad);

fourth_quad = find(cut_p1_rot_sin < 0 & cut_p1_rot_cos > 0);
cut_p1_rot_angle(fourth_quad) = 2 * pi + cut_p1_rot_angle(fourth_quad);

cut_p1_rot_angle_deg = 180 .* cut_p1_rot_angle / pi;

first_quad_rot = first_quad;
second_quad_rot = second_quad;
third_quad_rot = third_quad;
fourth_quad_rot = fourth_quad;


% Cut angles before rotation. asin only returns values between 
% pi/2 and -pi/2. Therefore, it is necessary to convert to values 
% between 0 and 2pi. 
cut_p1_sin = cut_p1_vectors(:, 2) ./ cut_p1;
cut_p1_cos = cut_p1_vectors(:, 1) ./ cut_p1;
cut_p1_angle = atan(cut_p1_sin./cut_p1_cos);

first_quad = find(cut_p1_sin > 0 & cut_p1_cos > 0);

second_quad = find(cut_p1_sin > 0 & cut_p1_cos < 0);
cut_p1_angle(second_quad) = pi + cut_p1_angle(second_quad);

third_quad = find(cut_p1_sin < 0 & cut_p1_cos < 0);
cut_p1_angle(third_quad) = pi + cut_p1_angle(third_quad);

fourth_quad = find(cut_p1_sin < 0 & cut_p1_cos > 0);
cut_p1_angle(fourth_quad) = 2 * pi + cut_p1_angle(fourth_quad);

cut_p1_angle_deg = 180 .* cut_p1_angle / pi;


% Substract the average motion before rotation from each one of the
% vertex initial and final positions.
p1_p2_vectors(:, 1) = p1_p2_vectors(:, 1) - sum(p1_p2_vectors(:, 1))./numel(p1_p2_vectors(:, 1));
p1_p2_vectors(:, 2) = p1_p2_vectors(:, 2) - sum(p1_p2_vectors(:, 2))./numel(p1_p2_vectors(:, 2));
p1_p2_vectors_um(:, 1) = p1_p2_vectors(:, 1) .* um_per_pix;
p1_p2_vectors_um(:, 2) = p1_p2_vectors(:, 2) .* um_per_pix;

% Motion (corrected for average movement) along X and Y.
% ATTENTION: THE CORRECTIONS FOR SIGN ARE DOING IGNORING THE ROTATION!!!
x_um = p1_p2_vectors_um(:, 1);
% In the second and third quads, movement away from the cut results in negative X (p2.x < p1.x) and viceversa. Here, we invert the signs.
x_um(second_quad) = -1 .* x_um(second_quad);
x_um(third_quad) = -1 .* x_um(third_quad);

y_um = p1_p2_vectors_um(:, 2);
% In the third and fourth quads, movement away from the cut results in negative Y (p2.y < p1.y) and viceversa. Here, we invert the signs. AND YES, THIS IS RIGHT, AS IT TURNS OUT THE MATLAB CONVENTION SITUATES THE FIRST QUADRANT AT THE SOUTHEAST OF AN IMAGINARY COMPASS, THE SECOND QUADRANT AT THE SOUTHWEST AND THE THIRD AND FOURTH QUADRANTS AT THE NORTHWEST AND NORTHEAST RESPECTIVELY.
y_um(third_quad) = -1 .* y_um(third_quad);
y_um(fourth_quad) = -1 .* y_um(fourth_quad);

% Radial displacement.
dr = cut_p1_cos .* p1_p2_vectors(:, 1) + cut_p1_sin .* p1_p2_vectors(:, 2);
dr_um = dr .* um_per_pix;

% Tangential displacement.
dth = - cut_p1_sin .* p1_p2_vectors(:, 1) + cut_p1_cos .* p1_p2_vectors(:, 2);
dth_um = dth .* um_per_pix;

% PLOTS
% dr vs angle (coded by distance) -------------------------------------
if (vis)
	vector_field_plots(dr_um, dth_um, cut_p1_um, cut_p1_rot_angle_deg, '', save_flag);
end
