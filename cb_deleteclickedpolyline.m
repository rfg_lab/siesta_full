% *********************************************************************************

function [polygons2 ind] = cb_deleteclickedpolyline(fig, polygons)

polygons2 = polygons;

ind = -1;
 
% if strncmp(get(fig,'Tag'),'DIP_Image',9)
   ln = get(fig,'CurrentObject');   
   ud = get(fig, 'UserData');
   curslice = ud.curslice + 1;
   slicepoly = cell(0);
   if strcmp(get(ln,'Tag'),'Polyline')
   	 polygons2 = polygons;
   	 xdata = get(ln, 'XData');
	 ydata = get(ln, 'YData');

	 [slicepoly{1:size(polygons2, 1)}] = polygons2{:, 1, curslice};
	 for i = 1:size(polygons2, 1)
	 	thepoly = slicepoly{i};
		
		if ((size(thepoly, 1) == numel(xdata)) & (nnz([xdata' ydata'] == thepoly(:, 1:2)) == numel(thepoly(:, 1:2))))
			ind = i;
			break;			
		end
	 end
	 
	 if ind > 0
	 	polygons2{ind, 1, curslice} = [];
		%disp('Deleted angle');
	 end
	 
	 delete(ln);
   end
% end
end
