function varargout = mycallbacks2D(varargin)

fig = varargin{1};
action = varargin{2};
ud = get(fig, 'UserData');
geometry = ud.geometry;
nodes = geometry(ud.index).nodes;
edges = geometry(ud.index).edges;
color = 'r';
color_selected = 'g';
roi_color = 'b';
bg_color = 'm';
min_l = 3; % Minimum interface length.
intensity_measurements = 'Relative to local DV|Scaled (0-1)|Background-corrected (relative to DV)|Background-corrected (relative to local DV)|Above local DV|Background and local DV substracted';
MIND = 10; % Minimum radius around the current interface to measure reference interfaces.
MININT = 10; % Minimum number of interfaces to measure for reference intensity in modes 1 and 4.

switch (action)
	case {'add_node', 'k_n'}
		set(fig, 'WindowButtonDownFcn', 'mycallbacks2D(gcbf, ''add_rem_node_down'');');
		set(fig, 'WindowButtonUpFcn', '');
		set(fig, 'Pointer', 'crossh');
	case 'no_add_node'
		set(fig, 'WindowButtonDownFcn', '');
		set(fig, 'WindowButtonUpFcn', '');
		set(fig, 'Pointer', 'arrow');

	case 'add_rem_node_down'
		% Add with left button, remove with right button.
		% Left-click.
		if ((~ strcmp(get(fig,'SelectionType'),'alt')) && (~ (strcmp(get(fig,'SelectionType'),'extend'))))
			ud = cb_clearedgeselection(fig, color);
			[nodes edges move_node_index] = cb_addnode(fig, nodes, edges, color, color_selected);
			geometry(ud.index).nodes = nodes;
			geometry(ud.index).edges = edges;
			ud.geometry = geometry;
			ud.move_node_index = move_node_index;
			set(fig, 'UserData', ud);
		% Right-click.			
		elseif strcmp(get(fig,'SelectionType'),'alt')
			coords = get(get(fig, 'CurrentAxes'), 'CurrentPoint');
			coords = round(coords(1, 1:2));
			[nodes edges] = cb_deletenode(fig, nodes, edges, coords);
			geometry(ud.index).nodes = nodes;
			geometry(ud.index).edges = edges;
			ud.geometry = geometry;
			set(fig, 'UserData', ud);
		end	

	case 'move_node_down'
		[nodes edges] = cb_movenode(fig, nodes, ud.move_node_index, edges, color, color_selected);
		geometry(ud.index).nodes = nodes;
		geometry(ud.index).edges = edges;
		ud.geometry = geometry;
		set(fig, 'UserData', ud);
	
	case 'no_move_node_down'
		ud = cb_clearedgeselection(fig, color);
		%[nodes edges] = cb_addnode(fig, nodes, edges, color, color_selected);
		[nodes edges] = cb_movenode(fig, nodes, ud.move_node_index, edges, color, color_selected);
		geometry(ud.index).nodes = nodes;
		geometry(ud.index).edges = edges;
		ud.geometry = geometry;
		set(fig, 'UserData', ud);
		
		set(fig, 'WindowButtonUpFcn', '');
		set(fig, 'WindowButtonMotionFcn', '');
		
	case {'add_edge', 'k_e'}
		set(fig, 'WindowButtonDownFcn', 'mycallbacks2D(gcbf, ''add_rem_edge_down'');');
		set(fig, 'WindowButtonUpFcn', '');
		set(fig, 'Pointer', 'crossh');

	case 'no_add_edge'
		set(fig, 'WindowButtonDownFcn', '');
		set(fig, 'WindowButtonUpFcn', '');
		set(fig, 'Pointer', 'arrow');

	case 'add_rem_edge_down'
		% Add with left button, remove with right button.
		% Left-click.
		if ((~ strcmp(get(fig,'SelectionType'),'alt')) && (~ (strcmp(get(fig,'SelectionType'),'extend'))))
			ind = cb_getnode(fig, nodes);
			if ind > 0
				ud.curr_node = ind;
				set(fig, 'UserData', ud);
				set(fig, 'WindowButtonDownFcn', 'mycallbacks2D(gcbf, ''add_edge_second_down'');');
				set(fig, 'WindowButtonUpFcn', '');
				set(fig, 'Pointer', 'crossh');
			end
		% Right-click.			
		elseif strcmp(get(fig,'SelectionType'),'alt')
			ud = cb_clearedgeselection(fig, color);
			[nodes edges] = cb_deleteedge(fig, nodes, edges, color, color_selected, 1);
			geometry(ud.index).nodes = nodes;
			geometry(ud.index).edges = edges;
			ud.geometry = geometry;
			set(fig, 'UserData', ud);
		end	
		
	case 'add_edge_second_down'
		% Left-click.
		if ((~ strcmp(get(fig,'SelectionType'),'alt')) && (~ (strcmp(get(fig,'SelectionType'),'extend'))))
			ind = cb_getnode(fig, nodes);
			if ind > 0
				edges = cb_addedge(fig, nodes, edges, ud.curr_node, ind, color, color_selected);
				geometry(ud.index).edges = edges;
				ud.geometry = geometry;
				set(fig, 'UserData', ud);
				set(fig, 'WindowButtonDownFcn', 'mycallbacks2D(gcbf, ''add_rem_edge_down'');');
				set(fig, 'WindowButtonUpFcn', '');
				set(fig, 'Pointer', 'crossh');
			end
		end


		
		
				
	case {'inspect_edge', 'k_t'}
		set(fig, 'WindowButtonDownFcn', 'mycallbacks2D(gcbf, ''inspect_edge_down'');');
		set(fig, 'WindowButtonUpFcn', '');
		set(fig, 'Pointer', 'crossh');              

	case 'inspect_edge_down'
		 cb_inspectedge(fig, MIND, MININT);
		
        case 'k_C'
                 [chain_lengths szav n normAPn normMYOn normALLn pALL pMYO pAP chains] = measure_myocablestats(ud.geometry(ud.index), ud.rselectededges{ud.index}, ud.roi, ud.paramminl, ud.paramang);
                 fprintf('\nDistribution of cluster sizes:\n');
                 disp(n);
                 fprintf('Average cluster size (weighted): %.2f\n', szav);
				
	case {'prev_geom', 'k_,'}
		if ud.index > 1
			slice2 = [];
			xl = xlim;
			yl = ylim;
			ud.index = ud.index - 1;
			set(fig, 'UserData', ud);
			ch = get(gca, 'Children');
			delete(ch);
			
			index = ud.index;
			imshift = ud.imshift;
			ang = ud.ang;
			prefix = ud.prefix;
			slice_list = ud.slice_list;
			
			if strcmp(ud.fileformat, 'ultraview')
				time_str = rnum2str(index+imshift, 4);
				set(fig, 'Name', cat(2, prefix, time_str));
				
				% ud.ang is saved in radians. ang = ang .* pi / 180; is
				% not necessary therefore.
				if numel(slice_list) > 1
					files = dir(cat(2, prefix, cat(2, time_str, '_568nm_Z*')));
			
					% By default, read red channel (I assume if you want to browse the
					% green channel you will use the 'im_segm_T' images produced by
					% the segmentation.
					if numel(files) == 0
						im = readtimeseries(cat(2, prefix, cat(2, time_str, '_488nm_Z.tif')));
					else
						im = readtimeseries(cat(2, prefix, cat(2, time_str, '_568nm_Z.tif')));
						
						slice2 = readim(cat(2, 'im_segm_T', cat(2, time_str, '.ics')));
						slice2 = squeeze(slice2(:, :, 1));
					end
					slice = stretch(rotation(stretch(dip_image(sum(double(im(0:end, 0:end, slice_list)), 3))), ang, 3, 'zoh'), 0, 100, 0, 255);
                                        %slice = stretch(rotation(stretch(dip_image(max(double(im(0:end, 0:end, slice_list)),[], 3))), ang, 3, 'zoh'), 0, 100, 0, 255);              
                                        %slice = rotation(dip_image(max(double(im(0:end, 0:end, slice_list)),[], 3)), ang, 3, 'zoh');                                   
				else
					im = expanddim(readim(cat(2, prefix, time_str)), 3);
			
					slice = rotation(im(:, :, slice_list), ang, 3, 'zoh');                                             		
			
				end
			elseif strcmp(ud.fileformat, 'metamorph')
				time_str = rnum2str(index+imshift, 1);
				set(fig, 'Name', cat(2, prefix, time_str));
				
				% ud.ang is saved in radians. ang = ang .* pi / 180; is
				% not necessary therefore.
				if numel(slice_list) > 1
					files = dir(cat(2, prefix, '_w2568-power_t*'));
			
					% By default, read red channel (I assume if you want to browse the
					% green channel you will use the 'im_segm_T' images produced by
					% the segmentation.
					if numel(files) == 0
						im = tiffread(cat(2, prefix, cat(2, '_w1488-power_t', cat(2, time_str, '.TIF'))));
					else
						im = tiffread(cat(2, prefix, cat(2, '_w2568-power_t', cat(2, time_str, '.TIF'))));
						
                                                % Try to read the "green" image used for segmentation. This is a single slice.                    
                                                try                     
                                                        slice2 = readim(cat(2, 'im_segm_T', cat(2, time_str, '.ics')));
                                                        slice2 = squeeze(slice2(:, :, 1));
                                        % If not present, just go with im.        
                                                catch
                                                        slice2 = stretch(rotation(stretch(dip_image(sum(double(im(0:end, 0:end, slice_list)), 3))), ang, 3, 'zoh'), 0, 100, 0, 255);         slice2 = squeeze(slice2);                
                                                end                            
					end
					slice = stretch(rotation(stretch(dip_image(sum(double(im(0:end, 0:end, slice_list)), 3))), ang, 3, 'zoh'), 0, 100, 0, 255);
                                        %slice = stretch(rotation(stretch(dip_image(max(double(im(0:end, 0:end, slice_list)),[], 3))), ang, 3, 'zoh'), 0, 100, 0, 255);              
                                        %slice = rotation(dip_image(max(double(im(0:end, 0:end, slice_list)),[], 3)), ang, 3, 'zoh');                                   
				else
					im = expanddim(readim(cat(2, prefix, time_str)), 3);
			
					slice = rotation(im(:, :, slice_list), ang, 3, 'zoh');
				end
			end
			
			imshow(double(slice), [0 255]);
			
			roi = [];

			if (exist(cat(2, cat(2, 'roi_T', time_str), '.mat'), 'file'))
				vars = load(cat(2, cat(2, 'roi_T', time_str), '.mat'));
				roi = vars.roi;
			else
				% This bit could be outside the loop, but thus, we keep the code
				% compact and easy to copy and paste.
				d = dir('./roi_T*');
		
				% There are ROIs defined.
				if (numel(d) > 0)
					rois = zeros(1, numel(d));
			
					for iroi = 1:numel(d)
						sroi = d(iroi).name;
						iT = strfind(sroi, '_T');
						rois(iroi) = str2num(sroi((iT+2):(iT+5)));
					end		
					% End this could be outside the loop.
					roisd = abs(rois - index - imshift);
					[tmproi roi_index] = min(roisd);
					roi_index = rois(roi_index);
	
					time_str_roi = rnum2str(roi_index, 4);
	
					vars = load(cat(2, cat(2, 'roi_T', time_str_roi), '.mat'));
					roi = vars.roi;
		
				% If no ROI has been defined, do nothing.
				else
					disp('No roi defined.');
				end
			end
			
			ud.roi = roi;
			
			if ~ isempty(roi)
				h = rectangle('Position', [roi(1, 1), roi(1, 2), abs(roi(2, 1) - roi(1, 1)), abs(roi(2, 2) - roi(1, 2))]);
				set(h, 'EdgeColor', 'b');
			end
			
			% Slowest bit.
			plot_geometry(ud.geometry(ud.index).nodes, ud.geometry(ud.index).edges, color, 0);
			cb_paintedgeselection(fig, ud.geometry(ud.index).nodes, ud.geometry(ud.index).edges, color_selected, color);
			% End of slowest bit.
			
			if ~ isempty(slice2)
				ud.colordata = joinchannels('rgb', slice, slice2);
				ud.imagedata = ud.colordata{1};
			elseif isfield(ud, 'colordata')
				ud = ud.rmfield(ud, 'colordata');
			end
			
			set(gca, 'Visible', 'off');
			set(gcf, 'UserData', ud);
		
			%xlim(xl);
			%ylim(yl);
			
			if ~ isempty(roi)
				xlim([roi(1, 1)-25 roi(2, 1)+25]);
				ylim([roi(1, 2)-25 roi(2, 2)+25]);
			else
				xlim([0 ud.imsize(2)-1]);
				xlim([0 ud.imsize(1)-1]);
			end
		% If you hit previous on the first, jump to the last time point.
		else
			ud.index = numel(ud.geometry);
			set(fig, 'UserData', ud);
			mycallbacks2D(fig, 'go_to');
		end
		
	case {'next_geom', 'k_.'}
		if ud.index < numel(geometry)
			slice2 = [];
			xl = xlim;
			yl = ylim;
			ud.index = ud.index + 1;
			set(fig, 'UserData', ud);
			ch = get(gca, 'Children');
			delete(ch);
			
			index = ud.index;
			imshift = ud.imshift;
			ang = ud.ang;
			prefix = ud.prefix;
			slice_list = ud.slice_list;
			
			if strcmp(ud.fileformat, 'ultraview')
				time_str = rnum2str(index+imshift, 4);
				set(fig, 'Name', cat(2, prefix, time_str));
		
				% ud.ang is saved in radians. ang = ang .* pi / 180; is
				% not necessary therefore.
				if numel(slice_list) > 1
					files = dir(cat(2, prefix, cat(2, time_str, '_568nm_Z*')));
			
					% If it exists, read red channel (I assume if you want to browse the
					% green channel you will use the 'im_segm_T' images produced by
					% the segmentation.
					if numel(files) == 0
						im = readtimeseries(cat(2, prefix, cat(2, time_str, '_488nm_Z.tif')));
					else
						im = readtimeseries(cat(2, prefix, cat(2, time_str, '_568nm_Z.tif')));
						
						slice2 = readim(cat(2, 'im_segm_T', cat(2, time_str, '.ics')));
						slice2 = squeeze(slice2(:, :, 1));
					end
					slice = stretch(rotation(stretch(dip_image(sum(double(im(0:end, 0:end, slice_list)), 3))), ang, 3, 'zoh'), 0, 100, 0, 255);
                                        %slice = stretch(rotation(stretch(dip_image(max(double(im(0:end, 0:end, slice_list)),[], 3))), ang, 3, 'zoh'), 0, 100, 0, 255);              
                                        %slice = rotation(dip_image(max(double(im(0:end, 0:end, slice_list)),[], 3)), ang, 3, 'zoh');
				else
					im = expanddim(readim(cat(2, prefix, time_str)), 3);
					slice = rotation(im(:, :, slice_list), ang, 3, 'zoh');
				end
			elseif strcmp(ud.fileformat, 'metamorph')
				time_str = rnum2str(index+imshift, 1);
				set(fig, 'Name', cat(2, prefix, time_str));
				
				% ud.ang is saved in radians. ang = ang .* pi / 180; is
				% not necessary therefore.
				if numel(slice_list) > 1
					files = dir(cat(2, prefix, '_w2568-power_t*'));
			
					% By default, read red channel (I assume if you want to browse the
					% green channel you will use the 'im_segm_T' images produced by
					% the segmentation.
					if numel(files) == 0
						im = tiffread(cat(2, prefix, cat(2, '_w1488-power_t', cat(2, time_str, '.TIF'))));
					else
						im = tiffread(cat(2, prefix, cat(2, '_w2568-power_t', cat(2, time_str, '.TIF'))));
						
                                                % Try to read the "green" image used for segmentation. This is a single slice.                    
                                                try                     
                                                        slice2 = readim(cat(2, 'im_segm_T', cat(2, time_str, '.ics')));
                                                        slice2 = squeeze(slice2(:, :, 1));
                                                % If not present, just go with im.        
                                                catch
                                                        slice2 = stretch(rotation(stretch(dip_image(sum(double(im(0:end, 0:end, slice_list)), 3))), ang, 3, 'zoh'), 0, 100, 0, 255);         slice2 = squeeze(slice2);                
                                                end                            
					end
					slice = stretch(rotation(stretch(dip_image(sum(double(im(0:end, 0:end, slice_list)), 3))), ang, 3, 'zoh'), 0, 100, 0, 255);
                                        %slice = stretch(rotation(stretch(dip_image(max(double(im(0:end, 0:end, slice_list)),[], 3))), ang, 3, 'zoh'), 0, 100, 0, 255);              
                                        %slice = rotation(dip_image(max(double(im(0:end, 0:end, slice_list)),[], 3)), ang, 3, 'zoh');
				else
					im = expanddim(readim(cat(2, prefix, time_str)), 3);
			
					slice = rotation(im(:, :, slice_list), ang, 3, 'zoh');
				end
			end

			imshow(double(slice), [0 255]);
		    
			roi = [];

			if (exist(cat(2, cat(2, 'roi_T', time_str), '.mat'), 'file'))
				vars = load(cat(2, cat(2, 'roi_T', time_str), '.mat'));
				roi = vars.roi;
			else
				% This bit could be outside the loop, but thus, we keep the code
				% compact and easy to copy and paste.
				d = dir('./roi_T*');
		
				% There are ROIs defined.
				if (numel(d) > 0)
					rois = zeros(1, numel(d));
			
					for iroi = 1:numel(d)
						sroi = d(iroi).name;
						iT = strfind(sroi, '_T');
						rois(iroi) = str2num(sroi((iT+2):(iT+5)));
					end		
					% End this could be outside the loop.
					roisd = abs(rois - index - imshift);
					[tmproi roi_index] = min(roisd);
					roi_index = rois(roi_index);
	
					time_str_roi = rnum2str(roi_index, 4);
	
					vars = load(cat(2, cat(2, 'roi_T', time_str_roi), '.mat'));
					roi = vars.roi;
		
				% If no ROI has been defined, do nothing.
				else
					disp('No roi defined.');
				end
			end
			
			ud.roi = roi;
			
			if ~ isempty(roi)
				h = rectangle('Position', [roi(1, 1), roi(1, 2), abs(roi(2, 1) - roi(1, 1)), abs(roi(2, 2) - roi(1, 2))]);
				set(h, 'EdgeColor', 'b');
			end
			
			plot_geometry(ud.geometry(ud.index).nodes, ud.geometry(ud.index).edges, color, 0);
			cb_paintedgeselection(fig, ud.geometry(ud.index).nodes, ud.geometry(ud.index).edges, color_selected, color);
			
			if ~ isempty(slice2)
				ud.colordata = joinchannels('rgb', slice, slice2);
				ud.imagedata = ud.colordata{1};
			elseif isfield(ud, 'colordata')
				ud = ud.rmfield(ud, 'colordata');
			end
			
			set(gca, 'Visible', 'off');
			set(gcf, 'UserData', ud);
			if ~ isempty(roi)
				xlim([roi(1, 1)-25 roi(2, 1)+25]);
				ylim([roi(1, 2)-25 roi(2, 2)+25]);
			else
				xlim([0 ud.imsize(2)-1]);
				xlim([0 ud.imsize(1)-1]);
			end
		% If you hit next on the last, jump to the first time point.
		else
			ud.index = 1;
			set(fig, 'UserData', ud);
			mycallbacks2D(fig, 'go_to');
		end

	case {'ask_n_goto', 'k_g'}
		if numel(ud.geometry) > 1 & nargin < 3
			tpo = dialog_timepoint([(1+ud.imshift) (numel(ud.geometry)+ud.imshift)]);
			tpo = tpo - ud.imshift;
		elseif numel(ud.geometry) > 1 & nargin == 3
			tpo = varargin{3};
                else
                        tpo = 1;                     
		end
		
		if tpo > 0
			ud.index = tpo;
			set(fig, 'UserData', ud);
			mycallbacks2D(fig, 'go_to');
		end

	case {'go_to'} % Unlike in next or prev, here ud.index already contains the time point you want to move to.
		if ud.index > 0
			slice2 = [];
			xl = xlim;
			yl = ylim;
			ch = get(gca, 'Children');
			delete(ch);
			
			index = ud.index;
			imshift = ud.imshift;
			ang = ud.ang;
			prefix = ud.prefix;
			slice_list = ud.slice_list;
			
			if strcmp(ud.fileformat, 'ultraview')
				time_str = rnum2str(index+imshift, 4);
				set(fig, 'Name', cat(2, prefix, time_str));
			
				% ud.ang is saved in radians. ang = ang .* pi / 180; is
				% not necessary therefore.
				if numel(slice_list) > 1
					files = dir(cat(2, prefix, cat(2, time_str, '_568nm_Z*')));
		
					% By default, read red channel (I assume if you want to browse the
					% green channel you will use the 'im_segm_T' images produced by
					% the segmentation.
					if numel(files) == 0
						im = readtimeseries(cat(2, prefix, cat(2, time_str, '_488nm_Z.tif')));
					else
						im = readtimeseries(cat(2, prefix, cat(2, time_str, '_568nm_Z.tif')));
					
						slice2 = readim(cat(2, 'im_segm_T', cat(2, time_str, '.ics')));
						slice2 = squeeze(slice2(:, :, 1));
					end
					
					slice = stretch(rotation(stretch(dip_image(sum(double(im(0:end, 0:end, slice_list)), 3))), ang, 3, 'zoh'), 0, 100, 0, 255);
                                        %slice = stretch(rotation(stretch(dip_image(max(double(im(0:end, 0:end, slice_list)),[], 3))), ang, 3, 'zoh'), 0, 100, 0, 255);              
                                        %slice = rotation(dip_image(max(double(im(0:end, 0:end, slice_list)),[], 3)), ang, 3, 'zoh');
				else
					im = expanddim(readim(cat(2, prefix, time_str)), 3);
		
					slice = rotation(im(:, :, slice_list), ang, 3, 'zoh');
				end
			
			elseif strcmp(ud.fileformat, 'metamorph')
				time_str = rnum2str(index+imshift, 1);
				set(fig, 'Name', cat(2, prefix, time_str));
				
				% ud.ang is saved in radians. ang = ang .* pi / 180; is
				% not necessary therefore.
				if numel(slice_list) > 1
					files = dir(cat(2, prefix, '_w2568-power_t*'));
			
					% By default, read red channel (I assume if you want to browse the
					% green channel you will use the 'im_segm_T' images produced by
					% the segmentation.
					if numel(files) == 0
						im = tiffread(cat(2, prefix, cat(2, '_w1488-power_t', cat(2, time_str, '.TIF'))));
					else
						im = tiffread(cat(2, prefix, cat(2, '_w2568-power_t', cat(2, time_str, '.TIF'))));
						%disp(cat(2, prefix, cat(2, '_w2568-power_t', cat(2, time_str, '.TIF'))));
                                                % Try to read the "green" image used for segmentation. This is a single slice.                    
                                                try                     
                                                        slice2 = readim(cat(2, 'im_segm_T', cat(2, time_str, '.ics')));
                                                        slice2 = squeeze(slice2(:, :, 1));
                                                % If not present, just go with im.        
                                                catch
                                                        slice2 = stretch(rotation(stretch(dip_image(sum(double(im(0:end, 0:end, slice_list)), 3))), ang, 3, 'zoh'), 0, 100, 0, 255);         slice2 = squeeze(slice2);                
                                                end                            
					end
					slice = stretch(rotation(stretch(dip_image(sum(double(im(0:end, 0:end, slice_list)), 3))), ang, 3, 'zoh'), 0, 100, 0, 255);
                                        %slice = stretch(rotation(stretch(dip_image(max(double(im(0:end, 0:end, slice_list)),[], 3))), ang, 3, 'zoh'), 0, 100, 0, 255);              
                                        %slice = rotation(dip_image(max(double(im(0:end, 0:end, slice_list)),[], 3)), ang, 3, 'zoh');
				else
					im = expanddim(readim(cat(2, prefix, time_str)), 3);
			
					slice = rotation(im(:, :, slice_list), ang, 3, 'zoh');
				end
			end
			
			imshow(double(slice), [0 255]);
			
			roi = [];

			if (exist(cat(2, cat(2, 'roi_T', time_str), '.mat'), 'file'))
				vars = load(cat(2, cat(2, 'roi_T', time_str), '.mat'));
				roi = vars.roi;
			else
				% This bit could be outside the loop, but thus, we keep the code
				% compact and easy to copy and paste.
				d = dir('./roi_T*');
		
				% There are ROIs defined.
				if (numel(d) > 0)
					rois = zeros(1, numel(d));
			
					for iroi = 1:numel(d)
						sroi = d(iroi).name;
						iT = strfind(sroi, '_T');
						rois(iroi) = str2num(sroi((iT+2):(iT+5)));
					end		
					% End this could be outside the loop.
					roisd = abs(rois - index - imshift);
					[tmproi roi_index] = min(roisd);
					roi_index = rois(roi_index);
	
					time_str_roi = rnum2str(roi_index, 4);
	
					vars = load(cat(2, cat(2, 'roi_T', time_str_roi), '.mat'));
					roi = vars.roi;
		
				% If no ROI has been defined, do nothing.
				else
					disp('No roi defined.');
				end
			end
			
			ud.roi = roi;
			
			if ~ isempty(roi)
				h = rectangle('Position', [roi(1, 1), roi(1, 2), abs(roi(2, 1) - roi(1, 1)), abs(roi(2, 2) - roi(1, 2))]);
				set(h, 'EdgeColor', 'b');
			end
			
			% Slowest bit.
			plot_geometry(ud.geometry(ud.index).nodes, ud.geometry(ud.index).edges, color, 0);
			cb_paintedgeselection(fig, ud.geometry(ud.index).nodes, ud.geometry(ud.index).edges, color_selected, color);
			% End of slowest bit.
			
			if ~ isempty(slice2)
				ud.colordata = joinchannels('rgb', slice, slice2);
				ud.imagedata = ud.colordata{1};
			else
				if isfield(ud, 'colordata')
					ud = ud.rmfield(ud, 'colordata');
				end
			end
			
			set(gca, 'Visible', 'off');
			set(gcf, 'UserData', ud);
		
			%xlim(xl);
			%ylim(yl);
			
			if ~ isempty(roi)
				xlim([roi(1, 1)-25 roi(2, 1)+25]);
				ylim([roi(1, 2)-25 roi(2, 2)+25]);
			else
				xlim([0 ud.imsize(2)-1]);
				xlim([0 ud.imsize(1)-1]);
			end
		end
				
	case {'consolidate_geom', 'k_1'}
		ud = cb_clearedgeselection(fig, color);
		
		xl = xlim;
		yl = ylim;
		if numel(ud.geometry) > 1
			[tpo tpf] = dialog_timepoints([(1+ud.imshift) (numel(ud.geometry)+ud.imshift)]);
			tpo = tpo - ud.imshift;
			tpf = tpf - ud.imshift;
		else
			tpo = 1;
			tpf = 1;
		end
		
		if tpo > 0
			wb = waitbar(0, 'Consolidating networks ...');
			for i = tpo:tpf
				geometry = consolidategeometry(ud.geometry(i));	
				ud.geometry(i).nodes = geometry.nodes;
				ud.geometry(i).edges = geometry.edges;
				
				waitbar((i-tpo+1)/(tpf-tpo+1));
			end
			
			close(wb);
		end

		set(fig, 'UserData', ud);
		ch = get(gca, 'Children');
		delete(ch(1:end-2));
			
		plot_geometry(ud.geometry(ud.index).nodes, ud.geometry(ud.index).edges, color, 0);
		set(gca, 'Visible', 'off');
		set(gcf, 'UserData', ud);
		xlim(xl);
		ylim(yl);

	case {'gen_cells', 'k_2'} 
	
		imshift = ud.imshift;
		prefix = ud.prefix;
		slice_list = ud.slice_list;
		if exist('frame_data.mat', 'file')
			load frame_data;
			shift = frames(1) - 1;
		else
			frames = [];
		end

		if numel(ud.geometry) > 1
			[tpo tpf] = dialog_timepoints([(1+ud.imshift) (numel(ud.geometry)+ud.imshift)]);
			tpo = tpo - ud.imshift;
			tpf = tpf - ud.imshift;
		else
			tpo = 1;
			tpf = 1;
		end
			
		if tpo > -1
			wb = waitbar(0, 'Generating cell maps ...');
			for i = tpo:tpf
				index = i;
				
				if ~ isempty(frames)
					time_str = rnum2str(index+imshift, 4);
		
					roi = [];
				
					if (exist(cat(2, cat(2, 'roi_T', time_str), '.mat'), 	'file'))
						vars = load(cat(2, cat(2, 'roi_T', time_str), 	'.mat'));
						roi = vars.roi;
					else
						% This bit could be outside the loop, but thus, we keep the code
						% compact and easy to copy and paste.
						d = dir('./roi_T*');
			
						% There are ROIs defined.
						if (numel(d) > 0)
							rois = zeros(1, numel(d));
					
							for iroi = 1:numel(d)
								sroi = d(iroi).name;
								iT = strfind(sroi, '_T');
								rois(iroi) = str2num(sroi((iT+2):(iT+5)));
							end		
							% End this could be outside the loop.
							roisd = abs(rois - index - imshift);
							[tmproi roi_index] = min(roisd);
							roi_index = rois(roi_index);
		
							time_str_roi = rnum2str(roi_index, 4);
		
							vars = load(cat(2, cat(2, 'roi_T', time_str_roi), '.mat'));
							roi = vars.roi;
			
						% If no ROI has been defined, do nothing.
						else
							disp('No roi defined.');
						end
					end
				else
					d = dir('./roi*');
			
					% There are ROIs defined.
					if (numel(d) > 0)
						vars = load(d(1).name);
						if isa(vars.roi, 'dip_image')
							roi = vars.roi;
						else
							roi = [];
						end	
			
					% If no ROI has been defined, look for the 	embryo limits.
					else	
						roi = [];
					end
				end
					ud.geometry(i).nodecellmap = regeneratenodecellmap(ud.geometry(i), roi);
					%ud.geometry(i).nodes = geometry.nodes;
					%ud.geometry(i).edges = geometry.edges;
										
					waitbar((i-tpo+1)/(tpf-tpo+1));	
					disp(cat(2, num2str(100.*(i-tpo+1)/(tpf-tpo+1)), '%'));	
			end
			
			close(wb);
		end
		
		%geometry = regeneratenodecellmap(ud.geometry(ud.index));	
		%ud.geometry(ud.index).nodes = geometry.nodes;
		%ud.geometry(ud.index).edges = geometry.edges;
		%ud.geometry(ud.index).nodecellmap = geometry.nodecellmap;
		set(fig, 'UserData', ud);
		
	% Define roi.
	case {'k_r'}
		set(fig, 'WindowButtonDownFcn', 'mycallbacks2D(gcbf, ''roi_down'');');
		set(fig, 'Pointer', 'crossh');
	
	case 'roi_down'
		cb_deleteroi(fig);
		ud.rlcoords = cb_getcoords(fig);
		set(fig, 'UserData', ud); % If you do this at the end of this "case", lineh property
					  % in udata is not properly stored.
		
		% If the right button was used to delete a line, coords is set to
		% an empty vector and nothing else should be done.
		if ~isempty(ud.rlcoords)
			if ~ isempty(ud.roi)
				[in on] = inpolygon(ud.rlcoords(1), ud.rlcoords(2), [ud.roi(1, 1); ud.roi(1, 1); ud.roi(2, 1); ud.roi(2, 1)], [ud.roi(1, 2); ud.roi(2, 2); ud.roi(2, 2); ud.roi(1, 2)]);
				
				if on == 0
					set(fig, 'WindowButtonMotionFcn', 'mycallbacks2D(gcbf, ''roi_motion'');');
					set(fig, 'WindowButtonUpFcn', 'mycallbacks2D(gcbf, ''roi_up'');');
				else
					ud.rtmp = ud.rlcoords;
					ud.rlcoords = ud.roi(1, :);
					set(fig, 'WindowButtonMotionFcn', 'mycallbacks2D(gcbf, ''roi_move'');');
					set(fig, 'WindowButtonUpFcn', 'mycallbacks2D(gcbf, ''roi_up_move'');');
					set(fig, 'UserData', ud);
				end
			else
				set(fig, 'WindowButtonMotionFcn', 'mycallbacks2D(gcbf, ''roi_motion'');');
				set(fig, 'WindowButtonUpFcn', 'mycallbacks2D(gcbf, ''roi_up'');');
			end
		else
			cb_deleteroi(fig);
			ud.roi = [];
			set(fig, 'WindowButtonUpFcn', '');	
			set(fig, 'UserData', ud);		
		end
	case 'roi_motion'
		cb_deleteroi(fig);
		p = cb_getcoords(fig);
		p1 = [min(ud.rlcoords(1), p(1)) min(ud.rlcoords(2), p(2)) 0]; 
		p2 = [max(ud.rlcoords(1), p(1)) max(ud.rlcoords(2), p(2)) 0]; 
		cb_drawroi(fig, [p1; p2], roi_color);
		fprintf('\n%dx%d', abs(p1(1)-p2(1)), abs(p1(2)-p2(2)));
	case 'roi_move'
		cb_deleteroi(fig);
		if size(ud.roi, 3) == 2
        		roi = cat(2, ud.roi, [0;0]);
        	else
        	        roi = ud.roi;
        	end
		cb_drawroi(fig, [roi(1, :) + cb_getcoords(fig) - ud.rtmp; roi(2, :) + cb_getcoords(fig) - ud.rtmp], roi_color);
		
	case 'roi_up'
		set(fig, 'WindowButtonMotionFcn', '');
		p = cb_getcoords(fig);
		p1 = [min(ud.rlcoords(1), p(1)) min(ud.rlcoords(2), p(2)) 0]; 
		p2 = [max(ud.rlcoords(1), p(1)) max(ud.rlcoords(2), p(2)) 0]; 
		ud.roi = [p1; p2];
		set(fig, 'UserData', ud);
	
	case 'roi_up_move'
		set(fig, 'WindowButtonMotionFcn', '');
		
		if size(ud.roi, 3) == 2
        		roi = cat(2, ud.roi, [0;0]);
        	else
        	        roi = ud.roi;
        	end
		ud.roi = [roi(1, :) + cb_getcoords(fig) - ud.rtmp; roi(2, :) + cb_getcoords(fig) - ud.rtmp];
		set(fig, 'UserData', ud);

	% Define roi.
	case {'k_b'}
		set(fig, 'WindowButtonDownFcn', 'mycallbacks2D(gcbf, ''bg_down'');');
		set(fig, 'Pointer', 'crossh');
	
	case 'bg_down'
		cb_deletebg(fig);
		ud.rlcoords = cb_getcoords(fig);
		set(fig, 'UserData', ud); % If you do this at the end of this "case", lineh property
					  % in udata is not properly stored.
		
		% If the right button was used to delete a line, coords is set to
		% an empty vector and nothing else should be done.
		if ~isempty(ud.rlcoords)
			if ~ isempty(ud.bg)
				[in on] = inpolygon(ud.rlcoords(1), ud.rlcoords(2), [ud.bg(1, 1); ud.bg(1, 1); ud.bg(2, 1); ud.bg(2, 1)], [ud.bg(1, 2); ud.bg(2, 2); ud.bg(2, 2); ud.bg(1, 2)]);
				
				if on == 0
					set(fig, 'WindowButtonMotionFcn', 'mycallbacks2D(gcbf, ''bg_motion'');');
					set(fig, 'WindowButtonUpFcn', 'mycallbacks2D(gcbf, ''bg_up'');');
				else
					ud.rtmp = ud.rlcoords;
					ud.rlcoords = ud.bg(1, :);
					set(fig, 'WindowButtonMotionFcn', 'mycallbacks2D(gcbf, ''bg_move'');');
					set(fig, 'WindowButtonUpFcn', 'mycallbacks2D(gcbf, ''bg_up_move'');');
					set(fig, 'UserData', ud);
				end
			else
				set(fig, 'WindowButtonMotionFcn', 'mycallbacks2D(gcbf, ''bg_motion'');');
				set(fig, 'WindowButtonUpFcn', 'mycallbacks2D(gcbf, ''bg_up'');');
			end
		else
			cb_deletebg(fig);
			ud.bg = [];
			set(fig, 'WindowButtonUpFcn', '');	
			set(fig, 'UserData', ud);		
		end
	case 'bg_motion'
		cb_deletebg(fig);
		p = cb_getcoords(fig);
		p1 = [min(ud.rlcoords(1), p(1)) min(ud.rlcoords(2), p(2)) 0]; 
		p2 = [max(ud.rlcoords(1), p(1)) max(ud.rlcoords(2), p(2)) 0]; 
		cb_drawbg(fig, [p1; p2], bg_color);
		fprintf('\n%dx%d', abs(p1(1)-p2(1)), abs(p1(2)-p2(2)));
	case 'bg_move'
		cb_deletebg(fig);
		cb_drawbg(fig, [ud.bg(1, :) + cb_getcoords(fig) - ud.rtmp; ud.bg(2, :) + cb_getcoords(fig) - ud.rtmp], bg_color);
		
	case 'bg_up'
		set(fig, 'WindowButtonMotionFcn', '');
		p = cb_getcoords(fig);
		p1 = [min(ud.rlcoords(1), p(1)) min(ud.rlcoords(2), p(2)) 0]; 
		p2 = [max(ud.rlcoords(1), p(1)) max(ud.rlcoords(2), p(2)) 0]; 
		ud.bg = [p1; p2];
		set(fig, 'UserData', ud);
	
	case 'bg_up_move'
		set(fig, 'WindowButtonMotionFcn', '');
		ud.bg = [ud.bg(1, :) + cb_getcoords(fig) - ud.rtmp; ud.bg(2, :) + cb_getcoords(fig) - ud.rtmp];
		set(fig, 'UserData', ud);
	
	case {'select_t1', 'k_T'}
		%index = ud.index;
		imshift = ud.imshift;
		prefix = ud.prefix;
		slice_list = ud.slice_list;
		if exist('frame_data.mat', 'file')
			load frame_data;
			shift = frames(1) - 1;
		else
			frames = [];
		end
		
		if numel(ud.geometry) > 1
			[tpo tpf min_l] = dialog_measurecells2D([(1+ud.imshift) (numel(ud.geometry)+ud.imshift)], min_l);
			tpo = tpo - ud.imshift;
			tpf = tpf - ud.imshift;
		else
			tpo = 1;
			tpf = 1;
			min_l = 3;
		end
		
		% If cancel was not pressed, continue.
		if tpo > 0
			[filename, pathname, ext] = uiputfile('*.mat', 'Save measurements');
			
			wb = waitbar(0, 'Measuring T2 processes ...');
			
			m = cell(1, numel(tpo:tpf));
			
			for i = tpo:tpf			
				index = i;
				
				if (tpo == tpf) && (~ isempty(ud.roi))
					roi = ud.roi(:, 1:2);
				elseif ~ isempty(frames)
					roi = load_roi(index+imshift);
				else
					d = dir('./roi*');
			
					% There are ROIs defined.
					if (numel(d) > 0)
						vars = load(d(1).name);
						if isa(vars.roi, 'dip_image')
							roi = vars.roi; 
						else
							roi = [];
						end	
			
					% If no ROI has been defined, use the entire embryo.
					else	
						roi = [];
					end
				end

				[m{i-tpo+1}] = measureT1(ud.geometry(i), roi, min_l);
				
				waitbar((i-tpo+1)/(tpf-tpo+1));
			end
	
			close(wb);
			
			if (~ isequal(filename, 0))
				save(cat(2, pathname, filename), 'm');
			else
				%plot_topology_2D(m, 5);
				disp(m{1});
				disp(pct_cellsrosettes);
			end
		end				
	case {'save_geom', 'k_3'}
                if nargin < 3		
                        cb_savegeometry(ud);
                else
                        cb_savegeometry(ud, varargin{3}); % Third argument can be the full path (including name) of the file where the geometry should be saved.
                end                        
	
	case {'save_roi', 'k_4'}
		cb_saveroi(ud);
		
	case {'k_h'}
		cb_showhideedges(fig);		
		
		
	case {'select_edge', 'k_s'}
		set(fig, 'WindowButtonDownFcn', 'mycallbacks2D(gcbf, ''select_edge_down'');');
		set(fig, 'WindowButtonUpFcn', '');
		set(fig, 'Pointer', 'crossh');

	%case 'no_select_edge'
	%	set(fig, 'WindowButtonDownFcn', '');
	%	set(fig, 'WindowButtonUpFcn', '');
	%	set(fig, 'Pointer', 'arrow');

	case 'select_edge_down'
		% Toggle selection with left button.
		% Left-click.
		if ((~ strcmp(get(fig,'SelectionType'),'alt')) && (~ (strcmp(get(fig,'SelectionType'),'extend'))))
			ind = cb_getedge(fig, nodes, edges);
			if ind > 0
				ud = cb_toggleedgeselection(fig, ind, color_selected, color);
				set(fig, 'UserData', ud);
			end
		% Maybe shift click to select everything within a box?
		end			
		
	case {'clear', 'k_c'}
		% Find the current action and clear accordingly (e.g. if selecting
		% interfaces, 'c' clears all selected interfaces).
		curr_action = get(fig, 'WindowButtonDownFcn');
		
		switch curr_action
			% Selecting interfaces.
			case 'mycallbacks2D(gcbf, ''select_edge_down'');'
				ud = cb_clearedgeselection(fig, color);
		end
		
		set(fig, 'UserData', ud);
		
		
	case {'measure_edges', 'k_6'}
		imshift = ud.imshift;
		prefix = ud.prefix;
		slice_list = ud.slice_list;
		m = cell(1, numel(ud.geometry));
		intensity_hist = [];
		mean_roi_int = [];
		max_roi_int = [];
		min_roi_int = [];
		mean_bg_int = [];
		
		if numel(ud.geometry) > 1
			[tpo tpf] = dialog_timepoints([(1+ud.imshift) (numel(ud.geometry)+ud.imshift)]);
			tpo = tpo - ud.imshift;
			tpf = tpf - ud.imshift;
		else
			tpo = 1;
			tpf = 1;
		end
		
		if tpo > -1
			[filename, pathname, ext] = uiputfile('*.mat', 'Save measurements');
			
			old_index = ud.index;
			
			ud.index = tpo;
			set(fig, 'UserData', ud);
			
			if numel(ud.geometry) > 1
        			mycallbacks2D(fig, 'go_to'); 
	        		ud = get(fig, 'UserData');
	        	end
			
			% For each time point ...
			for ii = tpo:tpf	
				% If the image is grey scale.
				if ~ isfield(ud, 'colordata')
					% Measure the edges.
					[m{ii} ih mean_roi std_roi max_roi min_roi mean_bg] = measure_edges(ud.geometry(ii), ud.rselectededges{ii}, ud, 1, -1, 3);
					intensity_hist(ii, :) = ih;
					mean_roi_int(ii) = mean_roi;
					fprintf('\nMean background intensity is %.2f.\n', mean_bg);
					mean_bg_int(ii) = mean_bg;
					std_roi_int(ii) = std_roi;
					max_roi_int(ii) = max_roi;
					min_roi_int(ii) = min_roi;
					
					% Load the next image.
					if numel(ud.geometry) > 1
        					mycallbacks2D(fig, 'next_geom');
	        				ud = get(fig, 'UserData');
	        		        end
					
				% Color images.
				else
					% Measure edges.
					[m{ii} ih mean_roi std_roi max_roi min_roi mean_bg] = measure_edges(ud.geometry(ii), ud.rselectededges{ii}, ud, 1, 2, 3);
					intensity_hist(ii, :) = ih;
					mean_roi_int(ii) = mean_roi;
					fprintf('\nMean background intensity is %.2f.\n', mean_bg);
					mean_bg_int(ii) = mean_bg;
					std_roi_int(ii) = std_roi;
					max_roi_int(ii) = max_roi;
					min_roi_int(ii) = min_roi;
					
					% Load the color image.
					if numel(ud.geometry) > 1
        					mycallbacks2D(fig, 'next_geom');
					        ud = get(fig, 'UserData');
					end
					
				end
			end
			
  			if (~ isequal(filename, 0))
	  			save(cat(2, pathname, filename), 'm', 'intensity_hist', 'mean_roi_int', 'mean_bg_int', 'std_roi_int', 'max_roi_int', 'min_roi_int');
  				%disp(intensity_hist);
  			else
  				disp(intensity_hist);
	  		end
	
			ud.index = old_index;
			if numel(ud.geometry) > 1
        			mycallbacks2D(fig, 'go_to'); 
	        		ud = get(fig, 'UserData');
	        	end
		end
		
	
%  		[filename, pathname, ext] = uiputfile('*.mat', 'Save measurements');
%  		if numel(ud.geometry) == 1
%  			z = 1;
%  		else 
%  			z = ud.index;
%  		end
%  			
%  		
%  		if ~ isfield(ud, 'colordata')
%  			[m intensity_hist] = measure_edges(ud.geometry(ud.index), ud.rselectededges{z});
%  		else
%  			[m intensity_hist] = measure_edges(ud.geometry(ud.index), ud.rselectededges{z}, ud, 1, 2);
%  		end
%  		
%  		
%  		if (~ isequal(filename, 0))
%  			save(cat(2, pathname, filename), 'm', 'intensity_hist');
%  			disp(m);
%  			disp(intensity_hist);
%  		else
%  			disp(m);
%  			disp(intensity_hist);
%  		end
		
	case {'measure_alignment', 'k_7'}
		[tpo tpf ang min_l min_c save_pics] = dialog_measuremovie2D([(1+ud.imshift) (numel(ud.geometry)+ud.imshift)]);
		
		if tpo ~= -1
			[filename, pathname, ext] = uiputfile('*.mat', 'Save alignment measurements');
				
			[m cl] = cb_measuremovie2D(ud.geometry, tpo, tpf, ang, min_l, ud.imshift, save_pics);
			
			if (~ isequal(filename, 0))
				save(cat(2, pathname, filename), 'm', 'cl');
			end
			
			disp(m);
			
			if tpo ~= tpf
				figure;
				frame_indeces = 1:numel(m.AlignmentFactor1);
				range = (frame_indeces - 1) .* 15 ./ 60;
				hp = plot(range, smooth_curve(m.AlignmentFactor1 .* 100., 5), 'r');
				set(hp, 'LineWidth', 2);
				hold on;
				%legend(gca, {'[0-4) \mum'; '[4-8) \mum'; '[8-12) \mum'; '[12-16) \mum'; '[16-20) \mum';}, 'Location', 'Best');
				xlim([0 ((floor(range(end)/5)+1)*5)]);
				ylimits = [0 100];
				ylim(ylimits);
				set(gca, 'YTick', (ylimits(1):10:ylimits(2)));
				set(gca, 'XGrid', 'off', 'YGrid', 'on', 'FontSize', 12);
				xlabel('Time {\itt} (minutes)', 'FontSize', 16);
				ylabel('Alignment factor 1 (%)', 'FontSize', 16);
	
				figure;
				frame_indeces = 1:numel(m.AlignmentFactor2);
				range = (frame_indeces - 1) .* 15 ./ 60;
				hp = plot(range, smooth_curve(m.AlignmentFactor2 .* 100., 5), 'r');
				set(hp, 'LineWidth', 2);
				hold on;
				%legend(gca, {'[0-4) \mum'; '[4-8) \mum'; '[8-12) \mum'; '[12-16) \mum'; '[16-20) \mum';}, 'Location', 'Best');
				xlim([0 ((floor(range(end)/5)+1)*5)]);
				ylimits = [0 100];
				ylim(ylimits);
				set(gca, 'YTick', (ylimits(1):10:ylimits(2))) 
				set(gca, 'XGrid', 'off', 'YGrid', 'on', 'FontSize', 12);
				xlabel('Time {\itt} (minutes)', 'FontSize', 16);
				ylabel('Alignment factor 2 (%)', 'FontSize', 16);
			end
			
                end
		
	case {'measure_mfn', 'k_8'}
		imshift = ud.imshift;
		
		if numel(ud.geometry) >= 1
			[tpo tpf ang ns alpha pershellanalysis r] = dialog_measuremfn([(1+ud.imshift) (numel(ud.geometry)+ud.imshift)], [ud.index ud.index ud.paramang ud.paramns ud.paramalpha ud.paramshell ud.paramrmax]);
			
			tpo = tpo - ud.imshift;
			tpf = tpf - ud.imshift;
		end
		
		if tpo > -1
			% Store parameters.
			ud.paramtpo = tpo;
			ud.paramtpf = tpf;
			ud.paramang = ang;
			ud.paramns = ns;
			ud.paramalpha = alpha;
			ud.paramshell = pershellanalysis;
			ud.paramrmax = r; 
			
			set(fig, 'UserData', ud);
			mfn = cell(1, numel(ud.geometry));
			ufn = cell(1, numel(ud.geometry));
			lfn = cell(1, numel(ud.geometry));
		
			[filename, pathname, ext] = uiputfile('*.mat', 'Save M function analysis');
			
			% For each time point ...
			for ii = tpo:tpf
				fprintf('\nAnalyzing time point %d/%d ...\n', ii-tpo+1, tpf-tpo+1);
				% Load ROI.
				roi = load_roi(ii + imshift);
				% And run analysis (using the current
				% value for min_edgelength).
				[mfn{ii}, ufn{ii}, lfn{ii}, dt{ii}, pop_sims{ii}, mvalues_sims{ii}, mipr{ii}, mipr_sims{ii}] = measure_mfn(ud.geometry(ii), ud.rselectededges{ii}, roi, r, alpha, ns, pershellanalysis, ang, ud.paramminl);
			end
			
  			if (~ isequal(filename, 0))
	  			save(cat(2, pathname, filename), 'mfn', 'ufn', 'lfn', 'dt', 'pop_sims', 'mvalues_sims', 'mipr', 'mipr_sims');
  				disp(ufn{tpo});
				disp(mfn{tpo});
				fprintf('\nP = %f', (numel(find(mvalues_sims{tpo}>=mfn{tpo}(1))) + 1)/(numel(mvalues_sims{tpo}) + 1));
  			else
				disp(ufn{tpo});
				disp(mfn{tpo});
				fprintf('\nP = %f', (numel(find(mvalues_sims{tpo}>=mfn{tpo}(1))) + 1)/(numel(mvalues_sims{tpo}) + 1));
	  		end
		end
	
	case {'select_vertical', 'k_i'}
		if numel(ud.geometry) > 1
			[tpo tpf] = dialog_timepoints([(1+ud.imshift) (numel(ud.geometry)+ud.imshift)]);
			tpo = tpo - ud.imshift;
			tpf = tpf - ud.imshift;
		else
			tpo = 1;
			tpf = 1;
		end		
				
		if tpo > 0
			wb = waitbar(0, 'Selecting isolated vertical interfaces ...');
			for i = tpo:tpf
				%ud = cb_selectorientationedges(fig, i, [75, 105]);
				ud = cb_selectunalignededges(fig, i, 75, 5);
				set(fig, 'UserData', ud);
				waitbar((i-tpo+1)/(tpf-tpo+1));
			end
			
			close(wb);
			
			cb_paintedgeselection(fig, ud.geometry(ud.index).nodes, ud.geometry(ud.index).edges, color_selected, color);
		end
	
	case {'select_vcables', 'k_l'}
 		if numel(ud.geometry) > 1
			[tpo tpf ang min_int_l min_chain_l save_pics] = dialog_measuremovie2D([(1+ud.imshift) (numel(ud.geometry)+ud.imshift)]);
			tpo = tpo - ud.imshift;
			tpf = tpf - ud.imshift;
		else
			[tpo tpf ang min_int_l min_chain_l save_pics] = dialog_measuremovie2D([(1+ud.imshift) (numel(ud.geometry)+ud.imshift)]);
                        tpo = tpo - ud.imshift;
                        tpf = tpf - ud.imshift;
                        %tpo = 1;
			%tpf = 1;
			%ang = 75;
			%min_int_l = 5;
			%min_chain_l = 2;
			%save_pics = 0;
		end		
				
		if tpo > 0
			wb = waitbar(0, 'Selecting cables ...');
			for i = tpo:tpf
				ud = cb_selectalignededges(fig, i, ang, min_chain_l, min_int_l, save_pics);
				set(fig, 'UserData', ud);
				
                                waitbar((i-tpo+1)/(tpf-tpo+1));
			end
			
			close(wb);
			
			cb_paintedgeselection(fig, ud.geometry(ud.index).nodes, ud.geometry(ud.index).edges, color_selected, color);
		end
	
	case {'measure_2D', 'k_5'}
		%index = ud.index;
		pct_cellsrosettes = [];
		imshift = ud.imshift;
		prefix = ud.prefix;
		slice_list = ud.slice_list;
		if exist('frame_data.mat', 'file')
			load frame_data;
			shift = frames(1) - 1;
		else
			frames = [];
		end
		
		if numel(ud.geometry) > 1
			[tpo tpf min_l] = dialog_measurecells2D([(1+ud.imshift) (numel(ud.geometry)+ud.imshift)], min_l);
			tpo = tpo - ud.imshift;
			tpf = tpf - ud.imshift;
		else
			tpo = 1;
			tpf = 1;
			min_l = 3;
		end
		
		% If cancel was not pressed, continue.
		if tpo > 0
			[filename, pathname, ext] = uiputfile('*.mat', 'Save measurements');
			
			wb = waitbar(0, 'Measuring cells ...');
			
			m = cell(1, numel(tpo:tpf));
			
			for i = tpo:tpf			
				index = i;
				
				if (tpo == tpf) && (~ isempty(ud.roi))
					roi = ud.roi(:, 1:2);
				elseif ~ isempty(frames)
					roi = load_roi(index+imshift);
				else
					d = dir('./roi*');
			
					% There are ROIs defined.
					if (numel(d) > 0)
						vars = load(d(1).name);
						if isa(vars.roi, 'dip_image')
							roi = vars.roi; 
						else
							roi = [];
						end	
			
					% If no ROI has been defined, use the entire embryo.
					else	
						roi = [];
					end
				end

				[m{i-tpo+1} pct_cellsrosettes(i-tpo+1)] = measure2D(ud.geometry(i), roi, min_l);
				
				waitbar((i-tpo+1)/(tpf-tpo+1));
			end
	
			close(wb);
			
			if (~ isequal(filename, 0))
				save(cat(2, pathname, filename), 'm', 'pct_cellsrosettes');
			else
				%plot_topology_2D(m, 5);
				disp(m{1});
				disp(pct_cellsrosettes);
			end
		end				
	
	
	case {'select_horizontal', 'k_d'}
		if nargin < 3 && numel(ud.geometry) > 1
			[tpo tpf] = dialog_timepoints([(1+ud.imshift) (numel(ud.geometry)+ud.imshift)]);
			tpo = tpo - ud.imshift;
			tpf = tpf - ud.imshift;
		elseif nargin == 3 % Provided time point number/s to analyze as function parameter.
			tpo = varargin{3}(1);
			tpf = varargin{3}(end);
		else
			tpo = 1;
			tpf = 1;
		end		
				
		if tpo > 0
			wb = waitbar(0, 'Selecting horizontal interfaces ...');
			
			ang = -30;
			min_chain_l = 1;
			min_int_l = 5;
			save_pics = 0;
			
			for i = tpo:tpf
				%ud = cb_selectorientationedges(fig, i, [0, 15], [165, 180]);
				ud = cb_selectalignededges(fig, i, ang, min_chain_l, min_int_l, save_pics);
				set(fig, 'UserData', ud);
				waitbar((i-tpo+1)/(tpf-tpo+1));
			end
			
			close(wb);
			
			cb_paintedgeselection(fig, ud.geometry(ud.index).nodes, ud.geometry(ud.index).edges, color_selected, color);
		end
	
	case {'select_positive', 'k_p'}
                if nargin < 8        
        		[tpo tpf ang min_l thr use_old method] = dialog_measureintensity([(1+ud.imshift) (numel(ud.geometry)+ud.imshift)], intensity_measurements, [ud.index ud.index ud.paramang ud.paramminl ud.parammethod ud.paramth ud.paramuseold]);
        		tpo = tpo - ud.imshift;
	        	tpf = tpf - ud.imshift;
                else
                        tpo = varargin{3};
                        tpf = varargin{3}; % Run on a single frame!
                        ang = varargin{4};
                        min_l = varargin{5};
                        thr = varargin{6};
                        use_old = varargin{7};
                        method = varargin{8};
                end              
		
                if tpo > 0
			old_index = ud.index;
			
			% Store parameters.
			ud.paramtpo = tpo;
			ud.paramtpf = tpf;
			ud.paramang = ang;
			ud.paramminl = min_l;
			ud.paramth = thr;
			ud.paramuseold = use_old;
			ud.parammethod = method; 
			
			ud.index = tpo;
			set(fig, 'UserData', ud);
			
			if numel(ud.geometry) > 1
        			mycallbacks2D(fig, 'go_to'); 
	        		ud = get(fig, 'UserData');
	        	end
			
			%wb = waitbar(0, 'Selecting positive interfaces ...');
			
			for i = tpo:tpf
				ud = cb_selectpositiveedges(fig, i, method, thr, ang, min_l, use_old, MIND, MININT);
				set(fig, 'UserData', ud);
				%waitbar((i-tpo+1)/(tpf-tpo+1));
					
				% Load the color image.
				if numel(ud.geometry) > 1
                                        mycallbacks2D(fig, 'next_geom');
        				ud = get(fig, 'UserData');
        			end
			end
			
			ud = get(fig, 'UserData');
			ud.index = old_index;
			set(fig, 'UserData', ud);

			if numel(ud.geometry) > 1
        			mycallbacks2D(fig, 'go_to'); 
	        	end

			
			%close(wb);
								
			%cb_paintedgeselection(fig, ud.geometry(ud.index).nodes, ud.geometry(ud.index).edges, color_selected, color);
			cb_paintedgeselection(fig, ud.geometry(ud.index).nodes, ud.geometry(ud.index).edges, color_selected, color);
		end
end


% ********************************************************************
function cb_drawroi(fig, roi, color)

w = abs(roi(2, 1) - roi(1, 1));
h = abs(roi(2, 2) - roi(1, 2));

if ((w > 1) && (h > 1))
	ax = get(fig, 'CurrentAxes');
	hold(ax, 'on');
	h = rectangle('Position', [roi(1, 1), roi(1, 2), w, h]);
	set(h, 'EdgeColor', color, 'Tag', 'ROI');
end
% ********************************************************************
function cb_drawbg(fig, roi, color)

w = abs(roi(2, 1) - roi(1, 1));
h = abs(roi(2, 2) - roi(1, 2));

if ((w > 1) && (h > 1))
	ax = get(fig, 'CurrentAxes');
	hold(ax, 'on');
	h = rectangle('Position', [roi(1, 1), roi(1, 2), w, h]);
	set(h, 'EdgeColor', color, 'Tag', 'BG');
end

% *********************************************************************************

function cb_deleteroi(fig)
ax = get(fig, 'CurrentAxes');
ch = get(ax, 'Children');

for i = 1:numel(ch)
	if strcmp(get(ch(i), 'Tag'), 'ROI')
		delete(ch(i));
		break;
	end
end

% *********************************************************************************

function cb_deletebg(fig)
ax = get(fig, 'CurrentAxes');
ch = get(ax, 'Children');

for i = 1:numel(ch)
	if strcmp(get(ch(i), 'Tag'), 'BG')
		delete(ch(i));
		break;
	end
end


% ********************************************************************
function [n e] = cb_deletenode_old(fig, nodes, edges)

obj = get(fig, 'CurrentObject'); 
if strcmp(get(obj, 'Tag'), 'node')
	n = nodes;
	ind = find((nodes(:, 1) == get(obj, 'YData')) & (nodes(:, 2) == get(obj, 'XData')));
	%if isempty(ind)
	%	e = edges;
	%	return;
	%end
	
	keep_edges = find((edges(:, 1) ~= ind) & (edges(:, 2) ~= ind));
	e = edges(keep_edges, :);
	
	ch = get(get(fig, 'CurrentAxes'), 'Children');
	for jj = 1:numel(ch)
		if strcmp(get(ch(jj), 'Tag'), 'edge')
			x = get(ch(jj), 'XData');
			y = get(ch(jj), 'YData');
			
			ind2 = find((x == get(obj, 'XData')) & (y == get(obj, 'YData')));
			if ~ isempty(ind2)
				delete(ch(jj));
			end
		%elseif strcmp(get(ch(jj), 'Tag'), 'node')
		%	x = get(ch(jj), 'XData');
		%	y = get(ch(jj), 'YData');
		%	
		%	if (x == n(ind, 2)) & (y == n(ind, 1))
		%		delete(ch(jj));
		%	end
		end
	end
	
	delete(obj);	
else
	n = nodes;
	e = edges;
end

% ********************************************************************
function [n e] = cb_deletenode(fig, nodes, edges, coords)

obj = get(fig, 'CurrentObject'); 

% Sometimes the object has been deleted before getting here. In that case,
% look for the closest edges (this is necessary for deleteedge with the
% processing flag on to work).
if isempty(obj)
	ax = get(fig, 'CurrentAxes');
	ch = get(ax, 'Children');
	for jj = 1:numel(ch)
		if strcmp(get(ch(jj), 'Tag'), 'edge')
			x = get(ch(jj), 'XData');
			y = get(ch(jj), 'YData');
			
			if ((coords(1) == x(1) & coords(2) == y(1)) | (coords(1) == x(2) & coords(2) == y(2)))
				obj = ch(jj); 
				break;
			end
		end
	end
end

if strcmp(get(obj, 'Tag'), 'edge')
	x = get(obj, 'XData');
	y = get(obj, 'YData');
	d1 = norm(coords(1:2)-[x(1) y(1)], 2);
	d2 = norm(coords(1:2)-[x(2) y(2)], 2);
	if (d1<d2) & (d1 <= 1)
		xn = x(1);
		yn = y(1);
	elseif d2<=1
		xn = x(2);
		yn = y(2);
	else
		n = nodes;
		e = edges;
		
		return;
	end
	
	n = nodes;
	ind = find((nodes(:, 1) == yn) & (nodes(:, 2) == xn));
	%if isempty(ind)
	%	e = edges;
	%	return;
	%end
	
	keep_edges = find((edges(:, 1) ~= ind) & (edges(:, 2) ~= ind));
	e = edges(keep_edges, :);
	
	ch = get(get(fig, 'CurrentAxes'), 'Children');
	for jj = 1:numel(ch)
		if strcmp(get(ch(jj), 'Tag'), 'edge')
			x = get(ch(jj), 'XData');
			y = get(ch(jj), 'YData');
			
			ind2 = find((x == xn) & (y == yn));
			if ~ isempty(ind2)
				delete(ch(jj));
			end
		%elseif strcmp(get(ch(jj), 'Tag'), 'node')
		%	x = get(ch(jj), 'XData');
		%	y = get(ch(jj), 'YData');
		%	
		%	if (x == n(ind, 2)) & (y == n(ind, 1))
		%		delete(ch(jj));
		%	end
		end
	end
	
	%delete(obj);	
else
	n = nodes;
	e = edges;
end

% ********************************************************************
function ind = cb_getnode_old(fig, nodes)

obj = get(fig, 'CurrentObject'); 
if strcmp(get(obj, 'Tag'), 'node')
	ind = find((nodes(:, 1) == get(obj, 'YData')) & (nodes(:, 2) == get(obj, 'XData')));
else
	ind = 0;
end

% ********************************************************************
function ind = cb_getnode(fig, nodes)

obj = get(fig, 'CurrentObject'); 
ax = get(fig, 'CurrentAxes');
coords = get(ax, 'CurrentPoint');
coords = round(coords(1, 1:2));

if strcmp(get(obj, 'Tag'), 'edge')
	x = get(obj, 'XData');
	y = get(obj, 'YData');
	d1 = norm(coords(1:2)-[x(1) y(1)], 2);
	d2 = norm(coords(1:2)-[x(2) y(2)], 2);
	if (d1<d2) & (d1 <= 1)
		xn = x(1);
		yn = y(1);
	elseif d2<=1
		xn = x(2);
		yn = y(2);
	else
		ind = 0;		
		return;
	end
	
	ind = find((nodes(:, 1) == yn) & (nodes(:, 2) == xn));
else
	ind = 0;
end

% ********************************************************************
function ind = cb_getedge(fig, nodes, edges)

obj = get(fig, 'CurrentObject');
if strcmp(get(obj, 'Tag'), 'edge')
	n = nodes;
	
	x = get(obj, 'XData');
	y = get(obj, 'YData');	
	
	node1 = [y(1) x(1)];
	ind1 = find((nodes(:, 1) == node1(1)) & (nodes(:, 2) == node1(2)));
	
	node2 = [y(2) x(2)];
	ind2 = find((nodes(:, 1) == node2(1)) & (nodes(:, 2) == node2(2)));
	
	if ind1(1) < ind2(1)
		ind = find((edges(:, 1) == ind1(1)) & (edges(:, 2) == ind2(1)));
	else
		ind = find((edges(:, 1) == ind2(1)) & (edges(:, 2) == ind1(1)));
	end
else
	ind = -1;
end


% ********************************************************************
function e = cb_addedge_old(fig, nodes, edges, ind1, ind2, color, color_selected)
n = nodes;
e = edges;
e(end+1, :) = sort([ind1(1) ind2(1)]);
pair_coords = n(e(end, 1:2), :);
lh=plot(pair_coords(:, 2), pair_coords(:, 1));
set(lh, 'Color', color, 'Tag', 'edge');

lh = plot(n(ind1, 2), n(ind1, 1), 'o');
set(lh, 'Color', color, 'MarkerSize', 4, 'MarkerFaceColor', color, 'Tag', 'node');
lh = plot(n(ind2, 2), n(ind2, 1), 'o');
set(lh, 'Color', color, 'MarkerSize', 4, 'MarkerFaceColor', color, 'Tag', 'node');

% ********************************************************************
function e = cb_addedge(fig, nodes, edges, ind1, ind2, color, color_selected)
n = nodes;
e = edges;
e(end+1, :) = sort([ind1(1) ind2(1)]);
pair_coords = n(e(end, 1:2), :);
lh=plot(pair_coords(:, 2), pair_coords(:, 1));
set(lh, 'Color', color, 'Tag', 'edge', 'Marker', 'o', 'MarkerFaceColor', color, 'MarkerSize', 4);

% ********************************************************************
function [n e move_node_index] = cb_addnode_old(fig, nodes, edges, color, color_selected)

move_node_index = 0;
obj = get(fig, 'CurrentObject');
ax = get(fig, 'CurrentAxes');
coords = get(ax, 'CurrentPoint');
coords = circshift(round(coords(1, 1:2)), [0 1]);
if strcmp(get(obj, 'Tag'), '')
	n = nodes;
	n(end+1, :) = coords;
	e = edges;

	lh = plot(n(end, 2), n(end, 1), 'o');
	set(lh, 'Color', color, 'MarkerSize', 4, 'MarkerFaceColor', color, 'Tag', 'node');
	
elseif strcmp(get(obj, 'Tag'), 'edge')
	n = nodes;
	n(end+1, :) = coords;
	
	x = get(obj, 'XData');
	y = get(obj, 'YData');
	
	e = edges;
	move_node_index = size(n, 1);
	
	% Delete edge.
	[n e] = cb_deleteedge(fig, n, e, color, color_selected, 0);		

	% Add node1-coords and node2-coords to the list of edges.
	node1 = [y(1) x(1)];
	ind1 = find((nodes(:, 1) == node1(1)) & (nodes(:, 2) == node1(2)));
	e(end+1, :) = [ind1(end) size(n, 1)];
	pair_coords = n(e(end, 1:2), :);
	lh=plot(pair_coords(:, 2), pair_coords(:, 1));
	set(lh, 'Color', color, 'Tag', 'edge');
	
	node2 = [y(2) x(2)];
	ind2 = find((nodes(:, 1) == node2(1)) & (nodes(:, 2) == node2(2)));
	e(end+1, :) = [ind2(end) size(n, 1)];
	pair_coords = n(e(end, 1:2), :);
	lh=plot(pair_coords(:, 2), pair_coords(:, 1));
	set(lh, 'Color', color, 'Tag', 'edge');
	
	lh = plot(n(ind1, 2), n(ind1, 1), 'o');
	set(lh, 'Color', color, 'MarkerSize', 4, 'MarkerFaceColor', color, 'Tag', 'node');
	lh = plot(n(ind2, 2), n(ind2, 1), 'o');
	set(lh, 'Color', color, 'MarkerSize', 4, 'MarkerFaceColor', color, 'Tag', 'node');
	lh = plot(n(end, 2), n(end, 1), 'o');
	set(lh, 'Color', color, 'MarkerSize', 4, 'MarkerFaceColor', color, 'Tag', 'node');

	set(fig, 'WindowButtonMotionFcn', 'mycallbacks2D(gcbf, ''move_node_down'');');
	set(fig, 'WindowButtonUpFcn', 'mycallbacks2D(gcbf, ''no_move_node_down'');');
	
	set(fig, 'CurrentObject', lh);

elseif strcmp(get(obj, 'Tag'), 'node')
	n = nodes;
	e = edges;

	x = get(gco, 'XData');
	y = get(gco, 'YData');
	
	for jj = 1 : size(n, 1)
		if (x == n(jj, 2)) & (y == n(jj, 1))
			move_node_index = jj;
			break;
		end
	end
	
	set(fig, 'WindowButtonMotionFcn', 'mycallbacks2D(gcbf, ''move_node_down'');');
	set(fig, 'WindowButtonUpFcn', 'mycallbacks2D(gcbf, ''no_move_node_down'');');
else
	n = nodes;
	e = edges;
end
% ********************************************************************
function [n e move_node_index] = cb_addnode(fig, nodes, edges, color, color_selected)
%  ;
move_node_index = 0;
obj = get(fig, 'CurrentObject');
ax = get(fig, 'CurrentAxes');
coords = get(ax, 'CurrentPoint');
coords = circshift(round(coords(1, 1:2)), [0 1]);

if strcmp(get(obj, 'Tag'), 'edge')
	n = nodes;
	e = edges;
	
	% Was the click on one of the sides of the edge (not necessary to create the node
	% then) or somewhere within the edge (addnode then). Do based on movie_index??
	x = get(obj, 'XData');
	y = get(obj, 'YData');
	
	isnode = 0;
	
	d1 = norm(coords([2 1])-[x(1) y(1)], 2);
	d2 = norm(coords([2 1])-[x(2) y(2)], 2);
	if (d1<d2) & (d1 <= 1)
		xn = x(1);
		yn = y(1);
		isnode = 1;
	elseif d2<=1
		xn = x(2);
		yn = y(2);
		isnode = 2;
	else
		isnode = 0;
	end

	if ~ isnode
		n(end+1, :) = coords;
	
		e = edges;
		move_node_index = size(n, 1);
		% Delete edge.
		[n e] = cb_deleteedge(fig, n, e, color, color_selected, 0);		
		% Add node1-coords and node2-coords to the list of edges.
		node1 = [y(1) x(1)];
		ind1 = find((n(:, 1) == node1(1)) & (n(:, 2) == node1(2)));
		e(end+1, :) = [ind1(end) size(n, 1)];
		pair_coords = n(e(end, 1:2), :);
		lh=plot(pair_coords(:, 2), pair_coords(:, 1));
		set(lh, 'Color', color, 'Tag', 'edge', 'Marker', 'o', 'MarkerFaceColor', color, 'MarkerSize', 4);
	
		node2 = [y(2) x(2)];
		ind2 = find((n(:, 1) == node2(1)) & (n(:, 2) == node2(2)));
		e(end+1, :) = [ind2(end) size(n, 1)];
		pair_coords = n(e(end, 1:2), :);
		lh=plot(pair_coords(:, 2), pair_coords(:, 1));
		set(lh, 'Color', color, 'Tag', 'edge', 'Marker', 'o', 'MarkerFaceColor', color, 'MarkerSize', 4);
		%set(fig, 'CurrentObject', lh);

	else
		ind = find((nodes(:, 1) == yn) & (nodes(:, 2) == xn));
		move_node_index = ind;

	end

	
	%lh = plot(n(ind1, 2), n(ind1, 1), 'o');
	%set(lh, 'Color', color, 'MarkerSize', 4, 'MarkerFaceColor', color, 'Tag', 'node');
	%lh = plot(n(ind2, 2), n(ind2, 1), 'o');
	%set(lh, 'Color', color, 'MarkerSize', 4, 'MarkerFaceColor', color, 'Tag', 'node');
	%lh = plot(n(end, 2), n(end, 1), 'o');
	%set(lh, 'Color', color, 'MarkerSize', 4, 'MarkerFaceColor', color, 'Tag', 'node');

	set(fig, 'WindowButtonMotionFcn', 'mycallbacks2D(gcbf, ''move_node_down'');');
	set(fig, 'WindowButtonUpFcn', 'mycallbacks2D(gcbf, ''no_move_node_down'');');
	
elseif strcmp(get(obj, 'Tag'), 'node')
	n = nodes;
	e = edges;

	x = get(gco, 'XData');
	y = get(gco, 'YData');
	
	for jj = 1 : size(n, 1)
		if (x == n(jj, 2)) & (y == n(jj, 1))
			move_node_index = jj;
			break;
		end
	end
	
	set(fig, 'WindowButtonMotionFcn', 'mycallbacks2D(gcbf, ''move_node_down'');');
	set(fig, 'WindowButtonUpFcn', 'mycallbacks2D(gcbf, ''no_move_node_down'');');
else
	n = nodes;
	e = edges;
end

% ********************************************************************
function [n e] = cb_movenode_old(fig, nodes, move_index, edges, color, color_selected)

ax = get(fig, 'CurrentAxes');
coords = get(ax, 'CurrentPoint');
coords = circshift(round(coords(1, 1:2)), [0 1]);
	
n = nodes;
e = edges;
	
% Delete edges associated with the moved node (copy code from deletenode)
% and replot them (copy code from addnode?).
ch = get(ax, 'Children');
for jj = 1:numel(ch)
	if strcmp(get(ch(jj), 'Tag'), 'edge')
		x = get(ch(jj), 'XData');
		y = get(ch(jj), 'YData');
		
		ind2 = find((x == n(move_index, 2)) & (y == n(move_index, 1)));
		if ~ isempty(ind2)
			delete(ch(jj));
		end
	elseif strcmp(get(ch(jj), 'Tag'), 'node')
		x = get(ch(jj), 'XData');
		y = get(ch(jj), 'YData');
		
		if (x == n(move_index, 2)) & (y == n(move_index, 1))
			delete(ch(jj));
		end
	end
end
	
%delete(obj);	
n(move_index, :) = coords;

% Add node1-coords and node2-coords to the list of edges.
theedges = find(e(:, 1) == move_index | e(:, 2) == move_index);

for ii = 1:numel(theedges)
	pair_coords = n(e(theedges(ii), 1:2), :);
	lh=plot(pair_coords(:, 2), pair_coords(:, 1));
	set(lh, 'Color', color, 'Tag', 'edge');
end

%lh = plot(n(ind1, 2), n(ind1, 1), 'o');
%set(lh, 'Color', color, 'MarkerSize', 4, 'MarkerFaceColor', color, 'Tag', 'node');
%lh = plot(n(ind2, 2), n(ind2, 1), 'o');
%set(lh, 'Color', color, 'MarkerSize', 4, 'MarkerFaceColor', color, 'Tag', 'node');
lh = plot(n(move_index, 2), n(move_index, 1), 'o');
set(lh, 'Color', color, 'MarkerSize', 4, 'MarkerFaceColor', color, 'Tag', 'node');

cb_paintedgeselection(fig, n, e, color_selected, color);

% ********************************************************************
function [n e] = cb_movenode(fig, nodes, move_index, edges, color, color_selected)

ax = get(fig, 'CurrentAxes');
coords = get(ax, 'CurrentPoint');
coords = circshift(round(coords(1, 1:2)), [0 1]);

obj = get(fig, 'CurrentObject');
%delete(obj);
	
n = nodes;
e = edges;
	
% Delete edges associated with the moved node (copy code from deletenode)
% and replot them (copy code from addnode?).
ch = get(ax, 'Children');
for jj = 1:numel(ch)
	if strcmp(get(ch(jj), 'Tag'), 'edge')
		% Was the click on one of the sides of the edge (movenode then) or
		% somewhere within the edge (addnode then). Do based on movie_index??
		x = get(ch(jj), 'XData');
		y = get(ch(jj), 'YData');
		
		ind2 = find(((x(1) == n(move_index, 2)) & (y(1) == n(move_index, 1))) | ((x(2) == n(move_index, 2)) & (y(2) == n(move_index, 1))));
		if ~ isempty(ind2)
			delete(ch(jj));
		end
	%elseif strcmp(get(ch(jj), 'Tag'), 'node')
	%	x = get(ch(jj), 'XData');
	%	y = get(ch(jj), 'YData');
	%	
	%	if (x == n(move_index, 2)) & (y == n(move_index, 1))
	%		delete(ch(jj));
	%	end
	end
end
	
%delete(obj);	
n(move_index, :) = coords;

% Add node1-coords and node2-coords to the list of edges.
theedges = find(e(:, 1) == move_index | e(:, 2) == move_index);

x_coords = [];
y_coords = [];

for ii = 1:numel(theedges)
	pair_coords = n(e(theedges(ii), 1:2), :);
	x_coords(:, end+1) = pair_coords(:, 1);
	y_coords(:, end+1) = pair_coords(:, 2);
	
	%lh=plot(pair_coords(:, 2), pair_coords(:, 1));
	%set(lh, 'Color', color, 'Tag', 'edge');
end

lh=plot(y_coords, x_coords);
set(lh, 'Color', color, 'Tag', 'edge', 'Marker', 'o', 'MarkerFaceColor', color, 'MarkerSize', 4);

cb_paintedgeselection(fig, n, e, color_selected, color);

% ********************************************************************
function ud = cb_clearedgeselection(fig, color_unsel)
ud = get(fig, 'UserData');
if numel(ud.geometry) == 1
	z = 1;
else
	z = ud.index;
end
ud.rselectededges{z} = [];

ax = get(fig, 'CurrentAxes');
ch = get(ax, 'Children');

for i = 1:numel(ch)
	if strcmp(get(ch(i), 'Tag'), 'edge')
		set(ch(i), 'Color', color_unsel);
	end
end


% ********************************************************************
function cb_paintedgeselection(fig, nodes, edges, color_sel, color_unsel)
ud = get(fig, 'UserData');
if numel(ud.geometry) == 1
	z = 1;
else 
	z = ud.index;
end

selected_list = ud.rselectededges{z};

ax = get(fig, 'CurrentAxes');
ch = get(ax, 'Children');

for i = 1:numel(ch)
	if strcmp(get(ch(i), 'Tag'), 'edge')
		n = nodes;
	
		x = get(ch(i), 'XData');
		y = get(ch(i), 'YData');
	
		node1 = [y(1) x(1)];
		ind1 = find((nodes(:, 1) == node1(1)) & (nodes(:, 2) == node1(2)));
	
		node2 = [y(2) x(2)];
		ind2 = find((nodes(:, 1) == node2(1)) & (nodes(:, 2) == node2(2)));
	
		if ind1(1) < ind2(1)
			ind = find((edges(:, 1) == ind1(1)) & (edges(:, 2) == ind2(1)));
		else
			ind = find((edges(:, 1) == ind2(1)) & (edges(:, 2) == ind1(1)));
		end
		
                if ~ isempty(ind)
                        ind = ind(1);
                else
                        continue;
                end 
                
		% If this edge is selected, paint it with the appropriate color.
                if (~ isempty(selected_list)) & (~ isempty(find(selected_list == ind)))
			set(ch(i), 'Color', color_sel);
		% If not, use the other color.
		else
			set(ch(i), 'Color', color_unsel);
		end
	end
end


% ********************************************************************
function ud = cb_selectedges(fig, index)
% index is the time point index in the geometry array.
ud = get(fig, 'UserData');

roi = load_roi(index + ud.imshift);
n = ud.geometry(index).nodes;
e = ud.geometry(index).edges;
if numel(ud.geometry) == 1
	z = 1;
else 
	z = index;
end

[tmp ud.rselectededges{z}] = mmaskpositions(n, e, roi);

% ********************************************************************
function ud = cb_selectorientationedges(varargin)

fig = varargin{1};
index = varargin{2};

ud = cb_selectedges(fig, index);
if numel(ud.geometry) == 1
	z = 1;
else 
	z = index;
end

m = measure_edges(ud.geometry(index), ud.rselectededges{z});
ids = m.ID;
ang = m.Orientation;

ind = [];
for ii = 1:(nargin-2)
	or = varargin{2+ii};
	min_ang = min(or);
	max_ang = max(or);
	ind = cat(2, ind, find((ang >= min_ang) & (ang <= max_ang)));
end	

ud.rselectededges{z} = ids(ind);


% ********************************************************************
function ud = cb_selectunalignededges(fig, index, ang, min_edgelength)

% Find user data.
%ud = cb_selectedges(fig, index);
ud = get(fig, 'UserData');
if numel(ud.geometry) == 1
	z = 1;
else 
	z = index;
end

% Load roi and measure alignment.
roi = load_roi(index+ud.imshift);
[cl chains] = alignment(ud.geometry, index, ang * pi / 180., 0, roi, ud.imshift, min_edgelength);

% Find the chains of length 1.
ind = find(cl == 1);
ids = [];
for ii = 1:numel(ind)
	ids = cat(2, ids, chains{ind(ii)});
end

ids = unique(ids);

% This ids index in the list of interfaces within the roi.
% We now need to convert those indeces to point to the list of all interfaces.
[tmp roiedges] = mmaskpositions(ud.geometry(index).nodes, ud.geometry(index).edges, roi);

ids = roiedges(ids);

ud.rselectededges{z} = ids;



% ********************************************************************
function ud = cb_selectpositiveedges(fig, index, method, thr, ang, min_edgelength, use_old, mind, minint)

% Find user data.
%ud = cb_selectedges(fig, index);
ud = get(fig, 'UserData');
if numel(ud.geometry) == 1
	z = 1;
else 
	z = index;
end

%  % Load roi and measure intensity.
roi = load_roi(ud.imshift + index);
if isempty(roi)
        roi = [0 0; ud.imsize(1)-1 ud.imsize(2)-1];
end
ud.roi = roi;

if (~ use_old) | isempty(ud.rmeasurements{index})
	switch method 
	 case {1, 4, 5, 6} % Intensity relative to local DV or background-corrected intensity relative to local DV.
		% Select horizontal interfaces.
		ud = cb_selectalignededges(fig, index, -30, 1, min_edgelength, 0);
		set(fig, 'UserData', ud);
		
		% Measure the intensity of these interfaces.
		[mh tmp mroi sroi maxroi minroi meanbg]  = measure_edges(ud.geometry(index), ud.rselectededges{index}, ud, 1, -1, 3, 1, mind, minint);
		
		% Use a dip_measurement as the normalization factor.
		norm_factor = mh;
	
	 case 3 % Background-corrected intensity relative to all DV.
		% Select horizontal interfaces.
		ud = cb_selectalignededges(fig, index, -30, 1, min_edgelength, 0);
		set(fig, 'UserData', ud);
		
		% Measure the intensity of these interfaces.
		[mh tmp mroi sroi maxroi minroi meanbg]  = measure_edges(ud.geometry(index), ud.rselectededges{index}, ud, 1, -1, 3, 1, mind, minint);
		
		% Find the normalization factor (background-substracted DV intensity).
		norm_factor = mean(mh.MeanBackgroundCorrectedIntensity); % Normalized to 1.
		
		fprintf('\nMean background intensity is %.2f.\n', meanbg);
		fprintf('\nMean bg-corrected DV intensity is %.2f.', norm_factor);
	 otherwise
		norm_factor = 1;
		meanbg = 0;
	end
	
	% Select interfaces within the angles requested.
	ud = cb_selectalignededges(fig, index, ang, 1, min_edgelength, 0);
	set(fig, 'UserData', ud);
	nAP = numel(ud.rselectededges{index});
	
	% Compute intensities.
	[m tmp mroi sroi maxroi minroi meanbg] = measure_edges(ud.geometry(index), ud.rselectededges{index}, ud, 1, -1, 3, norm_factor, mind, minint);
	
	ud.rmeasurements{index} = m;
	ud.rmeanroi(index) = mroi;
	ud.rmeanbg(index) = meanbg;
	ud.rstdroi(index) = sroi;
	ud.rmaxroi(index) = maxroi;
	ud.rminroi(index) = minroi;
else
	% Select interfaces within the angles requested.
	ud = cb_selectalignededges(fig, index, ang, 1, min_edgelength, 0);
	set(fig, 'UserData', ud);
	nAP = numel(ud.rselectededges{index});
	
	m = ud.rmeasurements{index};
	mroi = ud.rmeanroi(index);
	meanbg = ud.rmeanbg(index);
	fprintf('\nMean background intensity is %.2f.', meanbg);
	sroi = ud.rstdroi(index);
	maxroi = ud.rmaxroi(index);
	minroi = ud.rminroi(index);
end

% Find the interfaces above the threshold.
% and store the selected interfaces.
switch method
	case 1
		% Get intensity values.
		int = m.MeanRelativeIntensity;
		
	case 2
		% Compute threshold based on absolute intensities.
		%int = m.MeanAbsoluteIntensity;
		%[m1 s1] = rmsnan(int');
		%thr = (m1 + thr * s1);
		
		% Now read scaled intensities ...
		int = m.MeanScaledIntensity;
		
		% ... and scale threshold.
		%thr = thr / mroi;
		%thr = 1 + (thr - maxroi) / (maxroi - minroi);
		
	case {3, 4}
		int = m.MeanBackgroundCorrectedIntensity;
		
	case 5
		int = m.MeanAboveDVIntensity;
                
        case 6
                int = m.MeanBgAndDVSubstractedIntensity;
		
	otherwise
		% Get intensity values.
		int = m.MeanAbsoluteIntensity;
		
		% Compute threshold.
		%[m1 s1] = rmsnan(int');
		%thr = (m1 + thr * s1);
end

%disp(m);

%fprintf('\nThreshold value: %.2f\n\n', thr);

%if ang >=0 
%	ids = find(int >= thr & m.Length >= min_edgelength & m.Orientation >= ang & m.Orientation <= (180.-ang));
%else
%	ang = - ang;
%	ids = find(int >= thr & m.Length >= min_edgelength & (m.Orientation <= ang | m.Orientation >= (360. - ang)));
%end

if thr >= 0 & ang >=0
	ids = find(int >= thr & m.Length >= min_edgelength & m.Orientation >= ang & m.Orientation <= (180.-ang));
elseif ang >= 0 % Negative threshold selects interfaces BELOW a certain value.
	ids = find(int <= abs(thr) & m.Length >= min_edgelength & m.Orientation >= ang & m.Orientation <= (180.-ang)); 
elseif thr >= 0 % Negative angle selects interfaces below a certain angle.
	ang = - ang;
	ids = find(int >= thr & m.Length >= min_edgelength & (m.Orientation <= ang | m.Orientation >= (360. - ang)));
else % Both negative threshold and negative angle.
	ang = - ang;
	ids = find(int <= abs(thr) & m.Length >= min_edgelength & m.Orientation >= ang & m.Orientation <= (180.-ang)); 
end

% These ids index in the list of interfaces within the roi.
% We now need to convert those indeces to point to the list of all interfaces.
%[tmp roiedges] = mmaskpositions(ud.geometry(index).nodes, ud.geometry(index).edges, roi);

%ids = roiedges(ids);
ids = m.id(ids);

[nn ne] = mmaskpositions(ud.geometry(index).nodes, ud.geometry(index).edges, roi);
nTot = numel(ne);

fprintf('\n%d/%d AP interfaces.', nAP, nTot);
fprintf('\n%d/%d AP Myosin-positive interfaces.', numel(ids), nAP);

ud.rselectededges{z} = ids;




% ********************************************************************
function ud = cb_toggleedgeselection(fig, ind, color_sel, color_unsel)

ud = get(fig, 'UserData');
obj = get(fig, 'CurrentObject');

if strcmp(get(obj, 'Tag'), 'edge')
	if numel(ud.geometry) == 1
		z = 1;
	else 
		z = ud.index;
	end
	
	selected_list = ud.rselectededges{z};
	isselected = find(selected_list == ind);

	% If not selected, select.
	if isempty(isselected)
		ud.rselectededges{z} = cat(2, ud.rselectededges{z}, ind);
		set(obj, 'Color', color_sel);
% If selected, unselect.
	else
		otherselected = find(selected_list ~= ind);
		allselected = ud.rselectededges{z};
		ud.rselectededges{z} = allselected(otherselected);
		set(obj, 'Color', color_unsel);
	end
end
% ********************************************************************
function ud = cb_unselectedge(fig, ind, color_unsel)

ud = get(fig, 'UserData');
obj = get(fig, 'CurrentObject');

if strcmp(get(obj, 'Tag'), 'edge')
	if numel(ud.geometry) == 1
		z = 1;
	else 
		z = ud.index;
	end
	
	selected_list = ud.rselectededges{z};
	isselected = find(selected_list == ind);

	% If selected, unselect.
	if ~ isempty(isselected)
		otherselected = find(selected_list ~= ind);
		allselected = ud.rselectededges{z};
		ud.rselectededges{z} = allselected(otherselected);
		set(obj, 'Color', color_unsel);
	end
end

% ********************************************************************
function [n e] = cb_deleteedge_old(fig, nodes, edges, color, color_selected, process)

obj = get(fig, 'CurrentObject');
if strcmp(get(obj, 'Tag'), 'edge')
	n = nodes;
	
	x = get(obj, 'XData');
	y = get(obj, 'YData');	
	
	node1 = [y(1) x(1)];
	ind1 = find((nodes(:, 1) == node1(1)) & (nodes(:, 2) == node1(2)));
	
	node2 = [y(2) x(2)];
	ind2 = find((nodes(:, 1) == node2(1)) & (nodes(:, 2) == node2(2)));
	
	if ind1(1) < ind2(1)
		keep_edges = find((edges(:, 1) ~= ind1(1)) | (edges(:, 2) ~= ind2(1)));
	else
		keep_edges = find((edges(:, 1) ~= ind2(1)) | (edges(:, 2) ~= ind1(1)));
	end

	e = edges(keep_edges, :);
	
	delete(obj);
	
	% If one of the nodes was connected to two other edges,
	% delete them and connect the extremes.
	other_edges1 = find((e(:, 1) == ind1) | (e(:, 2) == ind1));
	if process == 1 & numel(other_edges1) == 2
		% Edge 1.
		edge1 = e(other_edges1(1), :);
		anode1 = edge1(find(edge1 ~= ind1));
		% Edge 2.
		edge2 = e(other_edges1(2), :);
		anode2 = edge2(find(edge2 ~= ind1));
		
		% Delete the shared node.
		ch = get(get(fig, 'CurrentAxes'), 'Children');
		for jj = 1:numel(ch)
			if strcmp(get(ch(jj), 'Tag'), 'node')
				x = get(ch(jj), 'XData');
				y = get(ch(jj), 'YData');
			
				if ((x == node1(2)) & (y == node1(1)))
					set(fig, 'CurrentObject', ch(jj));
					[n e] = cb_deletenode(fig, n, e);
					break;
				end
			end
		end
		
		% Reconnect the two isolated nodes.
		e = cb_addedge(fig, n, e, anode1, anode2, color, color_selected);
	end

	other_edges2 = find((e(:, 1) == ind2) | (e(:, 2) == ind2));
	if process == 1 & numel(other_edges2) == 2
		% Edge 1.
		edge1 = e(other_edges2(1), :);
		anode1 = edge1(find(edge1 ~= ind2));
		% Edge 2.
		edge2 = e(other_edges2(2), :);
		anode2 = edge2(find(edge2 ~= ind2));
		
		% Delete the shared node.
		ch = get(get(fig, 'CurrentAxes'), 'Children');
		for jj = 1:numel(ch)
			if strcmp(get(ch(jj), 'Tag'), 'node')
				x = get(ch(jj), 'XData');
				y = get(ch(jj), 'YData');
			
				if ((x == node2(2)) & (y == node2(1)))
					set(fig, 'CurrentObject', ch(jj));
					[n e] = cb_deletenode(fig, n, e);
					break;
				end
			end
		end
		
		% Reconnect the two isolated nodes.
		e = cb_addedge(fig, n, e, anode1, anode2, color, color_selected);
	end
		
else
	n = nodes;
	e = edges;
end


% ********************************************************************
function [n e] = cb_inspectedge(fig, min_dist, min_int)

min_dv = 5;

obj = get(fig, 'CurrentObject');
if strcmp(get(obj, 'Tag'), 'edge')
	% First find the edge.
	ud = get(fig, 'UserData');
	
	nodes = ud.geometry(ud.index).nodes;
	edges = ud.geometry(ud.index).edges;
	
	x = get(obj, 'XData');
	y = get(obj, 'YData');	
	
	node1 = [y(1) x(1)];
	ind1 = find((nodes(:, 1) == node1(1)) & (nodes(:, 2) == node1(2)));
	
	node2 = [y(2) x(2)];
	ind2 = find((nodes(:, 1) == node2(1)) & (nodes(:, 2) == node2(2)));
	
	if ind1 < ind2
		theindex = find((edges(:, 1) == ind1(1)) & (edges(:, 2) == ind2(1)));
	else
		theindex = find((edges(:, 1) == ind2(1)) & (edges(:, 2) == ind1(1)));
	end
	
	% Now measure it.
	%[m ih mean_roi std_roi max_roi min_roi mean_bg] = measure_edges(ud.geometry(ud.index), theindex, ud, 1, 1, 3);
	m = ud.rmeasurements{ud.index};
	%measure_edges(ud.geometry(ud.index), theindex, ud, 1, -1, 3, 1, min_dist, min_int)
	m = m(theindex);
	mean_bg = ud.rmeanbg(ud.index);
	
	% Now measure nearby DV interfaces. Using ud2 we avoid changing the selection (of edges) in the current time point.
	ud2 = cb_selectalignededges(fig, ud.index, -30, 1, 3, 0);
	%set(fig, 'UserData', ud);
	[mh tmp mroi sroi maxroi minroi meanbg]  = measure_edges(ud2.geometry(ud2.index), ud2.rselectededges{ud.index}, ud2, 1, -1, 3, 1, min_dist, min_int);
        
	dvcoords = [mh.X' mh.Y'];
	thiscoords = repmat([sum(x)/2, sum(y)/2], [size(dvcoords, 1), 1]);
	d = dvcoords - thiscoords;
	d = sum(d .* d, 2);	
	d = sqrt(d);	
	dth = min_dist;	
	ind = find(d < dth);
	dth2 = dth;
	
	% If not enough interfaces are close enough, expand the search area.
	while numel(ind) < min_int	 & dth2 < abs(ud.roi(1, 1)-ud.roi(2, 1))		
		dth2 = dth2 + .10 * dth;
		ind = find((d < dth2)); % find((d > 0) & (d < dth2)); Does not alter the results, but avoids using the interface itself in the case of the measurement of a DV interface.
		anidx = find(mh.Length(ind) >= min_dv); % These two lines control that very short DV edges, which tend to be bright due to node intentsity, do not mess up the measurements.
		ind = ind(anidx);
	end
	
	%disp(ind);

	%ud.rselectededges{ud.index} = mh.id(ind);
	%set(fig, 'UserData', ud);
	%cb_paintedgeselection(fig, ud.geometry(ud.index).nodes, ud.geometry(ud.index).edges, 'b', 'r');			
	% Finally, print out the measurements.
	disp(mh.MeanAbsoluteIntensity(ind));
	%fprintf('\n(%.0f, %.0f) ang = %.2f, bg = %.2f, abs_dv = %.2f, bg_corr_dv = %.2f, rel_dv = %.2f, abs = %.2f, bg_corr = %.2f, rel = %.2f', m.X, m.Y, m.Orientation, mean_bg, mean(mh.MeanAbsoluteIntensity(ind)), mean(mh.MeanBackgroundCorrectedIntensity(ind)), mean(mh.MeanRelativeIntensity(ind)), m.MeanAbsoluteIntensity, m.MeanBackgroundCorrectedIntensity, m.MeanRelativeIntensity);
	fprintf('\n(%.0f, %.0f) id = %.0f, ang = %.2f, l = %.2f, bg = %.2f, abs dv = %.2f, dv = %.2f, abs this = %.2f, this = %.2f', m.X, m.Y, m.id, m.Orientation, m.Length, mean_bg, mean(mh.MeanAbsoluteIntensity(ind)), mean(mh.MeanBackgroundCorrectedIntensity(ind)), m.MeanAbsoluteIntensity, (m.MeanAbsoluteIntensity - mean_bg)/mean(mh.MeanBackgroundCorrectedIntensity(ind))); %
end

% ********************************************************************
function [n e] = cb_deleteedge(fig, nodes, edges, color, color_selected, process)

obj = get(fig, 'CurrentObject');
if strcmp(get(obj, 'Tag'), 'edge')
	n = nodes;
	
	x = get(obj, 'XData');
	y = get(obj, 'YData');
	
	node1 = [y(1) x(1)];
	ind1 = find((nodes(:, 1) == node1(1)) & (nodes(:, 2) == node1(2)));
	
	node2 = [y(2) x(2)];
	ind2 = find((nodes(:, 1) == node2(1)) & (nodes(:, 2) == node2(2)));
	
	if ind1(1) < ind2(1)
		keep_edges = find((edges(:, 1) ~= ind1(1)) | (edges(:, 2) ~= ind2(1)));
	else
		keep_edges = find((edges(:, 1) ~= ind2(1)) | (edges(:, 2) ~= ind1(1)));
	end

	e = edges(keep_edges, :);
	
	delete(obj);
	
	if process
	
		% If one of the nodes was connected to two other edges,
		% delete them and connect the extremes.
		other_edges1 = find((e(:, 1) == ind1) | (e(:, 2) == ind1));
		if numel(other_edges1) == 2
			% Edge 1.
			edge1 = e(other_edges1(1), :);
			anode1 = edge1(find(edge1 ~= ind1));
			% Edge 2.
			edge2 = e(other_edges1(2), :);
			anode2 = edge2(find(edge2 ~= ind1));
			
			% Delete the shared node.
			ch = get(get(fig, 'CurrentAxes'), 'Children');
			for jj = 1:numel(ch)
				if strcmp(get(ch(jj), 'Tag'), 'edge')
					x = get(ch(jj), 'XData');
					y = get(ch(jj), 'YData');
				
					if (((x(1) == node1(2)) & (y(1) == node1(1))))
						[n e] = cb_deletenode(fig, n, e, [x(1) y(1)]);
						break;
					elseif (((x(2) == node1(2)) & (y(2) == node1(1))))
						[n e] = cb_deletenode(fig, n, e, [x(2) y(2)]);
						break;
					end
				end
			end
			
			% Reconnect the two isolated nodes.
			e = cb_addedge(fig, n, e, anode1, anode2, color, color_selected);
		end
	
		other_edges2 = find((e(:, 1) == ind2) | (e(:, 2) == ind2));
		if numel(other_edges2) == 2
			% Edge 1.
			edge1 = e(other_edges2(1), :);
			anode1 = edge1(find(edge1 ~= ind2));
			% Edge 2.
			edge2 = e(other_edges2(2), :);
			anode2 = edge2(find(edge2 ~= ind2));
			
			% Delete the shared node.
			ch = get(get(fig, 'CurrentAxes'), 'Children');
			for jj = 1:numel(ch)
				if strcmp(get(ch(jj), 'Tag'), 'edge')
					x = get(ch(jj), 'XData');
					y = get(ch(jj), 'YData');
				
					if (((x(1) == node2(2)) & (y(1) == node2(1))))
						[n e] = cb_deletenode(fig, n, e, [x(1) y(1)]);
						break;
					elseif (((x(2) == node2(2)) & (y(2) == node2(1))))
						[n e] = cb_deletenode(fig, n, e, [x(2) y(2)]);
						break;
					end
				end
			end
			
			% Reconnect the two isolated nodes.
			e = cb_addedge(fig, n, e, anode1, anode2, color, color_selected);
		end
	end
		
else
	n = nodes;
	e = edges;
end

% *********************************************************************************
% ang is in degrees.
function [m cl] = cb_measuremovie2D(geometry, tpo, tpf, ang, min_length, imshift, save_pics)

frame_indeces = (tpo-imshift):(tpf-imshift);
cl = cell(0);
lengths = cell(0);
mcl = zeros(1, numel(frame_indeces));
stdcl = zeros(1, numel(frame_indeces));
mcl_le2 = zeros(1, numel(frame_indeces));
stdcl_le2 = zeros(1, numel(frame_indeces));
align_f = zeros(1, numel(frame_indeces));
align_f2 = zeros(1, numel(frame_indeces));
align_le3 = zeros(1, numel(frame_indeces));
align_le4 = zeros(1, numel(frame_indeces));
max_align = zeros(1, numel(frame_indeces));

wb = waitbar(0, 'Measuring movie ...');

% For each movie frame.
for i = 1:numel(frame_indeces)
	% READ ROI ----------------------------------------------------------------------
	roi = load_roi(tpo+i-1);
	
	% COMPUTE ALIGNMENT ----------------------------------------------------------
	%save tmp geometry frame_indeces i ang save_pics roi imshift
	
	cl{i} = alignment(geometry, frame_indeces(i), ang*pi/180., save_pics, roi, imshift, min_length); % ang is in degrees.
	total = sum(cl{i});
	aligned = sum(cl{i}(find(cl{i} > 1)));
	align_f(i) = aligned / total; % Percentage of edges of the right orientation that participates in chains of length > 1.
	align_f2(i) = sum(cl{i} > 1) / numel(cl{i}); % Percentage of chains with more than one element.
	align_le3(i) = sum(cl{i}(find(cl{i} >= 3))) / total; % Percentage of edges of the right orientation that participates in chains of length >= 3.
	align_le4(i) = sum(cl{i}(find(cl{i} >= 4))) / total; % Percentage of edges of the right orientation that participates in chains of length >= 4.
	max_align(i) = max(cl{i});
	mcl(i) = mean(cl{i});
	stdcl(i) = std(cl{i}) ./ sqrt(numel(cl{i}));
	mcl_le2(i) = mean(cl{i}(find(cl{i} >= 2)));
	stdcl_le2(i) = std(cl{i}(find(cl{i} >= 2))) ./ sqrt(numel(cl{i}));
	
	waitbar((i)/(tpf-tpo+1), wb);
end

m = dip_measurement(tpo:tpf, 'AlignmentFactor1', align_f, 'AlignmentFactor2', align_f2, 'AlignmentFactorLE3', align_le3, 'AlignmentFactorLE4', align_le4, 'MeanChainLength', mcl, 'SEMChainLength', stdcl, 'MeanChainLengthLE2', mcl_le2, 'SEMChainLengthLE2', stdcl_le2, 'LongestChain', max_align);

close(wb);

% *********************************************************************************
function cb_savegeometry(ud, fullpath)

geometry = ud.geometry;
measurements = ud.rmeasurements;
mean_roi = ud.rmeanroi;
mean_bg = ud.rmeanbg;
std_roi = ud.rstdroi;
max_rel_roi = ud.rmaxroi;
min_rel_roi = ud.rminroi;

if nargin < 2
        [filename, pathname, ext] = uiputfile('*.mat', 'Save geometry');
        if (~ isequal(filename, 0))
		        save(cat(2, pathname, filename), 'geometry', 'measurements', 'mean_roi', 'mean_bg', 'std_roi', 'max_rel_roi', 'min_rel_roi');
        end
else
        if (~ isequal(fullpath, 0))
                        save(fullpath, 'geometry', 'measurements', 'mean_roi', 'mean_bg', 'std_roi', 'max_rel_roi', 'min_rel_roi');
        end
end
% *********************************************************************************
function cb_saveroi(ud)

roi = ud.roi(1:2, 1:2);
[filename, pathname, ext] = uiputfile('*.mat', 'Save ROI');
if (~ isequal(filename, 0))
		save(cat(2, pathname, filename), 'roi');
end


% ********************************************************************
function cb_showhideedges(fig)

flag = 'on';

ch = get(get(fig, 'CurrentAxes'), 'Children');
for jj = 1:numel(ch)
	if strcmp(get(ch(jj), 'Tag'), 'edge')
		if strcmp(get(ch(jj), 'Visible'), 'on')
			flag = 'off';
			break;
		else 
			flag = 'on';
			break;
		end
	end
end

for jj = jj:numel(ch)
	if strcmp(get(ch(jj), 'Tag'), 'edge')
		set(ch(jj), 'Visible', flag);
	end
end
