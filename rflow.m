% [Xflow Yflow Xpos Ypos fig] = rflow(im1, im2, final_res, ws, plots, gradient_flag, border_width, step_sz, filter_output, min_normxcorr)
% Calculates the 2D flow field to go from im1 to im2.
% 
% im1 and im2 are of type dip_image.
% final_res determines how far the pixels in the final flow are after interpolating the results. Alternatively, if this is a matrix with more than one row, the flow is only interpolated in the points given in this matrix interpreted as [x1 y1; x2 y2; ...; xn yn].
% ws is the window size used to calculate the flow. It defaults to [64 by 64]. If final_res is smaller than ws in any dimension, cubic spline interpolation will be used to compute the flow field with the appropriate spatial resolution. This parameter also determines how far can an object move across time points, as the algorithm used here is fast but only calculates one crosscorrelation per window in the image. If you use a version calculating all possible crosscorrelations, then any displacements would be fine (there is some commented code for that at the end of this function). But in that case, you will need to use a normalized crosscorrelation to be able to compare similarity values with different target windows. Another possibility is to change the metric from a crosscorrelation to something similar to what I used with Ignacio for registration: an adaptive threshold is applied to both gradient images, and the inverse of the distance transform is calculated for the target image. The metric is the sum of the product of the binarized version of the gradient image times the distance transform of the destination image. Empirically, if this value is [32 32] cells imaged at 63X with a binning of 2 every 15 sec can move faster, and some flow values will be miscalculated. See min_normxcorr for how to use a pyramidal approach regarding window size.
% plots is 1 if the flow field is to be plotted (at ws resolution first and at final_res on a second image) (defaults to 1).
% gradient_flag is 1 (default) to match using the magnitude of the image gradients, any other value to use the raw data.
% border_width determines the number of rows and columns to delete from the calculation. This is done after the initial flow calculation, and it is then passed on to the interpolation (default is 1).
% step_sz determines in how many pixels the flow is calculated (defaults to half of the window size).
% filter_output is 1 if you want to find outliers in the resulting data and filter them (defaults to zero).
% min_normxcorr is the minimum cross correlation value accepted as a "significant" cross-correlation. If the maximum cross-correlation value obtained at a particular pixel is smaller than this number, the window size will be duplicated, and the cross-correlation will be computed again (only for this particular pixel). The process is repeated until the cross-correlation value is >= than min_normxcorr or until the window size is greater than the image size (default is 0.0, or no pyramidal window size). In this last case, a value of nan is returned.
%
% Xflow is the X coordinate of the flow vectors. Xflow(ii) corresponds to the X coordinate of the flow vector at [Xpos(ii) Ypos(ii)] (see below).
% Yflow is the Y coordinate of the flow vectors. Yflow(ii) corresponds to the Y coordinate of the flow vector at [Xpos(ii) Ypos(ii)] (see below).
% Xpos contains the X coordinates of the points where the flow field has been computed.
% Ypos contains the Y coordinates of the points where the flow field has been computed.
% fig is empty if plots == 0, or a plot of the origin image with the vectors representing the field overlaid if plots ~= 0.
%
% Rodrigo Fernandez-Gonzalez
% 20110730
%
% See also: FINDSHIFT (DIPImage).


function [Xvaldesired Yvaldesired X0desired2 Y0desired2 fig] = rflow(firstslice, secondslice, desired_ws, ws, plots, gradient_flag, border_width, step_sz, filter_output, min_normxcorr)
tic
if nargin < 10
    min_normxcorr = 0.0;
end

if nargin < 9
        filter_output = 0;
end

if nargin < 2 || (~ isa(firstslice, 'dip_image')) || (~ isa(secondslice, 'dip_image'))
        error('At least two arguments are necessary and they must be of type dip_image.');
end

if nargin < 7 || border_width < 0
        border_width = 1;
end        

if nargin < 6
        gradient_flag = 1;
end        

if nargin < 5
        plots = 1;
end        

if nargin < 4
        if nargin == 3 && size(desired_ws, 1) == 1
                ws = desired_ws;
        else                
                ws = [64 64]; % This determines how far can an object move across time points if you use the algorithm below, which is fast but only calculates one crosscorrelation per window in the image. If you use the version above, calculating all possible crosscorrelations, then any displacements would be fine. But in that case, you will need to use a normalized crosscorrelation to be able to compare similarity values with different target windows. Another possibility is to change the metric from a crosscorrelation to something similar to what I used with Ignacio for registration: an adaptive threshold is applied to both gradient images, and the inverse of the distance transform is calculated for the target image. The metric is the sum of the product of the binarized version of the gradient image times the distance transform of the destination image. Empirically, if this value is [32 32] cells imaged at 63X with a binning of 2 every 15 sec can move faster, and some flow values will be miscalculated.
        end
end        

if nargin < 8
        step_sz = floor(ws/2);
end

if nargin < 3
        desired_ws = ws;
end

if numel(ws) == 1
        ws = [ws ws];
end

% points_flag indicates if the user wants to find the flow field at
% specific points. In that case, one can speed up the analysis a bit.
if numel(desired_ws) == 1
        desired_ws = [desired_ws desired_ws];
        points_flag = 0;
else
    points_flag = 1;
end

if numel(step_sz) == 1
        step_sz = [step_sz step_sz];
end

fig = [];

% Minimize dimensionality.
firstslice = squeeze(firstslice);
secondslice = squeeze(secondslice);

%firstslice = squeeze(ud.slices(:, :, 0));
%secondslice = squeeze(ud.slices(:, :, 1));
switch gradient_flag
        case 1
                firstimage = gradmag(firstslice);
                secondimage = gradmag(secondslice);
                
        otherwise
                firstimage = firstslice;
                secondimage = secondslice;
end        

sz = size(firstimage);

% First we calculate the flow field using ws as window size, and then we interpolate to achieve desired_ws.
Xval = []; % Flow field coordinates at ws resolution.
Yval = [];

%ind = 1;
% Slice ii of this image contains the correlation of the iith window in firstimage with all the windows in second image.
% CAREFUL: the cross-correlation image returned by normxcorr2 is larger than the target image! This is due to the fact that Matlab calculates the crosscorrelation for all overlaps between the template and the target image, including overlaps of 1 pixel.
%ccall = [];

ws0 = ws; % Store the original window size in case the pyramidal approach is used.

for ii = 0:step_sz(1):(sz(1)-1) % X scan.
        for jj = 0:step_sz(2):(sz(2)-1) % Y scan.
                % Ideally, what follows would be a do ... while ... end loop. Alas, Matlab does not have that structure, and that forces me to do some pretty unelegant things ...
        
                normxcorr = -1.0; % Initialize to -1 to go into the while loop at least once per pixel analyzed. 
                ws = ws0./2.; % The first thing done in the while loop is to double the window size (in case) we come from a previous iteration where the window size was not enough. So before going into the loop, I divide the window size by two. This also needs the 2*ws in the condition in the while loop.
                
                while normxcorr < min_normxcorr & 2*ws(1) < size(firstimage, 1) & 2*ws(2) < size(firstimage, 2)
                    ws = ws .* 2.; % Double window size.
                    search_ws = 2 .* ws + 1; % Search window size is twice the size of the interrogation window (+1 so that there is a central pixel).

                    % Interrogation window.
                    minx = max(0, ii-floor(ws(1)/2));
                    maxx = min(sz(1)-1, ii+floor(ws(1)/2));
                    miny = max(0, jj-floor(ws(2)/2));
                    maxy = min(sz(2)-1, jj+floor(ws(2)/2));                        
                    im1 = firstimage(minx:maxx, miny:maxy);
                    
                    % If the user wants to interpolate the vector field in specific points, and none of those points is in a given interrogation window, just skip the window by leaving the while loop.
                    if points_flag && (~ nnz(inpolygon(desired_ws(:, 1), desired_ws(:, 2), [minx maxx], [miny maxy])))
                        normxcorr = -1;
                        break;
                    end
                    
                    % Do this only for the windows that we are interested
                    % in (otherwise everything slows down dramatically).
                    % First condition: normxcorr2 will crash if all the values in the template are equal.
                    % Second condition:  make sure that at least 50% of the
                    % pixels are non-zero (this happens, for instance, when
                    % the images have been rotated). The results are much
                    % better than without this test.
                    if numel(unique(dip_array(im1))) == 1 || (nnz(dip_array(im1))/prod(size(im1))) < 0.5
                        
                        continue;
                    end
                    
                    % Search window (twice as large as the interrogation window).
                    minx = max(0, ii-floor(search_ws(1)/2));
                    maxx = min(sz(1)-1, ii+floor(search_ws(1)/2));
                    miny = max(0, jj-floor(search_ws(2)/2));
                    maxy = min(sz(2)-1, jj+floor(search_ws(2)/2));                        
                    im2 = secondimage(minx:maxx, miny:maxy);

                    % Calculate and save the crosscorrelation.
                    cc = dip_image(normxcorr2(dip_array(im1), dip_array(im2))); % dip_array returns the data as a uint16 matrix.              
                    %cc = dip_image(normxcorr2_mex(double(im1), double(im2))); % This is slower than normxcorr2, probably because this function needs to deal with doubles, and normxcorr2 can work with integers (if you use dip_array(im1, 'double') with the Matlab function, things slow down a bit).
                    
                    cc = cc((size(im1, 1)-1):(size(cc, 1)-1-size(im1, 1)+1), (size(im1, 2)-1):(size(cc, 2)-1-size(im1, 2)+1));
                    %ccall{ind} = cc;
                    %ind = ind + 1;
                    
                    if ii <= floor(ws(1)/2) % Too close to the left border for full interrogation and search windows.
                            centerx = 0;
                    elseif ii > floor(ws(1)/2) && ii < floor(search_ws(1)/2) % Too close to the left border for full search window.
                            centerx = ii-floor(ws(1)/2);
                    elseif ii >= (sz(1) - floor(ws(1)/2) -1) % Too close to the right border for full interrogation and search windows.
                            centerx = size(cc, 1)-1;
                    elseif ii < (sz(1) - floor(ws(1)/2) -1) && ii >= (sz(1) - floor(search_ws(1)/2) -1)% Too close to the right border for full search window.
                            centerx = floor(ws(1)/2) + 1;
                    else
                            centerx = floor(size(cc, 1)/2);
                    end                                
                    
                    if jj <= floor(ws(2)/2) % Too close to the top border for full interrogation and search windows.
                            centery = 0;
                    elseif jj > floor(ws(2)/2) && jj < floor(search_ws(2)/2) % Too close to the top border for full search window.
                            centery = jj-floor(ws(2)/2);
                    elseif jj >= (sz(2) - floor(ws(2)/2) -1) % Too close to the bottom border for full interrogation and search windows.
                            centery = size(cc, 2)-1;
                    elseif jj < (sz(2) - floor(ws(2)/2) -1) && jj >= (sz(2) - floor(search_ws(2)/2) -1)% Too close to the bottom border for full search window.
                            centery = floor(ws(2)/2) + 1;
                    else
                            centery = floor(size(cc, 2)/2);
                    end                                
                            
                    % Find the position of the maximum crosscorrelation.
                    [normxcorr coords] = max(cc);
                    %fprintf('\n(%d, %d) norm xcorr = %.4f (%d, %d)', ii, jj, normxcorr, coords(1)-centerx, coords(2)-centery);
                    
                    % Limit travelled distance: if the maximum cross-correlation is too far (e.g. on one of the corners, this can happen if there are black
                    % pixels in the image introduced when rotating them), repeat with a larger window size.
                    if norm(coords-[centerx centery], 2) >= mean(ws)/2
                        normxcorr = -1;
                    end

                end
                
                % If the cross-correlation was sufficient, store the coordinates of the maximum as the values of the flow field vector at the center of this window.
                if normxcorr >= min_normxcorr
                    Xval(jj/step_sz(2) + 1, ii/step_sz(1) + 1) = coords(1)-centerx; % This is different from the fast version because the cross-correlation image returned by normxcorr2 is larger than the target image (see above).
                    Yval(jj/step_sz(2) + 1, ii/step_sz(1) + 1) = coords(2)-centery;
                else
                    Xval(jj/step_sz(2) + 1, ii/step_sz(1) + 1) = nan;
                    Yval(jj/step_sz(2) + 1, ii/step_sz(1) + 1) = nan;
                end
                
        end
end

% Remove border measurements to avoid boundary effects.
Xval = Xval(1+border_width:end-border_width, 1+border_width:end-border_width);
Yval = Yval(1+border_width:end-border_width, 1+border_width:end-border_width);

% Generate the coordinates of all the centers of the windows of size ws.
[X0 Y0] = meshgrid((0:step_sz(1):(sz(1)-1)),  (0:step_sz(2):(sz(2)-1)));
% Remove border measurements to avoid boundary effects.
X0 = X0(1+border_width:end-border_width, 1+border_width:end-border_width);
Y0 = Y0(1+border_width:end-border_width, 1+border_width:end-border_width);

if filter_output
        % Filter the output data trying to remove outliers using a normalized median test (see Raffel et al., Chapter 6.1.5).
        mag = sqrt(Xval.*Xval + Yval.*Yval); % Vector magnitude.
        sk = 1; % Filter radius.
        kernel = ones(2*sk+1, 2*sk+1);
        kernel(sk+1, sk+1) = 0; % This is a kernel where all pixels are set to one except for the central pixel, set to zero. 
        
        medians = nan .* zeros(size(mag));
        residuals = nan .* zeros(size(mag));
        normresiduals = nan .* zeros(size(mag));
        
        % This moves a 3x3 kernel through the "mag" matrix.
        for ii = 1:size(mag, 1)
                miny = max(1, ii-sk); % Y coordinates.
                maxy = min(size(mag, 1), ii+sk);
                for jj = 1:size(mag, 2)
                        minx = max(1, jj - sk); % X coordinates.
                        maxx = min(size(mag, 2), jj + sk);
                        
                        mag_loop = mag;
                        mag_loop(ii, jj) = nan; % We set the vector in question to nan to then take advantage of rmediannan and calculate the median ignoring nans.
                        
                        subw = mag_loop(miny:maxy, minx:maxx); % Subwindow.
                        medians(ii, jj) = rmediannan(reshape(subw, [numel(subw), 1])); % this function will take the average of the 4th and 5th largest neighbors out of the eight neighbors of each vector (excluding borders).
                        residuals(ii, jj) = abs(mag(ii, jj)-medians(ii, jj));
                end
        end
        
        % This moves a 3x3 kernel through the "mag" matrix.
        for ii = 1:size(residuals, 1)
                miny = max(1, ii-sk); % Y coordinates.
                maxy = min(size(residuals, 1), ii+sk);
                for jj = 1:size(residuals, 2)
                        minx = max(1, jj - sk); % X coordinates.
                        maxx = min(size(residuals, 2), jj + sk);
                        
                        residuals_loop = residuals;
                        residuals_loop(ii, jj) = nan; % We set the vector in question to nan to then take advantage of rmediannan and calculate the median ignoring nans.
                        
                        subw = residuals_loop(miny:maxy, minx:maxx); % Subwindow.
                        themedianresidual = rmediannan(reshape(subw, [numel(subw), 1])); % this function will take the average of the 4th and 5th largest neighbors out of the eight neighbors of each vector (excluding borders).
                        normresiduals(ii, jj) = residuals(ii, jj) / (themedianresidual + 0.15); % Magic number from Raffel et al. Prevents divisions by 0 in static regions.
                end
        end
        
        % Threshold for small normalized residuals is the value under which 95% of the normalized residuals are included.
        allnormresiduals = reshape(normresiduals, [1, numel(normresiduals)]);
        thehisto = histc(allnormresiduals, 0:.5:max(allnormresiduals))./numel(allnormresiduals); % Histogram of normalized residuals.
        thecumsum = cumsum(thehisto); % Cumulative distribution function of residuals.
        [tmp th_ind] = min(abs(thecumsum-0.95)); % Find the bin in the histogram that is closest to 95%.
        eps_th = (th_ind-1)*.5; % Back-calculate the threshold value.
        [Yremove Xremove] = find(normresiduals > eps_th);
        %  Xval(ind_remove) = nan;
        %  Yval(ind_remove) = nan;
        % End filter the output data trying to remove outliers.
        
        % Replace the bad vectors using bilinear interpolation (see Raffel et al., 6.2).
        for ii = 1:numel(Yremove)
            Xval(Yremove(ii), Xremove(ii)) = interp2(X0, Y0, Xval, Xremove(ii), Yremove(ii), 'linear');
            Yval(Yremove(ii), Xremove(ii)) = interp2(X0, Y0, Yval, Xremove(ii), Yremove(ii), 'linear');
        end
        % End replace the bad vectors using bilinear interpolation (see Raffel et al., 6.2).
end %if filter_output
% Plot these vectors.
%if plots
%        firstslice
%        hold on;
%        h = quiver(X0, Y0, Xval, Yval, 1, 'color', 'r', 'LineWidth', 1.5);
% %          for ii = 1:size(X0, 1)
% %                  for jj = 1:size(Y0, 2)
% %                  %if ~((X0(ii) < ws(1)) || (Y0(ii) < ws(2)) || (X0(ii) >= (sz(1)-ws(1))) || (Y0(ii) >= (sz(2)-ws(2))))
% %                          h = line([X0(ii, jj) Xval(ii, jj)], [Y0(ii, jj) Yval(ii, jj)], 'color', 'r', 'LineWidth', 1); 
% %                          h = line([X0(ii, jj) X0(ii, jj)], [Y0(ii, jj) Y0(ii, jj)], 'color', 'r', 'Marker', 'o', 'LineWidth', 1);
% %                          drawnow;
% %                  %end
% %                  end
% %          end
% %        fig = gcf;
%end

% Generate the origin coordinates for the flow field with the appropriate resolution or use the points given by the user.
if size(desired_ws, 1) == 1
        [X0desired Y0desired] = meshgrid((0:desired_ws(1):(sz(1)-1)),  (0:desired_ws(2):(sz(2)-1)));;
else
        X0desired = desired_ws(:, 1);
        Y0desired = desired_ws(:, 2);
end        

% Interpolate the flow field. As in meshgrid, remember that the X coordinate corresponds to columns (second coordinate in Matlab) and the Y coordinate corresponds to rows (first coordinate in Matlab).
% interp2 will return nan for X0desired values outside the range specified in X0 (same for Y).
% one option is to pad the matrices with zeros and max values (see commented). But even then the result is no better than just setting to zero all nans. That can be done by adding a zero after the 'linear'. Other forms of interpolation, such as splines, do not return nan, but return
% values that are VERY wrong.
%Xval = [  zeros(1,size(Xval,2)+2); ...
%          zeros(size(Xval,1),1) Xval  zeros(size(Xval,1),1); ...
%          zeros(1,size(Xval,2)+2)  ];
%Yval = [  zeros(1,size(Yval,2)+2); ...
%          zeros(size(Yval,1),1) Yval  zeros(size(Yval,1),1); ...
%          zeros(1,size(Yval,2)+2)  ];
%[X0 Y0] = meshgrid((0:step_sz(1):(sz(1)-1)),  (0:step_sz(2):(sz(2)-1)));

Xvaldesired = interp2(X0, Y0, Xval, X0desired, Y0desired, 'linear', 0);
Yvaldesired = interp2(X0, Y0, Yval, X0desired, Y0desired, 'linear', 0);

% Calculate the end point of the flow field vector.
%  Xenddesired = X0desired + Xvaldesired;
%  Yenddesired = Y0desired + Yvaldesired;

% Return points where the flow was calculated.
X0desired2 = X0desired;
Y0desired2 = Y0desired;

if plots
        firstslice
        hold on;
        h = quiver(X0desired, Y0desired, Xvaldesired, Yvaldesired, 0,  'color', 'r', 'LineWidth', 1.5);
%          for ii = 1:numel(X0desired)
%          %if ~((X0(ii) < desired_ws(1)) || (Y0(ii) < desired_ws(2)) || (X0(ii) >= (sz(1)-desired_ws(1))) || (Y0(ii) >= (sz(2)-desired_ws(2))))
%                  h = line([X0desired(ii) Xenddesired(ii)], [Y0desired(ii) Yenddesired(ii)], 'color', 'r', 'LineWidth', 1); 
%                  h = line([X0desired(ii) X0desired(ii)], [Y0desired(ii) Y0desired(ii)], 'color', 'r', 'Marker', 'o', 'LineWidth', 1);
%                  drawnow;
%          %end
%          end 
        
        fig = gcf;
end
toc