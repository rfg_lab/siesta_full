% [m intensity_hist mean_roi std_roi max_roi min_roi] mean_bg = measure_edges(geometry, edge_indeces, <udata>, <channel>, <ref_channel>, <brush_sz>, <norm_factor>, <dth>)
% Channel is 1 or larger than 1.
% mean_roi is an absolute pixel value (unnormalized).
% max_roi and min_roi are given normalized to mean_roi. To find their absolute values multiply times mean_roi.
function [m intensity_hist mean_roi std_roi max_roi min_roi mean_bg] = measure_edges(geometry, edge_indeces, udata, channel, ref_channel, brush_sz, norm_factor, dth, intth, measure_int)

fig = gcf;
bin_sz = 15; % Bin size for orientation/intensity measurements.
min_dv = 5; % Min length of a DV interface to be used to compute the local DV intentsity around a certain interface. Used to avoid that very short DV interfaces, quite bright due to spill over from the nodes, mess up the intensity quantification.
if nargin < 9
	intth = 10; % Minimum number of interfaces to average for the reference measurement.
end

if nargin < 8
	dth = 15;
end

if nargin < 7
	norm_factor = 1;
end

if nargin < 6
	brush_sz = 1;
end

if nargin < 5
	ref_channel = -1;
end

if nargin < 4
	channel = 1;
end

if nargin < 3
	if isfield(get(fig), 'UserData')
		udata = get(fig, 'UserData');
	else
		udata = [];
		roi = [];
	end
end

if isfield(udata, 'roi') & (~ isempty(udata.roi))
	roi = udata.roi(1:2, 1:2);
elseif ~ isempty(udata)
	roi = [0 0; (udata.imsize(1)-1) (udata.imsize(2)-1)];
else
        roi = [0 0; max(geometry.nodes(:, 2)+1) max(geometry.nodes(:, 1))+1];
end

if nargin < 2
    edge_indeces = [];
end

n = geometry.nodes;
e = geometry.edges;

% Mask edges with ROI only if no interfaces were provided.
% Then select all the interfaces in the ROI.
if isempty(edge_indeces)
	[newnodes newedges] = mmaskpositions(n, e, roi);
	e = e(newedges, :);
	edge_indeces = 1:size(e, 1);
end

xcoord = nan .* ones(1, numel(edge_indeces));
ycoord = nan .* ones(1, numel(edge_indeces));
edge_length = nan .* ones(1, numel(edge_indeces));
edge_orientation = nan .* ones(1, numel(edge_indeces));
edge_meanintensity = nan .* ones(1, numel(edge_indeces));
edge_relativeintensity = nan .* ones(1, numel(edge_indeces)); % Relative intensity (normalized to the average roi intensity)
edge_scaledintensity1 = nan .* ones(1, numel(edge_indeces)); % Relative intensity (normalized to the average roi intensity) scaled to min_roi/mean_roi-max_roi/mean_roi (and rescaled to 0-1). min_roi and max_roi are chosen rather than 0-255 to account for bleaching over time (although this is minor).
edge_bgintensity = nan .* ones(1, numel(edge_indeces)); % Background-subtracted and relative to a normalization factor. 
edge_aboveintensity = nan .* ones(1, numel(edge_indeces)); % Intensity above local DV interfaces.
edge_bganddvsubstracted = nan .* ones(1, numel(edge_indeces)); % Background substracted intensity minus background substracted DV intensity.

wb = waitbar(0, 'Measuring edges ...');

% Measure roi intensity.
if ((~ isempty(udata)) & (isfield(udata, 'colordata')) & (~ isempty(udata.colordata)))
	amask = dip_image(zeros(udata.imsize(2), udata.imsize(1)), 'bin');
	amask(roi(1, 1):roi(2, 1), roi(1, 2):roi(2, 2)) = 1;
	mean_roi = mean(udata.colordata{channel}(amask));
	
	std_roi = std(udata.colordata{channel}(amask));
	%max_roi = 255/mean_roi;
	%min_roi = 0;
	max_roi = max(udata.colordata{channel}(amask))./mean_roi;
	min_roi = min(udata.colordata{channel}(amask))./mean_roi;
	
	% Measure background based on a manually drawn area.
	%if ~isempty(udata.bg)
	%	amask = dip_image(zeros(udata.imsize(2), udata.imsize(1)), 'bin');
	%	amask(udata.bg(1, 1):udata.bg(2, 1), udata.bg(1, 2):udata.bg(2, 2)) = 1;
	%	
	%	mean_bg = mean(udata.colordata{channel}(amask));
	%end
	
	% Measure background based on cytoplasmic pixels.
	%out = geometry2image(geometry, roi, size(amask));
	%bgmask = erosion(brmedgeobjs(~out, 1), 11);
	%mean_bg = mean(udata.colordata{channel}(bgmask));
	
	% Measure background based on space outside the embryo.
	bgmask = findbackground(udata.colordata{channel});
	save tmp bgmask;
	mean_bg = mean(udata.colordata{channel}(bgmask));
	
elseif ((~ isempty(udata)) & (isfield(udata, 'imagedata'))& (~ isempty(udata.imagedata)))
	amask = dip_image(zeros(udata.imsize(2), udata.imsize(1)), 'bin');
	amask(roi(1, 1):roi(2, 1), roi(1, 2):roi(2, 2)) = 1;
	mean_roi = mean(udata.imagedata{channel}(amask));
	std_roi = std(udata.imagedata{channel}(amask));
	max_roi = max(udata.imagedata{channel}(amask))./mean_roi;
	min_roi = min(udata.imagedata{channel}(amask))./mean_roi;
	
	% Measure background based on a manually drawn area.
	%if ~isempty(udata.bg)
	%	amask = dip_image(zeros(udata.imsize(2), udata.imsize(1)), 'bin');
	%	amask(udata.bg(1, 1):udata.bg(2, 1), udata.bg(1, 2):udata.bg(2, 2)) = 1;
	%	
	%	mean_bg = mean(udata.imagedata{channel}(amask));
	%end
	
	% Measure background based on cytoplasmic pixels.
	%out = geometry2image(geometry, roi, size(amask));
	%bgmask = erosion(brmedgeobjs(~out, 1), 11);
	%mean_bg = mean(udata.imagedata(bgmask));

	% Measure background based on space outside the embryo.
	bgmask = findbackground(udata.imagedata);
	if ~ isempty(nonzeros(unique(double(bgmask))))
		mean_bg = mean(udata.imagedata(bgmask));
	else
		mean_bg = 1;
	end
	

end

% For every edge ...
for i = 1:numel(edge_indeces)
	rel_int = nan;
	sc_int1  = nan;
	
	theedge = e(edge_indeces(i), :);
	thenodes = n(theedge, :);	
	
	x = thenodes(:, 2);
	y = thenodes(:, 1);
	
	% First point of the segment is that with the smallest X value.
        if (x(1) < x(2))
      	 p1 = [x(1) y(1)];
	 p2 = [x(2) y(2)];
        else
         p2 = [x(1) y(1)];
	 p1 = [x(2) y(2)];
        end
      
	% Check that the edge is within the roi.
	if min(x) < roi(1, 1) | max(x) > roi(2, 1) | min(y) < roi(1, 2) | max(y) > roi(2, 2)
		continue;
	end
	
        % Compute vector.
        v = p2 - p1;
        l = norm(v, 2);
      
        % If the second point is under the first one (largest y)
        % then the angle is larger than pi/2.
        if (l ~= 0 && p2(2) > p1(2))
         val = pi - asin(abs(v(2))/l);
        % Else the angle is smaller than pi/2.
        elseif (l ~= 0)
         val = asin(abs(v(2)) / l);
        % Else where are trying to measure the angle formed by a dot!!
        else
      	 val = 0.0;
        end           
	
	% TESTING - This piece of codes moves away from the nodes along the line determined by the interface (as many pixels as indicated by d) so that quantification of the intensity in an interface is not obscured by the intensity of the nodes.
%  	d = 5;
%  	
%  	yprimea = d / sqrt(((p2(1) - p1(1))/(p2(2) - p1(2)))^2 + 1) + p1(2);
%  	xprimea = sqrt(d * d - (yprimea - p1(2))^2) + p1(1);
%  		
%  	yprimeb = -1 * d / sqrt(((p2(1) - p1(1))/(p2(2) - p1(2)))^2 + 1) + p1(2);
%  	xprimeb = -1 * sqrt(d * d - (yprimeb - p1(2))^2) + p1(1);
%  
%  	da = norm(p2 - [xprimea yprimea], 2);
%  	db = norm(p2 - [xprimeb yprimeb], 2);
%  	
%  	if da < db
%  		x1 = xprimea;
%  		y1 = yprimea;
%  	else
%  		x1 = xprimeb;
%  		y1 = yprimeb;
%  	end
%  	
%  	
%  	toofar = norm([x1 y1] - p1, 2) > l;
%  	
%  	yprimea = p2(2) - d / sqrt(((p2(1) - p1(1))/(p2(2) - p1(2)))^2 + 1);
%  	xprimea = p2(1) - sqrt(d * d - (p2(2) - yprimea)^2);
%  		
%  	yprimeb = p2(2) + d / sqrt(((p2(1) - p1(1))/(p2(2) - p1(2)))^2 + 1);
%  	xprimeb = p2(1) + sqrt(d * d - (p2(2) - yprimea)^2);
%  
%  	da = norm(p1 - [xprimea yprimea], 2);
%  	db = norm(p1 - [xprimeb yprimeb], 2);
%  	
%  	if da < db
%  		x2 = xprimea;
%  		y2 = yprimea;
%  	else
%  		x2 = xprimeb;
%  		y2 = yprimeb;
%  	end
%  	
%  	x = round([x1 x2]);
%  	y = round([y1 y2]);
	
	% END TESTING	
	
       % Intensity.            
       if (~ isempty(udata)) & ((isfield(udata, 'colordata') & (~ isempty(udata.colordata))) | (isfield(udata, 'imagedata') & (~ isempty(udata.imagedata))))
	      mask = dip_image(zeros(udata.imsize(2), udata.imsize(1), 1), 'bin8'); 
	      [xl yl] = pixinline_bresenham([x(1) y(1); x(2) y(2)], brush_sz);
              mask(sub2ind([udata.imsize(2), udata.imsize(1)], yl, xl)) = 1;
%% Commented out.               
%                mask = dip_image(drawline2(mask, [x(1) y(1)], val, 1, brush_sz/4));
%  	      mask2 = dip_image(zeros(udata.imsize(2), udata.imsize(1), 1), 'bin8'); 
%        
%  	      [xc, yc] = meshgrid((min(x(1),x(2))-floor(brush_sz/2)):(max(x(1),x(2))+floor(brush_sz/2)), (min(y(1),y(2))-floor(brush_sz/2)):(max(y(1),y(2))+floor(brush_sz/2)));
%        
%  	      mask2(xc, yc) = 1;
%  	          
%  	      mask = (mask.*mask2>0);
%% End commented out.	      
              %save tmp mask mask2  
 	      %if nnz(double(mask)) == 0
	      %	disp('Empty!');
	      %end
	      %if numel(edge_indeces) == 1
	      %	im1 = udata.colordata{channel};
	      %im2 = mask;
	      %	save tmp im1 im2
	      %end
	      % Color image.
	      if (isfield(udata, 'colordata') && (~ isempty(udata.colordata))&& (~ isempty(udata.colordata{channel}(mask))))
		      int = mean(udata.colordata{channel}(mask));
		      %int = percentile(udata.colordata{channel}(mask), 50);
		      
		      % If relative intensitites are being measured, compute
		      % the intensity of the channel under analysis with respect
		      % to the reference one.
		      %if ref_channel > 0
		      	%rel_int = int ./ mean(udata.colordata{ref_channel}(mask)); % Normalize to the interface intensity in the other channel (this compensates for inhomogeneities in the illumination).
			
		      
		      
		      if isa(norm_factor, 'dip_measurement')
			% Compute distance between the interface under analysis and all normalization (DV) interfaces.
		      	dvcoords = [norm_factor.x' norm_factor.y'];
			thiscoords = repmat([sum(x)/2, sum(y)/2], [size(dvcoords, 1), 1]);
			d = dvcoords - thiscoords;
			d = sum(d .* d, 2);	
			d = sqrt(d);		
			
			% Find those interfaces closer than a certain distance, dth.
			ind = find(d < dth);
			dth2 = dth;
			
			% If no interfaces are close enough, expand the search area.
			while numel(ind) < intth & dth2 < abs(roi(1, 1)-roi(2, 1))				
				dth2 = dth2 + .10 * dth;
				ind = find(d < dth2); % find((d > 0) & (d < dth2)); Does not alter the results, but avoids using the interface itself in the case of the measurement of a DV interface.
				anidx = find(norm_factor.Length(ind) >= min_dv); % These two lines control that very short DV edges, which tend to be bright due to node intentsity, do not mess up the measurements. MAGIC NUMBER!!!
				ind = ind(anidx);
			end
			% Use the background corrected intensity of the "close" normalization interfaces as the normalization factor.
			nf = norm_factor.MeanBackgroundCorrectedIntensity;
			nf = mean(nf(ind));
			%if nf == 0
			%	disp(ind');
			%	disp(norm_factor.MeanBackgroundCorrectedIntensity(ind));
			%end
			
			bg_int = max(0, (int - mean_bg)) / nf;
		      	rel_int = int / mean(norm_factor.MeanRelativeIntensity(ind)); % Normalize to the DV intensity.
		      %end
		      	above_int = max(0, int - mean(norm_factor.MeanAbsoluteIntensity(ind)));
		        sc_int1 = 1 + (rel_int - max_roi) / (max_roi - min_roi);
		        bganddvsubstracted = max(0, (int - mean_bg) - nf);
		      else
		      	bg_int = max(0, (int - mean_bg)) / norm_factor;
			rel_int = int / norm_factor; % Normalize to the DV intensity.
		      %end
		        above_int = max(0, int - norm_factor);
			sc_int1 = 1 + (rel_int - max_roi) / (max_roi - min_roi);
                        bganddvsubstracted = max(0, (int - mean_bg) - norm_factor);                     
		      end
              
		      
	      % Grey scale.
	      %elseif ((~ isfield(udata, 'colordata'))  && (~ toofar))
	      elseif ((~ isfield(udata, 'colordata')))
	              int = mean(udata.imagedata(mask));
		      %int = percentile(udata.imagedata(mask));
		      if isa(norm_factor, 'dip_measurement')
			% Compute distance between the interface under analysis and all normalization (DV) interfaces.
		      	dvcoords = [norm_factor.x' norm_factor.y'];
			thiscoords = repmat([sum(x)/2, sum(y)/2], [size(dvcoords, 1), 1]);
			d = dvcoords - thiscoords;
			d = sum(d .* d, 2);	
			d = sqrt(d);		
			
			% Find those interfaces closer than a certain distance, dth.
			ind = find(d < dth);
			dth2 = dth;
			
			% If no interfaces are close enough, expand the search area.
			while numel(ind) < intth & dth2 < abs(roi(1, 1)-roi(2, 1))				
				dth2 = dth2 + .10 * dth;
				ind = find(d < dth2); % find((d > 0) & (d < dth2)); Does not alter the results, but avoids using the interface itself in the case of the measurement of a DV interface.
				anidx = find(norm_factor.Length(ind) >= min_dv); % These two lines control that very short DV edges, which tend to be bright due to node intentsity, do not mess up the measurements. MAGIC NUMBER!!!
				ind = ind(anidx);
			end
			
			% Use the background corrected intensity of the "close" normalization interfaces as the normalization factor.
			nf = norm_factor.MeanBackgroundCorrectedIntensity;
			nf = mean(nf(ind));
			
			bg_int = max(0, (int - mean_bg)) / nf; % Minus the "outside the embryo" intensity and normalized to the local DV intensity (which was also previously background substracted).
		      	rel_int = int / mean(norm_factor.MeanRelativeIntensity(ind)); 
		      %end
		        above_int = max(0, int - mean(norm_factor.MeanAbsoluteIntensity(ind)));
			sc_int1 = 1 + (rel_int - max_roi) / (max_roi - min_roi);
                        
                        bganddvsubstracted = max(0, (int - mean_bg) - nf);
		      else
		      	bg_int = max(0, (int - mean_bg)) / norm_factor;
		      	rel_int = int / norm_factor; % Normalize to the DV intensity.
		      %end
		        above_int = max(0, int - norm_factor);
			sc_int1 = 1 + (rel_int - max_roi) / (max_roi - min_roi);
                        bganddvsubstracted = max(0, (int - mean_bg) - norm_factor);                     
		      end
	
%  	      elseif toofar
%        		      int = cb_getpixvalue(fig, round([sum(x)/2-1 sum(y)/2-1]));
%  		      int = int(channel);
%  		      rel_int = int ./ mean_roi;
%  		      sc_int1 = 1 + (rel_int - max_roi) / (max_roi - min_roi);
%  		      if isa(norm_factor, 'dip_measurement')
%  			% Compute distance between the interface under analysis and all normalization (DV) interfaces.
%  		      	dvcoords = [norm_factor.x' norm_factor.y'];
%  			thiscoords = repmat([sum(x)/2, sum(y)/2], [size(dvcoords, 1), 1]);
%  			d = dvcoords - thiscoords;
%  			d = sum(d .* d, 2);	
%  			d = sqrt(d);		
%  			
%  			% Find those interfaces closer than a certain distance, dth.
%  			ind = find(d < dth);
%  			dth2 = dth;
%  			
%  			% If no interfaces are close enough, expand the search area.
%  			while numel(ind) < intth				
%  				dth2 = dth2 + .10 * dth;
%  				ind = find(d < dth2);
%  			end
%  			
%  			% Use the background corrected intensity of the "close" normalization interfaces as the normalization factor.
%  			nf = norm_factor.MeanBackgroundCorrectedIntensity;
%  			nf = mean(nf(ind));
%  			
%  			bg_int = max(0, (int - mean_bg)) / nf;
%  		      	rel_int = int / mean(norm_factor.MeanRelativeIntensity(ind)); 
%  		      %end
%  		      	above_int = max(0, int - mean(norm_factor.MeanAbsoluteIntensity(ind)));
%  		        sc_int1 = 1 + (rel_int - max_roi) / (max_roi - min_roi);
%  		      else
%  		      	bg_int = max(0, (int - mean_bg)) / norm_factor;
%  		      	rel_int = int / norm_factor; % Normalize to the DV intensity.
%  		      %end
%  		      	above_int = max(0, int - norm_factor);
%  		        sc_int1 = 1 + (rel_int - max_roi) / (max_roi - min_roi);
%  		      end
		      	      
	      % If the image where intensities should be measured is empty, we are probably
	      % measuring the intensity of a point.
	      else
      		      int = cb_getpixvalue(fig);
		      int = int(channel);
		      rel_int = int ./ mean_roi;
		      sc_int1 = 1 + (rel_int - max_roi) / (max_roi - min_roi);
		      if isa(norm_factor, 'dip_measurement')
			% Compute distance between the interface under analysis and all normalization (DV) interfaces.
		      	dvcoords = [norm_factor.x' norm_factor.y'];
			thiscoords = repmat([sum(x)/2, sum(y)/2], [size(dvcoords, 1), 1]);
			d = dvcoords - thiscoords;
			d = sum(d .* d, 2);	
			d = sqrt(d);		
			
			% Find those interfaces closer than a certain distance, dth.
			ind = find(d < dth);
			dth2 = dth;
			
			% If no interfaces are close enough, expand the search area.
			while numel(ind) < intth				
				dth2 = dth2 + .10 * dth;
				ind = find(d < dth2); % find((d > 0) & (d < dth2)); Does not alter the results, but avoids using the interface itself in the case of the measurement of a DV interface.
				anidx = find(norm_factor.Length(ind) >= min_dv); % These two lines control that very short DV edges, which tend to be bright due to node intentsity, do not mess up the measurements. MAGIC NUMBER!!!
				ind = ind(anidx);
			end
			
			% Use the background corrected intensity of the "close" normalization interfaces as the normalization factor.
			nf = norm_factor.MeanBackgroundCorrectedIntensity;
			nf = mean(nf(ind));
			
			bg_int = max(0, (int - mean_bg)) / nf;
		      	rel_int = int / mean(norm_factor.MeanRelativeIntensity(ind)); 
		      %end
		      	above_int = max(0, int - mean(norm_factor.MeanAbsoluteIntensity(ind)));
		        sc_int1 = 1 + (rel_int - max_roi) / (max_roi - min_roi);
                        bganddvsubstracted = max(0, (int - mean_bg) - nf);              
		      else
		      	bg_int = max(0, (int - mean_bg)) / norm_factor;
		      	rel_int = int / norm_factor; % Normalize to the DV intensity.
		      %end
		      	above_int = max(0, int - norm_factor);
		        sc_int1 = 1 + (rel_int - max_roi) / (max_roi - min_roi);
                        bganddvsubstracted = max(0, (int - mean_bg) - norm_factor);              
		      end
	      end
	      % END INTENSITY
	else
	      int = nan;  
	      bg_int = nan;
	      rel_int = nan;
	      above_int = nan;
	      sc_int1 = nan;
	      mean_bg = nan;
              bganddvsubstracted = nan;       
	end
	
	xcoord(i) = sum(x) / 2;
	ycoord(i) = sum(y) / 2;
	edge_length(i) = l;
	edge_orientation(i) = val*180.0/pi;
	edge_meanintensity(i) = int;
	edge_relativeintensity(i) = rel_int;
	edge_scaledintensity1(i) = sc_int1;
	edge_bgintensity(i) = bg_int;
	edge_aboveintensity(i) = above_int / mean_bg;
        edge_bganddvsubstracted(i) = bganddvsubstracted;       
	waitbar((i)/(numel(edge_indeces)));
end

m = dip_measurement(edge_indeces, 'X', xcoord, 'Y', ycoord, 'Length', edge_length, 'Orientation', edge_orientation, 'MeanAbsoluteIntensity', edge_meanintensity, 'MeanRelativeIntensity', edge_relativeintensity, 'MeanScaledIntensity', edge_scaledintensity1, 'MeanBackgroundCorrectedIntensity', edge_bgintensity, 'MeanAboveDVIntensity', edge_aboveintensity, 'MeanBgAndDVSubstractedIntensity', edge_bganddvsubstracted);


% Compute the average intensity of all the interfaces with a certain orientation.

% Convert all angles to the range [0 90].
ind = find(edge_orientation > 90);
edge_orientation(ind) = 180 - edge_orientation(ind);

nbins = 90/bin_sz;
intensity_hist = nan .* ones(1, nbins);
for ii = 1 : nbins
	ind = find((edge_orientation >= ((ii - 1) * bin_sz)) & (edge_orientation < (ii * bin_sz)));
	
	if numel(ind) > 0
		intensity_hist(ii) = mean(edge_scaledintensity1(ind));
	end
end

%disp(intensity_hist);

close(wb);
