

% *********************************************************************************
function cb_paintunique(fig, uniquecoords, unique_sz, unique_color)

cb_eraseunique(fig);

ud = get(fig, 'UserData');
curslice = ud.curslice + 1;

if ~ isempty(uniquecoords)
	coords = uniquecoords(curslice, :);
	if sum(coords == [-1 -1]) ~= 2
		cb_drawpoint(fig, coords, unique_sz, unique_color, 'Unique');
	end
end
end