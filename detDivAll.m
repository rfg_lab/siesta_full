function [dividingCell]=detDivAll(trackDiv)
% output dividingCell represent the cell that's about to divide
% input track Div has 3 pieces of info

% measures change in area and circularity (not necessary)

distances=trackDiv(:,:,1);
areaChange=trackDiv(:,:,2);
circleChange=trackDiv(:,:,3);

dividingCell(:,:,1)=cell(size(trackDiv(:,:,1)));
dividingCell(:,:,2)=cell(size(trackDiv(:,:,2)));
dividingCell(:,:,3)=cell(size(trackDiv(:,:,3)));


% i represent the different cell segments

dividingCell(1,:,1)=distances(1,:);
dividingCell(1,:,2)=areaChange(1,:);
dividingCell(1,:,3)=circleChange(1,:);

for i=2:size(distances,1)
    
    %     j represent different time points
    for j=1:size(distances(i,:),2);
        if isempty(distances{i,j})
            continue
        end
        
        dist=distances{i,j};
        
        [maxVal,maxInd]=max(dist);
        [minVal,minInd]=min(dist);
        
        numEl=size(dist,1);
                
        minIndLow=round(minInd-(numEl/16));
        minIndHigh=round(minInd+(numEl/16));
        maxIndLow=round(maxInd-(numEl/16));
        maxIndHigh=round(maxInd+(numEl/16));
        
        halfShift=circshift(dist,round(numEl/2));
        
        if minIndLow<1
            otherMin=halfShift(numEl+minIndLow:numEl);
            otherMin=[otherMin;halfShift(1:minInd)];
            otherMin=[otherMin;halfShift(minInd+1:minIndHigh)];
        elseif minIndHigh>numEl
            otherMin=halfShift(round(minIndLow):numEl);
            otherMin=[otherMin;halfShift(1:round(minIndHigh-numEl))];
        else
            otherMin=halfShift(minIndLow:minIndHigh);
        end
        
        if maxIndLow<1
            otherMax=halfShift(numEl+maxIndLow:numEl);
            otherMax=[otherMax;halfShift(1:maxInd)];
            otherMax=[otherMax;halfShift(maxInd+1:maxIndHigh)];
        elseif maxIndHigh>numEl
            otherMax=halfShift(round(maxIndLow):numEl);
            otherMax=[otherMax;halfShift(1:round(maxIndHigh-numEl))];
        else
            otherMax=halfShift(maxIndLow:maxIndHigh);
        end
                
        cond2=otherMin<(maxVal*0.6);
        cond3=otherMax>(maxVal*0.7);
        % check conditions for dumbell shape
        if (minVal/maxVal)<0.4 && nnz(cond2) && nnz(cond3)
        %if (minVal/maxVal)<0.4 && (dist(int8(checkEl))/maxVal)<0.6 && (dist(int8(checkElMax))/maxVal>0.8)

            dividingCell(i,j,1)=distances(i,j);
        end
    end
end
        
% for i=2:size(areaChange,1)
%     %     j represent different time points
%     %       j starts at 2 because initial time point as no change
%     for j=2:size(areaChange(i,:),2);
%         % if no value for previous time point, go to next time point
%         if isempty(areaChange{i,j-1})
%             continue
%         end
% 
%         dividingCell{i,j,2}=areaChange{i,j}-areaChange{i,j-1};
%         
%     end
% end
% 
% for i=2:size(circleChange,1)
%     %     j represent different time points
%     %       j starts at 2 because initial time point has no change
%     for j=2:size(circleChange(i,:),2);
%         % if no value for previous time point, go to next time point
%         if isempty(circleChange{i,j-1})
%             continue
%         end
%         dividingCell{i,j,3}=circleChange{i,j}-circleChange{i,j-1};
%         
%     end
% end



end
        
        
        
%         [maxVal,maxInd]=max(dist);
%         [minVal,minInd]=min(dist);
%         
%         numEl=size(dist,1);
%         
%         % find index of other min
%         if minInd+(numEl/2)>numEl
%             %wrap around
%             checkEl=(minInd+(numEl/2))-numEl;
%         else
%             checkEl=minInd+(numEl/2);
%         end
%         
%         % min range index values may be decimals
%         minRangeLowInd=(checkEl-(numEl/4));
%         minRangeHighInd=(checkEl+(numEl/4));
%         
%         if minRangeLowInd<1
%            minRangeLowInd=minRangeLowInd+numEl;    
%         end
%         
%         if minRangeHighInd>numEl
%            minRangeHighInd=minRangeHighInd-numEl;    
%         end
%         
%         % contains range of values in other min
%         otherMin=(dist(int8(minRangeLowInd):int8(minRangeHighInd)));
%         
%         % find index of other max
%         if maxInd+(numEl/2)>numEl
%             %wrap around
%             checkElMax=(maxInd+(numEl/2))-numEl;
%         else
%             checkElMax=maxInd+(numEl/2);
%         end
%        
%         % max range index values may be decimals
%         maxRangeLowInd=(checkElMax-(numEl/4));
%         maxRangeHighInd=(checkElMax+(numEl/4));
%         
%         if maxRangeLowInd<1
%            maxRangeLowInd=maxRangeLowInd+numEl;    
%         end
%         
%         if maxRangeHighInd>numEl
%            maxRangeHighInd=maxRangeHighInd-numEl;       
%         end
%         
%         % contains range of values in other max
%         otherMax=(dist(int8(maxRangeLowInd):int8(maxRangeHighInd)));
% 
%         cond2=otherMin<(maxVal*0.5);
%         cond3=otherMax>(maxVal*0.8);
%         % check conditions for dumbell shape
%         if (minVal/maxVal)<0.4 && nnz(cond2) && nnz(cond3)        
%             dividingCell(i,j)=distances(i,j);
%         end
        
    
    
    
