% [lcoords, rcoords, mcoords] = cb_getcoords(fig)
function [lcoords, rcoords, mcoords] = cb_getcoords(fig)

lcoords = [];
rcoords = [];
mcoords = [];
 
udata = get(fig, 'UserData');

udata.ax = get(fig, 'CurrentAxes');
udata.oldAxesUnits = get(udata.ax,'Units');
udata.oldNumberTitle = udata.curslice;
set(udata.ax,'Units','pixels');
pos = round(get(udata.ax, 'CurrentPoint'));
pos = pos(1, 1:2);

if (numel(udata.imsize) > 2 && udata.imsize(3) > 1)
    pos(3) = udata.curslice;
else
    pos(3) = 0;
end

if ((~ strcmp(get(fig,'SelectionType'),'alt')) && (~ (strcmp(get(fig,'SelectionType'),'extend'))))
    % Left mouse button
    lcoords = pos;
    rcoords = [];
    mcoords = [];
elseif strcmp(get(fig,'SelectionType'),'alt')
    % Right mouse button
    lcoords = [];
    rcoords = pos;
    mcoords = [];
elseif strcmp(get(fig,'SelectionType'),'extend')
    % Middle mouse button.
    lcoords = [];
    rcoords = [];
    mcoords = pos;
end
