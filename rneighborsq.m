% neigbors = rneighborsq(id1, g, returnorder)
% [neighbors volumes] = rneighborsq(id1, g, 0)
% [neighbors distances volumes] = rneighborsq(id1, g, 1)
% [neighbors volumes slice_vol] = rneighborsq(id1, g, 0)
% [neighbors distances volumes slice_vol] = rneighborsq(id1, g, 1)
%
% Obtains the neighbors for an object in a labeled image. To do so, it expands
% the object of interest and returns as neighbors all the objects intersected
% by the expanded one.
%
% ID1 is the label of the object in the image.
% G is the labeled image.
% RETURNORDER is 0 for neighbors sorted by label (label 0 is considered as a potential neighbor), 1 for neighbors
% sorted by distance (between centroids, label 0 is not included in the list of neighbors).
%
% NEIGHBORS is a list with the IDs of all the objects in contact with ID1.
% VOLUMES is an array of volumes of the intersection with each one of the NEIGHBORS.
% DISTANCES is an array of distances between the center of ID1 and the center of
% each one of the NEIGHBORS.
% SLICE_VOL is a cell array where each cell contains size(g, 3) arrays of numbers. Element j in cell i
% indicates the volume of the intersection between cells id1 and i in slice j. This information is used
% by extract_topology to distinguish interface from node neighbors.
%
%  Rodrigo Fernandez-Gonzalez
%  fernanr1@mskcc.org
%  3/15/2007

function varargout = rneighborsq(id1, g, returnorder)

% id1 is a labeled image with the desired object marked with the same
% label as in g).
if (isa(id1, 'dip_image'))
	t = id1;
	id1 = unique(double(t(find(t))));
	t = t > 0;

% or id1 is a label.
else
	t = g == id1;
end

td = dilation(t, 3, 'elliptic'); % Increase to 4??
neighbors = double(g(td));

switch (returnorder)
	case 1
		mycenter = measure(g, [], 'Center', id1, 3);
		mycenter = mycenter.Center';
		tmp = neighbors(find(neighbors~=id1));
		neighbors = unique(tmp);
		if (~ isempty(neighbors))
			neighbors = nonzeros(neighbors);
		else
			neighbors = id1;	
		end
		
		m = measure(g, [], 'Center', neighbors, 3);
		m = m.Center';
		d = zeros(numel(neighbors), 1);
		for i = 1 : numel(neighbors);
			d(i) = norm(mycenter - m(i, :));
		end
		
		[l I] = sort(d);
		varargout{1} = neighbors(I);
		varargout{2} = d(I);
		
		varargout{3} = zeros(1, numel(varargout{1}));
		for ii = 1 : numel(varargout{1})
			varargout{3}(ii) = nnz(tmp == varargout{1}(ii));
		end

		varargout{4} = cell(1, numel(varargout{1}));		
		for ii = 1 : numel(varargout{1})
			varargout{4}{ii} = zeros(1, size(g, 3));
			for jj = 0:(size(g, 3) - 1)
				neighbors = nnz(double((g(:, :, jj) == varargout{1}(ii)) & td(:, :, jj)));
				varargout{4}{ii}(jj+1) = neighbors;
			end
		end

	otherwise
		tmp = neighbors(find(neighbors~=id1));
		% Neighbor ids.
		varargout{1} = sort(unique(tmp));
		
		if nargout > 1
			% Degree of total overlap with each neighbor (contact surface).
			varargout{2} = zeros(1, numel(varargout{1}));
			for ii = 1 : numel(varargout{1})
				varargout{2}(ii) = nnz(tmp == varargout{1}(ii));
			end
		
			% Degree of overlap per slice (contact surface).
			if nargout > 2
				varargout{3} = cell(1, numel(varargout{1}));		
				% For every cell ...
				for ii = 1 : numel(varargout{1})				
					varargout{3}{ii} = zeros(1, size(g, 3));
					
					% For every plane ...
					for jj = 0:(size(g, 3) - 1)
						neighbors = nnz(double((g(:, :, jj) == varargout{1}(ii)) & td(:, :, jj)));	
						varargout{3}{ii}(jj+1) = neighbors;
					end
				end
			end
		end
end
