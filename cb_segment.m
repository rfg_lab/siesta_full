% *********************************************************************************
% only_seeds == 1 calculates the position of the seeds for the watershed.
% only_seeds == 0 runs the entire segmentation.
% only seeds set to an array of [x y (z)] coordinates provides the seeds to run the watershed.
%
% propagate_seeds == 1, calculates the "optic flow" and applies it to the seeds in this time point to calculate the seeds of the next time point.
% before it used the centers of the objects segmented in the current point as the seeds of the next one.
function [segm geom ud2] = cb_segment(fig, only_seeds, propagate_seeds)

    if nargin < 3
        propagate_seeds = 0;
    end        

    if nargin < 2
        only_seeds = 0;
    end
    
    ud = get(fig, 'UserData');
    ud2 = ud;

    roi = [];
    slice_list = (4:7);
    channel = 1;
    res = [.33 .33 1];
    tiny_cells = 0;
    min_dist = 1.2;
    dil_thresh = 2; % How much to dilate after adaptive threshold: 7;
    C = 0; % 0.08 for fixed, high-constrast images.
    p2a_th = 1.4;
    min_p2a = 1.6; 
    max_p2a = 2.15;
    min_seed_bit_sz = 4;
    gauss_factor = 0.65;

    subs_factor_xy = 1;
    clean_flag = 0;
    clean_seeds = 1;
    weighted = 0;
    write_flag = 0;
    filename = '';
    win_size = 30;
    debug_flag = 1;

    rot_ang = 0;
    geom = [];
    geometry_plane = 1;
    sum_proj = 1;

    default_flow_window = [64 64];
    size_factor_limit = 0.15; %0.75 works well mean(sizes) +- size_factor_limit*std(sizes) are the top and bottom limits the actual limit.

    min_dist_seed_polygon = 3; % This should be a parameter, but for now we will make it three pixels.

    [slice_list res rot_ang is2D min_dist gauss_factor closing_param divSettings] = dialog_segmentation(ud, only_seeds);
    
    % 3D SEGMENTATION CODE NOT READY YET.
    if ~ is2D
        segm = [];
        geom = [];
        fprintf('\n\nKeep dreaming about 3D segmentation in your next SIESTA!!\n\n');
        return;
    end        

    % Color images cannot be processed.
    if isa (ud2.slices, 'dip_image_array')
        segm = [];
        geom = [];
        fprintf('\n\nThis is a color image. Switch to one of the grayscale channels.\n\n');
        return;
    end

    % If the segmentation was not cancelled, store the parameters.
    if slice_list(1) ~= -1
        ud2.rres = res;
        ud2.parammindist = min_dist;
        ud2.paramgaussfactor = gauss_factor;
        ud2.paramclosing = closing_param;
    end        

    % closing_param is used to determine the window size, and not to do
    % any closing.
    default_flow_window = [closing_param closing_param];
    win_size = closing_param;

    % Segment single slice.
    if slice_list(1) >= 0 & numel(slice_list) == 1
	% Segment a single slice.
	wb = waitbar(0, 'Segmenting image ...');
	a = ud.slices(:, :, slice_list);
	tmp = dip_image(zeros(size(a, 2), size(a, 1), 4));
	tmp(:, :, 0) = a;
	tmp(:, :, 1) = a;
	tmp(:, :, 2) = a;
	tmp(:, :, 3) = a;
	a = tmp; clear tmp;
	clean_flag = 0; % Not necessary to clean output if input was a single plane (it will not make a difference).
	the_slice_list = (0:3);
	
	% This bit could be outside the loop, but thus, we keep the code
	% compact and easy to copy and paste.
	d = dir('./roi*');
        
	% There are ROIs defined.
	if (numel(d) > 0)
            vars = load(d(1).name);
            if isa(vars.roi, 'dip_image')
                roi = vars.roi;
            else
                roi = [];
            end
            
	elseif (~isempty(ud.rpolygons)) && (~isempty(ud.rpolygons{1, :, slice_list+1}))
            % Creates an roi based on the bounding box of a polygon. But the roi-only segmentation still uses the entire image ...
            %poly = ud.rpolygons{1, :, slice_list+1};
            roi = trajectories2mask(ud.rpolygons(1, :, ud.curslice+1), ud.imsize(1:2), 1, 1);
            
            im_roi=repmat(roi, [1 1 size(a, 3)]);
            roi = im_roi;
            
            % This takes care of annotation files were all
            % polygons were deleted, but the space still
            % remains in the annotation file. You just want to multiply by the roi if it is not completely empty.
            if nnz(double(roi)) > 0
                a = a.*roi;
            else
                roi = [];
            end
            
        else
            roi = []; 
 end
 
 % If we are trying to expand seeds, make sure we only pass the correct ones.       
 if numel(only_seeds) > 1 && ndims(only_seeds) > 2
     only_seeds = only_seeds(:, [1 2 3], ud.curslice+1);
 end                
 
 rotated_im = rotation(a, rot_ang .* pi/180., 3, 'zoh');
 rotated_im = round(stretch(rotated_im)); 
 [segm seed_fidu] = segm_fixed_fn2(dip_image(rotated_im, 'uint8'), roi, the_slice_list, channel, res, tiny_cells, min_dist, dil_thresh, C, subs_factor_xy, p2a_th, min_p2a, max_p2a, min_seed_bit_sz, gauss_factor, clean_flag, clean_seeds, weighted, write_flag, filename, win_size, debug_flag, only_seeds);
 %save tmp1 segm seed_fidu only_seeds

 if only_seeds == 1
     geom.nodes = seed_fidu;
     for j = 1:size(geom.nodes, 1)
         [ud2.rfiducials ud2.rnfiducials] = cb_storecurrentpoint([geom.nodes(j, 2) geom.nodes(j, 1) ud.curslice], ud2.rfiducials, ud2.rnfiducials);
     end
     
     waitbar(1, wb, 'Done!');        
     close(wb);
     cd(ud.rcurrdir);
     save seed_data seed_fidu
 elseif only_seeds == 0 | (ud.rNODESANDEDGES)
     
     waitbar(.5, wb, 'Extracting geometry ...');
     
     if geometry_plane >= 0
         [nodes edges nodecellmap] = extract_geometry(segm(:, :, geometry_plane));
         
         geom.nodes = nodes;
         geom.edges = edges;
         geom.nodecellmap = nodecellmap;
     end
     
     for j = 1:size(geom.nodes, 1)
         [ud2.rfiducials ud2.rnfiducials] = cb_storecurrentpoint([geom.nodes(j, 2) geom.nodes(j, 1) ud.curslice], ud2.rfiducials, ud2.rnfiducials);
     end
     
     waitbar(1, wb, 'Done!');
     
     close(wb);
     
     cd(ud.rcurrdir);
     save segmentation_data segm geom ud
     %writeim(ud.slices, 'tmp2', 'ICS', 'no'); %RODRIGO
     %writeim(ud.imagedata, 'tmp3', 'ICS', 'no'); %RODRIGO
     rfig2D(geom, 1, ud.imagedata, 0, 0, 0);
 else % if only_seeds was an array with the seeds.
     m = measure(segm);
     ids = m.id;
     
     % If necessary, propagate seeds.
     if propagate_seeds
         prev_seeds = ud2.rfiducials(:, :, slice_list+1); % Seeds in the current time point.
         ind = find(prev_seeds(:, 3) == slice_list); % Remove those where the Z coordinate is -1.
         prev_seeds = prev_seeds(ind, :);
         next_seeds = seed_fidu(:, [2 1 3]); % Centroids of segmented objects. These are (thank god!) ordered as the initial seeds were.
         
         % These array will contain the seeds that will be shifted: those centroids that do belong to cells too large or too small and the initial seed if the cell has an abnormal size (segmentation errors).
         final_seeds = zeros(size(prev_seeds));
         
         % m.Size contains the sizes of cells not in contact with the border of the image (deleted during the segmentation process but which still do have a seed). 
         sizes = zeros(1, size(final_seeds, 1));
         sizes(ids) = m.Size;
         
         % If the cell is not in the periphery, and the size is not too small and not too large, then shift the centroid of the cell. Otherwise, just shift the previous seed.
         % Find cells at the periphery (adjacent to the background, which has label 0 in segm).
         peripheral_cells = rneighborsq(0, segm, 0);
         
         % These are the cells that are too big or too small or at the periphery
         ind = unique(cat(2, find((sizes > median(sizes) + size_factor_limit*median(sizes)) | (sizes <= 0)), peripheral_cells));
         final_seeds(ind, 1:2) = prev_seeds(ind, 1:2);
         fprintf('%d seeds shifted using optic flow only (missegmented cells and peripheral cells).\n', numel(ind));
         
         %ind = find((sizes <= median(sizes) + size_factor_limit*median(sizes)) & (sizes > 0));
         ind = setdiff(ids, ind); % These are all the other cells, where one can shift the centroid. The order of parameters is important here: the fuller list needs to come first.
         final_seeds(ind, 1:2) = next_seeds(ind, 1:2);
         fprintf('%d seeds shifted using segmentation results and optic flow.\n', numel(ind));

         % Calculate the flow field and interpolate at the positions of the points to be shifted.
         [Xflow Yflow] = rflow(squeeze(ud.slices(:, :, slice_list)), squeeze(ud.slices(:, :, slice_list+1)), final_seeds(:, 1:2), default_flow_window, 0, 0);
         
         % Apply the flow.
         final_seeds(:, 1) = final_seeds(:, 1) + Xflow;
         final_seeds(:, 2) = final_seeds(:, 2) + Yflow;
         final_seeds(:, 3) = slice_list + 1;

         % Clip at the edges.
         ind = find(final_seeds(:, 1) < 0);
         final_seeds(ind, 1) = 0;
         ind = find(final_seeds(:, 2) < 0);
         final_seeds(ind, 2) = 0;
         ind = find(round(final_seeds(:, 1)) >= size(ud2.slices, 1));  % If size(ud2.slices, 1) is 100 and final_seeds(islice, 1) == 99.7, this number will be rounded up in segm_fixed_fn2 and generate an error. Therefore, round up in the check. This is not necessary in the previous to conditions.
         final_seeds(ind, 1) = size(ud2.slices, 1)-1;
         ind = find(round(final_seeds(:, 2)) >= size(ud2.slices, 2));
         final_seeds(ind, 2) = size(ud2.slices, 2)-1;
         
         % Store the shifted seeds (by default we are shifting forward).
         ud2.rfiducials(1:size(final_seeds, 1), 1:3, slice_list+2) = final_seeds;
         ud2.rnfiducials(slice_list+2) = size(final_seeds, 1);
     end
     
     % Go through the segmented objects ...
     for iobj = 1:numel(ids)
         % Create a binary image containing the object.
         tmp = double(segm(:, :, 0) == ids(iobj));
         % Find a polygon outlining it.
         p = bwboundaries(tmp);
         p = p{1}; % The output of bwboundaries contains one cell per object present in the image.
         p = p(:, [2 1]) - 1; % For bwboundaries, the smallest coordinates are [1,1], while in rfig2, [0,0] is a valid point for a fiducial.
         p = round(p .* subs_factor_xy);
         ud2.rpoly = [];
         
         % And store the polygon.
         if ~ isempty(p)
             if ~ isempty(p)
                 ud2.rpoly((end+1):(end+size(p,1)), :) = p; 
             else
                 ud2.rpoly(end+1, :) = ud.rpoly(1, :);
             end                                
             
             % Store in in the first empty spot.
             myind = 1;
             for is = 1:size(ud2.rpolygons, 1)
                 if (isempty(ud2.rpolygons{is, 1, ud2.curslice+1}))
                     ud2.rpolygons{is, 1, ud2.curslice + 1} = ud2.rpoly;
                     break;
                 end
                 myind = myind + 1;
             end
             
             % If there were no empty spots, store at the end.
             if (myind > size(ud2.rpolygons, 1))
                 ud2.rpolygons{end+1, 1, ud2.curslice + 1} = ud2.rpoly;
             end
             
             ud2.rpoly = [];
         end
     end
     
     % If necessary, extract geometry here.
     if (ud.rSAVE_GEOM)
         [nodes edges nodecellmap] = extract_geometry(segm(:, :, geometry_plane));
         cd(ud.rcurrdir);
         index = ud.curslice;
         save(cat(2, 'geometry_data', rnum2str(index, 4)), 'nodes', 'edges', 'nodecellmap'); 
     end
     
     
     waitbar(1, wb, 'Done!');        
     close(wb);                
 end   
 
 % Segment more than one slice.
    elseif slice_list(1) >= 0 

	if is2D
            % Sequential 2D segmentation here.
            wb = waitbar(0, 'If you are seeing this your system is SLOW!!! Just wait then ...');
            only_seeds_flag = only_seeds;              
            
            for islice = 1:numel(slice_list)
                wb = waitbar((islice-1)/numel(slice_list), wb, cat(2, 'Segmenting image ', cat(2, num2str(islice), cat(2, '/', num2str(numel(slice_list))))));
                index = slice_list(islice);
		
                a = ud.slices(:, :, index);
                tmp = dip_image(zeros(size(a, 2), size(a, 1), 4));
                tmp(:, :, 0) = a;
                tmp(:, :, 1) = a;
                tmp(:, :, 2) = a;
                tmp(:, :, 3) = a;
                a = tmp; clear tmp;
                clean_flag = 0; % Not necessary to clean output if input was a single plane (it will not make a difference).
                the_slice_list = (0:3);
		
                % This bit could be outside the loop, but thus, we keep the code
                % compact and easy to copy and paste.
                d = dir('./roi*');

                % There are ROIs defined.
                if (numel(d) > 0)
                    vars = load(d(1).name);
                    if isa(vars.roi, 'dip_image')
                        roi = vars.roi;
                    else
                        roi = [];
                    end
                    
                elseif (~isempty(ud.rpolygons)) && (~isempty(ud.rpolygons{1, :, index+1}))
                    % Creates an roi based on the bounding box of a polygon. But the roi-only segmentation still uses the entire image ...
                    poly = ud.rpolygons{1, :, index+1};
                    roi = trajectories2mask({poly}, ud.imsize(1:2), 1, 1);

                    im_roi=repmat(roi, [1 1 size(a, 3)]);
                    roi = im_roi;
                    
                    % This takes care of annotation files were all
                    % polygons were deleted, but the space still
                    % remains in the annotation file. You just want to multiply by the roi if it is not completely empty.
                    if nnz(double(roi)) > 0
                        a = a.*roi;
                    else
                        roi = [];
                    end

                else
                    roi = []; 
                end
            %only track div when it is turned on
            numSeedsBefore=ud2.rnfiducials(index+1);
            
            if divSettings(9)==1
                % assume ud2 has NOT been segmented
                %automateAll should reurn indices in
                %rfiducials cooresponding to the new
                %seeds
                %theNewSeedIndices
                if ~isempty(ud2.rpolygons)
                    %if trackedPolygon is empty, start new
                    %tracking
                    %                                     if
                    %
                    %                                     !!!!!!!!! was working previously but
                    %                                     not anymore :(
                    if isempty(ud2.trackedPolygon)||nnz(not(cellfun('isempty',ud2.trackedPolygon(:,:,:))))==0
                        ud2.trackedPolygon=ud2.rpolygons;
                    else
                        %in this case, trackedPolygon has
                        %stuff in it; take latest
                        %ud2.rpolygons and overlay with
                        %latest in trackedPlygon
                        
                        % finds all nonempty ud.rpolgyon
                        % TIME points, (mwt=last time point
                        % in ud.rpolygons)
                        mwt=find((cellfun('isempty',ud2.rpolygons(1,:,:))==0),1,'last');
                        
                        % delete trackedPolygon points at
                        % mwt and past it
                        ud2.trackedPolygon(:,:,mwt:end,:)=[];
                        
                        % find the centroids of cells in
                        % next time point
                        allCent=findAllCentroids(ud2.rpolygons(:,:,mwt));
                        %                                         if mwt-1<size(ud2.trackedPolygon(:,:,:),3)
                        for i=1:size(ud2.trackedPolygon(:,:,mwt-1),1);
                            % go through each cell in
                            % ud2.rpolygons (k)
                            % or maybe find(~(cellfun('isempty',ud.trackedPolygon(:,:,mwt-1))),1,'last')
                            % but don't use because
                            % overwirte deleted polygons
                            
                            if isempty(ud2.trackedPolygon{i,:,mwt-1})
                                continue;
                            end
                            
                            %check only seeds close to the
                            %current region
                            %                                             if mwt-1<size(ud2.trackedPolygon(:,:,:,1),3)
                            proxArea=ud2.trackedPolygon{i,:,mwt-1,1};
                            %allCent is centers of polygons
                            %in rpolygons
                            closeSeeds=inpolygon(allCent(:,1),allCent(:,2),proxArea(:,1),proxArea(:,2));
                            indCloseSeeds=find(closeSeeds);
                            
                            if size(indCloseSeeds,1)==1
                                [Areas,Area1,Area2]=polytest(ud2.trackedPolygon{i,:,mwt-1,1},ud2.rpolygons{indCloseSeeds,:,mwt});
                                A1A2Diff=abs(Area2-Area1)/Area1;
                                if (Areas/Area2) >=0.6 && (Areas/Area1) >=0.6 %%&& A1A2Diff <= 0.5
                                    ud2.trackedPolygon(i,:,mwt,1)=ud2.rpolygons(indCloseSeeds,:,mwt);
                                    % Michael Quick
                                    %(:,:,:,2) are area
                                    %measures
                                    ud2.trackedPolygon{i,:,mwt,2}=nnz(poly2mask(ud2.trackedPolygon{i,:,mwt,1}(:,1),ud2.trackedPolygon{i,:,mwt,1}(:,2),1000,1000));
                                    
                                    Geom=polygeom(ud2.trackedPolygon{i,:,mwt,1}(:,1),ud2.trackedPolygon{i,:,mwt,1}(:,2));
                                    %(:,:,:,3) are
                                    %circularity
                                    ud2.trackedPolygon{i,:,mwt,3}=(Geom(4)*Geom(4))/(4*pi*Geom(1));
                                    %(:,:,:,4) are
                                    %distances
                                    ud2.trackedPolygon{i,:,mwt,4}=sqrt(((ud2.trackedPolygon{i,:,mwt,1}(:,1)-Geom(2)).^2)+((ud2.trackedPolygon{i,:,mwt,1}(:,2)-Geom(3)).^2));
                                    %(:,:,:,5) are dumbbell
                                    %checks
                                    ud2.trackedPolygon{i,:,mwt,5}=isDumbbell(ud2.trackedPolygon{i,:,mwt,4},divSettings(6));
                                    %bound Ratios contain
                                    %(1) area ratio between min
                                    %bound box and polygon
                                    %and (2) area ratio
                                    %between convex hull
                                    %and polygon
                                    %lengthRatio contains
                                    %ratio between max axis
                                    %and min axis of
                                    %polygon
                                    [boundRatios,lengthRatio]=boxRatios(ud2.trackedPolygon{i,:,mwt,1});
                                    ud2.trackedPolygon{i,:,mwt,6}=boundRatios(1);
                                    ud2.trackedPolygon{i,:,mwt,7}=lengthRatio;
                                end
                            elseif size(indCloseSeeds,1)==0
                                continue;
                            elseif size(indCloseSeeds,1)>1
                                if ~isempty(ud2.trackDiv)
                                    for x=1:size(ud2.trackDiv,2)
                                        % check that the
                                        % numbers match and
                                        % polygons have not
                                        % been tracked
                                        if ismatrix(ud2.trackDiv)
                                            ud2=findDivPoly(ud2,mwt,x);
                                            
                                        elseif mwt==ud2.trackDiv(1,x) && ud2.trackDiv(1,x,2)==0
                                            ud2=findDivPoly(ud2,mwt,x,divSettings);
                                        end
                                    end
                                end
                            end
                            %                                             end
                        end
                        %                                         end
                        
                        
                        %michael quick check for division
                        %here
                        %                                         if mwt<size(ud2.trackedPolygon(:,:,:),3)
                        if mwt>size((ud2.trackedPolygon),3)
                                            
                                            display('THERE ARE NO CELLS TRACKED!');
                                            display('please run reseed based on tracked polygon to check division tracking');
                                            
                        else
                            for q=1:size(ud2.trackedPolygon(:,:,mwt,5),1)
                                if isempty(ud2.trackedPolygon{q,:,mwt,1})||isempty(ud2.trackedPolygon{q,:,mwt,2}) 
                                    continue;%skip empty polygons
                                end
                                % logistic regression here
                                % get size ratio, min circ,
                                % dumbbell ratio
                                area=squeeze(cell2mat(ud2.trackedPolygon(q,:,1:mwt,2)));
                                numTimeCond=length(area)>divSettings(4);
                                meanAreaCurr=mean(cell2mat(ud2.trackedPolygon(:,:,mwt,2)));
                                minAreaCond=area(end)>(meanAreaCurr*divSettings(8));
                                area=area(end)/min(area);
                                circle=squeeze(cell2mat(ud2.trackedPolygon(q,:,1:mwt,3)));
                                circle=min(circle);
                                dist=ud2.trackedPolygon{q,:,mwt,4};
                                freqInfo=findFFT2Hz(dist);
                                [d1,d2]=dumbellCheck(dist);
                                input=[1,area,circle,d1,d2,freqInfo'];
                                
                                %learnt parameters
                                dumbbellCond=d1<divSettings(6);
                                ud2.trackedPolygon{q,:,mwt,8}=sigmoid(ud.thetaLearn*input');
                                if round(sigmoid(ud.thetaLearn*input')) && numTimeCond && dumbbellCond && minAreaCond  %learning algorithm says it is dividing
                                    
                                    %if already
                                    %split
                                    %once,
                                    %don't
                                    %split
                                    %again
                                    contAgain=0;
                                    if (~isempty(ud2.trackDiv))
                                        indDiv=find(ud2.trackDiv(1,:,1)==mwt);
                                        if indDiv
                                            polygonOfInt=ud2.trackedPolygon{q,:,mwt,1};
                                            ind=inpolygon(ud2.rfiducials(:,1,mwt),ud2.rfiducials(:,2,mwt),polygonOfInt(:,1),polygonOfInt(:,2));
                                            row=find(ind);
                                            for z=1:size(row)
                                                if row(z)==ud2.trackDiv(2,indDiv,1)
                                                    contAgain=1;
                                                    break;
                                                end
                                            end
                                            
                                            if contAgain==1
                                                continue;
                                            end
                                        end
                                    end
                                    
                                    
                                    %divide
                                    %cell here
                                    %split
                                    %ud2.trackedPolygon(q,:,mwt,1)
                                    
                                    %splitFids2
                                    %splits
                                    %based on
                                    %centroid
                                    [newFids, ud2.trackDiv]=splitFidsLongAxis(ud2.trackedPolygon{q,:,mwt,1},ud2.rfiducials,mwt,ud2.trackDiv,divSettings(5));
                                    % [newFids, ud2.trackDiv]=splitFidsFitEllipse(ud2.trackedPolygon{q,:,mwt,1},ud2.rfiducials,mwt,ud2.trackDiv,divSettings(5));
                                    ud2.rfiducials=newFids;
                                    %                                                                 mgradient=1;
                                    display('!!!!!!!!!A cell has divided!!!!!!!!!');
                                    
                                    % go through each time slice (ii) of the newFids
                                    for ii = 1:size(newFids, 3)
                                        ind = find(newFids(:, 1, ii) == -1);
                                        
                                        if ~ isempty(ind)
                                            ud2.rnfiducials(ii) = ind(1)-1;
                                        else
                                            ud2.rnfiducials(ii) = size(newFids, 1);
                                        end
                                    end
                                    
                                end
                            end                         
                            
                        end
                        
                    end    

                    %         end
                    display('Analyzed Cell Divisions');
                end
            end %if divSettings(9)==1
            numSeedsAfter=ud2.rnfiducials(index+1);

                
                
                if numel(only_seeds_flag) > 1 && ndims(only_seeds_flag) > 2
                    only_seeds = only_seeds_flag(:, [1 2 3], index+1);
                    if numSeedsBefore < numSeedsAfter
                        only_seeds = ud2.rfiducials(:, [1 2 3], index+1);
                    end
                end
                
                rotated_im = rotation(a, rot_ang .* pi/180., 3, 'zoh');
                rotated_im = round(stretch(rotated_im));	
                
               if divSettings(7)==1 % if gradient option is selected in the UI
                    [segmGrad, seed_fiduGrad] = segm_fixed_fn2(dip_image(rotated_im, 'uint8'), roi, the_slice_list, channel, res, tiny_cells, min_dist, dil_thresh, C, subs_factor_xy, p2a_th, min_p2a, max_p2a, min_seed_bit_sz, gauss_factor, clean_flag, clean_seeds, weighted, write_flag, filename, win_size, debug_flag, only_seeds, 1, divSettings(7)); % gradient based segmentation.
                    [segm, seed_fidu] = segm_fixed_fn2(dip_image(rotated_im, 'uint8'), roi, the_slice_list, channel, res, tiny_cells, min_dist, dil_thresh, C, subs_factor_xy, p2a_th, min_p2a, max_p2a, min_seed_bit_sz, gauss_factor, clean_flag, clean_seeds, weighted, write_flag, filename, win_size, debug_flag, only_seeds, 1, 0); % intensity based segmentation
                else
                    [segm, seed_fidu] = segm_fixed_fn2(dip_image(rotated_im, 'uint8'), roi, the_slice_list, channel, res, tiny_cells, min_dist, dil_thresh, C, subs_factor_xy, p2a_th, min_p2a, max_p2a, min_seed_bit_sz, gauss_factor, clean_flag, clean_seeds, weighted, write_flag, filename, win_size, debug_flag, only_seeds, 1, 0);
                end
                %ave(cat(2,'file',num2str(index)), 'segm', 'seed_fidu', 'only_seeds');
                
                % If you are only finding the seeds, store them.
                if only_seeds == 1
                    geom.nodes = seed_fidu;
                    %                                  for j = 1:size(geom.nodes, 1)
                    %                                          [ud2.rfiducials ud2.rnfiducials] = cb_storecurrentpoint([geom.nodes(j, 2) geom.nodes(j, 1) index], ud2.rfiducials, ud2.rnfiducials);
                    %                                  end
                    
                    %waitbar(1, wb, 'Done!');        
                    %close(wb);
                    %cd(ud.rcurrdir);
                    %save seed_data seed_fidu
                    
                    
                    % If you are finding the seeds and expanding them ... (see the "else" for "If you are expanding seeds provided by the user").
                elseif only_seeds == 0                        			
                    % Now extract the geometry.
                    wb = waitbar((2*islice-1)/(2.*numel(slice_list)), wb, cat(2, 'Extracting geometry from image ', cat(2, num2str(islice), cat(2, '/', num2str(numel(slice_list))))));
                    
                    if geometry_plane >= 0
                        [nodes edges nodecellmap] = extract_geometry(segm(:, :, geometry_plane));
                        
                        geom.nodes = nodes;
                        geom.edges = edges;
                        geom.nodecellmap = nodecellmap;
                    end
                    
                    cd(ud.rcurrdir);
                    save(cat(2, 'segmentation_data', rnum2str(index, 4)), 'segm', 'geom', 'ud');
                    
                    %rfig2D(geom, 1, ud.slices(:, :, index), 0, 0, 0);
                    
                    %If you are expanding seeds provided by the user
                else % if only_seeds was an array with the seeds.   
                     %m = measure(segm);
                     %ids = m.id;
                     %sizes = m.Size;
                    ids = nonzeros(unique(uint16(segm)));
                    
                    % If necessary, propagate seeds.
                    if propagate_seeds && index <= (size(ud2.slices, 3)-2)
                        prev_seeds = ud2.rfiducials(:, :, index+1); % Seeds in the current time point. % MICHAEL's TRACKING CODE HERE.
                        ind = find(prev_seeds(:, 3) > -1);
                        % Remove those where the Z coordinate is -1.
                        
                        missing_seeds = setdiff(ind, ids); % The segmentation removes some cells on the edges. Therefore, ind will have as many or more elements than m.Size. We now find those elements.
                        
                        % And add them to the list of fiducials that need to be moved (without a centroid).
                        for imissing = 1:numel(missing_seeds)
                            ids(end+1) = missing_seeds(imissing);
                            %sizes(end+1) = 0;
                        end
                        
                        [ids sorted_index] = sort(ids);
                        %sizes = sizes(sorted_index);
                        
                        prev_seeds = prev_seeds(ind, :);
                        next_seeds = seed_fidu(:, [2 1 3]); % Centroids of segmented objects. These are (thank god!) ordered as the initial seeds were.
                        
                        % These array will contain the seeds that will be shifted: those centroids that do belong to cells too large or too small and the initial seed if the cell has an abnormal size (segmentation errors).
                        final_seeds = zeros(size(prev_seeds));
                        
                        % If the cell is not in the periphery, and the size is not too small and not too large, then shift the centroid of the cell. Otherwise, just shift the previous seed.
                        % Find cells at the periphery (adjacent to the background, which has label 0 in segm).
                        peripheral_cells = rneighborsq(0, segm, 0);
                        
                        % These are the cells that are too big or too small or at the periphery
                        %ind_messy = unique(cat(2, find((sizes > median(sizes) + size_factor_limit*median(sizes)) | (sizes <= 0)), peripheral_cells));
                        ind = 1:numel(ids);
                        final_seeds(ind, 1:2) = prev_seeds(ind, 1:2);
                        %fprintf('%d seeds shifted using optic flow only (missegmented cells and peripheral cells).\n', numel(ind_messy));
                        %ind_oddsizecells = find((sizes > median(sizes) + size_factor_limit*median(sizes)) | (sizes <= 0)); % odd size cells.
                        
                        %ind = find((sizes <= median(sizes) + size_factor_limit*median(sizes)) & (sizes > 0));
                        %ind = setdiff(ids, ind_messy); % These are all the other cells, where one can shift the centroid. The order of parameters is important here: the fuller list needs to come first.
                        %final_seeds(ind, 1:2) = next_seeds(ind, 1:2);
                        %fprintf('%d seeds shifted using segmentation results and optic flow.\n', numel(ind));

                        % Calculate next index. This allows moving forward or backward in time.
                        if islice < numel(slice_list)
                            next_ind = slice_list(islice+1);
                        elseif slice_list(islice) > slice_list(islice-1)
                            next_ind = slice_list(islice)+1;
                        else
                            next_ind = slice_list(islice)-1;
                        end

                        % Calculate the flow field and interpolate at the positions of the points to be shifted.
                        [Xflow Yflow] = rflow(squeeze(ud.slices(:, :, index)), squeeze(ud.slices(:, :, next_ind)), final_seeds(:, 1:2), default_flow_window, 0, 0);
                        
                        % Apply the flow. COMMENT OUT THESE THREE
                        % LINES TO SHUT DOWN THE OPTIC FLOW.
                        final_seeds(:, 1) = final_seeds(:, 1) + Xflow;
                        final_seeds(:, 2) = final_seeds(:, 2) + Yflow;
                        final_seeds(:, 3) = next_ind;
                        
                        % Clip at the edges.
                        ind = find(final_seeds(:, 1) < 0);
                        final_seeds(ind, 1) = 0;
                        ind = find(final_seeds(:, 2) < 0);
                        final_seeds(ind, 2) = 0;
                        ind = find(round(final_seeds(:, 1)) >= size(ud2.slices, 1)); % If size(ud2.slices, 1) is 100 and final_seeds(islice, 1) == 99.7, this number will be rounded up in segm_fixed_fn2 and generate an error. Therefore, round up in the check. This is not necessary in the previous to conditions.
                        final_seeds(ind, 1) = size(ud2.slices, 1)-1;
                        ind = find((final_seeds(:, 2)) >= size(ud2.slices, 2));
                        final_seeds(ind, 2) = size(ud2.slices, 2)-1;
                        
                        % Store the shifted seeds. Allows processing forward or backward in time.
                        %only_seeds_flag(1:size(final_seeds, 1), 1:3, index+2) = final_seeds;
                        %ud2.rfiducials(1:size(final_seeds, 1), 1:3, index+2) = final_seeds;
                        %ud2.rnfiducials(index+2) = size(final_seeds, 1);                                        
                        only_seeds_flag(1:size(final_seeds, 1), 1:3, next_ind+1) = final_seeds;
                        ud2.rfiducials(1:size(final_seeds, 1), 1:3, next_ind+1) = final_seeds;
                        ud2.rnfiducials(next_ind+1) = size(final_seeds, 1);
                        
                    end
                    
                    daughter=[];
                    if ~isempty(ud2.trackDiv)
                        divColumn=find(ud2.trackDiv(1,:,2)==0);
                        %if there is a division event
                        if divColumn
                            for z=1:size(divColumn)
                                daughter=[ud2.trackDiv(3,divColumn(z),1),daughter];
                                daughter=[ud2.trackDiv(4,divColumn(z),1),daughter];
                            end
                            %daughter contains seed id's of
                            %daughter cells
                        end
                    end

                    
                    % Go through the segmented objects ...
                    for iobj = 1:numel(ids)                                        
                        % Create a binary image containing the object.
                        if nnz(iobj==daughter)
                            tmp=double(segm(:,:,0)==ids(iobj));
                        else
                            tmp = double(segm(:, :, 0) == ids(iobj));
                        end
                        
                        
                        if ~ nnz(tmp) continue; end % This accounts for deleted objects (at the edges).
                                                    % Find a polygon outlining it.
                        p = bwboundaries(tmp);
                        
                        p = p{1}; % The output of bwboundaries contains one cell per object present in the image.
                        p = p(:, [2 1]) - 1; % For bwboundaries, the smallest coordinates are [1,1], while in rfig2, [0,0] is a valid point for a fiducial.
                        
                        % And store the polygon.
                        if ~ isempty(p)
                            if ~ isempty(p)
                                ud2.rpoly((end+1):(end+size(p,1)), :) = p; 
                            else
                                ud2.rpoly(end+1, :) = ud.rpoly(1, :);
                            end                                
                            
                            % Store in in the first empty spot.
                            myind = 1;
                            for is = 1:size(ud2.rpolygons, 1)
                                if (isempty(ud2.rpolygons{is, 1, index+1}))
                                    ud2.rpolygons{is, 1, index + 1} = ud2.rpoly;
                                    break;
                                end
                                myind = myind + 1;
                            end
                            
                            % If there were no empty spots, store at the end.
                            if (myind > size(ud2.rpolygons, 1))
                                ud2.rpolygons{end+1, 1, index + 1} = ud2.rpoly;
                            end
                            
                            % If the corresponding seed is too close or outside, the seed should be shifted (unless this object is too large or too small). I think this is redundant with everything above and you could probably save measuring the cells and just use polycenter to calculate area and polygon and make everything much, much faster ... This may require an arbitrary size threshold for the cells, but that may be OK and compensate for the speed gain. Or perhaps one should not move to the centroid, but away from the edges?
                            % COMMENT THIS IF BLOCK OUT TO TURN OFF SHIFTING SEEDS TO THE CELL CENTROID.
                            if propagate_seeds
                                [thearea cx cy] = polycenter(ud2.rpoly(:, 1), ud2.rpoly(:, 2)); % Is polycenter faster than polyarea? If so, switch them.
                                if thearea < 10000 && thearea > 50
                                    dist_seed_polygon = p_poly_dist(ud2.rfiducials(iobj, 1, index+1), ud2.rfiducials(iobj, 2, index+1), ud2.rpoly(:, 1), ud2.rpoly(:, 2));
                                    if dist_seed_polygon > - min_dist_seed_polygon % MICHAEL, CHECK THIS OUT.
                                        ud2.rfiducials(iobj, 1:2, next_ind+1) = [cx cy]; % This line is necessary for the final seeds stored in SIESTA.
                                        only_seeds_flag(iobj, 1:2, next_ind+1) = ud2.rfiducials(iobj, 1:2, next_ind+1); % This line is necessary so that when we move to the next slice, the seeds are updated and used to expand the polygons.
                                    end
                                end
                            end

                            ud2.rpoly = [];
                            
                        end
                        
                        
                    end
                    
                    % If necessary, extract geometry here.
                    if (ud.rSAVE_GEOM)
                        [nodes edges nodecellmap] = extract_geometry(segm(:, :, geometry_plane));
                        cd(ud.rcurrdir);
                        save(cat(2, 'geometry_data', rnum2str(index, 4)), 'nodes', 'edges', 'nodecellmap'); 
                    end                                        
                end	
                
                if numel(only_seeds_flag) == 1			
                    for j = 1:size(geom.nodes, 1)
                        [ud2.rfiducials ud2.rnfiducials] = cb_storecurrentpoint([geom.nodes(j, 2) geom.nodes(j, 1) index], ud2.rfiducials, ud2.rnfiducials);
                    end
                end		
            end
            
            waitbar(1, wb, 'Done!');
            
            close(wb);		
	else
            % 3D segmentation here.
            
 end
    else
	% Cancel segmentation
	segm = [];
	geom = [];
    end	

end

