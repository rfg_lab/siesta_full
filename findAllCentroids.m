function [centroids]=findAllCentroids(segments)

centroids=zeros(size(segments,1),2);
for i=1:size(segments,1)
    coords=segments{i};
    
    if ~isempty(coords)
        
        [area,cx,cy]=polycenter(coords(:,1),coords(:,2));
        if ~isnan(cx)
            centroids(i,:)=[cx,cy];
        end
    else
        centroids(i,:)=[-1,-1];
    end
 
end
end