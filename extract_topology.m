%  [neighbors, nneighbors, isborder, isinterfaceneigh, ids, ninterfaceneighbors, volumes] = extract_topology(in, volume_th)
%  [neighbors, nneighbors, isborder, isinterfaceneigh, ids, ninterfaceneighbors, volumes] = extract_topology(in, volume_th, obj_ids)
%
%  Obtains the neighbors for each cell in the input image.
%  
%  IN is a labeled dip_image.
%  VOLUME_TH is a threshold for the volume of the neighborhood
%  area (pixels within the neighbor object in contact with the
%  cell under analysis). Default value is interface_contact >=
%  >= 8 (although 7 is also a good value).
%  OBJ_IDS specifies a list of objects to compute neighbors of.
%  This function is computationally expensive, so you may want
%  to be selective about the number of cells you study. If this
%  parameter is empty or not provided, then all cells are
%  considered.
%
%  NEIGHBORS is a cell array where cell i contains the neighbors of
%  object i (0 might be a neighbor).
%  NNEIGHBORS is an array where element i contains the number of 
%  neighbors of object i (0 is not counted as a neighbor).
%  ISBORDER is a binary array where element i indicates whether
%  object i is adjacent to the border (neighbor of object 0).
%  ISINTERFACENEIGH is a cell array of binary arrays. Element j
%  in binary array i indicates whether object j is a node 
%  neighbor (0) or an interface neighbor (1) of object (i).
%  An object that shares a sufficiently long interface with another one,
%  even if it is in a single slice, is considered as an interface neighbor.
%  IDS is the id of each one of the cells analyzed (in the order
%  they were analyzed).
%  NINTERFACENEIGHBORS is an array where element i contains the 
%  number of number of interface neighbors of object i (0 is not counted 
%  as a neighbor). NNEIGHBORS-NINTERFACENEIGHBORS returns the number
%  of node neighbors per cell.
%  VOLUMES is a cell array where cell i contains the volume of the overlap
%  of i with each one of its neighbors (as given by neighbors{i}).
%  
%  Rodrigo Fernandez-Gonzalez
%  fernanr1@mskcc.org
%  6/29/2007

function varargout = extract_topology(varargin)


switch (nargin)
	case 1
		labeled_im = varargin{1};
		volume_th = 8; %* size(labeled_im, 3);
		object_list = nonzeros(unique(double(labeled_im)))';
	case 2
		labeled_im = varargin{1};
		volume_th = varargin{2};		
		object_list = nonzeros(unique(double(labeled_im)))';
	case 3
		labeled_im = varargin{1};
		volume_th = varargin{2};		
		object_list = varargin{3};
		if (isempty(object_list))
			object_list = nonzeros(unique(double(labeled_im)))';
		end
end

% Input is a labeled image, labeled_im.
neighbors = cell(numel(object_list), 1);
volumes = cell(numel(object_list), 1);
isinterfaceneigh = cell(numel(object_list), 1);
nneighbors = zeros(numel(object_list), 1);
isborder = zeros(numel(object_list), 1);
ids = zeros(numel(object_list), 1);
ninterfaceneighbors = zeros(numel(object_list), 1);
vol_per_slice = cell(numel(object_list), 1);

% Go through all objects.
for ii = 1:numel(object_list)
	% Compute the neighbors and the extent of the interfaces shared.
	[neighbors{ii} volumes{ii} tmp]= rneighborsq(object_list(ii), labeled_im, 0);
	
	% Objects with no neighbors are border objects.
	if (~ isempty(find(neighbors{ii} == 0)))
		isborder(ii) = 1;
		nneighbors(ii) = numel(neighbors{ii}) - 1;
	else
		nneighbors(ii) = numel(neighbors{ii});
	end

	ids(ii) = object_list(ii);
	
	%isinterfaceneigh{ii} = volumes{ii} >= volume_th;	
	
	% or ...
	% An object that shares a sufficiently long interface with another one,
	% even if it is in a single slice, is considered as an interface neighbor.
	isinterfaceneigh{ii} = zeros(1, numel(volumes{ii}));;	
	for jj = 1:numel(volumes{ii})
		if (any(tmp{jj} >= volume_th))
			isinterfaceneigh{ii}(jj) = 1;
			ninterfaceneighbors(ii)  = ninterfaceneighbors(ii) + 1;
		end 
	end	

	vol_per_slice{ii} = tmp;
end

varargout{1} = neighbors;
varargout{2} = nneighbors;
varargout{3} = isborder;
varargout{4} = isinterfaceneigh;
varargout{5} = ids;
varargout{6} = ninterfaceneighbors;
varargout{7} = volumes;
varargout{8} = vol_per_slice;
