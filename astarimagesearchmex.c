/*-----------------------------------------------------------------------------
 * astarimagesearchmex.c
 * A mex function tested for use with Matlab R2012b and higher.
 * This file conforms with C89 Standards. Please use a C89 compatible compiler.
 *
 * The main function is mexFunction.
 *
 * This is the mex implementation of astarimagesearch.m. I have
 * copied the text of the help file of that function below for your
 * convenience. Note that the default value for parameter B is different in
 * the mex implementation as percentile is calculated differently, and C89 has
 * a slightly higher precision than MATLAB. I have also added a parameter D
 * in this implementation which allows the user to abort the operation after
 * 'D' number of nodes have been searched.
 *
 * ASTARIMAGESEARCH A* path from start point to fin point.
 * PATH = ASTARIMAGESEARCH(IMAGESLICE,START,FIN) is the indexed pixels of
 *     the path from the indexed pixel START to the indexed pixel FIN along
 *     the graph generated from IMAGESLICE.
 *
 * PATH = ASTARTIMAGESEARCH(...,PARAMETERS) performs the
 *     search with the specified parameters, given as a vector. There are
 *     three parameters which affects the cost of the graph expressed as:
 *
 *         cost = A*brightness^B+C*distance
 *
 *     A fourth parameter D specifies the number of nodes to search before
 *     aborting the search. Aborted searches return -1. 
 *
 *     The PARAMETERS vector should be given in the order [A,B,C,D]. In the
 *     case PARAMETERS has less than four values, the parameters are assigned
 *     in order from left to right. You can use NaN as buffers to pick which
 *     values you want changed. The default values are [1.5,1.5,1,7000].
 *
 * PATH = ASTARIMAGESEARCH(...,BDEBUGMODE) when set to true, also plots
 *     the search path on the image, which is useful to visualize when you are
 *     not getting the path you want. Note that BDEBUGMODE is type logical.
 *
 * Author: Colin Tong
 * Date: 29-Sep-2016
-----------------------------------------------------------------------------*/

#include <math.h>
#include "matrix.h"
#include "mex.h"
#include <stdlib.h>

// Initialize subroutines

void loadParameters(double* parameters, const mxArray* userParams) {
    // Loads user specified parameters given the mxArray where they are stored
    if(mxIsEmpty(userParams)) {
        return;
    } else {
        // We treat userParams as a vector and index accordingly
        int numel = mxGetM(userParams) * mxGetN(userParams);
        if(numel > 4) {
            numel = 4;
        }
        double* data = mxGetPr(userParams);
        int i;
        for(i = 0; i < numel; i++) {
            if(!mxIsNaN(*(data+i))) {
                parameters[i] = *(data+i);
            }
        }
        return;
    }
}

int compare_doubles(const void *a, const void *b) {
    // Doubles comparator for use in qsort
    const double *da = (const double*)a;
    const double *db = (const double*)b;
    return (*da > *db) - (*da < *db);
}

double findFifthPercentile(double* input, int size) {
    // Computes the fifth percentile of the input
    // Ignore 0s caused by rotating the image
    int numberOfZeros = 0;
    int i;
    for(i = 0; i < size; i++) {
        if(*(input+i) == 0) {
            numberOfZeros++;
        }
    }
    // Duplicate input for sorting, we do not want to change the original data
    double* sorted = (double*)calloc(size-numberOfZeros,sizeof(double));
    int zeroIndex = 0;
    for(i = 0; i < size; i++) {
        if(*(input+i) != 0) {
            *(sorted+i-zeroIndex) = *(input+i);
        } else {
            zeroIndex++;
        }
    }
    qsort(sorted,size-numberOfZeros,sizeof(double),compare_doubles);
    double returnval = *(sorted+((int)((size-numberOfZeros)/20)));
    free(sorted);
    return returnval;
}

double findNinetyfifthPercentile(double* input, int size) {
    // Computes the ninety-fifth percentile of the input
    // Ignore 0s caused by rotating the image
    int numberOfZeros = 0;
    int i;
    for(i = 0; i < size; i++) {
        if(*(input+i) == 0) {
            numberOfZeros++;
        }
    }
    // Duplicate input for sorting, we do not want to change the original data
    double* sorted = (double*)calloc(size-numberOfZeros,sizeof(double));
    int zeroIndex = 0;
    for(i = 0; i < size; i++) {
        if(*(input+i) != 0) {
            *(sorted+i-zeroIndex) = *(input+i);
        } else {
            zeroIndex++;
        }
    }
    qsort(sorted,size-numberOfZeros,sizeof(double),compare_doubles);
    double returnval = *(sorted+((int)((size-numberOfZeros)/20*19)));
    free(sorted);
    return returnval;
}

double* findBothPercentiles(double* input, int size) {
    // Compute both percentiles at the same time for better performance
    // The return format is [fifthPercentile ninetyFifthPercentile]
    // Note that you should free the memory after you are done
    // Ignore 0s caused by rotating the image
    int numberOfZeros = 0;
    int i;
    for(i = 0; i < size; i++) {
        if(*(input+i) == 0) {
            numberOfZeros++;
        }
    }
    // Duplicate input for sorting, we do not want to change the original data
    double* sorted = (double*)calloc(size-numberOfZeros,sizeof(double));
    int zeroIndex = 0;
    for(i = 0; i < size; i++) {
        if(*(input+i) != 0) {
            *(sorted+i-zeroIndex) = *(input+i);
        } else {
            zeroIndex++;
        }
    }
    qsort(sorted,size-numberOfZeros,sizeof(double),compare_doubles);
    double* returnvals = (double*)calloc(2,sizeof(double));
    *(returnvals) = *(sorted+((int)((size-numberOfZeros)/20)));
    *(returnvals+1) = *(sorted+((int)((size-numberOfZeros)/20*19)));
    free(sorted);
    return returnvals;
}

double costCalc(double prevG, int nodeId, double distanceCost, int width, int height, double* costList, double cParam, int fin, int parentId) {
    double G = prevG + *(costList+nodeId) + distanceCost;
    int y = (int)floor(nodeId/width);
    int x = nodeId-(y*width);
    int finY = (int)floor(fin/width);
    int finX = fin-(finY*width);
    double H = cParam*(sqrt(pow(x-finX,2)+pow(y-finY,2)));
    bool bAbove = false;
    bool bBelow = false;
    bool bLeft = false;
    bool bRight = false;

    if(x > 0) { // There is a column to the left
        bLeft = true;
    }
    if(x < width-1) { // There is a column to the right
        bRight = true;
    }
    if(y > 0) { // There is a row above
        bAbove = true;
    }
    if(y < height-1) { // There is a row below
        bBelow = true;
    }
    double lowestNeighbour = -1;
    if(bLeft) { // Check left successors
        int leftNode = nodeId - 1;
        if(leftNode != parentId && (lowestNeighbour == -1 || *(costList+leftNode) < lowestNeighbour)) {
            lowestNeighbour = *(costList+leftNode);
        }
        if(bAbove) { // Check top left
            int topLeftNode = leftNode - width;
            if(topLeftNode != parentId && (lowestNeighbour == -1 || *(costList+topLeftNode) < lowestNeighbour)) {
                lowestNeighbour = *(costList+topLeftNode);
            }
        }
        if(bBelow) { // Check bottom left
            int bottomLeftNode = leftNode + width;
            if(bottomLeftNode != parentId && (lowestNeighbour == -1 || *(costList+bottomLeftNode) < lowestNeighbour)) {
                lowestNeighbour = *(costList+bottomLeftNode);
            }
        }
    }
    if(bAbove) { // Check top successor
        int topNode = nodeId - width;
        if(topNode != parentId && (lowestNeighbour == -1 || *(costList+topNode) < lowestNeighbour)) {
                lowestNeighbour = *(costList+topNode);
        }
    }
    if(bBelow) { // Check bottom successor
        int bottomNode = nodeId + width;
        if(bottomNode != parentId && (lowestNeighbour == -1 || *(costList+bottomNode) < lowestNeighbour)) {
            lowestNeighbour = *(costList+bottomNode);
        }
    }
    if(bRight) { // Check right successors
        int rightNode = nodeId + 1;
        if(rightNode != parentId && (lowestNeighbour == -1 || *(costList+rightNode) < lowestNeighbour)) {
            lowestNeighbour = *(costList+rightNode);
        }
        if(bAbove) { // Check top right
            int topRightNode = rightNode - width;
            if(topRightNode != parentId && (lowestNeighbour == -1 || *(costList+topRightNode) < lowestNeighbour)) {
                lowestNeighbour = *(costList+topRightNode);
            }
        }
        if(bBelow) { // Check bottom right
            int bottomRightNode = rightNode + width;
            if(bottomRightNode != parentId && (lowestNeighbour == -1 || *(costList+bottomRightNode) < lowestNeighbour)) {
                lowestNeighbour = *(costList+bottomRightNode);
            }
        }
    }
    H += lowestNeighbour;
    return G+H;
}

// Path data node definition. We use this to compute the A* path with dynamic sizing

typedef struct pathNode {
    int nodeId;
    struct pathNode* next;
} pathNode;

int calculatePath(mxArray* plhs[], int* pathData, int start, int fin) {
    //mexPrintf("PATH FOUND\n");
    // Start by creating a linked list of pathNodes, starting with the goal node
    struct pathNode* pathList = malloc(sizeof *pathList);
    pathList->nodeId = fin;
    pathList->next = NULL;
    // Now build the linked list using pathData
    int count = 0;
    pathNode* currNode = pathList;
    int currNodeId = fin;
    while(*(pathData+currNodeId) != 0) {
        count++;
        // Add the next node
        int nextNodeId = *(pathData+currNodeId);
        currNode->next = malloc(sizeof currNode->next);
        currNode->next->nodeId = nextNodeId;
        currNode->next->next = NULL;
        // Enter the next iteration
        currNodeId = nextNodeId;
        currNode = currNode->next;
    }
    // Build the return vector in MATLAB's format
    plhs[0] = mxCreateDoubleMatrix((mwSize)count+1,(mwSize)1,mxREAL);
    double* path = mxGetPr(plhs[0]);
    int index = 0;
    currNode = pathList;
    while(currNode != NULL) {
        *(path+index) = currNode->nodeId;
        currNode = currNode->next;
        index++;
    }
    *(path+count) = start;
    // Free memory from temporary linkedlist
    currNode = pathList;
    pathNode* nextNode;
    while(currNode != NULL) {
        nextNode = currNode->next;
        free(currNode);
        currNode = nextNode;
    }
    return count+1;
}

// Linked list node definition. We use this for the search frontier

typedef struct node {
    int nodeId;
    double fCost;
    double gCost;
    struct node* next;
} node;

// Linked list helper functions
node* removeNodeFromList(node* head, int index) {
    // Removes the node at the specified index and returns a pointer to the new head node.
    // If the only node is removed a NULL pointer is returned.
    if(index == 0) {
        node* newHead = head->next;
        free(head);
        return newHead;
    } else {
        // Go to the node before the one we want to remove
        node* currNode = head;
        int i;
        for(i = 1; i < index - 1; i++) {
            currNode = currNode->next;
            if(currNode == NULL) {
                // Out of bounds, do nothing
                return head;
            }
        }
        node* removeNode = currNode->next;
        if(removeNode == NULL) {
            // Out of bounds, do nothing
            return head;
        }
        node* appendNode = removeNode->next;
        currNode->next = appendNode;
        free(removeNode);
        return head;
    }
}

void processAddToOpen(int nodeId, double prevG, int parentId, double distanceCost, bool* closedList, double* costList, node** openList, int width, int height, int* pathData, double cParam, int fin, double* costCache) {
    //mexPrintf("ADD TO OPEN INPUTS: %d, %f, %d, %f, %p, %p, %p, %d, %p, %f, %d\n",nodeId, prevG, parentId, distanceCost, closedList, costList, openList, width, pathData, cParam, fin);
    if(*(closedList+nodeId) == true) { // This node has already been traversed
        return;
    }
    double G = prevG + *(costList+nodeId) + distanceCost;
    //mexPrintf("prevG: %f, cost: %f, distanceCost %f\n",prevG, *(costList+nodeId), distanceCost);
    double F = costCalc(prevG, nodeId, distanceCost, width, height, costList, cParam, fin, parentId);
    //mexPrintf("G and F values: %f, %f\n",G,F);

    // Check if this node has already been added from a different path, and if so check if it
    // has a lower cost. If a lower cost path exists, then terminate this search path.
    node* headNode = *openList;
    node* currNode;
    /*
    node* currNode = headNode;
    while(currNode != NULL) {
        //mexPrintf("CURRNODE: %p\n",currNode);
        if(currNode->nodeId == nodeId) {
            if(currNode->fCost <= F) {
                return; // Do not add this node to the open frontier
            }
        }
        currNode = currNode->next;
    }
     */
    if(*(costCache+nodeId) == -1) { // This node has never been added to the search frontier
        *(costCache+nodeId) = F;
    } else if(*(costCache+nodeId) <= F) { // This node has been added on a path with a lower or equal cost
        return; // Do not add the current path to the search frontier
    } else { // This path has the lowest cost so far, update the costCache with this value
        *(costCache+nodeId) = F;
    }
    //mexPrintf("EXITED WHILE LOOP\n");
    // At this point the node is okay to add to the open list, in ascending fCost order
    struct node* newNode = malloc(sizeof *newNode);
    newNode->nodeId = nodeId;
    newNode->fCost = F;
    newNode->gCost = G;
    int position = 0;
    if(headNode == NULL) { // If the list is empty then make the list equal to the new node
        newNode->next = NULL;
        *(openList) = newNode;
    } else if(F <= headNode->fCost) { // If the new node has the lowest fCost we must place it at the front and renew the head node
        //mexPrintf("NEW NODE HAS LOWEST COST\n");
        newNode->next = headNode;
        *(openList) = newNode;
    } else { // Find the appropriate position to insert the new node
        //mexPrintf("NEW NODE DOES NOT HAVE LOWEST COST\n");
        currNode = headNode;
        node* nextNode = headNode->next;
        if(nextNode != NULL) {
            while(F > nextNode->fCost) {
                position++;
                currNode = nextNode;
                nextNode = nextNode->next;
                if(nextNode == NULL) { // We reached the end of the open list, stop iterating
                    break;
                }
            }
        }
        // Insert the node in the right spot
        currNode->next = newNode;
        newNode->next = nextNode;
    }
    *(pathData+nodeId) = parentId;
    //mexPrintf("NODE INSERTED AT POSITION: %d\n",position);
}

void freeMemory(int* pathList, node* openList, bool* closedList, double* costCache) {
    free(pathList);
    free(closedList);
    free(costCache);
    node* currNode = openList;
    node* nextNode;
    while(currNode != NULL) {
        nextNode = currNode->next;
        free(currNode);
        currNode = nextNode;
    }
}

// Main function

void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[]) {
    // Variable declaractions
    double* imageSlice;
    int start, fin;
    double parameters[4] = {1.5,1.5,1,7000};
    bool bDebugMode = false;

    // Input output checking
    if(nrhs > 5) {
        mexErrMsgTxt("Too many inputs. Maximum is 5.");
    } else if(nrhs < 3) {
        mexErrMsgTxt("Not enough inputs. Minimum is 3.");
    } else if(nrhs == 4) { // Determine if fourth input is parameters of is bDebugMode
        if(mxIsLogical(prhs[3])) {
            // We consider the fourth input to be bDebugMode if it is type logical
            bDebugMode = *mxGetLogicals(prhs[3]);
        } else if(mxIsDouble(prhs[3])) {
            loadParameters(parameters, prhs[3]);
            bDebugMode = false;
        } else {
            mexErrMsgTxt("Input 4 must be type logical or type double.");
        }
    } else if(nrhs == 5) {// Determine which of the last two inputs is parameters and
        // which is bDebugMode
        if(mxIsLogical(prhs[3]) && mxIsDouble(prhs[4])) {
            bDebugMode = *mxGetLogicals(prhs[3]);
            loadParameters(parameters, prhs[4]);
        } else if(mxIsLogical(prhs[4]) && mxIsDouble(prhs[3])) {
            bDebugMode = *mxGetLogicals(prhs[4]);
            loadParameters(parameters, prhs[3]);
        } else {
            mexErrMsgTxt("Input 4 and 5 must have one type logical and one type double.");
        }
    }

    if(!mxIsDouble(prhs[0]) || mxIsComplex(prhs[0])) {
        mexErrMsgTxt("Input imageSlice must contain only non-complex type double.");
    } else {
        imageSlice = mxGetPr(prhs[0]);
    }

    if(mxGetNumberOfDimensions(prhs[0]) > 2) {
        mexErrMsgTxt("Input imageSlice must be two dimensional.");
    }

    start = (int)mxGetScalar(prhs[1]);
    fin = (int)mxGetScalar(prhs[2]);

    if(start == fin) {
        plhs[0] = mxCreateDoubleMatrix((mwSize)1,(mwSize)1,mxREAL);
        double* path = mxGetPr(plhs[0]);
        *(path) = start;
        return;
    }

    if(nlhs > 1) {
        mexErrMsgTxt("Too many outputs. Maximum is 1.");
    }

    // Verify input (debugging only)
    //mexPrintf("%s\n", bDebugMode ? "true" : "false");
    //mexPrintf("%f,%f,%f\n", parameters[0], parameters[1], parameters[2]);

    // Calculate brightness cost matrix
    // We must create a working copy or else we will alter the original matrix passed in from MATLAB
    int width = mxGetM(prhs[0]);
    int height = mxGetN(prhs[0]);

    //mexPrintf("width:%d,height:%d\n",width,height);

    int nImageSlice = width * height;
    double* costList = (double*)calloc(nImageSlice, sizeof(double));
    int i;
    for(i = 0; i < nImageSlice; i++) {
        *(costList+i) = *(imageSlice+i);
    }

    double* percentiles = findBothPercentiles(costList, nImageSlice);
    double lowPercentile = percentiles[0];
    double highPercentile = percentiles[1];
    free(percentiles);
    //double lowPercentile = findFifthPercentile(costList, nImageSlice);
    //double highPercentile = findNinetyfifthPercentile(costList, nImageSlice);
    // Limit intensities between 5th and 95th percentile to reduce effects of noise and saturation
    for(i = 0; i < nImageSlice; i++) {
        if(*(costList+i) < lowPercentile) {
            *(costList+i) = lowPercentile;
        }
        else if(*(costList+i) > highPercentile) {
            *(costList+i) = highPercentile;
        }
    }

    double minImageSlice = lowPercentile;
    double maxImageSlice = highPercentile;
    //mexPrintf("%f, %f\n", highPercentile, lowPercentile);
    for(i = 0; i < nImageSlice; i++) {
        *(costList+i) = pow(parameters[1]*((((*(costList+i)-minImageSlice)/(maxImageSlice-minImageSlice))-1)*-1),parameters[2]);
    }

    // Initialize required intermediate data structures
    int* pathData = (int*)calloc(nImageSlice, sizeof(int)); // Records the preceding node on the shortest path
    struct node* openList = malloc(sizeof *openList); // The head node of the open frontier. We keep this linked list ordered by ascending fCost.
    openList->nodeId = start;
    openList->fCost = 0;
    openList->gCost = 0;
    openList->next = NULL;
    bool* closedList = (bool*)calloc(nImageSlice, sizeof(bool)); // Records whether or not each node has been closed
    double* costCache = (double*)calloc(nImageSlice, sizeof(double)); // Records the current lowest cost of each node (we could find it by iterating through openList but this cache is much faster)
    for(i = 0; i < nImageSlice; i++) { // Initialize values to -1 to indicate that this node has not yet been added to the open list
        *(costCache+i) = -1;
    }

    //mexPrintf("Pre loop status: %d, %f, %f\n",openList->nodeId,openList->fCost,openList->gCost);
    int iteration = 0;
    while(openList != NULL) {
        // Pop the lowest cost node (first node) on the open list, call this node 'q'
        int qNodeId = openList->nodeId;
        double qGCost = openList->gCost;
        if(qGCost!=qGCost){
            mexPrintf("FatalError:gCost is nan. There is a bug.\n");
            mexPrintf("DATA DUMP:\n 5th:%f\n95th:%f\n",lowPercentile,highPercentile);
            break;
        }
        //mexPrintf("Loop start status: %d, %f, %f\n",qNodeId,openList->fCost,qGCost);
        openList = removeNodeFromList(openList, 0);

        // Generate q's successors
        int qY = (int)floor(qNodeId/width);
        int qX = qNodeId-(qY*width);
        bool bAbove = false;
        bool bBelow = false;
        bool bLeft = false;
        bool bRight = false;

        if(qX > 0) { // There is a column to the left
            bLeft = true;
        }
        if(qX < width-1) { // There is a column to the right
            bRight = true;
        }
        if(qY > 0) { // There is a row above
            bAbove = true;
        }
        if(qY < height-1) { // There is a row below
            bBelow = true;
        }

        //mexPrintf("Successor Data: Y:%d, X:%d, Above:%d, Below:%d, Left:%d, Right:%d\n",qY,qX,bAbove,bBelow,bLeft,bRight);

        if(bLeft) { // Check left successors
            int leftNode = qNodeId - 1;
            if(leftNode == fin) {
                *(pathData+leftNode) = qNodeId;
                int length = calculatePath(plhs,pathData,start,fin);
                freeMemory(pathData, openList, closedList, costCache);
                //mexPrintf("Path length/Nodes opened: %d/%d\n",length,iteration);
                return;
            } else {
                processAddToOpen(leftNode,qGCost,qNodeId,parameters[2],closedList,costList,&openList,width,height,pathData,parameters[2],fin,costCache);
            }
            if(bAbove) { // Check top left
                int topLeftNode = leftNode - width;
                if(topLeftNode == fin) {
                    *(pathData+topLeftNode) = qNodeId;
                    int length = calculatePath(plhs,pathData,start,fin);
                    freeMemory(pathData, openList, closedList, costCache);
                    //mexPrintf("Path length/Nodes opened: %d/%d\n",length,iteration);
                    return;
                } else {
                    processAddToOpen(topLeftNode,qGCost,qNodeId,parameters[2]*sqrt(2),closedList,costList,&openList,width,height,pathData,parameters[2],fin,costCache);
                }
            }
            if(bBelow) { // Check bottom left
                int bottomLeftNode = leftNode + width;
                if(bottomLeftNode == fin) {
                    *(pathData+bottomLeftNode) = qNodeId;
                    int length = calculatePath(plhs,pathData,start,fin);
                    freeMemory(pathData, openList, closedList, costCache);
                    //mexPrintf("Path length/Nodes opened: %d/%d\n",length,iteration);
                    return;
                } else {
                    processAddToOpen(bottomLeftNode,qGCost,qNodeId,parameters[2]*sqrt(2),closedList,costList,&openList,width,height,pathData,parameters[2],fin,costCache);
                }
            }
        }
        if(bAbove) { // Check top successor
            int topNode = qNodeId - width;
            if(topNode == fin) {
                *(pathData+topNode) = qNodeId;
                int length = calculatePath(plhs,pathData,start,fin);
                freeMemory(pathData, openList, closedList, costCache);
                //mexPrintf("Path length/Nodes opened: %d/%d\n",length,iteration);
                return;
            } else {
                processAddToOpen(topNode,qGCost,qNodeId,parameters[2],closedList,costList,&openList,width,height,pathData,parameters[2],fin,costCache);
            }
        }
        if(bBelow) { // Check bottom successor
            int bottomNode = qNodeId + width;
            if(bottomNode == fin) {
                *(pathData+bottomNode) = qNodeId;
                int length = calculatePath(plhs,pathData,start,fin);
                freeMemory(pathData, openList, closedList, costCache);
                //mexPrintf("Path length/Nodes opened: %d/%d\n",length,iteration);
                return;
            } else {
                processAddToOpen(bottomNode,qGCost,qNodeId,parameters[2],closedList,costList,&openList,width,height,pathData,parameters[2],fin,costCache);
            }
        }
        if(bRight) { // Check right successors
            int rightNode = qNodeId + 1;
            if(rightNode == fin) {
                *(pathData+rightNode) = qNodeId;
                int length = calculatePath(plhs,pathData,start,fin);
                freeMemory(pathData, openList, closedList, costCache);
                //mexPrintf("Path length/Nodes opened: %d/%d\n",length,iteration);
                return;
            } else {
                processAddToOpen(rightNode,qGCost,qNodeId,parameters[2],closedList,costList,&openList,width,height,pathData,parameters[2],fin,costCache);
            }
            if(bAbove) { // Check top right
                int topRightNode = rightNode - width;
                if(topRightNode == fin) {
                    *(pathData+topRightNode) = qNodeId;
                    int length = calculatePath(plhs,pathData,start,fin);
                    freeMemory(pathData, openList, closedList, costCache);
                    //mexPrintf("Path length/Nodes opened: %d/%d\n",length,iteration);
                    return;
                } else {
                    processAddToOpen(topRightNode,qGCost,qNodeId,parameters[2]*sqrt(2),closedList,costList,&openList,width,height,pathData,parameters[2],fin,costCache);
                }
            }
            if(bBelow) { // Check bottom right
                int bottomRightNode = rightNode + width;
                if(bottomRightNode == fin) {
                    *(pathData+bottomRightNode) = qNodeId;
                    int length = calculatePath(plhs,pathData,start,fin);
                    freeMemory(pathData, openList, closedList, costCache);
                    //mexPrintf("Path length/Nodes opened: %d/%d\n",length,iteration);
                    return;
                } else {
                    processAddToOpen(bottomRightNode,qGCost,qNodeId,parameters[2]*sqrt(2),closedList,costList,&openList,width,height,pathData,parameters[2],fin,costCache);
                }
            }
        }
        iteration++;
        //mexPrintf("Iteration complete. Iteration:%d\n",iteration);
        if(iteration > parameters[3]) {
            break;
        }
        // Put q on the closed list
        *(closedList+qNodeId) = true;
    }
    // No path found, return -1
    plhs[0] = mxCreateDoubleMatrix((mwSize)1,(mwSize)1,mxREAL);
    double* path = mxGetPr(plhs[0]);
    *(path) = -1;
    freeMemory(pathData, openList, closedList, costCache);
    mexPrintf("Error: No path found after limit of %d nodes explored\n", (int) parameters[3]);
    //mexPrintf("DATA DUMP:\n 5th:%f\n95th:%f\n",lowPercentile,highPercentile);
    /*
    mexPrintf("ZEROS:\n");
    for(i = 0; i < nImageSlice; i++) {
        if(*(imageSlice+i) == 0) {
            mexPrintf("%d\n",i);
        }
        //mexPrintf("%f\n",*(costList+i));
    }
     */
}
