% [x_coords y_coords plot_handle] = circle(center_x, center_y, radius, number_of_points, draw)

function [x y lineh] = circle(h, k, r, N, draw)

lineh = -1;

t=(0:N)*2*pi/N;
x = (r*cos(t)+h)';
y = (r*sin(t)+k)';

if draw
	lineh = plot(x, y);
end


