%INSTRUCTIONS: - THIS IS THE ONE WHERE WE ARE FIXING SIESTA.
%
% KYMOGRAPHS
% - First annotate the images with fiducials to register them (using 'R').
% - Draw a rectangle ('r' option) in one of the time points to determine the ROI.
% - Select 'K' and click on the contour of the ROI.
% - Choose a file name and format. The kymograph is displayed in a new window.
%
% FRAP analysis
% -Load time series.
% -Crop image around unique marker, change mapping mode to 12bits and check that the edge
%  under analysis remains in focus.
% -Draw fiducials around the bleached area in each one of the time points to analyze.
% -Add a unique marker at the center of the ROI.
% -Register (R) all tps to tp before bleaching.
% -Crop (P) the image around the unique marker.
% -Export (x) image as sequence (to save 16b info). Use ics format.
% -Draw ROI in all time points.
% -Draw trajectory along the edge span in the two time points preceding bleaching.
% -Save annotations.
% -Measure signal over time.
%
% CLUSTERING ANALYSIS
% - Project the image (from the color channel) using the 'J' option (channels 1-4).
% - Change to the channel that you will use to segment the image.
% - Save the projected image from the channel you want to quantify ('Y').
% - Segment the image.
% - Save original geometry.
% - Correct geometry.
% - Define ROI (350x350) and save it.
% - Measure edges by selecting positive interfaces ('p', min_l = 0, ang = 0).
% - Save the corrected and consolidated geometry (this will also save the edge measurements).
%
% Rodrigo Fernandez-Gonzalez
% fernanr1@mskcc.org
% 08/07/2007
%
% Lots of updates on 2016/08.
function varargout = mycallbacks2(varargin)

    extensions = {'*.csv'; '*.mat'; '*.*'};
    extensions_matfirst = {'*.mat'; '*.csv'; '*.*'};
    volume_analysis_filename = 'analysis_3D.mat';
    field_analysis_filename = 'analysis_field.mat';
    local_analysis_filename = 'analysis_local.mat';

    bin_limits = (0:15:180);
    delimiter_sz = 6;
    delimiter_color = 'bo';
    fiducial_sz = 6;
    fiducial_color = 'ro';
    selected_fiducial_color = 'co';
    fiducial_label_sz = 10;
    tr_label_sz = 10;
    unique_color = 'yo';
    line_color = 'b';
    poly_sz = 1;
    poly_color = 'g';
    ang_color = 'm';
    t_res = 15; % Time resolution in seconds.
    default_fps = 7;

    fig = varargin{1}; % these are fig
    ud = get(fig, 'UserData');

    action = varargin{2};

    switch (action)
        % KEYBOARD-ONLY CALLBACKS -------------------------------------------------------
	% Hit n or >
      case {'k_n', 'k_.'}
        %disp('N clicked');
        % This line necessary, because DIPImage only moves to the next
        % slice upon hitting n.
        set(fig, 'CurrentCharacter', 'n');
        eval('dipshow DIP_callback KeyPressFcn');
        repaint();
        
	% Hit p or <
      case {'k_p', 'k_,'}
        %disp('P clicked');
        % This line necessary, because DIPImage only moves to the previous
        % slice upon hitting p.
        set(fig, 'CurrentCharacter', 'p');
        eval('dipshow DIP_callback KeyPressFcn');
        repaint();
        
	% Jumps to a certain time point. Fkey moves key slices forward (e.g. Shift + 4 while on slice 36 goes to 40). A number only moves to that absolute time point. 	
      case {'k_0', 'k_1', 'k_2', 'k_3', 'k_4', 'k_5', 'k_6', 'k_7', 'k_8', 'k_9'}		            
        ind = str2num(action(end));
        ud = cb_goto(fig, ind);
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud);
        
        % Repaint.
        repaint();
        
      case {'k_f1', 'k_f2', 'k_f3', 'k_f4', 'k_f5', 'k_f6', 'k_f7', 'k_f8', 'k_f9', 'k_f10', 'k_f11', 'k_f12'}		            
        ind = str2num(action((strfind(action, 'f')+1):end));
        ud = cb_goto(fig, ud.curslice+ind);
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud);
        
        % Repaint.
        repaint();
        
      case {'go_to', 'k_g'}
        % Read where to go next.
        if nargin > 2 % Destination time point provided as an argument.
            answer = {num2str(varargin{3})}; % Wrap data as expected below for the output of a user dialog.
        else                        
            answer = inputdlg('Go to frame number ', 'Go to');
        end                        
        if ~ isempty(answer)
            
            ind = str2num(answer{1});
            
            % Jump there.
            ud = cb_goto(fig, ind);
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud);
            
        end        
        
        % Repaint.
        repaint();  

        
        % IO CALLBACKS -------------------------------------------------------
      case {'load_ts'; 'k_t'}
        cancel = cb_loadtimeseries(fig);
        
        % Move time point forward and backward to force proper image display.
        if ~ cancel
            set(fig, 'CurrentCharacter', 'n');
            eval('dipshow DIP_callback KeyPressFcn');
            set(fig, 'CurrentCharacter', 'p');
            eval('dipshow DIP_callback KeyPressFcn');
            
            repaint();                 	
        end

      case {'save_ts'; 'k_Y'}
        cancel = cb_savetimeseries(fig);
        
      case {'save_seq'; 'k_x'}
        cancel = cb_savesequence(fig);
        
      case {'save_avi'}
        cancel = cb_saveavi(fig);
        
      case {'load_stk'; 'k_I'}
        cancel = cb_loadcolorimage(fig);
        
        % Move time point forward and backward to force proper image display.
        if ~ cancel
            set(fig, 'CurrentCharacter', 'n');
            eval('dipshow DIP_callback KeyPressFcn');
            set(fig, 'CurrentCharacter', 'p');
            eval('dipshow DIP_callback KeyPressFcn');
        end
        
% $$$     case 'nxt_im'
% $$$         [newpath newpath2] = cb_loadnext(fig, ud.rfile, ud.rfile2) % fig is really a string with a file name.
% $$$         disp(cat(2, 'Loaded next image: ', newpath));
% $$$         if (~ isempty(newpath2))
% $$$             disp(cat(2, 'Loaded next image: ', newpath2));
% $$$         end
% $$$         
% $$$         ud.rfile = newpath;
% $$$         ud.rfile2 = newpath2;
% $$$         set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
% $$$         set(fig, 'UserData', ud);
% $$$         
% $$$     case 'prv_im'
% $$$         [newpath newpath2] = cb_loadprev(fig, ud.rfile, ud.rfile2); % fig is really a string with a file name.
% $$$         disp(cat(2, 'Loaded previous image: ', newpath));
% $$$         if (~ isempty(newpath2))
% $$$             disp(cat(2, 'Loaded previous image: ', newpath2));
% $$$         end
% $$$         
% $$$         ud.rfile = newpath;
% $$$         ud.rfile2 = newpath2;
% $$$         set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
% $$$         set(fig, 'UserData', ud);
% $$$         
% $$$     case 'save_im'
% $$$         curr_dir = pwd;
% $$$         cd(ud.rcurrdir);
% $$$         
% $$$         cb_saveimage(ud.slices);
% $$$         
% $$$         cd(curr_dir);
        
      case 'load_fiducials'
        ud = cb_loadfiducials(fig);
        cb_paintpoints(fig, ud.rfiducials, fiducial_sz, fiducial_color, ud.rselectedfiducials, selected_fiducial_color);
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud);
        
      case 'save_fiducials'
        cb_savefiducials(ud.rfiducials, ud.rnfiducials, extensions);
        
      case {'load_ann', 'k_L'}
        if nargin < 3
            ud = cb_loadannotations(fig, line_color);
        else
            ud = cb_loadannotations(fig, line_color, varargin{3});
        end
        
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud);
        
        repaint();
      case 'add_ann'
        if nargin < 3
            ud = cb_addannotations(fig, line_color);
        else
            ud = cb_addannotations(fig, line_color, varargin{3});
        end
        
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud);

        repaint();
        
      case 'load_fidu_only' % Substitute fiducials in current annotations for the ones in the selected file.
        ud = cb_loadfiducials(fig);
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud);
        repaint();
        
      case {'load_fidu_del'; 'k_D'}
        ud = cb_loadfiducialsasdelimiters(fig);
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud);
        repaint();
        
      case 'load_metamorph'
        ud = cb_loadmetamorphunique(fig);
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud);
        repaint();
        
      case 'import_labeled'
        ud = cb_importlabeledimage(fig);
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud);
        repaint();
        
      case {'save_ann'; 'k_a'}
        ud = cb_saveannotations(ud);
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud);
        
      case 'export_poly'
        figure(fig);
        set(fig, 'WindowButtonDownFcn', 'mycallbacks2(gcbf, ''export_poly_down'');');
        set(fig, 'WindowButtonUpFcn', '');
        set(fig, 'WindowButtonMotionFcn', 'show_coord_pix(gcf);');
        set(fig, 'Pointer', 'crossh');
        
      case 'export_poly_down'
        coords = cb_getcoords(fig);
        
        % If the right button was used to unselect, coords is set to
        % an empty vector.
        if ~isempty(coords)
            % selectcurrentpoint returns a vector with [index 0 ud.curslice+1].
            index = cb_selectcurrentpoint(fig, ud.rfiducials, ud.rselectedfiducials);
            index = index(1);
        end
        
        % Time points to export.
        cb_exportpolygonannotations(ud, index);
        
        % Michael's division code
        
      case 'export_poly_div'
        figure(fig);
        set(fig, 'WindowButtonDownFcn', 'mycallbacks2(gcbf, ''export_poly_down_div'');');
        set(fig, 'WindowButtonUpFcn', '');
        set(fig, 'WindowButtonMotionFcn', '');
        set(fig, 'Pointer', 'crossh');
        
      case 'export_poly_down_div'
        coords = cb_getcoords(fig);
        
        % If the right button was used to unselect, coords is set to
        % an empty vector.
        if ~isempty(coords)
            % selectcurrentpoint returns a vector with [index 0 ud.curslice+1].
            index = cb_selectcurrentpoint(fig, ud.rfiducials, ud.rselectedfiducials);
            index = index(1);
        end
        
        % Time points to export.
        cb_exportpolygonannotations_div(ud, index, coords(3));
        % Michael's division code ends
        
      case 'export_all_poly'
        for ifidu = 1:size(ud.rfiducials, 1)
            for ipoly = 1:size(ud.rpolygons, 1)
                thepoly = ud.rpolygons{ipoly, :, ud.curslice+1};
                if ~isempty(thepoly) && inpolygon(ud.rfiducials(ifidu, 1, ud.curslice+1), ud.rfiducials(ifidu, 2, ud.curslice+1), thepoly(:, 1), thepoly(:, 2))
                    cb_exportpolygonannotations(ud, ifidu);
                    break;
                end
            end
        end
        
      case {'export_poly_roi', 'k_O'}
        cancel = cb_exportpolygonsroi(fig);
        
        % OPTIONS CALLBACKS -------------------------------------------------------
        
      case 'pick_val' % If there is a 3rd argument to this action, and that argument is zero, then no repeats are allowed in the list of selected pixel values. if the value is 2, then when a value is selected for the second time it is removed from the list. The third time it is added back, ...
        if nargin < 3 || varargin{3} == 1
            ud.rpix_val_mode = 1;
        elseif nargin >= 3
            ud.rpix_val_mode = varargin{3};
        end
        figure(fig);
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud);
        set(fig, 'WindowButtonDownFcn', 'mycallbacks2(gcbf, ''pick_val_down'');');
        set(fig, 'WindowButtonUpFcn', 'mycallbacks2(gcbf, ''pick_val_up'');');
        set(fig, 'Pointer', 'crossh');
        
      case 'pick_val_down'
        switch ud.rpix_val_mode
          case 1
            ud.rPIX_VALUE(end+1) = cb_getpixvalue(fig); % Stores list of selected values.
            disp(cat(2, 'Pixel value: ', num2str(ud.rPIX_VALUE(end))));
            
          case 0 % No repetitions.
            val = cb_getpixvalue(fig);
            if isempty(find(ud.rPIX_VALUE == val))
                ud.rPIX_VALUE(end+1) = val;
            end
            
            disp(ud.rPIX_VALUE);
            
          case 2 % Repetition removes from list.
            val = cb_getpixvalue(fig);
            ind = find(ud.rPIX_VALUE == val);
            if isempty(ind)
                ud.rPIX_VALUE(end+1) = val;
            else
                ind = find(ud.rPIX_VALUE ~= val);
                ud.rPIX_VALUE = ud.rPIX_VALUE(ind);
            end
            
            disp(ud.rPIX_VALUE);
        end
        
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud);
        set(fig, 'WindowButtonMotionFcn', 'show_coord_pix(gcf); mycallbacks2(gcbf, ''pick_val_down'');');
        
      case 'pick_val_up'
        set(fig, 'WindowButtonMotionFcn', 'show_coord_pix(gcf);');
        
      case {'set_brush', 'k_B'}
        ud.rBRUSH_SZ = dialog_brushsz((1:2:51), ud.rBRUSH_SZ);
        
        if ud.rBRUSH_SZ > 0
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud);
        end
        
        repaint();
        
      case 'set_channel'
        ud.rCHANNEL = cb_setchannel(ud.rCHANNEL);
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud);
        
      case 'load_anntime'
        ud.rLOAD_ANNTIME = ~ ud.rLOAD_ANNTIME;
        
        menu1 = 'Options';
        menu2 = 'Load annotations with time series';
        switch ud.rLOAD_ANNTIME
          case 0
            status = 'off';
          otherwise
            status = 'on';
        end
        
        cb_setmenustatus(fig, menu1, menu2, status);
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud);
        
      case 'save_geom'
        ud.rSAVE_GEOM = ~ ud.rSAVE_GEOM;
        
        menu1 = 'Options';
        menu2 = 'Save geometries after segmentation';
        switch ud.rSAVE_GEOM
          case 0
            status = 'off';
          otherwise
            status = 'on';
        end
        
        cb_setmenustatus(fig, menu1, menu2, status);
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud);
        
      case 'nodes_edges'
        ud.rNODESANDEDGES = ~ ud.rNODESANDEDGES;
        
        menu1 = 'Options';
        menu2 = 'Display geometry in new window after segmentation';
        switch ud.rNODESANDEDGES
          case 0
            status = 'off';
          otherwise
            status = 'on';
        end
        
        cb_setmenustatus(fig, menu1, menu2, status);
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud);
        
      case 'draw_all'
        ud.rDRAW_ALL = ~ ud.rDRAW_ALL;
        
        menu1 = 'Options';
        menu2 = 'Draw in all frames';
        switch ud.rDRAW_ALL
          case 0
            status = 'off';
          otherwise
            status = 'on';
        end
        
        cb_setmenustatus(fig, menu1, menu2, status);
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud);
        
        
      case {'astar_config'}
        p = dialog_astar(ud.astar_handles);
        ud.astar_handles = p;
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud);
      
      case {'switch_ch'; 'k_s'}
        cb_switchchannel(fig);
        
      case {'crop_im'; 'k_P'}
        ud = cb_cropimage(fig, [300 300]);
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud);
        repaint();
        
      case {'proj_im'; 'k_J'}
        [tps ptype] = dialog_projection(ud, cb_projectimage);
        
        if tps(1) >= 0
            ud = cb_projectimage(fig, [tps(1) tps(end)], ptype);
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud);
            
            repaint();
        end
        
      case {'sum_ch'; 'k_+'}
        ud = cb_sumchannels(fig);
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud);
        repaint();
        
      case {'reg_fr'; 'k_R'}
        cb_registerframes(fig);
        repaint();

        
        % ANNOTATION CALLBACKS -------------------------------------------------------
        
      case 'paint_pix'
        figure(fig);
        set(fig, 'WindowButtonDownFcn', 'mycallbacks2(gcbf, ''paint_pix_down'');');
        set(fig, 'WindowButtonUpFcn', 'mycallbacks2(gcbf, ''paint_pix_up'');');
        set(fig, 'Pointer', 'crossh');
        
      case 'paint_pix_down'
        if ~isempty(ud.rPIX_VALUE) & isempty(ud.rchannels)
            cb_setpixvalue(gcbf, ud.rPIX_VALUE(end), ud.rBRUSH_SZ);
            set(fig, 'WindowButtonMotionFcn', 'show_coord_pix(gcf); mycallbacks2(gcbf, ''paint_pix_down'');');
        elseif isempty(ud.rchannels)
            msgbox('Select a grayscale level first in the Options menu.', 'Paint pixels');
        else
            msgbox('Sorry, I can only paint grayscale images.', 'Paint pixels');
        end
            
    case 'paint_pix_up'
        set(fig, 'WindowButtonMotionFcn', 'show_coord_pix(gcf);');
        
    case 'select_mode'
        figure(fig);
        set(fig, 'WindowButtonDownFcn', 'mycallbacks2(gcbf, ''select_acell'');');
        set(fig, 'WindowButtonUpFcn', '');
        set(fig, 'WindowButtonMotionFcn', 'show_coord_pix(gcf);');
        set(fig, 'Pointer', 'crossh');
        
    case 'select_acell'
        [tmp, ud.rrcoords, ud.rmcoords] = cb_getcoords(fig);
        
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud); % If you do this at the end of this "case", lineh property
        % in udata is not properly stored.
        
        % Select with left button, unselect with right button.
        
        % Left-click.
        if isempty(ud.rrcoords) & isempty(ud.rmcoords)
            ud.rpoly = cb_selectcell(fig, cb_getpixvalue(fig));
            
            if ~ isempty(ud.rpoly)
                % Store in in the first empty spot.
                myind = 1;
                for i = 1:size(ud.rpolygons, 1)
                    if (isempty(ud.rpolygons{i, 1, ud.curslice+1}))
                        ud.rpolygons{i, 1, ud.curslice + 1} = ud.rpoly;
                        break;
                    end
                    myind = myind + 1;
                end
                
                % If there were no empty spots, store at the end.
                if (myind > size(ud.rpolygons, 1))
                    ud.rpolygons{end+1, 1, ud.curslice + 1} = ud.rpoly;
                end
                
                %ud.rtmp = cb_clearlines(fig, ud.rtmp);
                
                ud.rpoly = [];
                ud.lineh = [];
                
                set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
                set(fig, 'UserData', ud);
                
                cb_drawpolygon(fig, ud.rpolygons{myind, 1, ud.curslice + 1}, poly_sz, poly_color);
            end
            % Right-click.
        elseif isempty(ud.rmcoords)
            ud.rpolygons = cb_deleteclickedpolyline(fig, ud.rpolygons);
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud);
        end
        
    case {'toggle_ann'; 'k_h'}
        cb_showhideannotations(fig);
        
    case {'fiducial_mark'; 'k_f'}
        figure(fig);
        set(fig, 'WindowButtonDownFcn', 'mycallbacks2(gcbf, ''fiducial_mark_down'');');
        set(fig, 'WindowButtonUpFcn', '');
        set(fig, 'WindowButtonMotionFcn', 'show_coord_pix(gcf);');
        set(fig, 'Pointer', 'crossh');
        
    case 'fiducial_mark_down'
        coords = cb_getcoords(fig);
        % If the right button was used to delete, coords is set to
        % an empty vector and nothing else should be done.
        if ~isempty(coords)
            [ud.rfiducials ud.rnfiducials] = cb_storecurrentpoint(coords, ud.rfiducials, ud.rnfiducials);
            cb_drawpoint(fig, coords, fiducial_sz, fiducial_color, 'Fiducial');
            %disp(coords);
            cb_paintfiduciallabels(fig, ud.rfiducials, fiducial_label_sz, fiducial_color, ud.rflag_fiduciallabels);
            
        else
            % Erase from screen and delete from list.
            % cb_erasepoints only removes from screen (used to repaint).
            [ud.rfiducials ud.rnfiducials] = cb_deletepoint(fig, ud.rfiducials, ud.rnfiducials);
            cb_paintfiduciallabels(fig, ud.rfiducials, fiducial_label_sz, fiducial_color, ud.rflag_fiduciallabels);
            figure(fig);
            %set(fig, 'WindowButtonMotionFcn', 'mycallbacks2(get(gcbf, ''User Data''), ''fiducial_mark_down'');');
            %set(fig, 'WindowButtonUpFcn', 'mycallbacks2(get(gcbf, ''User Data''), ''fiducial_mark'');');
        end
        [dummy n] = cb_consolidatefiducials(ud.rfiducials(:, :, ud.curslice+1));       ud.rfiducials(:, :, ud.curslice+1) = dummy;
        ud.rnfiducials(ud.curslice+1) = n;
        fprintf('%d fiducials in time point %d\n', n, ud.curslice);
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud);
        
        
    case {'del_fidu_poly', 'k_e'}
        figure(fig);
        set(fig, 'WindowButtonDownFcn', 'mycallbacks2(gcbf, ''del_fidu_poly_down'');');
        set(fig, 'WindowButtonUpFcn', '');
        set(fig, 'WindowButtonMotionFcn', 'show_coord_pix(gcf);');
        set(fig, 'Pointer', 'crossh');  
        
    case 'del_fidu_poly_down'
        [ud.rfiducials ud.rnfiducials] = cb_deletepointspolyline(fig, ud.rfiducials, ud.rnfiducials, 0);
        cb_paintfiduciallabels(fig, ud.rfiducials, fiducial_label_sz, fiducial_color, ud.rflag_fiduciallabels);
        
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud);
        
    case {'track_fidu', 'k_T'}
        [tpo tpf] = dialog_timepoints([0 (ud.imsize(3)-1)]);
        
        if tpo ~= -1
            % tpo and tpf start at 0.
            ud = cb_trackfiducials(fig, tpo+1, tpf+1, 12);
            cb_paintfiduciallabels(fig, ud.rfiducials, fiducial_label_sz, fiducial_color, ud.rflag_fiduciallabels);
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud);
        end
        
    case 'track_poly'
        [tpo tpf] = dialog_timepoints([0 (ud.imsize(3)-1)]);
        
        if tpo ~= -1
            % tpo and tpf start at 0.
            ud = cb_trackpolylines(fig, tpo+1, tpf+1, 12);
            cb_paintfiduciallabels(fig, ud.rfiducials, fiducial_label_sz, fiducial_color, ud.rflag_fiduciallabels);
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud);
        end
        
    case {'track_poly_area', 'k_%'}
        [tpo tpf] = dialog_timepoints([0 (ud.imsize(3)-1)], [ud.curslice+1 ud.curslice+1]);
        
        if tpo ~= -1
            % tpo and tpf start at 0.
            ud = cb_trackpolylinesarea(fig, tpo+1, tpf+1, 12);
            cb_paintfiduciallabels(fig, ud.rfiducials, fiducial_label_sz, fiducial_color, ud.rflag_fiduciallabels);
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud);
        end
        
    case {'track_poly_div', 'k_$'}
        [tpo tpf] = dialog_timepoints([0 (ud.imsize(3)-1)], [ud.curslice+1 ud.curslice+1]);
        
        if tpo ~= -1
            % tpo and tpf start at 0.
            %ud = cb_trackpolylinesarea(fig, tpo+1, tpf+1, 12);
            ud = automateAll(ud, tpo+1, tpf+1);
            
            cb_paintfiduciallabels(fig, ud.rfiducials, fiducial_label_sz, fiducial_color, ud.rflag_fiduciallabels);
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud);
        end
        
    case {'poly_2_fidu'}
        [tpo tpf] = dialog_timepoints([0 (ud.imsize(3)-1)], [ud.curslice+1 ud.curslice+1]);
        
        if tpo ~= -1
            % tpo and tpf start at 0.
            [ud.rfiducials ud.rnfiducials] = cb_polygoncentroids(fig, tpo+1, tpf+1);            
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud);
            repaint();

        end
  
  case {'copy_fidu', 'k_C'}
        %                  [tpo tpf] = dialog_timepoints([0 (ud.imsize(3)-1)]);
        %
        %                  if tpo ~= -1
        %                          % tpo and tpf start at 0.
        %                          ud = cb_copyfiducials(fig, tpo+1, tpf+1, ud.curslice+1);
        %                          cb_paintfiduciallabels(fig, ud.rfiducials, fiducial_label_sz, fiducial_color, ud.rflag_fiduciallabels);
        %                          set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        %                          set(fig, 'UserData', ud);
        %                  end
        ud.paramtpo = ud.curslice;
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud);
        
    case {'paste_fidu', 'k_V'}
        if ud.paramtpo > -1
            % tpo and tpf start at 0.
            ud = cb_copyfiducials(fig, ud.curslice+1, ud.curslice+1, ud.paramtpo+1);
            cb_paintpoints(fig, ud.rfiducials, fiducial_sz, fiducial_color, ud.rselectedfiducials, selected_fiducial_color);
            cb_paintfiduciallabels(fig, ud.rfiducials, fiducial_label_sz, fiducial_color, ud.rflag_fiduciallabels);
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud);
        end
        
    case 'move_ann'
        figure(fig);
        set(fig, 'WindowButtonDownFcn', 'mycallbacks2(gcbf, ''move_ann_down'');');
        set(fig, 'Pointer', 'crossh');
        
    case 'move_ann_down'
        % Delete line to redraw it at the next position.
        [tmp ud.rtmp] = cb_deleteclickedpolyline(fig, ud.rpolygons);
        
        if ud.rtmp ~= -1
            ud.rlcoords = cb_getcoords(fig);
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud); % If you do this at the end of this "case", lineh property
            % in udata is not properly stored.
            
            % If the right button was used to delete a line, coords is set to
            % an empty vector and nothing else should be done.
            % Here we look for the clicked polygon in ud.rpolygons.
            if ~isempty(ud.rlcoords)
                % At every point in time while we move the polygon, ud.rpoly contains the current polygon coordinates.
                ud.rpoly = ud.rpolygons{ud.rtmp, 1, ud.curslice + 1};
                
                if ~ isempty(ud.rpoly)
                    % Found it!
                    
                    % If we move the mouse, move the annotations.
                    set(fig, 'WindowButtonMotionFcn', 'show_coord_pix(gcf); mycallbacks2(gcbf, ''ann_move'');');
                    set(fig, 'WindowButtonUpFcn', 'mycallbacks2(gcbf, ''ann_up_move'');');
                    
                    % Moving the annotations consists on deleting the polygon (done above), and replotting the polygon and the fiducials (paintpoints erases the old fiducials, drawpolygon does not erase the old polygon).
                    cb_drawpolygon(fig, ud.rpoly, poly_sz, poly_color);
                    cb_paintpoints(fig, ud.rfiducials, fiducial_sz, fiducial_color, ud.rselectedfiducials, selected_fiducial_color);
                    set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
                    set(fig, 'UserData', ud);
                end
            end
        end
        
    case 'ann_move'
        pnt = cb_getcoords(fig); % Current position of the mouse.
        % At every point in time while we move the polygon, ud.rpoly contains the current polygon coordinates. pnt-ud.rlcoords is the shift between the point where we were last (ud.rlcoords, updated below) and the point where the cursor is now.
        ud.rpoly(:, 1) = ud.rpoly(:, 1) + pnt(1) - ud.rlcoords(1);
        ud.rpoly(:, 2) = ud.rpoly(:, 2) + pnt(2) - ud.rlcoords(2);
        
        % The original position of the polygon is still in ud.rpolygons.
        original_poly = ud.rpolygons{ud.rtmp, 1, ud.curslice + 1};
        
        % We shift the positions of the fiducials (which have not been modified yet) by as much as the polygon has moved.
        fidu = cb_shiftannotationspolyline(fig, ud.rfiducials, ud.rnfiducials, 1, original_poly, [(ud.rpoly(1, 1) - original_poly(1, 1)) (ud.rpoly(1, 2) - original_poly(1, 2)) 0]);
        
        % Now delete de old polygon.
        cb_deleteclickedpolyline(fig, ud.rpolygons);
        
        % Update the "initial" coords to the "current" coords.
        ud.rlcoords = pnt;
        
        % Draw polygon and the shifted points.
        cb_drawpolygon(fig, ud.rpoly, poly_sz, poly_color);
        cb_paintpoints(fig, fidu, fiducial_sz, fiducial_color, ud.rselectedfiducials, selected_fiducial_color);
        
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud);
        
    case 'ann_up_move'
        set(fig, 'WindowButtonMotionFcn', 'show_coord_pix(gcf);');
        pnt = cb_getcoords(fig);
        % At every point in time while we move the polygon, ud.rpoly contains the current polygon coordinates. pnt-ud.rlcoords is the shift between the point where we were last (ud.rlcoords, updated below) and the point where the cursor is now.
        ud.rpoly(:, 1) = ud.rpoly(:, 1) + pnt(1) - ud.rlcoords(1);
        ud.rpoly(:, 2) = ud.rpoly(:, 2) + pnt(2) - ud.rlcoords(2);
        
        % The original position of the polygon is still in ud.rpolygons.
        original_poly = ud.rpolygons{ud.rtmp, 1, ud.curslice + 1};
        
        % We shift the positions of the fiducials (which have not been modified yet) by as much as the polygon has moved. NOW WE STORE THE SHIFTED FIDUCIAL POSITIONS!!!
        ud.rfiducials = cb_shiftannotationspolyline(fig, ud.rfiducials, ud.rnfiducials, 1, original_poly, [(ud.rpoly(1, 1) - original_poly(1, 1)) (ud.rpoly(1, 2) - original_poly(1, 2)) 0]);
        
        % Draw shifted fiducials.
        cb_paintpoints(fig, ud.rfiducials, fiducial_sz, fiducial_color, ud.rselectedfiducials, selected_fiducial_color);
        
        % Update position of the polygon.
        ud.rpolygons{ud.rtmp, 1, ud.curslice + 1} = ud.rpoly;
        ud.rpoly = [];
        
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud);
        
    case {'delim_mark'; 'k_d'}
        figure(fig);
        set(fig, 'WindowButtonDownFcn', 'mycallbacks2(gcbf, ''delim_mark_down'');');
        set(fig, 'WindowButtonUpFcn', '');
        set(fig, 'WindowButtonMotionFcn', 'show_coord_pix(gcf);');
        set(fig, 'Pointer', 'crossh');
        
    case 'delim_mark_down'
        coords = cb_getcoords(fig);
        
        % If the right button was used to delete, coords is set to
        % an empty vector and nothing else should be done.
        if ~isempty(coords)
            [ud.rdelimiters ud.rndelimiters] = cb_storecurrentpoint(coords, ud.rdelimiters, ud.rndelimiters);
            cb_drawpoint(fig, coords, delimiter_sz, delimiter_color, 'Delimiter');
            %disp(coords);
        else
            % Erase from screen and delete from list.
            % cb_erasepoints only removes from screen (used to repaint).
            [ud.rdelimiters ud.rndelimiters] = cb_deletedelimiter(fig, ud.rdelimiters, ud.rndelimiters);
        end
        
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud);
        
    case {'unique_mark'; 'k_u'}
        figure(fig);
        set(fig, 'WindowButtonDownFcn', 'mycallbacks2(gcbf, ''unique_mark_down'');');
        set(fig, 'WindowButtonUpFcn', '');
        set(fig, 'WindowButtonMotionFcn', 'show_coord_pix(gcf);');
        set(fig, 'Pointer', 'crossh');
        
    case 'unique_mark_down'
        coords = cb_getcoords(fig);
        
        % If the right button was used to delete, coords is set to
        % an empty vector and nothing else should be done.
        if ~isempty(coords)
            ud.runiquecoords(coords(3) + 1, :) = coords(1:2);
            cb_eraseunique(fig);
            cb_drawpoint(fig, coords, fiducial_sz, unique_color, 'Unique');
            %disp(coords);
        else
            % Delete from list and erase from screen.
            ud.runiquecoords(ud.curslice + 1, :) = [-1 -1];
            cb_eraseunique(fig);
        end
        
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud);
        
    case 'delete_mode'
        figure(fig);
        set(fig, 'WindowButtonDownFcn', 'mycallbacks2(gcbf, ''delete_cell'');');
        set(fig, 'WindowButtonUpFcn', '');
        set(fig, 'WindowButtonMotionFcn', 'show_coord_pix(gcf);');
        set(fig, 'Pointer', 'crossh');
        
    case 'delete_cell'
        [ud.rlcoords ud.rrcoords] = cb_getcoords(fig);
        
        %Left click deletes cells.
        if ~isempty(ud.rlcoords)
            cb_deletecell(fig, cb_getpixvalue(fig));
            
            % Right click prints de cell id in the figure title.
        elseif ~isempty(ud.rrcoords)
            set(fig, 'Name', cat(2, 'Cell id: ', num2str(cb_getpixvalue(fig))));
            figure(fig);
        end
        
                
        %  	case {'pline_mask', 'k_M'}
        %  		set(fig, 'WindowButtonDownFcn', 'mycallbacks2(gcbf, ''pline_mask_down'');');
        %  		set(fig, 'Pointer', 'crossh');
        %  
        %  	case 'pline_mask_down'
        %  		ud.rlcoords = cb_getcoords(fig);
        %  		set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        %                  set(fig, 'UserData', ud); % If you do this at the end of this "case", lineh property
        %  					  % in udata is not properly stored.
        %  		
        %  		% If the right button was used to delete a line, coords is set to
        %  		% an empty vector and nothing else should be done.
        %  		if ~isempty(ud.rlcoords)
        %  			cb_drawline(fig, ud.rlcoords, cb_getcoords(fig), ud.rBRUSH_SZ, line_color);
        %  			%disp(coords);
        %  			set(fig, 'WindowButtonMotionFcn', 'mycallbacks2(gcbf, ''pline_mask_motion'');');
        %  			set(fig, 'WindowButtonUpFcn', 'mycallbacks2(gcbf, ''pline_mask_up'');');
        %  			
        %  		else
        %  			ud.rlns = cb_deleteclickedline(fig, ud.rlns);
        %  			set(fig, 'WindowButtonUpFcn', '');	
        %  			set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        %                          set(fig, 'UserData', ud);		
        %  		end
        %  	
        %  		
        %  	case 'pline_mask_motion'
        %  		cb_deleteline(fig);
        %  		cb_drawline(fig, ud.rlcoords, cb_getcoords(fig), ud.rBRUSH_SZ, line_color);
        %  		
        %  	case 'pline_mask_up'
        %  		set(fig, 'WindowButtonMotionFcn', '');
        %  		%ang = cb_getlineangle(gcbf);
        %  		%disp(cat(2, 'Angle: ', num2str(ang * 180 / pi)));
        %  		%disp(cat(2, 'Intensity: ', num2str(cb_getlineintensity(gcbf, ang, CHANNEL, BRUSH_SZ))));
        %  		ud.rlns = cb_storecurrentline(fig, ud.rlns);
        %  		set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        %                  set(fig, 'UserData', ud);		

    case 'spoly_line'
        figure(fig);
        set(fig, 'WindowButtonDownFcn', '');
        set(fig, 'WindowButtonMotionFcn', 'show_coord_pix(gcf);');
        set(fig, 'WindowButtonUpFcn', 'mycallbacks2(gcbf, ''spoly_line_up'');');
        set(fig, 'Pointer', 'crossh');
        
    case 'spoly_line_up'
        [ud.rlcoords ud.rrcoords ud.rmcoords] = cb_getcoords(fig);
        % Left click.
        if (~isempty(ud.rlcoords))
            % Make sure that the polygon does not touch the edge of the image.
            if (ud.rlcoords(1) == 0) ud.rlcoords(1) = 1; end
            if (ud.rlcoords(2) == 0) ud.rlcoords(2) = 1; end
            
            ud.rpoly(end+1, :) = ud.rlcoords;
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud); % If you do this at the end of this "case", lineh property
            % in udata is not properly stored.
            
            tmp = cb_getcoords(fig);
            % Make sure that the polygon does not touch the edge of the image.
            if (tmp(1) == 0) tmp(1) = 1; end
            if (tmp(2) == 0) tmp(2) = 1; end
            cb_drawlinepol(fig, ud.rlcoords, tmp, ud.rBRUSH_SZ, poly_color);
            
            set(fig, 'WindowButtonMotionFcn', 'show_coord_pix(gcf); mycallbacks2(gcbf, ''spoly_line_motion'');');
            set(fig, 'WindowButtonUpFcn', 'mycallbacks2(gcbf, ''spoly_line_up2'');');
            set(fig, 'WindowButtonDownFcn', '');
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud);
            % Right button can be used to delete selections when not drawing them.
        elseif (~isempty(ud.rrcoords))
            ud.rpolygons = cb_deleteclickedpolyline(fig, ud.rpolygons);
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud);
        end
        
    case 'spoly_line_up2'
        %disp('Click!!');
        %ud.rlcoords
        [tmp, ud.rrcoords, ud.rmcoords] = cb_getcoords(fig);
        % Make sure that the polygon does not touch the edge of the image.
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud); % If you do this at the end of this "case", lineh property
        % in udata is not properly stored.
        
        % The right button is used to close the polyline. Then ud.rcoords is set to
        % an empty vector. The middle button is used to delete the last segment.
        
        % Left-click.
        if isempty(ud.rrcoords) & isempty(ud.rmcoords)
            % Make sure that the polygon does not touch the edge of the image.
            if (tmp(1) == 0) tmp(1) = 1; end
            if (tmp(2) == 0) tmp(2) = 1; end
            cb_drawlinepol(fig, ud.rlcoords, tmp, ud.rBRUSH_SZ, poly_color);
            
            ud = get(fig, 'UserData');
            ud.rlcoords = tmp;
            ud.rtmp = cb_storecurrentlinepol(fig, ud.rtmp);
            
            ud.rpoly(end+1, :) = ud.rlcoords;
            %disp(coords);
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud);
            
            % Right-click.
        elseif isempty(ud.rmcoords)
            ud = get(fig, 'UserData');
            
            cb_deletelinepol(fig);
            
            
            % Close polygon
            ud.rpoly(end+1, :) = ud.rpoly(1, :);
            
            % Store in in the first empty spot.
            myind = 1;
            for i = 1:size(ud.rpolygons, 1)
                if (isempty(ud.rpolygons{i, 1, ud.curslice+1}))
                    ud.rpolygons{i, 1, ud.curslice + 1} = ud.rpoly;
                    break;
                end
                myind = myind + 1;
            end
            
            % If there were no empty spots, store at the end.
            if (myind > size(ud.rpolygons, 1))
                ud.rpolygons{end+1, 1, ud.curslice + 1} = ud.rpoly;
            end
            
            
            ud.rtmp = cb_clearlines(fig, ud.rtmp);
            
            ud.rpoly = [];
            ud.lineh = [];
            
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud);
            set(fig, 'WindowButtonDownFcn', '');
            set(fig, 'WindowButtonMotionFcn', 'show_coord_pix(gcf);');
            set(fig, 'WindowButtonUpFcn', 'mycallbacks2(gcbf, ''spoly_line_up'');');
            
            cb_drawpolygon(fig, ud.rpolygons{myind, 1, ud.curslice + 1}, poly_sz, poly_color);
            
            % Middle click.
        else
            if (numel(ud.rtmp >= 2))
                % Deletes the line currently being drawn.
                cb_deletelinepol(fig);
                
                % Deletes the previously drawn line.
                cb_deletelinepol(fig);
                
                ud.rtmp = ud.rtmp(1:(end - 1));
                ud.lineh = ud.rtmp(end);
                ud.rpoly = ud.rpoly(1:(end-1), :);
                ud.rlcoords = ud.rpoly(end, :);
                set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
                set(fig, 'UserData', ud);
                
                cb_drawlinepol(fig, ud.rlcoords, ud.rmcoords, ud.rBRUSH_SZ, poly_color);
            end
        end
        
    case 'spoly_line_motion'
        %ud1 = get(gcf, 'UserData');
        %disp('ud1');
        %get(ud1.lineh)
        
        cb_deletelinepol(fig);
        
        [moveto1 moveto2 moveto3] = cb_getcoords(fig);
        if (isempty(moveto1) & isempty(moveto2))
            moveto = moveto3;
        elseif isempty(moveto1)
            moveto = moveto2;
        else
            moveto = moveto1;
        end
        
        cb_drawlinepol(fig, ud.rlcoords, moveto, ud.rBRUSH_SZ, poly_color);
        
    case 'draw_ang'
        figure(fig);
        set(fig, 'WindowButtonDownFcn', '');
        set(fig, 'WindowButtonMotionFcn', 'show_coord_pix(gcf);');
        set(fig, 'WindowButtonUpFcn', 'mycallbacks2(gcbf, ''draw_ang_up'');');
        set(fig, 'Pointer', 'crossh');
        
    case 'draw_ang_up'
        [ud.rlcoords ud.rrcoords ud.rmcoords] = cb_getcoords(fig);
        % Left click.
        if (~isempty(ud.rlcoords))
            ud.rang(end+1, :) = ud.rlcoords;
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud); % If you do this at the end of this "case", lineh property
            % in udata is not properly stored.
            
            cb_drawlinepol(fig, ud.rlcoords, cb_getcoords(fig), ud.rBRUSH_SZ, ang_color);
            
            set(fig, 'WindowButtonMotionFcn', 'show_coord_pix(gcf); mycallbacks2(gcbf, ''draw_ang_motion'');');
            set(fig, 'WindowButtonUpFcn', 'mycallbacks2(gcbf, ''draw_ang_up2'');');
            set(fig, 'WindowButtonDownFcn', '');
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud);
            % Right button can be used to delete polygons when not drawing them.
        elseif (~isempty(ud.rrcoords))
            ud.rangles = cb_deleteclickedpolyline(fig, ud.rangles);
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud);
        end
        
    case 'draw_ang_up2'
        %disp('Click!!');
        %ud.rlcoords
        [tmp, ud.rrcoords, ud.rmcoords] = cb_getcoords(fig);
        
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud); % If you do this at the end of this "case", lineh property
        % in udata is not properly stored.
        
        % Left-click.
        if isempty(ud.rrcoords) & isempty(ud.rmcoords)
            cb_drawlinepol(fig, ud.rlcoords, cb_getcoords(fig), ud.rBRUSH_SZ, ang_color);
            
            ud = get(fig, 'UserData');
            ud.rlcoords = tmp;
            ud.rtmp = cb_storecurrentlinepol(fig, ud.rtmp);
            
            ud.rang(end+1, :) = ud.rlcoords;
            %disp(coords);
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud);
        end
        
        % If three points have been clicked on, finish drawing.
        if mod(size(ud.rang, 1), 3) == 0
            %Rud = get(fig, 'UserData');
            
            cb_deletelinepol(fig);
            
            % Store in in the first empty spot.
            myind = 1;
            for i = 1:size(ud.rangles, 1)
                if (isempty(ud.rangles{i, 1, ud.curslice+1}))
                    ud.rangles{i, 1, ud.curslice + 1} = ud.rang;
                    break;
                end
                myind = myind + 1;
            end
            
            % If there were no empty spots, store at the end.
            if (myind > size(ud.rangles, 1))
                ud.rangles{end+1, 1, ud.curslice + 1} = ud.rang;
            end
            
            
            ud.rtmp = cb_clearlines(fig, ud.rtmp);
            
            ud.rang = [];
            ud.lineh = [];
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud);
            set(fig, 'WindowButtonDownFcn', '');
            set(fig, 'WindowButtonMotionFcn', 'show_coord_pix(gcf);');
            set(fig, 'WindowButtonUpFcn', 'mycallbacks2(gcbf, ''draw_ang_up'');');
            
            cb_drawpolygon(fig, ud.rangles{myind, 1, ud.curslice + 1}, poly_sz, ang_color);
            
            thepnts = ud.rangles{myind, :, ud.curslice+1};
            
            ang = measure_angle(thepnts(1, :), thepnts(2, :), thepnts(3, :));
            fprintf('\nAngle: %.2f', ang);
        end
        
    case 'draw_ang_motion'
        %ud1 = get(gcf, 'UserData');
        %disp('ud1');
        %get(ud1.lineh)
        
        cb_deletelinepol(fig);
        %
        [moveto1 moveto2 moveto3] = cb_getcoords(fig);
        if (isempty(moveto1) & isempty(moveto2))
            moveto = moveto3;
        elseif isempty(moveto1)
            moveto = moveto2;
        else
            moveto = moveto1;
        end
        
        cb_drawlinepol(fig, ud.rlcoords, moveto, ud.rBRUSH_SZ, ang_color);
        
    case {'measure_ang', 'k_H'}
        %[tpo tpf] = dialog_timepoints([0 (ud.imsize(3)-1)]);
        fprintf('\n');
        disp(cb_measureangles(fig));
        
    case 'input_roi'
        % Read ROI parameters.
        roi = dialog_inputroi();
        
        myind = 1;
        % Make sure there are no errors.
        if roi(1, 1) ~= -1
            x1 = roi(1, 1) - floor(roi(2, 1) / 2);
            x2 = roi(1, 1) + floor(roi(2, 1) / 2);
            y1 = roi(1, 2) - floor(roi(2, 2) / 2);
            y2 = roi(1, 2) + floor(roi(2, 2) / 2);
            
            ud.rpoly = [x1 y1; x1 y2; x2 y2; x2 y1; x1 y1];
            
            for i = 1:size(ud.rpolygons, 1)
                if (isempty(ud.rpolygons{i, 1, ud.curslice+1}))
                    ud.rpolygons{i, 1, ud.curslice + 1} = ud.rpoly;
                    break;
                end
                myind = myind + 1;
            end
            
            % If there were no empty spots, store at the end.
            if (myind > size(ud.rpolygons, 1))
                ud.rpolygons{end+1, 1, ud.curslice + 1} = ud.rpoly;
            end
            
            ud.rpoly = [];
            
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud);
            
            % Plot the roi as a polyline.
            cb_drawpolygon(fig, ud.rpolygons{myind, 1, ud.curslice + 1}, poly_sz, poly_color);
        end
        
  case 'k_z' 
    if isempty(ud.zoom) % This is necessary in case of stretch to fill, which turns the image into a zoom == [].
        ud.zoom = 1;
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud);
    else
        switch ud.zoom            
          case 1
            ud.zoom = 2;
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud);
            
          case 2
            ud.zoom = 3;
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!                            
            set(fig, 'UserData', ud);
          
          case 3
            ud.zoom = 4;
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!                            
            set(fig, 'UserData', ud);
          
          case 4
            ud.zoom = 1;
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!                            
            set(fig, 'UserData', ud);	
          
          otherwise 
            ud.zoom = 1;
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!                            
            set(fig, 'UserData', ud);             
        end
    end
    
    diptruesize(fig, 100*ud.zoom);
        
         
        % Open polylines or TRAJECTORIES.
        
    case {'opoly_line'; 'k_j'}
        figure(fig);
        set(fig, 'WindowButtonDownFcn', '');
        set(fig, 'WindowButtonMotionFcn', 'show_coord_pix(gcf);');
        set(fig, 'WindowButtonUpFcn', 'mycallbacks2(gcbf, ''opoly_line_up'');');
        set(fig, 'Pointer', 'crossh');
        
    case 'opoly_line_up'
        [ud.rlcoords ud.rrcoords ud.rmcoords] = cb_getcoords(fig);
        % Left click.
        if (~isempty(ud.rlcoords))
            % Make sure that the polygon does not touch the edge of the image.
            if (ud.rlcoords(1) == 0) ud.rlcoords(1) = 1; end
            if (ud.rlcoords(2) == 0) ud.rlcoords(2) = 1; end
            ud.rpoly(end+1, :) = ud.rlcoords;
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud); % If you do this at the end of this "case", lineh property
            % in udata is not properly stored.
            
            tmp = cb_getcoords(fig);
            % Make sure that the polygon does not touch the edge of the image.
            if (tmp(1) == 0) tmp(1) = 1; end
            if (tmp(2) == 0) tmp(2) = 1; end
            cb_drawlinepol(fig, ud.rlcoords, tmp, ud.rBRUSH_SZ, line_color);
            
            set(fig, 'WindowButtonMotionFcn', 'show_coord_pix(gcf); mycallbacks2(gcbf, ''opoly_line_motion'');');
            set(fig, 'WindowButtonUpFcn', 'mycallbacks2(gcbf, ''opoly_line_up2'');');
            set(fig, 'WindowButtonDownFcn', '');
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud);
            % Right button can be used to delete polygons when not drawing them.
        elseif (~isempty(ud.rrcoords))
            ud.rtrajectories = cb_deleteclickedpolyline(fig, ud.rtrajectories);
            
            % Only delete the trajectory if the click was on a trajectory.
            count = 0;
            oldtr = ud.rtrajectories(:, :, ud.curslice + 1);
            for ii = 1:numel(oldtr)
                if ~isempty(oldtr{ii})
                    count = count + 1;
                end
            end
            
            ud.rntrajectories(ud.curslice + 1) = count;
            
            cb_painttrajectorylabels(fig, ud.rtrajectories, tr_label_sz, line_color, ud.rflag_fiduciallabels);
            
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud);
        end
        
    case 'opoly_line_up2'
        %disp('Click!!');
        %ud.rlcoords
        [tmp, ud.rrcoords, ud.rmcoords] = cb_getcoords(fig);
        % Make sure that the polygon does not touch the edge of the image.
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud); % If you do this at the end of this "case", lineh property
        % in udata is not properly stored.
        
        % The right button is used to close the polyline. Then ud.rcoords is set to
        % an empty vector. The middle button is used to delete the last segment.
        
        % Left-click.
        if isempty(ud.rrcoords) & isempty(ud.rmcoords)
            % Make sure that the polygon does not touch the edge of the image.
            if (tmp(1) == 0) tmp(1) = 1; end
            if (tmp(2) == 0) tmp(2) = 1; end
            cb_drawlinepol(fig, ud.rlcoords, tmp, ud.rBRUSH_SZ, line_color);
            
            ud = get(fig, 'UserData');
            ud.rlcoords = tmp;
            ud.rtmp = cb_storecurrentlinepol(fig, ud.rtmp);
            
            ud.rpoly(end+1, :) = ud.rlcoords;
            %disp(coords);
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud);
            
            % Right-click ends drawing.
        elseif isempty(ud.rmcoords)
            ud = get(fig, 'UserData');
            
            cb_deletelinepol(fig);
            
            % Do not close polygon
            %ud.rpoly(end+1, :) = ud.rpoly(1, :);
            ud = get(fig, 'UserData');
            
            if ~ ud.rDRAW_ALL
                % Store in in the first empty spot.
                myind = 1;
                for i = 1:size(ud.rtrajectories, 1)
                    if (isempty(ud.rtrajectories{i, 1, ud.curslice+1}))
                        ud.rtrajectories{i, 1, ud.curslice + 1} = ud.rpoly;
                        ud.rntrajectories(ud.curslice + 1) = ud.rntrajectories(ud.curslice + 1) + 1;
                        break;
                    end
                    myind = myind + 1;
                end
                
                % If there were no empty spots, store at the end.
                if (myind > size(ud.rtrajectories, 1))
                    ud.rtrajectories{end+1, 1, ud.curslice + 1} = ud.rpoly;
                    ud.rntrajectories(ud.curslice + 1) = ud.rntrajectories(ud.curslice + 1) + 1;
                end
                
                theind = myind;
            else
                % Store in in the first empty   spot.
                for zz = 1:ud.imsize(3)
                    myind = 1;
                    for i = 1:size(ud.rtrajectories, 1)
                        if (isempty(ud.rtrajectories{i, 1, zz}))
                            ud.rtrajectories{i, 1, zz} = ud.rpoly;
                            break;
                        end
                        myind = myind + 1;
                    end
                    
                    % If there were no empty spots, store at the end.
                    if (myind > size(ud.rtrajectories, 1))
                        ud.rtrajectories{end+1, 1, zz} = ud.rpoly;
                        ud.rntrajectories(zz) = ud.rntrajectories(zz) + 1;
                    end
                    
                    if zz == (ud.curslice + 1)
                        theind = myind;
                    end
                end
            end
            
            ud.rtmp = cb_clearlines(fig, ud.rtmp);
            
            if ndims(ud.rntrajectories) > 2
                ud.rntrajectories = squeeze(ud.rntrajectories(:, :, 1));
            end
            
            ud.rpoly = [];
            ud.lineh = [];
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud);
            set(fig, 'WindowButtonDownFcn', '');
            set(fig, 'WindowButtonMotionFcn', 'show_coord_pix(gcf);');
            set(fig, 'WindowButtonUpFcn', 'mycallbacks2(gcbf, ''opoly_line_up'');');
            
            cb_drawpolygon(fig, ud.rtrajectories{theind, 1, ud.curslice + 1}, poly_sz, line_color);
            
            cb_painttrajectorylabels(fig, ud.rtrajectories, tr_label_sz, line_color, ud.rflag_fiduciallabels);
            
            % Middle click.
        else
            if (numel(ud.rtmp >= 2))
                % Deletes the line currently being drawn.
                cb_deletelinepol(fig);
                
                % Deletes the previously drawn line.
                cb_deletelinepol(fig);
                
                ud.rtmp = ud.rtmp(1:(end - 1));
                ud.lineh = ud.rtmp(end);
                ud.rpoly = ud.rpoly(1:(end-1), :);
                ud.rlcoords = ud.rpoly(end, :);
                set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
                set(fig, 'UserData', ud);
                
                cb_drawlinepol(fig, ud.rlcoords, ud.rmcoords, ud.rBRUSH_SZ, line_color);
            end
        end
        
    case 'opoly_line_motion'
        %ud1 = get(gcf, 'UserData');
        %disp('ud1');
        %get(ud1.lineh)
        
        cb_deletelinepol(fig);
        
        [moveto1 moveto2 moveto3] = cb_getcoords(fig);
        if (isempty(moveto1) & isempty(moveto2))
            moveto = moveto3;
        elseif isempty(moveto1)
            moveto = moveto2;
        else
            moveto = moveto1;
        end
        
        cb_drawlinepol(fig, ud.rlcoords, moveto, ud.rBRUSH_SZ, line_color);
        
     case {'live_astar'; 'k_A'}
        ud.rpoly = [];
        set(fig, 'WindowButtonDownFcn', '');
        set(fig, 'WindowButtonMotionFcn', 'show_coord_pix(gcf);');
        set(fig, 'WindowButtonUpFcn', 'mycallbacks2(gcbf, ''astar_up'');');
        set(fig, 'Pointer', 'crossh');
        
    case 'astar_up'
        [ud.rlcoords ud.rrcoords ud.rmcoords] = cb_getcoords(fig);
        
        % Left click.
        if (~isempty(ud.rlcoords))
            % Make sure that the polygon does not touch the edge of the image.
            if (ud.rlcoords(1) == 0) ud.rlcoords(1) = 1; end
            if (ud.rlcoords(2) == 0) ud.rlcoords(2) = 1; end
            ud.rpoly(end+1, :) = ud.rlcoords;
            
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud); % If you do this at the end of this "case", lineh property
            % in udata is not properly stored.
            c = cb_getcoords(fig);
            % Make sure that the polygon does not touch the edge of the image.
            if (c(1) == 0) c(1) = 1; end
            if (c(2) == 0) c(2) = 1; end
            
            parameters = ud.astar_handles;
            p = astarimagesearchmex(double(ud.imagedata), ud.rlcoords(1) * size(ud.imagedata,1) + ud.rlcoords(2), c(1) * size(ud.imagedata,1) + c(2), parameters);

            path = zeros(numel(p),3);
            path(:,1) = floor(p(:)/size(ud.imagedata,1));
            path(:,2) = p(:)-(path(:,1)*size(ud.imagedata,1));
            path(:,3) = ud.rlcoords(3);
            path = flipud(path);
            if ~ isempty(path)
                cb_drawpolygon(fig, path, ud.rBRUSH_SZ, poly_color);
            end
            
            set(fig, 'WindowButtonMotionFcn', 'show_coord_pix(gcf); mycallbacks2(gcbf, ''astar_motion'');');
            set(fig, 'WindowButtonUpFcn', 'mycallbacks2(gcbf, ''astar_up2'');');
            set(fig, 'WindowButtonDownFcn', '');
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud);
            % Right button can be used to delete polygons when not drawing them.
        elseif (~isempty(ud.rrcoords))
            ud.rpolygons = cb_deleteclickedpolyline(fig, ud.rpolygons);
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud);
        end
        
        case 'astar_motion'
        
        cb_deletelinepol(fig);
        
        [moveto1 moveto2 moveto3] = cb_getcoords(fig);
        if (isempty(moveto1) && isempty(moveto2))
            moveto = moveto3;
        elseif isempty(moveto1)
            moveto = moveto2;
        else
            moveto = moveto1;
        end
        
        parameters = ud.astar_handles;
        p = astarimagesearchmex(double(ud.imagedata), ud.rlcoords(1) * size(ud.imagedata,1) + ud.rlcoords(2), moveto(1) * size(ud.imagedata,1) + moveto(2), parameters);
        path = zeros(numel(p),3);
        path(:,1) = floor(p(:)/size(ud.imagedata,1));
        path(:,2) = p(:)-(path(:,1)*size(ud.imagedata,1));
        path(:,3) = ud.rlcoords(3);
        path = flipud(path);
        if ~ isempty(path)
            cb_drawpolygon(fig, path, ud.rBRUSH_SZ, poly_color);
        end
        
        case 'astar_up2'
        %disp('Click!!');
        %ud.rlcoords
        [tmp, ud.rrcoords, ud.rmcoords] = cb_getcoords(fig);
        
        
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud); % If you do this at the end of this "case", lineh property
        % in udata is not properly stored.
        
        % The right button is used to close the polyline. Then ud.rcoords is set to
        % an empty vector. The middle button is used to delete the last segment.
        
        % Left-click.
        if isempty(ud.rrcoords) && isempty(ud.rmcoords)
            % Make sure that the polygon does not touch the edge of the image.
            if (tmp(1) == 0) tmp(1) = 1; end
            if (tmp(2) == 0) tmp(2) = 1; end
            c = cb_getcoords(fig);
            % Make sure that the polygon does not touch the edge of the image.
            if (c(1) == 0) c(1) = 1; end
            if (c(2) == 0) c(2) = 1; end
            
            parameters = ud.astar_handles;
            p = astarimagesearchmex(double(ud.imagedata), ud.rlcoords(1) * size(ud.imagedata,1) + ud.rlcoords(2), c(1) * size(ud.imagedata,1) + c(2), parameters);
            path = zeros(numel(p),3);
            path(:,1) = floor(p(:)/size(ud.imagedata,1));
            path(:,2) = p(:)-(path(:,1)*size(ud.imagedata,1));
            path(:,3) = ud.rlcoords(3);
            path = flipud(path);
            if ~ isempty(path)
                cb_drawpolygon(fig, path, ud.rBRUSH_SZ, poly_color);
                ud = get(fig, 'UserData');
                ud.rlcoords = tmp;
                % Store polyline handle in case it is necessary to delete it later.
                ud.rtmp = cb_storecurrentlinepol(fig, ud.rtmp);
                % Store coordinates of all the nodes along the polyline.
                ud.rpoly((end+1):(end+size(path,1)), :) = path;
                %disp(coords);
                set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
                set(fig, 'UserData', ud);
            end
            
            
            % Right-click.
        elseif isempty(ud.rmcoords)
            ud = get(fig, 'UserData');
            
            cb_deletelinepol(fig); 
            
            % Close polygon
            parameters = ud.astar_handles;
            p = astarimagesearchmex(double(ud.imagedata), ud.rrcoords(1) * size(ud.imagedata,1) + ud.rrcoords(2), ud.rpoly(1, 1) * size(ud.imagedata,1) + ud.rpoly(1, 2), parameters);
            path = zeros(numel(p),3);
            path(:,1) = floor(p(:)/size(ud.imagedata,1));
            path(:,2) = p(:)-(path(:,1)*size(ud.imagedata,1));
            path(:,3) = ud.rrcoords(3);
            path = flipud(path);
            
            if ~ isempty(path)
                ud.rpoly((end+1):(end+size(path,1)), :) = path;
            else
                ud.rpoly(end+1, :) = ud.rpoly(1, :);
            end
            
            if ~ ud.rDRAW_ALL
                % Store in in the first empty   spot.
                myind = 1;
                for i = 1:size(ud.rpolygons, 1)
                    if (isempty(ud.rpolygons{i, 1, ud.curslice+1}))
                        ud.rpolygons{i, 1, ud.curslice + 1} = ud.rpoly;
                        break;
                    end
                    myind = myind + 1;
                end
                
                % If there were no empty spots, store at the end.
                if (myind > size(ud.rpolygons, 1))
                    ud.rpolygons{end+1, 1, ud.curslice + 1} = ud.rpoly;
                end
                
                theind = myind;
            else
                % Store in in the first empty   spot.
                for zz = 1:ud.imsize(3)
                    myind = 1;
                    for i = 1:size(ud.rpolygons, 1)
                        if (isempty(ud.rpolygons{i, 1, zz}))
                            ud.rpolygons{i, 1, zz} = ud.rpoly;
                            break;
                        end
                        myind = myind + 1;
                    end
                    
                    % If there were no empty spots, store at the end.
                    if (myind > size(ud.rpolygons, 1))
                        ud.rpolygons{end+1, 1, zz} = ud.rpoly;
                    end
                    
                    if zz == (ud.curslice + 1)
                        theind = myind;
                    end
                end
            end
            
            
            ud.rtmp = cb_clearlines(fig, ud.rtmp);
            
            ud.rpoly = [];
            ud.lineh = [];
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud);
            set(fig, 'WindowButtonDownFcn', '');
            set(fig, 'WindowButtonMotionFcn', 'show_coord_pix(gcf);');
            set(fig, 'WindowButtonUpFcn', 'mycallbacks2(gcbf, ''astar_up'');');
            
            cb_drawpolygon(fig, ud.rpolygons{theind, 1, ud.curslice + 1}, poly_sz, poly_color);
            figure(fig);
            % Middle click.
        else
            if (numel(ud.rtmp >= 2))
                %                                  % Deletes the line currently being drawn.
                %                                  cb_deletelinepol(fig);
                %
                %                                  % Deletes the previously drawn line.
                %                                  cb_deletelinepol(fig);
                
                % ud.rtmp contains the list of fig for each drawn segment (added in storecurrentlinepol).
                % ud.rpoly contains the list of all the points drawn until now in the entire curve.
                % ud.lineh contains the handle to the last line drawn.
                if strcmp(get(get(fig, 'CurrentObject'),'Type'),'line')
                    
                    delete(get(fig, 'CurrentObject'));
                    
                    % The coordinates of the last good point in rpoly.
                    x = get(ud.rtmp(end), 'XData');
                    y = get(ud.rtmp(end), 'YData');
                    delete(ud.rtmp(end));
                    
                    ud.rtmp = ud.rtmp(1:(end - 1));
                    ud.lineh = ud.rtmp(end);
                    
                    ind = find(ud.rpoly(:, 1) == x(1) & ud.rpoly(:, 2) == y(1));
                    ud.rpoly = ud.rpoly(1:(ind), :);
                    ud.rlcoords = ud.rpoly(end, :);
                    set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
                    set(fig, 'UserData', ud);
                    
                    cb_drawlinepol(fig, ud.rlcoords, ud.rmcoords, ud.rBRUSH_SZ, poly_color);
                end
            end
        end
       

        
        
    case {'live_wire'; 'k_W'}
        ud.rpoly = [];
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud);
        figure(fig);
        set(fig, 'WindowButtonDownFcn', '');
        set(fig, 'WindowButtonMotionFcn', 'show_coord_pix(gcf);');
        set(fig, 'WindowButtonUpFcn', 'mycallbacks2(gcbf, ''lw_up'');');
        set(fig, 'Pointer', 'crossh');
        
    case 'lw_up'
        [ud.rlcoords ud.rrcoords ud.rmcoords] = cb_getcoords(fig);
        
        % Left click.
        if (~isempty(ud.rlcoords))
            % Make sure that the polygon does not touch the edge of the image.
            if (ud.rlcoords(1) == 0) ud.rlcoords(1) = 1; end
            if (ud.rlcoords(2) == 0) ud.rlcoords(2) = 1; end
            ud.rpoly(end+1, :) = ud.rlcoords;
            
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud); % If you do this at the end of this "case", lineh property
            % in udata is not properly stored.
            c = cb_getcoords(fig);
            % Make sure that the polygon does not touch the edge of the image.
            if (c(1) == 0) c(1) = 1; end
            if (c(2) == 0) c(2) = 1; end
            
            
            p = livewire(ud.imagedata, ud.rlcoords, c);
            if ~ isempty(p)
                cb_drawpolygon(fig, p, ud.rBRUSH_SZ, poly_color);
            end
            
            set(fig, 'WindowButtonMotionFcn', 'show_coord_pix(gcf); mycallbacks2(gcbf, ''lw_motion'');');
            set(fig, 'WindowButtonUpFcn', 'mycallbacks2(gcbf, ''lw_up2'');');
            set(fig, 'WindowButtonDownFcn', '');
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud);
            % Right button can be used to delete polygons when not drawing them.
        elseif (~isempty(ud.rrcoords))
            ud.rpolygons = cb_deleteclickedpolyline(fig, ud.rpolygons);
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud);
        end
        
    case 'lw_motion'
        
        cb_deletelinepol(fig);
        
        [moveto1 moveto2 moveto3] = cb_getcoords(fig);
        if (isempty(moveto1) & isempty(moveto2))
            moveto = moveto3;
        elseif isempty(moveto1)
            moveto = moveto2;
        else
            moveto = moveto1;
        end
        
        p = livewire(ud.imagedata, ud.rlcoords, moveto);
        if ~ isempty(p)
            cb_drawpolygon(fig, p, ud.rBRUSH_SZ, poly_color);
        end
        
    case 'lw_up2'
        %disp('Click!!');
        %ud.rlcoords
        [tmp, ud.rrcoords, ud.rmcoords] = cb_getcoords(fig);
        
        
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud); % If you do this at the end of this "case", lineh property
        % in udata is not properly stored.
        
        % The right button is used to close the polyline. Then ud.rcoords is set to
        % an empty vector. The middle button is used to delete the last segment.
        
        % Left-click.
        if isempty(ud.rrcoords) & isempty(ud.rmcoords)
            % Make sure that the polygon does not touch the edge of the image.
            if (tmp(1) == 0) tmp(1) = 1; end
            if (tmp(2) == 0) tmp(2) = 1; end
            c = cb_getcoords(fig);
            % Make sure that the polygon does not touch the edge of the image.
            if (c(1) == 0) c(1) = 1; end
            if (c(2) == 0) c(2) = 1; end
            
            p = livewire(ud.imagedata, ud.rlcoords, c);
            if ~ isempty(p)
                cb_drawpolygon(fig, p, ud.rBRUSH_SZ, poly_color);
                ud = get(fig, 'UserData');
                ud.rlcoords = tmp;
                % Store polyline handle in case it is necessary to delete it later.
                ud.rtmp = cb_storecurrentlinepol(fig, ud.rtmp);
                % Store coordinates of all the nodes along the polyline.
                ud.rpoly((end+1):(end+size(p,1)), :) = p;
                %disp(coords);
                set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
                set(fig, 'UserData', ud);
            end
            
            
            % Right-click.
        elseif isempty(ud.rmcoords)
            ud = get(fig, 'UserData');
            
            cb_deletelinepol(fig);
            
            % Close polygon
            p = livewire(ud.imagedata, ud.rrcoords, ud.rpoly(1, :));
            
            if ~ isempty(p)
                ud.rpoly((end+1):(end+size(p,1)), :) = p;
            else
                ud.rpoly(end+1, :) = ud.rpoly(1, :);
            end
            
            if ~ ud.rDRAW_ALL
                % Store in in the first empty   spot.
                myind = 1;
                for i = 1:size(ud.rpolygons, 1)
                    if (isempty(ud.rpolygons{i, 1, ud.curslice+1}))
                        ud.rpolygons{i, 1, ud.curslice + 1} = ud.rpoly;
                        break;
                    end
                    myind = myind + 1;
                end
                
                % If there were no empty spots, store at the end.
                if (myind > size(ud.rpolygons, 1))
                    ud.rpolygons{end+1, 1, ud.curslice + 1} = ud.rpoly;
                end
                
                theind = myind;
            else
                % Store in in the first empty   spot.
                for zz = 1:ud.imsize(3)
                    myind = 1;
                    for i = 1:size(ud.rpolygons, 1)
                        if (isempty(ud.rpolygons{i, 1, zz}))
                            ud.rpolygons{i, 1, zz} = ud.rpoly;
                            break;
                        end
                        myind = myind + 1;
                    end
                    
                    % If there were no empty spots, store at the end.
                    if (myind > size(ud.rpolygons, 1))
                        ud.rpolygons{end+1, 1, zz} = ud.rpoly;
                    end
                    
                    if zz == (ud.curslice + 1)
                        theind = myind;
                    end
                end
            end
            
            
            ud.rtmp = cb_clearlines(fig, ud.rtmp);
            
            ud.rpoly = [];
            ud.lineh = [];
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud);
            set(fig, 'WindowButtonDownFcn', '');
            set(fig, 'WindowButtonMotionFcn', 'show_coord_pix(gcf);');
            set(fig, 'WindowButtonUpFcn', 'mycallbacks2(gcbf, ''lw_up'');');
            
            cb_drawpolygon(fig, ud.rpolygons{theind, 1, ud.curslice + 1}, poly_sz, poly_color);
            figure(fig);
            % Middle click.
        else
            if (numel(ud.rtmp >= 2))
                %                                  % Deletes the line currently being drawn.
                %                                  cb_deletelinepol(fig);
                %
                %                                  % Deletes the previously drawn line.
                %                                  cb_deletelinepol(fig);
                
                % ud.rtmp contains the list of fig for each drawn segment (added in storecurrentlinepol).
                % ud.rpoly contains the list of all the points drawn until now in the entire curve.
                % ud.lineh contains the handle to the last line drawn.
                if strcmp(get(get(fig, 'CurrentObject'),'Type'),'line')
                    
                    delete(get(fig, 'CurrentObject'));
                    
                    % The coordinates of the last good point in rpoly.
                    x = get(ud.rtmp(end), 'XData');
                    y = get(ud.rtmp(end), 'YData');
                    delete(ud.rtmp(end));
                    
                    ud.rtmp = ud.rtmp(1:(end - 1));
                    ud.lineh = ud.rtmp(end);
                    
                    ind = find(ud.rpoly(:, 1) == x(1) & ud.rpoly(:, 2) == y(1));
                    ud.rpoly = ud.rpoly(1:(ind), :);
                    ud.rlcoords = ud.rpoly(end, :);
                    set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
                    set(fig, 'UserData', ud);
                    
                    cb_drawlinepol(fig, ud.rlcoords, ud.rmcoords, ud.rBRUSH_SZ, poly_color);
                end
            end
        end
        
    case 'deletetracked_poly_line'
        set(fig, 'WindowButtonDownFcn', '');
        set(fig, 'WindowButtonMotionFcn', '');
        set(fig, 'WindowButtonUpFcn', 'mycallbacks2(gcbf, ''deletetracked_poly_line_up'');');
        set(fig, 'Pointer', 'crossh');
        
    case 'deletetracked_poly_line_up'
        [ud.rlcoords ud.rrcoords ud.rmcoords] = cb_getcoords(fig);
        
        % Right click.
        if (~isempty(ud.rrcoords))
            [ud.rpolygons theindex] = cb_deleteclickedpolyline(fig, ud.rpolygons);
            
            for islice = 1:size(ud.rpolygons, 3)
                ud.rpolygons{theindex, 1, islice} = [];
            end
            
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud);
        end
        figure(fig);
        
    case {'ppoly_line'; 'k_y'}
        set(fig, 'WindowButtonDownFcn', '');
        set(fig, 'WindowButtonMotionFcn', 'show_coord_pix(gcf);');
        set(fig, 'WindowButtonUpFcn', 'mycallbacks2(gcbf, ''ppoly_line_up'');');
        set(fig, 'Pointer', 'crossh');
        
    case 'ppoly_line_up'
        [ud.rlcoords ud.rrcoords ud.rmcoords] = cb_getcoords(fig);
        
        % Left click.
        if (~isempty(ud.rlcoords))
            % Make sure that the polygon does not touch the edge of the image.
            if (ud.rlcoords(1) == 0) ud.rlcoords(1) = 1; end
            if (ud.rlcoords(2) == 0) ud.rlcoords(2) = 1; end
            ud.rpoly(end+1, :) = ud.rlcoords;
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud); % If you do this at the end of this "case", lineh property
            % in udata is not properly stored.
            
            [tmp, ud.rrcoords, ud.rmcoords] = cb_getcoords(fig);
            % Make sure that the polygon does not touch the edge of the image.
            if (tmp(1) == 0) tmp(1) = 1; end
            if (tmp(2) == 0) tmp(2) = 1; end
            
            cb_drawlinepol(fig, ud.rlcoords, tmp, ud.rBRUSH_SZ, poly_color);
            
            set(fig, 'WindowButtonMotionFcn', 'show_coord_pix(gcf); mycallbacks2(gcbf, ''ppoly_line_motion'');');
            set(fig, 'WindowButtonUpFcn', 'mycallbacks2(gcbf, ''ppoly_line_up2'');');
            set(fig, 'WindowButtonDownFcn', '');
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud);
            % Right button can be used to delete polygons when not drawing them.
        elseif (~isempty(ud.rrcoords))
            ud.rpolygons = cb_deleteclickedpolyline(fig, ud.rpolygons);
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud);
        end
        
    case 'ppoly_line_up2'
        %disp('Click!!');
        %ud.rlcoords
        [tmp, ud.rrcoords, ud.rmcoords] = cb_getcoords(fig);
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud); % If you do this at the end of this "case", lineh property
        % in udata is not properly stored.
        
        % The right button is used to close the polyline. Then ud.rcoords is set to
        % an empty vector. The middle button is used to delete the last segment.
        
        % Left-click.
        if isempty(ud.rrcoords) & isempty(ud.rmcoords)
            % Make sure that the polygon does not touch the edge of the image.
            if (tmp(1) == 0) tmp(1) = 1; end
            if (tmp(2) == 0) tmp(2) = 1; end
            
            cb_drawlinepol(fig, ud.rlcoords, tmp, ud.rBRUSH_SZ, poly_color);
            
            ud = get(fig, 'UserData');
            ud.rlcoords = tmp;
            ud.rtmp = cb_storecurrentlinepol(fig, ud.rtmp);
            
            ud.rpoly(end+1, :) = ud.rlcoords;
            %disp(coords);
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud);
            
            % Right-click.
        elseif isempty(ud.rmcoords)
            ud = get(fig, 'UserData');
            
            cb_deletelinepol(fig);
            
            
            % Close polygon
            ud.rpoly(end+1, :) = ud.rpoly(1, :);
            
            if ~ ud.rDRAW_ALL
                % Store in in the first empty 	spot.
                myind = 1;
                for i = 1:size(ud.rpolygons, 1)
                    if (isempty(ud.rpolygons{i, 1, ud.curslice+1}))
                        ud.rpolygons{i, 1, ud.curslice + 1} = ud.rpoly;
                        break;
                    end
                    myind = myind + 1;
                end
                
                % If there were no empty spots, store at the end.
                if (myind > size(ud.rpolygons, 1))
                    ud.rpolygons{end+1, 1, ud.curslice + 1} = ud.rpoly;
                end
                
                theind = myind;
            else
                % Store in in the first empty 	spot.
                for zz = 1:ud.imsize(3)
                    myind = 1;
                    for i = 1:size(ud.rpolygons, 1)
                        if (isempty(ud.rpolygons{i, 1, zz}))
                            ud.rpolygons{i, 1, zz} = ud.rpoly;
                            break;
                        end
                        myind = myind + 1;
                    end
                    
                    % If there were no empty spots, store at the end.
                    if (myind > size(ud.rpolygons, 1))
                        ud.rpolygons{end+1, 1, zz} = ud.rpoly;
                    end
                    
                    if zz == (ud.curslice + 1)
                        theind = myind;
                    end
                end
            end
            
            
            ud.rtmp = cb_clearlines(fig, ud.rtmp);
            
            ud.rpoly = [];
            ud.lineh = [];
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud);
            set(fig, 'WindowButtonDownFcn', '');
            set(fig, 'WindowButtonMotionFcn', 'show_coord_pix(gcf);');
            set(fig, 'WindowButtonUpFcn', 'mycallbacks2(gcbf, ''ppoly_line_up'');');
            
            cb_drawpolygon(fig, ud.rpolygons{theind, 1, ud.curslice + 1}, poly_sz, poly_color);
            
            % Middle click.
        else
            if (numel(ud.rtmp >= 2))
                % Deletes the line currently being drawn.
                cb_deletelinepol(fig);
                
                % Deletes the previously drawn line.
                cb_deletelinepol(fig);
                
                ud.rtmp = ud.rtmp(1:(end - 1));
                ud.lineh = ud.rtmp(end);
                ud.rpoly = ud.rpoly(1:(end-1), :);
                ud.rlcoords = ud.rpoly(end, :);
                set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
                set(fig, 'UserData', ud);
                
                cb_drawlinepol(fig, ud.rlcoords, ud.rmcoords, ud.rBRUSH_SZ, poly_color);
            end
        end
        
    case 'ppoly_line_motion'
        %ud1 = get(gcf, 'UserData');
        %disp('ud1');
        %get(ud1.lineh)
        
        cb_deletelinepol(fig);
        
        [moveto1 moveto2 moveto3] = cb_getcoords(fig);
        if (isempty(moveto1) & isempty(moveto2))
            moveto = moveto3;
        elseif isempty(moveto1)
            moveto = moveto2;
        else
            moveto = moveto1;
        end
        
        cb_drawlinepol(fig, ud.rlcoords, moveto, ud.rBRUSH_SZ, poly_color);
        
    case {'circle'; 'k_l'}
        figure(fig);
        set(fig, 'WindowButtonDownFcn', 'mycallbacks2(gcbf, ''circle_down'');');
        set(fig, 'WindowButtonMotionFcn', 'show_coord_pix(gcf);');
        set(fig, 'WindowButtonUpFcn', '');
        set(fig, 'Pointer', 'crossh');
        
    case 'circle_down'
        [ud.rlcoords, ud.rrcoords, ud.rmcoords] = cb_getcoords(fig);
        
        if (~ isempty(ud.rlcoords))
            set(fig, 'WindowButtonDownFcn', 'mycallbacks2(gcbf, ''circle_down'');');
            set(fig, 'WindowButtonMotionFcn', ' show_coord_pix(gcf); mycallbacks2(gcbf, ''circle_motion'');');
            set(fig, 'WindowButtonUpFcn', 'mycallbacks2(gcbf, ''circle_up'');');
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud);
            
            % IMPORTANT HERE. If not, the previously painted annotation
            % will be removed from the screen.
            cb_drawcircle(fig, ud.rlcoords, ud.rlcoords, ud.rBRUSH_SZ, poly_color);
            
        elseif (~isempty(ud.rrcoords))
            ud.rpolygons = cb_deleteclickedpolyline(fig, ud.rpolygons);
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud);
        end
        
    case 'circle_up'
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud); % If you do this at the end of this "case", lineh property
        % in udata is not properly stored.
        
        ud = get(fig, 'UserData');
        
        cb_deletelinepol(fig);
        
        % Close polygon
        [x y] = circle(ud.rlcoords(1), ud.rlcoords(2), norm(cb_getcoords(fig)-ud.rlcoords, 2), 256, 0);
        
        if ~ ud.rDRAW_ALL
            % Store in in the first empty 	spot.
            myind = 1;
            for i = 1:size(ud.rpolygons, 1)
                if (isempty(ud.rpolygons{i, 1, ud.curslice+1}))
                    ud.rpolygons{i, 1, ud.curslice + 1} = [x y];
                    break;
                end
                myind = myind + 1;
            end
            
            % If there were no empty spots, store at the end.
            if (myind > size(ud.rpolygons, 1))
                ud.rpolygons{end+1, 1, ud.curslice + 1} = [x y];
            end
            
            theind = myind;
        else
            % Store in in the first empty 	spot.
            for zz = 1:ud.imsize(3)
                myind = 1;
                for i = 1:size(ud.rpolygons, 1)
                    if (isempty(ud.rpolygons{i, 1, zz}))
                        ud.rpolygons{i, 1, zz} = [x y];
                        break;
                    end
                    myind = myind + 1;
                end
                
                % If there were no empty spots, store at the end.
                if (myind > size(ud.rpolygons, 1))
                    ud.rpolygons{end+1, 1, zz} = [x y];
                end
                
                if zz == (ud.curslice + 1)
                    theind = myind;
                end
            end
        end
        
        ud.rpoly = [];
        ud.lineh = [];
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud);
        set(fig, 'WindowButtonMotionFcn', 'show_coord_pix(gcf);');
        set(fig, 'WindowButtonUpFcn', '');
        
        cb_drawpolygon(fig, ud.rpolygons{theind, 1, ud.curslice + 1}, poly_sz, poly_color);
        
    case 'circle_motion'
        cb_deletelinepol(fig);
        cb_drawcircle(fig, ud.rlcoords, cb_getcoords(fig), ud.rBRUSH_SZ, poly_color);
        
    case {'rect'; 'k_r'}
        figure(fig);
        set(fig, 'WindowButtonDownFcn', 'mycallbacks2(gcbf, ''rect_down'');');
        set(fig, 'WindowButtonMotionFcn', 'show_coord_pix(gcf);');
        set(fig, 'WindowButtonUpFcn', '');
        set(fig, 'Pointer', 'crossh');
        
    case 'rect_down'
        [ud.rlcoords, ud.rrcoords, ud.rmcoords] = cb_getcoords(fig);
        
        if (~ isempty(ud.rlcoords))
            set(fig, 'WindowButtonDownFcn', 'mycallbacks2(gcbf, ''rect_down'');');
            set(fig, 'WindowButtonMotionFcn', 'show_coord_pix(gcf); mycallbacks2(gcbf, ''rect_motion'');');
            set(fig, 'WindowButtonUpFcn', 'mycallbacks2(gcbf, ''rect_up'');');
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud);
            
            % IMPORTANT HERE. If not, the previously painted annotation
            % will be removed from the screen.
            cb_drawrectangle(fig, ud.rlcoords, ud.rlcoords, ud.rBRUSH_SZ, poly_color);
            
        elseif (~isempty(ud.rrcoords))
            ud.rpolygons = cb_deleteclickedpolyline(fig, ud.rpolygons);
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud);
        end
        
    case 'rect_up'
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud); % If you do this at the end of this "case", lineh property
        % in udata is not properly stored.
        
        ud = get(fig, 'UserData');
        
        cb_deletelinepol(fig);
        
        % Close polygon
        curr_pnt = cb_getcoords(fig);
        x = [ud.rlcoords(1); curr_pnt(1); curr_pnt(1); ud.rlcoords(1); ud.rlcoords(1)];
        y = [ud.rlcoords(2); ud.rlcoords(2); curr_pnt(2); curr_pnt(2); ud.rlcoords(2)];
        
        if ~ ud.rDRAW_ALL
            % Store in in the first empty 	spot.
            myind = 1;
            for i = 1:size(ud.rpolygons, 1)
                if (isempty(ud.rpolygons{i, 1, ud.curslice+1}))
                    ud.rpolygons{i, 1, ud.curslice + 1} = [x y];
                    break;
                end
                myind = myind + 1;
            end
            
            % If there were no empty spots, store at the end.
            if (myind > size(ud.rpolygons, 1))
                ud.rpolygons{end+1, 1, ud.curslice + 1} = [x y];
            end
            
            theind = myind;
        else
            % Store in in the first empty 	spot.
            for zz = 1:ud.imsize(3)
                myind = 1;
                for i = 1:size(ud.rpolygons, 1)
                    if (isempty(ud.rpolygons{i, 1, zz}))
                        ud.rpolygons{i, 1, zz} = [x y];
                        break;
                    end
                    myind = myind + 1;
                end
                
                % If there were no empty spots, store at the end.
                if (myind > size(ud.rpolygons, 1))
                    ud.rpolygons{end+1, 1, zz} = [x y];
                end
                
                if zz == (ud.curslice + 1)
                    theind = myind;
                end
            end
        end
        
        ud.rpoly = [];
        ud.lineh = [];
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud);
        set(fig, 'WindowButtonMotionFcn', 'show_coord_pix(gcf);');
        set(fig, 'WindowButtonUpFcn', '');
        
        cb_drawpolygon(fig, ud.rpolygons{theind, 1, ud.curslice + 1}, poly_sz, poly_color);
        
    case 'rect_motion'
        cb_deletelinepol(fig);
        cb_drawrectangle(fig, ud.rlcoords, cb_getcoords(fig), ud.rBRUSH_SZ, poly_color);
        
    case 'clear_lines'
        ud.rlns = cb_clearlines(fig, ud.rlns);
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud);
        
        %  	case {'clear_dots_old'}
        %  		ud.rlns = cb_cleardots(fig, ud.rlns);
        %  		set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        %                  set(fig, 'UserData', ud);
        
    case 'clear_dots'
        [ud.rtrajectories ud.rntrajectories] = cb_cleardots(fig);
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud);
        
    case {'copy_poly', 'k_c'}
        figure(fig);
        set(fig, 'WindowButtonDownFcn', 'mycallbacks2(gcbf, ''copy_poly_down'');');
        set(fig, 'WindowButtonMotionFcn', 'show_coord_pix(gcf);');
        set(fig, 'WindowButtonUpFcn', '');
        set(fig, 'Pointer', 'crossh');
        
    case 'copy_poly_down'
        ud = cb_copypolygon(fig);
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud);
        
    case {'paste_poly', 'k_v'}
        [ud myind] = cb_pastepolygon(fig);
        
        if myind > 0
            cb_drawpolygon(fig, ud.rpolygons{myind, 1, ud.curslice + 1}, poly_sz, poly_color);
            
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud);
        end
        
    case {'move_poly', 'k_m'}
        figure(fig);
        set(fig, 'WindowButtonDownFcn', 'mycallbacks2(gcbf, ''move_poly_down'');');
        set(fig, 'Pointer', 'crossh');
        
    case 'move_poly_down'
        [tmp ud.rtmp] = cb_deleteclickedpolyline(fig, ud.rpolygons);
        
        if ud.rtmp ~= -1
            ud.rlcoords = cb_getcoords(fig);
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud); % If you do this at the end of this "case", lineh property
            % in udata is not properly stored.
            
            % If the right button was used to delete a line, coords is set to
            % an empty vector and nothing else should be done.
            if ~isempty(ud.rlcoords)
                ud.rpoly = ud.rpolygons{ud.rtmp, 1, ud.curslice + 1};
                if ~ isempty(ud.rpoly)
                    set(fig, 'WindowButtonMotionFcn', 'show_coord_pix(gcf); mycallbacks2(gcbf, ''poly_move'');');
                    set(fig, 'WindowButtonUpFcn', 'mycallbacks2(gcbf, ''poly_up_move'');');
                    cb_drawpolygon(fig, ud.rpoly, poly_sz, poly_color);
                    set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
                    set(fig, 'UserData', ud);
                end
            end
        end
        
    case 'poly_move'
        pnt = cb_getcoords(fig);
        ud.rpoly(:, 1) = ud.rpoly(:, 1) + pnt(1) - ud.rlcoords(1);
        ud.rpoly(:, 2) = ud.rpoly(:, 2) + pnt(2) - ud.rlcoords(2);
        cb_deleteclickedpolyline(fig, ud.rpolygons);
        ud.rlcoords = pnt;
        cb_drawpolygon(fig, ud.rpoly, poly_sz, poly_color);
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud);
        
    case 'poly_up_move'
        set(fig, 'WindowButtonMotionFcn', 'show_coord_pix(gcf);');
        pnt = cb_getcoords(fig);
        ud.rpoly(:, 1) = ud.rpoly(:, 1) + pnt(1) - ud.rlcoords(1);
        ud.rpoly(:, 2) = ud.rpoly(:, 2) + pnt(2) - ud.rlcoords(2);
        ud.rpolygons{ud.rtmp, 1, ud.curslice + 1} = ud.rpoly;
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud);
        
    case {'del_all_this', 'k_\'}
        % Michael edit
        ud = cb_deleteall(fig, 1);
        % Michael edit
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud);
        
    case {'del_all_all', 'k_-'}
        ud = cb_deleteallmarkings(fig);
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud);
        
        % Michael edit
        
    case 'del_all_seeds'
        ud = cb_deleteAllSeeds(fig);
        % Michael edit ends
        
        % MEASUREMENT CALLBACKS -------------------------------------------------------
        
        % Draw and Measure trajectory.
        
    case {'qpoly_line'; 'k_q'}
        figure(fig);
        set(fig, 'WindowButtonDownFcn', '');
        set(fig, 'WindowButtonMotionFcn', 'show_coord_pix(gcf);');
        set(fig, 'WindowButtonUpFcn', 'mycallbacks2(gcbf, ''qpoly_line_up'');');
        set(fig, 'Pointer', 'crossh');
        
    case 'qpoly_line_up'
        [ud.rlcoords ud.rrcoords ud.rmcoords] = cb_getcoords(fig);
        % Left click.
        if (~isempty(ud.rlcoords))
            ud.rpoly(end+1, :) = ud.rlcoords;
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud); % If you do this at the end of this "case", lineh property
            % in udata is not properly stored.
            
            cb_drawlinepol(fig, ud.rlcoords, cb_getcoords(fig), ud.rBRUSH_SZ, line_color);
            
            set(fig, 'WindowButtonMotionFcn', 'show_coord_pix(gcf); mycallbacks2(gcbf, ''qpoly_line_motion'');');
            set(fig, 'WindowButtonUpFcn', 'mycallbacks2(gcbf, ''qpoly_line_up2'');');
            set(fig, 'WindowButtonDownFcn', '');
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud);
            % Right button can be used to delete polygons when not drawing them.
        elseif (~isempty(ud.rrcoords))
            ud.rtrajectories = cb_deleteclickedpolyline(fig, ud.rtrajectories);
            % Only delete the trajectory if the click was on a trajectory.
            count = 0;
            oldtr = ud.rtrajectories(:, :, ud.curslice + 1);
            for ii = 1:numel(oldtr)
                if ~isempty(oldtr{ii})
                    count = count + 1;
                end
            end
            
            ud.rntrajectories(ud.curslice + 1) = count;
            
            cb_painttrajectorylabels(fig, ud.rtrajectories, tr_label_sz, line_color, ud.rflag_fiduciallabels);
            
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud);
        end
        
    case 'qpoly_line_up2'
        %disp('Click!!');
        %ud.rlcoords
        [tmp, ud.rrcoords, ud.rmcoords] = cb_getcoords(fig);
        
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud); % If you do this at the end of this "case", lineh property
        % in udata is not properly stored.
        
        % The right button is used to close the polyline. Then ud.rcoords is set to
        % an empty vector. The middle button is used to delete the last segment.
        
        % Left-click.
        if isempty(ud.rrcoords) & isempty(ud.rmcoords)
            cb_drawlinepol(fig, ud.rlcoords, cb_getcoords(fig), ud.rBRUSH_SZ, line_color);
            
            ud = get(fig, 'UserData');
            ud.rlcoords = tmp;
            ud.rtmp = cb_storecurrentlinepol(fig, ud.rtmp);
            
            ud.rpoly(end+1, :) = ud.rlcoords;
            %disp(coords);
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud);
            
            % Right-click ends drawing.
        elseif isempty(ud.rmcoords)
            ud = get(fig, 'UserData');
            
            cb_deletelinepol(fig);
            
            
            % Do not close polygon
            %ud.rpoly(end+1, :) = ud.rpoly(1, :);
            
            if ~ ud.rDRAW_ALL
                % Store in in the first empty spot.
                myind = 1;
                for i = 1:size(ud.rtrajectories, 1)
                    if (isempty(ud.rtrajectories{i, 1, ud.curslice+1}))
                        ud.rtrajectories{i, 1, ud.curslice + 1} = ud.rpoly;
                        ud.rntrajectories(ud.curslice + 1) = ud.rntrajectories(ud.curslice + 1) + 1;
                        break;
                    end
                    myind = myind + 1;
                end
                
                % If there were no empty spots, store at the end.
                if (myind > size(ud.rtrajectories, 1))
                    ud.rtrajectories{end+1, 1, ud.curslice + 1} = ud.rpoly;
                    ud.rntrajectories(ud.curslice + 1) = ud.rntrajectories(ud.curslice + 1) + 1;
                end
                
                theind = myind;
            else
                % Store in in the first empty spot.
                for zz = 1:ud.imsize(3)
                    myind = 1;
                    for i = 1:size(ud.rtrajectories, 1)
                        if (isempty(ud.rtrajectories{i, 1, zz}))
                            ud.rtrajectories{i, 1, zz} = ud.rpoly;
                            break;
                        end
                        myind = myind + 1;
                    end
                    
                    % If there were no empty spots, store at the end.
                    if (myind > size(ud.rtrajectories, 1))
                        ud.rtrajectories{end+1, 1, zz} = ud.rpoly;
                        ud.rntrajectories(zz) = ud.rntrajectories(zz) + 1;
                    end
                    
                    if zz == (ud.curslice + 1)
                        theind = myind;
                    end
                end
            end
            
            [ang l int] = cb_multicolormeasuretrajectory(fig, ud.rpoly, ud.rBRUSH_SZ, 1);
            ang = ang * 180 / pi;
            %disp(cat(2, 'Angle: ', num2str(ang * 180 / pi)));
            %disp(cat(2, 'Length: ', num2str(l)));
            %disp(cat(2, 'Intensity: ', num2str(cb_getlineintensity(fig, ang, ud.rCHANNEL, ud.rBRUSH_SZ))));
            coords = cb_getcoords(fig);
            thex = mean(ud.rpoly(:, 1));
            they = mean(ud.rpoly(:, 2));
            thez = mean(ud.rpoly(:, 3));
            int1 = int(1);
            int2 = int(2);
            int3 = int(3);
            rat12 = int1/int2;
            
            fprintf('\nX:%.2f\tY:%.2f\tZ:%.2f\tA:%.2f\tL:%.2f\tR:%.2f\tG:%.2f\tB:%.2f', thex, they, thez, ang, l, int1, int2, int3);
            
            ud.rtmp = cb_clearlines(fig, ud.rtmp);
            
            ud.rpoly = [];
            ud.lineh = [];
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud);
            set(fig, 'WindowButtonDownFcn', '');
            set(fig, 'WindowButtonMotionFcn', 'show_coord_pix(gcf);');
            set(fig, 'WindowButtonUpFcn', 'mycallbacks2(gcbf, ''qpoly_line_up'');');
            
            cb_drawpolygon(fig, ud.rtrajectories{theind, 1, ud.curslice + 1}, poly_sz, line_color);
            
            cb_painttrajectorylabels(fig, ud.rtrajectories, tr_label_sz, line_color, ud.rflag_fiduciallabels);
            
            % Middle click.
        else
            if (numel(ud.rtmp >= 2))
                % Deletes the line currently being drawn.
                cb_deletelinepol(fig);
                
                % Deletes the previously drawn line.
                cb_deletelinepol(fig);
                
                ud.rtmp = ud.rtmp(1:(end - 1));
                ud.lineh = ud.rtmp(end);
                ud.rpoly = ud.rpoly(1:(end-1), :);
                ud.rlcoords = ud.rpoly(end, :);
                set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
                set(fig, 'UserData', ud);
                
                cb_drawlinepol(fig, ud.rlcoords, ud.rmcoords, ud.rBRUSH_SZ, line_color);
            end
        end
        
    case 'qpoly_line_motion'
        %ud1 = get(gcf, 'UserData');
        %disp('ud1');
        %get(ud1.lineh)
        
        cb_deletelinepol(fig);
        
        [moveto1 moveto2 moveto3] = cb_getcoords(fig);
        if (isempty(moveto1) & isempty(moveto2))
            moveto = moveto3;
        elseif isempty(moveto1)
            moveto = moveto2;
        else
            moveto = moveto1;
        end
        
        cb_drawlinepol(fig, ud.rlcoords, moveto, ud.rBRUSH_SZ, line_color);
        
        
        %  	case {'signal'}
        %  		[channel thr_val] = dialog_quantifymask((1:3), ud.rCHANNEL); 
        %  		f = cb_quantifyimage(fig, channel, ud.rBRUSH_SZ, thr_val);
        %  		fprintf('\n%.2f%% of the pixels masked in channel %d are above the %.0f threshold level.\n', f * 100, channel, round(thr_val));
	
        %  	case {'signal_t', 'k_F'}
        %  		curr_dir = pwd;
        %  		cd(ud.rcurrdir);
        %  		
        %  		[filename, pathname, ext] = uiputfile({'*.mat'}, 'Save measurements');
        %  		
        %  		if (~ isequal(filename, 0))
        %  			tps = find(ud.rnfiducials); % Time points where fiducials were defined.
        %  			[pix_vals bg_vals tot_vals f flecuit] = cb_quantifytime(fig, ud.rCHANNEL, ud.rBRUSH_SZ, tps, ud.rpolygons{1, 1, ud.curslice+1}); % Quantify the signal in the time points where there are fiducials.
        %  			
        %  			save(cat(2, pathname, filename), 'f', 'flecuit', 'pix_vals', 'bg_vals', 'tot_vals');
        %  		else
        %  			tps = find(ud.rnfiducials); % Time points where fiducials were defined.
        %  			[pix_vals bg_vals tot_vals f flecuit] = cb_quantifytime(fig, ud.rCHANNEL, ud.rBRUSH_SZ, tps, ud.rpolygons{1, 1, ud.curslice+1}); % Quantify the signal in the time points where there are fiducials.
        %  		end
        %  		
        %  		cd(curr_dir);

        %figure; plot([-4 -2 0:2:((numel(f)-3)*2)], f, 'LineWidth', 3);ylim([0 1]);
        
    case 'tr_t'
        curr_dir = pwd;
        cd(ud.rcurrdir);
        
        if nargin < 3
            [filename, pathname, ext] = uiputfile(extensions_matfirst, 'Save measurements');
            plot_flag = 0;
            integrate = 0;
        else
            filename = varargin{3};
            pathname = fileparts(filename);
            plot_flag = 0;
            
            if nargin < 4
                integrate = 0; % Mean intensity in the trajectory.
            else
                integrate = varargin{4}; % Summed intensity along the trajectory.
            end
        end
        
        cd(curr_dir);
        if (~ isequal(filename, 0))
            udold = ud;
            
            tps = find(ud.rntrajectories); % Time points where trajectories were defined.
            
            % If a polygon was defined in the first time point where a trajectory was drawn, use it as the background region (a polygon will also be searched in each subsequent time point, but if not found, the polygon defined on the first time point will be used).
            if isempty(ud.rpolygons) | isempty(ud.rpolygons{1, 1, tps(1)})
                bgpolyflag = 0;
            else
                bgpolyflag = 1;
            end
            
            % If we only defined a trajectory in one time point, use the same trajectory in all time points.
            if numel(tps) == 1
                ud.rtrajectories(1, 1, :) = ud.rtrajectories(1, 1, tps(1));
                ud.rntrajectories(1:end) = 1;
                tps = 1:numel(ud.rntrajectories);
            end
            set(fig, 'UserData', ud);
            
            [bgcorrected_r bgcorrected_g bgcorrected_b pix_vals_r pix_vals_g pix_vals_b bg_vals length_vals] = cb_quantifytrajectoriestime(fig, ud.rCHANNEL, ud.rBRUSH_SZ, tps, bgpolyflag, integrate);
            
            ud = udold;
            set(fig, 'UserData', ud);
            
            if (~ isequal(filename, 0))
                if (strcmp(extensions_matfirst{ext}, '*.mat'))
                    save(cat(2, pathname, filename), 'tps', 'bgcorrected_r', 'bgcorrected_g', 'bgcorrected_b', 'pix_vals_r', 'pix_vals_g', 'pix_vals_b', 'bg_vals', 'length_vals');
                elseif (strcmp(extensions_matfirst{ext}, '*.csv'))
                    savecsv(cat(2, pathname, filename), 'tps', tps', 'bgcorrected_r', bgcorrected_r, 'bgcorrected_g', bgcorrected_g, 'bgcorrected_b', bgcorrected_b, 'pix_vals_r', pix_vals_r, 'pix_vals_g', pix_vals_g, 'pix_vals_b', pix_vals_b, 'bg_vals', bg_vals, 'length_vals', length_vals);
                end
            end
            
            if plot_flag
                um_per_pix = 0.2;
                tstep_secs = 5;
                figure;
                for ii = 1:size(pix_vals_g, 1)
                    pix_vals = pix_vals_g(ii, :);
                    plot((tps-tps(1)) .* tstep_secs, pix_vals, 'b', 'LineWidth', 3);
                    hold on;
                    plot((tps-tps(1)) .* tstep_secs, bg_vals(2, :), 'g', 'LineWidth', 3);
                    ysmooth = double(smooth(dip_image(pix_vals((ud.curslice+1+1):end)), 1));
                    ysmooth = [pix_vals(1:(ud.curslice+1)) ysmooth];
                    plot((tps-tps(1)) .* tstep_secs, ysmooth, 'r', 'LineWidth', 3);
                    plot((tps-tps(1)) .* tstep_secs, length_vals.*um_per_pix.*10, 'k', 'LineWidth', 3); % Length in tenths of micrometer (assuming a 0.33micron/pix resolution).
                end
                
                figure;
                for ii = 1:size(pix_vals_g, 1)
                    bgcorrected = bgcorrected_g(ii, :);
                    
                    plot((tps-tps(1)) .* tstep_secs, bgcorrected, 'r', 'LineWidth', 3);
                    hold on;
                    ylim([0 150]);
                end
                
                figure;
                for ii = 1:size(pix_vals_g, 1)
                    bgcorrected = bgcorrected_g(ii, :);
                    
                    plot((tps-tps(1)) .* tstep_secs, bgcorrected ./ (mean(bgcorrected(1:(ud.curslice+1)))), 'b', 'LineWidth', 2);
                    ylim([0.51 4]); xlabel('time (seconds)', 'FontSize', 16); ylabel('myosin intensity (a. u.)', 'FontSize', 16); set(gca, 'LineWidth', 2, 'FontSize', 16);
                    ysmooth = double(smooth(dip_image(bgcorrected ./ (mean(bgcorrected(1:(ud.curslice+1))))), 1));
                    hold on;
                    plot((tps-tps(1)) .* tstep_secs, ysmooth, 'r', 'LineWidth', 3);
                    ylim([0 1.1]);
                end
            end
        end
        
    case 'tr_profile'
        pixel_size = ud.rres(1);
        
        curr_dir = pwd;
        cd(ud.rcurrdir);
        
        if nargin < 5
            plot_flag = 0;
        else
            plot_flag = varargin{5};
        end
        
        if nargin < 4
            smooth_factor = 10;
        else
            smooth_factor = varargin{4};
        end
        
        if nargin < 3
            [filename, pathname, ext] = uiputfile(extensions_matfirst, 'Save measurements');
        else
            [pathname, filename, ext] = fileparts(varargin{3});
            pathname = cat(2, pathname, filesep);
            filename = cat(2, filename, ext);
            switch ext
                case '.mat'
                    ext = 1;
                case '.csv'
                    ext = 2;
                otherwise
                    error('Invalid extension.\n');
            end
        end
        cd(curr_dir);
        if (~ isequal(filename, 0))
            udold = ud;
            
            tps = find(ud.rntrajectories); % Time points where trajectories were defined.
            
            % If we only defined a trajectory in one time point, use the same trajectory in all time points. This "new" trajectories will be removed later. That is why udold was created.
            if numel(tps) == 1
                ud.rtrajectories(1, 1, :) = ud.rtrajectories(1, 1, tps(1));
                ud.rntrajectories(1:end) = 1;
                tps = 1:numel(ud.rntrajectories);
            end
            set(fig, 'UserData', ud);
            
            [pix_vals_r pix_vals_g pix_vals_b gaussparams d] = cb_quantifytrajectoryprofile_constantdist(fig, ud.rCHANNEL, ud.rBRUSH_SZ, tps, smooth_factor);
            
            ud = udold;
            set(fig, 'UserData', ud);
            
            if (~ isequal(filename, 0))
                if (strcmp(extensions_matfirst{ext}, '*.mat'))
                    save(cat(2, pathname, filename), 'tps', 'pix_vals_r', 'pix_vals_g', 'pix_vals_b', 'gaussparams', 'd');
                elseif (strcmp(extensions_matfirst{ext}, '*.csv'))
                    savecsv(cat(2, pathname, filename), 'tps', tps', 'pix_vals_r', pix_vals_r, 'pix_vals_g', pix_vals_g, 'pix_vals_b', pix_vals_b, 'gaussparams', gaussparams, 'd', d);
                end
            end
            
            cd(curr_dir);
            
            if plot_flag
                ref = mean(pix_vals_r(1:2, :));
                normpix = pix_vals_r ./ repmat(ref, [size(pix_vals_r, 1) 1]);
                cm = jet(size(normpix, 1));
                cm(1, :) = zeros(1, size(cm, 2));
                
                % Plot data and fits on a pixel scale.
                for ii = 1:size(normpix, 1)
                    figure;
                    hold on;
                    plot((1:size(normpix, 2)) .* d(ii), 1 - gaussianfn2(gaussparams(ii, :), (1:size(normpix, 2))), 'Color', cm(ii, :), 'LineWidth', 2);
                    yvals = double(smooth(dip_image(normpix(ii, :)), smooth_factor));
                    plot((1:size(normpix, 2)) .* d(ii), yvals, 'Color', cm(ii, :), 'LineWidth', 2, 'LineStyle', '--'); % Smoothing with gaussian widths of 5 or 10 looks super nice!!!
                end
                ylim([-0.1 1.1]);xlim([0 100]);
                %                          % Plot the fits on a pixel scale.
            %                          figure;
            %                          for ii = 1:size(normpix, 1)
            %                                  if ii > 2        
            %                                          plot((1:size(normpix, 2)) .* d(ii), 1 - gaussianfn2(gaussparams(ii, :), (1:size(normpix, 2))), 'Color', cm(ii, :), 'LineWidth', 2);
            %                                  else
            %                                          plot((1:size(normpix, 2)) .* d(ii), ones(1, size(normpix, 2)), 'Color', cm(ii, :), 'LineWidth', 2);
            %                                  end                
            %                                  hold on;
            %                                  %plot((1:size(normpix, 2)), yvals, 'Color', cm(ii, :), 'LineWidth', 2); % Smoothing with gaussian widths of 5 or 10 looks super nice!!!
            %                          end
            %                          ylim([-0.1 1.1]);xlim([0 100]);
            %                          
            %                          % Plot the raw data (with the same smoothing that was used for the fit) on a micron scale.
            %                          figure;
            %                          for ii = 1:size(normpix, 1)
            %                                  yvals = double(smooth(dip_image(normpix(ii, :)), 10));
            %                                  %yvals = pix_vals_r(ii, :);
            %                                  if ii == 1 || ii == 2 || ii == 3 || ii == 18
            %                                          plot((1:size(normpix, 2)) .* d(ii) .* pixel_size, yvals, 'Color', cm(ii, :), 'LineWidth', 2); % Smoothing with gaussian widths of 5 or 10 looks super nice!!!
            %                                  end        
            %                                  hold on;
            %                          end
            %                          
            %                          for ii = 1:size(normpix, 1)
            %                                  if ii == 1 || ii == 2       
            %                                          plot((1:size(normpix, 2)) .* d(ii) .* pixel_size, ones(1, size(normpix, 2)), 'Color', 'k', 'LineWidth', 2, 'LineStyle', '--');
            %                                  elseif ii == 3 || ii == 18
            %                                          plot((1:size(normpix, 2)) .* d(ii) .* pixel_size, 1 - gaussianfn2(gaussparams(ii, :), (1:size(normpix, 2))), 'Color', cm(ii, :), 'LineWidth', 2, 'LineStyle', '--');
            %                                  end                
            %                                  hold on;
            %                                  %plot((1:size(normpix, 2)), yvals, 'Color', cm(ii, :), 'LineWidth', 2); % Smoothing with gaussian widths of 5 or 10 looks super nice!!!
            %                          end
            %   
            %                          %xlim([0 100]);
            %  
            %                          
            %                          % Plot the raw data with almost no smoothing.
            %                          figure;
            %                          for ii = 1:size(normpix, 1)
            %                                  yvals = double(smooth(dip_image(normpix(ii, :)), 1));
            %                                  %yvals = pix_vals_r(ii, :);
            %                                  if ii == 1 || ii == 2 || ii == 3 || ii == 8 || ii == 13 || ii == 18
            %                                          plot((1:size(normpix, 2)) .* d(ii) .* pixel_size, yvals, 'Color', cm(ii, :), 'LineWidth', 2); % Smoothing with gaussian widths of 5 or 10 looks super nice!!!
            %                                  end        
            %                                  hold on;
            %                          end
            %                          
            %                          figure;
            %                          for ii = 1:size(normpix, 1)
            %                                  yvals = double(smooth(dip_image(pix_vals_r(ii, :)), 10));
            %                                  %yvals = pix_vals_r(ii, :);
            %                                  plot((1:size(normpix, 2)), yvals, 'Color', cm(ii, :), 'LineWidth', 2); % Smoothing with gaussian widths of 5 or 10 looks super nice!!!
            %                                  hold on;
            %                          end
            %                          %ylim([-0.1 1.1]);xlim([0 100]);
            %          
                
            end
        end
        
    case 'vertex_intensity'
        curr_dir = pwd;
        cd(ud.rcurrdir);
        [filename, pathname, ext] = uiputfile(extensions_matfirst, 'Save measurements');
        cd(curr_dir);
        
        tps=find(ud.rnfiducials~=0);
        for tt=1:size(tps,2)
            initP(tt,:)=ud.rfiducials(1,:,tps(tt)) ;
            finalP(tt,:)=ud.rfiducials(2,:,tps(tt)) ;
        end
        intialPoint=initP(:,1:2);
        finalPoint=finalP(:,1:2);
        [pix_vals_r int_after_bleach_corr  rawData Val_bg_sub Val_bleaching]=IntensityProfilePolygons_tzc(ud,tps,intialPoint,finalPoint);
        cd(curr_dir);
        if (~ isequal(filename, 0))
            if (strcmp(extensions_matfirst{ext}, '*.mat'))
                save(cat(2, pathname, filename), 'tps','pix_vals_r', 'int_after_bleach_corr','rawData','Val_bg_sub','Val_bleaching')
                
            elseif (strcmp(extensions_matfirst{ext}, '*.csv'))
                savecsv(cat(2, pathname, filename), 'tps', tps','rawData', rawData);
            end
        end
        
        cd(curr_dir);
        
    case {'eq_tr', 'k_='}
        tr = cb_equalizetrajectories(fig);
        ud.rtrajectories = tr;
        set(gcf, 'UserData', ud);
        cb_paintpolylines(fig, ud.rtrajectories, poly_sz, line_color, 1);
        
    case 'roi_t'
        curr_dir = pwd;
        cd(ud.rcurrdir);
        
        if nargin < 3
            [filename, pathname, ext] = uiputfile(extensions_matfirst, 'Save measurements');
        else
            [pathname, filename, ext] = fileparts(varargin{3});
            pathname = cat(2, pathname, filesep);
            filename = cat(2, filename, ext);
            switch ext
                case '.mat'
                    ext = 1;
                case '.csv'
                    ext = 2;
                otherwise
                    error('Invalid extension.');
            end
            
            fprintf('Analyzing %s ...\n', pathname);
        end
        cd(curr_dir);
        
        tps = 1:numel(ud.rnfiducials);
        
        if nargin < 3
            [pix_vals_r pix_vals_g pix_vals_b] = cb_quantifyroitime(fig, ud.rCHANNEL, ud.rBRUSH_SZ, tps)
        else % The difference is in the semi colon!!!! Do not display results if running this as a command.
            [pix_vals_r pix_vals_g pix_vals_b] = cb_quantifyroitime(fig, ud.rCHANNEL, ud.rBRUSH_SZ, tps);
        end
        
        % Percentage of signal variation with respect to the mean value.
        %                  if nargin < 3
        %                          100 .* abs(pix_vals_r - mean(pix_vals_r))./mean(pix_vals_r)
        %                  end
        
        cd(ud.rcurrdir);
        if (~ isequal(filename, 0))
            if (strcmp(extensions_matfirst{ext}, '*.mat'))
                save(cat(2, pathname, filename), 'tps', 'pix_vals_r', 'pix_vals_g', 'pix_vals_b');
            elseif (strcmp(extensions_matfirst{ext}, '*.csv'))
                savecsv(cat(2, pathname, filename), 'tps', tps', 'pix_vals_r', pix_vals_r, 'pix_vals_g', pix_vals_g, 'pix_vals_b', pix_vals_b);
            end
        end
        
        cd(curr_dir);
        
        %                  if nargin < 3
        %                          figure;
        %                          plot((tps-tps(1)) .* 2, pix_vals_g, 'b', 'LineWidth', 3);
        %                          hold on;
        %                          ysmooth = double(smooth(dip_image(pix_vals_g(3:end)), 1));
        %                          ysmooth = [pix_vals_g(1:2) ysmooth];
        %                          plot((tps-tps(1)) .* 2, ysmooth, 'r', 'LineWidth', 3);
        %                          xlabel('time (seconds)', 'FontSize', 16);
        %                          ylabel('myosin intensity (a. u.)', 'FontSize', 16);
        %                          set(gca, 'LineWidth', 2, 'FontSize', 16);
        %                  end
        
    case 'signal_polarity_OLD' % Uses line masks. Can be removed when you validate the new approach.
        [filename, pathname, ext] = uiputfile(extensions, 'Save quantification data');
        %mtrx = cb_quantifylines(gcbf, CHANNEL, BRUSH_SZ, lns);
        mtrx = cb_multicolorquantifylines(fig, [1 2 3], ud.rBRUSH_SZ, ud.rlns);
        
        fprintf('Red\n---\n');
        [binranksr nbinranksr] = plotpolarity(mtrx(:, 1), mtrx(:, 3), bin_limits);
        % Plot histogram.
        %disp(mtrx);
        disp(binranksr);
        disp(nbinranksr);
        
        fprintf('Green\n---\n');
        [binranksg nbinranksg] = plotpolarity(mtrx(:, 1), mtrx(:, 4), bin_limits);
        % Plot histogram.
        %disp(mtrx);
        disp(binranksg);
        disp(nbinranksg);
        
        fprintf('Blue\n---\n');
        [binranksb nbinranksb] = plotpolarity(mtrx(:, 1), mtrx(:, 5), bin_limits);
        % Plot histogram.
        %disp(mtrx);
        disp(binranksb);
        disp(nbinranksb);
        
        if (~ isequal(filename, 0))
            [tmp1 tmp2 theext] = fileparts(filename);
            theext = cat(2, '*', theext);
            
            % If the extension is written, use that mode.
            if (~ isempty(theext))
                for ie = 1:numel(extensions)
                    if strcmp(extensions{ie}, theext)
                        ext = ie;
                        break;
                    end
                end
            end
            
            
            if (strcmp(extensions{ext}, '*.csv'))
                %csvwrite(cat(2, pathname, filename), mtrx, 0, 0);
                
                %csvwrite(cat(2, pathname, filename), binranks, size(mtrx, 1) + 1, 0);
                %csvwrite(cat(2, pathname, filename), nbinranks, size(mtrx, 1) + 3, 0);
                dlmwrite(cat(2, pathname, filename), ['A', 'L', 'R', 'G', 'B']);
                dlmwrite(cat(2, pathname, filename), mtrx, '-append', 'roffset', 0, 'coffset', 0);
                dlmwrite(cat(2, pathname, filename), ['R'], '-append', 'roffset', 1, 'coffset', 0);
                dlmwrite(cat(2, pathname, filename), binranksr, '-append', 'roffset', 0, 'coffset', 0);
                dlmwrite(cat(2, pathname, filename), nbinranksr, '-append', 'roffset', 0, 'coffset', 0);
                dlmwrite(cat(2, pathname, filename), ['G'], '-append', 'roffset', 1, 'coffset', 0);
                dlmwrite(cat(2, pathname, filename), binranksg, '-append', 'roffset', 0, 'coffset', 0);
                dlmwrite(cat(2, pathname, filename), nbinranksg, '-append', 'roffset', 0, 'coffset', 0);
                dlmwrite(cat(2, pathname, filename), ['B'], '-append', 'roffset', 1, 'coffset', 0);
                dlmwrite(cat(2, pathname, filename), binranksb, '-append', 'roffset', 0, 'coffset', 0);
                dlmwrite(cat(2, pathname, filename), nbinranksb, '-append', 'roffset', 0, 'coffset', 0);
            elseif (strcmp(extensions{ext}, '*.mat'))
                save(cat(2, pathname, filename), 'mtrx', 'binranksr', 'nbinranksr', 'binranksg', 'nbinranksg', 'binranksb', 'nbinranksb');
            end
        end
        
    case {'signal_polarity', 'k_Q'}
        % Remove "dot" trajectories that will mess up with the analysis.
        [ud.rtrajectories ud.rntrajectories] = cb_cleardots(fig);
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud);
        
        curr_dir = pwd;
        cd(ud.rcurrdir);
        
        if nargin < 3
            [filename, pathname, ext] = uiputfile(extensions, 'Save quantification data');
            plot_flag = 0;
        else
            filename = varargin{3};
            pathname = fileparts(filename);
            plot_flag = 0;
        end
        cd(curr_dir);
        %mtrx = cb_quantifylines(gcbf, CHANNEL, BRUSH_SZ, lns);
        mtrx = cb_multicolorquantifytrajectories(fig, [1 2 3], ud.rBRUSH_SZ, ud.rtrajectories(:, :, ud.curslice+1));
        
        % Compute image backgrounds: mode, mean, cytoplasmic intensity.
        if ~ isempty(ud.colordata)
            im = ud.colordata;
            celloutlines = trajectories2mask(ud.rtrajectories(:, :, ud.curslice+1), ud.imsize, ud.rBRUSH_SZ); % Compute the cell outline mask.
            celloutlines = ~ dilation(celloutlines, 15); % Magic number: grow the outlines a bit to exclude as many edge pixels as possible. You could also have done this with a distance transform (which is more elegant).
            thecytoplasm(1) = mean(im{1}(celloutlines));
            
            themean(1) = mean(reshape(double(im{1}), [1 numel(double(im{1}))]));
            themode(1) = mode(reshape(double(im{1}), [1 numel(double(im{1}))]));
            if themode(1) == max(max(double(im{1})))
                
                h = diphist(im{1});
                % If >10% of pixels are saturated, complain.
                if h(end) > 0.1*sum(h)
                    fprintf('\nSaturated red channel!!!!\n');
                end
                [tmp themode(1)] = max(h(1:(end-1)));
                themode(1) = themode(1) - 1;
            elseif themode(1) == 0
                h = diphist(im{1});
                % If >10% of pixels are underexposed, complain.
                if h(1) > 0.1*sum(h)
                    fprintf('\nUnderexposed red channel!!!!\n');
                end
                [tmp themode(1)] = max(h(2:(end)));
                themode(1) = themode(1);
            end
            
            thecytoplasm(2) = mean(im{2}(celloutlines));
            themean(2) = mean(reshape(double(im{2}), [1 numel(double(im{2}))]));
            themode(2) = mode(reshape(double(im{2}), [1 numel(double(im{2}))]));
            if themode(2) == max(max(double(im{2})))
                h = diphist(im{2});
                % If >10% of pixels are saturated, complain.
                if h(end) > 0.1*sum(h)
                    fprintf('\nSaturated green channel!!!!\n');
                end
                [tmp themode(2)] = max(h(1:(end-1)));
                themode(2) = themode(2) - 1;
            elseif themode(2) == 0
                h = diphist(im{2});
                % If >10% of pixels are underexposed, complain.
                if h(1) > 0.1*sum(h)
                    fprintf('\nUnderexposed green channel!!!!\n');
                end
                [tmp themode(2)] = max(h(2:(end)));
                themode(2) = themode(2);
            end
            
            thecytoplasm(3) = mean(im{3}(celloutlines));
            themean(3) = mean(reshape(double(im{3}), [1 numel(double(im{3}))]));
            themode(3) = mode(reshape(double(im{3}), [1 numel(double(im{3}))]));
            if themode(3) == max(max(double(im{3})))
                h = diphist(im{3});
                % If >10% of pixels are saturated, complain.
                if h(end) > 0.1*sum(h)
                    fprintf('\nSaturated blue channel!!!!\n');
                end
                [tmp themode(3)] = max(h(1:(end-1)));
                themode(3) = themode(3) - 1;
            elseif themode(3) == 0
                h = diphist(im{3});
                % If >10% of pixels are underexposed, complain.
                if h(1) > 0.1*sum(h)
                    fprintf('\nUnderexposed blue channel!!!!\n');
                end
                [tmp themode(3)] = max(h(2:(end)));
                themode(3) = themode(3);
            end
        else
            im = ud.imagedata;
            
            celloutlines = trajectories2mask(ud.rtrajectories(:, :, ud.curslice+1), ud.imsize, ud.rBRUSH_SZ); % Compute the cell outline mask.
            celloutlines = ~ dilation(celloutlines, 15); % Magic number: grow the outlines a bit to exclude as many edge pixels as possible.
            thecytoplasm(1) = mean(im(celloutlines));
            
            themean(1) = mean(reshape(double(im{1}), [1 numel(double(im{1}))]));
            themode(1) = mode(reshape(double(im{1}), [1 numel(double(im{1}))]));
            if themode(1) == max(max(double(im{1})))
                h = diphist(im{1});
                % If >10% of pixels are saturated, complain.
                if h(end) > 0.1*sum(h)
                    fprintf('\nSaturated image!!!!\n');
                end
                [tmp themode(1)] = max(h(1:(end-1)));
                themode(1) = themode(1) - 1;
            elseif themode(1) == 0
                h = diphist(im{1});
                % If >10% of pixels are underexposed, complain.
                if h(1) > 0.1*sum(h)
                    fprintf('\nUnderexposed image!!!!\n');
                end
                [tmp themode(1)] = max(h(2:(end)));
                themode(1) = themode(1);
            end
            
            thecytoplasm(2) = thecytoplasm(1);
            thecytoplasm(3) = thecytoplasm(1);
            themean(2) = themean(1);
            themean(3) = themean(1);
            themode(2) = themode(1);
            themode(3) = themode(1);
        end
        
        % This chunk here runs the analysis considering angles between 0 and 180
        bin_limits = 0:15:180;
        [binranksr nbinranksr binabsr nbinabsr] = plotpolarity(mtrx(:, 1), mtrx(:, 3), bin_limits, plot_flag, thecytoplasm(1));
        [binranksg nbinranksg binabsg nbinabsg] = plotpolarity(mtrx(:, 1), mtrx(:, 4), bin_limits, plot_flag, thecytoplasm(2));
        [binranksb nbinranksb binabsb nbinabsb] = plotpolarity(mtrx(:, 1), mtrx(:, 5), bin_limits, plot_flag, thecytoplasm(3));
        
        % This chunk here runs the analysis considering angles between 0 and 90 0nly (e.g., 175 is 5 degrees).
        bin_limits = 0:15:90;
        ang0_90 = mtrx(:, 1);
        ind = find(ang0_90 > 90.);
        ang0_90(ind) = 180. - ang0_90(ind);
        
        [binranksr0_90 nbinranksr0_90 binabsr0_90 nbinabsr0_90] = plotpolarity(ang0_90, mtrx(:, 3), bin_limits, plot_flag, thecytoplasm(1));
        [binranksg0_90 nbinranksg0_90 binabsg0_90 nbinabsg0_90] = plotpolarity(ang0_90, mtrx(:, 4), bin_limits, plot_flag, thecytoplasm(2));
        [binranksb0_90 nbinranksb0_90 binabsb0_90 nbinabsb0_90] = plotpolarity(ang0_90, mtrx(:, 5), bin_limits, plot_flag, thecytoplasm(3));
        
        % This chunk here shifts angles 15 degrees clockwise and runs then the analysis considering angles between 0 and 90 0nly (e.g., 175 is 10 degrees).
        bin_limits = 0:15:90;
        ang_cw = mtrx(:, 1) + 15;
        ind = find(ang_cw > 180.);
        ang_cw(ind) = ang_cw(ind) - 180.;
        ind = find(ang_cw > 90.);
        ang_cw(ind) = 180. - ang_cw(ind);
        
        [binranksr_cw nbinranksr_cw binabsr_cw nbinabsr_cw] = plotpolarity(ang_cw, mtrx(:, 3), bin_limits, 0, thecytoplasm(1));
        [binranksg_cw nbinranksg_cw binabsg_cw nbinabsg_cw] = plotpolarity(ang_cw, mtrx(:, 4), bin_limits, 0, thecytoplasm(2));
        [binranksb_cw nbinranksb_cw binabsb_cw nbinabsb_cw] = plotpolarity(ang_cw, mtrx(:, 5), bin_limits, 0, thecytoplasm(3));
        
        % This chunk here shifts angles 15 degrees counter-clockwise and runs then the analysis considering angles between 0 and 90 0nly (e.g., 10 is 175 (which ends up as 5 after restricting to [0, 90]).
        bin_limits = 0:15:90;
        ang_ccw = mtrx(:, 1) - 15;
        ind = find(ang_ccw < 0.);
        ang_ccw(ind) = ang_ccw(ind) + 180.;
        ind = find(ang_ccw > 90.);
        ang_ccw(ind) = 180. - ang_ccw(ind);
        
        [binranksr_ccw nbinranksr_ccw binabsr_ccw nbinabsr_ccw] = plotpolarity(ang_ccw, mtrx(:, 3), bin_limits, 0, thecytoplasm(1));
        [binranksg_ccw nbinranksg_ccw binabsg_ccw nbinabsg_ccw] = plotpolarity(ang_ccw, mtrx(:, 4), bin_limits, 0, thecytoplasm(2));
        [binranksb_ccw nbinranksb_ccw binabsb_ccw nbinabsb_ccw] = plotpolarity(ang_ccw, mtrx(:, 5), bin_limits, 0, thecytoplasm(3));
        
        
        % This chunk here runs the analysis considering angles between 0 and 180 and the mode as background.
        bin_limits = 0:15:180;
        [binranksr_mode nbinranksr_mode binabsr_mode nbinabsr_mode] = plotpolarity(mtrx(:, 1), mtrx(:, 3), bin_limits, plot_flag, themode(1));
        [binranksg_mode nbinranksg_mode binabsg_mode nbinabsg_mode] = plotpolarity(mtrx(:, 1), mtrx(:, 4), bin_limits, plot_flag, themode(2));
        [binranksb_mode nbinranksb_mode binabsb_mode nbinabsb_mode] = plotpolarity(mtrx(:, 1), mtrx(:, 5), bin_limits, plot_flag, themode(3));
        
        % This chunk here runs the analysis considering angles between 0 and 90 0nly (e.g., 175 is 5 degrees) and the mode as background.
        bin_limits = 0:15:90;
        ang0_90 = mtrx(:, 1);
        ind = find(ang0_90 > 90.);
        ang0_90(ind) = 180. - ang0_90(ind);
        
        [binranksr0_90_mode nbinranksr0_90_mode binabsr0_90_mode nbinabsr0_90_mode] = plotpolarity(ang0_90, mtrx(:, 3), bin_limits, plot_flag, themode(1));
        [binranksg0_90_mode nbinranksg0_90_mode binabsg0_90_mode nbinabsg0_90_mode] = plotpolarity(ang0_90, mtrx(:, 4), bin_limits, plot_flag, themode(2));
        [binranksb0_90_mode nbinranksb0_90_mode binabsb0_90_mode nbinabsb0_90_mode] = plotpolarity(ang0_90, mtrx(:, 5), bin_limits, plot_flag, themode(3));
        
        % This chunk here shifts angles 15 degrees clockwise and runs then the analysis considering angles between 0 and 90 0nly (e.g., 175 is 10 degrees) and the mode as background.
        bin_limits = 0:15:90;
        ang_cw = mtrx(:, 1) + 15;
        ind = find(ang_cw > 180.);
        ang_cw(ind) = ang_cw(ind) - 180.;
        ind = find(ang_cw > 90.);
        ang_cw(ind) = 180. - ang_cw(ind);
        
        [binranksr_cw_mode nbinranksr_cw_mode binabsr_cw_mode nbinabsr_cw_mode] = plotpolarity(ang_cw, mtrx(:, 3), bin_limits, 0, themode(1));
        [binranksg_cw_mode nbinranksg_cw_mode binabsg_cw_mode nbinabsg_cw_mode] = plotpolarity(ang_cw, mtrx(:, 4), bin_limits, 0, themode(2));
        [binranksb_cw_mode nbinranksb_cw_mode binabsb_cw_mode nbinabsb_cw_mode] = plotpolarity(ang_cw, mtrx(:, 5), bin_limits, 0, themode(3));
        
        % This chunk here shifts angles 15 degrees counter-clockwise and runs then the analysis considering angles between 0 and 90 0nly (e.g., 10 is 175 (which ends up as 5 after restricting to [0, 90]) and the mode as background.
        bin_limits = 0:15:90;
        ang_ccw = mtrx(:, 1) - 15;
        ind = find(ang_ccw < 0.);
        ang_ccw(ind) = ang_ccw(ind) + 180.;
        ind = find(ang_ccw > 90.);
        ang_ccw(ind) = 180. - ang_ccw(ind);
        
        [binranksr_ccw_mode nbinranksr_ccw_mode binabsr_ccw_mode nbinabsr_ccw_mode] = plotpolarity(ang_ccw, mtrx(:, 3), bin_limits, 0, themode(1));
        [binranksg_ccw_mode nbinranksg_ccw_mode binabsg_ccw_mode nbinabsg_ccw_mode] = plotpolarity(ang_ccw, mtrx(:, 4), bin_limits, 0, themode(2));
        [binranksb_ccw_mode nbinranksb_ccw_mode binabsb_ccw_mode nbinabsb_ccw_mode] = plotpolarity(ang_ccw, mtrx(:, 5), bin_limits, 0, themode(3));
        
        if nargin < 3 % Display results only if not running this batch mode.
            fprintf('Cytoplasmic brightness (red, green, blue)\n');
            disp(thecytoplasm);
            fprintf('Mode image brightness (red, green, blue)\n');
            disp(themode);
            fprintf('Mean image brightness (red, green, blue)\n');
            disp(themean);
            
            fprintf('AVERAGES\n');
            fprintf('RED (trajectories grouped in 15-degree bins in the [0-180] degree interval)\n');
            disp(binabsr);
            fprintf('RED (angles converted t0 [0-90] degrees -e.g. 105 degrees = 75 degrees- and grouped in 15-degree intervals)\n');
            disp(binabsr0_90);
            
            fprintf('GREEN (trajectories grouped in 15-degree bins in the [0-180] degree interval)\n');
            disp(binabsg);
            fprintf('GREEN (angles converted t0 [0-90] degrees -e.g. 105 degrees = 75 degrees- and grouped in 15-degree intervals)\n');
            disp(binabsg0_90);
            
            fprintf('BLUE (trajectories grouped in 15-degree bins in the [0-180] degree interval)\n');
            disp(binabsb);
            fprintf('BLUE (angles converted t0 [0-90] degrees -e.g. 105 degrees = 75 degrees- and grouped in 15-degree intervals)\n');
            disp(binabsb0_90);
        end
        
        if (~ isequal(filename, 0))
            [tmp1 tmp2 theext] = fileparts(filename);
            theext = cat(2, '*', theext);
            
            % If the extension is written, use that mode.
            if (~ isempty(theext))
                for ie = 1:numel(extensions)
                    if strcmp(extensions{ie}, theext)
                        ext = ie;
                        break;
                    end
                end
            end
            
            cd(ud.rcurrdir);
            if (strcmp(extensions{ext}, '*.csv'))
                 %csvwrite(cat(2, pathname, filename), mtrx, 0, 0);
                
                %csvwrite(cat(2, pathname, filename), binranks, size(mtrx, 1) + 1, 0);
                %csvwrite(cat(2, pathname, filename), nbinranks, size(mtrx, 1) + 3, 0);
                %                                  dlmwrite(cat(2, pathname, filename), ['Cytoplasmic intensity (R,G,B)'], 'delimiter', '');
                %                                  dlmwrite(cat(2, pathname, filename), thecytoplasm, '-append', 'roffset', 0, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), ['Mode image intensity  (R,G,B)'], 'delimiter', '', '-append', 'roffset', 1, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), themode, '-append', 'roffset', 0, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), ['Mean image intensity  (R,G,B)'], 'delimiter', '', '-append', 'roffset', 1, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), themean, '-append', 'roffset', 0, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), ['A,L,R,G,B'], 'delimiter', '', '-append', 'roffset', 1, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), mtrx, '-append', 'roffset', 0, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), ['Cytoplasmic intensity subtracted as background'], 'delimiter', '', '-append', 'roffset', 1, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), ['R'], '-append', 'roffset', 1, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), binabsr, '-append', 'roffset', 0, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), nbinabsr, '-append', 'roffset', 0, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), binranksr, '-append', 'roffset', 0, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), nbinranksr, '-append', 'roffset', 0, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), binabsr0_90, '-append', 'roffset', 0, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), nbinabsr0_90, '-append', 'roffset', 0, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), binabsr_cw, '-append', 'roffset', 0, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), nbinabsr_cw, '-append', 'roffset', 0, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), binabsr_ccw, '-append', 'roffset', 0, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), nbinabsr_ccw, '-append', 'roffset', 0, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), ['G'], '-append', 'roffset', 1, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), binabsg, '-append', 'roffset', 0, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), nbinabsg, '-append', 'roffset', 0, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), binranksg, '-append', 'roffset', 0, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), nbinranksg, '-append', 'roffset', 0, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), binabsg0_90, '-append', 'roffset', 0, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), nbinabsg0_90, '-append', 'roffset', 0, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), binabsg_cw, '-append', 'roffset', 0, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), nbinabsg_cw, '-append', 'roffset', 0, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), binabsg_ccw, '-append', 'roffset', 0, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), nbinabsg_ccw, '-append', 'roffset', 0, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), ['B'], '-append', 'roffset', 1, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), binabsb, '-append', 'roffset', 0, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), nbinabsb, '-append', 'roffset', 0, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), binranksb, '-append', 'roffset', 0, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), nbinranksb, '-append', 'roffset', 0, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), binabsb0_90, '-append', 'roffset', 0, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), nbinabsb0_90, '-append', 'roffset', 0, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), binabsb_cw, '-append', 'roffset', 0, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), nbinabsb_cw, '-append', 'roffset', 0, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), binabsb_ccw, '-append', 'roffset', 0, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), nbinabsb_ccw, '-append', 'roffset', 0, 'coffset', 0);
                %                                  
                %                                  % Also write mode-substracted data.
                %                                  dlmwrite(cat(2, pathname, filename), ['Mode subtracted as background'], 'delimiter', '', '-append', 'roffset', 1, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), ['R'], '-append', 'roffset', 1, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), nbinabsr_mode, '-append', 'roffset', 0, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), nbinabsr0_90_mode, '-append', 'roffset', 0, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), nbinabsr_cw_mode, '-append', 'roffset', 0, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), nbinabsr_ccw_mode, '-append', 'roffset', 0, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), ['G'], '-append', 'roffset', 1, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), nbinabsg_mode, '-append', 'roffset', 0, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), nbinabsg0_90_mode, '-append', 'roffset', 0, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), nbinabsg_cw_mode, '-append', 'roffset', 0, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), nbinabsg_ccw_mode, '-append', 'roffset', 0, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), ['B'], '-append', 'roffset', 1, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), nbinabsb_mode, '-append', 'roffset', 0, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), nbinabsb0_90_mode, '-append', 'roffset', 0, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), nbinabsb_cw_mode, '-append', 'roffset', 0, 'coffset', 0);
                %                                  dlmwrite(cat(2, pathname, filename), nbinabsb_ccw_mode, '-append', 'roffset', 0, 'coffset', 0);
                %                                  
                savecsv(cat(2, pathname, filename), 'Cytoplasmic brightness (red, green, blue)', thecytoplasm, ' ', 'Mode image brightness (red, green, blue)', themode, ' ', 'Mean image brightness (red, green, blue)', themean, ' ', 'SINGLE TRAJECTORIES', 'Angle, Length, Red, Green, Blue', mtrx,' ', 'AVERAGES', 'RED (trajectories grouped in 15-degree bins in the [0-180] degree interval)', binabsr, 'RED (angles converted t0 [0-90] degrees -e.g. 105 degrees = 75 degrees- and grouped in 15-degree intervals)', binabsr0_90, ' ', 'GREEN (trajectories grouped in 15-degree bins in the [0-180] degree interval)', binabsg, 'GREEN (angles converted t0 [0-90] degrees -e.g. 105 degrees = 75 degrees- and grouped in 15-degree intervals)', binabsg0_90, ' ', 'BLUE (trajectories grouped in 15-degree bins in the [0-180] degree interval)', binabsb, 'BLUE (angles converted t0 [0-90] degrees -e.g. 105 degrees = 75 degrees- and grouped in 15-degree intervals)', binabsb0_90);
                
            else
                save(cat(2, pathname, filename), 'mtrx', 'binabsr', 'nbinabsr', 'binabsg', 'nbinabsg', 'binabsb', 'nbinabsb', 'binranksr', 'nbinranksr', 'binranksg', 'nbinranksg', 'binranksb', 'nbinranksb', 'binabsr0_90', 'nbinabsr0_90', 'binabsg0_90', 'nbinabsg0_90', 'binabsb0_90', 'nbinabsb0_90', 'binranksr0_90', 'nbinranksr0_90', 'binranksg0_90', 'nbinranksg0_90', 'binranksb0_90', 'nbinranksb0_90', 'thecytoplasm', 'themean', 'nbinabsr_mode', 'nbinabsr0_90_mode', 'nbinabsg_mode', 'nbinabsg0_90_mode', 'nbinabsb_mode', 'nbinabsb0_90_mode', 'nbinabsr_cw_mode', 'nbinabsr_ccw_mode', 'nbinabsg_cw_mode', 'nbinabsg_ccw_mode', 'nbinabsb_cw_mode', 'nbinabsb_ccw_mode');
            end
            cd(curr_dir);
        end
        
    case {'signal_polarity_gradient', 'k_G'}
        if nargin < 3
            [tpo tpf thebinsz themethod thesmoothing thekernelsz filename pathname ext bg_pixorientation_method] = dialog_gradientpolarity([0 (ud.imsize(3)-1)], [ud.paramtpo ud.paramtpf], ud.param_gradpol_bin_sz, ud.param_gradpol_method, ud.param_gradpol_smoothing, ud.param_gradpol_kernel_sz, ud.param_gradpol_bg_pixorientation);
        else
            filename = varargin{3};
            [pathname filename] = fileparts(filename);
            tpo = ud.curslice;
            tpf = tpo;
            thebinsz = ud.param_gradpol_bin_sz;
            themethod = ud.param_gradpol_method;
            thesmoothing = ud.param_gradpol_smoothing;
            thekernelsz = ud.param_gradpol_kernel_sz;
            bg_pixorientation_method = ud.param_gradpol_bg_pixorientation;
        end
        
        
        if tpo ~= -1 % If not cancelled ...
            Awg = []; % Grayscale.
            Awgr = []; % Color.
            Awgg = [];
            Awgb = [];
            
            if (~ isa(ud.slices, 'dip_image_array'))% Grayscale images.
                for ii = tpo:tpf
                    
                    thebg = -1;
                    switch(bg_pixorientation_method)
                        case 1 % no background, pixel orientation calculated for each channel independently
                            thebg = 0;
                        case 2 % background and pixel orientation calculated for each channel independently
                            thebg = ud.slices(:, :, ii);
                            
                        otherwise
                            disp('Invalid background and pixel orientation parameter for a greyscale image.');
                            return;
                    end
                    
                    Awg(end+1, :) = orientation2(ud.slices(:, :, ii), thebinsz, themethod, thesmoothing, thekernelsz, thebg);
                end
                
                % Save data.
                if (~ isequal(filename, 0))
                    switch ext
                        case 1 % csv
                            savecsv(cat(2, pathname, filename), ...
                                'Bin size', thebinsz, ' ', 'Method', themethod, ' ', 'Sigma', thesmoothing, ' ', 'Gradient kernel size', thekernelsz, ' ', 'Method to calculate background and pixel orientation (1 no background, pixel orientation calculated for each channel independently; 2 background and pixel orientation calculated for each channel independently; 3 background and pixel orientation calculated using red channel; 4 background and pixel orientation calculated using green channel; 5 background and pixel orientation calculated using blue channel)', bg_pixorientation_method, ' ', ' ', ...
                                'Measurements (one line per image)', Awg);
                        case 2% mat
                            save(cat(2, pathname, filename), 'thebinsz', 'themethod', 'thesmoothing', 'thekernelsz', 'bg_pixorientation_method', 'Awg');
                            
                    end
                end
                
                disp(Awg);
                
            else % Color images.
                for ii = tpo:tpf
                    thebg = -1;
                    
                    switch(bg_pixorientation_method)
                        case 1 % no background, pixel orientation calculated for each channel independently
                            thebgr = 0;
                            thebgg = 0;
                            thebgb = 0;
                        case 2 % background and pixel orientation calculated for each channel independently
                            thebgr = ud.slices{1}(:, :, ii);
                            thebgg = ud.slices{2}(:, :, ii);
                            thebgb = ud.slices{3}(:, :, ii);
                        case 3 % background and pixel orientation calculated using red channel
                            thebgr = ud.slices{1}(:, :, ii);
                            thebgg = ud.slices{1}(:, :, ii);
                            thebgb = ud.slices{1}(:, :, ii);
                        case 4 % background and pixel orientation calculated using green channel
                            thebgr = ud.slices{2}(:, :, ii);
                            thebgg = ud.slices{2}(:, :, ii);
                            thebgb = ud.slices{2}(:, :, ii);
                        case 5 % background and pixel orientation calculated using blue channel
                            thebgr = ud.slices{3}(:, :, ii);
                            thebgg = ud.slices{3}(:, :, ii);
                            thebgb = ud.slices{3}(:, :, ii);
                            
                        otherwise
                            disp('Invalid background and pixel orientation parameter for a color image.');
                            return;
                    end
                    
                    Awgr(end+1, :) = orientation2(ud.slices{1}(:, :, ii), thebinsz, themethod, thesmoothing, thekernelsz, thebgr);
                    Awgg(end+1, :) = orientation2(ud.slices{2}(:, :, ii), thebinsz, themethod, thesmoothing, thekernelsz, thebgg);
                    Awgb(end+1, :) = orientation2(ud.slices{3}(:, :, ii), thebinsz, themethod, thesmoothing, thekernelsz, thebgb);
                end
                
                % Save data.
                if (~ isequal(filename, 0))
                    switch ext
                        case 1 % csv
                            savecsv(cat(2, pathname, filename), ...
                                'Bin size', thebinsz, ' ', 'Method', themethod, ' ', 'Sigma', thesmoothing, ' ', 'Gradient kernel size', thekernelsz, ' ', 'Method to calculate background and pixel orientation (1 no background, pixel orientation calculated for each channel independently; 2 background and pixel orientation calculated for each channel independently; 3 background and pixel orientation calculated using red channel; 4 background and pixel orientation calculated using green channel; 5 background and pixel orientation calculated using blue channel)', bg_pixorientation_method, ' ', ' ', ...
                                'Red channel (one line per image)', Awgr, ' ', 'Green channel (one line per image)', Awgg, ' ', 'Blue channel (one line per image)', Awgb);
                        case 2% mat
                            save(cat(2, pathname, filename), 'thebinsz', 'themethod', 'thesmoothing', 'thekernelsz', 'bg_pixorientation_method', 'Awgr', 'Awgg', 'Awgb');
                            
                    end
                end
                
                disp(Awgr);
                disp(Awgg);
                disp(Awgb);
            end
            
            
            % Update parameter values.
            ud.paramtpo = tpo;
            ud.paramtpf = tpf;
            ud.param_gradpol_bin_sz = thebinsz;
            ud.param_gradpol_method = themethod;
            ud.param_gradpol_smoothing = thesmoothing;
            ud.param_gradpol_kernel_sz = thekernelsz;
            ud.param_gradpol_bg_pixorientation = bg_pixorientation_method;
            
            set(fig, 'UserData', []);
            set(fig, 'UserData', ud);
        end
        
    case {'signal_level'}
        curr_dir = pwd;
        cd(ud.rcurrdir);
        [filename, pathname, ext] = uiputfile(extensions, 'Save quantification data');
        cd(curr_dir);
        
        mtrx = cb_multicolorquantifypoly(fig, [1 2 3], ud.rpolygons);
        
        meanint = [mean(nonzeros(mtrx(:, 1))) mean(nonzeros(mtrx(:, 2))) mean(nonzeros(mtrx(:, 3)))];
        stdint = [std(nonzeros(mtrx(:, 1))) std(nonzeros(mtrx(:, 2))) std(nonzeros(mtrx(:, 3)))];
        
        fprintf('\n\nRed\n---\n');
        disp(mtrx(:, 1)');
        fprintf('mean of non-zero values: %.2f, standard deviation: %.2f\n\n', meanint(1), stdint(1));
        
        fprintf('Green\n---\n');
        disp(mtrx(:, 2)');
        fprintf('mean of non-zero values: %.2f, standard deviation: %.2f\n\n', meanint(2), stdint(2));
        
        fprintf('Blue\n---\n');
        disp(mtrx(:, 3)');
        fprintf('mean of non-zero values: %.2f, standard deviation: %.2f\n\n', meanint(3), stdint(3));
        
        if (~ isequal(filename, 0))
            [tmp1 tmp2 theext] = fileparts(filename);
            theext = cat(2, '*', theext);
            
            % If the extension is written, use that mode.
            if (~ isempty(theext))
                for ie = 1:numel(extensions)
                    if strcmp(extensions{ie}, theext)
                        ext = ie;
                        break;
                    end
                end
            end
            
            if (strcmp(extensions{ext}, '*.csv'))
                savecsv(cat(2, pathname, filename), 'Red', mtrx(:, 1)', 'Green', mtrx(:, 2)', 'Blue', mtrx(:, 3)', 'Mean of all regions', meanint, 'standard deviation', stdint);
                %                                  dlmwrite(cat(2, pathname, filename), ['R', 'G', 'B', 'S']);
                %  				dlmwrite(cat(2, pathname, filename), mtrx, '-append', 'roffset', 0, 'coffset', 0);
                %  				dlmwrite(cat(2, pathname, filename), ['M'], '-append', 'roffset', 1, 'coffset', 0);
                %  				dlmwrite(cat(2, pathname, filename), meanint, '-append', 'roffset', 0, 'coffset', 0);
                %  				dlmwrite(cat(2, pathname, filename), ['S'], '-append', 'roffset', 1, 'coffset', 0);
                %  				dlmwrite(cat(2, pathname, filename), stdint, '-append', 'roffset', 0, 'coffset', 0);
            else
                save(cat(2, pathname, filename), 'mtrx', 'meanint', 'stdint');
            end
        end
        
    case 'measure_poly'
        curr_dir = pwd;
        cd(ud.rcurrdir);
        
        m = [];
        
        [tpo tpf] = dialog_timepoints([0 (ud.imsize(3)-1)]);
        
        if tpo ~= -1
            if nargin < 3
                [filename, pathname, ext] = uiputfile(extensions_matfirst, 'Save polyline measurements');
            else
                filename = varargin{3};
            end
            if ~(isequal(filename,0) || isequal(pathname,0)) % if user doesn't cancel
                
                %currind = ud.curslice + 1;
                tpo = tpo + 1;
                tpf = tpf + 1;
                wb = waitbar(0, 'If you are seeing this your system is SLOW!!! Just wait then ...', ...
                    'Color', 'w');
                for ii = tpo:tpf
                    wb = waitbar((ii-tpo)/(tpf-tpo+1), wb, cat(2, 'Measuring image ', cat(2, num2str(ii-tpo+1), cat(2, '/', num2str(tpf-tpo+1)))));
                    if numel(ud.rpolygons(:, :, ii)) > 0
                        % Color or grayscale images? For now, do the same in both cases.
                        if ~isa(ud.imagedata, 'dip_image_array')
                            m{ii-tpo+1} = measurepoly(cell(ud.rpolygons(:, 1, ii)), {'center'; 'area'; 'perimeter'; 'length'; 'length_ellipse'; 'brightness'}, squeeze(ud.slices(:, :, ii-1)), ud.rBRUSH_SZ);
                        else
                            ichannel = 1:3;
                            m{ii-tpo+1} = measurepoly(cell(ud.rpolygons(:, 1, ii)), {'center'; 'area'; 'perimeter'; 'length'; 'length_ellipse'; 'brightness'}, squeeze(ud.slices{ichannel}(:, :, ii-1)), ud.rBRUSH_SZ);
                        end
                        
                        
                        % This adds a fiducial at the center of each cell. ------
                        %  			for i = 1:numel(m.id)
                        %  				[ud.rfiducials ud.rnfiducials] = cb_storecurrentpoint([m(i).Center' ud.curslice], ud.rfiducials, ud.rnfiducials);
                        %  				cb_drawpoint(fig, m(i).Center', fiducial_sz, 'rx', 'Fiducial');
                        %  				cb_paintfiduciallabels(fig, ud.rfiducials, fiducial_label_sz, fiducial_color, ud.rflag_fiduciallabels);
                        %
                        %  			end
                        % end This adds a fiducial at the center of each cell. ------
                    end
                end
                
                % Display results.
                fprintf('\nDisplaying the first time point (%d) measured:\n', tpo-1);
                disp(m{1});
                
                % Save results.
                if (~ isequal(filename, 0))
                    % As a mat file if so selected.
                    if (strcmp(extensions_matfirst{ext}, '*.mat'))
                        save(cat(2, pathname, filename), 'm');
                        
                        % As a csv file ...
                    elseif (strcmp(extensions_matfirst{ext}, '*.csv'))
                        % Get the names of all the fields.
                        thefields = fieldnames(m{1});
                        
                        % Put combinations of fied_name, field_data in a cell array that will be saved later.
                        theargs = [];
                        tps = tpo:tpf;
                        %                                          for ii = 1:numel(tps)
                        %                                                  % Also add headings indicating the time point if more than one.
                        %                                                  theargs{end+1} = cat(2, 'Time point ', num2str(tps(ii)-1)); %ii - 1, as tpo and tpf correspond to the time point + 1 (time points start in zero).
                        %
                        %                                                  for jj = 1:numel(thefields)
                        %                                                          theargs{end+1} = thefields{jj};
                        %                                                          theargs{end+1} = m{ii}.(thefields{jj});
                        %                                                  end
                        %                                                  theargs{end+1} = ' ';
                        %                                          end
                        
                        theargs{end+1} = 'Time points';
                        theargs{end+1} = tps-1;
                        theargs{end+1} = ' ';
                        
                        % For every field ...
                        for jj = 1:numel(thefields)
                            % Take the data from all time points.
                            thedata = [];
                            sz = size(m{1}.(thefields{jj}), 1);
                            for ii = 1:numel(tps)
                                thedata(1:size((m{ii}.(thefields{jj}))', 1), (end+1):(end+sz)) = (m{ii}.(thefields{jj}))';
                            end
                            
                            % Store field name and data.
                            theargs{end+1} = thefields{jj};
                            theargs{end+1} = thedata;
                            theargs{end+1} = ' ';
                        end
                        
                        savecsv(cat(2, pathname, filename), theargs);
                    end
                end
                
                waitbar(1, wb, 'Done!');
                close(wb);
                cd(curr_dir);
                set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
                set(fig, 'UserData', ud);
            end
        end
        
    case 'plot_poly'
        % Read parameters.
        [timepoints scale threeD show_axes numcolors] = dialog_plotpoly(ud);
        
        % The user may only want to plot the polygons at specific time points. Keeping that in mind, we create a new user data structure containing only the polygons from the appropriate time points.
        if (~ isempty(timepoints)) && (numcolors >= numel(timepoints)) % At least as many colors as time points need to be specified.
            ud2 = ud;
            %ud2.rpolygons = ud.rpolygons(timepoints);
            for ii = 1:size(ud.rpolygons, 3)
                if isempty(find(timepoints == ii))
                    ud2.rpolygons(:, :, ii) = {[]};
                end
            end
            
            overlay_polygons(ud2, threeD, scale, show_axes, numcolors);
            
            ud.paramplotpoly_threeD = threeD;
            ud.paramplotpoly_axes = show_axes;
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud);
        elseif (numcolors < numel(timepoints))
            fprintf('\nYou need to choose at least as many colors as time points you have selected.\n');
        end
        
    case 'sel_cells'
        disp(cb_getselectedobjects(ud));
        
    case 'msr_tri'
        curr_dir = pwd;
        cd(ud.rcurrdir);
        
        if nargin < 3
            [filename, pathname, ext] = uiputfile({'*.mat'}, 'Save measurements', volume_analysis_filename);
        else
            filename = varargin{3};
            % These manipulations are so that pathname and filename contain the same info here and if no parameter is provided.
            [pathname, filename, ext] = fileparts(filename);
            filename = cat(2, filename, ext);
        end
        
        cd(curr_dir);
        
        m = measuretri(ud.slices, [], {'vol', 'surf', 'height', 'cyl', 'hex', 'neighbors', 'type'}, cb_getselectedobjects(ud));
        %m = measuretri(ud.slices, [], {'neighbors'}, cb_getselectedobjects(ud));
        if (~ isequal(filename, 0))
            %			if (strcmp(extensions{ext}, '*.csv'))
            %				dlmwrite(cat(2, pathname, filename), ['A', 'L', 'R', 'G', 'B']);
            %				dlmwrite(cat(2, pathname, filename), mtrx, '-append', 'roffset', 0, 'coffset', 0);
            %				dlmwrite(cat(2, pathname, filename), ['R'], '-append', 'roffset', 1, 'coffset', 0);
            %				dlmwrite(cat(2, pathname, filename), binranksr, '-append', 'roffset', 0, 'coffset', 0);
            %				dlmwrite(cat(2, pathname, filename), nbinranksr, '-append', 'roffset', 0, 'coffset', 0);
            %				dlmwrite(cat(2, pathname, filename), ['G'], '-append', 'roffset', 1, 'coffset', 0);
            %				dlmwrite(cat(2, pathname, filename), binranksg, '-append', 'roffset', 0, 'coffset', 0);
            %				dlmwrite(cat(2, pathname, filename), nbinranksg, '-append', 'roffset', 0, 'coffset', 0);
            %				dlmwrite(cat(2, pathname, filename), ['B'], '-append', 'roffset', 1, 'coffset', 0);
            %				dlmwrite(cat(2, pathname, filename), binranksb, '-append', 'roffset', 0, 'coffset', 0);
            %				dlmwrite(cat(2, pathname, filename), nbinranksb, '-append', 'roffset', 0, 'coffset', 0);
            %			else
            save(fullfile(pathname, filename), 'm');
            %			end
        else
            disp(m);
        end
        
    case {'sel_two'; 'k_w'}
        figure(fig);
        set(fig, 'WindowButtonDownFcn', 'mycallbacks2(gcbf, ''sel_two_down'');');
        set(fig, 'WindowButtonUpFcn', '');
        set(fig, 'WindowButtonMotionFcn', 'show_coord_pix(gcf);');
        set(fig, 'Pointer', 'crossh');
        
    case 'sel_two_down'
        coords = cb_getcoords(fig);
        
        % If the right button was used to unselect, coords is set to
        % an empty vector.
        if ~isempty(coords)
            [ud.rselectedfiducials] = cb_selectcurrentpoint(fig, ud.rfiducials, ud.rselectedfiducials);
            cb_paintpoints(fig, ud.rfiducials, fiducial_sz, fiducial_color, ud.rselectedfiducials, selected_fiducial_color);
            cb_paintfiduciallabels(fig, ud.rfiducials, fiducial_label_sz, fiducial_color, ud.rflag_fiduciallabels);
        end
        
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud);
        
    case 'pair_dist'
        curr_dir = pwd;
        cd(ud.rcurrdir);
        
        [filename, pathname, ext] = uiputfile({'*.mat'}, 'Save pair distance analysis', '');
        cd(curr_dir);

        cut_t = ud.curslice + 1;
        p1 = ud.rfiducials(ud.rselectedfiducials(1), 1:2, :);
        p1 = reshape(p1, [size(p1, 3), 2, 1]);
        p1 = reshape(p1, [2, size(p1, 1), 1])';
        p2 = ud.rfiducials(ud.rselectedfiducials(2), 1:2, :);
        p2 = reshape(p2, [size(p2, 3), 2, 1]);
        p2 = reshape(p2, [2, size(p2, 1), 1])';
        
        if (~ isequal(filename, 0))
            l = cut_analysis(p1, p2, [], [], cut_t, t_res, 1, 0);
            save(cat(2, pathname, filename), 'l', 'cut_t');
        else
            cut_analysis(p1, p2, [], [], cut_t, t_res, 1, 0);
        end
        
    case 'local_cut'
        curr_dir = pwd;
        cd(ud.rcurrdir);
        
        [filename, pathname, ext] = uiputfile({'*.mat'}, 'Save local cut analysis', local_analysis_filename);

        cd(curr_dir);
        cut_t = ud.curslice + 1;
        
        first = 1;
        
        % Analysis including fiducials.
        if isfield(ud, 'rfiducials') & (~isempty(ud.rfiducials)) & nnz(ud.rnfiducials > 0)
            last = size(ud.rfiducials, 3);
            for i = 1:size(ud.rfiducials, 3)
                if ud.rfiducials(1, 1, i) == -1
                    first = first + 1;
                else
                    break;
                end
            end
            
            for i = size(ud.rfiducials, 3):-1:1
                if ud.rfiducials(1, 1, i) == -1
                    last = last - 1;
                else
                    break;
                end
            end
            
            p1 = ud.rfiducials(1, 1:2, first:last);
            p1 = reshape(p1, [size(p1, 3), 2, 1]);
            p1 = reshape(p1, [2, size(p1, 1), 1])';
            p2 = ud.rfiducials(2, 1:2, first:last);
            p2 = reshape(p2, [size(p2, 3), 2, 1]);
            p2 = reshape(p2, [2, size(p2, 1), 1])';
            % Analysis without fiducials.
        else
            last = size(ud.rpolygons, 3);
            for i = 1:size(ud.rpolygons, 3)
                if isempty(ud.rpolygons{1, 1, i})
                    first = first + 1;
                else
                    break;
                end
            end
            
            for i = size(ud.rpolygons, 3):-1:1
                if isempty(ud.rpolygons{1, 1, i})
                    last = last - 1;
                else
                    break;
                end
            end
            
            p1 = [];
            p2 = [];
            
        end
        
        cut_t = cut_t - first + 1;
        
        if (~ isequal(filename, 0))
            [l a p] = cut_analysis(p1, p2, ud.rpolygons(:, :, first:last), [], cut_t, t_res, 1, 0, ud.slices);
            save(cat(2, pathname, filename), 'l', 'a', 'p', 'cut_t');
        else
            cut_analysis(p1, p2, ud.rpolygons(:, :, first:last), [], cut_t, t_res, 1, 0, ud.slices);
        end
        
    case 'vector_field'
        curr_dir = pwd;
        
        [tpo tpf] = dialog_timepoints([0 (ud.imsize(3)-1)]);
        if tpo ~= -1
            cd(ud.rcurrdir);
            [filename, pathname, ext] = uiputfile({'*.mat'}, 'Save field analysis', field_analysis_filename);
            cd(curr_dir);
            if (~ isequal(filename, 0))
                %currind = ud.curslice + 1;
                tpo = tpo + 1;
                tpf = tpf + 1;
                if (tpo < ud.imsize(3))
                    vertex1_before = ud.rdelimiters(1, 1:2, tpo);
                    vertex2_before = ud.rdelimiters(2, 1:2, tpo);
                    vertex1_after = ud.rdelimiters(1, 1:2, tpf);
                    vertex2_after = ud.rdelimiters(2, 1:2, tpf);
                    p1 = cat(1, ud.rfiducials(:, 1:2, tpo), cat(1, vertex1_before, vertex2_before));
                    tmp = nonzeros(p1+1);
                    p1 = reshape(tmp, [numel(tmp)/(size(p1, 2) * size(p1, 3)) size(p1, 2) size(p1, 3)])-1;
                    p2 = cat(1, ud.rfiducials(:, 1:2, tpf), cat(1, vertex1_after, vertex2_after));
                    tmp = nonzeros(p2+1);
                    p2 = reshape(tmp, [numel(tmp)/(size(p2, 2) * size(p2, 3)) size(p2, 2) size(p2, 3)])-1;
                    
                    if (~ isequal(filename, 0))
                        [dr_um dth_um cut_p1_um cut_p1_rot_angle_deg] = vector_field_analysis(p1, p2, vertex1_before, vertex2_before, ud.runiquecoords(tpo, :), 0, 0);
                        save(cat(2, pathname, filename), 'dr_um', 'dth_um', 'cut_p1_um', 'cut_p1_rot_angle_deg');
                    else
                        vector_field_analysis(p1, p2, vertex1_before, vertex2_before, ud.runiquecoords(tpo, :), 1, 0);
                    end
                end
            end
        end
        
    case {'vf_time', 'k_V'}
        curr_dir = pwd;
        cd(ud.rcurrdir);
        
        [tpo tpf] = dialog_timepoints([0 (ud.imsize(3)-1)]);
        
        if tpo ~= -1
            [filename, pathname, ext] = uiputfile({'*.mat'}, 'Save field analysis', field_analysis_filename);
            cd(curr_dir);
            if (~ isequal(filename, 0))
                %currind = ud.curslice + 1;
                tpo = tpo + 1;
                tpf = tpf + 1;
                
                % cut_t is relative to tpo, tpo being index number 1.
                cut_t = (ud.curslice+1) - tpo + 1;
                if cut_t < 1
                    cut_t = ((tpf - tpo) / 3) + 1;
                end
                
                if (tpo < ud.imsize(3))
                    vertex1 = ud.rdelimiters(1, 1:2, tpo:tpf);
                    vertex2 = ud.rdelimiters(2, 1:2, tpo:tpf);
                    p = cat(1, ud.rfiducials(1:ud.rnfiducials(tpo), 1:2, tpo:tpf), cat(1, vertex1, vertex2));
                    tmp = nonzeros(p+1);
                    p = reshape(tmp, [numel(tmp)/(size(p, 2) * size(p, 3)) size(p, 2) size(p, 3)])-1;
                    
                    if (~ isequal(filename, 0))
                        [dr_um_t dth_um_t cut_p1_um_t cut_p1_rot_angle_deg_t] = vector_field_time_analysis(p, vertex1, vertex2, ud.runiquecoords(tpo:tpf, :), 0, 0, cut_t);
                        save(cat(2, pathname, filename), 'dr_um_t', 'dth_um_t', 'cut_p1_um_t', 'cut_p1_rot_angle_deg_t');
                    else
                        vector_field_time_analysis(p, vertex1, vertex2, ud.runiquecoords(tpo:tpf, :), 1, 0, cut_t);
                    end
                end
            end
        end
        cd(curr_dir);
        
        % Michael edit
    case 'startDivAnalyze'
        out=startDivAnalyze(ud); % This should be saved to a .mat or .csv file.
        %out=cell2mat(out(:,4));
        % Michael edit ends
        
    case {'toggle_fiduid', 'k_i'}
        ud.rflag_fiduciallabels = ~ ud.rflag_fiduciallabels;
        if ud.rflag_fiduciallabels
            cb_paintfiduciallabels(fig, ud.rfiducials, fiducial_label_sz, fiducial_color, ud.rflag_fiduciallabels);
            cb_painttrajectorylabels(fig, ud.rtrajectories, tr_label_sz, line_color, ud.rflag_fiduciallabels);
        else
            cb_erasefiduciallabels(fig);
            cb_erasetrajectorylabels(fig);
        end
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud);
        
    case 'segment'
        [segm geom ud] = cb_segment(fig, 0);
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud);
        set(fig, 'Pointer', 'arrow');
        cb_paintpoints(fig, ud.rfiducials, fiducial_sz, fiducial_color, ud.rselectedfiducials, selected_fiducial_color);
        
    case {'find_seeds', 'k_S'}
        [segm geom ud] = cb_segment(fig, 1);
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud);
        cb_paintpoints(fig, ud.rfiducials, fiducial_sz, fiducial_color, ud.rselectedfiducials, selected_fiducial_color);
        cb_paintfiduciallabels(fig, ud.rfiducials, fiducial_label_sz, fiducial_color, ud.rflag_fiduciallabels);

        set(fig, 'Pointer', 'arrow');
        
    case {'propagate_seeds', 'k_E'} % 'T' as in tracking or transfer.
        [tpo tpf correlationws] = dialog_opticflow([0 (ud.imsize(3)-1)], ud.paramcorrelationws);      
        if tpo ~= -1
            ud = cb_propagateseeds(fig, tpo+1, tpf+1, correlationws);
            ud.paramcorrelationws = correlationws; % Store this parameter.
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud);
            cb_paintpoints(fig, ud.rfiducials, fiducial_sz, fiducial_color, ud.rselectedfiducials, selected_fiducial_color);
            cb_paintfiduciallabels(fig, ud.rfiducials, fiducial_label_sz, fiducial_color, ud.rflag_fiduciallabels);
        end
        
    case {'expand_seeds', 'k_X'}
        [segm geom ud] = cb_segment(fig, ud.rfiducials);
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud);
        cb_paintpoints(fig, ud.rfiducials, fiducial_sz, fiducial_color, ud.rselectedfiducials, selected_fiducial_color);
        cb_paintpolylines(fig, ud.rpolygons, poly_sz, poly_color);
        cb_paintfiduciallabels(fig, ud.rfiducials, fiducial_label_sz, fiducial_color, ud.rflag_fiduciallabels);
        
        % Expand the seeds in the current time point and generate seeds for the next time point based on the centroid position of the cells identified in the current one.
        
    case {'expandnpropagate_seeds', 'k_Z'}
        [segm geom ud] = cb_segment(fig, ud.rfiducials, 1);
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud);
        cb_paintpoints(fig, ud.rfiducials, fiducial_sz, fiducial_color, ud.rselectedfiducials, selected_fiducial_color);
        cb_paintpolylines(fig, ud.rpolygons, poly_sz, poly_color);
        cb_paintfiduciallabels(fig, ud.rfiducials, fiducial_label_sz, fiducial_color, ud.rflag_fiduciallabels);
        
    case {'kymograph', 'k_K'}
        figure(fig);
        set(fig, 'WindowButtonDownFcn', 'mycallbacks2(gcbf, ''kymograph_down'');');
        set(fig, 'WindowButtonMotionFcn', 'show_coord_pix(gcf);');
        set(fig, 'WindowButtonUpFcn', '');
        set(fig, 'Pointer', 'crossh');
        
    case 'kymograph_down'
        cb_kymograph(fig);
        
    case 'quick_guide'
        dialog_quickguide;
        
    case 'help'
        dialog_help;
        
    case 'about'
        dialog_about;
        
    case {'snakes', 'k_M'}
        try
            mean_in=mean(ud.slices);
            if mean_in<500
                sigmagradient=1;
            elseif mean_in>=500 && mean_in<2000
                sigmagradient=30;
            elseif mean_in>2000 && mean_in<12500
                sigmagradient=60;
            elseif mean_in>=12500 && mean_in<22000
                sigmagradient=100;
            elseif mean_in>=22000
                sigmagradient=200;
            end
            
            [tpo tpf kappax kappay beta win_size sigmagradient AreaTol] = dialog_snakes([0, ud.imsize(3)-1 ud.curslice+1], ud.param_snakes_kappax, ud.param_snakes_kappay, ud.param_snakes_beta, ud.param_snakes_win_sz,sigmagradient,ud.param_snakes_areaTol);
            if tpo ~= -1
                if (~ isa(ud.slices, 'dip_image_array'))% Grayscale images.
                    if tpo>tpf
                        tps = tpo:-1:tpf;
                    else
                        tps = tpo:1:tpf;
                    end
                    
                    index_last_polygon = -1;
                    for jj = 1:size(ud.rpolygons, 1)
                        if ~isempty(ud.rpolygons{jj, 1, tpo+1})
                            index_last_polygon = jj;
                        end
                    end
                    
                    if index_last_polygon == -1
                        fprintf('No polygons defined on time point %d.\n', tpo+1);
                    else
                        thepoly = ud.rpolygons{index_last_polygon, 1, tpo+1};
                        [Xsnakes, Ysnakes] = snakes(ud.slices, thepoly(:, 1), thepoly(:, 2), tpo, tpf, kappax, kappay, beta,  win_size, sigmagradient, AreaTol);
                        
                        for jj = 1:size(Xsnakes, 1)
                            
                            thesnake = [cat(1,Xsnakes{jj, :}, Xsnakes{jj, :}(1,:)) cat(1, Ysnakes{jj, :}, Ysnakes{jj, :}(1,:)) repmat(tps(jj), [size(Xsnakes{jj, :}, 1)+1 1])];
                            if jj == 1
                                ud.rpolygons{index_last_polygon, 1, tps(jj)+1} = [];
                                ud.rpolygons{index_last_polygon, 1, tps(jj)+1} = thesnake;
                                
                            else
                                % Store in in the first empty spot.
                                myind = 1;
                                for i = 1:size(ud.rpolygons, 1)
                                    if (isempty(ud.rpolygons{i, 1, tps(jj)+1}))
                                        ud.rpolygons{i, 1, tps(jj)+1} = thesnake;
                                        break;
                                    end
                                    myind = myind + 1;
                                end
                                
                                % If there were no empty spots, store at the end.
                                if (myind > size(ud.rpolygons, 1))
                                    ud.rpolygons{end+1, 1, tps(jj)+1} = thesnake;
                                end
                                
                                theind = myind;
                            end
                        end
                    end
                    
                else
                    fprintf('One day Teresa will make this work on color images. For now, sit down and wait.\n');
                end
                %save test
                ud.paramtpo = tpo;
                ud.paramtpf = tpf;
                ud.param_snakes_kappax = kappax;
                ud.param_snakes_kappay = kappay;
                ud.param_snakes_beta = beta;
                ud.param_snakes_win_sz=win_size;
                ud.param_snakes_areaTol=AreaTol;
                
            end
            
            set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
            set(fig, 'UserData', ud);
            cb_paintpoints(fig, ud.rfiducials, fiducial_sz, fiducial_color, ud.rselectedfiducials, selected_fiducial_color);
            cb_paintfiduciallabels(fig, ud.rfiducials, fiducial_label_sz, fiducial_color, ud.rflag_fiduciallabels);
            cb_paintpolylines(fig, ud.rpolygons, poly_sz, poly_color);
        catch
        end
                        
        
end

%figure(fig);

    function repaint()
        ud = get(fig, 'UserData');
        
        cb_paintpoints(fig, ud.rfiducials, fiducial_sz, fiducial_color, ud.rselectedfiducials, selected_fiducial_color);
        cb_paintdelimiters(fig, ud.rdelimiters, delimiter_sz, delimiter_color);
        cb_paintunique(fig, ud.runiquecoords, fiducial_sz, unique_color);
        cb_paintpolylines(fig, ud.rpolygons, poly_sz, poly_color);
        cb_paintpolylines(fig, ud.rangles, poly_sz, ang_color, 0);
        cb_paintpolylines(fig, ud.rtrajectories, poly_sz, line_color, 0);
        cb_paintfiduciallabels(fig, ud.rfiducials, fiducial_label_sz, fiducial_color, ud.rflag_fiduciallabels);
        cb_painttrajectorylabels(fig, ud.rtrajectories, tr_label_sz, line_color, ud.rflag_fiduciallabels);
        
    end

    %figure(fig);
    %set(0, 'CurrentFigure', fig);
end

