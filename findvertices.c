/*
  image_out = findvertices(image_in, cube_sz)
  image_out = findvertices(image_in, cube_sz, rosette_size)
  image_out = findvertices(image_in, cube_sz, rosette_size, exact_flag)

  Looks for rosettes in a three-dimensional image (dimension three can equal 1, though).
  
  IMAGE_IN is a 3D-matrix (MxNxZ) representing a labeled image (e.g. double(labeled_dip_image)).
  CUBE_SZ is 3-element vector containing the row, col and Z size of a cube that will be dragged
  along image in looking for places where multiple cells (labels) are included in the cube.
  ROSETTE_SIZE is a number indicating the minimum or exact size of the rosettes sought.
  EXACT_FLAG indicates whether rosettes of size equal to (1) or equal or larger than (0)
  ROSETTE_SIZE should be looked for.

  IMAGE_OUT is a 2D matrix (Mx(NZ)) where pixels are set to one if they are around a high order 
  vertex (as defined by ROSETTE_SIZE). All the slices are juxtaposed as columns 
  (reshape(image_out, size(image_in) takes care of this)).
  
  Many multiplications are repeated in the C code: room to optimize a bit!
  
  Rodrigo Fernandez-Gonzalez
  fernanr1@mskcc.org
  2/14/2007
  10/11/2008 -> Revised, added error control.
*/

#define min(a,b) ((a) < (b) ? (a) : (b))
#include "mex.h"
#include <math.h>

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	/*Declarations*/
	mxArray *mximage_in, *mxcube_sz, *mx_min_rosette_size, *mx_exactly, *mx_cube, *mxcells, *mxtemp;
	double *image_in, *dptr, *tmp, *cells, *image_out, *cube_sz, *cube_tmp;
	int i, j, k, ii, jj, kk, M, N, M1, N1, numel, flag, ndims;
	int numtests, index; /* These two variables are just for verbose mode. If you remove prints, you can remove them. */
	int *dims, *dimtmp;

	/* Used to call Matlab */
	int nlhs1, nrhs1; 
	mxArray *plhs1, *prhs1;
	
	/* This is the maximum number of cells that can be encompassed by a rosette. */
	int MIN_ROSETTE_SIZE = 5;
	int EXACTLY = 0;
	
	/* Determines whether the entire surface of the cube of only its center is set to one when a rosette vertex is found under the cube. */
	int CUBE = 1;
	

	/* CHECK PARAMETERS (and obtain the optional ones!) */
	switch (nrhs)
	{
		case 2:
			if (! (mxIsNumeric(prhs[0]) && (mxGetN((mxArray *) prhs[0]) * mxGetM((mxArray *) prhs[0]) > 1)))
				mexErrMsgTxt("First argument should be a matrix.");
		
			if (! (mxIsNumeric(prhs[1])&& (mxGetN((mxArray *) prhs[1]) * mxGetM((mxArray *) prhs[1]) > 1)))
				mexErrMsgTxt("Second argument should be a 3-element matrix.");
				
			break;
		case 3:
			if (! (mxIsNumeric(prhs[0]) && (mxGetN((mxArray *) prhs[0]) * mxGetM((mxArray *) prhs[0]) > 1)))
				mexErrMsgTxt("First argument should be a matrix.");
		
			if (! (mxIsNumeric(prhs[1])&& (mxGetN((mxArray *) prhs[1]) * mxGetM((mxArray *) prhs[1]) > 1)))
				mexErrMsgTxt("Second argument should be a 3-element matrix.");
				
			if (! (mxIsNumeric(prhs[2])&& (mxGetN((mxArray *) prhs[2]) * mxGetM((mxArray *) prhs[2]) == 1)))
				mexErrMsgTxt("Third argument should be a number.");
				
			mx_min_rosette_size = (mxArray *) prhs[2];
			dptr = (double *) mxGetPr(mx_min_rosette_size);
			MIN_ROSETTE_SIZE = (int) dptr[0];
			
			break;

		case 4:
			if (! (mxIsNumeric(prhs[0]) && (mxGetN((mxArray *) prhs[0]) * mxGetM((mxArray *) prhs[0]) > 1)))
				mexErrMsgTxt("First argument should be a matrix.");
		
			if (! (mxIsNumeric(prhs[1])&& (mxGetN((mxArray *) prhs[1]) * mxGetM((mxArray *) prhs[1]) > 1)))
				mexErrMsgTxt("Second argument should be a 3-element matrix.");
				
			if (! (mxIsNumeric(prhs[2])&& (mxGetN((mxArray *) prhs[2]) * mxGetM((mxArray *) prhs[2]) == 1)))
				mexErrMsgTxt("Third argument should be a number.");

			if (! (mxIsNumeric(prhs[3])&& (mxGetN((mxArray *) prhs[3]) * mxGetM((mxArray *) prhs[3]) == 1)))
				mexErrMsgTxt("Fourth argument should be a number.");

			mx_min_rosette_size = (mxArray *) prhs[2];
			dptr = (double *) mxGetPr(mx_min_rosette_size);
			MIN_ROSETTE_SIZE = (int) dptr[0];

			mx_exactly = (mxArray *) prhs[3];
			dptr = (double *) mxGetPr(mx_exactly);
			EXACTLY = (int) dptr[0];
				
			break;
					
		case 5:
			if (! (mxIsNumeric(prhs[0]) && (mxGetN((mxArray *) prhs[0]) * mxGetM((mxArray *) prhs[0]) > 1)))
				mexErrMsgTxt("First argument should be a matrix.");
		
			if (! (mxIsNumeric(prhs[1])&& (mxGetN((mxArray *) prhs[1]) * mxGetM((mxArray *) prhs[1]) > 1)))
				mexErrMsgTxt("Second argument should be a 3-element matrix.");
				
			if (! (mxIsNumeric(prhs[2])&& (mxGetN((mxArray *) prhs[2]) * mxGetM((mxArray *) prhs[2]) == 1)))
				mexErrMsgTxt("Third argument should be a number.");

			if (! (mxIsNumeric(prhs[3])&& (mxGetN((mxArray *) prhs[3]) * mxGetM((mxArray *) prhs[3]) == 1)))
				mexErrMsgTxt("Fourth argument should be a number.");

			if (! (mxIsNumeric(prhs[4])&& (mxGetN((mxArray *) prhs[4]) * mxGetM((mxArray *) prhs[4]) == 1)))
				mexErrMsgTxt("Fifth argument should be a number.");

			mx_min_rosette_size = (mxArray *) prhs[2];
			dptr = (double *) mxGetPr(mx_min_rosette_size);
			MIN_ROSETTE_SIZE = (int) dptr[0];

			mx_exactly = (mxArray *) prhs[3];
			dptr = (double *) mxGetPr(mx_exactly);
			EXACTLY = (int) dptr[0];

			mx_cube = (mxArray *) prhs[4];
			dptr = (double *) mxGetPr(mx_cube);
			CUBE = (int) dptr[0];
				
			break;
					
		default:
			mexErrMsgTxt("Wrong number of input arguments.");
			break;
	}
	
	switch (nlhs)
	{
		case 0:
		case 1:
			break;
		case 2:
			

		default:
			mexErrMsgTxt("Wrong number of output arguments.");
			break;
	}

	/* End Check Parameters.*/

	/*Copy input pointers.*/
	mximage_in = (mxArray *) prhs[0];
	mxcube_sz = (mxArray *) prhs[1];

	/*Get parameters.*/
	image_in = (double *) mxGetPr(mximage_in);
	
	/*MATLAB gives the matrix by columns so to jump by position, 
	(x,y) maps to x*colLen+y, and (row, col) maps to col*nRows+row*/
		
	/* Image dimensions. */
	ndims = mxGetNumberOfDimensions(mximage_in);

        if (ndims == 3)        
        	dims = (int *) mxGetDimensions(mximage_in);		
        else if (ndims == 2)
        {
                dims = (int *) mxCalloc(3, sizeof(int));
                dimtmp = (int *) mxGetDimensions(mximage_in);		
                dims[0] = dimtmp[0];
                dims[1] = dimtmp[1];
                dims[2] = (int) 1;
        }

        /* Cube dimensions: this is necessary because singleton dimensions seem to be lost as arguments. */
        ndims = mxGetNumberOfDimensions(mxcube_sz);

        if (ndims == 3)        
        	cube_sz = (double *) mxGetPr(mximage_in);		
        else if (ndims == 2)
        {
                cube_sz = (double *) mxCalloc(3, sizeof(double));
                cube_tmp = (double *) mxGetPr(mxcube_sz);		
                cube_sz[0] = cube_tmp[0];
                cube_sz[1] = cube_tmp[1];
                cube_sz[2] = (double) 1.;
                /*printf("Heeeeello!\n");*/
        }

	/*printf("Image: %d %d %d\n", dims[0], dims[1], dims[2]);*/
	/*printf("%dD Cube: %f %f %f\n", ndims, cube_sz[0], cube_sz[1], cube_sz[2]);*/

	/* Number of iterations that will be necessary to slide the cube through the entire image. */
	numtests = (int) ((dims[1] - cube_sz[1] + 1) * (dims[0] - cube_sz[0] + 1) * (dims[2] - cube_sz[2] + 1));
	
	/* Output image. */
	M1 = (int) mxGetM(mximage_in);
	N1 = (int) mxGetN(mximage_in);
	image_out = mxCalloc(M1 * N1, sizeof(double));

	/* This matrix is to pass arguments to the Matlab command unique. */
	prhs1 = mxCreateDoubleMatrix(cube_sz[0] * cube_sz[1] * cube_sz[2], 1, mxREAL);
	
	/* And this is to cut out the portion of the image within the sliding cube. */
	tmp = (double *) mxMalloc(cube_sz[0] * cube_sz[1] * cube_sz[2] * sizeof(double));

	/* This flag is set to 1 if label 0 is included in the cube. */
	flag = 0;
	
	/* Index to point at all the different positions where the cube will be placed. */
	index = 0;
	
	/* For each one of the cube positions ... triple loop. */
	for (i = 0; i <= (dims[0] - cube_sz[0]); i ++)
	{
		for (j = 0; j <= (dims[1] - cube_sz[1]); j ++)
		{
			for (k = 0; k <= (dims[2] - cube_sz[2]); k ++)
			{
				if ((index)%100000 == 0) printf("Iteration %d out of %d ...\n", (index + 1), numtests);
				
				/* Copy the pixel values where the cube sits into tmp ... triple loop. */
				for (ii = i; ii < (i + cube_sz[0]); ii ++)
				{
					for (jj = j; jj < (j + cube_sz[1]); jj ++) 	
					{
						for (kk = k; kk < (k + cube_sz[2]); kk ++) 	
						{
							/*printf("(%d %d %d) => index %d = %f\n", ii, jj, kk, (int) ((kk-k) * cube_sz[0] * cube_sz[1] + (jj - j) * cube_sz[0] + (ii - i)), image_in[kk * dims[0] * dims[1] + jj * dims[0] + ii]);*/
							tmp[(int) ((kk-k) * cube_sz[0] * cube_sz[1] + (jj - j) * cube_sz[0] + (ii - i))] = image_in[kk * dims[0] * dims[1] + jj * dims[0] + ii];
							
						}
					}
			 	}
				
				/*printf("\n\n");*/
				
				nlhs1 = 1; /* One output from unique. */
				nrhs1 = 1; /* One input to unique. */

				/* Prepare argument to unique. */
				mxSetPr(prhs1, tmp);
				
				/* unique(tmp) */
				mexCallMATLAB(nlhs1, &plhs1, nrhs1, &prhs1, "unique");
				
				
				/* Read output from unique. */
				mxcells = (mxArray *) plhs1;
				
				/* Extract the number of elements in unique. */
				M = (int) mxGetM(mxcells);
				N = (int) mxGetN(mxcells);
				numel = M * N;
				cells = (double *) mxGetPr(mxcells);
								
				/* unique returns sorted elements. Therefore, if label zero is included
				   in this iteration, it will be the first element in the cell list.    */
				if (cells[0] == 0) flag = 1;
				
				/* If label zero is included in the cube, substract it from the total number
				   of labels in the cube. */
				if (flag)
					numel = numel - 1;
				
				/* Set only the upper left corner of the cube to one if it seats on top of a rosette. */
				/*if (numel >= MIN_ROSETTE_SIZE)
					image_out[k * dims[0] * dims[1] + j * dims[0] + i] = 1.0;*/
					
				/* Set the pixels in the cube to one if they seat on top of a rosette. */
				if (((! EXACTLY) && numel >= MIN_ROSETTE_SIZE) || (EXACTLY && numel == MIN_ROSETTE_SIZE))
				{	
					if (CUBE)	
						for (ii = i; ii < (i + cube_sz[0]); ii ++)
						{
							for (jj = j; jj < (j + cube_sz[1]); jj ++) 	
							{
								for (kk = k; kk < (k + cube_sz[2]); kk ++) 	
								{
									image_out[kk * dims[0] * dims[1] + jj * dims[0] + ii] = 1.0;								
								}
							}
					 	}
					
					else
					{
						ii = i + (cube_sz[0] - 1) / 2;
						jj = j + (cube_sz[1] - 1) / 2;
						kk = k + (cube_sz[2] - 1) / 2;
						
						image_out[kk * dims[0] * dims[1] + jj * dims[0] + ii] = 1.0;		
					}
				}
				
				/* Reset flag.*/
				flag = 0;
				

		        	mxDestroyArray(plhs1); /* If this line is removed, the code gets stuck.*/
				plhs1 = NULL;
				
				index ++;/*printf(" Check point #4.\n", index);*/
			}
		}
	}

	/*mxFree(cube_sz);mxFree(dims);*/
	/*mxFree(tmp); This line will make the code crash with an error: Attempt to free previously freed memory.*/
	/*mxDestroyArray(prhs1);*/

	/*Allocate memory for output pointer.*/
	plhs[0] = mxCreateDoubleMatrix(M1, N1, mxREAL);
	mxSetPr(plhs[0], image_out);

	printf("\nDone!\n");
	
	return;
}
