%  L = myholedrillingfunction1(beta, t)
% 

function [u_rtheta epsilon_0] = myholedrillingfunction2(sigma_xy, r)

nu = .5; % Poisson's ratio.
R_0 = 1; % Diameter of the hole (in microns).

E = 1; % Young's modulus. Constant for now (in other words, u_rtheta is the displacement times E).

C = (1+nu) .* power(R_0, 2) .* sigma_xy ./ E;
u_rtheta = C ./ r;

epsilon_0 = C .* (1 - nu) ./ (power(R_0, 2) .* (1 + nu));