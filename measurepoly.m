 function m = measurepoly(polygons, msr_list, image, brush_sz)

%tic
if nargin < 4
        brush_sz = 3; % Pixel width of the mask used to quantify intensities on image. 
end        

if isempty(polygons)
	m = [];
	return;
end

% Delete empty polygons and crappy ones (with only two points).
%  nonemptypoly = [];
%  for ii = 1:numel(polygons)
%          if (~ isempty(polygons{ii})) && (size(polygons{ii}, 1) > 2)
%                  nonemptypoly(end+1) = ii;
%          end
%  end
%  
%  polygons = polygons(nonemptypoly);
% This is taken care of further down.

if ~ iscell(msr_list)
	tmp = msr_list;
	msr_list = cell(1, 1);
	msr_list{1} = tmp;
end

nobjs = numel(polygons);
areas = nan.*zeros(1, nobjs);
perimeters = nan.*zeros(1, nobjs);
centers = nan.*zeros(nobjs, 2);
lh = nan.*zeros(1, nobjs);
lv = nan.*zeros(1, nobjs);
lellipseh = nan.*zeros(1, nobjs);
lellipsev = nan.*zeros(1, nobjs);
red_peri = nan.*zeros(1, nobjs);
red_center = nan.*zeros(1, nobjs);
green_peri = nan.*zeros(1, nobjs);
green_center = nan.*zeros(1, nobjs);
blue_peri = nan.*zeros(1, nobjs);
blue_center = nan.*zeros(1, nobjs);

obj_ids = 1:nobjs;

for io = 1:nobjs	
	%disp(cat(2, 'Measuring cell ', cat(2, num2str(io), cat(2, ' of ', num2str(nobjs)))));
	thepoly = polygons{io};

        % Only works if the polygon does not self intersect.       
        if isempty(thepoly) | size(thepoly, 1) <= 2
                areas(io) = nan;
                perimeters(io) = nan;
                centers(io, 1:2) = [nan nan]; 
		lh(io) = nan;
		lv(io) = nan;
                lellipseh(io) = nan;
                lellipsev(io) = nan;              
                red_peri(io) = nan;
                red_center(io) = nan;
                green_peri(io) = nan;
                green_center(io) = nan;
                blue_peri(io) = nan;
                blue_center(io) = nan;

        % Self intersect requires at least four points to work.
	elseif (numel(thepoly(1:4:end, 1)) >= 4 && isempty(selfintersect(thepoly(1:4:end, 1), thepoly(1:4:end, 2)))) || nargin < 3
	        for im = 1:numel(msr_list)
		        switch lower(msr_list{im})
			        case {'center'}
				        [areas(io) centers(io, 1) centers(io, 2)] = polycenter(thepoly(:, 1), thepoly(:, 2));
			        case {'area'}
				        areas(io) = polyarea(thepoly(:, 1), thepoly(:, 2));
			        case {'perimeter'}
				        perimeters(io) = polyperimeter(thepoly(:, 1), thepoly(:, 2));
				case {'length'}
					[lh(io) lv(io)] = polylength(thepoly(:, 1), thepoly(:, 2)); % Uses default function (@mean) and grid spacing (1).
                                case {'length_ellipse'} % Length along the X and Y axes, not length of the minor and major axes!!
                                        [dummy1 dummy2 lellipsev(io) lellipseh(io)] = myellipse(fitellipse(thepoly(:, 1), thepoly(:, 2))); % myellipse returns the vertical length first, then the horizontal one. 
                                case {'brightness'}
                                        % Create a mask with the cell outline.
                                        msk_peri = [];
                                        msk_peri = trajectories2mask({thepoly}, size(image{1}), brush_sz, 0);
                                        msk_center = [];
                                        msk_center = fillholes(msk_peri) - msk_peri; %trajectories2mask({thepoly}, ud.imsize(1:2), brush_sz, 1);  
                                                                        
                                        if nnz(double(msk_peri)) & nnz(double(msk_center))
                                                slice = [];
                                                slice = squeeze(image);

                                                if ~ isa(image, 'dip_image_array') % Greyscale image.
                                                        % Measure the mean intensities under the masks.
                                                        red_peri(io) = mean(slice(msk_peri));
                                                        green_peri(io) = red_peri(io); blue_peri(io) = red_peri(io);
                                                        
                                                        red_center(io) = mean(slice(msk_center));
                                                        green_center(io) = red_center(io); blue_center(io) = red_center(io);
                                                else % Color image.
                                                        % Measure the mean intensities under the masks.
                                                        red = slice{1};
                                                        red_peri(io) = mean(red(msk_peri));
                                                        red_center(io) = mean(red(msk_center));
                                                        
                                                        green = slice{2};
                                                        green_peri(io) = mean(green(msk_peri));
                                                        green_center(io) = mean(green(msk_center));
                                                        
                                                        blue = slice{3};
                                                        blue_peri(io) = mean(blue(msk_peri));
                                                        blue_center(io) = mean(blue(msk_center));
                                                end
                                        end         
		        end	
	        end
         % If the polygon DOES self intersect, then use image processing to compute these values.       
         else
                [X Y] = meshgrid(1:size(image{1}, 1), 1:size(image{1}, 2));
                X = reshape(X, [1, numel(X)]);
                Y = reshape(Y, [1, numel(Y)]);
                ind = inpolygon(X, Y, thepoly(:, 1), thepoly(:, 2));

		msr_str = [];
		for im = 1:numel(msr_list)
		        switch lower(msr_list{im})
			        case {'center'}
				        msr_str{end+1} = 'center';
			        case {'area'}
				        msr_str{end+1} = 'size';
			        case {'perimeter'}
				        msr_str{end+1} = 'perimeter';
				case {'length'}
					[lh(io) lv(io)] = polylength(thepoly(:, 1), thepoly(:, 2)); % Uses default function (@mean) and grid spacing (1).
                                case {'length_ellipse'}
                                        [dummy1 dummy2 lellipsev(io) lellipseh(io)] = myellipse(fitellipse(thepoly(:, 1), thepoly(:, 2))); % myellipse returns the vertical length first, then the horizontal one.       
                                case {'brightness'}
                                        % Create a mask with the cell outline.
                                        msk_peri = [];
                                        msk_peri = trajectories2mask({thepoly}, size(image{1}), brush_sz, 0);
                                        msk_center = [];
                                        msk_center = fillholes(msk_peri) - msk_peri; %trajectories2mask({thepoly}, ud.imsize(1:2), brush_sz, 1);  
                                                                        
                                        if nnz(double(msk_peri)) & nnz(double(msk_center))
                                                slice = [];
                                                slice = squeeze(image);

                                                if ~ isa(image, 'dip_image_array') % Greyscale image.
                                                        % Measure the mean intensities under the masks.
                                                        red_peri(io) = mean(slice(msk_peri));
                                                        green_peri(io) = red_peri(io); blue_peri(io) = red_peri(io);
                                                        
                                                        red_center(io) = mean(slice(msk_center));
                                                        green_center(io) = red_center(io); blue_center(io) = red_center(io);
                                                else % Color image.
                                                        % Measure the mean intensities under the masks.
                                                        red = slice{1};
                                                        red_peri(io) = mean(red(msk_peri));
                                                        red_center(io) = mean(red(msk_center));
                                                        
                                                        green = slice{2};
                                                        green_peri(io) = mean(green(msk_peri));
                                                        green_center(io) = mean(green(msk_center));
                                                        
                                                        blue = slice{3};
                                                        blue_peri(io) = mean(blue(msk_peri));
                                                        blue_center(io) = mean(blue(msk_center));
                                                end
                                        end                                                                                     
		                end	
	        end
                m = measure(dip_image(reshape(ind, [size(image{1}, 2) size(image{1}, 1)])), [], msr_str, [], 2);
                areas(io) = m(1).size;
                perimeters(io) = m(1).perimeter;
                centers(io, 1:2) = m(1).center';		
         end
end

m = dip_measurement(obj_ids, 'Center', centers', 'Area', areas, 'Perimeter', perimeters, 'LengthH', lh, 'LengthV', lv, 'LengthEllipseH', lellipseh, 'LengthEllipseV', lellipsev, 'Red brightness (perimeter)', red_peri, 'Red brightness (center)', red_center, 'Green brightness (perimeter)', green_peri, 'Green brightness (center)', green_center, 'Blue brightness (perimeter)', blue_peri, 'Blue brightness (center)', blue_center);

%toc
