% cb_deletelinepol(fig)
function cb_deletelinepol(fig)
udata = get(fig, 'UserData');


ax1 = get(fig, 'CurrentAxes');
ch1 = get(ax1, 'Children');

if ((isfield(udata,'lineh') && (~ isempty(udata.lineh)) && numel(udata.rtmp) == 0) || (isfield(udata,'lineh') && (~ isempty(udata.lineh)) && numel(udata.rtmp) > 0 && udata.rtmp(end) ~= udata.lineh))
    delete(udata.lineh);
    udata.lineh = [];
end

% Delete the first child of these axes of type "line" (the last line drawn).
ax2 = get(fig, 'CurrentAxes');
ch2 = get(ax2, 'Children');
%for i = 1:numel(ch)
%get(ch(i))


if strcmp(get(ch2(1), 'Type'),'line')
    x = get(ch2(1), 'XData');
    y = get(ch2(1), 'YData');
    
    if (numel(ch2) == numel(ch1) & (~ strcmp(get(ch2(1), 'Tag'),'Fiducial')) & (x(1) ~= x(end) | y(1) ~= y(end) | numel(x) <= 2) & (~ismember(ch2(1), udata.rtmp)))
        delete(ch2(1));
        %break;
    end
end
%end

set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
set(fig,'UserData',udata);
% end
end