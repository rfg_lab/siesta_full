% val = cb_getpixvalue(fig, pos)
function val = cb_getpixvalue(fig, pos)

if nargin < 2
    pos = [];
end

ax_children = get(get(fig, 'CurrentAxes'), 'Children');
img = ax_children(end);
udata = get(fig, 'UserData');

udata.ax = get(img,'Parent');
udata.oldAxesUnits = get(udata.ax,'Units');
udata.oldNumberTitle = udata.curslice;
set(udata.ax,'Units','pixels');
%      if strcmp(get(fig,'SelectionType'),'alt')
%         % Right mouse button
%         %udata.coords = dipfig_getcurpos(udata.ax); % Always over image!
%         udata.lineh = line([udata.coords(1),udata.coords(1)],...
%             [udata.coords(2),udata.coords(2)],'EraseMode','xor','Color',[0,0,0.8]);
%         set(fig,'WindowButtonMotionFcn','diptest(''motionR'')',...
%                 'WindowButtonUpFcn','diptest(''up'')',...
%                 'NumberTitle','off','UserData',[]);   % Solve MATLAB bug!
%         set(fig,'UserData',udata);
%         updateDisplayR(fig,udata.ax,udata);
%      else
% Left mouse button
if isempty(pos)
    pos = round(get(udata.ax, 'CurrentPoint'));
end


if ~ isempty(pos)
    pos = pos(1, 1:2);
    
    if isfield(udata, 'curslice') && ndims(udata.imagedata)>2 && isfield(udata, 'imagedata')
        pos(3) = udata.curslice;
        val = double(udata.imagedata(pos(1), pos(2), pos(3)));
    elseif ndims(udata.imagedata)>0 && isfield(udata, 'imagedata')
        val = double(udata.imagedata(pos(1), pos(2)));
    else
        val = -1;  % Empty display!
    end
    
else
    val = 0;
end
%end
% else
% 	val = -1;
end
