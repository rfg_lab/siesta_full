function cut_plots(l, a, p, cut_t, t_res, title_label, save_flag)

xyres = 1/4.9; % microns per pixel.

t = ((1:numel(l))-cut_t).*t_res;
i1 = find(t == -30);
if (isempty(i1))
	i1 = 1;
end

i2 = numel(t); %find(t == 60);
ind = i1:i2;

% PLOTS
% Relative l vs t -------------------------------------
l_0 = l;
l = 100.*(l - l(cut_t)) ./ l(cut_t);
figure;
h = plot(t(ind), l(ind), 'r');
set(h, 'Color', 'r', 'LineStyle', '-', 'LineWidth', 2);
hold on;
xlim([-40 70]);
ylimits = [-25 200]; 
ylim(ylimits);
set(gca, 'XTick', [-60:15:300], 'YTick', [-100:25:500], 'XGrid', 'off', 'YGrid', 'on', 'FontSize', 16);
xlabel('time (sec)', 'FontSize', 16);
ylabel('relative distance (% increase)', 'FontSize', 16);
title(title_label);

if (save_flag)
	saveas(gcf, cat(2, 'distance_time_', rstrtrim(title_label)), 'fig');
end

% Relative velocity vs t -------------------------------------
l_f = circshift(l_0, [0 -1]);
v = (l_f - l_0) ./ t_res;
v(end) = NaN;
v = 100.*(v - v(cut_t)) ./ v(cut_t);
figure;
h = plot(t(ind), v(ind), 'r');
set(h, 'Color', 'r', 'LineStyle', '-', 'LineWidth', 2);
hold on;
xlim([-40 70]);
ylimits = [-25 200]; 
ylim(ylimits);
set(gca, 'XTick', [-60:15:300], 'YTick', [-100:25:500], 'XGrid', 'off', 'YGrid', 'on', 'FontSize', 16);
xlabel('time (sec)', 'FontSize', 16);
ylabel('relative velocity (% increase)', 'FontSize', 16);
title(title_label);

if (save_flag)
	saveas(gcf, cat(2, 'velocity_time_', rstrtrim(title_label)), 'fig');
end

if ~ isempty(a)
% Absolute area vs time. ----------------------------------
tmin = t./60;
figure;
h = plot(tmin(ind), a(ind).*xyres.*xyres, 'r');
set(h, 'Color', 'r', 'LineStyle', '-', 'LineWidth', 3);
hold on;
xlim([tmin(1) tmin(end)]);
%ylimits = [0 3]; 
%ylim(ylimits);
set(gca, 'LineWidth', 2, 'XTick', [-60:5:3000], 'YTick', [-100:25:500], 'YScale', 'lin', 'XGrid', 'off', 'YGrid', 'off', 'FontSize', 16);
xlabel('time (min)', 'FontSize', 16);
ylabel('absolute area (\mum^2)', 'FontSize', 16);
title(title_label);

if (save_flag)
      saveas(gcf, cat(2, 'absarea_time_', rstrtrim(title_label)), 'fig');
end

% Contraction rate vs time. --------------------------------
tmin = t./60;
a_0 = a;
a_f = circshift(a_0, [0 -(60/t_res)]); % Rate per minute. Change -(60/t_res) into -1 for rate per 15/sec.
v = (a_f - a_0); % Rate per minute.
v((end-3):end) = NaN;
figure;
h = plot(tmin(ind), v(ind).*xyres.*xyres, 'r');
%h = plot(tmin(ind), smooth_curve2(v(ind).*xyres.*xyres,1), 'r');
set(h, 'Color', 'r', 'LineStyle', '-', 'LineWidth', 3);
hold on;
h = plot(tmin(ind), smooth_curve2(v(ind).*xyres.*xyres, 3), 'r');
set(h, 'Color', 'b', 'LineStyle', '-.', 'LineWidth', 3);
xlim([tmin(1) tmin(end)]);
ylimits = [-10 10]; 
ylim(ylimits);
set(gca, 'LineWidth', 2, 'XTick', [-60:5:3000], 'YTick', [-100:1:500], 'YScale', 'lin', 'XGrid', 'off', 'YGrid', 'off', 'FontSize', 16);
xlabel('time (min)', 'FontSize', 16);
ylabel('contraction rate (\mum^2/min)', 'FontSize', 16);
title(title_label);

if (save_flag)
      saveas(gcf, cat(2, 'absareacontractionrate_time_', rstrtrim(title_label)), 'fig');
end

% Relative a vs t -------------------------------------
figure;
h = plot(t(ind), a(ind)./mean(a(1:cut_t)), 'r');
set(h, 'Color', 'r', 'LineStyle', '-', 'LineWidth', 3);
hold on;
xlim([t(1) t(end)]);
ylimits = [0 3]; 
ylim(ylimits);
set(gca, 'XTick', [-60:300:3000], 'YTick', [-100:0.5:500], 'YScale', 'log', 'XGrid', 'off', 'YGrid', 'off', 'FontSize', 16);
xlabel('time (sec)', 'FontSize', 16);
ylabel('relative area (% increase)', 'FontSize', 16);
title(title_label);

if (save_flag)
      saveas(gcf, cat(2, 'area_time_', rstrtrim(title_label)), 'fig');
end




% Relative a vs relative l -------------------------------------
a = 100.*(a - a(cut_t)) ./ a(cut_t);
figure;
h = plot(l(ind), a(ind), 'ro');
set(h, 'LineWidth', 2);
hold on;
xlim([-25 200]);
ylimits = [-25 200]; 
ylim(ylimits);
set(gca, 'XTick', [-100:25:500], 'YTick', [-100:25:500], 'XGrid', 'off', 'YGrid', 'on', 'FontSize', 16);
ylabel('relative area (% increase)', 'FontSize', 16);
xlabel('relative distance (% increase)', 'FontSize', 16);
title(title_label);

%bpow = nlinfit(l(ind), a(ind), @mypow2, [1 0]);
%blin = nlinfit(l(ind), a(ind), @mylin, [1 0]);
%ppow = kstest(a(ind), mypow2(bpow, (0:25:175)))
%plin = kstest(a(ind), mylin(blin, (0:25:175)))

%if (ppow >= plin)
%	plot((0:10:175), mypow2(bpow, (0:10:175)), 'k-');
%else
%	plot((0:10:175), mylin(blin, (0:10:175)), 'k-');
%end

if (save_flag)
	saveas(gcf, cat(2, 'area_distance_', rstrtrim(title_label)), 'fig');
end
end

if ~ isempty(p)
% Absolute perimeter vs time. ----------------------------------
tmin = t./60;
figure;
h = plot(tmin(ind), p(ind).*xyres, 'r');
set(h, 'Color', 'r', 'LineStyle', '-', 'LineWidth', 3);
hold on;
xlim([tmin(1) tmin(end)]);
%ylimits = [0 3]; 
%ylim(ylimits);
set(gca, 'LineWidth', 2, 'XTick', [-60:5:3000], 'YTick', [-100:10:500], 'YScale', 'lin', 'XGrid', 'off', 'YGrid', 'off', 'FontSize', 16);
xlabel('time (min)', 'FontSize', 16);
ylabel('absolute perimeter (\mum)', 'FontSize', 16);
title(title_label);

if (save_flag)
      saveas(gcf, cat(2, 'absperimeter_time_', rstrtrim(title_label)), 'fig');
end

% Contraction rate vs time. --------------------------------
tmin = t./60;
p_0 = p;
p_f = circshift(p_0, [0 -(60/t_res)]); % Rate per minute. Change -(60/t_res) into -1 for rate per 15/sec.
v = (p_f - p_0); % Rate per minute.
v((end-3):end) = NaN;
figure;
h = plot(tmin(ind), v(ind).*xyres, 'r');
%h = plot(tmin(ind), smooth_curve2(v(ind).*xyres.*xyres,1), 'r');
set(h, 'Color', 'r', 'LineStyle', '-', 'LineWidth', 3);
hold on;
h = plot(tmin(ind), smooth_curve2(v(ind).*xyres, 3), 'r');
set(h, 'Color', 'b', 'LineStyle', '-.', 'LineWidth', 3);
xlim([tmin(1) tmin(end)]);
ylimits = [-7 7]; 
ylim(ylimits);
set(gca, 'LineWidth', 2, 'XTick', [-60:5:3000], 'YTick', [-100:1:500], 'YScale', 'lin', 'XGrid', 'off', 'YGrid', 'off', 'FontSize', 16);
xlabel('time (min)', 'FontSize', 16);
ylabel('contraction rate (\mum/min)', 'FontSize', 16);
title(title_label);

if (save_flag)
      saveas(gcf, cat(2, 'absperimetercontractionrate_time_', rstrtrim(title_label)), 'fig');
end
% Relative p vs t -------------------------------------
figure;
h = plot(t(ind), p(ind)./mean(p(1:cut_t)), 'r');
set(h, 'Color', 'r', 'LineStyle', '-', 'LineWidth', 3);
hold on;
xlim([t(1) t(end)]);
ylimits = [0 3]; 
ylim(ylimits);
set(gca, 'XTick', [-60:300:3000], 'YTick', [-100:0.5:500], 'YScale', 'log', 'XGrid', 'off', 'YGrid', 'off', 'FontSize', 16);
xlabel('time (sec)', 'FontSize', 16);
ylabel('relative perimeter (% increase)', 'FontSize', 16);
title(title_label);

if (save_flag)
      saveas(gcf, cat(2, 'perimeter_time_', rstrtrim(title_label)), 'fig');
end

% Relative p vs relative l -------------------------------------
p = 100.*(p - p(cut_t)) ./ p(cut_t);
figure;
h = plot(l(ind), p(ind), 'ro');
set(h, 'LineWidth', 2);
hold on;
xlim([-25 200]);
ylimits = [-10 50]; 
ylim(ylimits);
set(gca, 'XTick', [-100:25:500], 'YTick', cat(2, [-25:10:-15], [-10:10:500]), 'XGrid', 'off', 'YGrid', 'on', 'FontSize', 16);
ylabel('relative perimeter (% increase)', 'FontSize', 16);
xlabel('relative distance (% increase)', 'FontSize', 16);
title(title_label);

%bpow = nlinfit(l(ind), p(ind), @mypow2, [1 0]);
%blin = nlinfit(l(ind), p(ind), @mylin, [1 0]);
%ppow = kstest(p(ind), mypow2(bpow, (0:10:175)))
%plin = kstest(p(ind), mylin(blin, (0:10:175)))

%if (ppow > plin)
%	plot((0:10:175), mypow2(bpow, (0:10:175)), 'k-');
%else
%	plot((0:10:175), mylin(blin, (0:10:175)), 'k-');
%end


if (save_flag)
	saveas(gcf, cat(2, 'perimeter_distance_', rstrtrim(title_label)), 'fig');
end
end
