% *********************************************************************************
function cb_paintfiduciallabels(fig, fidu, fiducial_sz, fiducial_color, flag)

cb_erasefiduciallabels(fig);

if flag
	ud = get(fig, 'UserData');
	curslice = ud.curslice + 1;

	for i = 1 : size(fidu, 1)
		coords = fidu(i, 1:2, curslice);
			
		if sum(coords == [-1 -1]) ~= 2
			coords(1:2) = coords(1:2) + 2; % Move away from the position of the actual marker.
			cb_drawfiduciallabel(fig, coords, i, fiducial_sz, fiducial_color, 'Fiducial_Label');
		end
	end
end
end