%POLYPERIMETER Perimeter of polygon.
%    POLYPERIMETER(X,Y) returns the perimeter of the polygon specified
%    by the vertices in the vectors X and Y.  
%
% Rodrigo Fernandez-Gonzalez
% fernanr1@mskcc.org
% 08/08/2007

function p = polyperimeter(x, y)

x = reshape(x, [numel(x), 1]);
y = reshape(y, [numel(y), 1]);

coords = [x y];
shcoords = circshift(coords, [-1 0]);

d = coords - shcoords;
d = sum(d .* d, 2);
d = sum(sqrt(d));

p = d;

