% *********************************************************************************
% ASSUMES there is a trackedPolygon variable.
% index is seed index in time point currSlice+1 (currSlice is SIESTA time,
% start at 0)
function cb_exportpolygonannotations_div(ud, index, currSlice)

fprintf('Exporting annotations for polygon %d ... ', index);
curr_dir = pwd;
cd(ud.rcurrdir);

%[filename, pathname, ext] = uiputfile('*.mat', 'Export polygon annotations');
filename = cat(2, 'annotations_cell_div', rnum2str(index, 3), '.mat');
pathname = ud.rcurrdir;
ext = 1;

ud2 = ud; % Backup.
ud = init_userdata(ud2);

if (~ isequal(filename, 0))
                % Do not store the images in ud.
                sl = ud.slices;
                ud.slices = [];
                if isfield(ud, 'imagedata')
                        imd = ud.imagedata;
                        ud.imagedata = [];
                else
                        cold = ud.colordata;
                        ud.colordata = [];
                end
                
                % Now check, in every time point, which polygon contains the selected fiducial (of course, this assumes the fiducials have been tracked!).
                if numel(ud.imsize) >= 3
                        max_slice = ud.imsize(3)-1;
                else
                        max_slice = 0;
                end
                
                %michael
                %find the polygon the seed is in
                for i=1:size(ud.trackedPolygon(:,:,currSlice+1,1),1) % loop through all polygons in trackedPolygon at time currSlice+1
                    polygon=ud.trackedPolygon{i,:,currSlice+1,1};
                    if isempty(polygon)
                        continue;
                    end
                    in=inpolygon(ud2.rfiducials(index,1,currSlice+1),ud2.rfiducials(index,2,currSlice+1),polygon(:,1),polygon(:,2));
                    if in
                        indexTrack=i; % contains correct index in trackedPolygon
                        break;
                    end
                end
                
                trackedPolygonSize=size(ud.trackedPolygon);
                ud.rpolygons=cell(trackedPolygonSize(1:3));
                ud.rpolygons(indexTrack,:,:)=ud.trackedPolygon(indexTrack,:,:,1);
                size(ud.rpolygons)
                
                % go through trackDiv to organized daughter cells
                for j=1:size(ud2.trackDiv,2)
                    % go through the 3 possible div indices (parent, plus 2
                    % daughters
                    for j2=2:4
                        if indexTrack==ud2.trackDiv(j2,j,2)
                        timeDiv=ud2.trackDiv(1,j,2);
                        switch j2
                            case 2
                                d1Ind=ud2.trackDiv(3,j,2); %Daughter Cell
                                d2Ind=ud2.trackDiv(4,j,2); %Daughter Cell
                                ud.rpolygons(d1Ind,timeDiv:end)=ud.trackedPolygon(d1Ind,:,timeDiv:end,1);
                                ud.rpolygons(d2Ind,:,timeDiv:end)=ud.trackedPolygon(d2Ind,:,timeDiv:end,1);
                            case 3
                                d1Ind=ud2.trackDiv(2,j,2); %parent Cell
                                ud.rpolygons(d1Ind,1:timeDiv-1)=ud.trackedPolygon(d1Ind,:,1:timeDiv-1,1);
                                d2Ind=ud2.trackDiv(4,j,2); %daughter Cell
                                ud.rpolygons(d2Ind,:,timeDiv:end)=ud.trackedPolygon(d2Ind,:,timeDiv:end,1);
                            case 4
                                d1Ind=ud2.trackDiv(2,j,2); %parent Cell
                                ud.rpolygons(d1Ind,1:timeDiv-1)=ud.trackedPolygon(d1Ind,:,1:timeDiv-1,1);
                                d2Ind=ud2.trackDiv(3,j,2); %daughter Cell
                                ud.rpolygons(d2Ind,:,timeDiv:end)=ud.trackedPolygon(d2Ind,:,timeDiv:end,1);
                        end

                        fprintf('Division angle is %d ... ', ud2.trackDiv(5,j,2)*(180/pi));
                        end
                    end
                end
                
                %michael
                
%                 % For every time point.
%                 for ii = 0:max_slice
%                         ud.rpolygons{1, 1, ii+1} = [];
%                         p = ud2.rfiducials(index, 1:2, ii+1);
%                         
%                         % If the fiducial is not defined at a certain time point, continue ...    
%                         if p(1) ~= -1
%                                 % Find the polygon that contains the point.
%                                 for jj = 1:numel(ud2.rpolygons(:, :, ii+1))
%                                         thepoly = ud2.rpolygons{jj, 1, ii+1};
%                                         
%                                         % When you find it, store it in the new annotation structure for this time point.
%                                         if ~isempty(thepoly) && inpolygon(p(1), p(2), thepoly(:, 1), thepoly(:, 2))
%                                                 ud.rpolygons{1, 1, ii+1} = thepoly;
%                                                 break;
%                                         end
%                                 end
%                         end
%                 end
                                
                % This variable is neccesary in the annotation file for historical reasons.
                thelinemasks = {};
                save(cat(2, pathname, filename), 'ud', 'thelinemasks');
                ud.rcurrdir = pathname;
                
                ud = ud2;                
end

cd(curr_dir);
fprintf('done!\n');
end