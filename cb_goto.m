% ud = cb_goto(fig, ind)
function ud = cb_goto(fig, ind)

ud = get(fig, 'UserData');

ax_children = get(get(fig, 'CurrentAxes'), 'Children');
img = findobj(ax_children, 'Type', 'image');

% Check that the index is within range.
if (~ isempty(ind)) && (ind >= 0) && (ind < ud.imsize(3))
    
    % Jump there.
    
    ud.curslice = ind;
    if ~ isa(ud.slices, 'dip_image_array')
        ud.imagedata = squeeze(ud.slices(:, :, ind));
    else %if isfield(ud, 'colordata')
        ud.colordata = ud.slices{:}(:, :, ind);
        ud.imagedata = cat(3, ud.colordata{1}(:, :, 0), ud.colordata{2}(:, :, 0), ud.colordata{3}(:, :, 0));
        ud.rcurrchannel = 0;
    end
    
    display_data(fig, img, ud);
    tp = findobj(get(fig, 'Children'), 'Tag', 'tp');
    set(tp, 'String', ind);
    
    % This makes sure the window title and image mapping are correct.
    if ind>0 
        set(fig, 'CurrentCharacter', 'p');
        eval('dipshow DIP_callback KeyPressFcn');
        set(fig, 'CurrentCharacter', 'n');
        eval('dipshow DIP_callback KeyPressFcn');
    else % if ind == 0          
        set(fig, 'CurrentCharacter', 'n');
        eval('dipshow DIP_callback KeyPressFcn');
        set(fig, 'CurrentCharacter', 'p');
        eval('dipshow DIP_callback KeyPressFcn');
    end
end        

    
end
