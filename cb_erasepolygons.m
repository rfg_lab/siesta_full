% *********************************************************************************
function cb_erasepolygons(fig)

ax = get(fig, 'CurrentAxes');
children = get(ax, 'Children');

for i = 1:numel(children)
	obj = children(i);
	type = get(obj, 'Tag');

	if strcmp(type, 'Polyline')
		delete(obj);
	end
end
end