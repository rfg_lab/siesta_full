% out = shiftim(in, translation)
%
% Shifts an image filling in with zero-valued pixels.
% 
% IN is the image to be shifted.
% TRANSLATION is a 3-element vector with the dimensions of the shift in X, Y and Z.
%   
% OUT is the shifted image.
%
% Rodrigo Fernandez-Gonzalez
% fernanr1@mskcc.org
% 8/02/2007

function out = shiftim(in, translation)

if (numel(translation) ~= ndims(in))
	error('The translation vector must have as many elements as dimensions has the input image.');
end

if ndims(in) < 3
        in = expanddim(in, 3);
        translation(3) = 0;
end        

% Create new image.
out = dip_image(zeros(size(in, 2), size(in, 1), size(in, 3)));
if (ndims(out) < 3)
%  	out = expanddim(out, 3-ndims(out)); % Old dipimage.
        out = expanddim(out, 3); % Newer dipimage versions.
end

% ul_old and lr_old determine the square im image in that will still be visible in image out.
% ul_new and lr_new determine the coordinates in image out where the square determined by
% ul_old and lr_old should be pasted.
upper_left_new = [0 0 0];
lower_right_new = [0 0 0];
upper_left_old = [0 0 0];
lower_right_old = [0 0 0];

% The four corners are computed based on the sign of the shift.
for i = 1:3
	if (translation(i) <= 0)
		upper_left_new(i) = 0;
		lower_right_new(i) = size(in, i) + translation(i) - 1;
		upper_left_old(i) = - translation(i);
		lower_right_old(i) = size(in, i) - 1;
	else
		upper_left_new(i) = translation(i);
		lower_right_new(i) = size(in, i) - 1;
		upper_left_old(i) = 0;
		lower_right_old(i) = size(in, i) - translation(i) - 1;
	end
end

out(upper_left_new(1):lower_right_new(1), upper_left_new(2):lower_right_new(2), upper_left_new(3):lower_right_new(3)) = ...
		in(upper_left_old(1):lower_right_old(1), upper_left_old(2):lower_right_old(2), upper_left_old(3):lower_right_old(3));
