% Deletes ALL the markings in all frames.

function udata = cb_deleteallmarkings(fig)
udata = get(fig, 'UserData');

for ind = 1:udata.imsize(3)
    udata.rdelimiters(:, :, ind) = -1 .* ones(size(udata.rdelimiters, 1), size(udata.rdelimiters, 2));
    udata.rndelimiters(ind) = 0;
    
    udata.rfiducials(:, :, ind) = -1 .* ones(size(udata.rfiducials, 1), size(udata.rfiducials, 2));
    udata.rnfiducials(ind) = 0;
    
    udata.runiquecoords(ind, :) = [-1 -1];
    
    udata.rpolygons(:, :, ind) = {[]};
    %michael
    if ~isempty(udata.trackedPolygon)
        udata.trackedPolygon(:,:,ind:end)={[]};
        udata.trackDiv=[];
    end
    %michael
    udata.rtrajectories(:, :, ind) = {[]};
    udata.rntrajectories(:, :, ind) = 0;
    udata.rangles(:, :, ind) = {[]};
end

ind = udata.curslice + 1;

ax = get(fig, 'CurrentAxes');
ch = get(ax, 'Children');

for ii = 1:numel(ch)
    type = get(ch(ii), 'Tag');
    if strcmp(type, 'Fiducial') | strcmp(type, 'Delimiter') | strcmp(type, 'Unique') | strcmp(type, 'Polyline')
        delete(ch(ii));
    end
end
end
