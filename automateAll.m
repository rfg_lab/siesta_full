%function [newFids,trackedPolygon,trackDiv,dividingCell,polygons]=automateAll(ud,start,last)
function [ud2, theNewSeedIndices]=automateAll(ud,start,trueLast,seedDist,trackedPolygon)

% implementation: after "Expand from these seeds" is called, this function is called to determine if seeds should be split in a division event"
% can be called at every time point or at the last time point



% start, end are array times, start at time point 1, not 0

if trueLast-start<10
    newFids=ud.rfiducials;
    theNewSeedIndices=[];

else

%     for i=1:size(last,2)
%         %    if ud(:,:,last(i)) is empty
%         if nnz(not(cellfun('isempty',ud.rpolygons(:,:,last(i)))))==0;
%             trueLast=last(i-1);
%             break;
%         end
%         
%     end
%     
%     if ~exist ('trueLast','var')
%         trueLast=last(end);
%         
%     end
  
% display('Now doing automateAll');
% display('start time');
% display(start);
% display('last time');
% display(last);
    
% track the polygons
% trackedPolygon=trackPolyTest(ud,start,trueLast);
%trackedPolygon=trackPolyTest(ud,1,40);

% create 3 section cells; 1 - distances, 2 - area , 3 - circularity 
trackDiv=findDivCells(trackedPolygon,ud.rfiducials);

% check if distances satisfy the dumbbell condition
dividingCell=detDivAll(trackDiv);

% check distances satisfy, area and circularity satisfy, contains
% conditions to check fo growing area and growing circularity
polygons=isItDividing2(trackDiv,dividingCell,1.3,1.8,1.5);
%polygons contains time and segmentation of division event

display('gone through automateAll Analysis');

[seg,time]=find(~cellfun('isempty',polygons(:,:,1)));
nonEmptyInd=[seg,time];

fids=ud.rfiducials;

% divSeedInd keeps track of index at which cell divisions happen
theNewSeedIndices=[];
for i=1:size(nonEmptyInd,1)
    if nonEmptyInd(i,1)==1
        continue
    end
    
    segmentNum=nonEmptyInd(i,1);
    segmentTime=dividingCell{1,nonEmptyInd(i,2),1}; %segmentTime is array time
    
    %fids=splitFids(polygons(nonEmptyInd(i,1),nonEmptyInd(i,2),1),fids,polygons{nonEmptyInd(i,1),nonEmptyInd(i,2),1}+1);
    [fids, theNewSeedIndices]=splitFids(trackedPolygon{segmentNum-1,:,segmentTime},fids,segmentTime,theNewSeedIndices,seedDist);
    
    %trackedPolygon{nonEmptyInd(i,1),nonEmptyInd(i,2),1}+1);
    
    %!!!!!!!!!!! make sure to track the cell division polygon

end

% if isempty(divSeedInd)
%     theNewSeedIndices=[];
% end

% % delete ud.rpolygons for where the new fids were added
% ud.rpolygons(:,:,segmentTime)=cell(size(ud.rpolygons(:,:,segmentTime)));

newFids=fids;
%newFids=1;

% % i represent the different cells
% polygon={};
% for i=2:size(trackDiv,1)
%     polygon=(polygon;isItDividing(trackDiv,dividingCell,size(trackDiv(i,:,2),2),trackDiv(1,1,size(trackDiv(i,:,2),2)),i-1,trackedPolygon));
% end
% 
% % splitFids
% for i=1:size(polygon,1)
%     
% end
end

ud2 = ud;
ud2.rfiducials = newFids;
%theNewSeedIndices=[];


% fixing the rnfiducials

% go through each time slice (ii) of the newFids
for ii = 1:size(newFids, 3)
    ind = find(newFids(:, 1, ii) == -1);
    
    if ~ isempty(ind)
        ud2.rnfiducials(ii) = ind(1)-1;
    else
        ud2.rnfiducials(ii) = size(newFids, 1);
    end
end



end