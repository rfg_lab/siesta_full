
% *********************************************************************************
% tpo and tpf start from 1.
% Uses fiducials to track polylines.
function ud = cb_trackpolylines(fig, tpo, tpf, th)

ud = get(fig, 'UserData');
ud_old = ud;

[ud.rfiducials(:, :, tpo) ud.rnfiducials(tpo)] = cb_consolidatefiducials(ud.rfiducials(:, :, tpo));

disp(cat(2, 'Time point ', cat(2, num2str(tpo-1), cat(2, ' - ', cat(2, num2str(ud.rnfiducials(tpo)), ' fiducials')))));

% Reorganize polygons as well - first time point.      
% for every fiducial in this time point ...
polygon_list = ud.rpolygons(:, :, tpo);

for jj = 1:ud.rnfiducials(tpo)                
        % chek if the fiducial is in a polygon.
        p = ud.rfiducials(jj, 1:2, tpo);
        
        % Find the polygon that contains the point.
        for kk = 1:numel(ud.rpolygons(:, :, tpo))
                thepoly = ud.rpolygons{kk, 1, tpo};
                
                % When you find it, store the polygon with the same index as the fiducial.
                if ~isempty(thepoly) && inpolygon(p(1), p(2), thepoly(:, 1), thepoly(:, 2))
                        polygon_list{jj, 1, 1} = thepoly;
                        break;
                elseif kk == numel(ud.rpolygons(:, :, tpo))
                        polygon_list{jj, 1, 1} = [];
                end
        end
end

% Copy new list of polylines into the data structure.
for jj = 1:numel(polygon_list)
        ud.rpolygons{jj, 1, tpo} = polygon_list{jj, 1, 1};
end
% End reorganization of polygons.

for ii = tpo:(tpf-1)
        [ud.rfiducials(:, :, ii+1) ud.rnfiducials(ii+1)] = cb_consolidatefiducials(ud.rfiducials(:, :, ii+1));
        disp(cat(2, 'Time point ', cat(2, num2str(ii+1-1), cat(2, ' - ', cat(2, num2str(ud.rnfiducials(ii+1)), ' fiducials')))));
        
        % Find the position of the nodes to track.
        pnts1 = ud.rfiducials(1:ud.rnfiducials(ii), 1:2, ii);
        pnts2 = ud.rfiducials(1:ud.rnfiducials(ii+1), 1:2, ii+1);
        
        % If there are different number of nodes in consecutive time points, break.
        if size(pnts1, 1) ~= size(pnts2, 1)
                ud = ud_old;
                return;
        end
        
        % Find the distances between the nodes:
        % dt = [d_p1ii_p1ii d_p1ii_p2ii ... d_p1ii_p1ii+1 d_p1ii_p2ii+1 ...;
        %       d_p2ii_p1ii d_p2ii_p2ii ... d_p2ii_p1ii+1 d_p2ii_p2ii+1 ...;
        %       .
        %       .
        %       .
        %       d_p1ii+1_p1ii d_p1ii_1_p2ii ... dp1ii+1_p1ii+1 d_p1ii+1_p2ii+1 ...;
        %       .
        %       .
        %       .]
        dt = mfloyd([pnts1; pnts2], ones(size(pnts1, 1)+size(pnts2, 1)), 1);
        dt = dt(1:size(pnts1, 1), (size(pnts1, 1)+1):end);
        
        % For each fiducial in time point ii select the closest point in time point ii+1.
        %save tmp dt ud
        [dt, theindices] = sort(dt, 2); 
        I = theindices(:, 1); % Indeces into ud.rfiducials(:, :, ii+1) after consolidating.
        
        % When two points in tp ii are linked to the same point in tp ii+1, 
        % NOW: stop and complain!       
        % BEFORE: link only the closest one and find the second nearest point in ii+1 for the other one. THIS DID NOT WORK VERY WELL (CORRESPONDS TO THE COMMENTED CODE BELOW).
        
        [x n] = mmrepeat(sort(I));
        ind_iiplus1 = x(find(n == 2)); % Points in ii+1 with two points in ii.
        
        if numel(ind_iiplus1) > 0
                % Change the fiducial labels in the last time point to the consolidated ones
                % so that the error messages make sense.
                ud.rfiducials(1:ud.rnfiducials(ii+1), 1:2, ii+1) = pnts2;
                for jj = 1:numel(ind_iiplus1)      
                        fprintf('Error in time point %d, fiducial %d: two fiducials from time point %d are mapped here.\n', ii, ind_iiplus1(jj), ii-1);
                end                        
                break;
        end
                               
%          jj = 0;
%       while jj < numel(ind_iiplus1)
%               for jj = (jj+1):numel(ind_iiplus1)
%                       ind_ii = find(I == ind_iiplus1(jj)); % Points on ii that share the assigned point.
%                       [themin indmin] = sort(dt(ind_ii, 1)); % Order of ind_ii points.
%                       
%                       % If both nodes are very close to the same point, check next neighbors.
%                       if sum(dt(ind_ii, 1) < th) == numel(ind_ii)
%                               % If the node closest to the common neighbors is also closer to its
%                               % second neighbor, and if the distance between the node closest to
%                               % the common neighbor and its second neighbor is smaller (or very
%                               % close, see the 30% factor) than the
%                               % distance between the node furthest from the common neighbor and
%                               % the common neighbor, keep the node furthest from the common neighbor
%                               % linked to the common neighbor, and link the node closest to the
%                               % common neighbor to its second neighbor.                               
%                               if dt(ind_ii(indmin(1)), 2) < dt(ind_ii(indmin(2)), 2) & dt(ind_ii(indmin(1)), 2) <= (dt(ind_ii(indmin(2)), 1) + 0.3 * dt(ind_ii(indmin(2)), 1)) %0.3 originally was 0.25
%                                       I(ind_ii(indmin(1))) = theindices(ind_ii(indmin(1)), 2);
%                               else
%                                       % Find which one is the next closest neighbor.
%                                       new_assignment = theindices(ind_ii(indmin(2)), 2);
%                                       
%                                       % If it has not yet been assigned to anybody, assign it.
%                                       if isempty(find(theindices(:, 1) == new_assignment))
%                                               I(ind_ii(indmin(2))) = new_assignment;
%                                       % If it has been assigned, add to the list of issues to fix.
%                                       % However, modifying the length of ind_iiplus1 does not alter
%                                       % the "for" loop limits (since we are already inside the loop).
%                                       % This is why the "while" loop is necessary.
%                                       else
%                                               I(ind_ii(indmin(2))) = new_assignment;
%                                               ind_iiplus1(end+1) = new_assignment;
%                                               theindices(ind_ii(indmin(2)), :) = circshift(theindices(ind_ii(indmin(2)), :), [0 -1]);
%                                               dt(ind_ii(indmin(2)), :) = circshift(dt(ind_ii(indmin(2)), :), [0 -1]);
%                                               dt(ind_ii(indmin(2)), end) = inf;
%                                               
%                                       end
%                               end
%                       else            
%                               I(ind_ii(indmin(2))) = theindices(ind_ii(indmin(2)), 2);
%                       end
%               end     
%       end
        
        ud.rfiducials(1:ud.rnfiducials(ii+1), 1:2, ii+1) = ud.rfiducials(I, 1:2, ii+1);
                
        % Reorganize polygons as well - all time points tracked after the first.     
        % for every fiducial in this time point ...
        polygon_list = ud.rpolygons(:, :, ii+1);
        
        for jj = 1:ud.rnfiducials(ii+1)                
                % chek if the fiducial is in a polygon.
                p = ud.rfiducials(jj, 1:2, ii+1);
                
                % Find the polygon that contains the point.
                for kk = 1:numel(ud.rpolygons(:, :, ii+1))
                        thepoly = ud.rpolygons{kk, 1, ii+1};
                        
                        % When you find it, store the polygon with the same index as the fiducial.
                        if ~isempty(thepoly) && inpolygon(p(1), p(2), thepoly(:, 1), thepoly(:, 2))
                                polygon_list{jj, 1, 1} = thepoly;
                                break;
                        elseif kk == numel(ud.rpolygons(:, :, ii+1))
                                polygon_list{jj, 1, 1} = [];
                        end
                end
        end
        
        % Copy new list of polylines into the data structure.
        for jj = 1:numel(polygon_list)
                ud.rpolygons{jj, 1, ii+1} = polygon_list{jj, 1, 1};
        end
        % End reorganization of polygons.
end
end