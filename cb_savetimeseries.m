% *********************************************************************************
% cancel is 1 if the operation was canceled, 0 otherwise.
function cancel = cb_savetimeseries(fig)

extensions_images = {'*.tif'; '*.ics'; '*.*'};

ud = get(fig, 'UserData');

extensions_order = circshift(1:numel(extensions_images), [0 -1*(ud.paramimsave-1)]);
extensions_images = extensions_images(extensions_order);

curr_dir = pwd;
cd(ud.rcurrdir);

[filename, pathname, ext] = uiputfile(extensions_images, 'Save time series');

cd(curr_dir);

if ud.maxslice == 0
        ud.slices = ud.slices{:}(:, :, 0);
end

if (~ isequal(filename, 0))
        ext = extensions_order(ext);
	
        switch ext
		case 1
			format = 'tif';
                        % 8bit.
                        % Since we are going to write in "append" mode, delete the file if it already exists.
                        if exist(cat(2, pathname, filename), 'file')
                                delete(cat(2, pathname, filename)); 
                        end
                           
                        thematrix = double(ud.slices);
                        if max(thematrix(:)) >= 256
                            cast_fn = @uint16; % This prevents writing a 64bit tiff.
                        else
                            cast_fn = @uint8;
                        end% thematrix(:) arranges all pixels along a single dimension.

                        for ii = 1:ud.imsize(3)
                                imwrite(cast_fn(squeeze(ud.slices(:,:,ii-1))), cat(2, pathname, filename), format, 'Compression', 'none', 'WriteMode', 'append');
                        end
        	case 2
			format = 'ICSv2';
                        writeim(ud.slices, cat(2, pathname, filename), format, 'no');                     
		case 3
			format = 'TIFF';
                        writeim(ud.slices, cat(2, pathname, filename), format, 'no');
	end
	
        ud.paramimsave = ext;
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud);
	cancel = 0;		
else
	cancel = 1;
end
end