% *********************************************************************************
function [fidu2 nfidu2] = cb_deletedelimiter(fig, fidu, nfidu)

obj = get(fig, 'CurrentObject');
type = get(obj, 'Tag');

if strcmp(type, 'Delimiter')
	ud = get(fig, 'UserData');
	curslice = ud.curslice + 1;
	nfidu(curslice) = nfidu(curslice) - 1;
	%ind = intersect(find(fidu(:, 1, curslice) == get(obj, 'XData')), ...
	%		find(fidu(:, 2, curslice) == get(obj, 'YData')));
	ind = find((fidu(:, 1, curslice) == get(obj, 'XData')) & (fidu(:, 2, curslice) == get(obj, 'YData')));

	fidu(ind(end), :, curslice) = [-1 -1 -1];
	
	delete(obj);
end

fidu2 = fidu;
nfidu2 = nfidu;
end