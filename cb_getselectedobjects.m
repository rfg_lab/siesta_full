% *********************************************************************************
function object_list = cb_getselectedobjects(ud)

object_list = [];
polygons = ud.rpolygons;

for curslice = 1:size(polygons, 3)
	m = [];
	c = [];

	for i = 1 : size(polygons, 1)
		thepoly = polygons{i, 1, curslice};
	
		if ~ isempty(thepoly)
			% When you find the first polygon in a slice compute the
			% center of all the cells in that slice.
			if isempty(m)
				m = measure(squeeze(ud.slices(:, :, curslice - 1)), [], {'Center'}, [], 2);
				c(:, 1) = m.Center(1, :)';
				c(:, 2) = m.Center(2, :)';
			end

			% Find out if the centers of the cells are 
			% inside the polygon.			
			ind = find(pnspolym(thepoly(:, 1), thepoly(:, 2), c(:, 1), c(:, 2)));
			object_list = cat(2, object_list, m.id(ind));
			
		end
	end
end
end