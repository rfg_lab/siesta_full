% Author: Michael Wang
% Date: August 28, 2014
% 
% The following function takes a cell that is about to divide and create 2
% new seeds in the subsequent time point to represent the divided daughter
% cells. The distance between all the coordinates along the cell outline
% and the cell seed coordinate is recorded. Using this distance
% information, the parent cell seed is divded into two daughter cell seeds
% along the line cooresponding to the longest distance.
% 
% INPUT:
% polygon - contains the coordinates of the cell about to divide i.e.
% ud.rpolygons{1,:,138}
% fids - contains seed coordinates for all cells in all time points i.e.
% ud.rfiducials
% t - is the ud time (starting index 1) right before cell division 
% divInd - contains index and time information for dividing cells and their
% daughter cells WITHOUT the current cell division
% seedDist - how far along the dsitance line should the divided seeds be
% relative to the parent seed
% 
% OUTPUT:
% newFids - contains seed coordinates for all cells including the newly
% created divided seeds
% theNewSeedIndices - contains index and time information for dividing cells and their
% daughter cells INCLUDING the current cell division
% 
% ASSUMPTION:
% 1. polygon is the cell about to divide (i.e. we know which cell will divide)
% 2. each cell only has 1 seed inside its polygon

function [newFids, theNewSeedIndices]=splitFids(polygon,fids,t,divInd,seedDist)

% check to see which seed belongs in polygon
ind=inpolygon(fids(:,1,t),fids(:,2,t),polygon(:,1),polygon(:,2));

row=find(ind);

% test for row size, unnecessary but I'm scared to delete it
display('size of fids(row,1,t)');
size(fids(row,1,t))
display('size of polygon(:,1)');
size(polygon(:,1))

% makes sure there's only 1 seed in each polygon
if size(row,1)>1
    display('!!!!!ERROR!!!!!, more than 1 seed in the polygon');
    newFids=fids;
    theNewSeedIndices=[];
    return
end
    
% distance between all polygon coordinates and the seed in the polygon
dist=sqrt(((polygon(:,1)-fids(row,1,t)).^2)+((polygon(:,2)-fids(row,2,t)).^2));

% reference block of comment, does not relate to code
% compares each element of dist to its 10 neighboring values (20 total) to
% determine local max
% for i:size(dist)
%     
%     
% %     if dist(i)>dist(i+1,i+10)&& dist(i)>dist(i-1,i-10)
% %       record local max
% %         max=
% %     end
%     
% end

% find max distance and index corresponding to the polygon vertex
[maxDist,maxInd]=max(dist);

% draw line between max on polygon and seed
line=[fids(row,1:2,t);polygon(maxInd,1:2)];
% slope is change in x; change in y
slope=[line(2,1)-line(1,1);line(2,2)-line(1,2)];

% line is extended 5 times (may not be necessary)
x=line(1,1)-5*slope(1);
y=line(1,2)-5*slope(2);
newPoint=[x,y];
% lineNew=[line;newPoint];
lineNew=[polygon(maxInd,1:2);newPoint];


% find 2 points of intersection
intersects=InterX(polygon(:,1:2)',lineNew');
intersects=intersects';

% new fiducials are halfway between the 2 intersecting points
xslope=intersects(1,1)-fids(row,1,t);
yslope=intersects(1,2)-fids(row,2,t);

% first new fid
% newFid1=[fids(row,1,t)+(xslope/2),fids(row,2,t)+(yslope/2),fids(1,3,t)];
newFid1=[fids(row,1,t)+(xslope*seedDist),fids(row,2,t)+(yslope*seedDist),fids(1,3,t+1)];

% check if new seed is in the polygon (may be unnecessary)
if ~inpolygon(newFid1(1),newFid1(2),polygon(:,1),polygon(:,2));
    
end

xslope=intersects(end,1)-fids(row,1,t);
yslope=intersects(end,2)-fids(row,2,t);

% second new fid
% newFid2=[fids(row,1,t)+(xslope/2),fids(row,2,t)+(yslope/2),fids(1,3,t)];
newFid2=[fids(row,1,t)+(xslope*seedDist),fids(row,2,t)+(yslope*seedDist),fids(1,3,t+1)];

% check if new seed is in the polygon (may be unnecessary)
if ~inpolygon(newFid2(1),newFid2(2),polygon(:,1),polygon(:,2));

end

% ind = find(fids(:,1,t) == -1);
% find indices that are negative one
ind = find(fids(:,1,t+1) == -1);

% no negative ones, add to bottom
if isempty(ind)
    
    if ~ismember(newFid2,fids(:,:,t+1),'rows')
        newRow=size(fids, 1)+1;
        fids(newRow,:,:)=-1;
        %     fids(newRow,:,t)=newFid2;
        fids(newRow,:,t+1)=newFid2;
    else
        [~,newRow]=ismember(newFid2,fids(:,:,t+1),'rows');
    end

else
    newRow = ind(1);
    if ~ismember(newFid2,fids(:,:,t+1),'rows')
%     fids(newRow,:,t)=newFid2;
    fids(newRow,:,t+1)=newFid2;
    else
        [~,newRow]=ismember(newFid2,fids(:,:,t+1),'rows');
    end
end

if ~ismember(newFid1,fids(:,:,t+1),'rows')
    % fids(row,:,t)=newFid1;
    fids(row,:,t+1)=newFid1;

end

newFids=fids;




% seedDivAng=atan((mid1y-mid2y)/(mid1x-mid2x)); 

%first row represent parent, second row represent first daughter cell
newDivInd(:,:,1)=[t+1;row;row;newRow;0];
newDivInd(:,:,2)=0;
%newDivInd(:,:,2)=0;
%newDivInd=[t+1;row;newRow;newRow+1];

theNewSeedIndices=[divInd,newDivInd];
%theNewSeedIndices(:,:,2)=-1;


end