% mtrx = cb_multicolorquantifylines(fig, channels, brush_sz, lns)
function mtrx = cb_multicolorquantifylines(fig, channels, brush_sz, lns)

mtrx = zeros(numel(lns), 2+numel(channels));

for il = 1:numel(lns)
    [mtrx(il, 1) mtrx(il, 2) tmp] = cb_multicolormeasureline(fig, lns(il), channels, brush_sz);
    mtrx(il, 1) = mtrx(il, 1) * 180. / pi;
    for ic = 1:numel(channels)
        mtrx(il, ic + 2) = tmp(1, ic);
    end
    
    clear tmp;
end
end