% errorcode = savecsv(filename, param1, param2, ..., paramn)
%
% Saves data in a text file. csv is for comma-separated values, as commas will be use to separate numeric data. These files can be imported into Excel.
%
% filename is the full path to the destination file. IF THIS FILE EXISTS, savecsv WILL OVERWRITE IT.
% param1, ..., paramn can be strings or numeric data. Every parameter will be stored in its own line in the destination file. 
% ATTENTION: param1 can be a cell, and in that case, all its contents will be written independently as if they had been provided as sequential arguments to savecsv.
%
% errorcode is 0 if there is an error, 1 if the file is successfully written.
%
% Rodrigo Fernandez-Gonzalez
% 20110802
%
% Updates: 20110920 (cell in param1).

function errorcode = savecsv(varargin)

if nargin < 2
        fprintf('savecsv error: at least two parameters necessary.\n');
        errorcode = 0;
        return;
end

filename = varargin{1};

% For every parameter after the file name.
for ii = 2:nargin
        switch ii
                % For the first parameter after the file name, create the file (or delete and create it de novo if it existed already).
                case 2
                        % The second parameter can be a cell with all parameters inside.
                        if iscell(varargin{ii})
                                for jj = 1:numel(varargin{ii})
                                        switch jj
                                                % The first parameter requires dealing with file creation.
                                                case 1
                                                     switch isnumeric(varargin{ii}{jj})
                                                        case 0
                                                                dlmwrite(filename, varargin{ii}{jj}, 'delimiter', '', 'roffset', 0, 'coffset', 0);
                                                        otherwise
                                                                dlmwrite(filename, varargin{ii}{jj}, 'roffset', 0, 'coffset', 0);
                                                        end   
                                                % All other parameters are appended at the end of the file.
                                                otherwise
                                                        switch isnumeric(varargin{ii}{jj})
                                                                case 0
                                                                        dlmwrite(filename, varargin{ii}{jj}, 'delimiter', '', '-append', 'roffset', 0, 'coffset', 0);
                                                                otherwise
                                                                        dlmwrite(filename, varargin{ii}{jj}, '-append', 'roffset', 0, 'coffset', 0);
                                                        end       
                                        end
                                end
                        % End dealing with a potential cell in the second parameter.
                        
                        % If the second parameter is NOT a cell.
                        else
                                switch isnumeric(varargin{ii})
                                        case 0
                                                dlmwrite(filename, varargin{ii}, 'delimiter', '', 'roffset', 0, 'coffset', 0);
                                        otherwise
                                                dlmwrite(filename, varargin{ii}, 'roffset', 0, 'coffset', 0);
                                end
                        end
                % All other parameters are appended at the end of the file.
                otherwise
                        switch isnumeric(varargin{ii})
                                case 0
                                        dlmwrite(filename, varargin{ii}, 'delimiter', '', '-append', 'roffset', 0, 'coffset', 0);
                                otherwise
                                        dlmwrite(filename, varargin{ii}, '-append', 'roffset', 0, 'coffset', 0);
                        end
        end
end

errorcode = 1;