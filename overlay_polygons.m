% OVERLAY_POLYGONS(UD, THREED, XYZ_SCALE, SHOW_AXES, NUMCOLORS)
%
% Generates a plot overlaying all the polygons present in UD.rpolygons.
% IF THREED is 1, each polygon is assigned a height corresponding to the slice number.
% XYZ_SCALE is a three element vector that will be multiplied by the x, y and z coordinates of each point.
% If SHOW_AXES is 1, two axes showing coordinate values are displayed.
% NUMCOLORS contains the number of colors to include in the color
% map that will be used to plot the polygons. If this parameter is
% not present, or if it is empty, the function looks for the last time point with polygons and uses its number as the number of colors.

function overlay_polygons(ud, threeD, xyz_scale, show_axes, numcolors)

if nargin < 1
    error('Too few parameters!');
else
    thepolygons = ud.rpolygons; % Read the polygons from the annotation structure.
end

if nargin < 2
        threeD = 0; % By default plot in 2D.
end

if nargin < 3
        xyz_scale = [1 1 1]; % No scaling.
end        

if nargin < 4
        show_axes = 0; % No axes.
end        

if nargin < 5 || isempty(numcolors)
        % Find the last time point with polygons.
        last = -1;
        for ii = 1:size(thepolygons, 3)
                apoly = thepolygons{1, :, ii};
                if ~ isempty(apoly)
                        last = ii;
                end
        end
        
        if last == -1
                disp('No polygons defined in this movie.');
                return;
        end        
        
        % Color map: blue corresponds to early time points, dark to later ones.
        cm = jet(last);
        
else
        cm = jet(numcolors);
end


% Set the brush size to use in the plots.
if isfield(ud, 'rBRUSH_SZ')
        rBRUSH_SZ = ud.rBRUSH_SZ;
else
        rBRUSH_SZ = 3;
end

% Create figure.
fig = figure;
hold on;
axis ij equal;

% This indexes over colors. It is necessary because the number of time points of the movie and the number of time points that the user wants to plot, or the number of colors that the user wants to use, may be different.
icolor = 1;

% For every polygon.
for ii = 1:size(thepolygons, 3)
        apoly = thepolygons{1, :, ii};
        if size(apoly, 2) == 2
            apoly(1:size(apoly, 1), 3) = repmat(ii-1, [size(apoly, 1), 1]);
        end
        

        % 3D plot.
        if threeD && numel(apoly) > 0
                h = plot3(apoly(:, 1).*xyz_scale(1), apoly(:, 2).*xyz_scale(2), apoly(:, 3).*xyz_scale(3), 'Color', cm(icolor, :), 'LineWidth', rBRUSH_SZ);
                
                axis vis3d;
                axis equal;
                
                icolor = icolor + 1;
        % 2D plot (all polygons have the same height).
        elseif numel(apoly) > 0
                h = plot(apoly(:, 1).*xyz_scale(1), apoly(:, 2).*xyz_scale(2), 'Color', cm(icolor, :), 'LineWidth', rBRUSH_SZ);
                
                axis equal;
                
                icolor = icolor + 1;
        end 
end

% If necessary, remove axes and set background color to white.
if ~ show_axes
        set(gca, 'Visible', 'off');
        set(fig, 'Color', [1 1 1]);
end        


