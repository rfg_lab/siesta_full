% [cancel, filename, pathname, ext] = cb_loadcolorimage(fig)
% cancel is 1 if the operation was canceled, 0 otherwise.
function [cancel, filename, pathname, ext] = cb_loadcolorimage(fig)

extensions_images = {'*.tif'; '*.ics'; '*'};

ud = get(fig, 'UserData');

extensions_order = circshift(1:numel(extensions_images), [0 -1*(ud.paramimopen-1)]);
extensions_images = extensions_images(extensions_order);

curr_dir = pwd;
cd(ud.rcurrdir);

brush_sz = ud.rBRUSH_SZ;
load_annt = ud.rLOAD_ANNTIME;
if isfield(ud, 'rDRAW_ALL')
    draw_all = ud.rDRAW_ALL;
else
    draw_all = 0;
end

if isfield(ud, 'rSAVE_GEOM')
    save_geom = ud.rSAVE_GEOM;
else
    save_geom = 0;
end

if isfield(ud, 'rNODESANDEDGES')
    nodes_edges = ud.rNODESANDEDGES;
else
    nodes_edges = 1;
end

thezoom = ud.zoom;

[filename, pathname, ext] = uigetfile(extensions_images, 'Load color image');

cd(curr_dir);
if (~ isequal(filename, 0))
    ext = extensions_order(ext);
    
    % Delete markings on this time point if there are any.
    ud = cb_deleteall(fig);
    
    % For series of files ...
    try
        in = readtimeseries(cat(2, pathname, filename), '', [0 0], 'yes', 'no');
        % For a multi-plane file ...
    catch
        switch ext
            case 1 % Read multipage tiff
                in = tiffread(cat(2, pathname, filename), 1);
            case 2 % Read multipage ics.
                in = readim(cat(2, pathname, filename));
            otherwise
                in = tiffread(cat(2, pathname, filename));
        end
        
    end
    
    if ndims(in{1}) == 2
        in{1} = cat(3, in{1}, in{1});
        in{2} = cat(3, in{2}, in{2});
        in{3} = cat(3, in{3}, in{3});
        in = joinchannels('RGB', in{1}, in{2}, in{3});
 %  elseif ndims(in{1}) == 3
 %    theim = in;
 %    clear in;
 %    in{1} = theim(:, :, 0:3:end);
 %    in{2} = theim(:, :, 1:3:end);
 %    in{3} = theim(:, :, 2:3:end);
 %    in = joinchannels('RGB', in{1}, in{2}, in{3});
    end
    
    % Read a grayscale image as a color image.
    if ~ iscolor(in)
        in = joinchannels('RGB', in{1}, in{1}, in{1});
    end
    
    % Delete markings on this time point if there are any.
    ud = cb_deleteall(fig);
    
    ud.rlns = cb_clearlines(fig, ud.rlns); % Remove all the mask lines previously drawn (the fig will be lost when we call init_userdata).
    ud.imsize = size(in{1});
    ud = init_userdata(ud);
    ud.paramimopen = ext;
    ud.rcurrdir = pathname;
    ud.maxslice = ud.imsize(3) - 1;
    ud.slices = in;
    ud.curslice = 0;
    
    %if ndims(in{1}) == 3
    %                  size(ud.imagedata)
    %                  size(in{1})
    %                  disp(ud.curslice)
    ud.imagedata = cat(3, squeeze(in{1}(:, :, ud.curslice)), squeeze(in{2}(:, :, ud.curslice)), squeeze(in{3}(:, :, ud.curslice)));
    %elseif ndims(in{1}) == 2
    %        ud.imagedata = cat(3, squeeze(in{1}(:, :)), squeeze(in{2}(:, :)), squeeze(in{3}(:, :)));
    %end
    ud.mappingmode = 'lin';
    ud.colspace = 'RGB';
    
    ud.colordata = squeeze(in(:, :, ud.curslice));
    ud.rBRUSH_SZ = brush_sz;
    ud.rLOAD_ANNTIME = load_annt;
    ud.rDRAW_ALL = draw_all;
    ud.rSAVE_GEOM = save_geom;
    ud.rNODESANDEDGES = nodes_edges;
    ud.zoom = thezoom;
    %ud.winsize = 1;
    %ud.pan = pan;
    %ud.currange = [min(ud.imagedata(:)), max(ud.imagedata(:))];
    %ud.filename = filename;
    cancel = 0;
   
    ax_children = get(get(fig, 'CurrentAxes'), 'Children');
    img = findobj(ax_children, 'Type', 'image');
    
    % Change image object size.
    set(img, 'XData', [0 (ud.imsize(1)-1)], 'YData', [0 (ud.imsize(2)-1)]);
    
    display_data(fig, img, ud);
    
    % Change figure size.
    diptruesize(fig, 100*thezoom);
    %fprintf('\n%d  |\n', ud.rLOAD_ANNTIME);
    % Load markings if the option is selected and they exist.
    if ud.rLOAD_ANNTIME
        if exist(fullfile(ud.rcurrdir, 'annotations_field.mat'), 'file')
            mycallbacks2(fig, 'load_ann', fullfile(ud.rcurrdir, 'annotations_field.mat'));
        elseif exist(fullfile(ud.rcurrdir, 'annotations_cut.mat'), 'file')
            mycallbacks2(fig, 'load_ann', fullfile(ud.rcurrdir, 'annotations_cut.mat'));
        elseif exist(fullfile(ud.rcurrdir, 'annotations_fraproi.mat'), 'file')
            mycallbacks2(fig, 'load_ann', fullfile(ud.rcurrdir, 'annotations_fraproi.mat'));
        end
    end
else
    cancel = 1;
end


end

