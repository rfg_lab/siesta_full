% *********************************************************************************
% tpo and tpf start from 1.
% Shift seeds using flows.
function ud = cb_propagateseeds(fig, tpo, tpf, ws)
ud = get(fig, 'UserData');
ud_old = ud;

if abs(tpo-(ud.curslice+1)) == 0
        tps = tpo:tpf;
elseif abs(tpf-(ud.curslice+1)) == 0
        tps = tpf:-1:tpo;
elseif abs(tpo-(ud.curslice+1)) ~= 0 && abs(tpf - (ud.curslice+1)) ~= 0
        fprintf('\ERROR: at least one of the time points must be adjacent to the current one.\n');
        ud = ud_old;
        return;
end

[ud.rfiducials(:, :, tpo) ud.rnfiducials(tpo)] = cb_consolidatefiducials(ud.rfiducials(:, :, tpo));

for ii = 1:(numel(tps))
        thetp = tps(ii);
        
        if ii == 1
                prevtp = ud.curslice + 1;
        else
                prevtp = tps(ii-1); % This could be the next or the previous, depending on whether we are backwards or forward, respectively.
        end
        
        next_seeds = ud.rfiducials(:, :, prevtp);                
        
        ind = find(next_seeds(:, 3) == (prevtp-1));
        [Xflow Yflow] = rflow(squeeze(ud.slices(:, :, prevtp-1)), squeeze(ud.slices(:, :, thetp-1)), ud.rfiducials(ind, 1:2, prevtp), ws, 0, 0); % This shifts the seeds without paying attention to the segmentation results.
        
        next_seeds(ind, 1) = next_seeds(ind, 1) + Xflow;
        next_seeds(ind, 2) = next_seeds(ind, 2) + Yflow;
        next_seeds(ind, 3) = thetp - 1;
        
        % Clip at the ends: the seeds cannot be beyond the image margins.
        ind2 = find(next_seeds(:, 1) < 0);
        next_seeds(intersect(ind, ind2), 1) = 0;
        ind2 = find(next_seeds(:, 1) >= ud.imsize(1));
        next_seeds(intersect(ind, ind2), 1) = ud.imsize(1)-1;
        ind2 = find(next_seeds(:, 2) < 0);
        next_seeds(intersect(ind, ind2), 2) = 0;
        ind2 = find(next_seeds(:, 2) >= ud.imsize(2));
        next_seeds(intersect(ind, ind2), 2) = ud.imsize(2)-1;

        ud.rfiducials(1:size(next_seeds, 1), 1:3, thetp) = next_seeds;
        ud.rnfiducials(thetp) = numel(ind);
        
        fprintf('origin slice %d, destination slice %d\n', prevtp-1, thetp-1);
end
end