% *********************************************************************************
function [ud myind] = cb_pastepolygon(fig)
ud = get(fig, 'UserData');

if isempty(ud.rpoly)
	myind = -1;
	return;
end

myind = 1;
for i = 1:size(ud.rpolygons, 1)
	if (isempty(ud.rpolygons{i, 1, ud.curslice+1}))
		ud.rpolygons{i, 1, ud.curslice + 1} = ud.rpoly;
		break;
	end
	myind = myind + 1;
end

% If there were no empty spots, store at the end.
if (myind > size(ud.rpolygons, 1))
	ud.rpolygons{end+1, 1, ud.curslice + 1} = ud.rpoly;
end
	
end