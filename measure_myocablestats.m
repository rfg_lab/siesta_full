% [chain_lengths szav n normAPn normMYOn normALLn pALL pMYO pAP chains] = 
%       measure_myocablestats(g, selected_edges, roi, min_edgelength, ang) pershellanalysis)
%
% Measures the properties of the cables formed by the selected edges in a certain geometry g.
%
% Rodrigo Fernandez-Gonzalez
% 2009/03/03
function [chain_lengths szav n normAPn normMYOn normALLn pALL pMYO pAP chains] = measure_myocablestats(g, selected_edges, roi, min_edgelength, ang)

max_size = 15;

% Initialize output parameters.
chain_lengths = []; % Contains the length of each one of the cables identified.
n = zeros(1, max_size);
% Control input parameters.
if nargin < 3
	return;
end

if nargin < 4
	min_edgelength = 3;
end

if nargin < 5
        ang = 60;
end        

% Obtain geometry from user data.
nodes = g.nodes;
edges = g.edges;

% Remove edges outside the ROI.
[newnodes newedges] = mmaskpositions(nodes, edges, roi);
edges = edges(newedges, :);

nn = size(nodes, 1);
ne = size(edges, 1);

% edge_cm == POSITIONS: use the center of mass of each one of the edges as the POSITIONS argument. Some of these things are not necessary here.
edge_cm = zeros(ne, 2);
edge_l = zeros(ne, 1);
edge_ang = zeros(ne, 1);

for ii = 1 : ne
	pair_coords = nodes(edges(ii, 1:2), :);
	edge_cm(ii, :) = (pair_coords(2, :) + pair_coords(1, :)) ./2;

	x = pair_coords(:, 2);
	y = pair_coords(:, 1);
	
	% First point of the segment is that with the smallest X value.
        if (x(1) < x(2))
      	 p1 = [x(1) y(1)];
	 p2 = [x(2) y(2)];
        else
         p2 = [x(1) y(1)];
	 p1 = [x(2) y(2)];
        end
      
        % Compute vector.
        v = p2 - p1;
        edge_l(ii) = norm(v, 2);
      
        % If the second point is under the first one (largest y)
        % then the angle is larger than pi/2.
        if (edge_l(ii) ~= 0 && p2(2) > p1(2))
         val = pi - asin(abs(v(2))/edge_l(ii));
        % Else the angle is smaller than pi/2.
        elseif (edge_l(ii) ~= 0)
         val = asin(abs(v(2)) / edge_l(ii));
        % Else where are trying to measure the angle formed by a dot!!
        else
      	 val = 0.0;
        end        
	edge_ang(ii) = val * 180 / pi;
end

% selected_edges point to the original index list, not the the list of edges within the ROI.
% Now we make the list point to edges within the ROI.
[tmp, selected_edges] = intersect(newedges, selected_edges);
edge_status = zeros(1, ne);
edge_status(selected_edges) = 1;

% Now extract neigh_graph. Here you are extracting information about neighborhood between edges.
neigh_graph = zeros(ne);

for ii = 1 : nn
	%disp(cat(2, 'Loop 2, iteration ', num2str(i)));
	a1 = find(edges(:, 1) == ii);
	a2 = find(edges(:, 2) == ii);
	a = union(a1, a2); % All these are the indices of edges_cm that should be made neighbors. UNION removes repeated elements.
	if (isempty(a) | numel(a) == 1) 
		continue;
	end

	combs = nchoosek(a, 2);

	neigh_graph(sub2ind(size(neigh_graph), combs(:, 1), combs(:, 2))) = 1;
	combs = circshift(combs, [0 1]);
	neigh_graph(sub2ind(size(neigh_graph), combs(:, 1), combs(:, 2))) = 1;

end

% This code was borrowed from alignment.m
chainstmp = cell(0);
inchain = []; % Cotains myo+ edges (in selected_edges) that have been visited already.

inds = selected_edges;

% For every "interesting" edge ...
for ii = 1 : numel(inds)
        % ... that has not been visited already ...
        if ~ ismember(inds(ii), inchain)
                % Find the chain it belongs to.
                achain = chain(edge_status, neigh_graph, inds(ii));
                
                % Put all edges in this chain 
                % in the list of visited ones.
                inchain = union(inchain, achain);

                % Remove chained edges that are too short (consider as vertices).
                %if ~ isempty(edge_length)
                %       l = edge_length(achain);
                %       long_edges = find(l >= min_length);
                %       achain = achain(long_edges);
                %end
                
                % Store that chain (only long enough edges) and its length.
                long_edges = find(edge_l(achain) > min_edgelength);
                achain = achain(long_edges);
                %chain_lengths(end+1) = numel(achain) - sum(edge_length(achain) <= min_length);
                if ~ isempty(achain)
                        chain_lengths(end+1) = numel(achain);
                        chainstmp{end+1} = achain;
                end
        end
end

ind = find(chain_lengths > 0);
chain_lengths = chain_lengths(ind);

chains = cell(1, numel(ind));

for ii = 1:numel(ind)
        chains{ii} = chainstmp{ind(ii)};
end


% End code borrowed from alignment.m

% Compute chain statistics.
% Compute number of AP edges (taken from measure_mfn).
if ang >=0 
        nAP = numel(find(edge_l >= min_edgelength & edge_ang >= ang & edge_ang <= (180-ang)));
else
        ang = - ang;
        nAP = numel(find(edge_l >= min_edgelength & (edge_ang <= ang | edge_ang >= (360-ang))));
end

% Cluster size distribution, n(size) (number clusters of size sz per AP edge).
sz_vector = 1:max_size;

for sz = sz_vector
        n(sz) = numel(find(chain_lengths == sz));
        normALLn(sz) = n(sz) / size(edges, 1); % Normalized to all edges (clusters of size sz per edge).
        normAPn(sz) = n(sz) / nAP; % Normalized to "AP" edges. This is the good one (clusters of size sz per AP edge).
        normMYOn(sz) = n(sz) / nnz(edge_status); % Normalized to positive edges (clusters of size sz per selected edge).
end


pALL = sum(sz_vector .* normALLn); % Probability that any edge is myosin +.
pAP = sum(sz_vector .* normAPn); % Probability that an AP edge is myosin +.
pMYO = sum(sz_vector .* normMYOn); % Probability that a Myo+ edge is myosin + == 1.

% These four all provide the same result. Weighted average cluster size.
szav = sum(sz_vector .* sz_vector .* n) / sum(sz_vector .* n);

szavALL = sum(sz_vector .* sz_vector .* normALLn) / pALL;
szavAP = sum(sz_vector .* sz_vector .* normAPn) / pAP;
szavMYO = sum(sz_vector .* sz_vector .* normMYOn) / pMYO;



