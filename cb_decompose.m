% cb_decompose(fig)
function cb_decompose(fig)

ax_children = get(get(fig, 'CurrentAxes'), 'Children');
img = findobj(ax_children, 'Type', 'image');
ud = get(fig, 'UserData');

if size(ud.slices, 1) == 3 && size(ud.imagedata, 3) == 3
	ud.rcurrchannel = 1;
	ud.rchannels = ud.slices;
	ud.slices = ud.rchannels{1};
	ud.colordata = [];
	ud.imagedata = ud.slices(:, :, ud.curslice);
	ud.colspace = '';
	display_data(fig, img, ud);
end
end
