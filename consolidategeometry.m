% Removes two-way nodes (by removing all edges connected to it). Does not modify node positions.
function geom_out = consolidategeometry(geom_in)

max_loop = 10;

n = geom_in.nodes;
e = geom_in.edges;

% Remove nodes that are only shared by two edges.
[a b] = mmrepeat(sort(reshape(e, [1, numel(e)])));
remove_nodes = a(find(b==2));

delete_edges = [];
keep_edges = [];
new_edges = [];

for in = 1:numel(remove_nodes)
	anode = remove_nodes(in);
	involved_edges = cat(1, find(e(:, 1) == anode), find(e(:, 2) == anode));
	delete_edges = cat(1, delete_edges, involved_edges);
	involved_edges = [e(involved_edges(1), :); e(involved_edges(2), :)];
	keep_edges = cat(1, keep_edges, intersect(find(e(:, 1) ~= anode), find(e(:, 2) ~= anode)));
	
	anewedge = sort(reshape(involved_edges(find(involved_edges ~= anode)), [1, 2]));
	
	loop_control = 0;
	
	if ~ isempty(find(remove_nodes == anewedge(1)))
		prev = anode;
		thenode = anewedge(1);
		while (true)
			neighbors = e(cat(1, find(e(:, 1) == thenode), find(e(:, 2) == thenode)), :);
			nxt = unique(neighbors(intersect(find(neighbors ~= prev), find(neighbors ~= thenode))));

			if isempty(find(remove_nodes == nxt)) | (loop_control > max_loop)
				anewedge(1) = nxt;
				break;
			else
				prev = thenode;
				thenode = nxt;
				loop_control = loop_control + 1;
			end
		end
	end
	
	if loop_control <= max_loop
		loop_control = 0;
	else
		%disp('WARNING while consolidating geometry.');
		continue;
	end
	
	if ~ isempty(find(remove_nodes == anewedge(2)))
		prev = anode;
		thenode = anewedge(2);
		while (true)
			neighbors = e(cat(1, find(e(:, 1) == thenode), find(e(:, 2) == thenode)), :);
			nxt = unique(neighbors(intersect(find(neighbors ~= prev), find(neighbors ~= thenode))));
			
			if isempty(find(remove_nodes == nxt)) | (loop_control > max_loop)
				anewedge(2) = nxt;
				break;
			else
				prev = thenode;
				thenode = nxt;
				loop_control = loop_control + 1;
			end
		end
	end

	if loop_control > max_loop
		%disp('WARNING while consolidating geometry.');
		continue;
	end

	
	anewedge = sort(anewedge);
	if isempty(new_edges) || (~isempty(new_edges) && (isempty(intersect(find(new_edges(:, 1) == anewedge(1)), find(new_edges(:, 2) == anewedge(2))))))
		new_edges(end+1, :) = anewedge;
	end
end

keep_edges = setdiff(unique(keep_edges), unique(delete_edges));

e = cat(1, e(keep_edges, :), new_edges);

geom_out.nodes = n;
geom_out.edges = e;
geom_out.nodecellmap = geom_in.nodecellmap;
