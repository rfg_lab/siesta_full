% PNSPOLYM Determines whether a group of points are inside or outside a polygon.
%
%   PNSPOLYM(XPOL, YPOL, XPNTS, YPNTS) returns a vector with element i set to 1 if 
%   the point determined by (XPNTS(i), YPNTS(i)) is inside the polygon defined by 
%   the vectors (XPOL, YPOL), or 0 otherwise.
%
%   W Randolph Franklin (WRF) 
%   http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html
%   
%   Matlab code by:
%   
%   Rodrigo Fernandez-Gonzalez
%   fernanr1@mskcc.org
%   2007/08/18

function c = pnspolym(xp, yp, xpns, ypns)
% Number of points to check. */

npn = numel(xpns);
c = zeros(1, npn);

npol = numel(xp);
	
for pns = 1:npn
	x = xpns(pns);
	y = ypns(pns);
	
	i = 0;
	j = npol - 1;
	for i = 0:(npol-1) 
	  if ((((yp(i+1)<=y) && (y<yp(j+1))) || ...
	       ((yp(j+1)<=y) && (y<yp(i+1)))) && ...
	      (x < (xp(j+1) - xp(i+1)) * (y - yp(i+1)) / (yp(j+1) - yp(i+1)) + xp(i+1)))
		    c(pns) = ~ c(pns);
	  end
	   
	  j = i;
	  
	end
end  


