function udata = set_udata_computed_log(udata,cdata)
% won't work if any pixel is non-finite
if ~isfield(udata,'computed') | ~isfield(udata.computed,'canlog')
   udata.computed.canlog = '';
   if length(udata.imsize)==3 & udata.globalstretch
      if isempty(udata.colspace)
         if any(~isfinite(udata.slices))
            udata.computed.canlog = 'non-finite values';
         end
      else
         for ii=1:udata.channels
            if any(~isfinite(udata.slices{ii}))
               udata.computed.canlog = 'non-finite values';
               break;
            end
         end
      end
   else
      if any(~isfinite(cdata))
         udata.computed.canlog = 'non-finite values';
      end
   end
end
if isempty(udata.computed.canlog) & ~isfield(udata.computed,'log')
   if length(udata.imsize)==3 & udata.globalstretch
      if ~isempty(udata.colspace) & ~strcmp(udata.colspace,'RGB')
         warning('Cannot perform global stretching on non-RGB color images.')
         % Don't worry about this. It cannot happen.
      else
         cdata = udata.slices;
         if ~isempty(udata.colspace)
            %??? Quick & dirty solution: use only green channel.
            cdata = cdata{2};
         end
         cdata = mapcomplexdata(cdata,udata.complexmapping);
      end
   end
   if ~isfield(udata,'computed') | ~isfield(udata.computed,'lin')
      udata.computed.lin = [min(cdata),max(cdata)];
   end
   lims = udata.computed.lin;
   if lims(2)-lims(1) > 0
      med = (median(cdata) - lims(1)) / (lims(2) - lims(1));
      if med == 0
         med = (mean(cdata) - lims(1)) / (lims(2) - lims(1));
      end
      med = ((med-1)/med).^2;
      udata.computed.log = [med-1,log2(med)];
   else
      udata.computed.canlog = 'only one value';
   end
end
end

