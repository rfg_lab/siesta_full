%s starting node index
%d end node index
%A weight matrix (SPARSE). A(i,j) is the distance between the node indexed by i and the node indexed by j.
%
%[path, pathcost] = dijkstra(A, s, d);
