function [dumbbell1,dumbbell2]=dumbellCheck(dist)       
        
        [maxVal,maxInd]=max(dist);
        [minVal,minInd]=min(dist);
        
        numEl=size(dist,1);
                
        minIndLow=round(minInd-(numEl/16));
        minIndHigh=round(minInd+(numEl/16));
        maxIndLow=round(maxInd-(numEl/16));
        maxIndHigh=round(maxInd+(numEl/16));
        
        halfShift=circshift(dist,round(numEl/2));
        
        if minIndLow<1
            otherMin=halfShift(numEl+minIndLow:numEl);
            otherMin=[otherMin;halfShift(1:minInd)];
            otherMin=[otherMin;halfShift(minInd+1:minIndHigh)];
        elseif minIndHigh>numEl
            otherMin=halfShift(round(minIndLow):numEl);
            otherMin=[otherMin;halfShift(1:round(minIndHigh-numEl))];
        else
            otherMin=halfShift(minIndLow:minIndHigh);
        end
        
        if maxIndLow<1
            otherMax=halfShift(numEl+maxIndLow:numEl);
            otherMax=[otherMax;halfShift(1:maxInd)];
            otherMax=[otherMax;halfShift(maxInd+1:maxIndHigh)];
        elseif maxIndHigh>numEl
            otherMax=halfShift(round(maxIndLow):numEl);
            otherMax=[otherMax;halfShift(1:round(maxIndHigh-numEl))];
        else
            otherMax=halfShift(maxIndLow:maxIndHigh);
        end
                
%         cond2=otherMin<(maxVal*0.5);
%         cond3=otherMax>(maxVal*0.7); % change this to be a variable in div settings
%         % check conditions for dumbell shape used to be 0.4 instead of 0.45
%         if (minVal/maxVal)<divSetting && nnz(cond2) %&& nnz(cond3)
%         %if (minVal/maxVal)<0.4 && (dist(int8(checkEl))/maxVal)<0.6 && (dist(int8(checkElMax))/maxVal>0.8)
% 
%             yesDumbbell=1;
%         else
%             yesDumbbell=0;
%         end
        
        dumbbell1=minVal/maxVal;
        dumbbell2=min(otherMin/maxVal);
end