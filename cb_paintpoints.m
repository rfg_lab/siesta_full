function cb_paintpoints(fig, fidu, fiducial_sz, fiducial_color, sel_fidu, selected_fiducial_color)

cb_erasepoints(fig);

ud = get(fig, 'UserData');
curslice = ud.curslice + 1;

for i = 1 : size(fidu, 1)
	coords = fidu(i, 1:2, curslice);
	
	if sum(coords == [-1 -1]) ~= 2
		if (curslice == sel_fidu(3)) && ((i == sel_fidu(1)) || (i == sel_fidu(2)))
			cb_drawpoint(fig, coords, fiducial_sz, selected_fiducial_color, 'Fiducial');
		else
			cb_drawpoint(fig, coords, fiducial_sz, fiducial_color, 'Fiducial');
		end
	end
end
end