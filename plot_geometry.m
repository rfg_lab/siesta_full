%PLOT_GEOMETRY Displays the 2D segmentation of an embryo.
%   PLOT_GEOMETRY(nodes, edges, color) draws the segmentation output. nodes is a matrix of
%   [X Y] coordinates, and edges is a matrix of [i j] indeces into the rows of nodes.
%
%   PLOT_GEOMETRY(nodes, edges, color, newfig, xlimits, ylimits) uses the two-element vectors
%   xlimits and ylimits to determine the visible are on the output figure which can be
%   newly created (newfig == 1) or the current figure (newfig == 2).
%
%   color defaults to 'b', xlimits defaults to [0 671] and ylimits defaults to [0 511].
%
% Rodrigo Fernandez-Gonzalez
% fernanr1@mskcc.org
% 2007/10/17

function plot_geometry(nodes, edges, color, newfig, xlimits, ylimits, marker_sz, line_wd, node_flag)
if nargin < 9
        node_flag = 1;
end

if nargin < 8
        line_wd = 1;
end

if nargin < 7
	marker_sz = 4;
end

if nargin < 6 | isempty(ylimits)
	ylimits = [0 511];
end

if nargin < 5 | isempty(xlimits)
	xlimits = [0 671];
end

if nargin < 4
	newfig = 1;
end

if nargin < 3
	color = 'b';
end

if nargin < 2
	error('NODES and EDGES parameters are necessary to plot a geometry.');
end

%  if min(nodes(:, 1)) <=0
%  	nodes(:, 1) = nodes(:, 1) + 1 + abs(min(nodes(:, 1)));
%  end
%  
%  if min(nodes(:, 2)) <=0
%  	nodes(:, 2) = nodes(:, 2) + 1 + abs(min(nodes(:, 2)));
%  end

nn = size(nodes, 1);
ne = size(edges, 1);

if newfig
	figure; 
end

hold on;
x_coords = [];
y_coords = [];

line_wd = 3;node_flag = 0;
%line_wd = 3; marker_sz = 6;
if nargin == 5 & numel(xlimits) > 2
        ncm = xlimits;
        xlimits = [0 671];
        cell_ids = unique(ncm(:, 1));
        for ii = 1:numel(cell_ids)
	        ind = find(ncm(:, 1) == cell_ids(ii));
	        thenodes = nodes(ncm(ind, 2), :);
	        if size(thenodes, 1) < 3
		        continue;
	        end
	        %if cell_ids(ii) == 343
	        %	plot(thenodes(:, 2), thenodes(:, 1), 'bo'); %Right nodes are plotted.
	        %end
	        coords = [mean(thenodes(:, 2)), mean(thenodes(:, 1))];
	        lh = plot(coords(1), coords(2), 'o');
	        set(lh, 'Color', 'g', 'MarkerSize', 2);
	        lh = text(round(coords(1)), round(coords(2)), num2str(cell_ids(ii)));
	        set(lh, 'FontSize', 6);
        end
end
%tic
% Prepare coordinates and then plot all edges at once.
for i = 1 : ne
	pair_coords = nodes(edges(i, 1:2), :);
        if (pair_coords(1, 1) ~= pair_coords(2, 1) | pair_coords(1, 2) ~= pair_coords(2, 2))
		x_coords(:, end+1) = pair_coords(:, 1);
		y_coords(:, end+1) = pair_coords(:, 2);
		%h = text(mean(pair_coords(:, 2)), mean(pair_coords(:, 1)), num2str(i));
		%set(h, 'FontSize', 6, 'Color', [1 1 1]);
	end
end

if node_flag
        lh=plot(y_coords, x_coords);
        set(lh, 'Color', color, 'Tag', 'edge', 'Marker', 'o', 'MarkerFaceColor', color, 'MarkerSize', marker_sz, 'LineWidth', line_wd);
else
        lh=plot(y_coords, x_coords);
        set(lh, 'Color', color, 'Tag', 'edge', 'LineWidth', line_wd);
end
%toc
% Plot nodes.
%x_coords = [];
%y_coords = [];

%for i = 1 : nn
%	lh = plot(nodes(i, 2), nodes(i, 1), 'o');
%	set(lh, 'Color', color, 'MarkerSize', 4, 'MarkerFaceColor', color, 'Tag', 'node');

%end

%toc
axis ij; % Origin @ upper left corner.
axis equal; % Same units in both axes.

% Good only for images from our spinning disc.
xlim(xlimits);
ylim(ylimits);
