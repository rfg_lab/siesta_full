% [cancel, filename, pathname, ext] = cb_loadtimeseries(fig)
% cancel is 1 if the operation was canceled, 0 otherwise.

function cancel = cb_loadtimeseries(fig, filename, in)
    extensions_images = {'*.tif'; '*.ics'; '*'};

    ud = get(fig, 'UserData');

    extensions_order = circshift(1:numel(extensions_images), [0 -1*(ud.paramimopen-1)]);
    extensions_images = extensions_images(extensions_order);

    curr_dir = pwd;
    cd(ud.rcurrdir);
    brush_sz = ud.rBRUSH_SZ;
    load_annt = ud.rLOAD_ANNTIME;

    if isfield(ud, 'rDRAW_ALL')
	draw_all = ud.rDRAW_ALL;
    else
	draw_all = 0;
    end

    if isfield(ud, 'rSAVE_GEOM')
        save_geom = ud.rSAVE_GEOM;
    else
        save_geom = 0;
    end

    if isfield(ud, 'rNODESANDEDGES')
        nodes_edges = ud.rNODESANDEDGES;
    else
        nodes_edges = 0;
    end

    thezoom = ud.zoom;

    if ~exist('filename', 'var') && ~exist('in', 'var')
        [filename, pathname, ext] = uigetfile(extensions_images, 'Load time series');
    elseif exist('filename', 'var') || (exist('filename', 'var') && isempty(filename)) 
        pathname = '';
        ext = '';
    end
    cd(curr_dir);

    if (~ isequal(filename, 0))
	if ~isempty(ext), ext = extensions_order(ext); end
	
	% Delete markings on this time point if there are any.
	ud = cb_deleteall(fig);
	
        if ~exist('in', 'var')
            % For series of files ...
            try
                in = readtimeseries(cat(2, pathname, filename), '', [0 0], 'no', 'no');
                % For a multi-plane file ...
            catch
                switch ext
                  case 1 % Read multipage tiff
                    in = tiffread(cat(2, pathname, filename));
                  case 2 % Read multipage ics.
                    in = readim(cat(2, pathname, filename));
                  otherwise
                    in = tiffread(cat(2, pathname, filename));
                end

            end
        end
        % Deal with 2D images: make pseudo 3D.
        if ndims(in) == 2
            in = cat(3, in, in);
        end
        
        
        % Deal with 4D image: for now just take the first plane.
        if ndims(in) == 4
            in = squeeze(in(:, :, 0, :));
        end

        ud.imsize = size(in);
        ud = init_userdata(ud);
        ud.paramimopen = ext;
        ud.rcurrdir = pathname;
        ud.maxslice = ud.imsize(3) - 1;
        ud.slices = in;
        ud.curslice = 0;
        ud.imagedata = squeeze(in(:, :, ud.curslice));
        ud.mappingmode = 'lin';
        ud.colspace = '';
        ud.colordata = '';
        ud.rBRUSH_SZ = brush_sz;
        ud.rLOAD_ANNTIME = load_annt;
        ud.rDRAW_ALL = draw_all;
        ud.rSAVE_GEOM = save_geom;
        ud.rNODESANDEDGES = nodes_edges;
        ud.zoom = thezoom;
        %    ud.winsize = 1;
        %ud.pan = 0;
        %ud.currange = [min(ud.imagedata(:)), max(ud.imagedata(:))];
        cancel = 0;
        
        ax_children = get(get(fig, 'CurrentAxes'), 'Children');
        img = findobj(ax_children, 'Type', 'image');
        
        % Change image object size.
        set(img, 'XData', [0 (ud.imsize(1)-1)], 'YData', [0 (ud.imsize(2)-1)]);

        display_data(fig, img, ud);
        %    tp = findobj(get(fig, 'Children'), 'Tag', 'tp');
        %set(tp, 'String', 0);
        
	% Change figure size.
	diptruesize(fig, 100*thezoom);
	%fprintf('\n%d  |\n', ud.rLOAD_ANNTIME);
	% Load markings if the option is selected and they exist.
	if ud.rLOAD_ANNTIME
            if exist(fullfile(ud.rcurrdir, 'annotations_field.mat'), 'file')
                mycallbacks2(fig, 'load_ann', fullfile(ud.rcurrdir, 'annotations_field.mat'));
            elseif exist(fullfile(ud.rcurrdir, 'annotations_cut.mat'), 'file')
                mycallbacks2(fig, 'load_ann', fullfile(ud.rcurrdir, 'annotations_cut.mat'));
            elseif exist(fullfile(ud.rcurrdir, 'annotations_fraproi.mat'), 'file')
                mycallbacks2(fig, 'load_ann', fullfile(ud.rcurrdir, 'annotations_fraproi.mat'));                     
            end	
 end
 
 
    else
	cancel = 1;
    end
end