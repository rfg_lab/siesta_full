% Finds rosettes on a 2D geometry. It calls CONSOLIDATEGEOMETRY2D beforehand
% with geometry(index) and minnodedist as parameter. Also, it tries to read
% and roi from disk if it is not provided as an argument.
%
% [rosette_celllist g rosette_node_indeces] = findrosettes2D(geometry, index, minnodedist)
% [rosette_celllist g rosette_node_indeces] = findrosettes2D(geometry, index, minnodedist, roi)
% [rosette_celllist g rosette_node_indeces] = findrosettes2D(geometry, index, minnodedist, roi, minrosettecells, maxrosettecells)

function [rosette_celllist g rosette_node_indeces] = findrosettes2D(geometry, index, minnodedist, roi, minrosettecells, maxrosettecells)

if nargin < 6
	maxrosettecells = inf;
end

if nargin < 5
	minrosettecells = 5;
end
%minnodedist = 5; % Minimum distance between nodes to consider them separated.

n = geometry(index).nodes;
e = geometry(index).edges;
cnm = geometry(index).nodecellmap;

if nargin < 4
	roi = load_roi(index);
end

% Apply roi to nodes, edges ...
[newnodes newedges] = mmaskpositions(n, e, roi);
e = e(newedges, :);

% ... and nodecellmap. For each cell ...
cell_ids = unique(cnm(:, 1));
new_cnm = [];
for ii = 1:numel(cell_ids)
	% If all the nodes in the cell are not in the mask, exclude the cell.
	cell_indeces = find(cnm(:, 1) == cell_ids(ii));
	if nnz(intersect(cnm(cell_indeces, 2), newnodes)) == numel(cell_indeces)
		ind = size(new_cnm, 1) + 1;
		new_cnm(ind:(ind+numel(cell_indeces)-1), 1:2) = cnm(cell_indeces, 1:2);
	end
end

cnm = new_cnm;

% Now refine rosette search by finding pseudo high-order nodes formed by other, 
% very-close nodes.
% Distance between neighboring nodes.
%dt = nan.*zeros(size(n, 1));
%for ii = 1:size(e, 1)
%	dt(e(ii, 1), e(ii, 2)) = norm(n(e(ii, 1), :)-n(e(ii, 2), :), 2);
%end

% Find very close nodes, i.e., super-short edges.
%node_distance_indeces = find((dt < minnodedist)); %nan < k == 0, nan > k == 0
%tiny_edges = [];
%[tiny_edges(:, 1) tiny_edges(:, 2)] = ind2sub(size(dt), node_distance_indeces);
%tiny_edges = sort(tiny_edges, 2);

% Remove very short edges.
%for ii = 1:size(tiny_edges, 1)
	% Add an intermediate node.
%	n(end+1, :) = (n(tiny_edges(ii, 1), :)+n(tiny_edges(ii, 2), :)) ./ 2.;
	
	% Connect all nodes connected to either one of the deleted nodes to the new node.
%	e(find((e == tiny_edges(ii, 1)) | (e == tiny_edges(ii, 2)))) = size(n, 1);
	
	% Substitute the deleted nodes in the cell map.
%	cnm(find((cnm(:, 2) == tiny_edges(ii, 1)) | (cnm(:, 2) == tiny_edges(ii, 2))), 2) = size(n, 1);
%end

%e = sort(e, 2);

% Delete edges where both nodes are the same.
%ind = find(e(:, 1) ~= e(:, 2));
%e = e(ind, :);

% Delete nodes from the cell-node map when they 
% appear more than once for any given cell.
%cell_ids = unique(cnm(:, 1));
%new_cnm = [];
%for ii = 1:numel(cell_ids)	
%	cellnode_indeces = find(cnm(:, 1) == cell_ids(ii));
%	cell_nodes_withrep = cnm(cellnode_indeces, 2);
%	[cell_nodes] = unique(cell_nodes_withrep);
%	thecell = ones(numel(cell_nodes), 2);
%	thecell(:, 1) = cell_ids(ii);
%	thecell(:, 2) = cell_nodes;	
%	ind = size(new_cnm, 1) + 1;
%	new_cnm(ind:(ind+numel(cell_nodes)-1), 1:2) = thecell;
%end
%cnm = new_cnm;

g.nodes = n;
g.edges = e;
g.nodecellmap = cnm;

g = consolidategeometry2D(g, minnodedist);
n = g.nodes;
e = g.edges;
cnm = g.nodecellmap;

% Find rosettes as high-order vertices.
ln = sort(reshape(e, [1, numel(e)]));
[ens nrepeats] = mmrepeat(ln);

rosette_node_indeces = ens(find((nrepeats >= minrosettecells) & (nrepeats <= maxrosettecells)));

rosette_celllist = cell(0, 1);

for ii = 1:numel(rosette_node_indeces)
	rosette_celllist{ii} = cnm(find(cnm(:, 2)==rosette_node_indeces(ii)), 1);
end

% Remove border rosettes: the cell ids will be messed up.
nonborder_rosettes = [];
for ii = 1:numel(rosette_celllist)
	if ((numel(rosette_celllist{ii}) >= minrosettecells) && (numel(rosette_celllist{ii}) <= maxrosettecells))
		nonborder_rosettes(end+1) = ii;
	end
end

rosette_celllist = rosette_celllist(nonborder_rosettes);
rosette_node_indeces = rosette_node_indeces(nonborder_rosettes);
g.nodes = n;
g.edges = e;
g.nodecellmap = cnm;
