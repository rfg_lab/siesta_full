% *********************************************************************************
% tpo and tpf start from 1.
% Generate a new fiducial at the centroid of each polyline.
function [fidu2 nfidu2] = cb_polygoncentroids(fig, tpo, tpf)

    ud = get(fig, 'UserData');
    ud_old = ud;

    [ud.rfiducials(:, :, tpo) ud.rnfiducials(tpo)] = cb_consolidatefiducials(ud.rfiducials(:, :, tpo));

    fidu = ud.rfiducials;
    nfidu = ud.rnfiducials;
    
    % For every time point.
    for ii = tpo:tpf
        pnts1 = [];
        % For every polygon.
        for jj = 1:numel(ud.rpolygons(:, :, ii))
            thepoly = ud.rpolygons{jj, 1, ii};
            if ~ isempty(thepoly) % This takes care of the
                                  % presence of empty
                                  % polygons in the list.
                [dummy pnts1(1) pnts1(2)] = polycenter(thepoly(:, 1), thepoly(:, 2));
                pnts1(3) = ii-1; % Third coordinate: SIESTA stores Z coordinates starting at 0.
                [fidu nfidu] = cb_storecurrentpoint(pnts1, fidu, nfidu);
            end
        end
    end
    
    fidu2 = fidu;
    nfidu2 = nfidu;
end


