% *********************************************************************************
function cb_registerframes(fig)

% Get image info.
ud = get(fig, 'UserData');
img = findobj(get(get(fig, 'CurrentAxes'), 'Children'), 'Type', 'image');

% Remove jitter from image.
[out d] = remove_jitter(ud.slices, ud.rfiducials, ud.rnfiducials, ud.curslice+1);
ind = 1;
	
% Correct markings.
for ii = 1:ud.imsize(3)
	% Only in corrected frames.
	if ud.rnfiducials(ii) > 0
		% Correct fiducials.
		ud.rfiducials(:, 1:2, ii) = ud.rfiducials(:, 1:2, ii) + repmat(d(ind, 1:2), [size(ud.rfiducials, 1) 1]);
		
		% Correct unique markers.
		if ud.runiquecoords(ii, 1) > -1
			ud.runiquecoords(ii, 1:2) = ud.runiquecoords(ii, 1:2) + d(ind, 1:2);
        end
        
        % Circles and polylines.
        for pp = 1:size(ud.rpolygons, 1)
            if (~ isempty(ud.rpolygons{pp, 1, ii}))
                ud.rpolygons{pp, 1, ii}(:, 1:2) = ud.rpolygons{pp, 1, ii}(:, 1:2) + repmat(d(ind, 1:2), [size(ud.rpolygons{pp, 1, ii}(:, 1:2), 1) 1]);
            end
        end
        
        % Correct other types of marking here.
        % Open polylines.
        for pp = 1:size(ud.rtrajectories, 1)
            if (~ isempty(ud.rtrajectories{pp, 1, ii}))
                ud.rtrajectories{pp, 1, ii}(:, 1:2) = ud.rtrajectories{pp, 1, ii}(:, 1:2) + repmat(d(ind, 1:2), [size(ud.rtrajectories{pp, 1, ii}(:, 1:2), 1) 1]);
            end
        end
        
        % Increase the counter of corrected frames visited.
        ind = ind + 1;
    end
end



% New Image.
ud.slices = out;

% Change image object size.
%set(img, 'XData', [0 (ud.imsize(1)-1)], 'YData', [0 (ud.imsize(2)-1)]);
	
% Grey-scale images.
if (~ isfield(ud, 'colordata')) | isempty(ud.colordata)
	ud.imagedata = squeeze(ud.slices(:, :, ud.curslice));
	display_data(fig, img, ud);
	
% Color images
else
	ud.colordata = ud.slices(:, :, ud.curslice);
	ud.imagedata = cat(3, ud.colordata{1}, ud.colordata{2}, ud.colordata{3});
	ud.colspace = 'RGB';
	ud.rcurrchannel = 0;
	ud.rchannels = [];
	display_data(fig, img, ud);
end

% Change figure size.
diptruesize(fig, 100 * ud.zoom);		


end