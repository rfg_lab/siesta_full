% lns2 = cb_storecurrentlinepol(fig, lns)
function lns2 = cb_storecurrentlinepol(fig, lns)
%if isfield(udata,'lineh')
%	lns2 = cat(2, lns, udata.lineh);
%	udata.lineh = [];
%end

% Store the second index under current axes. The first one corresponds to the line being drawn
% immediately after clicking the mouse, while te second one is the line that was just drawn.
ax = get(fig, 'CurrentAxes');
ch = get(ax, 'Children');
if (strcmp(get(ch(2), 'Type'),'line'))
    lns2 = cat(2, lns, ch(2));
end

%set(fig,'UserData',[]);	% Solve MATLAB bug! (Rodrigo's comment)
%set(fig,'UserData',udata);
end