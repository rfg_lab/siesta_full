% values = gaussianfn(params, x)
% Evaluate the Gaussian defined by params on vector x.
%
% PARAMS is a vector with three elements, the average of the Gaussian (first),
% its standard deviation (second) and a scale factor (third).
% X is a vector containing the coordinate values where the Gaussian will be
% evaluated.
%
% VALUES is a vector with the results of evaluating the Gaussian.
%
% Rodrigo Fernandez-Gonzalez
% rfgonzalez@lbl.gov
% 5/14/2009


function values = gaussianfn2(params, x)

if (nargin ~= 2)
    error('Wrong number of parameters.');
    return;
end

if (isempty(params) | isempty(x))
    error('Empty parameter.');
    return;
end

% Retrieve parameters.
mu = params(1);
sigma = params(2);
scale = params(3);

% Evaluate the Gaussian.
values = scale .* exp((- (x - mu).^2) ./ (2 .* sigma.^2));

return;
