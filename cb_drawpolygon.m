% *********************************************************************************


function cb_drawpolygon(fig, polygon, d, color)

ax = get(fig, 'CurrentAxes');
ud = get(fig, 'UserData');
hold(ax, 'on');

h = plot(ax, polygon(:, 1), polygon(:, 2), 'Color', color, 'MarkerSize', d, 'Tag', 'Polyline', 'LineWidth', ud.rBRUSH_SZ);
set(fig, 'CurrentObject', h);
end

