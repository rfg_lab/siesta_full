% out = trajectories2mask(tr, image_size, brush_sz, solid_flag)
%
% brush_sz is used to draw the trajectory and then take either the outline or the interior (or both, see below).
% solid_flag determines if the mask is only the outline of the trajectory (0), the interior (1, for closed trajectories) or both (2, for closed trajectories).

function out = trajectories2mask(tr, sz, brush_sz, solid_flag)

out = [];

if nargin < 2
	return;
end

if nargin < 3
        brush_sz = 3;
end

if nargin < 4
        solid_flag = 0;
end        

out = dip_image(zeros(sz(2), sz(1)), 'bin');

% Calculate polygon outline.
for ii = 1:numel(tr)
        atr = tr{ii};
        for jj = 1:(size(atr, 1)-1)
                % length
                [x y] = pixinline_bresenham(atr(jj:(jj+1), 1:2), brush_sz);
                out(sub2ind([sz(2), sz(1)], y, x)) = 1;
        end

end

switch solid_flag
        case 0                                
                out = out;
        case 1
                out = fillholes(out) - out;
%                  [X Y] = meshgrid(0:(sz(1)-1), 0:(sz(2)-1));  
%                  for ii = 1:numel(tr)
%                          atr = tr{ii};
%                          if ~ isempty(atr)
%  				[in on] = inpolygon(X, Y, atr(:, 1), atr(:, 2));
%                                  in = find(in & (~on)); % Take only points inside the trajectory, not on the border.
%                                  out(in) = 1;
%                          end
%                  end
        case 2
                out = fillholes(out);
%                  [X Y] = meshgrid(0:(sz(1)-1), 0:(sz(2)-1));  
%                  for ii = 1:numel(tr)
%                          atr = tr{ii};
%                          if ~ isempty(atr)
%                                  [in on] = inpolygon(X, Y, atr(:, 1), atr(:, 2));
%                                  in = find(in | on); % Take points inside the trajectory or on the border.
%                                  out(in) = 1;
%                          end
%                  end
end                
