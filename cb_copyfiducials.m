% *********************************************************************************
% tpo, tpf and tpcurr start from 1.
function ud = cb_copyfiducials(fig, tpo, tpf, tpcurr)

ud = get(fig, 'UserData');
ud_old = ud;

disp(cat(2, 'Time point ', cat(2, num2str(tpo-1), cat(2, ' - ', cat(2, num2str(ud.rnfiducials(tpo)), ' fiducials')))));

[ud_old.rfiducials(:, :, tpcurr) ud_old.rnfiducials(tpcurr)] = cb_consolidatefiducials(ud_old.rfiducials(:, :, tpcurr));

ud.rfiducials(:, :, tpcurr) = ud_old.rfiducials(:, :, tpcurr);
ud.rnfiducials(tpcurr) = ud_old.rnfiducials(tpcurr);

for ii = tpo:tpf
        ud.rfiducials(:, :, ii) = ud_old.rfiducials(:, :, tpcurr);
        ud.rnfiducials(ii) = ud_old.rnfiducials(tpcurr);
        
        disp(cat(2, 'Time point ', cat(2, num2str(ii-1), cat(2, ' - ', cat(2, num2str(ud.rnfiducials(ii)), ' fiducials')))));
        
end
end