% ud = cb_projectimage(fig, range, ptype)
% This function called with no arguments returns the different types of projection available.
function ud = cb_projectimage(fig, range, ptype)

if nargin == 0
    ud = {'max'; 'sum'; 'prod'};
    return;
end

ax_children = get(get(fig, 'CurrentAxes'), 'Children');
img = ax_children(end);
ud = get(fig, 'UserData');

range = sort(range);

im = ud.slices;

if ndims(im{1}) == 3
    % Determine slices to project.
    if isempty(range)
        range = [0 size(im{1}, 3)-1];
    else
        if range(1) < 0
            range(1) = 0;
        end
        
        if range(2) >= size(im{1}, 3)
            range(2) = size(im{1}, 3) - 1;
        end
    end
    
    % Project grayscale.
    if ~ isa(im, 'dip_image_array')
        switch ptype
            case 'max'
                thenewim = max(im(:, :, range(1):range(2)), [], 3);
            case 'sum'
                thenewim = stretch(sum(im(:, :, range(1):range(2)), [], 3));
            case 'prod'
                thenewim = stretch(prod(im(:, :, range(1):range(2)), [], 3));
            otherwise
                thenewim = max(im(:, :, range(1):range(2)), [], 3);
        end
        
        thenewim = cat(3, thenewim, thenewim);
        
        % Project color.
    else
        switch ptype
            case 'sum'
                for ii = 1:3
                    thenewim{ii} = squeeze(stretch(sum(im{ii}(:, :, range(1):range(2)), [], 3)));
                end
            case 'max'
                for ii = 1:3
                    thenewim{ii} = squeeze(max(im{ii}(:, :, range(1):range(2)), [], 3));
                end
            case 'prod'
                for ii = 1:3
                    thenewim{ii} = squeeze(stretch(prod(im{ii}(:, :, range(1):range(2)), [], 3)));
                end
            otherwise
                for ii = 1:3
                    thenewim{ii} = squeeze(max(im{ii}(:, :, range(1):range(2)), [], 3));
                end
        end
        
        thenewim{1} = cat(3, thenewim{1}, thenewim{1});
        thenewim{2} = cat(3, thenewim{2}, thenewim{2});
        thenewim{3} = cat(3, thenewim{3}, thenewim{3});
        thenewim = joinchannels('RGB', thenewim{1}, thenewim{2}, thenewim{3});
    end
end

ud.slices = thenewim;
ud.imsize = size(ud.slices{1});
ud.curslice = 0;
%curr_dir = ud.rcurrdir;
%thezoom = ud.zoom;
%ud = init_userdata(ud);
%ud.rcurrdir = curr_dir;
%ud.mappingmode = 'lin';
%ud.colspace = '';
%ud.colordata = '';
%ud.zoom = thezoom;

% Change image object size.
set(img, 'XData', [0 (ud.imsize(1)-1)], 'YData', [0 (ud.imsize(2)-1)]);

% Grey-scale images.
if (~ isfield(ud, 'colordata')) | isempty(ud.colordata)
    ud.imagedata = ud.slices(:, :, 0);
    ud.maxslice = ud.imsize(3) - 1;
    
    if ~ isempty(ud.rchannels)
        ud.rchannels = [];
    end
    
    %figure; dipshow(ud.imagedata);
    display_data(fig, img, ud);
    
    % Color images
else
    ud.maxslice = ud.imsize(3) - 1;
    ud.colordata = ud.slices(:, :, 0);
    ud.imagedata = cat(3, ud.slices{1}(:, :, 0), ud.slices{2}(:, :, 0), ud.slices{3}(:, :, 0));
    ud.colspace = 'RGB';
    ud.rcurrchannel = 0;
    ud.rchannels = ud.slices;
    display_data(fig, img, ud);
end
%writeim(ud.slices, 'tmp1', 'ICS', 'no'); %RODRIGO
% Change figure size.
diptruesize(fig, 100 * ud.zoom);

end