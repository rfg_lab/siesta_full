% function ud = cb_selectalignededges(fig, index, ang, min_chainlength, min_edgelength, save_pics)
%
% ********************************************************************
function ud = cb_selectalignededges(fig, index, ang, min_chainlength, min_edgelength, save_pics)

% Find user data.
%ud = cb_selectedges(fig, index);
ud = get(fig, 'UserData');
if numel(ud.geometry) == 1
	z = 1;
else 
	z = index;
end

% Load roi and measure alignment.
roi = load_roi(index+ud.imshift);
if isempty(roi)
        roi = [0 0; ud.imsize(1)-1 ud.imsize(2)-1];
end

% dipimage names images 'slice' by default. But if this image gets a name (if the mat and the image file had the same name), then rfig2D assigns the file name to ud.imname. In that case, if we want to save a picture of the alignment, we do it as alignment_<ud.imname>.jpeg
if save_pics && (~strcmp(ud.imname, 'slice'))
        save_pics = cat(2, 'alignment_', ud.imname);
end
[cl chains] = alignment(ud.geometry, index, ang * pi / 180., save_pics, roi, ud.imshift, min_edgelength);

% Find the chains at least as long as indicated by the minimum chain length,
% and select the member interfaces.
ind = find(cl >= min_chainlength);
ids = [];
for ii = 1:numel(ind)
	ids = cat(2, ids, chains{ind(ii)});
end

ids = unique(ids);

% This ids index in the list of interfaces within the roi.
% We now need to convert those indeces to point to the list of all interfaces.
[tmp roiedges] = mmaskpositions(ud.geometry(index).nodes, ud.geometry(index).edges, roi);

ids = roiedges(ids);


ud.rselectededges{z} = ids;

