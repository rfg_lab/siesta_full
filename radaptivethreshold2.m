function bw=radaptivethreshold2(IM,ws,C,tm)
%tic
%ADAPTIVETHRESHOLD An adaptive thresholding algorithm that seperates the
%foreground from the background with nonuniform illumination.
%  bw=adaptivethreshold(IM,ws,C) outputs a binary image bw with the local 
%   threshold mean-C or median-C to the image IM.
%  ws is the local window size.
%  tm is 0 or 1, a switch between mean and median. tm=0 mean(default); tm=1 median.
%
% ORIGINAL:
%  Contributed by Guanglei Xiong (xgl99@mails.tsinghua.edu.cn)
%  at Tsinghua University, Beijing, China.
%
% DIPIMAGE VERSION:
%  Rodrigo Fernandez-Gonzalez
%  fernanr1@mskcc.org
%  2007/08/15
%
%
%  For more information, please see
%  http://homepages.inf.ed.ac.uk/rbf/HIPR2/adpthrsh.htm

if (nargin<3)
    error('You must provide the image IM, the window size ws, and C.');
elseif (nargin==3)
    tm=0;
elseif (tm~=0 && tm~=1)
    error('tm must be 0 or 1.');
end

if tm==0
    mIM=unif(IM,ws,'rectangular');
else
    mIM=medif(IM, ws, 'rectangular');
end
sIM=mIM-IM-C;
zPlanes = size(sIM, 3);
if (zPlanes > 1)
	bw = ~(sIM >= 0);
	bw = medif(dip_image(double(bw), 'double'), 3, 'rectangular');
	
else
	bw = ~(sIM >= 0);
end
%toc
