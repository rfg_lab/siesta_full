% dt = mfloyd(positions, neigh_graph, weighted)
%
% Implements Floyd's algorithm for the shortest distance
% between all possible pairs of positions without recovering
% the actual path.
% For a short and nice pseudocode see:
% http://www-unix.mcs.anl.gov/dbpp/text/node35.html
%
% POSITIONS is a matrix where each row represents the coordinates of a point.
% NEIGH_GRAPH is a matrix where (i, j) = 1 indicates that positions i and j
% are immediate neighbors.
% WEIGHTED determines whether to use number of edges (0) or edge 
% lengths to measure distances.
% DT is a matrix where element (i, j) contains the minimum distance between
% positions i and j in neigh_graph.
%
% Rodrigo Fernandez-Gonzalez
% fernanr1@mskcc.org
% 11/06/2006
