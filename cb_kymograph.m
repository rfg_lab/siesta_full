% *********************************************************************************
function kymogr = cb_kymograph(fig)

kymin = [];

ud = get(fig, 'UserData');
obj = get(fig, 'CurrentObject');

if strcmp(get(obj, 'Tag'), 'Polyline')
    x = unique(get(obj, 'XData'));
    y = unique(get(obj, 'YData'));
else
    return;
end

extensions_images = {'*.tif'; '*.ics'; '*.*'};

curr_dir = pwd;
cd(ud.rcurrdir);

[filename, pathname, ext] = uiputfile(extensions_images, 'Save time series');
cd(curr_dir);
if (~ isequal(filename, 0))
    % Find slices with annotations and therefore aligned.
    marked = find(ud.rnfiducials > 0);
    %first = marked(1) - 1;
    %last = marked(end) - 1;
    marked = marked - 1;
    
    % If you used here first:last instead of marked, you would take all slices independently of whether they were annotated or not.
    if ~isempty(marked)
        kymim = ud.slices(x(1):x(2), y(1):y(2), marked);
    else
        kymim = ud.slices(x(1):x(2), y(1):y(2), 0:(ud.imsize(3)-1));
    end
    
    % Grey scale images.
    if ~ isa(kymim, 'dip_image_array')
        kymogr = [];
        
        for ii = 1:size(kymim, 3)
            kymogr = cat(2, double(kymogr), double(kymim(:, :, ii - 1)));
        end
        
        kymogr = dip_image(kymogr);
        % Color images.
    else
        kymogr = cell(1, 3);
        
        for ii = 1:size(kymim{1}, 3)
            kymogr{1} = cat(2, double(kymogr{1}), double(kymim{1}(:, :, ii - 1)));
        end
        for ii = 1:size(kymim{2}, 3)
            kymogr{2} = cat(2, double(kymogr{2}), double(kymim{2}(:, :, ii - 1)));
        end
        for ii = 1:size(kymim{3}, 3)
            kymogr{3} = cat(2, double(kymogr{3}), double(kymim{3}(:, :, ii - 1)));
        end
        
        kymogr = joinchannels('RGB', kymogr{1}, kymogr{2}, kymogr{3});
    end
    
    dipshow(kymogr);
    
    if (~ isequal(filename, 0))
        switch ext
            case 1
                format = 'TIFF';
            case 2
                format = 'ICSv2';
            otherwise
                format = 'TIFF';
        end
        
        % Save original data.
        writeim(uint16(kymogr), cat(2, pathname, filename), format, 'no');
        % Save kymograph to visualize.
        [tmp name] = fileparts(filename);
        writeim(uint8(stretch(kymogr)), cat(2, pathname, cat(2, name, '_vis')), format, 'no');
        cancel = 0;
    else
        cancel = 1;
    end
end
end