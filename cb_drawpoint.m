% *********************************************************************************

function cb_drawpoint(fig, coords, d, color, thetag)

ax = get(fig, 'CurrentAxes');
hold(ax, 'on');
plot(ax, coords(1), coords(2), color, 'MarkerSize', d, 'Tag', thetag);
%t = text(coords(1), coords(2), num2str(d)); 
%set(t, 'Color', color(1), 'Tag', thetag);
end