% *********************************************************************************

function poly = cb_selectcell(fig, cell_id)

r = 3;
c = [0 0];
poly = [];

if (cell_id > 0)
 	% if strncmp(get(fig,'Tag'),'DIP_Image',9)
	      img = findobj(get(get(fig,'CurrentAxes'), 'Children'), 'Type', 'image');
	      if strcmp(get(img,'Type'),'image')
              udata = get(fig,'UserData');
              m = measure(squeeze(udata.slices(:, :, udata.curslice)), [], {'Center'}, cell_id, 2);
              c(1) = m.Center(1, :)';
              c(2) = m.Center(2, :)';
              
              poly = [c(1) + r c(2); ...
                  c(1)     c(2) - r; ...
                  c(1) - r c(2); ...
                  c(1)     c(2) + r;
                  c(1) + r c(2)];
          end
	% end
end
end