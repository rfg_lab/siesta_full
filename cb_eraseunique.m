% *********************************************************************************
function cb_eraseunique(fig)

ax = get(fig, 'CurrentAxes');
children = get(ax, 'Children');

for i = 1:numel(children)
	obj = children(i);
	type = get(obj, 'Tag');

	if strcmp(type, 'Unique')
		delete(obj);
		break;
	end
end
end