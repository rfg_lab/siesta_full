% *********************************************************************************

function [selfidu2] = cb_selectcurrentpoint(fig, fidu, selfidu)

obj = get(fig, 'CurrentObject');
ud = get(fig, 'UserData');
selfidu2 = selfidu;
type = get(obj, 'Tag');

if strcmp(type, 'Fiducial')
	% Find the selected point.
	curslice = ud.curslice + 1;
	ind = find((fidu(:, 1, curslice) == get(obj, 'XData')) & (fidu(:, 2, curslice) == get(obj, 'YData')));
	if selfidu2(1) == 0
		selfidu2 = [ind(1), 0, curslice];
	elseif (selfidu2(2) == 0) && (curslice == selfidu2(3))
		selfidu2(2) = ind(1);
	elseif selfidu2(2) ~= 0
		selfidu2 = [ind(1) 0 curslice];
	elseif curslice ~= selfidu2(3)
		selfidu2 = [ind(1) 0 curslice];
    end
    
    set(obj, 'Color', 'c');
end
end