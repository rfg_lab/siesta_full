% *********************************************************************************
function [fidu2 nfidu2] = cb_deletepoint(fig, fidu, nfidu, coords)

obj = get(fig, 'CurrentObject');

if ~ isempty(obj)
        type = get(obj, 'Tag');

        if strcmp(type, 'Fiducial')
	        ud = get(fig, 'UserData');
	        curslice = ud.curslice + 1;
	        nfidu(curslice) = nfidu(curslice) - 1;
	        %ind = intersect(find(fidu(:, 1, curslice) == get(obj, 'XData')), ...
	        %		find(fidu(:, 2, curslice) == get(obj, 'YData')));
	        ind = find((fidu(:, 1, curslice) == get(obj, 'XData')) & (fidu(:, 2, curslice) == get(obj, 'YData')));
        
	        fidu(ind(end), :, curslice) = [-1 -1 -1];
	        
	        delete(obj);
        end
else % If this function is called with the right button clicked, delete the nearby objects. However, this deletion won't be visible until the annotations are repainted.
                if ~exist('coords', 'var')
                    [tmp1 coords tmp2] = cb_getcoords(fig);
                end
                ud = get(fig, 'UserData');
                curslice = ud.curslice + 1;
                nfidu(curslice) = nfidu(curslice) - 1;
                %ind = intersect(find(fidu(:, 1, curslice) == get(obj, 'XData')), ...
                %               find(fidu(:, 2, curslice) == get(obj, 'YData')));
                ind = find(abs(fidu(:, 1, curslice) - coords(1)) < 2 & abs(fidu(:, 2, curslice) - coords(2)) < 2);
        
                if ~ isempty(ind)
                        fidu(ind(end), :, curslice) = [-1 -1 -1];
                end
                
                %delete(obj);

end
fidu2 = fidu;
nfidu2 = nfidu;
end