% [tr ntr] = cb_cleardots(fig)
function [tr ntr] = cb_cleardots(fig)
udata = get(fig, 'UserData');

if isfield(udata,'ax')
    set(udata.ax,'Units',udata.oldAxesUnits);
    udata = rmfield(udata,{'ax','oldAxesUnits','oldNumberTitle'});
end

for ii = 1:numel(udata.rntrajectories)
    thetrs = {udata.rtrajectories{:, :, ii}}';
    keep_trs = [];
    
    if udata.rntrajectories(ii) == 0
        continue;
    end
    
    for jj = 1:numel(thetrs)
        atr = thetrs{jj};
        if (~ isempty(atr)) & (size(atr, 1) > 1) & (~(((atr(1,1) == atr(2, 1))) & ((atr(1,2) == atr(2, 2)))))
            keep_trs = [keep_trs jj];
        end
    end
    
    thetrs = {thetrs{keep_trs}}';
    tr(:, :, ii) = thetrs;
    ntr(ii) = numel(keep_trs);
end
end