%  L = myholedrillingfunction1(beta, t)
%
%  nu = .5; % Poisson's ratio.
%  R_0 = 1; % Diameter of the hole (in microns).
%  
%  r = t(:, 1); % Distance from hole to point in microns.
%  theta = t(:, 2); % Angle in radians.
%  
%  E = 1; % Young's modulus. Constant for now (in other words, u_rtheta is the displacement times E).
%  sigma_x = beta(1);
%  sigma_y = beta(2);
%  
%  u_rtheta = (1+nu) .* ((power(R_0, 2) .* (sigma_x + sigma_y) ./ r) + ((4 .* power(R_0, 2) ./ ((1 + nu) .* r) - power(R_0, 4) ./ power(r, 3))) .* (sigma_x - sigma_y) .* cos(2.*theta)) ./ (2 * E);

function u_rtheta = myholedrillingfunction1(beta, t)

nu = .5; % Poisson's ratio.
R_0 = 1; % Diameter of the hole (in microns).

r = t(:, 1);
theta = t(:, 2);

E = 1; % Young's modulus. Constant for now (in other words, u_rtheta is the displacement times E).
sigma_x = beta(1);
sigma_y = beta(2);

u_rtheta = (1+nu) .* ((power(R_0, 2) .* (sigma_x + sigma_y) ./ r) + ((4 .* power(R_0, 2) ./ ((1 + nu) .* r) - power(R_0, 4) ./ power(r, 3))) .* (sigma_x - sigma_y) .* cos(2.*theta)) ./ (2 * E);