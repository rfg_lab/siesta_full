% Author: Michael Wang
% Date: August 28, 2014
% 
% The following function takes a cell that is about to divide and create 2
% new seeds in the subsequent time point to represent the divided daughter
% cells. The distance between all the coordinates along the cell outline
% and the cell seed coordinate is recorded. Using this distance
% information, the parent cell seed is divded into two daughter cell seeds
% along the line cooresponding to the longest distance.
% 
% INPUT:
% polygon - contains the coordinates of the cell about to divide i.e.
% ud.rpolygons{1,:,138}
% fids - contains seed coordinates for all cells in all time points i.e.
% ud.rfiducials
% t - is the ud time (starting index 1) right before cell division 
% divInd - contains index and time information for dividing cells and their
% daughter cells WITHOUT the current cell division
% seedDist - how far along the distance line should the divided seeds be
% relative to the parent seed
% 
% OUTPUT:
% newFids - contains seed coordinates for all cells including the newly
% created divided seeds
% theNewSeedIndices - contains index and time information for dividing cells and their
% daughter cells INCLUDING the current cell division
% 
% ASSUMPTION:
% 1. polygon is the cell about to divide (i.e. we know which cell will divide)
% 2. each cell only has 1 seed inside its polygon

%split based on longest axis of polygon
function [newFids, theNewSeedIndices]=splitFidsLongAxis(polygon,fids,t,divInd,seedDist)

% check to see which seed belongs in polygon
ind=inpolygon(fids(:,1,t),fids(:,2,t),polygon(:,1),polygon(:,2));

% row indicates which seed is in the polygon about to divide
row=find(ind);

if size(row,1)>1
   plotSeeds(fids(:,:,t),1); hold on; plot(polygon(:,1),polygon(:,2));
    display('!!!!!ERROR!!!!!, more than 1 seed in the polygon');
    newFids=fids;
    theNewSeedIndices=[];
    return
end


% find centroid of polygon
geom=polygeom(polygon(:,1),polygon(:,2));

xCen=geom(2);
yCen=geom(3);

% distance between all polygon coordinates and the seed in the polygon
dist=sqrt(((polygon(:,1)-xCen).^2)+((polygon(:,2)-yCen).^2));

% reference block of comment, does not relate to code
% compares each element of dist to its 10 neighboring values (20 total) to
% determine local max
% for i:size(dist)
%     
%     
% %     if dist(i)>dist(i+1,i+10)&& dist(i)>dist(i-1,i-10)
% %       record local max
% %         max=
% %     end
%     
% end

% find min bound rect
[rectx,recty,~,~]=minboundrect(polygon(:,1),polygon(:,2));

%find longest axis
axisLength=zeros(4,1);
for i=1:4
    axisLength(i)=sqrt(((rectx(i+1)-rectx(i))^2) + ((recty(i+1)-recty(i))^2));
end

ind=find(axisLength==min(axisLength(1:2)));
shortAxis(1)=ind(1);
shortAxis(2)=shortAxis(1)+2;
shortLine1=[rectx(shortAxis(1)),recty(shortAxis(1));rectx(shortAxis(1)+1),recty(shortAxis(1)+1)];
shortLine2=[rectx(shortAxis(2)),recty(shortAxis(2));rectx(shortAxis(2)+1),recty(shortAxis(2)+1)];

point1=[(shortLine1(1,1)+shortLine1(2,1))/2,(shortLine1(1,2)+shortLine1(2,2))/2];
point2=[(shortLine2(1,1)+shortLine2(2,1))/2,(shortLine2(1,2)+shortLine2(2,2))/2];

longLine=[point1;point2];
xCen=(point1(1)+point2(1))/2;
yCen=(point1(2)+point2(2))/2;

% first new fid
newFid1=[(xCen+point1(1))/2,(yCen+point1(2))/2,fids(1,3,t+1)];

% check if new seed is in the polygon (may be unnecessary)
if ~inpolygon(newFid1(1),newFid1(2),polygon(:,1),polygon(:,2));
    
end

% second new fid
newFid2=[(xCen+point2(1))/2,(yCen+point2(2))/2,fids(1,3,t+1)];

% check if new seed is in the polygon (may be unnecessary)
if ~inpolygon(newFid2(1),newFid2(2),polygon(:,1),polygon(:,2));

end

% ind = find(fids(:,1,t) == -1);
% find indices that are negative one
ind = find(fids(:,1,t+1) == -1);

% no negative ones, add to bottom
if isempty(ind)
    
    if ~ismember(newFid2,fids(:,:,t+1),'rows')
        newRow=size(fids, 1)+1;
        fids(newRow,:,:)=-1;
        %     fids(newRow,:,t)=newFid2;
        fids(newRow,:,t+1)=newFid2;
    else
        [~,newRow]=ismember(newFid2,fids(:,:,t+1),'rows');
    end

else
    newRow = ind(1);
    if ~ismember(newFid2,fids(:,:,t+1),'rows')
%     fids(newRow,:,t)=newFid2;
    fids(newRow,:,t+1)=newFid2;
    else
        [~,newRow]=ismember(newFid2,fids(:,:,t+1),'rows');
    end
end

if ~ismember(newFid1,fids(:,:,t+1),'rows')
    % fids(row,:,t)=newFid1;
    fids(row,:,t+1)=newFid1;

end

newFids=fids;

% seedDivAng=atan((mid1y-mid2y)/(mid1x-mid2x)); 

%first row represent parent, second row represent first daughter cell
newDivInd(:,:,1)=[t+1;row;row;newRow;0];
newDivInd(:,:,2)=0;
%newDivInd(:,:,2)=0;
%newDivInd=[t+1;row;newRow;newRow+1];

theNewSeedIndices=[divInd,newDivInd];
%theNewSeedIndices(:,:,2)=-1;


end