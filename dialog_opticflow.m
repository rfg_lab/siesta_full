% [first last window_size] = dialog_opticflow([default_first_tp default_last_tp], [default_window_size_x default_window_size_y])

function varargout = dialog_opticflow(varargin)
% DIALOG_OPTICFLOW M-file for dialog_opticflow.fig
%      H = DIALOG_OPTICFLOW([min max]) returns the handle to a new DIALOG_OPTICFLOW
%	   containing two popupmenus, from min to max.




%      DIALOG_OPTICFLOW, by itself, creates a new DIALOG_OPTICFLOW or raises the existing
%      singleton*.
%
%      H = DIALOG_OPTICFLOW returns the handle to a new DIALOG_OPTICFLOW or the handle to
%      the existing singleton*.
%
%      DIALOG_OPTICFLOW('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DIALOG_OPTICFLOW.M with the given input arguments.
%
%      DIALOG_OPTICFLOW('Property','Value',...) creates a new DIALOG_OPTICFLOW or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before dialog_timepoints_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to dialog_opticflow_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help dialog_opticflow

% Last Modified by GUIDE v2.5 21-May-2015 10:20:11

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @dialog_opticflow_OpeningFcn, ...
                   'gui_OutputFcn',  @dialog_opticflow_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before dialog_opticflow is made visible.
function dialog_opticflow_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to dialog_opticflow (see VARARGIN)

% Choose default command line output for dialog_opticflow
handles.output = hObject;

% Parameter values for components. RODRI EDITED HERE.
% THIS HAS TO BE BEFORE guidata(hObject, handles)!!!!
set(handles.figure1, 'name', 'Propagate seeds');
warning('off', 'all'); % This is necessary to remove a warning
					   % caused by a Matlab bug.

timepoints = sort(varargin{1});
tps = timepoints(1):timepoints(2);
handles.tps = tps;
handles.closemethod = -1;
str = [];

for i = 1:numel(tps)
	str = cat(2, str, cat(2, num2str(tps(i)), '|'));
end

str = str(1:(end-1));
set(handles.popupmenu1, 'String', str, 'Value', 1);
set(handles.popupmenu2, 'String', str, 'Value', numel(tps));
%set(handles.popupmenu1, 'String', str, 'Value', 76);
%set(handles.popupmenu2, 'String', str, 'Value', 76);

% Set X, Y window size.
if nargin >= 2
    set(handles.edit1, 'String', num2str(varargin{2}(1)));
    set(handles.edit2, 'String', num2str(varargin{2}(2)));
else
    set(handles.edit1, 'String', num2str(64));
    set(handles.edit2, 'String', num2str(64));
end

% Update handles structure.
guidata(hObject, handles);


% UIWAIT makes dialog_opticflow wait for user response (see UIRESUME)
uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = dialog_opticflow_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
%varargout{1} = handles.output;
%get(handles.figure1)
%uiwait(handles.figure1);

tp = [-1 -1];
ws_x = -1;
ws_y = -1;

if isfield(handles, 'figure1')
    d = guidata(handles.figure1);
    
    if d.closemethod == 1
        tp(1) = handles.tps(get(handles.popupmenu1, 'Value'));
        tp(2) = handles.tps(get(handles.popupmenu2, 'Value'));
        tp = sort(tp);
        ws_x = floor(str2double(get(handles.edit1, 'String')));
        ws_y = floor(str2double(get(handles.edit2, 'String')));
    end
    close(handles.figure1);
end

varargout{1} = tp(1);
varargout{2} = tp(2);
varargout{3} = [ws_x ws_y];

% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1

selected = get(handles.popupmenu1,'Value');
prev_str = get(handles.popupmenu1, 'String');
set(handles.popupmenu1, 'String', prev_str, 'Value', selected);

% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2
selected = get(handles.popupmenu2,'Value');
prev_str = get(handles.popupmenu2, 'String');
set(handles.popupmenu2, 'String', prev_str, 'Value', selected);


% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%set(handles.figure1, 'Visible', 'off');
handles.closemethod = 1;
guidata(handles.figure1, handles);
uiresume(handles.figure1);



% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.closemethod = 0;
guidata(handles.figure1, handles);
uiresume(handles.figure1);





function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double
if isempty(eventdata) % return pressed or lost focus
    if isnan(str2double(get(hObject,'String'))) % str2double returns nan if there is a problem with the conversion.
        set(hObject, 'String', num2str(64));
    end
end

% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double
if isempty(eventdata) % return pressed or lost focus
    if isnan(str2double(get(hObject,'String'))) % str2double returns nan if there is a problem with the conversion.
        set(hObject, 'String', num2str(64));
    end
end

% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
