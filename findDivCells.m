function [trackDiv]=findDivCells(trackPolygon,fids)
% after cells have been tracked, use this function to record area growth
% and circularity measures

% trackPolygon is already the polygons i.e. trackPolygon
% fids should be ud.rfiducials


% take each segment and overlay previous time point segment

temp=(size(trackPolygon));
% time=temp(3);

% column for time
% rows for segmentation

trackDiv=cell(temp(1)+1,temp(3),3);





% trackDiv={};
column=1;

for t=1:temp(3);
    
    % for non empty time points
    if nnz(not(cellfun('isempty',trackPolygon(:,:,t))))~=0;
        
        
        % (t-1) is SIESTA time, t is array time starting at 1
        trackDiv{1,column,1}=t;
        trackDiv{1,column,2}=t;
        trackDiv{1,column,3}=t;
        
        
        % i for segmentations in CURRENT (t) time frame
        % for each segmentation, compare current segmentation size to t+1
        % segementation size
        for i=1:size(trackPolygon(:,:,t),1);
            if isempty(trackPolygon{i,:,t})==1
                continue;
            end
            
            Areas1=nnz(poly2mask(trackPolygon{i,:,t}(:,1),trackPolygon{i,:,t}(:,2),1000,1000));
%             im_poly1 = trajectories2mask(trackPolygon(i,:,t),[1000 1000], 1, 2);
%             Areas1=nnz(double(im_poly1));
            
            %column2 is area
            trackDiv{i+1,column,2}=Areas1;
            
            Geom=polygeom(trackPolygon{i,:,t}(:,1),trackPolygon{i,:,t}(:,2));
            
            Cir=(Geom(4)*Geom(4))/(4*pi*Geom(1));
            
            %column3 is circularity
            trackDiv{i+1,column,3}=Cir;
            
            
            ind=inpolygon(fids(:,1,t),fids(:,2,t),trackPolygon{i,:,t}(:,1),trackPolygon{i,:,t}(:,2));
            
            row=find(ind);
            
            % NOTE if a cell segment is inside of another segment, then row
            % will return 2 values meaning that there are 2 fiducials
            % inside of one cell outline. This is an error in siesta. refer
            % to test4good(error).jpg for more info
            if size(row)==1
                dist=sqrt(((trackPolygon{i,:,t}(:,1)-fids(row,1,t)).^2)+((trackPolygon{i,:,t}(:,2)-fids(row,2,t)).^2));
            end
              
            %column1 contains distances
            trackDiv{i+1,column,1}=dist;
            
            
        end
        
        column=column+1;
    end
    
end
end