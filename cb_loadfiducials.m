% *********************************************************************************
function udata = cb_loadfiducials(fig) % Changes the current fiducials into the fiducials of the selected file.

udata = get(fig, 'UserData');
curr_dir = pwd;
cd(udata.rcurrdir);

[filename, pathname, ext] = uigetfile('*.mat', 'Load fiducial markers');
cd(curr_dir);
if (~ isequal(filename, 0))
		vars = load(fullfile(pathname, filename));
                
                % If the file does not contain a ud structure, exit.
                if ~ isfield(vars, 'ud')
                        disp('Wrong annotations format.');
                        cd(curr_dir);
                        return;
                end
                
                ud = vars.ud;
                
                if size(udata.rfiducials, 3) == size(ud.rfiducials, 3)
                        udata.rfiducials = ud.rfiducials;
                        udata.rnfiducials = ud.rnfiducials;
                elseif size(udata.rfiducials, 3) > size(ud.rfiducials, 3)
                        udata.rfiducials(1:size(ud.rfiducials, 1), 1:size(ud.rfiducials, 2), 1:size(ud.rfiducials, 3)) = ud.rfiducials;
                        udata.rnfiducials(1:numel(ud.rnfiducials)) = ud.rnfiducials;
                end 
end
cd(curr_dir);
end