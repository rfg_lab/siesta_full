% *********************************************************************************
function cb_paintpolylines(fig, polygons, d, color, erase_old_polygons)

if nargin < 5
	erase_old_polygons = 1;
end

if erase_old_polygons
	cb_erasepolygons(fig);
end

ud = get(fig, 'UserData');
curslice = ud.curslice + 1;

for i = 1 : size(polygons, 1)
        thepoly = polygons{i, 1, curslice};
	
	if ~ isempty(thepoly)
		cb_drawpolygon(fig, thepoly, d, color); %pause(1);
	end
end
end