% [bgcorrected_r bgcorrected_g bgcorrected_b pix_vals_r pix_vals_g pix_vals_b bg_vals length_vals] = cb_quantifytrajectoriestime(fig, channel, brush_sz, tp_list, bgpolyflag, integrate)
% bgpolyflag indicates whether the a polygon has been drawn defining the background region (1) or not (0).
function [bgcorrected_r bgcorrected_g bgcorrected_b pix_vals_r pix_vals_g pix_vals_b bg_vals length_vals] = cb_quantifytrajectoriestime(fig, channel, brush_sz, tp_list, bgpolyflag, integrate)
tp_list
if nargin < 6
    integrate = 0; % Sum (1) or mean (0) intensity along the trajectory?
end

udata = get(fig, 'UserData');

n = max(udata.rntrajectories(:, :, 1));
pix_vals_r = nan .* zeros(n, numel(tp_list)); % One row per trajectory, one column per time point.
pix_vals_g = nan .* zeros(n, numel(tp_list)); % One row per trajectory, one column per time point.
pix_vals_b = nan .* zeros(n, numel(tp_list)); % One row per trajectory, one column per time point.

bgcorrected_r = nan .* zeros(n, numel(tp_list)); % One row per trajectory, one column per time point.
bgcorrected_g = nan .* zeros(n, numel(tp_list)); % One row per trajectory, one column per time point.
bgcorrected_b = nan .* zeros(n, numel(tp_list)); % One row per trajectory, one column per time point.

length_vals = nan .* zeros(n, numel(tp_list)); % One row per trajectory, one column per time point.
bg_vals = nan .* zeros(3, numel(tp_list)); % One value per time point.

if bgpolyflag
    [X Y] = meshgrid(0:(udata.imsize(1)-1), 0:(udata.imsize(2)-1));
end

% Find shortest trajectory.
nl = inf;
for ii = 1:numel(tp_list)
    thetp = tp_list(ii);
    trajectories = udata.rtrajectories(:, :, thetp);
    
    for jj = 1:udata.rntrajectories(thetp)
        tr = trajectories{jj};
        
        [ang l] = cb_multicolormeasuretrajectory(fig, tr, brush_sz, thetp - 1);
        
        if l < nl
            nl = l;
        end
    end
end

% For every time point.
for ii = 1:numel(tp_list)
    thetp = tp_list(ii);
    
    if (~ isfield(udata, 'colordata')) | isempty(udata.colordata)
        im = udata.slices(:, :, thetp-1);
        
        if bgpolyflag
            mask = dip_image(zeros(udata.imsize(2), udata.imsize(1), 1), 'bin8');
            thepoly = udata.rpolygons{1, 1, thetp};
            % If no polygon was drawn at this time point, use the coordinates of the polygon drawn at the first time point.
            if isempty(thepoly)
                thepoly = udata.rpolygons{1, 1, tp_list(1)};
            end
            
            in = find(inpolygon(X, Y, thepoly(:, 1), thepoly(:, 2)));
            mask(in) = 1;
            
            bg_vals(1:3, ii) = mean(im(in));
        else
            mask = findbackgroundauto(im);
            
            % Use the background mask only if it contains at least 20% of the pixels in the image.
            if ((~isempty(mask)) & (nnz(double(mask)) > (0.05 * prod(size(im)))))
                bg_vals(1:3, ii) = mean(im(mask));
            else
                bg_vals(1:3, ii) = 0;
            end
        end
    else
        for ic = 1:3
            im = udata.slices{ic}(:, :, thetp-1);
            
            if bgpolyflag
                mask = dip_image(zeros(udata.imsize(2), udata.imsize(1), 1), 'bin8');
                thepoly = udata.rpolygons{1, 1, thetp};
                % If no polygon was drawn at this time point, use the coordinates of the polygon drawn at the first time point.
                if isempty(thepoly)
                    thepoly = udata.rpolygons{1, 1, tp_list(1)};
                end
                
                in = find(inpolygon(X, Y, thepoly(:, 1), thepoly(:, 2)));
                mask(in) = 1;
                bg_vals(ic, ii) = mean(im(in));
            else
                mask = findbackgroundauto(im);
                % Use the background mask only if it contains at least 10% of the pixels in the image.
                if ((~isempty(mask)) & (nnz(double(mask)) > (0.1 * prod(size(im)))))
                    bg_vals(ic, ii) = mean(im(mask));
                else
                    bg_vals(ic, ii) = 0;
                end
            end
        end
    end
    
    trajectories = udata.rtrajectories(:, :, thetp);
    
    % For every trajectory in this time point.
    for jj = 1:udata.rntrajectories(thetp)
        tr = trajectories{jj};
        
        if ~ isempty(tr)
            % Measure intensity.
            if ~ integrate
                [ang l theintensity] = cb_multicolormeasurecentertrajectory(fig, tr, brush_sz, thetp-1, nl, @mean);
                
                pix_vals_r(jj, ii) = theintensity(1);
                bgcorrected_r(jj, ii) = pix_vals_r(jj, ii) - bg_vals(1, ii);
                pix_vals_g(jj, ii) = theintensity(2);
                bgcorrected_g(jj, ii) = pix_vals_g(jj, ii) - bg_vals(2, ii);
                pix_vals_b(jj, ii) = theintensity(3);
                bgcorrected_b(jj, ii) = pix_vals_b(jj, ii) - bg_vals(3, ii);
                length_vals(jj, ii) = l;
            else
                [ang l theintensity npnts] = cb_multicolormeasuretrajectory(fig, tr, brush_sz, thetp-1, @sum); % Use entire edge length.
                
                pix_vals_r(jj, ii) = theintensity(1);
                bgcorrected_r(jj, ii) = pix_vals_r(jj, ii) - npnts .* bg_vals(1, ii);
                pix_vals_g(jj, ii) = theintensity(2);
                bgcorrected_g(jj, ii) = pix_vals_g(jj, ii) - npnts .* bg_vals(2, ii);
                pix_vals_b(jj, ii) = theintensity(3);
                bgcorrected_b(jj, ii) = pix_vals_b(jj, ii) - npnts .* bg_vals(3, ii);
                length_vals(jj, ii) = l;
            end
            
        end
    end
end
end