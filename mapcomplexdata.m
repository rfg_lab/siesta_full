%
% Map complex data to the real domain
%
function cdata = mapcomplexdata(cdata,complexmapping)
if ~isreal(cdata)
   switch complexmapping
      case 'real'
         cdata = real(cdata);
      case 'imag'
         cdata = imag(cdata);
      case 'phase'
         cdata = phase(cdata);
      otherwise
         cdata = abs(cdata);
   end
end
end
