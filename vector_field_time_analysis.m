% cut_t is relative to size(points, 3). It is the index of the time point immediately before ablation. The first possible index is 1.
function [dr_um_t dth_um_t cut_p1_um_t cut_p1_rot_angle_deg_t dr_um_cum dth_um_cum cut_p1_um_cum cut_p1_rot_angle_deg_cum] = vector_field_time_analysis(points, vertex_1, vertex_2, cut, vis, save_flag, cut_t)

% Analysis comparing each time point to the previous.
dr_um_t = nan .* zeros(size(points, 3), size(points, 1)); % One row per time point, one column per node.
dr_th_t = nan .* zeros(size(points, 3), size(points, 1));
cut_p1_um_t = nan .* zeros(size(points, 3), size(points, 1));
cut_p1_rot_angle_deg_t = nan .* zeros(size(points, 3), size(points, 1));

% Analysis comparing time points to the time point before the cut.
dr_um_cum = nan .* zeros(size(points, 3), size(points, 1)); % One row per time point, one column per node.
dr_th_cum = nan .* zeros(size(points, 3), size(points, 1));
cut_p1_um_cum = nan .* zeros(size(points, 3), size(points, 1));
cut_p1_rot_angle_deg_cum = nan .* zeros(size(points, 3), size(points, 1));


% Since dr and dth are computed as differences between two time points, for n time points
% we will only have n-1 dr values. Thus, we set dr to NaN at the first time point.
for ii = 1:(size(points, 3))
	
	if ii > 1
		[dr_um_t(ii, :) dth_um_t(ii, :) cut_p1_um_t(ii, :) cut_p1_rot_angle_deg_t(ii, :)] = vector_field_analysis(points(:, :, ii-1), points(:, :, ii), vertex_1(1, :, ii-1), vertex_2(1, :, ii-1), cut(ii-1, :), 0, 0);
	end
	
	% For the cumulative displacement, we do have a value per time point.
	[dr_um_cum(ii, :) dth_um_cum(ii, :) cut_p1_um_cum(ii, :) cut_p1_rot_angle_deg_cum(ii, :)] = vector_field_analysis(points(:, :, cut_t), points(:, :, ii), vertex_1(1, :, cut_t), vertex_2(1, :, cut_t), cut(cut_t, :), 0, 0);
end

if vis
	vector_field_time_plots(dr_um_t, dth_um_t, cut_p1_um_t, cut_p1_rot_angle_deg_t, '', save_flag, cut_t);
	vector_field_time_plots(dr_um_cum, dth_um_cum, cut_p1_um_cum, cut_p1_rot_angle_deg_cum, 'cumulative_dr', save_flag, cut_t);
end