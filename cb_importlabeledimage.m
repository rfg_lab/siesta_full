% *********************************************************************************
% Extracts polygons from a labeled image.

function ud2 = cb_importlabeledimage(fig)

subs_factor_xy = 1;

ud2 = get(fig, 'UserData');
curr_dir = pwd;
cd(ud2.rcurrdir);
curr_tp = ud2.curslice;

[filename, pathname, ext] = uigetfile({'*.tif'; '*.ics'}, 'Import annotations from labeled image');
cd(curr_dir);
if (~ isequal(filename, 0))
        try
            im = readtimeseries(cat(2, pathname, filename), '', [0 0], 'no', 'no');
        % For a multi-plane file ...
        catch
            switch ext
                case 1 % Read multipage tiff
                    im = tiffread(cat(2, pathname, filename));
                case 2 % Read multipage ics.
                    im = readim(cat(2, pathname, filename));
                otherwise
                    fprintf('\nError importing labeled image %s%s.', pathname, filename);
                    cd(curr_dir);
                    return;
            end

        end

        ids = nonzeros(unique(double(im)));

        % Add a pseudo dimension to a 2D image.
        if ndims(im) == 2
            im = reshape(im, [size(im, 1) size(im, 2), 1]);
        end
        
        % Go through the slices
        for iplane = 0:(size(im, 3)-1)
                tic;
                mycallbacks2(fig, 'go_to', iplane);
                
                % Go through the segmented objects ...        
                for iobj = 1:numel(ids)
                        %fprintf('Importing cell %d/%d (plane %d) ...', iobj, numel(ids), iplane);
                        % Create a binary image containing the object.
                        tmp = double(im(:, :, iplane) == ids(iobj));
                        % Find a polygon outlining it.
                        p = bwboundaries(tmp);
                        
                        if isempty(p)
                        %        fprintf(' empty.\n');
                                continue;
                        end
                        
                        p = p{1}; % The output of bwboundaries contains one cell per object present in the image.
                        p = p(:, [2 1]) - 1; % For bwboundaries, the smallest coordinates are [1,1], while in rfig2, [0,0] is a valid point for a fiducial.
                        p = round(p .* subs_factor_xy);
                        ud2.rpoly = [];
                        
                        % And store the polygon.
                        if ~ isempty(p)
                                if ~ isempty(p)
                                        ud2.rpoly((end+1):(end+size(p,1)), :) = p; 
                                else
                                        ud2.rpoly(end+1, :) = ud.rpoly(1, :);
                                end                                
                                
                                % Store in in the first empty spot.
                                myind = 1;
                                for is = 1:size(ud2.rpolygons, 1)
                                        if (isempty(ud2.rpolygons{is, 1, iplane+1}))
                                                ud2.rpolygons{is, 1, iplane + 1} = ud2.rpoly;
                                                break;
                                        end
                                        myind = myind + 1;
                                end
                        
                                % If there were no empty spots, store at the end.
                                if (myind > size(ud2.rpolygons, 1))
                                        ud2.rpolygons{end+1, 1, iplane + 1} = ud2.rpoly;
                                end
                                
                                ud2.rpoly = [];
                        end
                        %fprintf(' done.\n');
                end
                
                thetime = toc; % Time invested in last plane.
                thetime = thetime * (size(im, 3)-iplane-1); % Approximate time remaining in seconds.
                if thetime > 3600
                        fprintf('Approximately %.2f hours left ...\n', thetime/3600);
                elseif thetime > 60                   
                        fprintf('Approximately %.2f minutes left ...\n', thetime/60);
                else
                        fprintf('Approximately %.2f seconds left ...\n', thetime);
                end
        end
        
        mycallbacks2(fig, 'go_to', curr_tp); % Go back to the time point where everything started.
end
cd(curr_dir);
end