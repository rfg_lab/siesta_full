% [mvalues, populations] = msims(ns, positions, p, np, r, dt, good_positions, shells)
%
% Returns the M values resulting from the evaluation of MUNIV on each one
% of the ns random "labelings" that it generates.
%  
% NS is the number of simulations to run.
% POSITIONS is a matrix where each row represents the coordinates of a point.
% P is the label for the population of interest.
% NP is the number of positions that belong to P in each simulation.
% R is the maximum distance/s to consider two positions as neighbors.
% DT is the table of minimum distances among any two positions.
% GOOD_POSITIONS is a vector with ones for POSITIONS that can be assigned
% to P in the simulations, and zeros for those where this is not possible.
% SHELLS is zero for 'within' (<=r) analysis and non-zero for 'at'
% (==r) analysis.
%  
% MVALUES is a matrix containing the M values that result from each of the
% simulations (one simulation per row).
% POPULATIONS is a matrix where index i in row j indicates the population 
% of position i in simulation j.
% MIPR is a list of M values per position belonging to P.
%
% Rodrigo Fernandez-Gonzalez
% fernanr1@mskcc.org
% 11/29/2006

function varargout = msims(ns, positions, p, np, r, dt, good_positions, shells)

if (ns == 0)
	varargout{1, 1} = [];
	varargout{1, 2} = [];
	varargout{1, 3} = [];
	
	return;
end	


% THIS ONLY WHEN IN THE SIMULATION THERE IS JUST A SUBSET OF INTERESTING
% POSITIONS THAT CAN BE ASSIGNED TO THE LABELED POPULATION.
N = sum(good_positions == 1);
good_indices = find(good_positions == 1);

% WHAT IF np > N?

% Generate random distributions of np positions within a total of N of them.
% Repeat ns times. Restrict yourself to N good positions.

try
	good_populations = mrandomlabel(N, p, np, ns);
catch
	% This is true only if mrandomlabel returns with an error.
	varargout{1, 1} = [];
	varargout{1, 2} = [];
	varargout{1, 3} = [];
	
	return;
end

% Now map the good positions back to all the positions.
good_populations = mod(find(good_populations' == 1), N);
good_populations(find(good_populations == 0)) = N;
all_indices = reshape(good_indices(good_populations), [np ns])';

mvalues = zeros(ns, prod(size(r)));
mipr = zeros(np, numel(r), ns);
populations = zeros(ns, size(positions, 1));

% This loop was not written in C, as it would still require a call
% to an interpreted Matlab function (muniv) from C. If this is slow, 
% you will have to vectorize mnipr.m and muniv.m so that they can 
% work with 3D arrays for the POPULATIONS parameter.
tic

fprintf(1, 'Running simulations ...');
for i = 1 : ns
	%fprintf(1, 'Running simulation number %d ...', i); 
	populations(i, all_indices(i, :)) = 1;
	% mchecklabelingconditions1(populations(i, :));
	[mvalues(i, :) mipr(:, :, i)] = muniv(positions, populations(i, :), p, r, dt, shells);
	%fprintf(1, ' done.\n');
end
fprintf(1, ' done.\n');

toc

varargout{1, 1} = mvalues;
varargout{1, 2} = populations;
varargout{1, 3} = mipr;

return;
