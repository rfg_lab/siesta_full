/*
  dt = mfloyd(positions, neigh_graph, weighted)

  Implements Floyd's algorithm for the shortest distance
  between all possible pairs of positions without recovering
  the actual path.
  For a short and nice pseudocode see:
  http://www-unix.mcs.anl.gov/dbpp/text/node35.html

  POSITIONS is a matrix where each row represents the coordinates of a point.
  NEIGH_GRAPH is a matrix where (i, j) = 1 indicates that positions i and j
  are immediate neighbors.
  DT is a matrix where element (i, j) contains the minimum distance between
  positions i and j in neigh_graph.

  Rodrigo Fernandez-Gonzalez
  fernanr1@mskcc.org
  11/06/2006
*/

#define min(a,b) ((a) < (b) ? (a) : (b))
#include "mex.h"
#include <math.h>

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	/*Declarations*/
	mxArray *positions, *neighGraph, *weighted;
	double *dPositions, *dNeighGraph;
	char cWeighted;
	int i, j, k, ind1, ind2;
	int N, dim;
	double *newR, *oldR;
	double DOUBLE_MAX = 2e9;
	double distance;

	/* CHECK PARAMETERS! */
	switch (nrhs)
	{
		case 2:
			if (! (mxIsNumeric(prhs[0]) && (mxGetN((mxArray *) prhs[0]) * mxGetM((mxArray *) prhs[0]) > 1)))
				mexErrMsgTxt("First argument should be a matrix.");
		
			if (! (mxIsNumeric(prhs[1])&& (mxGetN((mxArray *) prhs[1]) * mxGetM((mxArray *) prhs[1]) == 1)))
				mexErrMsgTxt("Second argument should be a number.");
			break;
		case 3:
			if (! (mxIsNumeric(prhs[0]) && (mxGetN((mxArray *) prhs[0]) * mxGetM((mxArray *) prhs[0]) > 1)))
				mexErrMsgTxt("First argument should be a matrix.");
		
			if (! (mxIsNumeric(prhs[1]) && (mxGetN((mxArray *) prhs[1]) * mxGetM((mxArray *) prhs[1]) > 1)))		
				mexErrMsgTxt("Second argument should be a matrix.");
		
			if (! (mxIsNumeric(prhs[2])&& (mxGetN((mxArray *) prhs[2]) * mxGetM((mxArray *) prhs[2]) == 1)))
				mexErrMsgTxt("Third argument should be a number.");
		
			break;
		default:
			mexErrMsgTxt("Wrong number of input arguments.");
			break;
	}
	
	switch (nlhs)
	{
		case 0:
		case 1:
			break;

		default:
			mexErrMsgTxt("Wrong number of output arguments.");
			break;
	}

	/* End Check Parameters.*/

	/*printf("Computing distance table ...\n");*/
	
	/*Copy input pointers.*/
	positions = (mxArray *) prhs[0];
	neighGraph = (mxArray *) prhs[1];
	weighted = (mxArray *) prhs[2];

	/*Get parameters.*/
	dPositions = (double *) mxGetPr(positions);
	dNeighGraph = (double *) mxGetPr(neighGraph);
	/*MATLAB gives the matrix by columns so to jump by position, 
	(x,y) maps to x*colLen+y, and (row, col) maps to col*nRows+row*/
		
	cWeighted = (char) mxGetScalar(weighted);
	
	N = mxGetM(positions);
	dim = mxGetN(positions);/*printf("dim: %d\n\n", dim);*/

	/*printf("Positions\n");
	for (i = 0; i < N; i ++)
	{
		for (j = 0; j < dim; j ++)
		{
			printf("%d: %f ", (j*dim+i), dPositions[j * dim + i]);
		}
		
		printf("\n");
	}*/

	/*printf("NeighGraph\n");
	for (i = 0; i < N; i ++)
	{
		for (j = 0; j < N; j ++)
		{
			printf("%d: %f ", (j*dim+i), dNeighGraph[j * dim + i]);
		}
		
		printf("\n");
	}*/

	/*rowLen = mxGetN(xData);*/
	/*colLen = mxGetM(xData);*/

	/*printf("Allocating memory and initializing ...\n");*/
	
	newR = (double *) mxMalloc(sizeof(double) * N * N);

        /* Matrix initialization.*/
        for (i = 0; i < N; i ++)
        {
            for (j = i; j < N; j ++)
            {
	    	ind1 = i * N + j; /*printf("Ind: %d\n", ind1);*/
		ind2 = j * N + i;
		
                if (i == j)
                {
                    newR[ind1] = 0.0; /*printf("Ind: %d\n", newR[ind1]);*/
                }

                else
                {   
		    /*printf("%d and %d neighbors? %d\n", i, j, (int) dNeighGraph[ind1]);*/
                    if (dNeighGraph[ind1] != 0)
                    {
                        
                        switch (cWeighted)
			{
				/* Here we are using unweighted distances.*/
				case ((char) 0):
					newR[ind1] = 1.0;
		                        newR[ind2] = 1.0;/*printf("Ind: %d\n", newR[ind1]);*/
					
					break;
					
				/* If you want to use weighted distances, then: */	
				default:
					/* Compute the distance between both positions. */
					distance = 0.0;
					
					for (k = 0; k < dim; k ++)
					{
						distance += pow((dPositions[k * N + i] - dPositions[k * N + j]), 2.0);
						/*printf("%d: %f, %d: %f, distance: %f\n", (k * N + i), (k * N + j), dPositions[k * N + i], dPositions[k * N + j], distance);*/
					}
					
					distance = sqrt(distance);
					
					/* Assign it to the distance table at the appropriate cells. */
					newR[ind1] = distance;/*printf("Ind: %f\n", distance);*/
		                        newR[ind2] = distance;
					
					break;
                        }

                    }

                    else
                    {
                        /* Use a large value so that there is no overflow. */
                        newR[ind1] = DOUBLE_MAX;
                        newR[ind2] = DOUBLE_MAX;
                    }
                }
            }
        }
	
	/*printf("Initial matrix\n");
	for (i = 0; i < N; i ++)
	{
		for (j = 0; j < N; j ++)
		{
			printf("%d: %f ", (i*N+j), newR[i * N + j]);
		}
		
		printf("\n");
	}*/

	printf("Computing distance table ...\n----------------------------\n");
	
        /* Iterations.*/
        for (k = 0; k < N; k ++)
        {
	    if ((k+1)%100 == 0) printf("Iteration %d of %d\n", (k+1), N);
            /*printf("k=%d, N=%d\n", k, N);*/
            oldR = newR;
            newR = (double *) mxMalloc(sizeof(double) * N * N);

            for (i = 0; i < N; i ++)
            {
                for (j = 0; j < N; j ++)
                {
                    newR[j * N + i] = min(oldR[j * N + i], oldR[k * N + i] + oldR[j * N + k]); 
		    /*printf("%f ", newR[j * N + i]);*/
		    /*newR[i * N + j] = newR[j * N + i];*/
                }
		/*printf("\n");*/
            }
	    
	    /* Free old pointer. Without this line the function crashes the computer
	       for large matrices after a couple hundred iterations. */
	    mxFree(oldR);
	    /*printf("\n\n\n\n\n\n");*/
        }

	printf("\nDone!\n");
	
	/*Allocate memory for output pointer.*/
	plhs[0] = mxCreateDoubleMatrix(N, N, mxREAL);

	mxSetPr(plhs[0], newR);
	
	return;
}
