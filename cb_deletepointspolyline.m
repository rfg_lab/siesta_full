% *********************************************************************************
function [fidu2 nfidu2] = cb_deletepointspolyline(fig, fidu, nfidu, inpoly)

obj = get(fig, 'CurrentObject');
type = get(obj, 'Tag');
ax = get(fig, 'CurrentAxes');
ch = get(ax, 'Children');


if strcmp(type, 'Polyline')
    polyx = get(obj, 'XData');
    polyy = get(obj, 'YData');
	ud = get(fig, 'UserData');
	curslice = ud.curslice + 1;
	nfidu(curslice) = nfidu(curslice) - 1;
	%ind = intersect(find(fidu(:, 1, curslice) == get(obj, 'XData')), ...
	%		find(fidu(:, 2, curslice) == get(obj, 'YData')));
	
	for i = 1:numel(ch)
		 if strcmp(get(ch(i), 'Tag'), 'Fiducial')
		 	X = get(ch(i), 'XData');
			Y = get(ch(i), 'YData');
			[in on] = inpolygon(Y, X, polyy, polyx);
		
			% If the point is inside (or on) the polygon and you have to delete points inside
			% or if the point is outside and those are the points you are supposed to delete.
			if ((in+on) >= 1 && inpoly) | ((in+on) == 0 && (~ inpoly))
				set(fig, 'CurrentObject', ch(i));
				[fidu nfidu] = cb_deletepoint(fig, fidu, nfidu);
			end
		end
	end
end

fidu2 = fidu;
nfidu2 = nfidu;
end