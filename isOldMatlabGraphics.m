function boolean = isOldMatlabGraphics()
    % Returns true if the version of MATLAB being run is older than 2014b,
    % the version at which huge changes were made to the graphics system
    % and breaks old code
    versionNumber = version('-release');
    if(str2double(versionNumber(1:4)) < 2014)
        boolean = true;
        return;
    elseif(str2double(versionNumber(1:4)) == 2014 && versionNumber(5) == 'a')
        boolean = true;
        return;
    else
        boolean = false;
        return;
    end
end