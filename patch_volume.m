%  volume = patch_volume(patch, direction)
%
%  Measures the volume of an object represented by a triangulation of its surface.
%  Each triangle is projected onto a certain plain, and the volume of the prism that
%  forms is added or subtracted from the total object volume depending on the
%  orientation of the triangle. The finer the mesh, the more accurate the estimate.
%  
%  PATCH is a structure with VERTICES and FACES fields as that produced by ISOSURFACE
%  or ISOCAPS.
%  DIRECTION is a unit vector that determines the direction in which the vertices are 
%  projected. Defaults to [0 0 1]. If [0 0 1] is used, make sure the top and bottom of
%  the object are provided.
%
%  VOLUME is the volume of the object.
%
%  Rodrigo Fernandez-Gonzalez
%  fernanr1@mskcc.org
%  4/23/2007

function varargout = patch_volume(varargin)

% Parse input arguments.
switch (nargin)
	case 1
		fv = varargin{1};
		vector = [0 0 1];
		
	case 2
		fv = varargin{1};
		vector = varargin{2};
		vector = vector ./ norm(vector, 2);
end


volume = 0.;

% For every triangle on the surface ...
for ii = 1:size(fv.faces, 1)
	a = fv.vertices(fv.faces(ii, 3), :);
	b = fv.vertices(fv.faces(ii, 2), :);
	c = fv.vertices(fv.faces(ii, 1), :);
	
	ac = c-a;
	ab = b-a;
	
	% ||p|| is the area of the triangle.
	p = cross(ab, ac)./2;
	
	% v = ||p|| * ||vector|| * cos(theta) = area_triangle * 1 * cos(theta) = 
	% = area_triangle_projected_on_plane_defined_by_vector
	v = dot(p, vector);

	% p is a vector pointing from the origin to the center of the triangle.
	p = (a+b+c)./3;
	
	% dot(p, vector) = height_of_p_with_respect_to_the_plane_defined_by_vector.
	volume = volume + v * dot(p, vector);
end

varargout{1} = abs(volume);
