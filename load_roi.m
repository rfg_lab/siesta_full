% roi = load_roi(index)
%
% index is actually the time point number (not its ordinal).
% e.g.: for segm_T0004, index == 4.

function roi = load_roi(index)

time_str = rnum2str(index, 4);
if (exist(cat(2, cat(2, 'roi_T', time_str), '.mat'), 'file'))
	%disp(cat(2, cat(2, 'roi_T', time_str), '.mat'));
	vars = load(cat(2, cat(2, 'roi_T', time_str), '.mat'));
	roi = vars.roi;
else
	% This bit could be outside the loop, but thus, we keep the code
	% compact and easy to copy and paste.
	d = dir('./roi_T*');
	
	% There are ROIs defined.
	if (numel(d) > 0)
		rois = zeros(1, numel(d));
		
		for iroi = 1:numel(d)
			sroi = d(iroi).name;
			iT = strfind(sroi, '_T');
			rois(iroi) = str2num(sroi((iT+2):(iT+5)));
		end		
		% End this could be outside the loop.

		roisd = abs(rois - (index));
		[tmproi roi_index] = min(roisd);
		roi_index = rois(roi_index);

		time_str_roi = rnum2str(roi_index, 4);	
		%disp(cat(2, cat(2, 'roi_T', time_str_roi), '.mat'));
		vars = load(cat(2, cat(2, 'roi_T', time_str_roi), '.mat'));
		roi = vars.roi;
	
	% If no ROI has been defined, do nothing.
	else
		roi = [];
		fprintf('No roi defined.\n');
	end
end