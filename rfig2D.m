% Example:
% rfig2D('/home/rodrigo/data/Images/20080502/exported_117GFP_sqhmCherry_2/geometry_corrected1-15-24-32-41-49_myoadj_localth_data.mat', 49, '117GFP_sqhmCherry_2', 0, (2:4), -8)
%
% Quick keys:
% , : previous time point
% . : next time point
% 1: consolidate geometry
% 2: generate cell map (assigns nodes and edges to cells)
% 3: save geometry
% 4: save roi
% 5: measure cells (?)
% 6: measure edges
% 7: measure alignment
% 8: measure cable index
% b: define background area
% C: cable statistics
% c: clear edge selection
% d: select horizontal edges
% e: edit edges
% g: go to time point
% h: hide/show geometry
% i: select isolated vertical edges
% l: select linked vertical edges
% n: edit nodes
% p: select positive (intensity based) edges
% r: define roi
% s: select edges
% T: measure T1 processes (orientation, ...)
% t: inspect edge intensity

function fig = rfig2D(varargin)
%rfig2D('geometry_data_corrected', <index>, 'im_segm_T', imshift, slices, rot_ang)
iconpath = '/home/rodrigo/src/CellSegmentation/icons/';

slice2 = [];
fileformat = []; % Ultraview or Metamorph-acquired image.

switch nargin
	case 1
		varargin{2} = 1;
		prefix = '';
		imshift = 1;
		slice_list = 0;
		ang = 0;		
	case 2
		prefix = 'im_segm_T';
		imshift = 1;
		slice_list = 0;
		ang = 0;
	case 6
		prefix = varargin{3};
		imshift = varargin{4};
		slice_list = varargin{5};
		ang = varargin{6};
	
	otherwise
		error('Wrong number of parameters.');
end	

if isstr(varargin{1})
	if exist(varargin{1}, 'file')
		[pathstr name ext] = fileparts(varargin{1});
		if ~ isempty(pathstr)
			cd(pathstr);
			fprintf('\nChanging to directory %s.\n', pathstr);
		end
		
		vars = load(varargin{1});
		if isfield(vars, 'geometry')
			geometry = vars.geometry;
			index = 1;
			if nargin > 1
				index = varargin{2};
			end

			curr_geom = geometry(index);
			
			if isfield(vars, 'measurements')
				measurements = vars.measurements;
			else
				measurements = cell(1, numel(geometry));
			end
			
			if isfield(vars, 'mean_roi')
				mean_roi = vars.mean_roi;
			else
				mean_roi = zeros(1, numel(geometry));
			end
			
			if isfield(vars, 'mean_bg')
				mean_bg = vars.mean_bg;
			else
				mean_bg = zeros(1, numel(geometry));
			end
			
			if isfield(vars, 'std_roi')
				std_roi = vars.std_roi;
			else
				std_roi = zeros(1, numel(geometry));
			end
			
			if isfield(vars, 'max_rel_roi')
				max_roi = vars.max_rel_roi;
			else
				max_roi = zeros(1, numel(geometry));
			end
			
			if isfield(vars, 'min_rel_roi')
				min_roi = vars.min_rel_roi;
			else				
				min_roi = zeros(1, numel(geometry));
			end
			
		elseif isfield(vars, 'g')
			geometry = vars.g;
			index = 1;
			if nargin > 1
				index = varargin{2};
			end
			
			curr_geom = geometry(index);
			
			if isfield(vars, 'measurements')
				measurements = vars.measurements;
			else
				measurements = cell(1, numel(geometry));
			end
			
			if isfield(vars, 'mean_roi')
				mean_roi = vars.mean_roi;
			else
				mean_roi = zeros(1, numel(geometry));
			end
			
			if isfield(vars, 'mean_bg')
				mean_bg = vars.mean_bg;
			else
				mean_bg = zeros(1, numel(geometry));
			end
			
			if isfield(vars, 'std_roi')
				std_roi = vars.std_roi;
			else
				std_roi = zeros(1, numel(geometry));
			end
			
			if isfield(vars, 'max_rel_roi')
				max_roi = vars.max_rel_roi;
			else
				max_roi = zeros(1, numel(geometry));
			end
			
			if isfield(vars, 'min_rel_roi')
				min_roi = vars.min_rel_roi;
			else				
				min_roi = zeros(1, numel(geometry));
			end
			
                elseif isfield(vars, 'cellgeom')
                        geometry = vars.cellgeom;
                        index = 1;
                        if nargin > 1
                                index = varargin{2};
                        end
                        
                        curr_geom = geometry(index);
                        
                        if isfield(vars, 'measurements')
                                measurements = vars.measurements;
                        else
                                measurements = cell(1, numel(geometry));
                        end
                        
                        if isfield(vars, 'mean_roi')
                                mean_roi = vars.mean_roi;
                        else
                                mean_roi = zeros(1, numel(geometry));
                        end
                        
                        if isfield(vars, 'mean_bg')
                                mean_bg = vars.mean_bg;
                        else
                                mean_bg = zeros(1, numel(geometry));
                        end
                        
                        if isfield(vars, 'std_roi')
                                std_roi = vars.std_roi;
                        else
                                std_roi = zeros(1, numel(geometry));
                        end
                        
                        if isfield(vars, 'max_rel_roi')
                                max_roi = vars.max_rel_roi;
                        else
                                max_roi = zeros(1, numel(geometry));
                        end
                        
                        if isfield(vars, 'min_rel_roi')
                                min_roi = vars.min_rel_roi;
                        else                            
                                min_roi = zeros(1, numel(geometry));
                        end
                        
		else
			error('Geometry not found in the specified file.');
		end
	else
		error('File not found.');		
	end

elseif isstruct(varargin{1})
	geometry = varargin{1};
	measurements = cell(1, numel(geometry));
	mean_roi = zeros(1, numel(geometry));
	mean_bg = zeros(1, numel(geometry));
	std_roi = zeros(1, numel(geometry));
	max_roi = zeros(1, numel(geometry));
	min_roi = zeros(1, numel(geometry));
	
	index = 1;
	
	if nargin > 1
		index = varargin{2};
	end
	
	curr_geom = geometry(index);
end

% Deciding what background file to load.
if isstr(prefix)
        ang = ang .* pi / 180;
	if numel(slice_list) > 1
				
		time_str = rnum2str(index+imshift, 4);
		files = dir(cat(2, prefix, cat(2, time_str, '_568nm_Z*')));
		
		% By default, read red channel (I assume if you want to browse the
		% green channel you will use the 'im_segm_T' images produced by
		% the segmentation.
		
		% No old format red channel.
		if numel(files) == 0
			files = dir(cat(2, prefix, '_w2568-power_t*'));
			
			% No new format red channel
			if numel(files) == 0
				files = dir(cat(2, prefix, cat(2, time_str, '_488nm_Z*')));
				
				% No old format green channel - assume new format green channel.
				if numel(files) == 0
					time_str = rnum2str(index+imshift, 1);
	                                try 				
                                                im = tiffread(cat(2, prefix, cat(2, '_w1488-power_t', cat(2, time_str, '.TIF'))));
                                        catch
                                                im = tiffread(cat(2, prefix, time_str, '.TIF'));
                                        end
					fileformat = 'metamorph';
				% Found old format green channel.
				else
					im = readtimeseries(cat(2, prefix, cat(2, time_str, '_488nm_Z.tif')));
					fileformat = 'ultraview';
				end
			% Found new format red channel.
			else
				time_str = rnum2str(index+imshift, 1);
				im = tiffread(cat(2, prefix, cat(2, '_w2568-power_t', cat(2, time_str, '.TIF'))));
				fileformat = 'metamorph';
			
                                % Try to read the "green" image used for segmentation. This is a single slice.                    
	                        try			
                                        slice2 = readim(cat(2, 'im_segm_T', cat(2, time_str, '.ics')));
                                        slice2 = squeeze(slice2(:, :, 1));
                               % If not present, just go with im.        
				catch
                                        slice2 = stretch(rotation(stretch(dip_image(sum(double(im(0:end, 0:end, slice_list)), 3))), ang, 3, 'zoh'), 0, 100, 0, 255);         slice2 = squeeze(slice2);                
                                end                            
 			end
		% Found old format red channel.
		else
			im = readtimeseries(cat(2, prefix, cat(2, time_str, '_568nm_Z.tif')));
			fileformat = 'ultraview';
			
			slice2 = readim(cat(2, 'im_segm_T', cat(2, time_str, '.ics')));
			slice2 = squeeze(slice2(:, :, 1));
		end
		slice = stretch(rotation(stretch(dip_image(sum(double(im(0:end, 0:end, slice_list)), 3))), ang, 3, 'zoh'), 0, 100, 0, 255); % See notebook on 20090424 regarding why this is correct.
                %slice = rotation(dip_image(sum(double(im(0:end, 0:end, slice_list)), 3)), ang, 3, 'zoh');              
                %slice = stretch(rotation(stretch(dip_image(max(double(im(0:end, 0:end, slice_list)),[], 3))), ang, 3, 'zoh'), 0, 100, 0, 255);              
		%slice = rotation(dip_image(max(double(im(0:end, 0:end, slice_list)),[], 3)), ang, 3, 'zoh');              
                
        elseif slice_list == 0
            im = tiffread(prefix);
            slice = stretch(rotation(stretch(dip_image(sum(double(im(0:end, 0:end, slice_list)), 3))), ang, 3, 'zoh'), 0, 100, 0, 255);         slice = squeeze(slice);
        % In case you only want im_segm_T to open (single plane).
	else
		if isstr(varargin{1}) && isempty(prefix)
                        matfilename = varargin{1};           
                        extind = regexp(matfilename, '.mat');
                        tiffilename = cat(2, matfilename(1:(extind)), 'tif');
                        if exist(tiffilename, 'file')
                                im = readim(tiffilename);
                                if ndims(im) == 2
                                        im = expanddim(im, 3);
                                end
                                
                                slice = rotation(dip_image(sum(double(im(0:end, 0:end, slice_list)), 3)), ang, 3, 'zoh'); 
                                
                                time_str = '0000';
                        else
                                error('Image file %s not found.', tifffilename);
                        end                        
                else
                
                        time_str = rnum2str(index+imshift, 4);
		        files = dir(cat(2, prefix, cat(2, time_str, '*')));
		        
		        % If no files were found with the old time formating (four digits)
		        % use the new one.
		        if numel(files) == 0
			        time_str = rnum2str(index+imshift, 1);
			        fileformat = 'metamorph';
		        else
			        fileformat = 'ultraview';
		        end
		        
		        im = readim(cat(2, prefix, time_str));
                        if ndims(im) == 2
                                im = expanddim(im, 3);
                        end		
                        %slice = double(im(0:end, 0:end, slice_list)); % Do not stretch when not sum projecting.
                        slice = rotation(dip_image(sum(double(im(0:end, 0:end, slice_list)), 3)), ang, 3, 'zoh'); % Do not stretch when not sum projecting.
                end	
        end
elseif isa(prefix, 'dip_image') | isa(prefix, 'dip_image_array')
	slice = prefix;
	prefix = '';
	time_str = '';
end
%imshow(double(slice), [0 255]);
fig = dipshow(slice);
ud = get(fig, 'UserData');
if ~ isempty(slice2)
	ud.colordata = joinchannels('rgb', slice, slice2);
	set(fig, 'UserData', ud);
	ud.imagedata = ud.colordata{1};
end

set(fig, 'MenuBar', 'figure');

% Find an roi ... if defined.
roi = [];

if (exist(cat(2, cat(2, 'roi_T', time_str), '.mat'), 'file'))
	vars = load(cat(2, cat(2, 'roi_T', time_str), '.mat'));
	roi = vars.roi;
else
	% This bit could be outside the loop, but thus, we keep the code
	% compact and easy to copy and paste.
	d = dir('./roi_T*');

	% There are ROIs defined.
	if (numel(d) > 0)
		rois = zeros(1, numel(d));

		for iroi = 1:numel(d)
			sroi = d(iroi).name;
			iT = strfind(sroi, '_T');
			rois(iroi) = str2num(sroi((iT+2):(iT+5)));
		end		
		% End this could be outside the loop.
		roisd = abs(rois - index - imshift);
		[tmproi roi_index] = min(roisd);
		roi_index = rois(roi_index);

		time_str_roi = [];
		for ii = 1 : (4-numel(num2str(roi_index)))
			time_str_roi = cat(2, time_str_roi, '0');
		end

		time_str_roi = cat(2, time_str_roi, num2str(roi_index));

		vars = load(cat(2, cat(2, 'roi_T', time_str_roi), '.mat'));
		roi = vars.roi;

	% If no ROI has been defined, do nothing.
	else
		roi = [];
	end
end

if isempty(roi)
	d = dir('./roi*');
			
	% There are ROIs defined.
	if (numel(d) > 0)
		vars = load(d(1).name);
		if ~isa(vars.roi, 'dip_image')
			roi = vars.roi;
		else
			roi = [];
		end
			
	% If no ROI has been defined, look for the embryo limits.
	else
		roi = [];
	end
end

if ~ isempty(roi)
	h = rectangle('Position', [roi(1, 1), roi(1, 2), abs(roi(2, 1) - roi(1, 1)), abs(roi(2, 2) - roi(1, 2))]);
	set(h, 'EdgeColor', 'b', 'Tag', 'ROI');
	
	xlim([roi(1, 1)-25 roi(2, 1)+25]);
	ylim([roi(1, 2)-25 roi(2, 2)+25]);
else
	xlim([0 ud.imsize(2)-1]);
	xlim([0 ud.imsize(1)-1]);
	
	disp('No roi defined.');
end

x = xlim; 
y = ylim;

plot_geometry(curr_geom.nodes, curr_geom.edges, 'r', 0);
%plot_geometry(curr_geom.nodes, curr_geom.edges, 'b', 0, [], [], 5, 3);
ud.geometry = geometry;
ud.rmeasurements = measurements;
ud.rmeanroi = mean_roi;
ud.rmeanbg = mean_bg;
ud.rstdroi = std_roi;
ud.rmaxroi = max_roi;
ud.rminroi = min_roi;
ud.rselectededges = cell(1, numel(ud.geometry));
ud.index = index;
ud.prefix = prefix;
ud.imshift = imshift;
ud.slice_list = slice_list;
ud.ang = ang;
ud.fileformat = fileformat;
ud = init_userdata(ud);

if size(roi, 2) == 2
	roi(1, 3) = 0;
	roi(2, 3) = 0;
end

ud.roi = roi;
set(gca, 'Visible', 'off');
if isstr(varargin{1})
        matfilename = varargin{1};
        indext = regexp(matfilename, '.mat'); 
        ud.imname = matfilename(1:(indext-1));
end        
set(fig, 'UserData', ud);
set(fig, 'KeyPressFcn', 'mycallbacks2D(gcbf, cat(2, ''k_'', get(gcbf, ''CurrentCharacter'')));');
axis ij;
set(fig, 'Name', cat(2, prefix, time_str));

% Processing toolbar
htb = uitoolbar;

%imaddnode = rand(16, 16, 3); % An icon 16x16 pixels, three channels (color).
%nodebutton = uitoggletool(htb, 'CData', imaddnode, 'TooltipString', 'Add/Remove node', 'OnCallback', @nested_callbackwrapper_node, 'OffCallback', 'mycallbacks2D(gcbf, ''no_add_node'');');
%imaddedge = rand(16, 16, 3); % An icon 16x16 pixels, three channels (color).
%edgebutton = uitoggletool(htb, 'CData', imaddedge, 'TooltipString', 'Add/Remove edge', 'OnCallback', @nested_callbackwrapper_edge, 'OffCallback', 'mycallbacks2D(gcbf, ''no_add_edge'');');
%imprevious = rand(16, 16, 3); % An icon 16x16 pixels, three channels (color).
%uipushtool(htb, 'CData', imprevious, 'TooltipString', 'Previous geometry', 'ClickedCallback', 'mycallbacks2D(gcbf, ''prev_geom'');');
%imnext = rand(16, 16, 3); % An icon 16x16 pixels, three channels (color).
%uipushtool(htb, 'CData', imnext, 'TooltipString', 'Next geometry', 'ClickedCallback', 'mycallbacks2D(gcbf, ''next_geom'');');
imconsolidate = rand(16, 16, 3);%imread(cat(2, iconpath, 'number1.jpg')); % An icon 16x16 pixels, three channels (color).
uipushtool(htb, 'CData', imconsolidate, 'TooltipString', 'Consolidate geometry', 'ClickedCallback', 'mycallbacks2D(gcbf, ''consolidate_geom'');');
imgencellmap = rand(16, 16, 3);%imread(cat(2, iconpath, 'number2.jpg')); % An icon 16x16 pixels, three channels (color).
uipushtool(htb, 'CData', imgencellmap, 'TooltipString', 'Generate cell map', 'ClickedCallback', 'mycallbacks2D(gcbf, ''gen_cells'');');
imsave = rand(16, 16, 3);%imread(cat(2, iconpath, 'number3.jpg')); %rand(16, 16, 3); % An icon 16x16 pixels, three channels (color).
uipushtool(htb, 'CData', imsave, 'TooltipString', 'Save geometry ...', 'ClickedCallback', 'mycallbacks2D(gcbf, ''save_geom'');');
imsaveroi = rand(16, 16, 3);%imread(cat(2, iconpath, 'number4.jpg')); %rand(16, 16, 3); % An icon 16x16 pixels, three channels (color).
uipushtool(htb, 'CData', imsaveroi, 'TooltipString', 'Save ROI ...', 'ClickedCallback', 'mycallbacks2D(gcbf, ''save_roi'');');
immeasure2D = rand(16, 16, 3);%imread(cat(2, iconpath, 'number5.jpg'));
uipushtool(htb, 'CData', immeasure2D, 'TooltipString', 'Measure cells ...', 'ClickedCallback', 'mycallbacks2D(gcbf, ''measure_2D'');');
immeasureedges = rand(16, 16, 3);%imread(cat(2, iconpath, 'number6.jpg'));
uipushtool(htb, 'CData', immeasureedges, 'TooltipString', 'Measure edges ...', 'ClickedCallback', 'mycallbacks2D(gcbf, ''measure_edges'');');
immeasurealignment = rand(16, 16, 3);%imread(cat(2, iconpath, 'number7.jpg'));
uipushtool(htb, 'CData', immeasurealignment, 'TooltipString', 'Measure alignment ...', 'ClickedCallback', 'mycallbacks2D(gcbf, ''measure_alignment'');');
immeasuremfn = rand(16, 16, 3);
uipushtool(htb, 'CData', immeasuremfn, 'TooltipString', 'Measure M function ...', 'ClickedCallback', 'mycallbacks2D(gcbf, ''measure_mfn'');');


% Interaction toolbar
htbint = uitoolbar;
imaskngoto = rand(16, 16, 3); % An icon 16x16 pixels, three channels (color).
uipushtool(htbint, 'CData', imaskngoto, 'TooltipString', 'Go to - g', 'ClickedCallback', 'mycallbacks2D(gcbf, ''ask_n_goto'');');

imselectvertical = rand(16, 16, 3); % An icon 16x16 pixels, three channels (color).
uipushtool(htbint, 'CData', imselectvertical, 'TooltipString', 'Select vertical interfaces - i', 'ClickedCallback', 'mycallbacks2D(gcbf, ''select_vertical'');');
imselectverticalchains = rand(16, 16, 3); % An icon 16x16 pixels, three channels (color).
uipushtool(htbint, 'CData', imselectverticalchains, 'TooltipString', 'Select cables - l', 'ClickedCallback', 'mycallbacks2D(gcbf, ''select_vcables'');');
imselecthorizontal = rand(16, 16, 3); % An icon 16x16 pixels, three channels (color).
uipushtool(htbint, 'CData', imselecthorizontal, 'TooltipString', 'Select horizontal interfaces - d', 'ClickedCallback', 'mycallbacks2D(gcbf, ''select_horizontal'');');
imselectpositive = rand(16, 16, 3); % An icon 16x16 pixels, three channels (color).
uipushtool(htbint, 'CData', imselectpositive, 'TooltipString', 'Select positive interfaces - p', 'ClickedCallback', 'mycallbacks2D(gcbf, ''select_positive'');');


%if ~ isempty(roi)
%	xlim([roi(1, 1)-25 roi(2, 1)+25]);
%	ylim([roi(1, 2)-25 roi(2, 2)+25]);
%else
%	xlim([0 ud.imsize(2)-1]);
%	xlim([0 ud.imsize(1)-1]);
%end
set(gca, 'XLim', x, 'YLim', y);

function nested_callbackwrapper_node(h, eventdata, handles)
	set(edgebutton, 'State', 'off');
	mycallbacks2D(gcbf, 'add_node');
end

function nested_callbackwrapper_edge(h, eventdata, handles)
	set(nodebutton, 'State', 'off');
	mycallbacks2D(gcbf, 'add_edge');
end

end
