% *********************************************************************************

function lns2 = cb_clearlines(handles, lns)
% if strncmp(get(fig,'Tag'),'DIP_Image',9)
        objects = findobj();
        
	for il = 1:numel(lns)
                aline = lns(il);
                if (~ isempty(find(objects == aline)))
                        delete(lns(il));
                end
	end
	
	lns2 = [];
% else
%     lns2 = [];
% end

end