% *********************************************************************************

function cb_deletecell(fig, cell_id)

if (cell_id > 0)
% 	if strncmp(get(fig,'Tag'),'DIP_Image',9)
	      img = get(fig,'CurrentObject');
	      if strcmp(get(img,'Type'),'image')
        	  udata = get(fig,'UserData');
		      udata.ax = get(fig, 'CurrentAxes');
		      udata.oldAxesUnits = get(udata.ax,'Units');
		      udata.oldNumberTitle = udata.curslice;
		      set(udata.ax,'Units','pixels');
		      udata.slices(udata.slices == cell_id) = 0;
		      udata.imagedata(udata.imagedata == cell_id) = 0;
		      display_data(fig, img, udata);
              end
% 	end
end
end