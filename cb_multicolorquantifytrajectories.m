% mtrx = cb_multicolorquantifytrajectories(fig, channels, brush_sz, tr)
% tr is a cell containing an array per trajectory.
function mtrx = cb_multicolorquantifytrajectories(fig, channels, brush_sz, tr)

mtrx = zeros(numel(tr), 2+numel(channels)); % nchannels + angle + length.
for il = 1:numel(tr)
    [mtrx(il, 1) mtrx(il, 2) tmp] = cb_multicolormeasuretrajectory(fig, tr{il}, brush_sz);
    mtrx(il, 1) = mtrx(il, 1) * 180. / pi;
    
    if ~ isempty(tmp) % tmp is empty, for instance, if there are no trajectories.
        for ic = 1:numel(channels)
            mtrx(il, ic + 2) = tmp(1, ic);
        end
    else % Intensities set to -1 if there are no trajectories.
        for ic = 1:numel(channels)
            mtrx(il, ic + 2) = -1;
        end
    end
    
    clear tmp;
end
end