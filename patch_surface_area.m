% PATCH_SURFACE_AREA Computes the surface area of a 3D surface.
%
%  PATCH_SURFACE_AREA(PTCH) returns the surface area of the object represented by
%  the triangulation PTCH. The units are given by the units used to expressed the
%  coordinates of the points in PTCH.
%
%  Rodrigo Fernandez-Gonzalez
%  fernanr1@mskcc.org
%  2007/08/18

function sa = patch_surface_area(ptch)

myp = ptch;
nf = myp.faces;
nv = myp.vertices;

if size(nf,2) ~= 3
    error('ptch_area expects to get triangular faces')
end

sa = 0.;

for i=1:size(nf,1)  % for each face
    v1 = nv(nf(i,1),:); % first vertex
    v2 = nv(nf(i,2),:);
    v3 = nv(nf(i,3),:);
    a=norm(v1-v2);  % length of first side
    b=norm(v1-v3);
    c=norm(v2-v3);
    p = (a + b + c)/2;  % superperimeter
    % A = sqrt(p*(p - a)*(p - b)*(p - c));   % Heron's formula for triangle area 
    
    % According to wikipedia, the following formula & computation sequence
    % can be used to prevent numerical instability in the evaluation - see
    % http://en.wikipedia.org/wiki/Heron's_formula#Numerical_stability
    vs = sort([a,b,c]);
    c1=vs(1);b1=vs(2);a1=vs(3);
    A1=(1/4)*sqrt((a1+(b1+c1))*(c1-(a1-b1))*(c1+(a1-b1))*(a1+(b1-c1)));
   
    sa = sa + A1; 
end
