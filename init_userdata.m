function ud2 = init_userdata(ud)

if (numel(ud.imsize) == 2)
    ud.imsize = cat(2, ud.imsize, 1);
end

ud.rbottom = ud.imsize(3) - 1;
ud.rBRUSH_SZ = 3;
ud.rCHANNEL = 1;
ud.rDRAW_ALL = 0;
ud.rSAVE_GEOM = 0;
ud.rNODESANDEDGES = 0;
ud.rLOAD_ANNTIME = 1;
ud.rchannels = [];
ud.coords = [];
ud.rcurrchannel = 0;
ud.rcurrdir = '.';
ud.rdelimiters = -1 .* ones(0, 3, ud.imsize(3)); % Multiple points per slice.
ud.rndelimiters = zeros(1, ud.imsize(3));
ud.rfiducials = -1 .* ones(0, 3, ud.imsize(3)); % Multiple points per slice.
ud.rnfiducials = zeros(1, ud.imsize(3));
ud.rfile = [];
ud.rfile2 = [];
ud.rflag_fiduciallabels = 0;
ud.rlcoords = [];
ud.rrcoords = [];
ud.rmcoords = [];
ud.rlns = [];
ud.robjects = [];
ud.rPIX_VALUE = [];
ud.rpix_val_mode = 1; % Modifier for the pick pixel value option. If 0, no repeated pixel values are stored. If 1, repetitions are allowed. If 2, when a value is repeated it is removed from the list.
ud.rpoly = [];
ud.rpolygons = cell(0, 0, ud.imsize(3));
ud.rtrajectories = cell(0, 0, ud.imsize(3));
ud.rntrajectories = zeros(1, ud.imsize(3));
ud.rang = [];
ud.rangles = cell(0, 0, ud.imsize(3));
ud.astar_handles = [1.5 1.5 2 7000]; % Default A* parameters.

ud.rres = [16.0/90 16.0/90 1.0];
ud.rselectedfiducials = zeros(1, 3); %[index1, index2, z]
ud.rtinycells = 0;
ud.rtmp = [];
ud.rtop = 0;
ud.runiquecoords = -1 .* ones(ud.imsize(3), 2); % A single point per slice.
ud.roi = [];
ud.bg = []; % Background area.
ud.globalstretch = 0; % To be able to use different pixel value mappings.
ud.paramtpo = -1;
ud.paramtpf = -1;
ud.paramang = 60.0;
ud.paramminl = 3.0;
ud.paramth = 0.0;
ud.parammethod = 4;
ud.paramuseold = 0;
ud.paramns = 1999;
ud.paramalpha = 0.05;
ud.paramshell = 1;
ud.paramrmax = 1;
ud.paramimopen = 1;
ud.paramimsave = 1;



if isfield(ud, 'paramgaussmindist')
    ud.parammindist = ud.parammindist; % Preserve the old value when loading a new image if this field already exists.
else
    ud.parammindist = 0.5;
end

if isfield(ud, 'paramgaussfactor')
    ud.paramgaussfactor = ud.paramgaussfactor; % Preserve the old value if this field already exists.
else
    ud.paramgaussfactor = 0.65;
end

if isfield(ud, 'paramclosing')
    ud.paramclosing = ud.paramclosing;
else
    ud.paramclosing = 64;
end

if isfield(ud, 'paramplotpoly_threeD')
    ud.paramplotpoly_threeD = ud.paramplotpoly_threeD;
else
    ud.paramplotpoly_threeD = 1;
end

if isfield(ud, 'paramplotpoly_axes')
    ud.paramplotpoly_axes = ud.paramplotpoly_axes;
else
    ud.paramplotpoly_axes = 0;
end

if isfield(ud, 'paramcorrelationws')
    ud.paramcorrelationws = ud.paramcorrelationws;
else
    ud.paramcorrelationws = [64 64];
end

% Parameters for gradient-based polarity measurements.
if isfield(ud, 'param_gradpol_bin_sz')
    ud.param_gradpol_bin_sz = ud.param_gradpol_bin_sz;
else
    ud.param_gradpol_bin_sz = 15;
end

if isfield(ud, 'param_gradpol_method')
    ud.param_gradpol_method = ud.param_gradpol_method;
else
    ud.param_gradpol_method = 'brightness'; % 'gradient' or 'brightness'.
end

if isfield(ud, 'param_gradpol_smoothing')
    ud.param_gradpol_smoothing = ud.param_gradpol_smoothing;
else
    ud.param_gradpol_smoothing = 3;
end

if isfield(ud, 'param_gradpol_kernel_sz')
    ud.param_gradpol_kernel_sz = ud.param_gradpol_kernel_sz;
else
    ud.param_gradpol_kernel_sz = 3; % 9??
end

if isfield(ud, 'param_gradpol_bg_pixorientation')
    ud.param_gradpol_bg_pixorientation = ud.param_gradpol_bg_pixorientation;
else
    ud.param_gradpol_bg_pixorientation = 1; % 1 no background, pixel orientation calculated for each channel independently; 2 background and pixel orientation calculated for each channel independently; 3 background and pixel orientation calculated using red channel; 4 background and pixel orientation calculated using green channel; 5 background and pixel orientation calculated using blue channel.
end

% Parameters for snake-based segmentation.
if isfield(ud, 'param_snakes_kappax') % Balloon force along the X axis.
    ud.param_snakes_kappax = ud.param_snakes_kappax;
else
    ud.param_snakes_kappax = 0.4;
end

if isfield(ud, 'param_snakes_kappay') % Balloon force along the Y axis.
    ud.param_snakes_kappay = ud.param_snakes_kappay;
else
    ud.param_snakes_kappay = 0.4;
end

if isfield(ud, 'param_snakes_beta') % Snake "smoothness".
    ud.param_snakes_beta = ud.param_snakes_beta;
else
    ud.param_snakes_beta = 5;
end

if isfield(ud, 'param_snakes_win_sz') % Number of iterations.
    ud.param_snakes_win_sz = ud.param_snakes_win_sz;
else
    ud.param_snakes_win_sz = 100;
end

if isfield(ud, 'param_snakes_areaTol') % Number of iterations.
    ud.param_snakes_areaTol = ud.param_snakes_areaTol;
else
    ud.param_snakes_areaTol = 25;
end

% END Parameters for snake-based segmentation.

if isfield(ud, 'trackedPolygon')
    ud.trackedPolygon = ud.trackedPolygon;
else
    ud.trackedPolygon = [];
end

if isfield(ud, 'trackDiv')
    ud.trackDiv = ud.trackDiv;
else
    ud.trackDiv = [];
end

if isfield(ud, 'curslice')
    ud.curslice = ud.curslice;
else
    ud.curslice = 0;
end

if ~isfield(ud, 'colmap')
    ud.colmap = '';
end

if ~isfield(ud, 'isdiv')
    ud.isdiv = 0;
else
    ud.isdiv = ud.isdiv;
end

if isfield(ud, 'param_semiauto_seeds_pixval') % Balloon force along the Y axis.
    ud.param_semiauto_seeds_pixval = ud.param_semiauto_seeds_pixval;
else
    ud.param_semiauto_seeds_pixval = 0;    
end

if isfield(ud, 'param_semiauto_seeds_objsize') % Balloon force along the Y axis.
    ud.param_semiauto_seeds_objsize = ud.param_semiauto_seeds_objsize;
else
    ud.param_semiauto_seeds_objsize = 50;
end

if isfield(ud, 'thetaLearn') % Default theta values for the 15 features used to detect dividing cells.
    ud.thetaLearn = ud.thetaLearn;
else
    ud.thetaLearn = [29.0105 2.5910 -60.6749 20.6315 17.5993 2.7543 4.3025 -0.4801 3.0601 0.1469 3.4567 6.0090 -0.7133 -3.9283 -0.6929];
end

ud2 = ud;
