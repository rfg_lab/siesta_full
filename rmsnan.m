%RMSNAN Mean, std and sem ignoring NaN elements.
%    [M S E] = RMSNAN(X,DIM) returns the mean, standard deviation
%    and standard error of the mean for matrix X computed along
%    dimension DIM (see MEAN for more info) ignoring elements of
%    the matrix set to the NaN value.  
%
% Rodrigo Fernandez-Gonzalez
% fernanr1@mskcc.org
% 2007/09/24

function varargout = rmsnan(vector, dim, output)

if nargin < 3
	output = 1;
end

if nargin < 2
	dim = 1;
end

if ~ isempty(vector)
	switch dim
		case 1
			m = zeros(1, size(vector, 2));
			s = zeros(1, size(vector, 2));
			e = zeros(1, size(vector, 2));
			
			for i = 1:size(vector, 2)
				ind = find(~ isnan(vector(:, i)));
				if ~ isempty(ind)
					m(i) = mean(vector(ind, i), dim);
					s(i) = std(vector(ind, i), 0, dim);
					e(i) = s(i) / sqrt(numel(ind));
				else
					m(i) = nan;
					s(i) = nan;
					e(i) = nan;
				end
			end
		
		case 2
			m = zeros(size(vector, 1), 1);
			s = zeros(size(vector, 1), 1);
			e = zeros(size(vector, 1), 1);
	
			for i = 1:size(vector, 1)
				ind = find(~ isnan(vector(i, :)));
				
				if ~ isempty(ind)
					m(i) = mean(vector(i, ind), dim);
					s(i) = std(vector(i, ind), 0, dim);
					e(i) = s(i) / sqrt(numel(ind));
				else
					m(i) = nan;
					s(i) = nan;
					e(i) = nan;
				end
			end
	
		case 3
			m = zeros(size(vector, 1), size(vector, 2));
			s = zeros(size(vector, 1), size(vector, 2));
			e = zeros(size(vector, 1), size(vector, 2));
			
			for i = 1:size(vector, 1)
				for j = 1:size(vector, 2)
					ind = find(~ isnan(vector(i, j, :)));
					
					if ~ isempty(ind)
						m(i, j) = mean(vector(i, j, ind), dim);
						s(i, j) = std(vector(i, j, ind), 0, dim);
						e(i, j) = s(i, j) / sqrt(numel(ind));
					else
						m(i, j) = nan;
						s(i, j) = nan;
						e(i, j) = nan;
					end
				end
			end
	end
else
	m = nan;
	s = nan;
	e = nan;
end
		

switch output
	case 1
		varargout{1} = m;
		varargout{2} = s;
		varargout{3} = e;
	case 2
		varargout{3} = m;
		varargout{1} = s;
		varargout{2} = e;
	case 3
		varargout{2} = m;
		varargout{3} = s;
		varargout{1} = e;
end
