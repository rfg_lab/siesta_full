% [x y] = pixinline_bresenham(coordinates, width)
% coordinates = [x1 y2; x2 y2]
% width should be an integer
%
% Constant perceived line thickness.
%
% Rodrigo Fernandez-Gonzalez

function [x y] = pixinline_bresenham(coordinates, width)

if (size(coordinates, 1) < 2 | size(coordinates, 2) < 2)
        error('coordinates = [x1 y2; x2 y2]');
end        

x1 = coordinates(1, 1);
x2 = coordinates(2, 1);
y1 = coordinates(1, 2);
y2 = coordinates(2, 2);

%  if (x1 < 0 | x2 < 0 | y1 < 0 | y2 < 0)
%          error('coordinates must be positive');
%  end        

%  if (x1 > x2)
%          x1 = x1 + 1;
%  else
%          x2 = x2 + 1;
%  end
%  
%  if (y1 > y2)
%          y1 = y1 + 1;
%  else
%          y2 = y2 + 1;
%  end

%  if size(coordinates, 2) == 3
%          z1 = coordinates(1, 3);
%          z2 = coordinates(2, 3);
%  else
%          z1 = 0;
%          z2 = 0;
%  end

hw = floor(width / 2.);

dx = abs(x2-x1);
dy = abs(y2-y1);

x = [];
y = [];

if (dx > dy)
        for ii = (-hw):hw
                y1new = y1 + ii;
                y2new = y2 + ii;
                [tmpx tmpy] = bresenham_line3d([x1 y1new 0], [x2 y2new 0]);
                x = [x tmpx];
                y = [y tmpy];
        end
else
        for ii = (-hw):hw
                x1new = x1 + ii;
                x2new = x2 + ii;
                [tmpx tmpy] = bresenham_line3d([x1new y1 0], [x2new y2 0]);
                x = [x tmpx];
                y = [y tmpy];
        end
end