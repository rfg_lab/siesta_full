function m = measuretri(in, grey, msr_list, obj_ids)

tic

vol_factor = 0.025; % Fraction of the volume of the smaller cell region (top, center or bottom) for a volume change to be significant.

for ii = 1:numel(msr_list)
        msr_list{ii} = lower(msr_list{ii});
end

% Measuring "cylindricity" requires measuring volume, surface area and height.
if ismember('cyl', msr_list) | ismember('hex', msr_list)
        if (~ismember('vol', msr_list)) & (~ ismember('volume', msr_list))
                msr_list{end+1} = 'vol';
        end
        
        if (~ismember('surf', msr_list)) & (~ ismember('surface_area', msr_list))
                msr_list{end+1} = 'surf';
        end
        
        if (~ismember('height', msr_list))
                msr_list{end+1} = 'height';
        end
end

if isempty(obj_ids)
	obj_ids = nonzeros(unique(double(in)));
end

if ~ iscell(msr_list)
	tmp = msr_list;
	msr_list = cell(1, 1);
	msr_list{1} = tmp;
end

nobjs = numel(obj_ids);
volume = zeros(1, nobjs);
surface_area = zeros(1, nobjs);
height = zeros(1, nobjs);
neighbors = zeros(1, nobjs);
cyl = zeros(1, nobjs);
hex = zeros(1, nobjs);
type_obj = zeros(1, nobjs);

for io = 1:nobjs	
	disp(cat(2, 'Measuring cell ', cat(2, num2str(io), cat(2, ' of ', num2str(nobjs)))));
	msk = in == obj_ids(io);
	ptch = isosurface(double(msk), 0);
        
	for im = 1:numel(msr_list)
		switch (msr_list{im})
			case {'volume'; 'vol'}
				volume(io) = patch_volume(ptch, [0 1 0]);
                        case {'surface_area', 'surf'}
                                surface_area(io) = patch_surface_area(ptch);
                        case {'height'}
                                height(io) = max(ptch.vertices(:, 3))-min(ptch.vertices(:, 3));
                        case {'neighbors'}
                                %neighbors(io) = nnz(rneighborsq(obj_ids(io), in, 0));
                                [tmp1 tmp2 tmp3 tmp4 tmp5 neighbors(io)] = extract_topology(in, 8, obj_ids(io));
                        case {'type'}
                                t = min(ptch.vertices(:, 3))-1; % Top slice (-1 because vertices are index starting at 1, while slices start at zero).
                                b = max(ptch.vertices(:, 3))-1; % Bottom
                                inc = floor((b-t)/3); % Minimum number of slices (minus 1) per block if we divide the cell in 3 blocks.
                                center_slices = t+inc:b-inc;
                                center_factor = inc/((b-inc)-(t+inc)); % Sometimes there will be extra slices in the center (e.g. if the cell encompasses eight sliceS). In that case, we take a proportion of the central region equal to the top and bottom regions. For the example with eight slices, the top is assigned slices 0,1 and 2, the bottom is assigned 5, 6 and 7 and the center is assigned 2/3 of the volume encompassed by slices 2, 3, 4 and 5.
                                ptch_t = isosurface(double(msk(:, :, t:t+inc)));
                                v_t = patch_volume(ptch_t, [0 1 0]);
                                ptch_b = isosurface(double(msk(:, :, b-inc:b)));
                                v_b = patch_volume(ptch_b, [0 1 0]);
                                ptch_c = isosurface(double(msk(:, :, center_slices)));
                                v_c = center_factor * patch_volume(ptch_c, [0 1 0]);
                        
                                % Now examine the volumes of top, bottom and center and decide on the shape of the object.
                                % A vol_factor (5%) change in volume between regions is necessary to consider that the volume changes between regions.
                                if v_t < v_c & (v_c - v_t) > vol_factor * v_t & (v_c < v_b | (v_c - v_b) < vol_factor * v_b)
                                        type_obj(io) = 1; % Upright pyramid.
                                elseif  v_b < v_c & (v_c - v_b) > vol_factor * v_b & (v_c < v_t | (v_c - v_t) < vol_factor * v_t)
                                        type_obj(io) = 2; % Inverted pyramid.
                                elseif v_t < v_c & (v_c - v_t) > vol_factor * v_t & v_b < v_c & (v_c - v_b) > vol_factor * v_b 
                                        type_obj(io) = 3; % Barrel.
                                elseif v_c < v_t & (v_t - v_c) > vol_factor * v_c & v_c < v_b & (v_t - v_b) > vol_factor * v_b
                                        type_obj(io) = 4; % Sandglass.
                                else
                                        type_obj(io) = 5; % Volume homogeneously distributed.
                                end

                        otherwise
                                % do later.
		end	
	end
end

if ismember('cyl', msr_list)
        cyl = 4 .* pi .* height .* volume ./ (surface_area .* surface_area); % I defined this to be 1 for cylinders and 0 for a flat plane (a sliced cylinder). It will be smaller than one for any prism whose bases are polygons.
end

if ismember('hex', msr_list)
        hex = 8 .* sqrt(3) .* height .* volume ./ (surface_area .* surface_area); % This is very similar to cylindricity, only defined to be 1 for prisms whose bases are regular hexagons; greater than one for prisms that have regular heptagons, octagons, etc as their bases, greatest for cylinders, and smaller than one for prims whose bases are regular pentagons, squares and triangles. Thus, this value can be used to estimate the number of neighbors of the cell if appropriately calibrated (what happens if bases are not regular, though?).
end

m = dip_measurement(obj_ids, 'Volume', volume, 'Surface area', surface_area, 'Height', height, 'Cylindricity', cyl, 'Hexagonality', hex, 'Neighbors', neighbors, 'Type', type_obj);

toc
