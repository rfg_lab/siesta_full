function varargout = dialog_quickguide(varargin)
% DIALOG_QUICKGUIDE M-file for dialog_quickguide.fig
%      H = DIALOG_QUICKGUIDE(possible_values, current_value) returns the handle to a new DIALOG_QUICKGUIDE
%	   containing one popup menu.




%      DIALOG_QUICKGUIDE, by itself, creates a new DIALOG_QUICKGUIDE or raises the existing
%      singleton*.
%
%      H = DIALOG_QUICKGUIDE returns the handle to a new DIALOG_QUICKGUIDE or the handle to
%      the existing singleton*.
%
%      DIALOG_QUICKGUIDE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DIALOG_QUICKGUIDE.M with the given input arguments.
%
%      DIALOG_QUICKGUIDE('Property','Value',...) creates a new DIALOG_QUICKGUIDE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before dialog_about_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to dialog_quickguide_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help dialog_quickguide

% Last Modified by GUIDE v2.5 27-Oct-2011 18:52:56

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @dialog_quickguide_OpeningFcn, ...
                   'gui_OutputFcn',  @dialog_quickguide_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before dialog_quickguide is made visible.
function dialog_quickguide_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to dialog_quickguide (see VARARGIN)

% Choose default command line output for dialog_quickguide
handles.output = hObject;

% License logo.
%  r = r ./ 255;
%  g = g ./ 255;
%  b = b ./ 255;
%  im = cat(3, r, g, b);
load(which('siesta_data.mat'), 'siestaim');
axes(handles.axes3);
imagesc(double(siestaim{1}));
axis off;

% Update handles structure.
guidata(hObject, handles);


% UIWAIT makes dialog_quickguide wait for user response (see UIRESUME)
uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = dialog_quickguide_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
%varargout{1} = handles.output;
%get(handles.figure1)
%uiwait(handles.figure1);

if isfield(handles, 'figure1')
    close(handles.figure1);
end
	
varargout{1} = 1;

% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%set(handles.figure1, 'Visible', 'off');
handles.closemethod = 1;
guidata(handles.figure1, handles);
uiresume(handles.figure1);
