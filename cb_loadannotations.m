function udata = cb_loadannotations(fig, line_color, filename)

udata = get(fig, 'UserData');
curr_dir = pwd;
if ~isempty(udata.rcurrdir), cd(udata.rcurrdir);
else cd('~'); end

if nargin < 3
    [filename, pathname, ext] = uigetfile('*.mat', 'Load annotations');
else
    [pathname, filename, ext] = fileparts(filename);
end
if (~ isequal(filename, 0))
    vars = load(fullfile(pathname, filename));
    
    % If the file does not contain a ud structure, exit.
    if ~ isfield(vars, 'ud')
        disp('Wrong annotations format.');
        return;
    end
    
    ud = vars.ud;
    
    %udata.rlns = [];
    
    if size(udata.rfiducials, 3) == size(ud.rfiducials, 3)
        udata.rfiducials = ud.rfiducials;
        udata.rnfiducials = ud.rnfiducials;
        udata.rlcoords = [];
        udata.rrcoords = [];
        udata.rmcoords = [];
        udata.rpoly = [];
        udata.rang = [];
        udata.rpolygons = ud.rpolygons;
        udata.rtmp = [];
        
        if isfield(ud, 'runiquecoords')
            udata.runiquecoords = ud.runiquecoords;
        else
            udata.runiquecoords = -1 .* ones(ud.imsize(3), 2);
        end
        
        if isfield(ud, 'rdelimiters')
            udata.rdelimiters = ud.rdelimiters;
            udata.rndelimiters = ud.rndelimiters;
        else
            udata.rdelimiters = -1 .* ones(0, 3, ud.imsize(3)); % Multiple points per slice.
            udata.rndelimiters = zeros(1, ud.imsize(3));
        end
        
        if isfield(ud, 'rangles')
            udata.rangles = ud.rangles;
        else
            udata.rangles = cell(0, 0, ud.imsize(3));
        end
        
        %michael
        if isfield(ud, 'trackDiv')
            udata.trackDiv = ud.trackDiv;
        else
            udata.trackDiv = [];
        end
        
        if isfield(ud, 'trackedPolygon')
            udata.trackedPolygon = ud.trackedPolygon;
        else
            udata.trackedPolygon = [];
        end
        %michael
        
        if isfield(ud, 'rtrajectories')
            udata.rtrajectories = ud.rtrajectories;
            udata.rntrajectories = ud.rntrajectories;
        else
            udata.rtrajectories = cell(0, 0, ud.imsize(3));
            udata.rntrajectories = zeros(1, ud.imsize(3));
        end
        
        udata.rcurrdir = pathname;
        
    elseif size(udata.rfiducials, 3) > size(ud.rfiducials, 3)
        udata.rfiducials(1:size(ud.rfiducials, 1), 1:size(ud.rfiducials, 2), 1:size(ud.rfiducials, 3)) = ud.rfiducials;
        udata.rnfiducials(1:numel(ud.rnfiducials)) = ud.rnfiducials;
        udata.rlcoords = [];
        udata.rrcoords = [];
        udata.rmcoords = [];
        udata.rpoly = [];
        udata.rpolygons(:, :, 1:ud.imsize(3)) = ud.rpolygons;
        udata.rtmp = [];
        
        if isfield(ud, 'runiquecoords')
            udata.runiquecoords(1:ud.imsize(3), :) = ud.runiquecoords;
        else
            udata.runiquecoords = -1 .* ones(udata.imsize(3), 2);
        end
        
        if isfield(ud, 'rdelimiters')
            udata.rdelimiters(1:size(ud.rdelimiters, 1), 1:size(ud.rdelimiters, 2), 1:size(ud.rdelimiters, 3)) = ud.rdelimiters;
            udata.rndelimiters(1:numel(ud.rndelimiters)) = ud.rndelimiters;
        else
            udata.rdelimiters = -1 .* ones(0, 3, udata.imsize(3)); % Multiple points per slice.
            udata.rndelimiters = zeros(1, udata.imsize(3));
        end
        
        if isfield(ud, 'rangles')
            udata.rangles = ud.rangles;
        else
            udata.rangles = cell(0, 0, ud.imsize(3));
        end
        
        %michael
        if isfield(ud, 'trackDiv')
            udata.trackDiv = ud.trackDiv;
        else
            udata.trackDiv = [];
        end
        
        if isfield(ud, 'trackedPolygon')
            udata.trackedPolygon = ud.trackedPolygon;
        else
            udata.trackedPolygon = [];
        end
        %michael
        
        
        
        if isfield(ud, 'rtrajectories')
            udata.rtrajectories = ud.rtrajectories;
            udata.rntrajectories = ud.rntrajectories;
        else
            udata.rtrajectories = cell(0, 0, ud.imsize(3));
            udata.rntrajectories = zeros(1, ud.imsize(3));
        end
        
        udata.rcurrdir = pathname;
    elseif size(udata.rfiducials, 3) < size(ud.rfiducials, 3)
        udata.rfiducials = ud.rfiducials(1:size(ud.rfiducials, 1), 1:size(ud.rfiducials, 2), 1:size(udata.rfiducials, 3));
        udata.rnfiducials = ud.rnfiducials(1:numel(udata.rnfiducials));
        udata.rlcoords = [];
        udata.rrcoords = [];
        udata.rmcoords = [];
        udata.rpoly = [];
        udata.rpolygons = ud.rpolygons(:, :, 1:udata.imsize(3));
        udata.rtmp = [];
        
        if isfield(ud, 'runiquecoords')
            udata.runiquecoords = ud.runiquecoords(1:udata.imsize(3), :);
        else
            udata.runiquecoords = -1 .* ones(udata.imsize(3), 2);
        end
        
        if isfield(ud, 'rdelimiters')
            udata.rdelimiters = ud.rdelimiters(1:size(udata.rdelimiters, 1), 1:size(udata.rdelimiters, 2), 1:size(udata.rdelimiters, 3));
            udata.rndelimiters = ud.rndelimiters(1:numel(udata.rndelimiters));
        else
            udata.rdelimiters = -1 .* ones(0, 3, udata.imsize(3)); % Multiple points per slice.
            udata.rndelimiters = zeros(1, udata.imsize(3));
        end
        
        if isfield(ud, 'rangles')
            udata.rangles = ud.rangles;
        else
            udata.rangles = cell(0, 0, ud.imsize(3));
        end
        
        %michael
        if isfield(ud, 'trackDiv')
            udata.trackDiv = ud.trackDiv;
        else
            udata.trackDiv = [];
        end
        
        if isfield(ud, 'trackedPolygon')
            udata.trackedPolygon = ud.trackedPolygon;
        else
            udata.trackedPolygon = [];
        end
        %michael
        
        if isfield(ud, 'rtrajectories')
            udata.rtrajectories = ud.rtrajectories;
            udata.rntrajectories = ud.rntrajectories;
        else
            udata.rtrajectories = cell(0, 0, ud.imsize(3));
            udata.rntrajectories = zeros(1, ud.imsize(3));
        end
        
        udata.rcurrdir = pathname;
    end
    
    % If line masks (old annotation format) present -> Convert to trajectories!!
    if isfield(vars, 'thelinemasks')
        thelinemasks = vars.thelinemasks;
        cd(curr_dir);
        udata.rlns = cb_clearlines(fig, udata.rlns);
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', udata);
        
        for ii = 1:numel(thelinemasks)
            thetr = [thelinemasks{ii}.XData' thelinemasks{ii}.YData' zeros(2, 1)];
            % Store in in the first empty spot.
            myind = 1;
            for i = 1:size(udata.rtrajectories, 1)
                if (isempty(udata.rtrajectories{i, 1, 1}))
                    udata.rtrajectories{i, 1, 1} = thetr;
                    udata.rntrajectories(1) = udata.rntrajectories(1) + 1;
                    break;
                end
                myind = myind + 1;
            end
            
            % If there were no empty spots, store at the end.
            if (myind > size(udata.rtrajectories, 1))
                udata.rtrajectories{end+1, 1, 1} = thetr;
                udata.rntrajectories(1) = udata.rntrajectories(1) + 1;
            end
            
            theind = myind;
        end
        
    end
end

cd(curr_dir);
end
