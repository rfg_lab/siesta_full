function [m pct_cellsrosettes] = measure2D(geometry, roi, min_l)

[rosette_celllist g] = findrosettes2D(geometry, 1, min_l, roi);
pct_cellsrosettes = 100 .* numel(unique(cell2mat(rosette_celllist'))) ./ numel(unique(g.nodecellmap(:, 1)));

n = geometry.nodes;
e = geometry.edges;

if isempty(roi)
	roi = [min(n(:, 2)) min(n(:, 1)); max(n(:, 2)), max(n(:, 1))]; 
end

ncm = geometry.nodecellmap;
ids = unique(ncm(:, 1));

% We cannot set a size for the measurements, as we do not know yet
% how many cells are inside the roi.
roi_ids = [];
topology = [];
topology_corrected = [];

% For every cell ...
for i = 1:numel(ids)
	% Find its vertices.
	idx = find(ncm(:, 1) == ids(i));
	vertices = ncm(idx, 2);
	
	% If all the vertices are inside the roi ...
	if nnz(inpolygon(n(vertices, 2), n(vertices, 1), [roi(1, 1); roi(1, 1); roi(2, 1); roi(2, 1)], [roi(1, 2); roi(2, 2); roi(2, 2); roi(1, 2)])) == numel(idx)
		% Measure cell properties and add them to the measurement list.
		roi_ids(end+1) = ids(i);	
		topology(end+1) = numel(idx);
		
		% Find the edges connecting the nodes to form the cell.
		ind_e = intersect(find(ismember(e(:, 1), vertices)), find(ismember(e(:, 2), vertices)));
		
		side_count = 0;
		
		for jj = 1:numel(ind_e)
			v = n(e(ind_e(jj), 1), :) - n(e(ind_e(jj), 2), :);
			l = norm(v, 2);
			if l > min_l
				side_count = side_count + 1;
			end
		end
				
		topology_corrected(end+1) = side_count;
	end
end

m = dip_measurement(roi_ids, 'NInterfaces', topology, cat(2, 'NInterfacesLT', num2str(min_l)), topology_corrected); 