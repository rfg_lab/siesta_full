% [populations, labeled_indices] = mrandomlabel(N, p, np, nl)
%
% Returns an array of "labelings". Each row contains the indices
% of the labeled positions for a certain "labeling".
%  
% N is the number of positions to choose from.
% NP is the number of positions to be labeled in each labeling.
% NL (optional) is the number of labelings to return.
%
% POPULATIONS is an NLxN matrix where each row contains a
% random "labeling" with NP positions in population P.
% LABELED_INDICES is an NLxNP matrix where each row
% contains the indices of the positions that belong to P
% in each one of the labeling contained in POPULATIONS.
%  
% Rodrigo Fernandez-Gonzalez
% fernanr1@mskcc.org
% 11/14/2006
