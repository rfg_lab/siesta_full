% outsideembryo = findbackground(im, th, min_sz)
%
% Find the background around a signal (e.g. embryo image).
%
% TH (optional) is the minimum intensity for the signal (bg is dimmer than this).
% MIN_SZ (optional) is the minimum size for a background area to be taken into consideration.
% INSIDE (optional) is 1 if the background needs to be measured in the cytoplasm and 0 if it should be measured outside the embryo (defaults to 0).
%
% Rodrigo Fernandez-Gonzalez
% 20080815

function outsideembryo = findbackground(im, th, min_sz, inside)

% Default parameter values.
if nargin < 2
        % Calculate image histogram.
        v = (0:5:max(im));
        n = histc(reshape(double(im), [1 numel(double(im))]), v);
        
        % Find histogram peaks.
        [tmp ind] = sort(n);
                
        % Use largest non-zero peak as threshold (substracting one bin to be conservative).
        v = v(ind);
        if ind(end) == 1
                th = v(end-1) - 5;       
        else
                th = v(end) - 5;                                       
        end                
end

if nargin < 3
	min_sz = 1500;
end

if nargin < 4
        inside = 0;
end        

if ~ inside
        % Threshold image (take only dim pixels), but not black ones, which are used to fill in after rotation of the image. Then remove holes in thresholded area.
        bgmask = fillholes(opening(im < th & im > 0)); % First erode, then dilate. This is fairly conservative in the pixels that are considered background.
        %bgmask = fillholes(closing(im < th & im > 0)); % First dilate, then erode; this is less conservative in the pixels that are considered background, and towards the edges some signal pixels may be erroneously classified as background.
else
        bgmask = erosion(im < th & im > 0, 3);
        min_sz = 0;                
end

% Label connected components.
l = label(bgmask);

% Take only big "chunks" of background.
try
        m = measure(l, [], {'size'}, [], 1, min_sz);
        
        % Create background image using only large enough background objects.
        if ~ isempty(m)
                outsideembryo = l == m.id(1);
        
                for ii = 2:numel(m.id)
                        outsideembryo = outsideembryo | (l == m.id(ii));
                end
                
                outsideembryo = fillholes(outsideembryo);
        else
                outsideembryo = dip_image(zeros(size(im, 2), size(im, 1)));
        end
catch
        fprintf('Measuring background in the cell cytoplasm (no region large enough found outside the embryo).\n');
        outsideembryo = erosion(im < th & im > 0, 3);
end        

