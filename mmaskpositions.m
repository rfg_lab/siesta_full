% [in_positions, in_edges] = mmaskpositions(positions, edges, border_coords)
% Returns the indices of the positions inside the mask, and the indices of 
% all the edges connecting non-mask positions or mask and non-mask
% positions, thus excluding edges connecting border positions.
% 
% POSITIONS is a matrix where each row represents the coordinates of a point.
% EDGES is a 2-column matrix where each row represents a connection between
% positions in the form [index_pos_1 index_pos_2], with index_pos_1 <
% index_pos_2.
% BORDER_COORDS is 2x2 coordinate where each row contains the (X Y) coordinates
% of the lower left and the upper right corners of the mask respectively.
% Coordinates are measured from the lower left corner of the image.
%
% IN_POSITIONS is a list of indices within positions pointing at those that are
% not at the border.
% IN_EDGES is a list of indices within edges pointing at those connecting
% non-border or border and non-border positions.
% Rodrigo Fernandez-Gonzalez
% fernanr1@mskcc.org
% 2007/09/19


function varargout = mmaskpositions(varargin)

% Default values for some parameters.
border_width = []; % All the image.

% Check parameters.
switch (nargin)
	case 2
		positions = varargin{1, 1};
		if (~ (isnumeric(positions) && prod(size(positions)) > 1 && size(positions, 3) == 1))
			error('??? First argument should be a 2D-matrix.');
		end

		edges = varargin{1, 2};
		if (~ (isnumeric(edges) && prod(size(edges)) > 1 && size(edges, 2) == 2 && size(edges, 3) == 1))
			error('??? Second argument should be a 2D-matrix with two columns.');
		end

	case 3
		positions = varargin{1, 1};
		if (~ (isnumeric(positions) && prod(size(positions)) > 1 && size(positions, 3) == 1))
			error('??? First argument should be a 2D-matrix.');
		end

		edges = varargin{1, 2};
		if (~ (isnumeric(edges) && prod(size(edges)) > 1 && size(edges, 2) == 2 && size(edges, 3) == 1))
			error('??? Second argument should be a 2D-matrix with two columns.');
		end

		border_width = varargin{1, 3};
		if (~ (isnumeric(border_width) && numel(border_width) == 4))
			error('??? Third argument should be 4-element matrix.');
		end

	otherwise
		error('??? Wrong number of input arguments.');			
		
end

% END check parameters.

%keep_indeces = find((positions(:, 1)>=0) & (positions(:, 2)>=0));

%positions = positions(keep_indeces, :);

xpol = [border_width(1, 1) border_width(2, 1) border_width(2, 1) border_width(1, 1)]';
ypol = [border_width(1, 2) border_width(1, 2) border_width(2, 2) border_width(2, 2)]';

inside = pnspolym(xpol, ypol, positions(:, 2), positions(:, 1));
% KEEP contains the positions of the positions to keep.
keepindices = find(inside);

% REMOVE contains the positions of the border positions.
%removeindices = find(~ inside);

% Now that the border positions are removed, I still want to preserve
% the edges between preserved and removed positions, between removed and
% preserved positions and between preserved and preserved positions.
edgeind = 1;

for i = 1 : size(edges, 1)
	% This condition takes all interfaces with at least one edge in the roi.
	%if (((~ isempty(find(keepindices == edges(i, 1)))) && (~ isempty(find(removeindices == edges(i, 2))))) || ((~ isempty(find(keepindices == edges(i, 2)))) && (~ isempty(find(removeindices == edges(i, 1))))) || ((~ isempty(find(keepindices == edges(i, 1)))) && (~ isempty(find(keepindices == edges(i, 2))))))
	% This condition takes only interfaces with both edges in the roi.
	if (((~ isempty(find(keepindices == edges(i, 1)))) && (~ isempty(find(keepindices == edges(i, 2))))))

		keepedges(edgeind) = i;
		edgeind = edgeind + 1;
	end
end

varargout{1, 1} = keepindices';
varargout{1, 2} = keepedges;      

return;
