% ang_deg = measure_angle(pnt1, pnt2, pnt3)
%
% where pnt1-3 are 1xdim arrays.
%
% Rodrigo Fernandez-Gonzalez
% 20080712
function ang_deg = measure_angle(pnt1, pnt2, pnt3)

% Check parameters.
if isempty(pnt1) | isempty(pnt2) | isempty(pnt3)
	ang_deg = -1;
	return;
end

% alpha = acos(dotprod(v21, v23)/(|v21|*|v23|)
v21 = pnt1 - pnt2;
v23 = pnt3 - pnt2;

cosalpha = sum(v21 .* v23) / (norm(v21, 2) * norm(v23, 2));
ang_deg = acos(cosalpha) * 180. / pi;
end
