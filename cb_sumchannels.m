% ud = cb_sumchannels(fig)
% This function called with no arguments returns the different types of projection available.
function ud = cb_sumchannels(fig)
ax = get(fig, 'CurrentAxes');
ax_children = get(ax, 'Children');
img = ax_children(end);
ud = get(fig, 'UserData');

im = ud.slices;

% Only color images can be summed with this function.
if isa(im, 'dip_image_array')
    im = stretch(im{1}+im{2}+im{3});
end

% All the rest should be sustitutable with cb_loadtimeseries(fig, [], im);.
im = cat(3, im, im);

ud.slices = im;
ud.imsize = size(ud.slices{1});
ud.curslice = 0;
%curr_dir = ud.rcurrdir;
%thezoom = ud.zoom;
%ud = init_userdata(ud);
%ud.rcurrdir = curr_dir;
%ud.mappingmode = 'lin';
%ud.colspace = '';
%ud.colordata = '';
%ud.zoom = thezoom;

ud.imagedata = ud.slices(:, :, 0);
ud = rmfield(ud, 'colordata');
ud.colspace = '';
ud.maxslice = ud.imsize(3) - 1;

if ~ isempty(ud.rchannels)
    ud.rchannels = [];
end

% Change image object size.
set(img, 'XData', [0 (ud.imsize(1)-1)], 'YData', [0 (ud.imsize(2)-1)]);

display_data(fig, img, ud);

diptruesize(fig, 100 * ud.zoom);

end