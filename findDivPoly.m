function [ud2]=findDivPoly(ud2,mwt,x,divSettings)

%first find cooresponding polygons for seeds
parentInd=ud2.trackDiv(2,x);
d1Ind=ud2.trackDiv(3,x);
d2Ind=ud2.trackDiv(4,x);

parentCoords=ud2.rfiducials(parentInd,:,mwt-1);
d1coords=ud2.rfiducials(d1Ind,:,mwt);
d2coords=ud2.rfiducials(d2Ind,:,mwt);

coords=[d1coords;d2coords];

%numPoly finds size of trackedPolygons
numPolyt=size(ud2.trackedPolygon(:,:,mwt-1,1),1);
numPolyr=size(ud2.rpolygons(:,:,mwt),1);


% go through each polygon in time mwt and see which of the 2 daughter
% cells match the coordinates
ud2.trackDiv(1,x,2)=mwt;
for y=1:max(numPolyt,numPolyr)
    if y>size(ud2.rpolygons(:,:,mwt),1)
        continue;
    end
    
    segment=ud2.rpolygons{y,:,mwt};
    
    if isempty(segment)
        continue;
    end
    
    in=inpolygon(coords(:,1),coords(:,2),segment(:,1),segment(:,2));
    
    if in(1)==1 %d1 coords polygon found
        ud2.trackedPolygon(numPolyt+1,:,mwt,1)=ud2.rpolygons(y,:,mwt);
        % Michael Quick
        %(:,:,:,2) are area
        %measures
        ud2.trackedPolygon{numPolyt+1,:,mwt,2}=nnz(poly2mask(ud2.trackedPolygon{numPolyt+1,:,mwt,1}(:,1),ud2.trackedPolygon{numPolyt+1,:,mwt,1}(:,2),1000,1000));
        %(:,:,:,3) are
        %circularity
        Geom=polygeom(ud2.trackedPolygon{numPolyt+1,:,mwt,1}(:,1),ud2.trackedPolygon{numPolyt+1,:,mwt,1}(:,2));
        ud2.trackedPolygon{numPolyt+1,:,mwt,3}=(Geom(4)*Geom(4))/(4*pi*Geom(1));
        %(:,:,:,4) are
        %distances
        ud2.trackedPolygon{numPolyt+1,:,mwt,4}=sqrt(((ud2.trackedPolygon{numPolyt+1,:,mwt,1}(:,1)-Geom(2)).^2)+((ud2.trackedPolygon{numPolyt+1,:,mwt,1}(:,2)-Geom(3)).^2));
        %(:,:,:,5) are dumbbell
        %checks
        ud2.trackedPolygon{numPolyt+1,:,mwt,5}=isDumbbell(ud2.trackedPolygon{numPolyt+1,:,mwt,4},divSettings(6));
        % record polygon division index
        ud2.trackDiv(3,x,2)=numPolyt+1;
    elseif in(2)==1 %d2 coords polygon found
        ud2.trackedPolygon(numPolyt+2,:,mwt,1)=ud2.rpolygons(y,:,mwt);
        % Michael Quick
        %(:,:,:,2) are area
        %measures
        ud2.trackedPolygon{numPolyt+1,:,mwt,2}=nnz(poly2mask(ud2.trackedPolygon{numPolyt+1,:,mwt,1}(:,1),ud2.trackedPolygon{numPolyt+1,:,mwt,1}(:,2),1000,1000));
        %(:,:,:,3) are
        %circularity
        Geom=polygeom(ud2.trackedPolygon{numPolyt+1,:,mwt,1}(:,1),ud2.trackedPolygon{numPolyt+1,:,mwt,1}(:,2));
        ud2.trackedPolygon{numPolyt+1,:,mwt,3}=(Geom(4)*Geom(4))/(4*pi*Geom(1));
        %(:,:,:,4) are
        %distances
        ud2.trackedPolygon{numPolyt+1,:,mwt,4}=sqrt(((ud2.trackedPolygon{numPolyt+1,:,mwt,1}(:,1)-Geom(2)).^2)+((ud2.trackedPolygon{numPolyt+1,:,mwt,1}(:,2)-Geom(3)).^2));
        %(:,:,:,5) are dumbbell
        %checks
        ud2.trackedPolygon{numPolyt+1,:,mwt,5}=isDumbbell(ud2.trackedPolygon{numPolyt+1,:,mwt,4},divSettings(6));
        ud2.trackDiv(4,x,2)=numPolyt+2;
    elseif in(1)==1 && in(2)==1 %% both seeds in 1 polygon???????
        ERROR
    end
    
end
% by now, both daughter seeds have cooresponding daughter polygons

% now find parent polygon index acoording to trackedPolygon(:,:,mwt)
numPolyParent=size(ud2.trackedPolygon(:,:,mwt-1),1);
for y=1:numPolyParent
    segment2=ud2.trackedPolygon{y,:,mwt-1};
    
    if isempty(segment2)
        continue;
    end
    
    in=inpolygon(parentCoords(:,1),parentCoords(:,2),segment2(:,1),segment2(:,2));
    
    if in==1 %d1 coords polygon found
        % record polygon division index
        ud2.trackDiv(2,x,2)=y;
        
    end
end


%find polygon angles

ind1=ud2.trackDiv(3,x,2);
ind2=ud2.trackDiv(4,x,2);

%check that both polygons have been found, if not, don't measure angles
if ind1~=0 && ind2~=0
    seg1=ud2.trackedPolygon{ind1,:,mwt,1};
    seg2=ud2.trackedPolygon{ind2,:,mwt,1};
    ud2.trackDiv(5,x,2)=measureDivAngAuto(seg1,seg2);
end

end