function [Xsnake Ysnake]=snakes(g,x0,y0,s0,sf,kappax,kappay,beta,win_size, sigmagradient, AreaTol)

%% Segmentation of wounds using active contours
% INPUTS
% g:            is a set of images of the type dip_image
% x0:           colum vector containing coordinates of the initialization curve within the horizontal axis
% y0:           colum vector coordinates of the inicialization curve within the vertical axis
% s0:           first slice to segement  
% sf:           last slice to segment 
% kappax:       balloon force coefficient, x axis;
% kappay:       balloon force coefficient, y axis;     
% beta:         parameter that control the smoothness of the snake. Bigger betas make the snake more smooth;
% win_size:     window size to compute optical flow;
% sigmagradient:    sigma of the gaussian used to compute the gradient. Has to increase with
% the mean intensity of the images
% AreaTol:      area tolerance to stop iterative process
%
%OUTPUTS
% Xsnake:       cell containing the x coordinates of the snakes for each
% time point
% Ysnake:       cell containing the y coordinates of the snakes for each
% time point
%
% Teresa Zulueta-Coarasa
% 2013

tic;

%%Other parameters

alpha = 3; %parameter that controls the lenght of the snake. Bigger alphas make the snake shrink ;
gamma =1; %step size to discretize the time parameter;


if s0>sf
    step=-1;
    numElements=(s0-sf)+1;
else
    step=1;
    numElements=(sf-s0)+1;
end





%% Step 1
% If the initial curves contain few points interpolate initial coordinates to obtain more points
p=size(x0,1);
x=[];
y=[];
if p<60 
    for kk=1:p
        DistSeg=0.5;
        if kk==p
             
        else 
            if x0(kk)==x0(kk+1)
   
                 if y0(kk)>y0(kk+1)
                    DistSeg=-DistSeg;
                end
                yi=y0(kk):DistSeg:y0(kk+1);
                xi = interp1(y0(kk:(kk+1)),x0(kk:(kk+1)),yi);
                
            else
                
                if x0(kk)>x0(kk+1)
                    DistSeg=-DistSeg;
                end
                xi=x0(kk):DistSeg:x0(kk+1);
                yi = interp1(x0(kk:(kk+1)),y0(kk:(kk+1)),xi);
            end
            
            x=[x;xi'];
            y=[y;yi'];
        end
    end
    
    
else
    x=x0;
    y=y0;
end



Xsnake=cell(numElements,1);
Ysnake=cell(numElements,1);
index=[];


cont=1;
wb = waitbar(0, 'Running MEDUSA ...', 'Color', 'w');
for ii=s0:step:sf
    if(~ishandle(wb)), break, end
    wb = waitbar((ii-min(s0,sf))/abs(sf-s0+1), wb, cat(2, 'Running MEDUSA in time point ', cat(2, num2str(ii-min(s0,sf)+1), cat(2, '/', num2str(abs(sf-s0+1))))), 'Color', 'w'); 
    img = g(:,:,ii);       

    %% Step 2
    % Apply optical flow to propagate the snake to next timepoints
    if s0>sf & ii~=s0
        try
            [Xflow Yflow Xpos Ypos] = rflow(g(:,:,(ii+1)),g(:,:,ii),[win_size win_size],[win_size win_size],0);
        catch err
            h1 = errordlg('The window size to compute cross-correlation is too small, please try to increase it','wrong window size')
            return
        end
            cx=sum(x)/size(x,1);   % Compute centroid of previous snake
            cy=sum(y)/size(y,1);
            dist=sqrt((Xpos-cx).^2+(Ypos-cy).^2);
            [xmind ixmind]=min(dist,[],1);
            [ymind iymind]=min(xmind,[],2);
            ixmind=ixmind(iymind);
            for jj=1:size(x,1)     %apply displacement to every point
                xx(jj,1)=x(jj)+Xflow(ixmind,iymind);
                yy(jj,1)=y(jj)+Yflow(ixmind,iymind);
            end
            x=xx;
            y=yy;
           
            
    elseif s0<sf & ii~=s0
     
            [Xflow Yflow Xpos Ypos] = rflow(g(:,:,(ii-1)),g(:,:,ii),[win_size win_size],[win_size win_size],0);
            cx=sum(x)/size(x,1);   % Compute centroid of previous snake
            cy=sum(y)/size(y,1);
            dist=sqrt((Xpos-cx).^2+(Ypos-cy).^2);
            [xmind ixmind]=min(dist,[],1);
            [ymind iymind]=min(xmind,[],2);
            ixmind=ixmind(iymind);
            for jj=1:size(x,1)     %apply displacement to every point
                xx(jj,1)=x(jj)+Xflow(ixmind,iymind);
                yy(jj,1)=y(jj)+Yflow(ixmind,iymind);
            end
            x=xx;
            y=yy;    
    end
    
    %% Step 3
    % Snakes implementation

    % Build matrix P that will be used to find the new coordinates (Kass, Witkin & Terzopoulos, 1988)
    N = length(x);
    a = gamma*(2*alpha+6*beta)+1;
    b = gamma*(-alpha-4*beta);
    c = gamma*beta;
    P = diag(repmat(a,1,N));                                % diagonal of a 1xN tiling of copies of a
    P = P + diag(repmat(b,1,N-1), 1) + diag(   b, -N+1);
    P = P + diag(repmat(b,1,N-1),-1) + diag(   b,  N-1);
    P = P + diag(repmat(c,1,N-2), 2) + diag([c,c],-N+2);
    P = P + diag(repmat(c,1,N-2),-2) + diag([c,c], N-2);

    % Invert Matrix
    P = inv(P);

    % Compute external force
    f = gradient(gradmag(img,sigmagradient));   % gradient magnitude operator detects the amplitude edges at which pixels change their gray-level suddenly.

    [maxx,maxy] = imsize(f);
    endSnakes=0;
    AreaSnakePrev=0;
    contadorError=0;
    while endSnakes~=1
       % % Get the value of the external force at the snake�s control points
       coords = [x,y];
       contadorError=contadorError+1;
%      
       try
       fex = get_subpixel(squeeze(f{1}),coords,'linear');
       catch err2
           h2 = errordlg('The balloon force is too high, please try to reduce it','wrong balloon force')
           return
       end
       try
       fey = get_subpixel(squeeze(f{2}),coords,'linear');
       catch err3
           h3 = errordlg('The balloon force is too high, please try to reduce it','wrong balloon force')
           return
       end
%        
       % Calculate balloon force. The ballon force is a unit vector normal
       % to the curve at each point multiplied by the amplitude of the
       % force
       
       if kappax~=0 | kappay~=0
          b = [coords(2:end,:);coords(1,:)] - [coords(end,:);coords(1:end-1,:)];  %Compute Derivative
          m = sqrt(sum(b.^2,2));                                                   %To normalize
          for aa=1:length(m)
              if m(aa)==0
                  m(aa)=1;
              end
          end
          bx =  b(:,2)./m;  % if we define dx=x2-x1 and dy=y2-y1, then the normals are (-dy, dx) and (dy, -dx).
          by = -b(:,1)./m;
          %
          %Compute a point of the director vector
          x_v=x+5*bx;
          y_v=y+5*by;
          %To visualize these vectors:
%           figure(1)
%           hold on
%           quiver(x,y,bx,by,'r')
%           for nn=1:size(x,1)
%             plot([x(nn) x_v(nn)],[y(nn) y_v(nn)],'c')
%           end
%           quiver(x,y,fex,fey,'y')
          %Compute if the vector points inside or outside the polygon
           x_v=x_v(1:20:end);
           y_v=y_v(1:20:end);
          IN = inpolygon(x_v,y_v,x,y) ;
          num_zeros=length(find(IN==0));
          num_ones=length(find(IN==1));
          if num_ones>num_zeros
              bx=-bx;
              by=-by;
          end
          %Add balloon force to external force
          fex = fex+kappax*bx;
          fey = fey+kappay*by;
%           %To visualize these vectors:
            %quiver(x,y,fex,fey,'g')
         end
      

       % Move control points
           
       x = P*(x+gamma*fex);
       y = P*(y+gamma*fey);
       
       % Measure area indide the curve to stop algorithm
       numimage=ii*ones(size(x));
       polygon{1,1}=[x,y,numimage];
       measurements= measurepoly(polygon, {'center'; 'area'; 'perimeter'}, g(:,:,ii), 1);
       AreaSnake=measurements.area;
       if abs(AreaSnake-AreaSnakePrev)<AreaTol
           endSnakes=1;
       end
       AreaSnakePrev=AreaSnake;
        
  
    end
 
     x=round(x);
     y=round(y);
     
    
   
     %% Step 4
     %Subsample the snake and apply the LiveWire
     Step=8;
     cpX=x(1:Step:end,:);
     cpY=y(1:Step:end,:);
     cp=[cpX,cpY,ii*ones(size(cpX,1),1)];
     PathLW=[];
      
     for ee=1:size(cp,1)
         if ee==size(cp,1)
             path = livewire(img, cp(ee,:,:), cp(1,:,:));
         else
             path = livewire(img, cp(ee,:,:), cp(ee+1,:,:));
         end
         PathLW=[PathLW;path];
     end
     x=PathLW(:,1);
     y=PathLW(:,2);
    
     Xsnake{cont,1}=[x];
     Ysnake{cont,1}=[y];
     cont=cont+1;
     
    
   
end
toc
if(ishandle(wb))
    waitbar(1, wb, 'Done!', 'Color', 'w');
    close(wb);
end



