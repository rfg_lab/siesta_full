# siesta_full
A SIESTA version with the latest functionality

Code provided AS-IS.
We are not responsible for bugs, errors, etc.
However, we welcome comments on bugs, errors, etc.
USE AT YOUR OWN RISK ;)

Execute by typing siesta at the Matlab prompt.

SIESTA has been developed and (somewhat) tested on this platform:

----------------------------------------------------------------------------------------------------
MATLAB Version: 8.4.0.150421 (R2014b)
MATLAB License Number: 676468
Operating System: Mac OS X  Version: 10.12.3 Build: 16D32 
Java Version: Java 1.7.0_55-b13 with Oracle Corporation Java HotSpot(TM) 64-Bit Server VM mixed mode
----------------------------------------------------------------------------------------------------
MATLAB                                                Version 8.4        (R2014b)
DIPimage, a scientific image analysis toolbox         Version 2.8                
DIPlib, a scientific image analysis library           Version 2.8                
Image Processing Toolbox                              Version 9.1        (R2014b)

Which means you need DIPImage (http://www.diplib.org/dipimage) and the Image Processing Toolbox (Mathworks)
to run all the functionality. This also means that if you are trying to run SIESTA on a more recent version
of Matlab (e.g. 2016b), you may run into trouble, because Mathworks introduced a number of changes that
affect back-compatibility. We are working on these, but it may take some time before we have a version of
SIESTA that works on all Matlab versions.

Or we may just rewrite the whole damned thing in Python ;)

Code by:

Teresa Zulueta-Coarasa

Michael Wang

Colin Tong

Angeline Yasodhara

and

Rodrigo Fernandez-Gonzalez

rodrigo.fernandez.gonzalez@utoronto.ca
2007-2017

