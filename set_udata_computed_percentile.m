function udata = set_udata_computed_percentile(udata,cdata)
if ~isfield(udata,'computed') | ~isfield(udata.computed,'percentile')
   if length(udata.imsize)==3 & udata.globalstretch
      if ~isempty(udata.colspace) & ~strcmp(udata.colspace,'RGB')
         warning('Cannot perform global stretching on non-RGB color images.')
         % Don't worry about this. It cannot happen.
      else
         cdata = udata.slices;
         if ~isempty(udata.colspace)
            perc = [inf,-inf];
            for ii=1:udata.channels
               tmp = mapcomplexdata(cdata{ii},udata.complexmapping);
               perc = [min(percentile(tmp,5),perc(1)),max(percentile(tmp,95),perc(2))];
            end
            return
         else
            cdata = mapcomplexdata(cdata,udata.complexmapping);
         end
      end
   end
   udata.computed.percentile = [percentile(cdata,5),percentile(cdata,95)];
end
end

