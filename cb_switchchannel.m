% *********************************************************************************
function cb_switchchannel(fig)
ax = get(fig, 'CurrentAxes');
ax_children = get(ax, 'Children');
img = ax_children(end);
ud = get(fig, 'UserData');

if ~ isempty(ud.rchannels)
    ud.rcurrchannel = ud.rcurrchannel + 1;
    if (ud.rcurrchannel == 4)
        ud.rcurrchannel = 0;
    end
    
    switch (ud.rcurrchannel)
        % Display each one of the channels.
        case {1; 2; 3}
            ud.slices = ud.rchannels{ud.rcurrchannel};
            ud.colordata = [];
            ud.imagedata = ud.slices(:, :, ud.curslice);
            ud.colspace = '';
            display_data(fig, img, ud);
            
            % Display the color image.
        case 0
            ud.slices = ud.rchannels;
            ud.colordata = ud.slices(:, :, ud.curslice);
            ud.imagedata = cat(3, ud.colordata{1}, ud.colordata{2}, ud.colordata{3});
            ud.colspace = 'RGB';
            display_data(fig, img, ud);
    end
else
    cb_decompose(fig);
end
end
