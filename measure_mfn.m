% [m, u, l, dt, pop_sims, mvalues_sims, mipr, mipr_sims] = 
%			measure_mfn(g, selected_edges, roi, r, alpha, ns, pershellanalysis)
%
% Measures the M function on a certain geometry using selected_edges as the population
% of interest. selected_edges contains indeces into the rows of g.edges.
%
% Rodrigo Fernandez-Gonzalez
% 2008/06/15
function [m, u, l, dt, pop_sims, mvalues_sims, mipr, mipr_sims] = measure_mfn(g, selected_edges, roi, r, alpha, ns, pershellanalysis, ang, min_edgelength, vis, fsimname, simulate_polarity)

% Initialize output parameters.
m = [];
u = [];
l = [];
dt = [];
pop_sims = [];
mvalues_sims = [];
mipr = [];
mipr_sims = [];

% Control input parameters.
if nargin < 3
	return;
end

if nargin < 4
	r = 1;
end

if nargin < 5
	alpha = 0.05;
end

if nargin < 6
	ns = 199;
end

if nargin < 7
	pershellanalysis = 1;
end

if nargin < 8
	ang = 0;
end

if nargin < 9
	min_edgelength = 3;
end

if nargin < 10
        vis = 0;
end

if nargin < 11 || isempty(fsimname) % This parameter is used to store images of the simulation distributions of positive edges.
        fsimname = datestr(now, 30);
end

if nargin < 12 % This parameter determines whether in the simulations only vertical interfaces (>= ang) are used or if all interfaces are used.
        simulate_polarity = 1;
end

% Obtain geometry from user data.
nodes = g.nodes;
edges = g.edges;

% Remove edges outside the ROI.
[newnodes newedges] = mmaskpositions(nodes, edges, roi);
edges = edges(newedges, :);

nn = size(nodes, 1);
ne = size(edges, 1);

% edge_cm == POSITIONS: use the center of mass of each one of the edges as the POSITIONS argument.
edge_cm = zeros(ne, 2);
edge_l = zeros(ne, 1);
edge_ang = zeros(ne, 1);

for ii = 1 : ne
	pair_coords = nodes(edges(ii, 1:2), :);
	edge_cm(ii, :) = (pair_coords(2, :) + pair_coords(1, :)) ./2;

	x = pair_coords(:, 2);
	y = pair_coords(:, 1);
	
	% First point of the segment is that with the smallest X value.
        if (x(1) < x(2))
      	 p1 = [x(1) y(1)];
	 p2 = [x(2) y(2)];
        else
         p2 = [x(1) y(1)];
	 p1 = [x(2) y(2)];
        end
      
        % Compute vector.
        v = p2 - p1;
        edge_l(ii) = norm(v, 2);
      
        % If the second point is under the first one (largest y)
        % then the angle is larger than pi/2.
        if (edge_l(ii) ~= 0 && p2(2) > p1(2))
         val = pi - asin(abs(v(2))/edge_l(ii));
        % Else the angle is smaller than pi/2.
        elseif (edge_l(ii) ~= 0)
         val = asin(abs(v(2)) / edge_l(ii));
        % Else where are trying to measure the angle formed by a dot!!
        else
      	 val = 0.0;
        end        
	edge_ang(ii) = val * 180 / pi;
end

% selected_edges point to the original index list, not the the list of edges within the ROI.
% Now we make the list point to edges within the ROI.
[tmp, selected_edges] = intersect(newedges, selected_edges);
edge_status = zeros(1, ne);
edge_status(selected_edges) = 1;

% Now extract neigh_graph. Here you are extracting information about neighborhood between edges.
neigh_graph = zeros(ne);

for ii = 1 : nn
	%disp(cat(2, 'Loop 2, iteration ', num2str(i)));
	a1 = find(edges(:, 1) == ii);
	a2 = find(edges(:, 2) == ii);
	a = union(a1, a2); % All these are the indices of edges_cm that should be made neighbors. UNION removes repeated elements.
	if (isempty(a) | numel(a) == 1) 
		continue;
	end

	combs = nchoosek(a, 2);

	neigh_graph(sub2ind(size(neigh_graph), combs(:, 1), combs(:, 2))) = 1;
	combs = circshift(combs, [0 1]);
	neigh_graph(sub2ind(size(neigh_graph), combs(:, 1), combs(:, 2))) = 1;

end

if simulate_polarity
        % Use ROI edges with certain orientation and length requirements in the simulations.
        scrambling_edges = zeros(1, ne);
        if ang >=0 
	        ids = find(edge_l >= min_edgelength & edge_ang >= ang & edge_ang <= (180-ang));
        else
	        ang = - ang;
	        ids = find(edge_l >= min_edgelength & (edge_ang <= ang | edge_ang >= (360-ang)));
        end

        scrambling_edges(ids) = 1;
else
        % Only use length requirements.
        scrambling_edges = zeros(1, ne);
        ids = find(edge_l > min_edgelength);
        
        scrambling_edges(ids) = 1;
end
        
fprintf('\nPercent of interfaces that can be used for the simulations: %d/%d=%.2f%%.\n', nnz(scrambling_edges), numel(scrambling_edges), 100 * nnz(scrambling_edges)/numel(scrambling_edges));
% Computing dt is actually much faster than the simulations, so I choose not to save it.
%if isfield(udata, 'rdt') & (~ isempty(udata.rdt{index}))
%	[m, u, l, dt, pop_sims, mvalues_sims, mipr, mipr_sims] = manalysisuni(edge_cm, edge_status, 1, r, dt, alpha, ns, 0, scrambling_edges, pershellanalysis);


[m, u, l, dt, pop_sims, mvalues_sims, mipr, mipr_sims] = manalysisuni(edge_cm, edge_status, 1, r, neigh_graph, 0, alpha, ns, 0, scrambling_edges, pershellanalysis);

% This saves an image for each one of the simulations.
if vis
   cwd = pwd;
   
   for ii = 1:size(mipr_sims, 3)
        ne = size(edges, 1);
        shed = pop_sims(ii, :);

  	fh = figure;
	set(fh, 'InvertHardCopy', 'off');
	set(fh, 'Color', [1 1 1]);
	%plot(nodes(:, 1), nodes(:, 2), 'ob');
	hold on;
	lh = zeros(1, 5);
	
	ne = size(edges, 1);
	for i = 1 : ne
	%disp(cat(2, 'Loop 1, iteration ', num2str(i)));
         pair_coords = nodes(edges(i, 1:2), :);
	 lh(1) = plot(pair_coords(:, 2), pair_coords(:, 1));
	 set(lh(1), 'Color', [0 0 1]);
	 if (pair_coords(1, 1) ~= pair_coords(2, 1) | pair_coords(1, 2) ~= pair_coords(2, 2))
		 lh(1) = plot(pair_coords(:, 2), pair_coords(:, 1));
		 set(lh(1), 'Color', [1 1 1]);
         end
	end
	
	set(gca, 'Color', 'k');
	
        for i = 1 : numel(shed)
  	 if (shed(i) == 1)
 		pair_coords = nodes(edges(i, 1:2), :);
	        if (pair_coords(1, 1) ~= pair_coords(2, 1) | pair_coords(1, 2) ~= pair_coords(2, 2))
		 lh(2) = plot(pair_coords(:, 2), pair_coords(:, 1));
		 set(lh(2), 'Color', [1 0 0]);
	        else
	 	 %lh(2) = plot(pair_coords(:, 2)+rand*2-1, pair_coords(:, 1)+rand*2-1, 'o');
		 %set(lh(2), 'Color', [1 0 0], 'MarkerSize', 5);
	        end
	        set(lh(2), 'LineWidth', 5);
 		 %lh(2) = plot(edge_cm(i, 2), edge_cm(i, 1), '*r');
		 %set(lh(2), 'MarkerSize', 2.5);
		 %t = text(edge_cm(i, 2), edge_cm(i, 1), num2str(round(edge_length(i)*10)/10.));
		 %t = text(edge_cm(i, 2), edge_cm(i, 1), num2str(round(len(frame_index, inv_edges_map(frame_index, i))*10)/10.));
		 %set(t, 'Color', [1 1 1]);
		 %set(t, 'FontSize', 5);
		 %elseif (edge_vertical(i) == 1)
 	 %	 lh(4) = plot(edge_cm(i, 2), edge_cm(i, 1), 'bh');
  	 end
        end
        fullfile(cwd, cat(2, 'sim_', cat(2, num2str(ii), cat(2, '_', fsimname))))
        saveas(fh, fullfile(cwd, cat(2, 'sim_', cat(2, num2str(ii), cat(2, '_', fsimname)))), 'fig');;
        close(fh);
   end
end

	
