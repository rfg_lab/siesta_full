% Creates a geometry where very short edges (< minnondedist)
% are removed and substituted by nodes. It correctes the nodecellmap as well.
%
% g = consolidategeometry2D(geometry, minnodedist)

function g = consolidategeometry2D(geometry, minnodedist)

%minnodedist = 5; % Minimum distance between nodes to consider them separated.

n = geometry.nodes;
e = geometry.edges;
cnm = geometry.nodecellmap;

% Now refine rosette search by finding pseudo high-order nodes formed by other, 
% very-close nodes.
% Distance between neighboring nodes.
dt = nan.*zeros(size(n, 1));
for ii = 1:size(e, 1)
	dt(e(ii, 1), e(ii, 2)) = norm(n(e(ii, 1), :)-n(e(ii, 2), :), 2);
end

% Find very close nodes, i.e., super-short edges.
node_distance_indeces = find((dt < minnodedist)); %nan < k == 0, nan > k == 0
tiny_edges = [];
[tiny_edges(:, 1) tiny_edges(:, 2)] = ind2sub(size(dt), node_distance_indeces);
tiny_edges = sort(tiny_edges, 2);

% Remove very short edges.
for ii = 1:size(tiny_edges, 1)
	% Add an intermediate node.
	n(end+1, :) = (n(tiny_edges(ii, 1), :)+n(tiny_edges(ii, 2), :)) ./ 2.;
	
	% Connect all nodes connected to either one of the deleted nodes to the new node.
	e(find((e == tiny_edges(ii, 1)) | (e == tiny_edges(ii, 2)))) = size(n, 1);
	
	% Substitute the deleted nodes in the cell map.
	cnm(find((cnm(:, 2) == tiny_edges(ii, 1)) | (cnm(:, 2) == tiny_edges(ii, 2))), 2) = size(n, 1);
end

e = sort(e, 2);

% Delete edges where both nodes are the same.
ind = find(e(:, 1) ~= e(:, 2));
e = e(ind, :);

% Delete nodes from the cell-node map when they 
% appear more than once for any given cell.
cell_ids = unique(cnm(:, 1));
new_cnm = [];
for ii = 1:numel(cell_ids)	
	cellnode_indeces = find(cnm(:, 1) == cell_ids(ii));
	cell_nodes_withrep = cnm(cellnode_indeces, 2);
	[cell_nodes] = unique(cell_nodes_withrep);
	thecell = ones(numel(cell_nodes), 2);
	thecell(:, 1) = cell_ids(ii);
	thecell(:, 2) = cell_nodes;	
	ind = size(new_cnm, 1) + 1;
	new_cnm(ind:(ind+numel(cell_nodes)-1), 1:2) = thecell;
end
cnm = new_cnm;

g.nodes = n;
g.edges = e;
g.nodecellmap = cnm;
