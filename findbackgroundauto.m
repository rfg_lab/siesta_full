% Works well in images with a binary pixel distribution where the background is a maximum.
% out = findbackgroundauto(im, minsize)
%
% out is a binary image.
% minsize, optional, is the minimum size of a background region measured with connectivity 1 (defaults to 500 pixels).
%
% Rodrigo Fernandez-Gonzalez
% fernanr1@mskcc.org

function out = findbackgroundauto(im, minsize)

if nargin < 2
        minsize = 500;
end        

% Calculate and smooth the image histogram.
h = diphist(im, [0 max(im)], max(im));
smoothh = smooth_curve(h, 20);
%smoothh = double(smooth(dip_image(h), 1)); % Adjusts the background more to the edge of the embryo, resulting in slightly
% more variable background levels.

L = extr(smoothh);

minima = find(L{2}) + 1; % +1 is necessary, since L{1}(1) corresponds to zero intensity, L{1}(2) to intensity 1, ...

th = minima(find(minima > 2));
th = th(1);

out = (im <= th);
out = label(out, 1, minsize);
out = (out > 0);
