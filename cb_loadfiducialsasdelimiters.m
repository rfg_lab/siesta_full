% *********************************************************************************
function udata = cb_loadfiducialsasdelimiters(fig)

radiustodeletefiducials = 5;
udata = get(fig, 'UserData');
curr_dir = pwd;
cd(udata.rcurrdir);

[filename, pathname, ext] = uigetfile('*.mat', 'Load fiducials as delimiters');
if (~ isequal(filename, 0))
	vars = load(cat(2, pathname, filename));
	ud = vars.ud;
	
	%udata.rlns = [];
	
	if size(udata.rdelimiters, 3) == size(ud.rfiducials, 3)
		udata.rdelimiters = ud.rfiducials;
		udata.rndelimiters = ud.rnfiducials;
		
		% Delete old fiducials that overlap with the new delimiters.
		for zz = 1:size(udata.rdelimiters, 3)
			for ii = 1:udata.rndelimiters(zz)
				pnt = udata.rdelimiters(ii, 1:2, zz);
				pnt = repmat(pnt, [size(udata.rfiducials, 1), 1]);
				
				d = pnt - udata.rfiducials(:, 1:2, zz);
				
				d = sqrt(sum(d .* d, 2)); 
				
				% Find very close by fiducials.
				theclosest = find(d < radiustodeletefiducials);
				
				% Delete the closest point.
				if ~ isempty(theclosest)
					[tmp index] = sort(theclosest);	
					udata.rnfiducials(zz) = udata.rnfiducials(zz) - 1;
					
					udata.rfiducials(theclosest(index(1)), :, zz) = [-1 -1 -1];
					
					
					
					%delete(obj);
					%[udata.rfiducials udata.rnfiducials] = cb_deletepoint(fig, udata.rfiducials, udata.rnfiducials);
				end
			end
		end
		
		udata.rcurrdir = pathname;
		
	elseif size(udata.rdelimiters, 3) > size(ud.rfiducials, 3)
		udata.rdelimiters(1:size(ud.rfiducials, 1), 1:size(ud.rfiducials, 2), 1:size(ud.rfiducials, 3)) = ud.rfiducials;
		udata.rndelimiters(1:numel(ud.rnfiducials)) = ud.rnfiducials;
		
		% Delete old fiducials that overlap with the new delimiters.
		for zz = 1:size(udata.rdelimiters, 3)
			for ii = 1:udata.rndelimiters(zz)
				pnt = udata.rdelimiters(ii, 1:2, zz);
				pnt = repmat(pnt, [size(udata.rfiducials, 1), 1]);
				
				d = pnt - udata.rfiducials(:, 1:2, zz);
				
				d = sqrt(sum(d .* d, 2)); 
				
				% Find very close by fiducials.
				theclosest = find(d < radiustodeletefiducials);
				
				% Delete the closest point.
				if ~ isempty(theclosest)
					[tmp index] = sort(theclosest);	
					udata.rnfiducials(zz) = udata.rnfiducials(zz) - 1;
					
					udata.rfiducials(theclosest(index(1)), :, zz) = [-1 -1 -1];
					
					
					
					%delete(obj);
					%[udata.rfiducials udata.rnfiducials] = cb_deletepoint(fig, udata.rfiducials, udata.rnfiducials);
				end
			end
		end
		
		udata.rcurrdir = pathname;
	end

	% if there are only two delimiters, add a unique marker in between them.
	for curslice = 1:size(ud.rfiducials, 3)
		if udata.rndelimiters(curslice) == 2
			coords = (udata.rdelimiters(1, :, curslice) + udata.rdelimiters(2, :, curslice)) / 2.;
			udata.runiquecoords(curslice, :) = coords(1:2);			
		end
	end
end

cd(curr_dir);
end