% str = rnum2str(num, str_length)
% Converts a number num into a string of length str_length:
% >> rnum2str(5, 10)
% 
% ans =
%
% 0000000005
%
%  Rodrigo Fernandez-Gonzalez
%  fernanr1@mskcc.org
%  2008/01/21



function str = rnum2str(num, str_len)

str = [];

for ii = 1 : (str_len-numel(num2str(num)))
	str = cat(2, str, '0');
end

str = cat(2, str, num2str(num));
