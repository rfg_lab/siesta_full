function varargout = dialog_astar(varargin)
% DIALOG_ASTAR MATLAB code for dialog_astar.fig
%      DIALOG_ASTAR, by itself, creates a new DIALOG_ASTAR or raises the existing
%      singleton*.
%
%      H = DIALOG_ASTAR returns the handle to a new DIALOG_ASTAR or the handle to
%      the existing singleton*.
%
%      DIALOG_ASTAR('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DIALOG_ASTAR.M with the given input arguments.
%
%      DIALOG_ASTAR('Property','Value',...) creates a new DIALOG_ASTAR or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before dialog_astar_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to dialog_astar_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help dialog_astar

% Last Modified by GUIDE v2.5 19-May-2017 13:53:07

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @dialog_astar_OpeningFcn, ...
                   'gui_OutputFcn',  @dialog_astar_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before dialog_astar is made visible.
function dialog_astar_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to dialog_astar (see VARARGIN)



% Choose default command line output for dialog_astar
handles.output = hObject;

set(handles.figure1, 'name', 'A*');
warning('off', 'all'); % This is necessary to remove a warning
					   % caused by a Matlab bug.

input = varargin{1};
a = input(1);
b = input(2);
c = input(3);
its = input(4);

handles.closemethod = -1;

set(handles.afield, 'String', num2str(a));
set(handles.bfield, 'String', num2str(b));
set(handles.cfield, 'String', num2str(c));
set(handles.dfield, 'String', num2str(its));

handles = guidata(hObject);
if(isOldMatlabGraphics())
    fplot(@(x) x^str2double(get(handles.bfield,'String')),[0 1],handles.bscale);
else
    fplot(handles.bscale,@(x) x^str2double(get(handles.bfield,'String')),[0 1]);
end

% Update handles structure
guidata(hObject, handles);


% UIWAIT makes dialog_astar wait for user response (see UIRESUME)
uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = dialog_astar_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure

try
    d = guidata(handles.figure1);
    if d.closemethod == 1
        a = eval(get(handles.afield, 'String'));
        b = eval(get(handles.bfield, 'String'));
        c = eval(get(handles.cfield, 'String'));
        its = eval(get(handles.dfield, 'String'));
    else
        a = 1.5;
        b = 1.5;
        c = 2;
        its = 7000;
    end
    
    close(handles.figure1);
    
catch
    a = 1.5;
    b = 1.5;
    c = 2;
    its = 7000;
end

varargout{1} = [a b c its];

function afield_Callback(hObject, eventdata, handles)
% hObject    handle to afield (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of afield as text
%        str2double(get(hObject,'String')) returns contents of afield as a double


% --- Executes during object creation, after setting all properties.
function afield_CreateFcn(hObject, eventdata, handles)
% hObject    handle to afield (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function bfield_Callback(hObject, eventdata, handles)
% hObject    handle to bfield (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of bfield as text
%        str2double(get(hObject,'String')) returns contents of bfield as a double
handles = guidata(hObject);
if(isOldMatlabGraphics())
    fplot(@(x) x^str2double(get(handles.bfield,'String')),[0 1],handles.bscale);
else
    fplot(handles.bscale,@(x) x^str2double(get(handles.bfield,'String')),[0 1]);
end
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function bfield_CreateFcn(hObject, eventdata, handles)
% hObject    handle to bfield (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function cfield_Callback(hObject, eventdata, handles)
% hObject    handle to cfield (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of cfield as text
%        str2double(get(hObject,'String')) returns contents of cfield as a double


% --- Executes during object creation, after setting all properties.
function cfield_CreateFcn(hObject, eventdata, handles)
% hObject    handle to cfield (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in closebutton.
function closebutton_Callback(hObject, eventdata, handles)
% hObject    handle to closebutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.closemethod = 1;
guidata(handles.figure1, handles);
uiresume(handles.figure1);


function dfield_Callback(hObject, eventdata, handles)
% hObject    handle to dfield (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of dfield as text
%        str2double(get(hObject,'String')) returns contents of dfield as a double


% --- Executes during object creation, after setting all properties.
function dfield_CreateFcn(hObject, eventdata, handles)
% hObject    handle to dfield (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
