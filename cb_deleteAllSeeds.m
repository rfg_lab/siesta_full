% *********************************************************************************
% Deletes ALL the seeds in all frames and re-numbers based on
% trackedPolygon
%MIchael
function udata = cb_deleteAllSeeds(fig)
udata = get(fig, 'UserData');

udata.rfiducials= -1 .* ones(size(udata.trackedPolygon, 1), size(udata.rfiducials, 2),udata.imsize(3));

for ind = 1:udata.imsize(3)
    udata.rnfiducials(ind) = 0;
end



mwt=find((cellfun('isempty',udata.rpolygons(1,:,:))==0),1,'first');
%mwt has first nonempty index of ud.trackedPolygon

% go through all time points
for i=mwt:size(udata.trackedPolygon(:,:,:,1),3)
    
    %find non empty index of polygons
    nonEmptyInd=find(~cellfun(@isempty,udata.trackedPolygon(:,:,i,1)));
    % go through each non empty index
    for j=1:size(nonEmptyInd,1)
        if ~isempty(udata.trackedPolygon{nonEmptyInd(j),:,i,1})
            polygon=udata.trackedPolygon{nonEmptyInd(j),:,i,1};
            [~,cx,cy]=polycenter(polygon(:,1),polygon(:,2));
            udata.rfiducials(nonEmptyInd(j),:,i)=[cx,cy,i];
        end
    end
end


%set seeds here

set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
set(fig, 'UserData', udata);

end
%Michael