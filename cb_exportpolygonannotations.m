% *********************************************************************************
% ASSUMES THE FIDUCIALS HAVE BEEN TRACKED.
function cb_exportpolygonannotations(ud, index)

if index == 0
    fprintf('Click on a fiducial to export the corresponding polygon.\n');
    return;
end
    
fprintf('Exporting annotations for polygon %d ... ', index);
curr_dir = pwd;
cd(ud.rcurrdir);

%[filename, pathname, ext] = uiputfile('*.mat', 'Export polygon annotations');
filename = cat(2, 'annotations_cell', rnum2str(index, 3), '.mat');
pathname = ud.rcurrdir;
ext = 1;

ud2 = ud; % Backup.
ud = init_userdata(ud2);

if (~ isequal(filename, 0))
                % Do not store the images in ud.
                sl = ud.slices;
                ud.slices = [];
                if isfield(ud, 'imagedata')
                        imd = ud.imagedata;
                        ud.imagedata = [];
                else
                        cold = ud.colordata;
                        ud.colordata = [];
                end
                
                % Now check, in every time point, which polygon contains the selected fiducial (of course, this assumes the fiducials have been tracked!).
                if numel(ud.imsize) >= 3
                        max_slice = ud.imsize(3)-1;
                else
                        max_slice = 0;
                end
                
                % For every time point.
                for ii = 0:max_slice
                        ud.rpolygons{1, 1, ii+1} = [];
                        
                        p = ud2.rfiducials(index, 1:2, ii+1);
                        
                        % If the fiducial is not defined at a certain time point, continue ...    
                        if p(1) ~= -1
                                % Find the polygon that contains the point.
                                for jj = 1:numel(ud2.rpolygons(:, :, ii+1))
                                        thepoly = ud2.rpolygons{jj, 1, ii+1};
                                        
                                        % When you find it, store it in the new annotation structure for this time point.
                                        if ~isempty(thepoly) && inpolygon(p(1), p(2), thepoly(:, 1), thepoly(:, 2))
                                                ud.rpolygons{1, 1, ii+1} = thepoly;
                                                break;
                                        end
                                end
                        end
                end
                                
                % This variable is neccesary in the annotation file for historical reasons.
                thelinemasks = {};
                save(cat(2, pathname, filename), 'ud', 'thelinemasks');
                ud.rcurrdir = pathname;
                
                ud = ud2;                
end

cd(curr_dir);
fprintf('done!\n');
end