% dt = mdistancetable(positions, neigh_graph, weighted)
% [dt, neigh_graph] = mdistancetable(positions, graph_type, weighted)
%
% Computes the minimum distance between two positions within neigh_graph.
% If neigh_graph is not given, it is computed based on the graph type
% requested.
% 
% POSITIONS is a matrix where each row represents the coordinates of a point.
% NEIGH_GRAPH (optional) is a matrix where (i, j) = 1 indicates that positions 
% i and j are immediate neighbors.
% GRAPH_TYPE (optional) is a string that determines the type of neighborhood 
% graph to be built if neigh_graph is not given: 'euclidean', 'delaunay', 
% 'rng', 'refrng', 'knn'.
% WEIGHTED (optional) determines whether to use number of edges (0) or edge 
% lengths to measure distances.
% DT is a matrix where element (i, j) contains the minimum distance between
% positions i and j in neigh_graph.
%
% Rodrigo Fernandez-Gonzalez
% fernanr1@mskcc.org
% 11/08/2006

function varargout = mdistancetable(varargin)

% Default values for some parameters. This will be modified in a second
% if provided.
graph_type = 'euclidean';
weighted = 0;

% Check parameters.
switch (nargin)
	case 1
		positions = varargin{1, 1};
		if (~ (isnumeric(positions) && prod(size(positions)) > 1 && size(positions, 3) == 1))
			error('??? First argument should be a matrix.');
		end
		% GENERATE NEIGH_GRAPH IN HERE!!!!!!	
		
	case 2
		positions = varargin{1, 1};
		if (~ (isnumeric(positions) && prod(size(positions)) > 1 && size(positions, 3) == 1))
			error('??? First argument should be a matrix.');
		end	
		
		if (isstr(varargin{1, 2}))
			graph_type = varargin{1, 2};
			% GENERATE NEIGH_GRAPH IN HERE!!!!!!
		elseif (isnumeric(varargin{1, 2}) && prod(size(varargin{1, 2})) > 1)
			neigh_graph = varargin{1, 2};
		elseif (isnumeric(varargin{1, 2}) && prod(size(varargin{1, 2})) == 1)
			weighted = varargin{1, 2};
			% GENERATE NEIGH_GRAPH IN HERE!!!!!!
		else
			error('??? Second argument should be a matrix or a string.');
		end
	case 3
		positions = varargin{1, 1};
		if (~ (isnumeric(positions) && prod(size(positions)) > 1 && size(positions, 3) == 1))
			error('??? First argument should be a matrix.');
		end	

		weighted = varargin{1, 3};
		if (~ (isnumeric(weighted) && prod(size(weighted)) == 1))
			error('??? Third argument should be a number.');
		end

		if (isstr(varargin{1, 2}))
			graph_type = varargin{1, 2};
			% GENERATE NEIGH_GRAPH IN HERE!!!!!!
		elseif (isnumeric(varargin{1, 2}) && prod(size(varargin{1, 2})) > 1)
			neigh_graph = varargin{1, 2};
		else
			error('??? Second argument should be a matrix or a string.');
		end
		
	otherwise
		error('??? Wrong number of input arguments.');			
end

varargout{1, 1} = mfloyd(positions, neigh_graph, weighted);
varargout{1, 2} = neigh_graph;

return;
