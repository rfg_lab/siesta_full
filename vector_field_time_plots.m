% Called by vector_field_time_analysis.
% cut_t is relative to size(dr_um, 1).
function vector_field_time_plots(dr_um, dth_um, cut_p1_um, cut_p1_rot_angle_deg, title_label, save_flag, cut_t)

bgcolor = [0.8 0.8 0.8];

drd_m = nan .* zeros(size(dr_um, 1), 5); % One row per time point, one column per distance range.
drd_e = nan .* zeros(size(dr_um, 1), 5); % Error.
dthd_m = nan .* zeros(size(dr_um, 1), 5); % One row per time point, one column per distance range.
dthd_e = nan .* zeros(size(dr_um, 1), 5); % Error.
dtha_m = nan .* zeros(size(dr_um, 1), 12); % One row per time point, one column per distance range.
dtha_e = nan .* zeros(size(dr_um, 1), 12); % Error.
dra_m = nan .* zeros(size(dr_um, 1), 12); % One row per time point, one column per distance range.
dra_e = nan .* zeros(size(dr_um, 1), 12); % Error.

% dr_um and dth_um can now be subgrouped on the basis of the
% distance from the vertex to the cut given by cut_p1_um
% (0-4um, 4-8, ..., 16-20) or their angle with respect to the cut 
% given by cut_p1_rot_angle (0-30deg, ..., 330-360).
d04 = find(cut_p1_um(cut_t, :) >= 0 & cut_p1_um(cut_t, :) < 4);
d48 = find(cut_p1_um(cut_t, :) >= 4 & cut_p1_um(cut_t, :) < 8);
d812 = find(cut_p1_um(cut_t, :) >= 8 & cut_p1_um(cut_t, :) < 12);
d1216 = find(cut_p1_um(cut_t, :) >= 12 & cut_p1_um(cut_t, :) < 16);
d1620 = find(cut_p1_um(cut_t, :) >= 16 & cut_p1_um(cut_t, :) < 20);
cm = [1 0 0; 1 0.5 0; 0 1 0; 0 1 1; 0 0 1]; % Color map for plots.

a030 = find(cut_p1_rot_angle_deg(cut_t, :) >= 0 & cut_p1_rot_angle_deg(cut_t, :) < 30);
a3060 = find(cut_p1_rot_angle_deg(cut_t, :) >= 30 & cut_p1_rot_angle_deg(cut_t, :) < 60);
a6090 = find(cut_p1_rot_angle_deg(cut_t, :) >= 60 & cut_p1_rot_angle_deg(cut_t, :) < 90);
a90120 = find(cut_p1_rot_angle_deg(cut_t, :) >= 90 & cut_p1_rot_angle_deg(cut_t, :) < 120);
a120150 = find(cut_p1_rot_angle_deg(cut_t, :) >= 120 & cut_p1_rot_angle_deg(cut_t, :) < 150);
a150180 = find(cut_p1_rot_angle_deg(cut_t, :) >= 150 & cut_p1_rot_angle_deg(cut_t, :) < 180);
a180210 = find(cut_p1_rot_angle_deg(cut_t, :) >= 180 & cut_p1_rot_angle_deg(cut_t, :) < 210);
a210240 = find(cut_p1_rot_angle_deg(cut_t, :) >= 210 & cut_p1_rot_angle_deg(cut_t, :) < 240);
a240270 = find(cut_p1_rot_angle_deg(cut_t, :) >= 240 & cut_p1_rot_angle_deg(cut_t, :) < 270);
a270300 = find(cut_p1_rot_angle_deg(cut_t, :) >= 270 & cut_p1_rot_angle_deg(cut_t, :) < 300);
a300330 = find(cut_p1_rot_angle_deg(cut_t, :) >= 300 & cut_p1_rot_angle_deg(cut_t, :) < 330);
a330360 = find(cut_p1_rot_angle_deg(cut_t, :) >= 330 & cut_p1_rot_angle_deg(cut_t, :) < 360);

for ii = 1:size(dr_um, 1)
	drd_m(ii, :) = [mean(dr_um(ii, d04)) mean(dr_um(ii, d48)) mean(dr_um(ii, d812)) mean(dr_um(ii, d1216)) mean(dr_um(ii, d1620))];
	
	drd_e(ii, :) = [std(dr_um(ii, d04)) std(dr_um(ii, d48)) std(dr_um(ii, d812)) std(dr_um(ii, d1216)) std(dr_um(ii, d1620))];
	
	dthd_m(ii, :) = [mean(dth_um(ii, d04)) mean(dth_um(ii, d48)) mean(dth_um(ii, d812)) mean(dth_um(ii, d1216)) mean(dth_um(ii, d1620))];
	
	dthd_e(ii, :) = [std(dth_um(ii, d04)) std(dth_um(ii, d48)) std(dth_um(ii, d812)) std(dth_um(ii, d1216)) std(dth_um(ii, d1620))];
	
	dra_m(ii, :) = [mean(dr_um(ii, a030)) mean(dr_um(ii, a3060)) mean(dr_um(ii, a6090)) mean(dr_um(ii, a90120)) mean(dr_um(ii, a120150)) mean(dr_um(ii, a150180)) mean(dr_um(ii, a180210)) mean(dr_um(ii, a210240)) mean(dr_um(ii, a240270)) mean(dr_um(ii, a270300)) mean(dr_um(ii, a300330)) mean(dr_um(ii, a330360))];
	
	dra_e(ii, :) = [std(dr_um(ii, a030)) std(dr_um(ii, a3060)) std(dr_um(ii, a6090)) std(dr_um(ii, a90120)) std(dr_um(ii, a120150)) std(dr_um(ii, a150180)) std(dr_um(ii, a180210)) std(dr_um(ii, a210240)) std(dr_um(ii, a240270)) std(dr_um(ii, a270300)) std(dr_um(ii, a300330)) std(dr_um(ii, a330360))];
	
	dtha_m(ii, :) = [mean(dth_um(ii, a030)) mean(dth_um(ii, a3060)) mean(dth_um(ii, a6090)) mean(dth_um(ii, a90120)) mean(dth_um(ii, a120150)) mean(dth_um(ii, a150180)) mean(dth_um(ii, a180210)) mean(dth_um(ii, a210240)) mean(dth_um(ii, a240270)) mean(dth_um(ii, a270300)) mean(dth_um(ii, a300330)) mean(dth_um(ii, a330360))];
	
	dtha_e(ii, :) = [std(dth_um(ii, a030)) std(dth_um(ii, a3060)) std(dth_um(ii, a6090)) std(dth_um(ii, a90120)) std(dth_um(ii, a120150)) std(dth_um(ii, a150180)) std(dth_um(ii, a180210)) std(dth_um(ii, a210240)) std(dth_um(ii, a240270)) std(dth_um(ii, a270300)) std(dth_um(ii, a300330)) std(dth_um(ii, a330360))];
end


% PLOTS
% dr vs time (coded by distance) --------------------------------------------------------------
t_res = 5; % Make PARAMETER.
ntp = size(drd_m, 1);
t = ((1:ntp)-cut_t).*t_res;

ncolors = size(drd_m, 2); % As many colors as different columns -distances- there are in dr.
thecolormap = jet(ncolors);
thecolormap = thecolormap(ncolors:-1:1, :);

figure;

title_label = '';

markers = 'ox+*s';

for ii = 1:size(drd_m, 2)
	h = plot(t, drd_m(:, ii));
	set(h, 'Color', thecolormap(ii, :), 'LineStyle', '-', 'LineWidth', 2, 'Marker', markers(ii));
	%set(h, 'Color', 'k', 'LineStyle', '-.', 'LineWidth', 2);
	hold on;
end

xlim([-40 70]);
ylimits = [-0.5 3]; 
ylim(ylimits);
set(gca, 'LineWidth', 2);
set(gca, 'XTick', [-60:15:300], 'YTick', [-10:0.25:50], 'XGrid', 'off', 'YGrid', 'on', 'FontSize', 16);
xlabel('Time after the cut (seconds)', 'FontSize', 16);
ylabel('Mean {\itd_r} (\mum)', 'FontSize', 16);
h = legend({'[0-4) \mum'; '[4-8) \mum'; '[8-12) \mum'; '[12-16) \mum'; '[16-20) \mum';}, 'Location', 'Best');
%set(h, 'Color', bgcolor);
title(title_label);

if (save_flag)
	savefig(cat(2, 'dr_distance_time', rstrtrim(title_label)), gcf, 'jpeg');
end


% dr vs time (coded by angle) --------------------------------------------------------------
t_res = 5; % Make PARAMETER.
ntp = size(dra_m, 1);
t = ((1:ntp)-cut_t).*t_res;

ncolors = size(dra_m, 2); % As many colors as different columns -distances- there are in dr.
thecolormap = jet(ncolors);
thecolormap = thecolormap(ncolors:-1:1, :);

figure;

title_label = '';

markers = 'ox+*sdv^<>ph.';

for ii = 1:size(dra_m, 2)
	h = plot(t, dra_m(:, ii));
	set(h, 'Color', thecolormap(ii, :), 'LineStyle', '-', 'LineWidth', 2, 'Marker', markers(ii));
	%set(h, 'Color', 'k', 'LineStyle', '-.', 'LineWidth', 2);
	hold on;
end

xlim([-40 70]);
ylimits = [-0.5 3]; 
ylim(ylimits);
set(gca, 'LineWidth', 2);
set(gca, 'XTick', [-60:15:300], 'YTick', [-10:0.25:50], 'XGrid', 'off', 'YGrid', 'on', 'FontSize', 16);
xlabel('Time after the cut (seconds)', 'FontSize', 16);
ylabel('Mean {\itd_r} (\mum)', 'FontSize', 16);
h = legend({'[0-30)°'; '[30-60)°'; '[60-90)°'; '[90-120)°'; '[120-150)°'; '[150-180)°'; '[180-210)°'; '[210-240)°'; '[240-270)°'; '[270-300)°'; '[300-330)°'; '[330-360)°'}, 'Location', 'Best');
%set(h, 'Color', bgcolor);
title(title_label);

if (save_flag)
	savefig(cat(2, 'dr_angle_time', rstrtrim(title_label)), gcf, 'jpeg');
end
