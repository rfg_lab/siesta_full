function[polygon]=isItDividing2(trackDiv,dividingCell,areaCon,cirConMax,cirConMin)
% e.g. trackDiv(2,:,:)
% e.g. dividingCell(2,:,1)
% trackDiv is one ROW in trackDiv
% dividingCell is one ROW in dividing cell (that means 1 segmentation)
% tDiv is trackDiv t
% t is SIESTA time
% areaCon determines the ratio between max area and min area
% cirCon determines the ratio between max circularity and min circularity


% find non empty index
[seg,time]=find(~cellfun('isempty',dividingCell(:,:,1)));

nonEmptyInd=[seg,time];

polygon=cell(size(dividingCell(:,:,1)));

polygon(1,:)=dividingCell(1,:,1);



% go through each non empty index
for i=1:size(nonEmptyInd,1)
    test=1;
    test2=2;
    test3=3;
    test4=4;
    if nonEmptyInd(i,1)==1
        continue
    end
    
    % segTimeInfo is 2 pieces of information
    segTimeInfo=nonEmptyInd(i,:);
    area=cell2mat(trackDiv(segTimeInfo(1),1:segTimeInfo(2),2));
    circle=cell2mat(trackDiv(segTimeInfo(1),1:segTimeInfo(2),3));
        
    if size(area,2)>9
%         max(area)
        if max(area)/min(area)>areaCon
           
            %should also check max index is greater than min index
            if size(circle,2)>9
                [minValue,~]=min(circle);
                [maxValue,~]=max(circle);
%                 maxValue=circle(end);
                if minValue<cirConMin && maxValue>cirConMax
                    polygon(nonEmptyInd(i,1),nonEmptyInd(i,2),1)=dividingCell(nonEmptyInd(i,1),nonEmptyInd(i,2));
                    
                    % add time info
                    timeofDiv=dividingCell(1,nonEmptyInd(i,2));
                    polygon(nonEmptyInd(i,1),nonEmptyInd(i,2),2)=timeofDiv;
                end
            end
        end
    end
end