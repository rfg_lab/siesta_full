function [angle]=measureDivAngAuto(cell1Seg,cell2Seg)
    [~,mid1x,mid1y]=polycenter(cell1Seg(:,1),cell1Seg(:,2));
    [~,mid2x,mid2y]=polycenter(cell2Seg(:,1),cell2Seg(:,2));
    
    angle=atan((mid1y-mid2y)/(mid1x-mid2x));    
end            
