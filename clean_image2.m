%out = clean_image2(in)
%
%  Remove shot noise from a 3D image by multiplying groups of three
%  consecutive slices.
%  
%  IN is an input image.
%
%  OUT is the output image.
%  
%  Rodrigo Fernandez-Gonzalez
%  fernanr1@mskcc.org
%  06/14/2007

function out = clean_image2(in)

in_dbl = double(in);
in_prv = circshift(in_dbl, [0 0 1]);
in_nxt = circshift(in_dbl, [0 0 -1]);

in_prv(:, :, 1) = (in_dbl(:, :, 1) + in_nxt(:, :, 1)) ./ 2;
in_nxt(:, :, end) = (in_dbl(:, :, end) + in_prv(:, :, end)) ./ 2;

tmp = in_prv .* in_dbl .* in_nxt;
themin = min(min(min(tmp)));
themax = max(max(max(tmp)));
out_dbl = round(((tmp - themin).*(255-0)./(themax - themin))+0);

out = dip_image(out_dbl, 'uint8');
