% *********************************************************************************
function [fidu2 nfidu2] = cb_consolidatefiducials(fidu)

fidu2 = [];
nfidu2 = [];

for ii = 1:size(fidu, 3)
	ind = 1;
	for jj = 1:size(fidu, 1)
		if fidu(jj, 1, ii) > -1 && fidu(jj, 2, ii) > -1
			fidu2(ind, :, ii) = fidu(jj, :, ii);
			ind = ind + 1;
		end
	end
	
	fidu2(ind:size(fidu, 1), :, ii) = -1 .* ones(numel(ind:size(fidu, 1)), size(fidu,2));
	nfidu2(ii) = ind - 1;
end
end