function show_coord_pix(fig)
ch = get(fig, 'Children');
coord = findobj(ch, 'Tag', 'coord');
ax = findobj(ch, 'Tag', 'axes2');
if strncmp(get(coord, 'Visible'), 'on', 2)
    cur_point = get(ax, 'CurrentPoint');
    if cur_point(1,1) > 0 && cur_point(1,2) > 0
        try
            img = findobj(get(ax, 'Children'), 'Type', 'image');
            udata = get(fig, 'UserData');
            [ud, cdata_scaled, cdata] = display_data(fig, img, udata);
            
            x = round(cur_point(1,1));
            y = round(cur_point(1,2));
            
            pix = cb_getpixvalue(fig, [x, y]);
            set(coord, 'String', ...
                ['Coordinates: (', num2str(round(cur_point(1,1))), ...
                ' , ', num2str(round(cur_point(1,2))), ') ', ...
                'Pixel value: ', num2str(pix), ' ']);
        catch
        end
    end
end