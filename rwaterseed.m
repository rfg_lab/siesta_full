% Old version of waterseed, uses dip_growregions rather than dip_seededwatershed.
% Works with the newest version of dip_growregions, which takes one fewer parameter than the former one.
%WATERSEED   Watershed initialized with a seed image
%
% WATERSEED performs a watershed on the image GREY_IMAGE, starting with
% the seeds in the labelled SEED_IMAGE. The labelled regions are grown
% by addressing their neighbors (defined by CONNECTIVITY) in the order
% of the grey-values in GREY_IMAGE, lower first.
%
% SYNOPSIS:
%  image_out = watershed(seed_image,grey_image,connectivity)
%
% PARAMETERS:
%  connectivity: defines the metric.
%  connectivity: defines which pixels are considered neighbours: up to
%     'connectivity' coordinates can differ by maximally 1. Thus:
%     * A connectivity of 1 indicates 4-connected neighbours in a 2D image
%       and 6-connected in 3D.
%     * A connectivity of 2 indicates 8-connected neighbourhood in 2D, and
%       18 in 3D.
%     * A connectivity of 3 indicates a 26-connected neighbourhood in 3D.
%     Connectivity can never be larger than the image dimensionality.
%
% DEFAULTS:
%  connectivity = 1
%
% NOTE:
%  For large images (with more than 1 million pixels) a different sorting
%  technique is used, which is much quicker but does not produce good
%  results if the images has plateaus (regions with constant grey-value).
%  Feel free to tweak this function or use the low-level function
%  DIP_GROWREGIONS if this is a problem for you.
%
% SEE ALSO:
%  watershed, dip_growregions

% (C) Copyright 2004-2005               Pattern Recognition Group
%     All rights reserved               Faculty of Applied Physics
%                                       Delft University of Technology
%                                       Lorentzweg 1
%                                       2628 CJ Delft
%                                       The Netherlands
%
% Cris Luengo, February 2005.

function image_out = rwaterseed(varargin)

% Rodrigo added the roi parameter.
d = struct('menu','Segmentation',...
           'display','Watershed with seeds',...
           'inparams',struct('name',       {'seed_image','grey_image','connectivity', 'roi'},...
                             'description',{'Seed image','Grey image','Connectivity', 'Mask'},...
                             'type',       {'image',     'image',      'array',       'image'},...
                             'dim_check',  {0,           0,            0,             0},...
                             'range_check',{[],          [],           'N+',          []},...
                             'required',   {1,           1,            0,             0},...
                             'default',    {'a',         'b',          1,             'c'}...
                            ),...
           'outparams',struct('name',{'image_out'},...
                              'description',{'Output image'},...
                              'type',{'image'}...
                              )...
          );
if nargin == 1
   s = varargin{1};
   if ischar(s) & strcmp(s,'DIP_GetParamList')
      image_out = d;
      return
   end
end
try
   [seed_image,grey_image,connectivity, roi] = getparams(d,varargin{:});
catch
   if ~isempty(paramerror)
      error(paramerror)
   else
      error(firsterr)
   end
end

% These changes by Rodrigo.
image_out = dip_growregions(seed_image,grey_image,roi,connectivity,0,'low_first');
%  
%  if prod(size(seed_image))>1e6
%     % for large images, we prefer to use the heap, which is much quicker
%     image_out = dip_growregions(seed_image,grey_image,[],connectivity,0,'low_first','heapsort');
%  else
%     % for small images, we prefer to use the stack, which addresses
%     % pixels in a more rational order
%     image_out = dip_growregions(seed_image,grey_image,[],connectivity,0,'low_first','insertionsort');
%  end
