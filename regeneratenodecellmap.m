%cellnodemap = regeneratenodecellmap(geometry, roi) Assigns nodes to cells.
%
% roi is in coordinates ([x1 y1; x2 y2]) and optional.
%
% Rodrigo Fernandez-Gonzalez
% fernanr1@mskcc.org
% 20080220

function cellnodemap = regeneratenodecellmap(geometry, roi)

% Copy the geometry onto an image and mask with the roi.
out = geometry2image(geometry);
labeled_entire = label(~out, 1);

% roi is optional.
if nargin < 2 | isempty(roi)
	roi = [0+5+1 0+5+1; (size(out, 1)-5) (size(out, 2)-5)];
end

roi_im = dip_image(zeros(size(labeled_entire, 2), size(labeled_entire, 1)), 'bin');
roi_im((roi(1, 1)-5-1):(roi(2, 1)+5-1), (roi(1, 2)-5-1):(roi(2, 2)+5-1)) = 1;
l = labeled_entire .* roi_im;
cell_ids = nonzeros(unique(double(l)));

% Edges will be used to make sure that only nodes connected to any other node are considered.
% In that case, they will appear at least once in the edge list.
nodes = geometry.nodes;
edges = unique(reshape(geometry.edges, [1, numel(geometry.edges)]));

% Make an image where only the valid nodes are included.
nodes_im = dip_image(zeros(size(l, 2), size(l, 1)), 'uint16');
for ii = 1:size(nodes, 1)
	if (~ isempty(find(edges == ii))) & nodes(ii, 1) < size(nodes_im, 2) & nodes(ii, 2) < size(nodes_im, 1)
		nodes_im(nodes(ii, 2), nodes(ii, 1)) = ii;
	end
end

cellnodemap = [];
nnodes = [];
jj = 0;
% For every cell ...
for ii = 1:numel(cell_ids)
	% Create a mask slightly expanding the cell body.
	cell_im = dilation(labeled_entire == cell_ids(ii), 7, 'elliptic');
	
	% Ignore border cells.
	pixels = double(roi_im(cell_im));
	
	if ~ isempty(find(pixels == 0))
		continue;
	end
	
	% Check nodes under the mask.
	node_inds = nonzeros(double(nodes_im(cell_im)));
	
	% Add [cell node_index] combinations for each node_index.
	for kk = 1:numel(node_inds)
		jj = jj + 1;
		cellnodemap(jj, 1:2) = [cell_ids(ii) node_inds(kk)];
	end
end
