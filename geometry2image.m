% VERY SLOW: out = geometry2image(geometry, <roi>, <image_size>)

function out = geometry2image(geometry, roi, sz)

if nargin < 3
	sz = [];
end

if nargin < 2
	roi = [];
end

nodes = geometry.nodes;
edges = geometry.edges;

if ~ isempty(roi)
	[tmp newe] = mmaskpositions(nodes, edges, roi);
	edges = edges(newe, :);
end

if isempty(sz)
	out = dip_image(zeros(circshift(max(nodes)+10, [0 0])), 'bin');
else
	out = dip_image(zeros(sz(2), sz(1)), 'bin');
end

for i = 1:size(edges, 1)
	p1 = nodes(edges(i, 1), :);
	p1 = circshift(p1, [0 1]);
	p2 = nodes(edges(i, 2), :);
	p2 = circshift(p2, [0 1]);
	out = drawline(out, p1(1), p1(2), p2(1), p2(2), 1);
end
