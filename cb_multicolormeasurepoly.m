% [int sz] = cb_multicolormeasurepoly(fig, thepoly, channels)
function [int sz] = cb_multicolormeasurepoly(fig, thepoly, channels)

int = 0;
udata = get(fig, 'UserData');

% Intensity.
mask = trajectories2mask({thepoly}, udata.imsize, 1, 2);
sz = nnz(double(mask));

%Make function cb_showmask:
%overlay(udata.colordata{1}, mask);

if ~ isempty(udata.colordata)
    int = zeros(1, numel(channels));
    for ic = 1:numel(channels)
        channel = channels(ic);
        if (~ isempty(udata.colordata{channel}(mask)))
            int(ic) = mean(udata.colordata{channel}(mask));
            % If the image where intensities should be measured is empty, we are probably
            % measuring the intensity of a point.
        else
            tmp = cb_getpixvalue(fig);
            int(ic) = tmp(channel);
        end
    end
else
    int = zeros(1, 3);
    if (~ isempty(udata.imagedata(mask)))
        if isempty(udata.rcurrchannel) | (udata.rcurrchannel == 0)
            udata.rcurrchannel = (1:3);
        end
        int(udata.rcurrchannel) = mean(udata.imagedata(mask));
        % If the image where intensities should be measured is empty, we are probably
        % measuring the intensity of a point.
    else
        int(udata.rcurrchannel) = cb_getpixvalue(fig);
    end
end
% END INTENSITY

end
