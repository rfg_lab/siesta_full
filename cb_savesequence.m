% cancel = cb_savesequence(fig)
% cancel is 1 if the operation was canceled, 0 otherwise.
function cancel = cb_savesequence(fig)

extensions_images = {'*.tif'; '*.ics'; '*.*'};

ud = get(fig, 'UserData');

extensions_order = circshift(1:numel(extensions_images), [0 -1*(ud.paramimsave-1)]);
extensions_images = extensions_images(extensions_order);

curr_dir = pwd;
cd(ud.rcurrdir);

[filename, pathname, ext] = uiputfile(extensions_images, 'Export file sequence ...');
iext = find(filename == '.');

if ~isempty(iext)
	iext = iext(end);
else
	iext = numel(filename) + 1;
end

if (~ isequal(filename, 0))
        ext = extensions_order(ext);
	
        switch ext
		case 1
			format = 'TIFF';
		case 2
			format = 'ICSv2';
		otherwise
			format = 'TIFF';
	end
	
        firstslice = dip_array(squeeze(ud.slices(:, :, 0))); % dip_array returns the array of data without casting (in its original form).
        if isa(firstslice(1, 1), 'double') | max(ud.slices) > 255
                cast_fn = @uint16; % This prevents writing a 64bit tiff.
        else
                cast_fn = @uint8;
        end
        
	for ii = 1:(ud.maxslice+1) % Use maxslice instead of imsize to control saving of "pseudo 3D images".
		theslice = cast_fn(squeeze(ud.slices(:, :, ii-1)));
		writeim(theslice, cat(2, cat(2, cat(2, pathname, filename(1:(iext-1))), rnum2str(ii-1, 4)), filename(iext:end)), format, 'no');
	end
        ud.paramimsave = ext;
        set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
        set(fig, 'UserData', ud);
	cancel = 0;		
else
	cancel = 1;
end

cd(curr_dir);
end