%sc = smooth_curve(curve, n)
%
%  Smoothens the outline of a curve by averaging over a certain
%  number of data points.
%  
%  CURVE contains the Y value for each one of the curve points.
%  N is the number of data points to average over.
%
%  SC is the smooth curve.
%  
%  Rodrigo Fernandez-Gonzalez
%  fernanr1@mskcc.org
%  06/14/2007
%  Added control for single element CURVE -> 20130218.

function sc = smooth_curve(curve, n)

if numel(curve) == 1
    sc = curve;
    return;
end

if (mod(n, 2) ~= 0)
	n = n - 1;
end

l = numel(curve);
curve = reshape(curve, [1, l]);


c = zeros(n+1, l);
mid = (n/2) + 1;
c(mid, :) = curve;

for ii = 1:(n/2)
	cprev = circshift(curve, [0 ii]);
	cprev(1:ii) = curve(1:ii);

	cnext = circshift(curve, [0 (-1*ii)]);
	cnext((l-ii+1):l) = curve((l-ii+1):l);
	
	c(mid-ii, :) = cprev;
	c(mid+ii, :) = cnext;	
end

sc = rmsnan(c, 1);
sc(find(isnan(curve))) = nan;