% Author: Michael Wang
% Date: August 28, 2014
% 
% The following function provides the area information for polygons given in the x-y coordinate system.
% 
% INPUT:
% polygon1 and polygon2 are 2 column vector variables containing x and y coordinates of the polygon of interest
% 
% OUTPUT:
% area - provides the overlaping area of the input polygons
% area1 - provides the area of polygon1
% area2 - provides the area of polygon2

function [area,area1,area2]=polytest(polygon1,polygon2)

% converts polygonal regions of interest into mask
% output of poly2mask is logical
im_poly1 = poly2mask(polygon1(:,1),polygon1(:,2),1000,1000);
im_poly2 = poly2mask(polygon2(:,1),polygon2(:,2),1000,1000);

% finding the total number of logicals in each mask provides the areas
area=nnz((im_poly1.*im_poly2));
area1=nnz((im_poly1));
area2=nnz((im_poly2));

end