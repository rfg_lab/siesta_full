% **************************************************************************
% tr2 = cb_equalizetrajectories(fig)
% Changes the length of all trajectories so that they are "almost" equal (coordinates are integers,
% so some variation occur). The length used is that of the shortest trajectory.
function tr2 = cb_equalizetrajectories(fig)

l = 0;
udata = get(fig, 'UserData');

tr2 = cell(1, 1, udata.imsize(3));

tp_list = 1:numel(udata.rntrajectories);

% Find shortest trajectory.
nl = inf;
for ii = 1:numel(tp_list)
    thetp = tp_list(ii);
    trajectories = udata.rtrajectories(:, :, thetp);
    
    for jj = 1:udata.rntrajectories(thetp)
        tr = trajectories{jj};
        
        [ang l] = cb_multicolormeasuretrajectory(fig, tr, 1, thetp - 1);
        
        if l < nl
            nl = l;
        end
    end
end

% Make all trajectories same length.
for ii = 1:numel(tp_list)
    thetp = tp_list(ii);
    trajectories = udata.rtrajectories(:, :, thetp);
    
    for jj = 1:udata.rntrajectories(thetp)
        tr = trajectories{jj};
        trnew = cb_changetrajectorylength(tr, nl);
        tr2{jj, :, ii} = trnew;
    end
end

end