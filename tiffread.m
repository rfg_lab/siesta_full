% Read in one image file
% In case it's a multi-page TIFF file, read in all the images in it.
%
% out = tiffread(fname,col, range)
% Modified by Rodrigo to read muti-page color TIFF files.
function out = tiffread(fname,col, range)

M = dipio_imagefilegetinfo(fname, [], 1);

if nargin < 2
	col = 0;
end

if nargin < 3
	range = [0 M.numberOfImages-1];
else
	range = sort(range);
end

if ~col
   tmp = dipio_imagereadtiff(fname,range(1));
   
   if ndims(M.size) == 3
   	tmp = squeeze(tmp(:, :, end));
   end
   
   tmp = colorspace(tmp,'grey');
   colsp = colorspace(tmp);
   
   out = newim([size(tmp, 1) size(tmp, 2) (range(2) - range(1) + 1)],datatype(tmp));
   if ~isempty(colsp)
      out = colorspace(out,colsp);
   end
else
   tmp = dipio_imagereadtiff(fname,range(1));
   tmp = joinchannels('RGB', tmp(:, :, 0), tmp(:, :, 1), tmp(:, :, 2)); 
   out = newimar(size(tmp));
   colsp = colorspace(tmp);
   for ii = 1:prod(size(tmp))
      out{ii} = newim([size(tmp{ii}, 1) size(tmp{ii}, 2) (range(2) - range(1) + 1)],datatype(tmp{ii}));
   end
   
   if ~isempty(colsp)
      out = colorspace(out,colsp);
   end
end

if (range(2) - range(1)) == 0
   out = tmp;
else
   out(:,:,0) = tmp;
   for ii=1:(range(2)-range(1))
      tmp = dipio_imagereadtiff(fname,range(1)+ii); % If the file is not a TIFF, numberOfImages is always 1 -- unless we add new file types!
      if ~col
  	 if ndims(M.size) == 3
   	    tmp = squeeze(tmp(:, :, end));
   	 end
         tmp = colorspace(tmp,'grey');
      else
         tmp = joinchannels('RGB', tmp(:, :, 0), tmp(:, :, 1), tmp(:, :, 2));
      end
      out(:,:,ii) = tmp;
   end
end

if col
	
end
