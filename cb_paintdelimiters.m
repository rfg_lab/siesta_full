function cb_paintdelimiters(fig, fidu, fiducial_sz, fiducial_color)

cb_erasedelimiters(fig);

ud = get(fig, 'UserData');
curslice = ud.curslice + 1;

for i = 1 : size(fidu, 1)
	coords = fidu(i, 1:2, curslice);
	
	if sum(coords == [-1 -1]) ~= 2
		cb_drawpoint(fig, coords, fiducial_sz, fiducial_color, 'Delimiter');
	end
end
end