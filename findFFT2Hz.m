function out=findFFT2Hz(dist)%,colour) % find frequency information for 1 to 10 hz

Fs=length(dist);
T=1/Fs;
L=length(dist);
t=(0:L-1)*T;
Y=fft(dist);
P2 = abs(Y/L);
P1 = P2(1:L/2+1);
P1(2:end-1) = 2*P1(2:end-1);
f = Fs*(0:(L/2))/L;
% plot(f,P1,colour,'linewidth',2)

if length(P1)>10
out=P1(2:11); % 1 to 10 hz info
else
    P1(end+1:11)=0;
    out=P1(2:end);
end