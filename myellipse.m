% [yrot xrot ylength xlength] = myellipse(params)
%
% The output of fitellipse can provide the parameter params.

function [yrot xrot ylength xlength] = myellipse(params)
cx = params(1); % Ellipse center [cx cy]
cy = params(2);
rx = params(3); % Ellipse radii [rx ry]
ry = params(4); 
theta_rad = params(5); % Angle (in radians) of the major axis of the ellipse with respect to a horizontal line going through [cx cy] and measure counter-clockwise.

x = (cx - 2 .* rx):.01:(cx + 2 .* rx); % x values.

% y values, you need to take both the positive and the negative result of the square root.
yplus = sqrt((1 - ((x - cx).^2)./(rx^2)) .* (ry^2)) + cy;
yminus = -1 .* sqrt((1 - ((x - cx).^2)./(rx^2)) .* (ry^2)) + cy;

% Connect in the right order and shift everything so that the center of the ellipse is now at [0 0].
x = cat(2, x, x(end:-1:1)) - cx;
y = cat(2, yplus, yminus(end:-1:1)) - cy;

% Remove imaginary numbers (negative square roots).
ind = find(angle(y) == 0. | angle(y) == pi);
x = x(ind);
y = y(ind);

% Rotate counter-clockwise around [0 0] by theta_rad radians ...
costheta = cos(theta_rad);
sintheta = sin(theta_rad);

xrot = costheta .* x - sintheta .* y;
yrot = sintheta .* x + costheta .* y;

% Extract length of the rotated ellipse along the x and y axes.
[dummy ind] = sort(abs(yrot));
xlength = abs(xrot(ind(1)))*2;
[dummy ind] = sort(abs(xrot));
ylength = abs(yrot(ind(1)))*2;

% And shifting back so that the center is in [cx cy].
xrot = xrot + cx;
yrot = yrot + cy;
