% *********************************************************************************
function cb_painttrajectorylabels(fig, tr, tr_sz, tr_color, flag)

cb_erasetrajectorylabels(fig);

if flag
        ud = get(fig, 'UserData');
        curslice = ud.curslice + 1;

        for ii = 1 : size(tr, 1)
                if isempty(tr{ii, 1, curslice})
                        continue;
                end
                % This selects coordinates in the "center" of the trajectory.
                %[dummy coords(1) coords(2)] = polycenter(tr{ii, 1, curslice}(:, 1), tr{ii, 1, curslice}(:, 2));
                
                % This, on the other hand, selects a point along the trajectory.
                idx = floor(size(tr{ii, 1, curslice}, 1)/2);
                coords = tr{ii, 1, curslice}(idx, 1:2);
                
                if sum(coords == [-1 -1]) ~= 2
                        coords(1:2) = coords(1:2) + 2; % Move away from the position of the actual marker.
                        cb_drawfiduciallabel(fig, coords, ii, tr_sz, tr_color, 'Trajectory_Label');
                end
        end
end
end