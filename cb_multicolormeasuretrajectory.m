% [ang l int npix] = cb_multicolormeasuretrajectory(fig, tr, brush_sz, slice_index, intensity_fn)
% Approximates angle (in radians) as the angle between first and last points.
% Length is the sum of the lengths of all the segments that form the trajectory.
% int is the (mean, sum) intensity across the entire trajectory (width determined by
% brush_sz). intensity_fn is a pointer to a function (e.g. @mean) that determines the
% function used to evaluate the intensity of the trajectory. npix is the number of pixels
% covered by the mask used to quantify the trajectory.

function [ang l int npix] = cb_multicolormeasuretrajectory(fig, tr, brush_sz, slice_index, intensity_fn)

if nargin < 5
    intensity_fn = @mean; %Other potential values are @sum, @max, @median, ...
end

ang = 0;
l = 0;
int = [];
npix = 0;

channels = 1:3;

udata = get(fig, 'UserData');

if nargin < 4
    slice_index = udata.curslice;
end

if isempty(tr)
    fprintf('Empty trajectory at time point %.0d!!!\n', slice_index);
    return;
end

x = [tr(1, 1) tr(end, 1)];
y = [tr(1, 2) tr(end, 2)];

% Angle.
if (x(1) < x(2))
    p1 = [x(1) y(1)];
    p2 = [x(2) y(2)];
else
    p2 = [x(1) y(1)];
    p1 = [x(2) y(2)];
end

% Compute vector.
v = p2 - p1;
l = norm(v, 2);

% If the second point is under the first one (largest y)
% then the angle is larger than pi/2.
if (l ~= 0 && p2(2) > p1(2))
    ang = pi - asin(abs(v(2))/l);
    % Else the angle is smaller than pi/2.
elseif (l ~= 0)
    ang = asin(abs(v(2)) / l);
    % Else where are trying to measure the angle formed by a dot!!
else
    ang = 0.0;
end

% END ANGLE

% Create intensity mask and compute length: using a polygon (different perceived thicknesses depending on the line orientation).
%        mask = dip_image(zeros(udata.imsize(2), udata.imsize(1), 1), 'bin8');
%        l = 0;
%        for ii = 1:(size(tr, 1)-1)
%          % length
%          l = l + norm([tr(ii+1,1)-tr(ii,1) tr(ii+1,2)-tr(ii,2)], 2);
%          [x y] = pixinline(tr(ii:(ii+1), 1:2), brush_sz);
%          mask(sub2ind([udata.imsize(2), udata.imsize(1)], y, x)) = 1;
%        end

% Compute intensity only if the output argument is there.
if nargout >= 3
    % Create intensity mask and compute length: using multiple bresenham lines (constant perceived thickness, this is the good one!).
    try
        mask = dip_image(zeros(udata.imsize(2), udata.imsize(1), 1), 'bin8');
        l = 0;
        for ii = 1:(size(tr, 1)-1)
            % length
            l = l + norm([tr(ii+1,1)-tr(ii,1) tr(ii+1,2)-tr(ii,2)], 2);
            [x y] = pixinline_bresenham(tr(ii:(ii+1), 1:2), brush_sz);
            mask(sub2ind([udata.imsize(2), udata.imsize(1)], y, x)) = 1;
        end
    catch
        tr
    end
    npix = nnz(double(mask)); % Number of pixels covered by mask.
    
    %Make function cb_showmask:
    %overlay(udata.colordata{1}, mask);
    %dipshow(mask);
    
    if (isfield(udata, 'colordata')) & (~ isempty(udata.colordata))
        int = zeros(1, numel(channels));
        for ic = 1:numel(channels)
            channel = channels(ic);
            im = udata.slices{channel}(:, :, slice_index);
            if (~ isempty(im(mask)))
                int(ic) = intensity_fn(im(mask));
                % If the image where intensities should be measured is empty, we are probably
                % measuring the intensity of a point.
                %                        else
                %                                tmp = cb_getpixvalue(fig);
                %                                int(ic) = tmp(channel);
            end
        end
    else
        int = zeros(1, 3);
        im = udata.slices(:, :, slice_index);
        if (~ isempty(im(mask)))
            if isempty(udata.rcurrchannel) | (udata.rcurrchannel == 0)
                udata.rcurrchannel = (1:3);
            end
            
            int(udata.rcurrchannel) = intensity_fn(im(mask));
            % If the image where intensities should be measured is empty, we are probably
            % measuring the intensity of a point.
            %                else
            %                        int(udata.rcurrchannel) = cb_getpixvalue(fig);
        end
    end
end
% END INTENSITY

end
