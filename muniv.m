% [mpr, mipr, dt, pposi] = muniv(positions, populations, p, r, neigh_graph, weighted, shells)
% [mpr, mipr, dt, pposi] = muniv(positions, populations, p, r, graph_type, weighted, shells)
% [mpr, mipr, dt, pposi] = muniv(positions, populations, p, r, dt, shells)
% [mpr, mipr, dt, pposi] = muniv(populations, p, r, dt, shells)
%
% Computes the mvalues at a range, r, of distances for population p.
% Based on Fernandez-Gonzalez et al., "A tool for the quantitative 
% spatial analysis of complex cellular systems", IEEE Transactions in 
% Image Processing 14(9):1300-1313, 2005.
% 
% POSITIONS is a matrix where each row represents the coordinates of a point.
% POPULATIONS is a vector where index i indicates the population of position i.
% P is the population of interest.
% R is the maximum distance/s to consider two positions as neighbors.
% NEIGH_GRAPH (optional) is a matrix where (i, j) = 1 indicates that positions 
% i and j are immediate neighbors.
% GRAPH_TYPE (optional) is a string that determines the type of neighborhood 
% graph to be built if neigh_graph is not given: 'euclidean', 'delaunay', 
% 'rng', 'refrng', 'knn'.
% WEIGHTED determines whether to use number of edges (0) or edge 
% lengths to measure distances.
% DT is the table of minimum distances among any two positions.
% SHELLS is zero for 'within' (<=r) analysis and non-zero for 'at'
% (==r) analysis.
%
% MPR is a list of M values that show the distribution of positions belonging
% to P within the range of distances R.
% MIPR is a list of M values per position belonging to P.
% PPOSI contains the indices of the positions belonging to P.
% Rodrigo Fernandez-Gonzalez
% fernanr1@mskcc.org
% 11/13/2006

function varargout = muniv(varargin)

% Default values for some parameters. This will be modified in a second
% if provided.
r = (0:5);
graph_type = 'euclidean';
weighted = 0;
shells = 0;

% Check parameters and generate neighborhood graph (if not provided)
% and table of distances between positions.
switch (nargin)
	case 5
		populations = varargin{1, 1};
		if (~ ((isnumeric(populations) || islogical(populations)) && prod(size(populations)) > 1 && size(populations, 3) == 1))
			error('??? First argument should be a 2D-matrix.');
		end
		
		p = varargin{1, 2};
		if (~ (isnumeric(p) && prod(size(p)) == 1))
			error('??? Second argument should be a number.');
		end

		r = varargin{1, 3};
		if (~ isnumeric(r))
			error('??? Third argument should be numeric.');
		end
		
		dt = varargin{1, 4};
		if (~ (isnumeric(dt) && prod(size(dt)) > 1))
			error('??? Fourth argument should be a matrix.');
		end
		
		shells = varargin{1, 5};
		if (~ (isnumeric(shells) && numel(shells) == 1))
			error('??? Fifth argument should be a number.');
		end
		
	case 6 % This here for compatibility with a previous version. Still, POSITIONS will not be used at all.
		positions = varargin{1, 1};
		if (~ (isnumeric(positions) && prod(size(positions)) > 1 && size(positions, 3) == 1))
			error('??? First argument should be a 2D-matrix.');
		end

		populations = varargin{1, 2};
		if (~ ((isnumeric(populations) || islogical(populations)) && prod(size(populations)) > 1 && size(populations, 3) == 1))
			error('??? Second argument should be a 2D-matrix.');
		end
		
		p = varargin{1, 3};
		if (~ (isnumeric(p) && prod(size(p)) == 1))
			error('??? Third argument should be a number.');
		end

		r = varargin{1, 4};
		if (~ isnumeric(r))
			error('??? Fourth argument should be numeric.');
		end
		
		dt = varargin{1, 5};
		if (~ (isnumeric(dt) && prod(size(dt)) > 1))
			error('??? Fifth argument should be a matrix.');
		end
		
		shells = varargin{1, 6};
		if (~ (isnumeric(shells) && numel(shells) == 1))
			error('??? Sixth argument should be a number.');
		end
		
	case 7
		positions = varargin{1, 1};
		if (~ (isnumeric(positions) && prod(size(positions)) > 1 && size(positions, 3) == 1))
			error('??? First argument should be a 2D-matrix.');
		end

		populations = varargin{1, 2};
		if (~ ((isnumeric(populations) || islogical(populations)) && prod(size(populations)) > 1 && size(populations, 3) == 1))
			error('??? Second argument should be a 2D-matrix.');
		end
		
		p = varargin{1, 3};
		if (~ (isnumeric(p) && prod(size(p)) == 1))
			error('??? Third argument should be a number.');
		end

		r = varargin{1, 4};
		if (~ isnumeric(r))
			error('??? Third argument should be numeric.');
		end
		
		if (isstr(varargin{1, 5}))
			graph_type = varargin{1, 5};
		elseif (isnumeric(varargin{1, 5}) && prod(size(varargin{1, 5})) > 1)
			neigh_graph = varargin{1, 5};
		else
			error('??? Fifth argument should be a matrix or a string.');
		end
		
		weighted = varargin{1, 6};
		if (~ (isnumeric(weighted) && prod(size(weighted)) == 1))
			error('??? Sixth argument should be a number.');
		end
		
		if (isstr(varargin{1, 5}))
			[dt, neigh_graph] = mdistancetable(positions, graph_type, weighted);
		else
			dt = mdistancetable(positions, neigh_graph, weighted);
		end
		
		shells = varargin{1, 7};
		if (~ (isnumeric(shells) && numel(shells) == 1))
			error('??? Seventh argument should be a number.');
		end
		
	otherwise
		error('??? Wrong number of input arguments.');			
end

% Indices to positions belonging to population p.
pposi = find(populations == p);

% If there are no positions belonging to p return an empty array.
if (isempty(pposi))
	%error(cat(2, 'No positions belong to population ', num2str(p)));
	varargout{1, 1} = [];
	varargout{1, 2} = [];
	varargout{1, 3} = [];
	varargout{1, 4} = [];
	return;
	
% And if there is only one, return a zero array.
elseif (numel(pposi) == 1)
	varargout{1, 1} = zeros(size(r));
	varargout{1, 2} = zeros(1, numel(r));
	varargout{1, 3} = dt;
	varargout{1, 4} = pposi;
	return;
end

% Scale factor.
N = numel(populations);
Np = numel(pposi);
% Here 1 is substracted at both numerator and denominator so that the M value
% converges to one (when all the positions in the image, but the one under
% analysis, are considered.
if (~ shells)
	sc = (N - 1.0) / (Np - 1.0);
else
	sc = N / Np; % No need to correct here, since the entire image will never be included, and therefore the total will never tend to one.
end

[nir, nipr] = mnipr(pposi, p, r, dt, populations, shells);

% These three lines ignore isolated positions (those with no neighbors).
% IS THIS THE BEST WAY TO DEAL WITH THIS???
noneighbors = find(nir == 0);
nipr(noneighbors) = 0; % This line is probably not necessary.
nir(noneighbors) = 1; % To avoid division by zero.

% Array of per-position M values (mipr).
varargout{1, 2} = sc .* nipr ./ nir;

% Global M values (mpr).
varargout{1, 1} = sum(varargout{1, 2}) ./ Np;
varargout{1, 3} = dt;
varargout{1, 4} = pposi;

return;
