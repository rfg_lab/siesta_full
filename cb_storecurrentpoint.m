% *********************************************************************************

function [fidu2 nfidu2] = cb_storecurrentpoint(coords, fidu, nfidu)

nfidu(coords(3) + 1) = nfidu(coords(3) + 1) + 1;

if (size(fidu, 1) < nfidu(coords(3) + 1))
	fidu = cat(1, fidu, repmat([-1 -1 -1], [nfidu(coords(3) + 1) - size(fidu, 1), 1, numel(nfidu)]));
end

% Look for the first empty slot.
for i = 1 : size(fidu, 1)
	c = fidu(i, 1:2, coords(3) + 1);
	
	if (c(1) < 0 || c(2) < 0)
		fidu(i, :, coords(3) + 1) = coords;
		break;
	end
end

fidu2 = fidu;
nfidu2 = nfidu;
end