function udata = display_data(fig,imh,udata)
cdata = udata.imagedata;
iscol = ~isempty(udata.colspace);
if ~iscol & islogical(cdata)
   cdata = uint8(cdata);
   mappingmode = 'normal';
   currange = [0,1];
else
   mappingmode = udata.mappingmode;
   cdata = mapcomplexdata(cdata,udata.complexmapping);
   switch mappingmode
      case 'angle'
         currange = [-pi,pi];
      case 'orientation'
         currange = [-pi,pi]/2;
      case 'log'
         udata = set_udata_computed_log(udata,cdata);
         if ~isempty(udata.computed.canlog)
            warning(['Image contains ',udata.computed.canlog,'. Cannot perform log stretch.'])
            mappingmode = 'normal';
            currange = [0,255];
         else
            %??? The LOG-stretch is quite difficult.
            %udata = set_udata_computed_lin(udata,cdata);
            %currange = udata.computed.lin;
            %if currange(1)~=0
            %   cdata = cdata - currange(1);
            %end
            %newmax = 130.65 - 1;
            %if (currange(2)-currange(1))~=newmax
            %   cdata = cdata*(newmax/(currange(2)-currange(1)));
            %end
            %cdata = log2(cdata+1);
            %currange = [0,log2(130.65)];
            currange = udata.computed.lin;
            newmax = udata.computed.log(1);
            if currange(1)~=0
               cdata = cdata - currange(1);
            end
            if (currange(2)-currange(1))~=newmax
               cdata = cdata*(newmax/(currange(2)-currange(1)));
            end
            cdata = log2(cdata+1);
            currange = [0,udata.computed.log(2)];
         end
      case 'base'
         if ~isfield(udata,'computed') | ~isfield(udata.computed,'base')
            udata = set_udata_computed_lin(udata,cdata);
            currange = udata.computed.lin;
            currange(2) = max(abs(currange));
            currange(1) = -currange(2);
            udata.computed.base = currange;
         else
            currange = udata.computed.base;
         end
      case 'percentile'
         udata = set_udata_computed_percentile(udata,cdata);
         currange = udata.computed.percentile;
      case 'lin'
         udata = set_udata_computed_lin(udata,cdata);
         currange = udata.computed.lin;
      case 'normal'
         currange = [0,255];
      case '12bit'
         currange = [0 4095];
      case '16bit'
         currange = [0 65535];
      case 's8bit'
         currange = [-128 127];
      case 's12bit'
         currange = [-2048 2047];
      case 's16bit'
         currange = [-32768 32767];
      case ''
         % use existing currange
         currange = udata.currange;
         if length(currange)~=2
            currange = [0,255];
            mappingmode = 'normal';
         end
      otherwise % not supposed to happen
         currange = [0,255];
         mappingmode = 'normal';
   end
   if currange(1) == currange(2)
      currange = currange + [-1,1];
   elseif currange(1) > currange(2)
      currange = currange([2,1]);
   end
   if currange(1) ~= 0
      cdata = cdata-currange(1);
   end
   if iscol
      if currange(2)-currange(1) ~= 1
         cdata = cdata*(1/(currange(2)-currange(1)));
      end
      %if ~any(strcmp(mappingmode,{'lin','log','base'}))
         % we cannot skip the clip. Apparently it is really bad to have any
         % pixel with value 1+1e-9.
      cdata = dip_clip(cdata,0,1,'both');
      %end
      %if any(dipgetpref('Gamma')~=1)
         %range of cdata is here already [0,1], need no further scaling
        % g = dipgetpref('Gamma');
        % cdata(:,:,0) = cdata(:,:,0).^g(1);
        % cdata(:,:,1) = cdata(:,:,1).^g(2);
        % cdata(:,:,2) = cdata(:,:,2).^g(3);
        % cdata = dip_clip(cdata,0,1,'both');
      %end
      cdata = double(cdata);
   else
      if currange(2)-currange(1) ~= 255
         cdata = cdata*(255/(currange(2)-currange(1)));
      end
      if strcmp(udata.colmap,'labels')
         cdata = mod(cdata-currange(1),currange(2)-currange(1)+1);
      end
      %g = dipgetpref('GammaGrey');
      %if g~=1
      %   cdata = 255*(cdata./255).^g; %range must be 255
      %end
      if ~any(strcmp(mappingmode,{'lin','log','base'}))
         cdata = dip_clip(cdata,0,255,'both');
      end
      cdata = uint8(cdata);
   end
end
set(imh,'cdata',cdata);
udata.mappingmode = mappingmode;
udata.currange = currange;
set(fig, 'UserData', []); % Adding this line speeds the next one up 1000x!!!!!
set(fig, 'UserData', udata);
end

