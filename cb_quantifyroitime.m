% [pix_vals_r pix_vals_g pix_vals_b] = cb_quantifyroitime(fig, channel, brush_sz, tp_list)
function [pix_vals_r pix_vals_g pix_vals_b] = cb_quantifyroitime(fig, channel, brush_sz, tp_list)

udata = get(fig, 'UserData');

pix_vals_r = nan .* zeros(1, numel(tp_list)); % One row per trajectory, one column per time point.
pix_vals_g = nan .* zeros(1, numel(tp_list)); % One row per trajectory, one column per time point.
pix_vals_b = nan .* zeros(1, numel(tp_list)); % One row per trajectory, one column per time point.

% For every time point.
for ii = 1:numel(tp_list)
    thetp = tp_list(ii);
    
    if (~ isfield(udata, 'colordata')) | isempty(udata.colordata)
        im = udata.slices(:, :, thetp-1);
        
    else
        im = joinchannels('rgb', udata.slices{1}(:, :, 1), udata.slices{2}(:, :, 1), udata.slices{3}(:, :, 1));
    end
    
    if ((numel(udata.rpolygons(:, :, thetp)) > 0) & (~ isempty(udata.rpolygons{1, :, thetp})))
        roi = udata.rpolygons{1, :, thetp};
        mask = dip_image(zeros(udata.imsize(2), udata.imsize(1), 1), 'bin8');
        mask((min(roi(:, 1))+2):(max(roi(:, 1))-2), min(roi(:, 2)):max(roi(:, 2))) = 1; %+2 and -2 try to restrict the region to the center of the edge.
        
        
        if (~ isfield(udata, 'colordata')) | isempty(udata.colordata)
            theintensity = repmat(mean(im(mask)), [1, 3]);
            
            pix_vals_r(ii) = theintensity(1);
            pix_vals_g(ii) = theintensity(2);
            pix_vals_b(ii) = theintensity(3);
        else
            pix_vals_r(ii) = mean(im{1}(mask));
            pix_vals_g(ii) = mean(im{2}(mask));
            pix_vals_b(ii) = mean(im{3}(mask));
        end
    end
    
end
end