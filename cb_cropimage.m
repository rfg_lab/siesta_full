% ud = cb_cropimage(fig, sz)
% If there is a polygon drawn in this slice, use its bounding box to crop. Otherwise, use a unique marker.
function ud = cb_cropimage(fig, sz)

ax_children = get(get(fig, 'CurrentAxes'), 'Children');
img = findobj(ax_children, 'Type', 'image');
ud = get(fig, 'UserData');

pnt = ud.runiquecoords(ud.curslice+1, :);
poly = ud.rpolygons{1, 1, ud.curslice+1};

if ~ isempty(poly)
        % X crop.
        xmin = min(poly(:, 1));
        xmax = max(poly(:, 1));
        
        % Make sure that there is enough room to cut.
        if xmin < 0
                xmax = xmax + abs(xmin);
                xmin = 0;
                
                if xmax >= ud.imsize(1)
                        return;
                end
        end
        
        if xmax >= ud.imsize(1)
                xmin = xmin - (xmax - ud.imsize(1) + 1);
                xmax = ud.imsize(1) - 1;
                
                if xmin < 0
                        return;
                end
        end
        
        % Y crop.
        % Check how far to crop above and below the current point.
        ymin = min(poly(:, 2));
        ymax = max(poly(:, 2));
         
        % Make sure that there is enough room to cut.
        if ymin < 0
                ymax = ymax + abs(ymin);
                ymin = 0;
                
                if ymax >= ud.imsize(2)
                        return;
                end
        
        elseif ymax >= ud.imsize(2)
                ymin = ymin - (ymax - ud.imsize(2) + 1);
                ymax = ud.imsize(2) - 1;
                
                if ymin < 0
                        return;
                end
        end
        
        % Crop image.
        ud.slices = ud.slices{:}(xmin:xmax, ymin:ymax, :);
        ud.imsize = size(ud.slices{1});
        %curr_dir = ud.rcurrdir;
        %thezoom = ud.zoom;
        %ud = init_userdata(ud);
        %ud.rcurrdir = curr_dir;
        %ud.mappingmode = 'lin';
        %ud.colspace = '';
        %ud.colordata = '';
        %ud.zoom = thezoom;
        
        % Change image object size.
        set(img, 'XData', [0 (ud.imsize(1)-1)], 'YData', [0 (ud.imsize(2)-1)]);
        
        % Grey-scale images.
        if (~ isfield(ud, 'colordata')) | isempty(ud.colordata)
                ud.imagedata = squeeze(ud.slices(:, :, ud.curslice));
                
                if ~ isempty(ud.rchannels)
                        ud.rchannels = [];
                end
                display_data(fig, img, ud);
        
        % Color images
        else
                ud.colordata = ud.slices{:}(:, :, ud.curslice);
                ud.imagedata = cat(3, ud.colordata{1}(:, :, 0), ud.colordata{2}(:, :, 0), ud.colordata{3}(:, :, 0));
                ud.colspace = 'RGB';
                ud.rcurrchannel = 0;
                ud.rchannels = ud.slices;
                display_data(fig, img, ud);
        end

        % Change figure size.
        diptruesize(fig, 100 * ud.zoom);
        
        d = repmat(-1.*[xmin ymin], [ud.imsize(3), 1]);
        ind = 1;
        % Correct markings.
        for ii = 1:ud.imsize(3)
                % Correct fiducials.
                if ud.rnfiducials(ii) > 0
                        ud.rfiducials(:, 1:2, ii) = ud.rfiducials(:, 1:2, ii) + repmat(d(ind, :), [size(ud.rfiducials, 1) 1]);
                end
                
                % Correct unique markers.
                if ud.runiquecoords(ii, 1) > -1
                        ud.runiquecoords(ii, 1:2) = ud.runiquecoords(ii, 1:2) + d(ind, :);
                end
                
                % Circles and polylines.
                for pp = 1:size(ud.rpolygons, 1)
                        if (~ isempty(ud.rpolygons{pp, 1, ii}))
                                ud.rpolygons{pp, 1, ii}(:, 1:2) = ud.rpolygons{pp, 1, ii}(:, 1:2) + repmat(d(ind, 1:2), [size(ud.rpolygons{pp, 1, ii}(:, 1:2), 1) 1]);
                        end
                end
                        
                % Correct other types of marking here.
                % Open polylines.
                for pp = 1:size(ud.rtrajectories, 1)
                        if (~ isempty(ud.rtrajectories{pp, 1, ii}))
                                ud.rtrajectories{pp, 1, ii}(:, 1:2) = ud.rtrajectories{pp, 1, ii}(:, 1:2) + repmat(d(ind, 1:2), [size(ud.rtrajectories{pp, 1, ii}(:, 1:2), 1) 1]);
                        end
                end
                
                % Increase the counter of corrected frames visited.
                ind = ind + 1;
        end

elseif sum(pnt ~= [-1 -1])
	% X crop.
	% Check how far to crop to the left and right of the current point.
	if mod(sz(1), 2) == 0
		half1 = (sz(1) / 2) - 1;
		half2 = sz(1) / 2;
	else
		half1 = floor(sz(1) / 2);
		half2 = half1;
	end
		
	xmin = pnt(1) - half1;
	xmax = pnt(1) + half2;
	
	% Make sure that there is enough room to cut.
	if xmin < 0
		xmax = xmax + abs(xmin);
		xmin = 0;
		
		if xmax >= ud.imsize(1)
			return;
		end
	end
	
	if xmax >= ud.imsize(1)
		xmin = xmin - (xmax - ud.imsize(1) + 1);
		xmax = ud.imsize(1) - 1;
		
		if xmin < 0
			return;
		end
	end
	
	% Y crop.
	% Check how far to crop above and below the current point.
	if mod(sz(2), 2) == 0
		half1 = (sz(2) / 2) - 1;
		half2 = sz(2) / 2;
	else
		half1 = floor(sz(2) / 2);
		half2 = half1;
	end
		
	ymin = pnt(2) - half1;
	ymax = pnt(2) + half2;
	
	% Make sure that there is enough room to cut.
	if ymin < 0
		ymax = ymax + abs(ymin);
		ymin = 0;
		
		if ymax >= ud.imsize(2)
			return;
		end
	
	elseif ymax >= ud.imsize(2)
		ymin = ymin - (ymax - ud.imsize(2) + 1);
		ymax = ud.imsize(2) - 1;
		
		if ymin < 0
			return;
		end
	end
	
	% Crop image.
	ud.slices = ud.slices{:}(xmin:xmax, ymin:ymax, :);
	ud.imsize = size(ud.slices{1});
	%curr_dir = ud.rcurrdir;
	%thezoom = ud.zoom;
	%ud = init_userdata(ud);
	%ud.rcurrdir = curr_dir;
	%ud.mappingmode = 'lin';
	%ud.colspace = '';
	%ud.colordata = '';
	%ud.zoom = thezoom;
	
	% Change image object size.
	set(img, 'XData', [0 (ud.imsize(1)-1)], 'YData', [0 (ud.imsize(2)-1)]);
	
	% Grey-scale images.
	if (~ isfield(ud, 'colordata')) | isempty(ud.colordata)
		ud.imagedata = squeeze(ud.slices(:, :, ud.curslice));
                
                if ~ isempty(ud.rchannels)
                        ud.rchannels = [];
                end
		display_data(fig, img, ud);
	
	% Color images
	else
		ud.colordata = ud.slices{:}(:, :, ud.curslice);
		ud.imagedata = cat(3, ud.colordata{1}(:, :, 0), ud.colordata{2}(:, :, 0), ud.colordata{3}(:, :, 0));
		ud.colspace = 'RGB';
		ud.rcurrchannel = 0;
		ud.rchannels = ud.slices;
		display_data(fig, img, ud);
	end

	% Change figure size.
	diptruesize(fig, 100 * ud.zoom);
	
	d = repmat(-1.*[xmin ymin], [ud.imsize(3), 1]);
	ind = 1;
	% Correct markings.
	for ii = 1:ud.imsize(3)
		% Correct fiducials.
		if ud.rnfiducials(ii) > 0
			ud.rfiducials(:, 1:2, ii) = ud.rfiducials(:, 1:2, ii) + repmat(d(ind, :), [size(ud.rfiducials, 1) 1]);
		end
		
		% Correct unique markers.
		if ud.runiquecoords(ii, 1) > -1
			ud.runiquecoords(ii, 1:2) = ud.runiquecoords(ii, 1:2) + d(ind, :);
		end
		
		% Circles and polylines.
		for pp = 1:size(ud.rpolygons, 1)
			if (~ isempty(ud.rpolygons{pp, 1, ii}))
				ud.rpolygons{pp, 1, ii}(:, 1:2) = ud.rpolygons{pp, 1, ii}(:, 1:2) + repmat(d(ind, 1:2), [size(ud.rpolygons{pp, 1, ii}(:, 1:2), 1) 1]);
			end
		end
			
		% Correct other types of marking here.
		% Open polylines.
		for pp = 1:size(ud.rtrajectories, 1)
                        if (~ isempty(ud.rtrajectories{pp, 1, ii}))
                                ud.rtrajectories{pp, 1, ii}(:, 1:2) = ud.rtrajectories{pp, 1, ii}(:, 1:2) + repmat(d(ind, 1:2), [size(ud.rtrajectories{pp, 1, ii}(:, 1:2), 1) 1]);
                        end
                end
                
		% Increase the counter of corrected frames visited.
		ind = ind + 1;
	end
end
end